﻿Public Class FrmSelEstado

    Private Sub FrmSelEstadosFil_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Por si es uno
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@identificador", SqlDbType.BigInt, identificador)
        BaseII.CreateMyParameter("@numero", ParameterDirection.Output, SqlDbType.Int)
        BaseII.ProcedimientoOutPut("DameNumEstadosCompania")
        Dim numciudades As Integer = BaseII.dicoPar("@numero")
        If numciudades = 1 Then
            'Dim conexion As New SqlConnection(MiConexion)
            'conexion.Open()
            'Dim comando As New SqlCommand()
            'comando.Connection = conexion
            'comando.CommandText = " exec DameIdentificador"
            'identificador = comando.ExecuteScalar()
            'conexion.Close()
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@identificador", SqlDbType.BigInt, identificador)
            BaseII.Inserta("InsertaSeleccionEstadotbl")
            llevamealotro()
            Exit Sub
        End If
        'Fin Por si es uno
        colorea(Me)

        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@opcion", SqlDbType.Int, 1)
        BaseII.CreateMyParameter("@clv_estado", SqlDbType.Int, 0)
        BaseII.CreateMyParameter("@identificador", SqlDbType.Int, identificador)
        BaseII.Inserta("ProcedureSeleccionEstado")

        llenalistboxs()
    End Sub
    Private Sub llenalistboxs()
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@opcion", SqlDbType.Int, 2)
        BaseII.CreateMyParameter("@clv_estado", SqlDbType.Int, 0)
        BaseII.CreateMyParameter("@identificador", SqlDbType.Int, identificador)
        loquehay.DataSource = BaseII.ConsultaDT("ProcedureSeleccionEstado")
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@opcion", SqlDbType.Int, 3)
        BaseII.CreateMyParameter("@clv_estado", SqlDbType.Int, 0)
        BaseII.CreateMyParameter("@identificador", SqlDbType.Int, identificador)
        seleccion.DataSource = BaseII.ConsultaDT("ProcedureSeleccionEstado")
    End Sub

    Private Sub agregar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles agregar.Click
        If loquehay.Items.Count = 0 Then
            Exit Sub
        End If
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@opcion", SqlDbType.Int, 4)
        BaseII.CreateMyParameter("@clv_estado", SqlDbType.Int, loquehay.SelectedValue)
        BaseII.CreateMyParameter("@identificador", SqlDbType.Int, identificador)
        BaseII.Inserta("ProcedureSeleccionEstado")
        llenalistboxs()
    End Sub

    Private Sub quitar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles quitar.Click
        If seleccion.Items.Count = 0 Then
            Exit Sub
        End If
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@opcion", SqlDbType.Int, 5)
        BaseII.CreateMyParameter("@clv_estado", SqlDbType.Int, seleccion.SelectedValue)
        BaseII.CreateMyParameter("@identificador", SqlDbType.Int, identificador)
        BaseII.Inserta("ProcedureSeleccionEstado")
        llenalistboxs()
    End Sub

    Private Sub agregartodo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles agregartodo.Click
        If loquehay.Items.Count = 0 Then
            Exit Sub
        End If
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@opcion", SqlDbType.Int, 6)
        BaseII.CreateMyParameter("@clv_estado", SqlDbType.Int, 0)
        BaseII.CreateMyParameter("@identificador", SqlDbType.Int, identificador)
        BaseII.Inserta("ProcedureSeleccionEstado")
        llenalistboxs()
    End Sub

    Private Sub quitartodo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles quitartodo.Click
        If seleccion.Items.Count = 0 Then
            Exit Sub
        End If
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@opcion", SqlDbType.Int, 7)
        BaseII.CreateMyParameter("@clv_estado", SqlDbType.Int, 0)
        BaseII.CreateMyParameter("@identificador", SqlDbType.Int, identificador)
        BaseII.Inserta("ProcedureSeleccionEstado")
        llenalistboxs()
    End Sub

    Private Sub Button5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button5.Click
        EntradasSelCiudad = 0
        Me.Close()
    End Sub

    Private Sub Button6_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button6.Click
        FrmSelCiudadJ.Show()
        Me.Close()
    End Sub
    Private Sub llevamealotro()
        FrmSelCiudadJ.Show()
        Me.Close()
    End Sub
End Class