<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmOpRep
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.CMBLabel1 = New System.Windows.Forms.Label()
        Me.CMBLabel2 = New System.Windows.Forms.Label()
        Me.DateTimePicker1 = New System.Windows.Forms.DateTimePicker()
        Me.DateTimePicker2 = New System.Windows.Forms.DateTimePicker()
        Me.ComboBox1 = New System.Windows.Forms.ComboBox()
        Me.MUESTRAUSUARIOS1BindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.NewsoftvDataSet = New softvFacturacion.NewsoftvDataSet()
        Me.MUESTRAUSUARIOSBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.CMBLabel3 = New System.Windows.Forms.Label()
        Me.NewsoftvDataSetBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.MUESTRAUSUARIOSTableAdapter = New softvFacturacion.NewsoftvDataSetTableAdapters.MUESTRAUSUARIOSTableAdapter()
        Me.ComboBox2 = New System.Windows.Forms.ComboBox()
        Me.MUESTRACAJASBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.ComboBox3 = New System.Windows.Forms.ComboBox()
        Me.MUESTRASUCURSALESBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.MUESTRASUCURSALESTableAdapter = New softvFacturacion.NewsoftvDataSetTableAdapters.MUESTRASUCURSALESTableAdapter()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.Button2 = New System.Windows.Forms.Button()
        Me.CheckBox1 = New System.Windows.Forms.CheckBox()
        Me.MUESTRAUSUARIOS1TableAdapter = New softvFacturacion.NewsoftvDataSetTableAdapters.MUESTRAUSUARIOS1TableAdapter()
        Me.MUESTRACAJASTableAdapter = New softvFacturacion.NewsoftvDataSetTableAdapters.MUESTRACAJASTableAdapter()
        Me.Procedimientos_arnoldo = New softvFacturacion.Procedimientos_arnoldo()
        Me.MUESTRAVENDORES_TodosTableAdapter = New softvFacturacion.Procedimientos_arnoldoTableAdapters.MUESTRAVENDORES_TodosTableAdapter()
        Me.MUESTRAVENDORESTodosBindingSource1 = New System.Windows.Forms.BindingSource(Me.components)
        Me.ComboVendedores = New System.Windows.Forms.ComboBox()
        Me.Button3 = New System.Windows.Forms.Button()
        CType(Me.MUESTRAUSUARIOS1BindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.NewsoftvDataSet, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.MUESTRAUSUARIOSBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.NewsoftvDataSetBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.MUESTRACAJASBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.MUESTRASUCURSALESBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Procedimientos_arnoldo, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.MUESTRAVENDORESTodosBindingSource1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'CMBLabel1
        '
        Me.CMBLabel1.AutoSize = True
        Me.CMBLabel1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBLabel1.ForeColor = System.Drawing.Color.FromArgb(CType(CType(128, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.CMBLabel1.Location = New System.Drawing.Point(24, 28)
        Me.CMBLabel1.Name = "CMBLabel1"
        Me.CMBLabel1.Size = New System.Drawing.Size(186, 16)
        Me.CMBLabel1.TabIndex = 6
        Me.CMBLabel1.Text = "Fecha Inicial del Reporte:"
        '
        'CMBLabel2
        '
        Me.CMBLabel2.AutoSize = True
        Me.CMBLabel2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBLabel2.ForeColor = System.Drawing.Color.FromArgb(CType(CType(128, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.CMBLabel2.Location = New System.Drawing.Point(24, 95)
        Me.CMBLabel2.Name = "CMBLabel2"
        Me.CMBLabel2.Size = New System.Drawing.Size(179, 16)
        Me.CMBLabel2.TabIndex = 7
        Me.CMBLabel2.Text = "Fecha Final del Reporte:"
        '
        'DateTimePicker1
        '
        Me.DateTimePicker1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.DateTimePicker1.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.DateTimePicker1.Location = New System.Drawing.Point(27, 54)
        Me.DateTimePicker1.Name = "DateTimePicker1"
        Me.DateTimePicker1.Size = New System.Drawing.Size(163, 21)
        Me.DateTimePicker1.TabIndex = 0
        '
        'DateTimePicker2
        '
        Me.DateTimePicker2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.DateTimePicker2.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.DateTimePicker2.Location = New System.Drawing.Point(27, 123)
        Me.DateTimePicker2.Name = "DateTimePicker2"
        Me.DateTimePicker2.Size = New System.Drawing.Size(163, 21)
        Me.DateTimePicker2.TabIndex = 1
        '
        'ComboBox1
        '
        Me.ComboBox1.DataSource = Me.MUESTRAUSUARIOS1BindingSource
        Me.ComboBox1.DisplayMember = "NOMBRE"
        Me.ComboBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ComboBox1.ForeColor = System.Drawing.Color.Black
        Me.ComboBox1.FormattingEnabled = True
        Me.ComboBox1.Location = New System.Drawing.Point(241, 66)
        Me.ComboBox1.Name = "ComboBox1"
        Me.ComboBox1.Size = New System.Drawing.Size(189, 23)
        Me.ComboBox1.TabIndex = 2
        Me.ComboBox1.ValueMember = "CLV_USUARIO"
        Me.ComboBox1.Visible = False
        '
        'MUESTRAUSUARIOS1BindingSource
        '
        Me.MUESTRAUSUARIOS1BindingSource.DataMember = "MUESTRAUSUARIOS1"
        Me.MUESTRAUSUARIOS1BindingSource.DataSource = Me.NewsoftvDataSet
        '
        'NewsoftvDataSet
        '
        Me.NewsoftvDataSet.DataSetName = "NewsoftvDataSet"
        Me.NewsoftvDataSet.EnforceConstraints = False
        Me.NewsoftvDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'MUESTRAUSUARIOSBindingSource
        '
        Me.MUESTRAUSUARIOSBindingSource.DataMember = "MUESTRAUSUARIOS"
        Me.MUESTRAUSUARIOSBindingSource.DataSource = Me.NewsoftvDataSet
        '
        'CMBLabel3
        '
        Me.CMBLabel3.AutoSize = True
        Me.CMBLabel3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBLabel3.ForeColor = System.Drawing.Color.FromArgb(CType(CType(128, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.CMBLabel3.Location = New System.Drawing.Point(238, 47)
        Me.CMBLabel3.Name = "CMBLabel3"
        Me.CMBLabel3.Size = New System.Drawing.Size(77, 16)
        Me.CMBLabel3.TabIndex = 11
        Me.CMBLabel3.Text = "Cajero(a):"
        Me.CMBLabel3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.CMBLabel3.Visible = False
        '
        'NewsoftvDataSetBindingSource
        '
        Me.NewsoftvDataSetBindingSource.DataSource = Me.NewsoftvDataSet
        Me.NewsoftvDataSetBindingSource.Position = 0
        '
        'MUESTRAUSUARIOSTableAdapter
        '
        Me.MUESTRAUSUARIOSTableAdapter.ClearBeforeFill = True
        '
        'ComboBox2
        '
        Me.ComboBox2.DataSource = Me.MUESTRACAJASBindingSource
        Me.ComboBox2.DisplayMember = "NOMBRE"
        Me.ComboBox2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ComboBox2.ForeColor = System.Drawing.Color.Black
        Me.ComboBox2.FormattingEnabled = True
        Me.ComboBox2.Location = New System.Drawing.Point(241, 66)
        Me.ComboBox2.Name = "ComboBox2"
        Me.ComboBox2.Size = New System.Drawing.Size(189, 23)
        Me.ComboBox2.TabIndex = 2
        Me.ComboBox2.ValueMember = "CLV_USUARIO"
        Me.ComboBox2.Visible = False
        '
        'MUESTRACAJASBindingSource
        '
        Me.MUESTRACAJASBindingSource.DataMember = "MUESTRACAJAS"
        Me.MUESTRACAJASBindingSource.DataSource = Me.NewsoftvDataSet
        '
        'ComboBox3
        '
        Me.ComboBox3.DisplayMember = "NOMBRE"
        Me.ComboBox3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ComboBox3.ForeColor = System.Drawing.Color.Black
        Me.ComboBox3.FormattingEnabled = True
        Me.ComboBox3.Location = New System.Drawing.Point(241, 66)
        Me.ComboBox3.Name = "ComboBox3"
        Me.ComboBox3.Size = New System.Drawing.Size(189, 23)
        Me.ComboBox3.TabIndex = 2
        Me.ComboBox3.ValueMember = "CLV_SUCURSAL"
        Me.ComboBox3.Visible = False
        '
        'MUESTRASUCURSALESBindingSource
        '
        Me.MUESTRASUCURSALESBindingSource.DataMember = "MUESTRASUCURSALES"
        Me.MUESTRASUCURSALESBindingSource.DataSource = Me.NewsoftvDataSet
        '
        'MUESTRASUCURSALESTableAdapter
        '
        Me.MUESTRASUCURSALESTableAdapter.ClearBeforeFill = True
        '
        'Button1
        '
        Me.Button1.BackColor = System.Drawing.Color.DarkOrange
        Me.Button1.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button1.Location = New System.Drawing.Point(319, 185)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(136, 36)
        Me.Button1.TabIndex = 4
        Me.Button1.Text = "&Aceptar"
        Me.Button1.UseVisualStyleBackColor = False
        '
        'Button2
        '
        Me.Button2.BackColor = System.Drawing.Color.DarkOrange
        Me.Button2.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button2.Location = New System.Drawing.Point(471, 185)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(136, 36)
        Me.Button2.TabIndex = 5
        Me.Button2.Text = "&Cancelar"
        Me.Button2.UseVisualStyleBackColor = False
        '
        'CheckBox1
        '
        Me.CheckBox1.AutoSize = True
        Me.CheckBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CheckBox1.ForeColor = System.Drawing.Color.FromArgb(CType(CType(128, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.CheckBox1.Location = New System.Drawing.Point(241, 111)
        Me.CheckBox1.Name = "CheckBox1"
        Me.CheckBox1.Size = New System.Drawing.Size(92, 20)
        Me.CheckBox1.TabIndex = 3
        Me.CheckBox1.Text = "Resumen"
        Me.CheckBox1.UseVisualStyleBackColor = True
        Me.CheckBox1.Visible = False
        '
        'MUESTRAUSUARIOS1TableAdapter
        '
        Me.MUESTRAUSUARIOS1TableAdapter.ClearBeforeFill = True
        '
        'MUESTRACAJASTableAdapter
        '
        Me.MUESTRACAJASTableAdapter.ClearBeforeFill = True
        '
        'Procedimientos_arnoldo
        '
        Me.Procedimientos_arnoldo.DataSetName = "Procedimientos_arnoldo"
        Me.Procedimientos_arnoldo.EnforceConstraints = False
        Me.Procedimientos_arnoldo.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'MUESTRAVENDORES_TodosTableAdapter
        '
        Me.MUESTRAVENDORES_TodosTableAdapter.ClearBeforeFill = True
        '
        'MUESTRAVENDORESTodosBindingSource1
        '
        Me.MUESTRAVENDORESTodosBindingSource1.DataMember = "MUESTRAVENDORES_Todos"
        Me.MUESTRAVENDORESTodosBindingSource1.DataSource = Me.Procedimientos_arnoldo
        '
        'ComboVendedores
        '
        Me.ComboVendedores.DataSource = Me.MUESTRAVENDORESTodosBindingSource1
        Me.ComboVendedores.DisplayMember = "Nombre"
        Me.ComboVendedores.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ComboVendedores.ForeColor = System.Drawing.Color.Black
        Me.ComboVendedores.FormattingEnabled = True
        Me.ComboVendedores.Location = New System.Drawing.Point(241, 66)
        Me.ComboVendedores.Name = "ComboVendedores"
        Me.ComboVendedores.Size = New System.Drawing.Size(189, 23)
        Me.ComboVendedores.TabIndex = 2
        Me.ComboVendedores.ValueMember = "Clv_Vendedor"
        Me.ComboVendedores.Visible = False
        '
        'Button3
        '
        Me.Button3.BackColor = System.Drawing.Color.DarkOrange
        Me.Button3.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button3.Location = New System.Drawing.Point(471, 108)
        Me.Button3.Name = "Button3"
        Me.Button3.Size = New System.Drawing.Size(136, 51)
        Me.Button3.TabIndex = 12
        Me.Button3.Text = "&Reporte Detallado"
        Me.Button3.UseVisualStyleBackColor = False
        '
        'FrmOpRep
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.WhiteSmoke
        Me.ClientSize = New System.Drawing.Size(619, 244)
        Me.Controls.Add(Me.Button3)
        Me.Controls.Add(Me.ComboVendedores)
        Me.Controls.Add(Me.CheckBox1)
        Me.Controls.Add(Me.Button2)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.ComboBox3)
        Me.Controls.Add(Me.ComboBox2)
        Me.Controls.Add(Me.CMBLabel3)
        Me.Controls.Add(Me.ComboBox1)
        Me.Controls.Add(Me.DateTimePicker2)
        Me.Controls.Add(Me.DateTimePicker1)
        Me.Controls.Add(Me.CMBLabel2)
        Me.Controls.Add(Me.CMBLabel1)
        Me.Name = "FrmOpRep"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Opciones del Reporte"
        CType(Me.MUESTRAUSUARIOS1BindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.NewsoftvDataSet, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.MUESTRAUSUARIOSBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.NewsoftvDataSetBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.MUESTRACAJASBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.MUESTRASUCURSALESBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Procedimientos_arnoldo, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.MUESTRAVENDORESTodosBindingSource1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents CMBLabel1 As System.Windows.Forms.Label
    Friend WithEvents CMBLabel2 As System.Windows.Forms.Label
    Friend WithEvents DateTimePicker1 As System.Windows.Forms.DateTimePicker
    Friend WithEvents DateTimePicker2 As System.Windows.Forms.DateTimePicker
    Friend WithEvents ComboBox1 As System.Windows.Forms.ComboBox
    Friend WithEvents CMBLabel3 As System.Windows.Forms.Label
    Friend WithEvents NewsoftvDataSetBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents NewsoftvDataSet As softvFacturacion.NewsoftvDataSet
    Friend WithEvents MUESTRAUSUARIOSBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents MUESTRAUSUARIOSTableAdapter As softvFacturacion.NewsoftvDataSetTableAdapters.MUESTRAUSUARIOSTableAdapter
    Friend WithEvents ComboBox2 As System.Windows.Forms.ComboBox
    Friend WithEvents ComboBox3 As System.Windows.Forms.ComboBox
    Friend WithEvents MUESTRASUCURSALESBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents MUESTRASUCURSALESTableAdapter As softvFacturacion.NewsoftvDataSetTableAdapters.MUESTRASUCURSALESTableAdapter
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents Button2 As System.Windows.Forms.Button
    Friend WithEvents CheckBox1 As System.Windows.Forms.CheckBox
    Friend WithEvents MUESTRAUSUARIOS1BindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents MUESTRAUSUARIOS1TableAdapter As softvFacturacion.NewsoftvDataSetTableAdapters.MUESTRAUSUARIOS1TableAdapter
    Friend WithEvents MUESTRACAJASBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents MUESTRACAJASTableAdapter As softvFacturacion.NewsoftvDataSetTableAdapters.MUESTRACAJASTableAdapter
    Friend WithEvents Procedimientos_arnoldo As softvFacturacion.Procedimientos_arnoldo
    Friend WithEvents MUESTRAVENDORES_TodosTableAdapter As softvFacturacion.Procedimientos_arnoldoTableAdapters.MUESTRAVENDORES_TodosTableAdapter
    Friend WithEvents MUESTRAVENDORESTodosBindingSource1 As System.Windows.Forms.BindingSource
    Friend WithEvents ComboVendedores As System.Windows.Forms.ComboBox
    Friend WithEvents Button3 As System.Windows.Forms.Button
End Class
