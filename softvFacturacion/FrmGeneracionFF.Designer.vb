﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmGeneracionFF
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.tbTelefonoR = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.tbCodigoPostalR = New System.Windows.Forms.TextBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.tbPaisR = New System.Windows.Forms.TextBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.tbEstadoR = New System.Windows.Forms.TextBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.tbMunicipioR = New System.Windows.Forms.TextBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.tbReferenciaR = New System.Windows.Forms.TextBox()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.tbrfcR = New System.Windows.Forms.TextBox()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.tbNombreR = New System.Windows.Forms.TextBox()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.tbCalleR = New System.Windows.Forms.TextBox()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.tbNoExtR = New System.Windows.Forms.TextBox()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.tbNoIntR = New System.Windows.Forms.TextBox()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.tbColoniaR = New System.Windows.Forms.TextBox()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.tbLocalidadR = New System.Windows.Forms.TextBox()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.tbEmailR = New System.Windows.Forms.TextBox()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.gbReceptor = New System.Windows.Forms.GroupBox()
        Me.gbComprobante = New System.Windows.Forms.GroupBox()
        Me.Label18 = New System.Windows.Forms.Label()
        Me.Label15 = New System.Windows.Forms.Label()
        Me.Label16 = New System.Windows.Forms.Label()
        Me.Label17 = New System.Windows.Forms.Label()
        Me.tbTotal = New System.Windows.Forms.TextBox()
        Me.cbSerie = New System.Windows.Forms.ComboBox()
        Me.tbIVA = New System.Windows.Forms.TextBox()
        Me.tbSubTotal = New System.Windows.Forms.TextBox()
        Me.tbDescripcion = New System.Windows.Forms.TextBox()
        Me.tbImporte = New System.Windows.Forms.TextBox()
        Me.bnAgregar = New System.Windows.Forms.Button()
        Me.bnEliminar = New System.Windows.Forms.Button()
        Me.dgConcepto = New System.Windows.Forms.DataGridView()
        Me.Descripcion = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Importe = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Label19 = New System.Windows.Forms.Label()
        Me.Label21 = New System.Windows.Forms.Label()
        Me.gbConcepto = New System.Windows.Forms.GroupBox()
        Me.bnGenerar = New System.Windows.Forms.Button()
        Me.bnSalir = New System.Windows.Forms.Button()
        Me.gbReceptor.SuspendLayout()
        Me.gbComprobante.SuspendLayout()
        CType(Me.dgConcepto, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gbConcepto.SuspendLayout()
        Me.SuspendLayout()
        '
        'tbTelefonoR
        '
        Me.tbTelefonoR.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.tbTelefonoR.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbTelefonoR.Location = New System.Drawing.Point(727, 191)
        Me.tbTelefonoR.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.tbTelefonoR.MaxLength = 50
        Me.tbTelefonoR.Name = "tbTelefonoR"
        Me.tbTelefonoR.Size = New System.Drawing.Size(277, 24)
        Me.tbTelefonoR.TabIndex = 12
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(59, 65)
        Me.Label1.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(78, 18)
        Me.Label1.TabIndex = 120
        Me.Label1.Text = "Nombre :"
        '
        'tbCodigoPostalR
        '
        Me.tbCodigoPostalR.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.tbCodigoPostalR.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbCodigoPostalR.Location = New System.Drawing.Point(727, 158)
        Me.tbCodigoPostalR.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.tbCodigoPostalR.MaxLength = 50
        Me.tbCodigoPostalR.Name = "tbCodigoPostalR"
        Me.tbCodigoPostalR.Size = New System.Drawing.Size(277, 24)
        Me.tbCodigoPostalR.TabIndex = 11
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(83, 98)
        Me.Label2.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(56, 18)
        Me.Label2.TabIndex = 119
        Me.Label2.Text = "Calle :"
        '
        'tbPaisR
        '
        Me.tbPaisR.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.tbPaisR.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbPaisR.Location = New System.Drawing.Point(727, 124)
        Me.tbPaisR.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.tbPaisR.MaxLength = 150
        Me.tbPaisR.Name = "tbPaisR"
        Me.tbPaisR.Size = New System.Drawing.Size(409, 24)
        Me.tbPaisR.TabIndex = 10
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(25, 132)
        Me.Label3.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(109, 18)
        Me.Label3.TabIndex = 118
        Me.Label3.Text = "No. Exterior :"
        '
        'tbEstadoR
        '
        Me.tbEstadoR.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.tbEstadoR.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbEstadoR.Location = New System.Drawing.Point(727, 91)
        Me.tbEstadoR.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.tbEstadoR.MaxLength = 150
        Me.tbEstadoR.Name = "tbEstadoR"
        Me.tbEstadoR.Size = New System.Drawing.Size(409, 24)
        Me.tbEstadoR.TabIndex = 9
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.Location = New System.Drawing.Point(32, 165)
        Me.Label4.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(103, 18)
        Me.Label4.TabIndex = 117
        Me.Label4.Text = "No. Interior :"
        '
        'tbMunicipioR
        '
        Me.tbMunicipioR.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.tbMunicipioR.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbMunicipioR.Location = New System.Drawing.Point(727, 58)
        Me.tbMunicipioR.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.tbMunicipioR.MaxLength = 150
        Me.tbMunicipioR.Name = "tbMunicipioR"
        Me.tbMunicipioR.Size = New System.Drawing.Size(409, 24)
        Me.tbMunicipioR.TabIndex = 8
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.Location = New System.Drawing.Point(63, 198)
        Me.Label5.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(76, 18)
        Me.Label5.TabIndex = 116
        Me.Label5.Text = "Colonia :"
        '
        'tbReferenciaR
        '
        Me.tbReferenciaR.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.tbReferenciaR.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbReferenciaR.Location = New System.Drawing.Point(727, 25)
        Me.tbReferenciaR.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.tbReferenciaR.MaxLength = 150
        Me.tbReferenciaR.Name = "tbReferenciaR"
        Me.tbReferenciaR.Size = New System.Drawing.Size(409, 24)
        Me.tbReferenciaR.TabIndex = 7
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.Location = New System.Drawing.Point(43, 231)
        Me.Label6.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(90, 18)
        Me.Label6.TabIndex = 115
        Me.Label6.Text = "Localidad :"
        '
        'tbrfcR
        '
        Me.tbrfcR.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.tbrfcR.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbrfcR.Location = New System.Drawing.Point(156, 25)
        Me.tbrfcR.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.tbrfcR.MaxLength = 50
        Me.tbrfcR.Name = "tbrfcR"
        Me.tbrfcR.Size = New System.Drawing.Size(409, 24)
        Me.tbrfcR.TabIndex = 0
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7.Location = New System.Drawing.Point(605, 32)
        Me.Label7.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(99, 18)
        Me.Label7.TabIndex = 114
        Me.Label7.Text = "Referencia :"
        '
        'tbNombreR
        '
        Me.tbNombreR.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.tbNombreR.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbNombreR.Location = New System.Drawing.Point(156, 58)
        Me.tbNombreR.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.tbNombreR.MaxLength = 150
        Me.tbNombreR.Name = "tbNombreR"
        Me.tbNombreR.Size = New System.Drawing.Size(409, 24)
        Me.tbNombreR.TabIndex = 1
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label8.Location = New System.Drawing.Point(613, 65)
        Me.Label8.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(90, 18)
        Me.Label8.TabIndex = 113
        Me.Label8.Text = "Municipio :"
        '
        'tbCalleR
        '
        Me.tbCalleR.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.tbCalleR.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbCalleR.Location = New System.Drawing.Point(156, 91)
        Me.tbCalleR.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.tbCalleR.MaxLength = 250
        Me.tbCalleR.Name = "tbCalleR"
        Me.tbCalleR.Size = New System.Drawing.Size(409, 24)
        Me.tbCalleR.TabIndex = 2
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label9.Location = New System.Drawing.Point(640, 98)
        Me.Label9.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(71, 18)
        Me.Label9.TabIndex = 112
        Me.Label9.Text = "Estado :"
        '
        'tbNoExtR
        '
        Me.tbNoExtR.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.tbNoExtR.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbNoExtR.Location = New System.Drawing.Point(156, 124)
        Me.tbNoExtR.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.tbNoExtR.MaxLength = 50
        Me.tbNoExtR.Name = "tbNoExtR"
        Me.tbNoExtR.Size = New System.Drawing.Size(277, 24)
        Me.tbNoExtR.TabIndex = 3
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label10.Location = New System.Drawing.Point(581, 165)
        Me.Label10.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(125, 18)
        Me.Label10.TabIndex = 111
        Me.Label10.Text = "Código Postal :"
        '
        'tbNoIntR
        '
        Me.tbNoIntR.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.tbNoIntR.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbNoIntR.Location = New System.Drawing.Point(156, 158)
        Me.tbNoIntR.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.tbNoIntR.MaxLength = 50
        Me.tbNoIntR.Name = "tbNoIntR"
        Me.tbNoIntR.Size = New System.Drawing.Size(277, 24)
        Me.tbNoIntR.TabIndex = 4
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label11.Location = New System.Drawing.Point(663, 132)
        Me.Label11.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(51, 18)
        Me.Label11.TabIndex = 110
        Me.Label11.Text = "País :"
        '
        'tbColoniaR
        '
        Me.tbColoniaR.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.tbColoniaR.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbColoniaR.Location = New System.Drawing.Point(156, 191)
        Me.tbColoniaR.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.tbColoniaR.MaxLength = 150
        Me.tbColoniaR.Name = "tbColoniaR"
        Me.tbColoniaR.Size = New System.Drawing.Size(409, 24)
        Me.tbColoniaR.TabIndex = 5
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label12.Location = New System.Drawing.Point(623, 198)
        Me.Label12.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(84, 18)
        Me.Label12.TabIndex = 109
        Me.Label12.Text = "Teléfono :"
        '
        'tbLocalidadR
        '
        Me.tbLocalidadR.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.tbLocalidadR.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbLocalidadR.Location = New System.Drawing.Point(156, 224)
        Me.tbLocalidadR.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.tbLocalidadR.MaxLength = 150
        Me.tbLocalidadR.Name = "tbLocalidadR"
        Me.tbLocalidadR.Size = New System.Drawing.Size(409, 24)
        Me.tbLocalidadR.TabIndex = 6
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label13.Location = New System.Drawing.Point(648, 231)
        Me.Label13.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(60, 18)
        Me.Label13.TabIndex = 108
        Me.Label13.Text = "Email :"
        '
        'tbEmailR
        '
        Me.tbEmailR.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.tbEmailR.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbEmailR.Location = New System.Drawing.Point(727, 224)
        Me.tbEmailR.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.tbEmailR.MaxLength = 50
        Me.tbEmailR.Name = "tbEmailR"
        Me.tbEmailR.Size = New System.Drawing.Size(277, 24)
        Me.tbEmailR.TabIndex = 13
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label14.Location = New System.Drawing.Point(91, 32)
        Me.Label14.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(52, 18)
        Me.Label14.TabIndex = 94
        Me.Label14.Text = "RFC :"
        '
        'gbReceptor
        '
        Me.gbReceptor.Controls.Add(Me.Label3)
        Me.gbReceptor.Controls.Add(Me.tbTelefonoR)
        Me.gbReceptor.Controls.Add(Me.Label14)
        Me.gbReceptor.Controls.Add(Me.Label1)
        Me.gbReceptor.Controls.Add(Me.tbEmailR)
        Me.gbReceptor.Controls.Add(Me.tbCodigoPostalR)
        Me.gbReceptor.Controls.Add(Me.Label13)
        Me.gbReceptor.Controls.Add(Me.Label2)
        Me.gbReceptor.Controls.Add(Me.tbLocalidadR)
        Me.gbReceptor.Controls.Add(Me.tbPaisR)
        Me.gbReceptor.Controls.Add(Me.Label12)
        Me.gbReceptor.Controls.Add(Me.tbColoniaR)
        Me.gbReceptor.Controls.Add(Me.tbEstadoR)
        Me.gbReceptor.Controls.Add(Me.Label11)
        Me.gbReceptor.Controls.Add(Me.Label4)
        Me.gbReceptor.Controls.Add(Me.tbNoIntR)
        Me.gbReceptor.Controls.Add(Me.tbMunicipioR)
        Me.gbReceptor.Controls.Add(Me.Label10)
        Me.gbReceptor.Controls.Add(Me.Label5)
        Me.gbReceptor.Controls.Add(Me.tbNoExtR)
        Me.gbReceptor.Controls.Add(Me.tbReferenciaR)
        Me.gbReceptor.Controls.Add(Me.Label9)
        Me.gbReceptor.Controls.Add(Me.Label6)
        Me.gbReceptor.Controls.Add(Me.tbCalleR)
        Me.gbReceptor.Controls.Add(Me.tbrfcR)
        Me.gbReceptor.Controls.Add(Me.Label8)
        Me.gbReceptor.Controls.Add(Me.Label7)
        Me.gbReceptor.Controls.Add(Me.tbNombreR)
        Me.gbReceptor.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbReceptor.Location = New System.Drawing.Point(80, 215)
        Me.gbReceptor.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.gbReceptor.Name = "gbReceptor"
        Me.gbReceptor.Padding = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.gbReceptor.Size = New System.Drawing.Size(1171, 268)
        Me.gbReceptor.TabIndex = 1
        Me.gbReceptor.TabStop = False
        Me.gbReceptor.Text = "Receptor"
        '
        'gbComprobante
        '
        Me.gbComprobante.Controls.Add(Me.Label18)
        Me.gbComprobante.Controls.Add(Me.Label15)
        Me.gbComprobante.Controls.Add(Me.Label16)
        Me.gbComprobante.Controls.Add(Me.Label17)
        Me.gbComprobante.Controls.Add(Me.tbTotal)
        Me.gbComprobante.Controls.Add(Me.cbSerie)
        Me.gbComprobante.Controls.Add(Me.tbIVA)
        Me.gbComprobante.Controls.Add(Me.tbSubTotal)
        Me.gbComprobante.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbComprobante.Location = New System.Drawing.Point(80, 15)
        Me.gbComprobante.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.gbComprobante.Name = "gbComprobante"
        Me.gbComprobante.Padding = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.gbComprobante.Size = New System.Drawing.Size(323, 193)
        Me.gbComprobante.TabIndex = 0
        Me.gbComprobante.TabStop = False
        Me.gbComprobante.Text = "Comprobante"
        '
        'Label18
        '
        Me.Label18.AutoSize = True
        Me.Label18.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label18.Location = New System.Drawing.Point(51, 53)
        Me.Label18.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label18.Name = "Label18"
        Me.Label18.Size = New System.Drawing.Size(57, 18)
        Me.Label18.TabIndex = 131
        Me.Label18.Text = "Serie :"
        '
        'Label15
        '
        Me.Label15.AutoSize = True
        Me.Label15.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label15.Location = New System.Drawing.Point(15, 86)
        Me.Label15.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(90, 18)
        Me.Label15.TabIndex = 128
        Me.Label15.Text = "Sub Total :"
        '
        'Label16
        '
        Me.Label16.AutoSize = True
        Me.Label16.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label16.Location = New System.Drawing.Point(53, 119)
        Me.Label16.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(57, 18)
        Me.Label16.TabIndex = 130
        Me.Label16.Text = "I.V.A. :"
        '
        'Label17
        '
        Me.Label17.AutoSize = True
        Me.Label17.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label17.Location = New System.Drawing.Point(53, 153)
        Me.Label17.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label17.Name = "Label17"
        Me.Label17.Size = New System.Drawing.Size(56, 18)
        Me.Label17.TabIndex = 129
        Me.Label17.Text = "Total :"
        '
        'tbTotal
        '
        Me.tbTotal.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbTotal.Location = New System.Drawing.Point(124, 145)
        Me.tbTotal.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.tbTotal.MaxLength = 50
        Me.tbTotal.Name = "tbTotal"
        Me.tbTotal.Size = New System.Drawing.Size(169, 24)
        Me.tbTotal.TabIndex = 3
        '
        'cbSerie
        '
        Me.cbSerie.DisplayMember = "Serie"
        Me.cbSerie.FormattingEnabled = True
        Me.cbSerie.Location = New System.Drawing.Point(124, 43)
        Me.cbSerie.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.cbSerie.Name = "cbSerie"
        Me.cbSerie.Size = New System.Drawing.Size(169, 26)
        Me.cbSerie.TabIndex = 0
        Me.cbSerie.ValueMember = "Id"
        '
        'tbIVA
        '
        Me.tbIVA.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbIVA.Location = New System.Drawing.Point(124, 112)
        Me.tbIVA.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.tbIVA.MaxLength = 50
        Me.tbIVA.Name = "tbIVA"
        Me.tbIVA.Size = New System.Drawing.Size(169, 24)
        Me.tbIVA.TabIndex = 2
        '
        'tbSubTotal
        '
        Me.tbSubTotal.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbSubTotal.Location = New System.Drawing.Point(124, 79)
        Me.tbSubTotal.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.tbSubTotal.MaxLength = 50
        Me.tbSubTotal.Name = "tbSubTotal"
        Me.tbSubTotal.Size = New System.Drawing.Size(169, 24)
        Me.tbSubTotal.TabIndex = 1
        '
        'tbDescripcion
        '
        Me.tbDescripcion.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.tbDescripcion.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbDescripcion.Location = New System.Drawing.Point(304, 53)
        Me.tbDescripcion.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.tbDescripcion.MaxLength = 150
        Me.tbDescripcion.Name = "tbDescripcion"
        Me.tbDescripcion.Size = New System.Drawing.Size(632, 24)
        Me.tbDescripcion.TabIndex = 0
        '
        'tbImporte
        '
        Me.tbImporte.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.tbImporte.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbImporte.Location = New System.Drawing.Point(304, 86)
        Me.tbImporte.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.tbImporte.MaxLength = 50
        Me.tbImporte.Name = "tbImporte"
        Me.tbImporte.Size = New System.Drawing.Size(169, 24)
        Me.tbImporte.TabIndex = 1
        '
        'bnAgregar
        '
        Me.bnAgregar.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.bnAgregar.Location = New System.Drawing.Point(969, 84)
        Me.bnAgregar.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.bnAgregar.Name = "bnAgregar"
        Me.bnAgregar.Size = New System.Drawing.Size(100, 28)
        Me.bnAgregar.TabIndex = 2
        Me.bnAgregar.Text = "&Agregar"
        Me.bnAgregar.UseVisualStyleBackColor = True
        '
        'bnEliminar
        '
        Me.bnEliminar.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.bnEliminar.Location = New System.Drawing.Point(969, 135)
        Me.bnEliminar.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.bnEliminar.Name = "bnEliminar"
        Me.bnEliminar.Size = New System.Drawing.Size(100, 28)
        Me.bnEliminar.TabIndex = 3
        Me.bnEliminar.Text = "&Eliminar"
        Me.bnEliminar.UseVisualStyleBackColor = True
        '
        'dgConcepto
        '
        Me.dgConcepto.AllowUserToAddRows = False
        Me.dgConcepto.AllowUserToDeleteRows = False
        Me.dgConcepto.BackgroundColor = System.Drawing.Color.WhiteSmoke
        Me.dgConcepto.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgConcepto.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.Descripcion, Me.Importe})
        Me.dgConcepto.Location = New System.Drawing.Point(145, 135)
        Me.dgConcepto.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.dgConcepto.Name = "dgConcepto"
        Me.dgConcepto.ReadOnly = True
        Me.dgConcepto.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgConcepto.Size = New System.Drawing.Size(816, 206)
        Me.dgConcepto.TabIndex = 127
        Me.dgConcepto.TabStop = False
        '
        'Descripcion
        '
        Me.Descripcion.DataPropertyName = "Descripcion"
        Me.Descripcion.HeaderText = "Descripción"
        Me.Descripcion.Name = "Descripcion"
        Me.Descripcion.ReadOnly = True
        Me.Descripcion.Width = 400
        '
        'Importe
        '
        Me.Importe.DataPropertyName = "Importe"
        DataGridViewCellStyle1.Format = "C2"
        DataGridViewCellStyle1.NullValue = Nothing
        Me.Importe.DefaultCellStyle = DataGridViewCellStyle1
        Me.Importe.HeaderText = "Importe"
        Me.Importe.Name = "Importe"
        Me.Importe.ReadOnly = True
        Me.Importe.Width = 150
        '
        'Label19
        '
        Me.Label19.AutoSize = True
        Me.Label19.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label19.Location = New System.Drawing.Point(175, 60)
        Me.Label19.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label19.Name = "Label19"
        Me.Label19.Size = New System.Drawing.Size(108, 18)
        Me.Label19.TabIndex = 121
        Me.Label19.Text = "Descripción :"
        '
        'Label21
        '
        Me.Label21.AutoSize = True
        Me.Label21.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label21.Location = New System.Drawing.Point(211, 94)
        Me.Label21.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label21.Name = "Label21"
        Me.Label21.Size = New System.Drawing.Size(75, 18)
        Me.Label21.TabIndex = 129
        Me.Label21.Text = "Importe :"
        '
        'gbConcepto
        '
        Me.gbConcepto.Controls.Add(Me.dgConcepto)
        Me.gbConcepto.Controls.Add(Me.Label21)
        Me.gbConcepto.Controls.Add(Me.tbDescripcion)
        Me.gbConcepto.Controls.Add(Me.Label19)
        Me.gbConcepto.Controls.Add(Me.tbImporte)
        Me.gbConcepto.Controls.Add(Me.bnAgregar)
        Me.gbConcepto.Controls.Add(Me.bnEliminar)
        Me.gbConcepto.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbConcepto.Location = New System.Drawing.Point(80, 495)
        Me.gbConcepto.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.gbConcepto.Name = "gbConcepto"
        Me.gbConcepto.Padding = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.gbConcepto.Size = New System.Drawing.Size(1171, 364)
        Me.gbConcepto.TabIndex = 2
        Me.gbConcepto.TabStop = False
        Me.gbConcepto.Text = "Concepto"
        '
        'bnGenerar
        '
        Me.bnGenerar.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.bnGenerar.Location = New System.Drawing.Point(1147, 15)
        Me.bnGenerar.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.bnGenerar.Name = "bnGenerar"
        Me.bnGenerar.Size = New System.Drawing.Size(181, 44)
        Me.bnGenerar.TabIndex = 3
        Me.bnGenerar.Text = "&GENERAR"
        Me.bnGenerar.UseVisualStyleBackColor = True
        '
        'bnSalir
        '
        Me.bnSalir.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.bnSalir.Location = New System.Drawing.Point(1147, 66)
        Me.bnSalir.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.bnSalir.Name = "bnSalir"
        Me.bnSalir.Size = New System.Drawing.Size(181, 44)
        Me.bnSalir.TabIndex = 4
        Me.bnSalir.Text = "&SALIR"
        Me.bnSalir.UseVisualStyleBackColor = True
        '
        'FrmGeneracionFF
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.AutoScroll = True
        Me.BackColor = System.Drawing.Color.WhiteSmoke
        Me.ClientSize = New System.Drawing.Size(1344, 898)
        Me.Controls.Add(Me.bnSalir)
        Me.Controls.Add(Me.bnGenerar)
        Me.Controls.Add(Me.gbConcepto)
        Me.Controls.Add(Me.gbComprobante)
        Me.Controls.Add(Me.gbReceptor)
        Me.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Name = "FrmGeneracionFF"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Generación de Facturas Fiscales (FacturaNet)"
        Me.gbReceptor.ResumeLayout(False)
        Me.gbReceptor.PerformLayout()
        Me.gbComprobante.ResumeLayout(False)
        Me.gbComprobante.PerformLayout()
        CType(Me.dgConcepto, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gbConcepto.ResumeLayout(False)
        Me.gbConcepto.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents tbTelefonoR As System.Windows.Forms.TextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents tbCodigoPostalR As System.Windows.Forms.TextBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents tbPaisR As System.Windows.Forms.TextBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents tbEstadoR As System.Windows.Forms.TextBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents tbMunicipioR As System.Windows.Forms.TextBox
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents tbReferenciaR As System.Windows.Forms.TextBox
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents tbrfcR As System.Windows.Forms.TextBox
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents tbNombreR As System.Windows.Forms.TextBox
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents tbCalleR As System.Windows.Forms.TextBox
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents tbNoExtR As System.Windows.Forms.TextBox
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents tbNoIntR As System.Windows.Forms.TextBox
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents tbColoniaR As System.Windows.Forms.TextBox
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents tbLocalidadR As System.Windows.Forms.TextBox
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents tbEmailR As System.Windows.Forms.TextBox
    Friend WithEvents Label14 As System.Windows.Forms.Label
    Friend WithEvents gbReceptor As System.Windows.Forms.GroupBox
    Friend WithEvents gbComprobante As System.Windows.Forms.GroupBox
    Friend WithEvents Label18 As System.Windows.Forms.Label
    Friend WithEvents Label15 As System.Windows.Forms.Label
    Friend WithEvents Label16 As System.Windows.Forms.Label
    Friend WithEvents Label17 As System.Windows.Forms.Label
    Friend WithEvents tbTotal As System.Windows.Forms.TextBox
    Friend WithEvents cbSerie As System.Windows.Forms.ComboBox
    Friend WithEvents tbIVA As System.Windows.Forms.TextBox
    Friend WithEvents tbSubTotal As System.Windows.Forms.TextBox
    Friend WithEvents tbDescripcion As System.Windows.Forms.TextBox
    Friend WithEvents tbImporte As System.Windows.Forms.TextBox
    Friend WithEvents bnAgregar As System.Windows.Forms.Button
    Friend WithEvents bnEliminar As System.Windows.Forms.Button
    Friend WithEvents dgConcepto As System.Windows.Forms.DataGridView
    Friend WithEvents Label19 As System.Windows.Forms.Label
    Friend WithEvents Label21 As System.Windows.Forms.Label
    Friend WithEvents gbConcepto As System.Windows.Forms.GroupBox
    Friend WithEvents bnGenerar As System.Windows.Forms.Button
    Friend WithEvents bnSalir As System.Windows.Forms.Button
    Friend WithEvents Descripcion As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Importe As System.Windows.Forms.DataGridViewTextBoxColumn
End Class
