Imports System.Data.SqlClient
Public Class FrmTipoNota

    Private Sub FrmTipoNota_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        colorea(Me)
        Dim CONLIDIA As New SqlClient.SqlConnection(MiConexion)
        CONLIDIA.Open()
        'TODO: esta l�nea de c�digo carga datos en la tabla 'DataSetLydia.Muestra_Tipo_Nota' Puede moverla o quitarla seg�n sea necesario.
        Me.Muestra_Tipo_NotaTableAdapter.Connection = CONLIDIA
        Me.Muestra_Tipo_NotaTableAdapter.Fill(Me.DataSetLydia.Muestra_Tipo_Nota, LiTipo)
        CONLIDIA.Close()
    End Sub

    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button3.Click
        If IsNumeric(Me.ComboBox1.SelectedValue) = True Then
            
            If LiTipo = 4 Then
                glotipoFacGlo = Me.ComboBox1.SelectedValue
                CAPTURAFACTURAGLOBAL.Show()
                bnd = 3
            ElseIf LiTipo = 5 Then
                glotipoFacGlo = Me.ComboBox1.SelectedValue
                CAPTURAFACTURAGLOBALESPECIALES.Show()
                bnd = 3
            ElseIf LiTipo <> 4 And LiTipo <> 5 Then
                glotipoNota = Me.ComboBox1.SelectedValue
                FrmNotasdeCredito.Show()
                bnd = 3
            End If
        End If
        Me.Close()
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        If LiTipo <> 4 And LiTipo <> 5 Then
            FrmNotasdeCredito.Close()
        ElseIf LiTipo = 4 Then
            CAPTURAFACTURAGLOBAL.Close()
        ElseIf LiTipo = 5 Then
            CAPTURAFACTURAGLOBALESPECIALES.Close()
        End If
        Me.Close()
    End Sub
  
End Class