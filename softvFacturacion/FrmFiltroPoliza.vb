﻿Public Class FrmFiltroPoliza

    Private Sub FrmFiltroPoliza_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        colorea(Me)
        UspGuardaFormularios(Me.Name, Me.Text)
        UspGuardaBotonesFormularioSiste(Me, Me.Name)
        UspDesactivaBotones(Me, Me.Name)
        llenaDistribuidor()
        dtpInicio.Value = Today
        dtpFinal.Value = Today
    End Sub

    Private Sub llenaDistribuidor()
        Try
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@Clv_plaza", SqlDbType.Int, 0)
            BaseII.CreateMyParameter("@nombre", SqlDbType.VarChar, "")
            cbDistribuidor.DataSource = BaseII.ConsultaDT("Muestra_Plazas")
        Catch ex As Exception

        End Try
    End Sub

    Private Sub llenaPlaza()
        Try
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@clv_plaza", SqlDbType.Int, cbDistribuidor.SelectedValue)
            cbCompania.DataSource = BaseII.ConsultaDT("MuestraCompaniaPlaza")
        Catch ex As Exception

        End Try
    End Sub

    Private Sub GeneraPoliza()
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@FECHAINI", SqlDbType.DateTime, dtpInicio.Value)
        BaseII.CreateMyParameter("@FECHAFIN", SqlDbType.DateTime, dtpFinal.Value)
        BaseII.CreateMyParameter("@TIPO", SqlDbType.VarChar, "C", 1)
        BaseII.CreateMyParameter("@SUCURSAL", SqlDbType.Int, 0)
        BaseII.CreateMyParameter("@CAJA", SqlDbType.Int, 0)
        BaseII.CreateMyParameter("@CAJERA", SqlDbType.VarChar, GloUsuario, 11)
        BaseII.CreateMyParameter("@CLV_FACTURA", SqlDbType.Int, 0)
        BaseII.CreateMyParameter("@CLV_USUARIO", SqlDbType.VarChar, GloUsuario, 11)
        BaseII.CreateMyParameter("@CLV_LLAVE_POLIZANEW", ParameterDirection.Output, SqlDbType.Int)
        BaseII.CreateMyParameter("@NUMERROR", ParameterDirection.Output, SqlDbType.Int)
        BaseII.CreateMyParameter("@MSJERROR", ParameterDirection.Output, SqlDbType.VarChar, 250)
        BaseII.CreateMyParameter("@Clv_Plaza", SqlDbType.BigInt, cbDistribuidor.SelectedValue)
        BaseII.ProcedimientoOutPut("GeneraPolizaNuevo2")
        LocGloClv_poliza = CInt(BaseII.dicoPar("@CLV_LLAVE_POLIZANEW").ToString())

        LocopPoliza = "N"
        bitsist(GloUsuario, 0, GloSistema, Me.Name, "", "Se generó póliza", "Con clave de póliza: " + CStr(LocGloClv_poliza), LocClv_Ciudad)

        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@Clv_llave_Poliza", SqlDbType.Int, LocGloClv_poliza)
        BaseII.CreateMyParameter("@opc", SqlDbType.VarChar, LocopPoliza, 5)
        BaseII.CreateMyParameter("@clave", ParameterDirection.Output, SqlDbType.Int)
        BaseII.ProcedimientoOutPut("Dame_Clave_poliza")
        GloPoliza2 = CInt(BaseII.dicoPar("@clave").ToString())
        FrmPoliza.Show()
    End Sub

    Private Sub dtpInicio_ValueChanged(sender As Object, e As EventArgs) Handles dtpInicio.ValueChanged
        Me.dtpFinal.MinDate = Me.dtpInicio.Value
    End Sub

    Private Sub cbDistribuidor_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cbDistribuidor.SelectedIndexChanged
        'llenaPlaza()
    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        'If cbCompania.Items.Count = 0 Then
        '    Exit Sub
        'End If
        'GloIdCompania = cbCompania.SelectedValue
        GeneraPoliza()
        Me.Close()
    End Sub

    Private Sub Button5_Click(sender As Object, e As EventArgs) Handles Button5.Click
        Me.Close()
    End Sub

    Private Sub Label6_Click(sender As Object, e As EventArgs) Handles Label6.Click

    End Sub
End Class