Imports System.Data.SqlClient

Public Class BwrEntregasParciales
    Private flag As Boolean = False

    Private Sub Llena_companias()

        Try
            BaseII.limpiaParametros()
            'BaseII.CreateMyParameter("@Op", SqlDbType.Int, 3)
            BaseII.CreateMyParameter("@ClvUsuario", SqlDbType.Int, GloClvUsuario)
            ComboBoxCompanias.DataSource = BaseII.ConsultaDT("Muestra_Compania_RelUsuario")
            ComboBoxCompanias.DisplayMember = "razon_social"
            ComboBoxCompanias.ValueMember = "id_compania"



            'GloIdCompania = 0
            'ComboBoxCiudades.Text = ""
        Catch ex As Exception

        End Try
    End Sub
    Private Sub busqueda(ByVal op As Integer)
        Try
            Dim CON As New SqlConnection(MiConexion)
            CON.Open()
            If GloTipoUsuario = 40 Or GloTipoUsuario = 1 Or GloTipoUsuario = 2 Or GloTipoUsuario = 51 Or GloTipoUsuario = 52 Then
                Select Case op
                    Case 0
                        Me.BUSCAPARCIALESTableAdapter.Connection = CON
                        Me.BUSCAPARCIALESTableAdapter.Fill(Me.NewsoftvDataSet.BUSCAPARCIALES, 0, "", "", "", "", "01/01/1900", GloClvUsuario, ComboBoxCompanias.SelectedValue)
                    Case 1
                        Me.BUSCAPARCIALESTableAdapter.Connection = CON
                        Me.BUSCAPARCIALESTableAdapter.Fill(Me.NewsoftvDataSet.BUSCAPARCIALES, 1, Me.TextBox1.Text, "", "", "", "01/01/1900", GloClvUsuario, ComboBoxCompanias.SelectedValue)
                    Case 2
                        Me.BUSCAPARCIALESTableAdapter.Connection = CON
                        Me.BUSCAPARCIALESTableAdapter.Fill(Me.NewsoftvDataSet.BUSCAPARCIALES, 2, "", Me.TextBox2.Text, "", "", "01/01/1900", GloClvUsuario, ComboBoxCompanias.SelectedValue)
                    Case 3
                        Me.BUSCAPARCIALESTableAdapter.Connection = CON
                        Me.BUSCAPARCIALESTableAdapter.Fill(Me.NewsoftvDataSet.BUSCAPARCIALES, 3, "", "", Me.TextBox3.Text, "", "01/01/1900", GloClvUsuario, ComboBoxCompanias.SelectedValue)
                    Case 4
                        Me.BUSCAPARCIALESTableAdapter.Connection = CON
                        Me.BUSCAPARCIALESTableAdapter.Fill(Me.NewsoftvDataSet.BUSCAPARCIALES, 4, "", "", "", Me.TextBox4.Text, "01/01/1900", GloClvUsuario, ComboBoxCompanias.SelectedValue)
                    Case Else
                        Me.BUSCAPARCIALESTableAdapter.Connection = CON
                        Me.BUSCAPARCIALESTableAdapter.Fill(Me.NewsoftvDataSet.BUSCAPARCIALES, 5, "", "", "", "", Me.TextBox5.Text, GloClvUsuario, ComboBoxCompanias.SelectedValue)
                End Select
            ElseIf GloTipoUsuario = 21 Then
                Select Case op
                    Case 0
                        Me.BUSCAPARCIALESTableAdapter.Connection = CON
                        Me.BUSCAPARCIALESTableAdapter.Fill(Me.NewsoftvDataSet.BUSCAPARCIALES, 0, "", "", "", GloUsuario, "01/01/1900", GloClvUsuario, ComboBoxCompanias.SelectedValue)
                    Case 1
                        Me.BUSCAPARCIALESTableAdapter.Connection = CON
                        Me.BUSCAPARCIALESTableAdapter.Fill(Me.NewsoftvDataSet.BUSCAPARCIALES, 1, Me.TextBox1.Text, "", "", GloUsuario, "01/01/1900", GloClvUsuario, ComboBoxCompanias.SelectedValue)
                    Case 2
                        Me.BUSCAPARCIALESTableAdapter.Connection = CON
                        Me.BUSCAPARCIALESTableAdapter.Fill(Me.NewsoftvDataSet.BUSCAPARCIALES, 2, "", Me.TextBox2.Text, "", GloUsuario, "01/01/1900", GloClvUsuario, ComboBoxCompanias.SelectedValue)
                    Case 3
                        Me.BUSCAPARCIALESTableAdapter.Connection = CON
                        Me.BUSCAPARCIALESTableAdapter.Fill(Me.NewsoftvDataSet.BUSCAPARCIALES, 3, "", "", Me.TextBox3.Text, GloUsuario, "01/01/1900", GloClvUsuario, ComboBoxCompanias.SelectedValue)
                    Case 4
                        Me.BUSCAPARCIALESTableAdapter.Connection = CON
                        Me.BUSCAPARCIALESTableAdapter.Fill(Me.NewsoftvDataSet.BUSCAPARCIALES, 4, "", "", "", GloUsuario, "01/01/1900", GloClvUsuario, ComboBoxCompanias.SelectedValue)
                    Case Else
                        Me.BUSCAPARCIALESTableAdapter.Connection = CON
                        Me.BUSCAPARCIALESTableAdapter.Fill(Me.NewsoftvDataSet.BUSCAPARCIALES, 5, "", "", "", GloUsuario, Me.TextBox5.Text, GloClvUsuario, ComboBoxCompanias.SelectedValue)
                End Select
            End If
            CON.Close()
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try

    End Sub

    Private Sub BwrEntregasParciales_Activated(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Activated
        If GloBnd = True Then
            GloBnd = False
            busqueda(0)
        End If
    End Sub

    Private Sub BwrEntregasParciales_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        colorea(Me)
        Llena_companias()
        flag = True
        busqueda(0)
        UspGuardaFormularios(Me.Name, Me.Text)
        UspGuardaBotonesFormularioSiste(Me, Me.Name)
        UspDesactivaBotones(Me, Me.Name)
    End Sub

    Private Sub SplitContainer1_Panel1_Paint(ByVal sender As System.Object, ByVal e As System.Windows.Forms.PaintEventArgs) Handles SplitContainer1.Panel1.Paint

    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        busqueda(1)
    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        busqueda(2)
    End Sub

    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button3.Click
        busqueda(3)
    End Sub

    Private Sub Button4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button4.Click
        busqueda(4)
    End Sub

    Private Sub Button5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button5.Click
        busqueda(5)
    End Sub

    Private Sub TextBox1_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles TextBox1.KeyPress
        If Asc(e.KeyChar) = 13 Then
            busqueda(1)
        End If
    End Sub

    Private Sub TextBox1_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TextBox1.TextChanged

    End Sub

    Private Sub TextBox2_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles TextBox2.KeyPress
        If Asc(e.KeyChar) = 13 Then
            busqueda(2)
        End If
    End Sub

    Private Sub TextBox3_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles TextBox3.KeyPress
        If Asc(e.KeyChar) = 13 Then
            busqueda(3)
        End If
    End Sub

    Private Sub TextBox4_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles TextBox4.KeyPress
        If Asc(e.KeyChar) = 13 Then
            busqueda(4)
        End If
    End Sub

    Private Sub TextBox5_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles TextBox5.KeyPress
        If Asc(e.KeyChar) = 13 Then
            busqueda(5)
        End If
    End Sub

    Private Sub Button10_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button10.Click
        Me.Close()


    End Sub

    Private Sub Button6_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button6.Click
        op = "N"
        My.Forms.FrmEntregasParciales.Show()

    End Sub

    Private Sub Button7_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button7.Click


        If Me.DataGridView1.RowCount > 0 Then
            op = "C"
            GloCajera = Me.DataGridView1.SelectedCells(2).Value
            My.Forms.FrmEntregasParciales.Show()
        Else
            MsgBox("No hay Cajeras(os) que Consultar", , "Atenci�n")
        End If

    End Sub

    Private Sub Button8_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button8.Click

        If Me.DataGridView1.RowCount > 0 Then
            op = "M"
            GloCajera = Me.DataGridView1.SelectedCells(2).Value
            My.Forms.FrmEntregasParciales.Show()
        Else
            MsgBox("No hay Cajeras(os) que Modificar", , "Atenci�n")
        End If
    End Sub

    Private Sub Button9_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button9.Click
        If Me.DataGridView1.RowCount > 0 Then
            GloConsecutivo = Me.DataGridView1.SelectedCells(0).Value
            GloReporte = 2
            locnomsupervisor = Me.CMBNomRecibioTextBox.Text
            My.Forms.FrmImprimirRepGral.Show()
        Else
            MsgBox("Sin Datos, No Puedes Imprimir", , "Atenci�n")
        End If
    End Sub

    Private Sub ConsecutivoTextBox_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CMBConsecutivoTextBox.TextChanged
        If IsNumeric(Me.CMBConsecutivoTextBox.Text) = True Then
            Consecutivo = Me.CMBConsecutivoTextBox.Text
        End If
    End Sub

    Private Sub NomCajeraLabel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)

    End Sub

    Private Sub DataGridView1_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles DataGridView1.CellContentClick

    End Sub


    Private Sub ComboBoxCompanias_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ComboBoxCompanias.TextChanged
        If flag Then
            busqueda(0)
        End If

        'GloIdCompania = ComboBoxCompanias.SelectedValue
    End Sub
End Class