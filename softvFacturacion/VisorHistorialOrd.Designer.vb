<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class VisorHistorialOrd
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim NombreLabel As System.Windows.Forms.Label
        Dim NUMEROLabel As System.Windows.Forms.Label
        Dim CALLELabel As System.Windows.Forms.Label
        Dim ContratoLabel As System.Windows.Forms.Label
        Dim Clv_calleLabel1 As System.Windows.Forms.Label
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Clv_calleLabel2 = New System.Windows.Forms.Label()
        Me.BUSCAORDSERBindingSource1 = New System.Windows.Forms.BindingSource(Me.components)
        Me.LydiaDataSet2 = New softvFacturacion.LydiaDataSet2()
        Me.DataGridView1 = New System.Windows.Forms.DataGridView()
        Me.clv_orden = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.status = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.contrato = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.nombre = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.calle = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.numero = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Clv_TipSer = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Button6 = New System.Windows.Forms.Button()
        Me.CMBNombreTextBox = New System.Windows.Forms.TextBox()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.RadioButton3 = New System.Windows.Forms.RadioButton()
        Me.RadioButton2 = New System.Windows.Forms.RadioButton()
        Me.RadioButton1 = New System.Windows.Forms.RadioButton()
        Me.SplitContainer1 = New System.Windows.Forms.SplitContainer()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.NUMEROLabel1 = New System.Windows.Forms.Label()
        Me.CALLELabel1 = New System.Windows.Forms.Label()
        Me.ContratoLabel1 = New System.Windows.Forms.Label()
        Me.CMBLabel1 = New System.Windows.Forms.Label()
        Me.Button5 = New System.Windows.Forms.Button()
        Me.Button3 = New System.Windows.Forms.Button()
        Me.BUSCAORDSERTableAdapter = New softvFacturacion.LydiaDataSet2TableAdapters.BUSCAORDSERTableAdapter()
        NombreLabel = New System.Windows.Forms.Label()
        NUMEROLabel = New System.Windows.Forms.Label()
        CALLELabel = New System.Windows.Forms.Label()
        ContratoLabel = New System.Windows.Forms.Label()
        Clv_calleLabel1 = New System.Windows.Forms.Label()
        CType(Me.BUSCAORDSERBindingSource1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LydiaDataSet2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox1.SuspendLayout()
        Me.SplitContainer1.Panel1.SuspendLayout()
        Me.SplitContainer1.Panel2.SuspendLayout()
        Me.SplitContainer1.SuspendLayout()
        Me.Panel1.SuspendLayout()
        Me.SuspendLayout()
        '
        'NombreLabel
        '
        NombreLabel.AutoSize = True
        NombreLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        NombreLabel.ForeColor = System.Drawing.Color.White
        NombreLabel.Location = New System.Drawing.Point(9, 135)
        NombreLabel.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        NombreLabel.Name = "NombreLabel"
        NombreLabel.Size = New System.Drawing.Size(78, 18)
        NombreLabel.TabIndex = 3
        NombreLabel.Text = "Nombre :"
        '
        'NUMEROLabel
        '
        NUMEROLabel.AutoSize = True
        NUMEROLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        NUMEROLabel.ForeColor = System.Drawing.Color.White
        NUMEROLabel.Location = New System.Drawing.Point(4, 290)
        NUMEROLabel.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        NUMEROLabel.Name = "NUMEROLabel"
        NUMEROLabel.Size = New System.Drawing.Size(83, 18)
        NUMEROLabel.TabIndex = 9
        NUMEROLabel.Text = "Numero  :"
        '
        'CALLELabel
        '
        CALLELabel.AutoSize = True
        CALLELabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        CALLELabel.ForeColor = System.Drawing.Color.White
        CALLELabel.Location = New System.Drawing.Point(32, 202)
        CALLELabel.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        CALLELabel.Name = "CALLELabel"
        CALLELabel.Size = New System.Drawing.Size(56, 18)
        CALLELabel.TabIndex = 7
        CALLELabel.Text = "Calle :"
        '
        'ContratoLabel
        '
        ContratoLabel.AutoSize = True
        ContratoLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        ContratoLabel.ForeColor = System.Drawing.Color.White
        ContratoLabel.Location = New System.Drawing.Point(4, 85)
        ContratoLabel.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        ContratoLabel.Name = "ContratoLabel"
        ContratoLabel.Size = New System.Drawing.Size(84, 18)
        ContratoLabel.TabIndex = 6
        ContratoLabel.Text = "Contrato :"
        '
        'Clv_calleLabel1
        '
        Clv_calleLabel1.AutoSize = True
        Clv_calleLabel1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Clv_calleLabel1.ForeColor = System.Drawing.Color.White
        Clv_calleLabel1.Location = New System.Drawing.Point(9, 43)
        Clv_calleLabel1.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Clv_calleLabel1.Name = "Clv_calleLabel1"
        Clv_calleLabel1.Size = New System.Drawing.Size(78, 18)
        Clv_calleLabel1.TabIndex = 1
        Clv_calleLabel1.Text = "# Orden :"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.ForeColor = System.Drawing.Color.White
        Me.Label4.Location = New System.Drawing.Point(4, 0)
        Me.Label4.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(290, 24)
        Me.Label4.TabIndex = 5
        Me.Label4.Text = "Datos de la Orden de Servicio"
        '
        'Clv_calleLabel2
        '
        Me.Clv_calleLabel2.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.BUSCAORDSERBindingSource1, "Clv_Orden", True))
        Me.Clv_calleLabel2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Clv_calleLabel2.ForeColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.Clv_calleLabel2.Location = New System.Drawing.Point(117, 43)
        Me.Clv_calleLabel2.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Clv_calleLabel2.Name = "Clv_calleLabel2"
        Me.Clv_calleLabel2.Size = New System.Drawing.Size(133, 28)
        Me.Clv_calleLabel2.TabIndex = 2
        '
        'BUSCAORDSERBindingSource1
        '
        Me.BUSCAORDSERBindingSource1.DataMember = "BUSCAORDSER"
        Me.BUSCAORDSERBindingSource1.DataSource = Me.LydiaDataSet2
        '
        'LydiaDataSet2
        '
        Me.LydiaDataSet2.DataSetName = "LydiaDataSet2"
        Me.LydiaDataSet2.EnforceConstraints = False
        Me.LydiaDataSet2.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'DataGridView1
        '
        Me.DataGridView1.AllowUserToAddRows = False
        Me.DataGridView1.AllowUserToDeleteRows = False
        Me.DataGridView1.AllowUserToOrderColumns = True
        Me.DataGridView1.BackgroundColor = System.Drawing.Color.WhiteSmoke
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle1.BackColor = System.Drawing.Color.Chocolate
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle1.ForeColor = System.Drawing.Color.White
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.DataGridView1.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle1
        Me.DataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DataGridView1.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.clv_orden, Me.status, Me.contrato, Me.nombre, Me.calle, Me.numero, Me.Clv_TipSer})
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.DataGridView1.DefaultCellStyle = DataGridViewCellStyle2
        Me.DataGridView1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.DataGridView1.Location = New System.Drawing.Point(0, 0)
        Me.DataGridView1.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.DataGridView1.MultiSelect = False
        Me.DataGridView1.Name = "DataGridView1"
        Me.DataGridView1.ReadOnly = True
        Me.DataGridView1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.DataGridView1.Size = New System.Drawing.Size(740, 897)
        Me.DataGridView1.TabIndex = 1
        Me.DataGridView1.TabStop = False
        '
        'clv_orden
        '
        Me.clv_orden.DataPropertyName = "Clv_Orden"
        Me.clv_orden.HeaderText = "# Orden"
        Me.clv_orden.Name = "clv_orden"
        Me.clv_orden.ReadOnly = True
        '
        'status
        '
        Me.status.DataPropertyName = "STATUS"
        Me.status.HeaderText = "STATUS"
        Me.status.Name = "status"
        Me.status.ReadOnly = True
        '
        'contrato
        '
        Me.contrato.DataPropertyName = "Contrato"
        Me.contrato.HeaderText = "Contrato"
        Me.contrato.Name = "contrato"
        Me.contrato.ReadOnly = True
        '
        'nombre
        '
        Me.nombre.DataPropertyName = "Nombre"
        Me.nombre.HeaderText = "Nombre"
        Me.nombre.Name = "nombre"
        Me.nombre.ReadOnly = True
        '
        'calle
        '
        Me.calle.DataPropertyName = "CALLE"
        Me.calle.HeaderText = "Calle"
        Me.calle.Name = "calle"
        Me.calle.ReadOnly = True
        '
        'numero
        '
        Me.numero.DataPropertyName = "NUMERO"
        Me.numero.HeaderText = "Número"
        Me.numero.Name = "numero"
        Me.numero.ReadOnly = True
        '
        'Clv_TipSer
        '
        Me.Clv_TipSer.DataPropertyName = "Clv_TipSer"
        Me.Clv_TipSer.HeaderText = "Clv_TipSer"
        Me.Clv_TipSer.Name = "Clv_TipSer"
        Me.Clv_TipSer.ReadOnly = True
        '
        'Button6
        '
        Me.Button6.BackColor = System.Drawing.Color.DarkOrange
        Me.Button6.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button6.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button6.ForeColor = System.Drawing.Color.Black
        Me.Button6.Location = New System.Drawing.Point(1157, 751)
        Me.Button6.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Button6.Name = "Button6"
        Me.Button6.Size = New System.Drawing.Size(181, 44)
        Me.Button6.TabIndex = 32
        Me.Button6.Text = "&IMPRIMIR"
        Me.Button6.UseVisualStyleBackColor = False
        Me.Button6.Visible = False
        '
        'CMBNombreTextBox
        '
        Me.CMBNombreTextBox.BackColor = System.Drawing.Color.DarkOrange
        Me.CMBNombreTextBox.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.CMBNombreTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.BUSCAORDSERBindingSource1, "Nombre", True))
        Me.CMBNombreTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBNombreTextBox.ForeColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.CMBNombreTextBox.Location = New System.Drawing.Point(121, 135)
        Me.CMBNombreTextBox.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.CMBNombreTextBox.Multiline = True
        Me.CMBNombreTextBox.Name = "CMBNombreTextBox"
        Me.CMBNombreTextBox.ReadOnly = True
        Me.CMBNombreTextBox.Size = New System.Drawing.Size(228, 57)
        Me.CMBNombreTextBox.TabIndex = 6
        Me.CMBNombreTextBox.TabStop = False
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.RadioButton3)
        Me.GroupBox1.Controls.Add(Me.RadioButton2)
        Me.GroupBox1.Controls.Add(Me.RadioButton1)
        Me.GroupBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox1.ForeColor = System.Drawing.Color.Black
        Me.GroupBox1.Location = New System.Drawing.Point(15, 118)
        Me.GroupBox1.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Padding = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.GroupBox1.Size = New System.Drawing.Size(343, 212)
        Me.GroupBox1.TabIndex = 10
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Ver Por Status"
        '
        'RadioButton3
        '
        Me.RadioButton3.AutoSize = True
        Me.RadioButton3.ForeColor = System.Drawing.Color.Black
        Me.RadioButton3.Location = New System.Drawing.Point(29, 154)
        Me.RadioButton3.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.RadioButton3.Name = "RadioButton3"
        Me.RadioButton3.Size = New System.Drawing.Size(125, 28)
        Me.RadioButton3.TabIndex = 12
        Me.RadioButton3.TabStop = True
        Me.RadioButton3.Text = "Con Visita"
        Me.RadioButton3.UseVisualStyleBackColor = True
        '
        'RadioButton2
        '
        Me.RadioButton2.AutoSize = True
        Me.RadioButton2.ForeColor = System.Drawing.Color.Black
        Me.RadioButton2.Location = New System.Drawing.Point(29, 94)
        Me.RadioButton2.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.RadioButton2.Name = "RadioButton2"
        Me.RadioButton2.Size = New System.Drawing.Size(134, 28)
        Me.RadioButton2.TabIndex = 11
        Me.RadioButton2.TabStop = True
        Me.RadioButton2.Text = "Ejecutadas"
        Me.RadioButton2.UseVisualStyleBackColor = True
        '
        'RadioButton1
        '
        Me.RadioButton1.AutoSize = True
        Me.RadioButton1.ForeColor = System.Drawing.Color.Black
        Me.RadioButton1.Location = New System.Drawing.Point(29, 38)
        Me.RadioButton1.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.RadioButton1.Name = "RadioButton1"
        Me.RadioButton1.Size = New System.Drawing.Size(136, 28)
        Me.RadioButton1.TabIndex = 10
        Me.RadioButton1.TabStop = True
        Me.RadioButton1.Text = "Pendientes"
        Me.RadioButton1.UseVisualStyleBackColor = True
        '
        'SplitContainer1
        '
        Me.SplitContainer1.Location = New System.Drawing.Point(16, 7)
        Me.SplitContainer1.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.SplitContainer1.Name = "SplitContainer1"
        '
        'SplitContainer1.Panel1
        '
        Me.SplitContainer1.Panel1.AutoScroll = True
        Me.SplitContainer1.Panel1.BackColor = System.Drawing.Color.WhiteSmoke
        Me.SplitContainer1.Panel1.Controls.Add(Me.GroupBox1)
        Me.SplitContainer1.Panel1.Controls.Add(Me.Panel1)
        Me.SplitContainer1.Panel1.Controls.Add(Me.CMBLabel1)
        '
        'SplitContainer1.Panel2
        '
        Me.SplitContainer1.Panel2.Controls.Add(Me.DataGridView1)
        Me.SplitContainer1.Size = New System.Drawing.Size(1115, 897)
        Me.SplitContainer1.SplitterDistance = 370
        Me.SplitContainer1.SplitterWidth = 5
        Me.SplitContainer1.TabIndex = 34
        Me.SplitContainer1.TabStop = False
        '
        'Panel1
        '
        Me.Panel1.BackColor = System.Drawing.Color.DarkOrange
        Me.Panel1.Controls.Add(NUMEROLabel)
        Me.Panel1.Controls.Add(Me.NUMEROLabel1)
        Me.Panel1.Controls.Add(CALLELabel)
        Me.Panel1.Controls.Add(Me.CALLELabel1)
        Me.Panel1.Controls.Add(ContratoLabel)
        Me.Panel1.Controls.Add(Me.ContratoLabel1)
        Me.Panel1.Controls.Add(Me.CMBNombreTextBox)
        Me.Panel1.Controls.Add(Me.Label4)
        Me.Panel1.Controls.Add(NombreLabel)
        Me.Panel1.Controls.Add(Clv_calleLabel1)
        Me.Panel1.Controls.Add(Me.Clv_calleLabel2)
        Me.Panel1.Location = New System.Drawing.Point(8, 401)
        Me.Panel1.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(359, 346)
        Me.Panel1.TabIndex = 8
        '
        'NUMEROLabel1
        '
        Me.NUMEROLabel1.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.BUSCAORDSERBindingSource1, "NUMERO", True))
        Me.NUMEROLabel1.ForeColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.NUMEROLabel1.Location = New System.Drawing.Point(117, 293)
        Me.NUMEROLabel1.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.NUMEROLabel1.Name = "NUMEROLabel1"
        Me.NUMEROLabel1.Size = New System.Drawing.Size(133, 28)
        Me.NUMEROLabel1.TabIndex = 10
        '
        'CALLELabel1
        '
        Me.CALLELabel1.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.BUSCAORDSERBindingSource1, "CALLE", True))
        Me.CALLELabel1.ForeColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.CALLELabel1.Location = New System.Drawing.Point(121, 204)
        Me.CALLELabel1.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.CALLELabel1.Name = "CALLELabel1"
        Me.CALLELabel1.Size = New System.Drawing.Size(228, 28)
        Me.CALLELabel1.TabIndex = 8
        '
        'ContratoLabel1
        '
        Me.ContratoLabel1.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.BUSCAORDSERBindingSource1, "Contrato", True))
        Me.ContratoLabel1.ForeColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.ContratoLabel1.Location = New System.Drawing.Point(117, 87)
        Me.ContratoLabel1.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.ContratoLabel1.Name = "ContratoLabel1"
        Me.ContratoLabel1.Size = New System.Drawing.Size(133, 28)
        Me.ContratoLabel1.TabIndex = 7
        '
        'CMBLabel1
        '
        Me.CMBLabel1.AutoSize = True
        Me.CMBLabel1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBLabel1.ForeColor = System.Drawing.Color.FromArgb(CType(CType(128, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.CMBLabel1.Location = New System.Drawing.Point(12, 15)
        Me.CMBLabel1.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.CMBLabel1.Name = "CMBLabel1"
        Me.CMBLabel1.Size = New System.Drawing.Size(273, 20)
        Me.CMBLabel1.TabIndex = 1
        Me.CMBLabel1.Text = "Buscar Orden de Servicio Por :"
        '
        'Button5
        '
        Me.Button5.BackColor = System.Drawing.Color.DarkOrange
        Me.Button5.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button5.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button5.ForeColor = System.Drawing.Color.Black
        Me.Button5.Location = New System.Drawing.Point(1157, 817)
        Me.Button5.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Button5.Name = "Button5"
        Me.Button5.Size = New System.Drawing.Size(181, 44)
        Me.Button5.TabIndex = 33
        Me.Button5.Text = "&SALIR"
        Me.Button5.UseVisualStyleBackColor = False
        '
        'Button3
        '
        Me.Button3.BackColor = System.Drawing.Color.Orange
        Me.Button3.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button3.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button3.ForeColor = System.Drawing.Color.Black
        Me.Button3.Location = New System.Drawing.Point(1157, 7)
        Me.Button3.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Button3.Name = "Button3"
        Me.Button3.Size = New System.Drawing.Size(181, 44)
        Me.Button3.TabIndex = 30
        Me.Button3.Text = "&CONSULTA"
        Me.Button3.UseVisualStyleBackColor = False
        '
        'BUSCAORDSERTableAdapter
        '
        Me.BUSCAORDSERTableAdapter.ClearBeforeFill = True
        '
        'VisorHistorialOrd
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.AutoScroll = True
        Me.ClientSize = New System.Drawing.Size(1355, 912)
        Me.Controls.Add(Me.Button6)
        Me.Controls.Add(Me.Button3)
        Me.Controls.Add(Me.SplitContainer1)
        Me.Controls.Add(Me.Button5)
        Me.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.MaximizeBox = False
        Me.Name = "VisorHistorialOrd"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Visor de Historial de Ordenes de Servicio"
        CType(Me.BUSCAORDSERBindingSource1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LydiaDataSet2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.SplitContainer1.Panel1.ResumeLayout(False)
        Me.SplitContainer1.Panel1.PerformLayout()
        Me.SplitContainer1.Panel2.ResumeLayout(False)
        Me.SplitContainer1.ResumeLayout(False)
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Clv_calleLabel2 As System.Windows.Forms.Label
    Friend WithEvents DataGridView1 As System.Windows.Forms.DataGridView
    Friend WithEvents Button6 As System.Windows.Forms.Button
    Friend WithEvents CMBNombreTextBox As System.Windows.Forms.TextBox
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents RadioButton3 As System.Windows.Forms.RadioButton
    Friend WithEvents RadioButton2 As System.Windows.Forms.RadioButton
    Friend WithEvents RadioButton1 As System.Windows.Forms.RadioButton
    Friend WithEvents SplitContainer1 As System.Windows.Forms.SplitContainer
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents NUMEROLabel1 As System.Windows.Forms.Label
    Friend WithEvents CALLELabel1 As System.Windows.Forms.Label
    Friend WithEvents ContratoLabel1 As System.Windows.Forms.Label
    Friend WithEvents CMBLabel1 As System.Windows.Forms.Label
    Friend WithEvents Button5 As System.Windows.Forms.Button
    Friend WithEvents Button3 As System.Windows.Forms.Button
    Friend WithEvents LydiaDataSet2 As softvFacturacion.LydiaDataSet2
    Friend WithEvents BUSCAORDSERBindingSource1 As System.Windows.Forms.BindingSource
    Friend WithEvents BUSCAORDSERTableAdapter As softvFacturacion.LydiaDataSet2TableAdapters.BUSCAORDSERTableAdapter
    Friend WithEvents clv_orden As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents status As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents contrato As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents nombre As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents calle As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents numero As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Clv_TipSer As System.Windows.Forms.DataGridViewTextBoxColumn
End Class
