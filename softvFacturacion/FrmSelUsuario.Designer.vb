<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmSelUsuario
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle4 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.Button2 = New System.Windows.Forms.Button()
        Me.Button3 = New System.Windows.Forms.Button()
        Me.Button4 = New System.Windows.Forms.Button()
        Me.Button5 = New System.Windows.Forms.Button()
        Me.Button6 = New System.Windows.Forms.Button()
        Me.DataGridPro = New System.Windows.Forms.DataGridView()
        Me.IdSistema = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Clv_Id = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Clave = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Nombre = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridTmp = New System.Windows.Forms.DataGridView()
        Me.IdSistema2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Clv_Id2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Clave2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Nombre2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        CType(Me.DataGridPro, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DataGridTmp, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Button1
        '
        Me.Button1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button1.Location = New System.Drawing.Point(468, 174)
        Me.Button1.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(85, 31)
        Me.Button1.TabIndex = 0
        Me.Button1.Text = ">"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'Button2
        '
        Me.Button2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button2.Location = New System.Drawing.Point(468, 212)
        Me.Button2.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(85, 31)
        Me.Button2.TabIndex = 2
        Me.Button2.Text = ">>"
        Me.Button2.UseVisualStyleBackColor = True
        '
        'Button3
        '
        Me.Button3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button3.Location = New System.Drawing.Point(468, 250)
        Me.Button3.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Button3.Name = "Button3"
        Me.Button3.Size = New System.Drawing.Size(85, 31)
        Me.Button3.TabIndex = 3
        Me.Button3.Text = "<"
        Me.Button3.UseVisualStyleBackColor = True
        '
        'Button4
        '
        Me.Button4.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button4.Location = New System.Drawing.Point(468, 288)
        Me.Button4.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Button4.Name = "Button4"
        Me.Button4.Size = New System.Drawing.Size(85, 31)
        Me.Button4.TabIndex = 4
        Me.Button4.Text = "<<"
        Me.Button4.UseVisualStyleBackColor = True
        '
        'Button5
        '
        Me.Button5.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button5.Location = New System.Drawing.Point(659, 489)
        Me.Button5.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Button5.Name = "Button5"
        Me.Button5.Size = New System.Drawing.Size(181, 44)
        Me.Button5.TabIndex = 6
        Me.Button5.Text = "&ACEPTAR"
        Me.Button5.UseVisualStyleBackColor = True
        '
        'Button6
        '
        Me.Button6.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button6.Location = New System.Drawing.Point(848, 489)
        Me.Button6.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Button6.Name = "Button6"
        Me.Button6.Size = New System.Drawing.Size(181, 44)
        Me.Button6.TabIndex = 7
        Me.Button6.Text = "&SALIR"
        Me.Button6.UseVisualStyleBackColor = True
        '
        'DataGridPro
        '
        Me.DataGridPro.AllowUserToAddRows = False
        Me.DataGridPro.AllowUserToDeleteRows = False
        Me.DataGridPro.BackgroundColor = System.Drawing.Color.WhiteSmoke
        Me.DataGridPro.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.DataGridPro.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle1
        Me.DataGridPro.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DataGridPro.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.IdSistema, Me.Clv_Id, Me.Clave, Me.Nombre})
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.DataGridPro.DefaultCellStyle = DataGridViewCellStyle2
        Me.DataGridPro.Location = New System.Drawing.Point(45, 52)
        Me.DataGridPro.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.DataGridPro.Name = "DataGridPro"
        Me.DataGridPro.ReadOnly = True
        Me.DataGridPro.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.DataGridPro.Size = New System.Drawing.Size(368, 393)
        Me.DataGridPro.TabIndex = 8
        '
        'IdSistema
        '
        Me.IdSistema.DataPropertyName = "IdSistema"
        Me.IdSistema.HeaderText = "IdSistema"
        Me.IdSistema.Name = "IdSistema"
        Me.IdSistema.ReadOnly = True
        Me.IdSistema.Visible = False
        '
        'Clv_Id
        '
        Me.Clv_Id.DataPropertyName = "Clv_Id"
        Me.Clv_Id.HeaderText = "Clv_Id"
        Me.Clv_Id.Name = "Clv_Id"
        Me.Clv_Id.ReadOnly = True
        Me.Clv_Id.Visible = False
        '
        'Clave
        '
        Me.Clave.DataPropertyName = "Clave"
        Me.Clave.HeaderText = "Clave"
        Me.Clave.Name = "Clave"
        Me.Clave.ReadOnly = True
        Me.Clave.Visible = False
        '
        'Nombre
        '
        Me.Nombre.DataPropertyName = "Nombre"
        Me.Nombre.HeaderText = "Nombre"
        Me.Nombre.Name = "Nombre"
        Me.Nombre.ReadOnly = True
        Me.Nombre.Width = 200
        '
        'DataGridTmp
        '
        Me.DataGridTmp.AllowUserToAddRows = False
        Me.DataGridTmp.AllowUserToDeleteRows = False
        Me.DataGridTmp.BackgroundColor = System.Drawing.Color.WhiteSmoke
        Me.DataGridTmp.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None
        DataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.DataGridTmp.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle3
        Me.DataGridTmp.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DataGridTmp.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.IdSistema2, Me.Clv_Id2, Me.Clave2, Me.Nombre2})
        DataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle4.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.DataGridTmp.DefaultCellStyle = DataGridViewCellStyle4
        Me.DataGridTmp.Location = New System.Drawing.Point(595, 52)
        Me.DataGridTmp.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.DataGridTmp.Name = "DataGridTmp"
        Me.DataGridTmp.ReadOnly = True
        Me.DataGridTmp.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.DataGridTmp.Size = New System.Drawing.Size(368, 393)
        Me.DataGridTmp.TabIndex = 9
        '
        'IdSistema2
        '
        Me.IdSistema2.DataPropertyName = "IdSistema"
        Me.IdSistema2.HeaderText = "IdSistema"
        Me.IdSistema2.Name = "IdSistema2"
        Me.IdSistema2.ReadOnly = True
        Me.IdSistema2.Visible = False
        '
        'Clv_Id2
        '
        Me.Clv_Id2.DataPropertyName = "Clv_Id"
        Me.Clv_Id2.HeaderText = "Clv_Id"
        Me.Clv_Id2.Name = "Clv_Id2"
        Me.Clv_Id2.ReadOnly = True
        Me.Clv_Id2.Visible = False
        '
        'Clave2
        '
        Me.Clave2.DataPropertyName = "Clave"
        Me.Clave2.HeaderText = "Clave"
        Me.Clave2.Name = "Clave2"
        Me.Clave2.ReadOnly = True
        Me.Clave2.Visible = False
        '
        'Nombre2
        '
        Me.Nombre2.DataPropertyName = "Nombre"
        Me.Nombre2.HeaderText = "Nombre"
        Me.Nombre2.Name = "Nombre2"
        Me.Nombre2.ReadOnly = True
        Me.Nombre2.Width = 200
        '
        'FrmSelUsuario
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.AutoScroll = True
        Me.BackColor = System.Drawing.Color.WhiteSmoke
        Me.ClientSize = New System.Drawing.Size(1045, 548)
        Me.Controls.Add(Me.DataGridTmp)
        Me.Controls.Add(Me.DataGridPro)
        Me.Controls.Add(Me.Button6)
        Me.Controls.Add(Me.Button5)
        Me.Controls.Add(Me.Button4)
        Me.Controls.Add(Me.Button3)
        Me.Controls.Add(Me.Button2)
        Me.Controls.Add(Me.Button1)
        Me.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Name = "FrmSelUsuario"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Selecciona Usuarios"
        CType(Me.DataGridPro, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DataGridTmp, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents Button2 As System.Windows.Forms.Button
    Friend WithEvents Button3 As System.Windows.Forms.Button
    Friend WithEvents Button4 As System.Windows.Forms.Button
    Friend WithEvents Button5 As System.Windows.Forms.Button
    Friend WithEvents Button6 As System.Windows.Forms.Button
    Friend WithEvents DataGridPro As System.Windows.Forms.DataGridView
    Friend WithEvents DataGridTmp As System.Windows.Forms.DataGridView
    Friend WithEvents IdSistema As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Clv_Id As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Clave As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Nombre As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents IdSistema2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Clv_Id2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Clave2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Nombre2 As System.Windows.Forms.DataGridViewTextBoxColumn
End Class
