﻿Public Class FrmInfoDetalle

    Private Sub FrmInfoDetalle_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        colorea(Me)
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@clv_session", SqlDbType.Int, gloClv_Session)
        BaseII.CreateMyParameter("@clv_detalle", SqlDbType.Int, gloClv_Detalle)
        BaseII.CreateMyParameter("@msg", ParameterDirection.Output, SqlDbType.VarChar, 1800)
        BaseII.ProcedimientoOutPut("sp_dameInfodelCobro")
        tbInfo.Text = BaseII.dicoPar("@msg")
        Me.Focus()
    End Sub
End Class