﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmEntregasGlobales
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.lblM010 = New System.Windows.Forms.Label()
        Me.lblM020 = New System.Windows.Forms.Label()
        Me.lblB1000 = New System.Windows.Forms.Label()
        Me.lblM050 = New System.Windows.Forms.Label()
        Me.lblB500 = New System.Windows.Forms.Label()
        Me.lblM1 = New System.Windows.Forms.Label()
        Me.lblB200 = New System.Windows.Forms.Label()
        Me.lblM2 = New System.Windows.Forms.Label()
        Me.lblB100 = New System.Windows.Forms.Label()
        Me.lblM5 = New System.Windows.Forms.Label()
        Me.lblB50 = New System.Windows.Forms.Label()
        Me.lblM10 = New System.Windows.Forms.Label()
        Me.lblB20 = New System.Windows.Forms.Label()
        Me.lblM20 = New System.Windows.Forms.Label()
        Me.lblM100 = New System.Windows.Forms.Label()
        Me.lblM50 = New System.Windows.Forms.Label()
        Me.gbxIngresaGastos = New System.Windows.Forms.GroupBox()
        Me.gbxEntregaGlobal = New System.Windows.Forms.GroupBox()
        Me.txtImporte = New System.Windows.Forms.TextBox()
        Me.gbxMonedas = New System.Windows.Forms.GroupBox()
        Me.txtM100 = New System.Windows.Forms.TextBox()
        Me.txtM50 = New System.Windows.Forms.TextBox()
        Me.txtM010 = New System.Windows.Forms.TextBox()
        Me.txtM20 = New System.Windows.Forms.TextBox()
        Me.txtM020 = New System.Windows.Forms.TextBox()
        Me.txtM10 = New System.Windows.Forms.TextBox()
        Me.txtM050 = New System.Windows.Forms.TextBox()
        Me.txtM5 = New System.Windows.Forms.TextBox()
        Me.txtM1 = New System.Windows.Forms.TextBox()
        Me.txtM2 = New System.Windows.Forms.TextBox()
        Me.gbxBilletes = New System.Windows.Forms.GroupBox()
        Me.txtB1000 = New System.Windows.Forms.TextBox()
        Me.txtB20 = New System.Windows.Forms.TextBox()
        Me.txtB50 = New System.Windows.Forms.TextBox()
        Me.txtB100 = New System.Windows.Forms.TextBox()
        Me.txtB200 = New System.Windows.Forms.TextBox()
        Me.txtB500 = New System.Windows.Forms.TextBox()
        Me.txtEfectivoCobrado = New System.Windows.Forms.TextBox()
        Me.lblCobradoEfectivo = New System.Windows.Forms.Label()
        Me.txtDescripcion = New System.Windows.Forms.TextBox()
        Me.txtUsuario = New System.Windows.Forms.TextBox()
        Me.dtpFecha = New System.Windows.Forms.DateTimePicker()
        Me.txtClaveEntrega = New System.Windows.Forms.TextBox()
        Me.CMBlblUsuario = New System.Windows.Forms.Label()
        Me.CMBlblClaveEntrega = New System.Windows.Forms.Label()
        Me.CMBlblFecha = New System.Windows.Forms.Label()
        Me.CMBlblDescripcion = New System.Windows.Forms.Label()
        Me.bnEntregasGlobalesCancelar = New System.Windows.Forms.ToolStripButton()
        Me.bnEntregasGlobalesGuargar = New System.Windows.Forms.ToolStripButton()
        Me.bnEntregasGlobales = New System.Windows.Forms.BindingNavigator(Me.components)
        Me.gbxIngresaGastos.SuspendLayout()
        Me.gbxEntregaGlobal.SuspendLayout()
        Me.gbxMonedas.SuspendLayout()
        Me.gbxBilletes.SuspendLayout()
        CType(Me.bnEntregasGlobales, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.bnEntregasGlobales.SuspendLayout()
        Me.SuspendLayout()
        '
        'lblM010
        '
        Me.lblM010.AutoSize = True
        Me.lblM010.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblM010.ForeColor = System.Drawing.Color.LightSlateGray
        Me.lblM010.Location = New System.Drawing.Point(24, 324)
        Me.lblM010.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.lblM010.Name = "lblM010"
        Me.lblM010.Size = New System.Drawing.Size(54, 18)
        Me.lblM010.TabIndex = 92
        Me.lblM010.Text = "M010:"
        '
        'lblM020
        '
        Me.lblM020.AutoSize = True
        Me.lblM020.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblM020.ForeColor = System.Drawing.Color.LightSlateGray
        Me.lblM020.Location = New System.Drawing.Point(24, 292)
        Me.lblM020.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.lblM020.Name = "lblM020"
        Me.lblM020.Size = New System.Drawing.Size(54, 18)
        Me.lblM020.TabIndex = 91
        Me.lblM020.Text = "M020:"
        '
        'lblB1000
        '
        Me.lblB1000.AutoSize = True
        Me.lblB1000.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblB1000.ForeColor = System.Drawing.Color.LightSlateGray
        Me.lblB1000.Location = New System.Drawing.Point(25, 46)
        Me.lblB1000.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.lblB1000.Name = "lblB1000"
        Me.lblB1000.Size = New System.Drawing.Size(60, 18)
        Me.lblB1000.TabIndex = 72
        Me.lblB1000.Text = "B1000:"
        '
        'lblM050
        '
        Me.lblM050.AutoSize = True
        Me.lblM050.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblM050.ForeColor = System.Drawing.Color.LightSlateGray
        Me.lblM050.Location = New System.Drawing.Point(24, 260)
        Me.lblM050.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.lblM050.Name = "lblM050"
        Me.lblM050.Size = New System.Drawing.Size(54, 18)
        Me.lblM050.TabIndex = 90
        Me.lblM050.Text = "M050:"
        '
        'lblB500
        '
        Me.lblB500.AutoSize = True
        Me.lblB500.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblB500.ForeColor = System.Drawing.Color.LightSlateGray
        Me.lblB500.Location = New System.Drawing.Point(25, 78)
        Me.lblB500.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.lblB500.Name = "lblB500"
        Me.lblB500.Size = New System.Drawing.Size(51, 18)
        Me.lblB500.TabIndex = 75
        Me.lblB500.Text = "B500:"
        '
        'lblM1
        '
        Me.lblM1.AutoSize = True
        Me.lblM1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblM1.ForeColor = System.Drawing.Color.LightSlateGray
        Me.lblM1.Location = New System.Drawing.Point(24, 228)
        Me.lblM1.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.lblM1.Name = "lblM1"
        Me.lblM1.Size = New System.Drawing.Size(36, 18)
        Me.lblM1.TabIndex = 89
        Me.lblM1.Text = "M1:"
        '
        'lblB200
        '
        Me.lblB200.AutoSize = True
        Me.lblB200.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblB200.ForeColor = System.Drawing.Color.LightSlateGray
        Me.lblB200.Location = New System.Drawing.Point(25, 110)
        Me.lblB200.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.lblB200.Name = "lblB200"
        Me.lblB200.Size = New System.Drawing.Size(51, 18)
        Me.lblB200.TabIndex = 77
        Me.lblB200.Text = "B200:"
        '
        'lblM2
        '
        Me.lblM2.AutoSize = True
        Me.lblM2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblM2.ForeColor = System.Drawing.Color.LightSlateGray
        Me.lblM2.Location = New System.Drawing.Point(24, 196)
        Me.lblM2.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.lblM2.Name = "lblM2"
        Me.lblM2.Size = New System.Drawing.Size(36, 18)
        Me.lblM2.TabIndex = 88
        Me.lblM2.Text = "M2:"
        '
        'lblB100
        '
        Me.lblB100.AutoSize = True
        Me.lblB100.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblB100.ForeColor = System.Drawing.Color.LightSlateGray
        Me.lblB100.Location = New System.Drawing.Point(25, 142)
        Me.lblB100.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.lblB100.Name = "lblB100"
        Me.lblB100.Size = New System.Drawing.Size(51, 18)
        Me.lblB100.TabIndex = 80
        Me.lblB100.Text = "B100:"
        '
        'lblM5
        '
        Me.lblM5.AutoSize = True
        Me.lblM5.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblM5.ForeColor = System.Drawing.Color.LightSlateGray
        Me.lblM5.Location = New System.Drawing.Point(24, 164)
        Me.lblM5.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.lblM5.Name = "lblM5"
        Me.lblM5.Size = New System.Drawing.Size(36, 18)
        Me.lblM5.TabIndex = 87
        Me.lblM5.Text = "M5:"
        '
        'lblB50
        '
        Me.lblB50.AutoSize = True
        Me.lblB50.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblB50.ForeColor = System.Drawing.Color.LightSlateGray
        Me.lblB50.Location = New System.Drawing.Point(25, 174)
        Me.lblB50.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.lblB50.Name = "lblB50"
        Me.lblB50.Size = New System.Drawing.Size(42, 18)
        Me.lblB50.TabIndex = 81
        Me.lblB50.Text = "B50:"
        '
        'lblM10
        '
        Me.lblM10.AutoSize = True
        Me.lblM10.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblM10.ForeColor = System.Drawing.Color.LightSlateGray
        Me.lblM10.Location = New System.Drawing.Point(24, 132)
        Me.lblM10.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.lblM10.Name = "lblM10"
        Me.lblM10.Size = New System.Drawing.Size(45, 18)
        Me.lblM10.TabIndex = 86
        Me.lblM10.Text = "M10:"
        '
        'lblB20
        '
        Me.lblB20.AutoSize = True
        Me.lblB20.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblB20.ForeColor = System.Drawing.Color.LightSlateGray
        Me.lblB20.Location = New System.Drawing.Point(25, 206)
        Me.lblB20.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.lblB20.Name = "lblB20"
        Me.lblB20.Size = New System.Drawing.Size(42, 18)
        Me.lblB20.TabIndex = 82
        Me.lblB20.Text = "B20:"
        '
        'lblM20
        '
        Me.lblM20.AutoSize = True
        Me.lblM20.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblM20.ForeColor = System.Drawing.Color.LightSlateGray
        Me.lblM20.Location = New System.Drawing.Point(24, 100)
        Me.lblM20.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.lblM20.Name = "lblM20"
        Me.lblM20.Size = New System.Drawing.Size(45, 18)
        Me.lblM20.TabIndex = 85
        Me.lblM20.Text = "M20:"
        '
        'lblM100
        '
        Me.lblM100.AutoSize = True
        Me.lblM100.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblM100.ForeColor = System.Drawing.Color.LightSlateGray
        Me.lblM100.Location = New System.Drawing.Point(24, 36)
        Me.lblM100.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.lblM100.Name = "lblM100"
        Me.lblM100.Size = New System.Drawing.Size(54, 18)
        Me.lblM100.TabIndex = 83
        Me.lblM100.Text = "M100:"
        '
        'lblM50
        '
        Me.lblM50.AutoSize = True
        Me.lblM50.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblM50.ForeColor = System.Drawing.Color.LightSlateGray
        Me.lblM50.Location = New System.Drawing.Point(24, 68)
        Me.lblM50.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.lblM50.Name = "lblM50"
        Me.lblM50.Size = New System.Drawing.Size(45, 18)
        Me.lblM50.TabIndex = 84
        Me.lblM50.Text = "M50:"
        '
        'gbxIngresaGastos
        '
        Me.gbxIngresaGastos.Controls.Add(Me.gbxEntregaGlobal)
        Me.gbxIngresaGastos.Controls.Add(Me.gbxMonedas)
        Me.gbxIngresaGastos.Controls.Add(Me.gbxBilletes)
        Me.gbxIngresaGastos.Controls.Add(Me.txtEfectivoCobrado)
        Me.gbxIngresaGastos.Controls.Add(Me.lblCobradoEfectivo)
        Me.gbxIngresaGastos.Controls.Add(Me.txtDescripcion)
        Me.gbxIngresaGastos.Controls.Add(Me.txtUsuario)
        Me.gbxIngresaGastos.Controls.Add(Me.dtpFecha)
        Me.gbxIngresaGastos.Controls.Add(Me.txtClaveEntrega)
        Me.gbxIngresaGastos.Controls.Add(Me.CMBlblUsuario)
        Me.gbxIngresaGastos.Controls.Add(Me.CMBlblClaveEntrega)
        Me.gbxIngresaGastos.Controls.Add(Me.CMBlblFecha)
        Me.gbxIngresaGastos.Controls.Add(Me.CMBlblDescripcion)
        Me.gbxIngresaGastos.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbxIngresaGastos.Location = New System.Drawing.Point(13, 34)
        Me.gbxIngresaGastos.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.gbxIngresaGastos.Name = "gbxIngresaGastos"
        Me.gbxIngresaGastos.Padding = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.gbxIngresaGastos.Size = New System.Drawing.Size(1069, 639)
        Me.gbxIngresaGastos.TabIndex = 8
        Me.gbxIngresaGastos.TabStop = False
        '
        'gbxEntregaGlobal
        '
        Me.gbxEntregaGlobal.Controls.Add(Me.txtImporte)
        Me.gbxEntregaGlobal.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbxEntregaGlobal.Location = New System.Drawing.Point(44, 490)
        Me.gbxEntregaGlobal.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.gbxEntregaGlobal.Name = "gbxEntregaGlobal"
        Me.gbxEntregaGlobal.Padding = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.gbxEntregaGlobal.Size = New System.Drawing.Size(421, 123)
        Me.gbxEntregaGlobal.TabIndex = 100
        Me.gbxEntregaGlobal.TabStop = False
        Me.gbxEntregaGlobal.Text = "Total Entrega Global"
        '
        'txtImporte
        '
        Me.txtImporte.Enabled = False
        Me.txtImporte.Location = New System.Drawing.Point(68, 58)
        Me.txtImporte.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.txtImporte.Name = "txtImporte"
        Me.txtImporte.ReadOnly = True
        Me.txtImporte.Size = New System.Drawing.Size(251, 34)
        Me.txtImporte.TabIndex = 5
        Me.txtImporte.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'gbxMonedas
        '
        Me.gbxMonedas.Controls.Add(Me.txtM100)
        Me.gbxMonedas.Controls.Add(Me.lblM50)
        Me.gbxMonedas.Controls.Add(Me.lblM100)
        Me.gbxMonedas.Controls.Add(Me.txtM50)
        Me.gbxMonedas.Controls.Add(Me.lblM20)
        Me.gbxMonedas.Controls.Add(Me.txtM010)
        Me.gbxMonedas.Controls.Add(Me.txtM20)
        Me.gbxMonedas.Controls.Add(Me.lblM010)
        Me.gbxMonedas.Controls.Add(Me.lblM10)
        Me.gbxMonedas.Controls.Add(Me.txtM020)
        Me.gbxMonedas.Controls.Add(Me.txtM10)
        Me.gbxMonedas.Controls.Add(Me.lblM020)
        Me.gbxMonedas.Controls.Add(Me.lblM5)
        Me.gbxMonedas.Controls.Add(Me.txtM050)
        Me.gbxMonedas.Controls.Add(Me.txtM5)
        Me.gbxMonedas.Controls.Add(Me.lblM050)
        Me.gbxMonedas.Controls.Add(Me.lblM2)
        Me.gbxMonedas.Controls.Add(Me.txtM1)
        Me.gbxMonedas.Controls.Add(Me.txtM2)
        Me.gbxMonedas.Controls.Add(Me.lblM1)
        Me.gbxMonedas.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbxMonedas.Location = New System.Drawing.Point(573, 208)
        Me.gbxMonedas.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.gbxMonedas.Name = "gbxMonedas"
        Me.gbxMonedas.Padding = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.gbxMonedas.Size = New System.Drawing.Size(421, 405)
        Me.gbxMonedas.TabIndex = 99
        Me.gbxMonedas.TabStop = False
        Me.gbxMonedas.Text = "Monedas"
        '
        'txtM100
        '
        Me.txtM100.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtM100.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtM100.Location = New System.Drawing.Point(124, 26)
        Me.txtM100.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.txtM100.Name = "txtM100"
        Me.txtM100.Size = New System.Drawing.Size(129, 25)
        Me.txtM100.TabIndex = 0
        '
        'txtM50
        '
        Me.txtM50.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtM50.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtM50.Location = New System.Drawing.Point(124, 58)
        Me.txtM50.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.txtM50.Name = "txtM50"
        Me.txtM50.Size = New System.Drawing.Size(129, 25)
        Me.txtM50.TabIndex = 1
        '
        'txtM010
        '
        Me.txtM010.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtM010.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtM010.Location = New System.Drawing.Point(124, 314)
        Me.txtM010.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.txtM010.Name = "txtM010"
        Me.txtM010.Size = New System.Drawing.Size(129, 25)
        Me.txtM010.TabIndex = 9
        '
        'txtM20
        '
        Me.txtM20.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtM20.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtM20.Location = New System.Drawing.Point(124, 90)
        Me.txtM20.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.txtM20.Name = "txtM20"
        Me.txtM20.Size = New System.Drawing.Size(129, 25)
        Me.txtM20.TabIndex = 2
        '
        'txtM020
        '
        Me.txtM020.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtM020.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtM020.Location = New System.Drawing.Point(124, 282)
        Me.txtM020.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.txtM020.Name = "txtM020"
        Me.txtM020.Size = New System.Drawing.Size(129, 25)
        Me.txtM020.TabIndex = 8
        '
        'txtM10
        '
        Me.txtM10.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtM10.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtM10.Location = New System.Drawing.Point(124, 122)
        Me.txtM10.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.txtM10.Name = "txtM10"
        Me.txtM10.Size = New System.Drawing.Size(129, 25)
        Me.txtM10.TabIndex = 3
        '
        'txtM050
        '
        Me.txtM050.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtM050.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtM050.Location = New System.Drawing.Point(124, 250)
        Me.txtM050.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.txtM050.Name = "txtM050"
        Me.txtM050.Size = New System.Drawing.Size(129, 25)
        Me.txtM050.TabIndex = 7
        '
        'txtM5
        '
        Me.txtM5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtM5.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtM5.Location = New System.Drawing.Point(124, 154)
        Me.txtM5.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.txtM5.Name = "txtM5"
        Me.txtM5.Size = New System.Drawing.Size(129, 25)
        Me.txtM5.TabIndex = 4
        '
        'txtM1
        '
        Me.txtM1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtM1.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtM1.Location = New System.Drawing.Point(124, 218)
        Me.txtM1.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.txtM1.Name = "txtM1"
        Me.txtM1.Size = New System.Drawing.Size(129, 25)
        Me.txtM1.TabIndex = 6
        '
        'txtM2
        '
        Me.txtM2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtM2.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtM2.Location = New System.Drawing.Point(124, 186)
        Me.txtM2.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.txtM2.Name = "txtM2"
        Me.txtM2.Size = New System.Drawing.Size(129, 25)
        Me.txtM2.TabIndex = 5
        '
        'gbxBilletes
        '
        Me.gbxBilletes.Controls.Add(Me.txtB1000)
        Me.gbxBilletes.Controls.Add(Me.txtB20)
        Me.gbxBilletes.Controls.Add(Me.lblB20)
        Me.gbxBilletes.Controls.Add(Me.txtB50)
        Me.gbxBilletes.Controls.Add(Me.lblB50)
        Me.gbxBilletes.Controls.Add(Me.txtB100)
        Me.gbxBilletes.Controls.Add(Me.lblB100)
        Me.gbxBilletes.Controls.Add(Me.txtB200)
        Me.gbxBilletes.Controls.Add(Me.lblB200)
        Me.gbxBilletes.Controls.Add(Me.lblB1000)
        Me.gbxBilletes.Controls.Add(Me.txtB500)
        Me.gbxBilletes.Controls.Add(Me.lblB500)
        Me.gbxBilletes.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbxBilletes.Location = New System.Drawing.Point(44, 208)
        Me.gbxBilletes.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.gbxBilletes.Name = "gbxBilletes"
        Me.gbxBilletes.Padding = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.gbxBilletes.Size = New System.Drawing.Size(421, 257)
        Me.gbxBilletes.TabIndex = 98
        Me.gbxBilletes.TabStop = False
        Me.gbxBilletes.Text = "Billetes"
        '
        'txtB1000
        '
        Me.txtB1000.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtB1000.CausesValidation = False
        Me.txtB1000.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtB1000.Location = New System.Drawing.Point(125, 36)
        Me.txtB1000.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.txtB1000.Name = "txtB1000"
        Me.txtB1000.Size = New System.Drawing.Size(129, 25)
        Me.txtB1000.TabIndex = 0
        '
        'txtB20
        '
        Me.txtB20.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtB20.CausesValidation = False
        Me.txtB20.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtB20.Location = New System.Drawing.Point(125, 196)
        Me.txtB20.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.txtB20.Name = "txtB20"
        Me.txtB20.Size = New System.Drawing.Size(129, 25)
        Me.txtB20.TabIndex = 5
        '
        'txtB50
        '
        Me.txtB50.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtB50.CausesValidation = False
        Me.txtB50.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtB50.Location = New System.Drawing.Point(125, 164)
        Me.txtB50.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.txtB50.Name = "txtB50"
        Me.txtB50.Size = New System.Drawing.Size(129, 25)
        Me.txtB50.TabIndex = 4
        '
        'txtB100
        '
        Me.txtB100.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtB100.CausesValidation = False
        Me.txtB100.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtB100.Location = New System.Drawing.Point(125, 132)
        Me.txtB100.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.txtB100.Name = "txtB100"
        Me.txtB100.Size = New System.Drawing.Size(129, 25)
        Me.txtB100.TabIndex = 3
        '
        'txtB200
        '
        Me.txtB200.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtB200.CausesValidation = False
        Me.txtB200.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtB200.Location = New System.Drawing.Point(125, 100)
        Me.txtB200.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.txtB200.Name = "txtB200"
        Me.txtB200.Size = New System.Drawing.Size(129, 25)
        Me.txtB200.TabIndex = 2
        '
        'txtB500
        '
        Me.txtB500.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtB500.CausesValidation = False
        Me.txtB500.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtB500.Location = New System.Drawing.Point(125, 68)
        Me.txtB500.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.txtB500.Name = "txtB500"
        Me.txtB500.Size = New System.Drawing.Size(129, 25)
        Me.txtB500.TabIndex = 1
        '
        'txtEfectivoCobrado
        '
        Me.txtEfectivoCobrado.Location = New System.Drawing.Point(865, 57)
        Me.txtEfectivoCobrado.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.txtEfectivoCobrado.Name = "txtEfectivoCobrado"
        Me.txtEfectivoCobrado.ReadOnly = True
        Me.txtEfectivoCobrado.Size = New System.Drawing.Size(185, 26)
        Me.txtEfectivoCobrado.TabIndex = 2
        Me.txtEfectivoCobrado.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'lblCobradoEfectivo
        '
        Me.lblCobradoEfectivo.AutoSize = True
        Me.lblCobradoEfectivo.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCobradoEfectivo.Location = New System.Drawing.Point(656, 60)
        Me.lblCobradoEfectivo.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.lblCobradoEfectivo.Name = "lblCobradoEfectivo"
        Me.lblCobradoEfectivo.Size = New System.Drawing.Size(184, 20)
        Me.lblCobradoEfectivo.TabIndex = 7
        Me.lblCobradoEfectivo.Text = "Efectivo Disponible :"
        '
        'txtDescripcion
        '
        Me.txtDescripcion.Location = New System.Drawing.Point(168, 92)
        Me.txtDescripcion.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.txtDescripcion.Multiline = True
        Me.txtDescripcion.Name = "txtDescripcion"
        Me.txtDescripcion.Size = New System.Drawing.Size(883, 95)
        Me.txtDescripcion.TabIndex = 3
        '
        'txtUsuario
        '
        Me.txtUsuario.Location = New System.Drawing.Point(444, 58)
        Me.txtUsuario.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.txtUsuario.Name = "txtUsuario"
        Me.txtUsuario.ReadOnly = True
        Me.txtUsuario.Size = New System.Drawing.Size(185, 26)
        Me.txtUsuario.TabIndex = 1
        Me.txtUsuario.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'dtpFecha
        '
        Me.dtpFecha.Checked = False
        Me.dtpFecha.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpFecha.Location = New System.Drawing.Point(168, 58)
        Me.dtpFecha.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.dtpFecha.Name = "dtpFecha"
        Me.dtpFecha.Size = New System.Drawing.Size(140, 26)
        Me.dtpFecha.TabIndex = 0
        '
        'txtClaveEntrega
        '
        Me.txtClaveEntrega.Location = New System.Drawing.Point(168, 22)
        Me.txtClaveEntrega.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.txtClaveEntrega.Name = "txtClaveEntrega"
        Me.txtClaveEntrega.ReadOnly = True
        Me.txtClaveEntrega.Size = New System.Drawing.Size(185, 26)
        Me.txtClaveEntrega.TabIndex = 0
        Me.txtClaveEntrega.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'CMBlblUsuario
        '
        Me.CMBlblUsuario.AutoSize = True
        Me.CMBlblUsuario.Location = New System.Drawing.Point(343, 62)
        Me.CMBlblUsuario.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.CMBlblUsuario.Name = "CMBlblUsuario"
        Me.CMBlblUsuario.Size = New System.Drawing.Size(86, 20)
        Me.CMBlblUsuario.TabIndex = 5
        Me.CMBlblUsuario.Text = "Usuario :"
        '
        'CMBlblClaveEntrega
        '
        Me.CMBlblClaveEntrega.AutoSize = True
        Me.CMBlblClaveEntrega.Location = New System.Drawing.Point(8, 26)
        Me.CMBlblClaveEntrega.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.CMBlblClaveEntrega.Name = "CMBlblClaveEntrega"
        Me.CMBlblClaveEntrega.Size = New System.Drawing.Size(139, 20)
        Me.CMBlblClaveEntrega.TabIndex = 0
        Me.CMBlblClaveEntrega.Text = "Clave Entrega :"
        '
        'CMBlblFecha
        '
        Me.CMBlblFecha.AutoSize = True
        Me.CMBlblFecha.Location = New System.Drawing.Point(81, 60)
        Me.CMBlblFecha.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.CMBlblFecha.Name = "CMBlblFecha"
        Me.CMBlblFecha.Size = New System.Drawing.Size(72, 20)
        Me.CMBlblFecha.TabIndex = 1
        Me.CMBlblFecha.Text = "Fecha :"
        '
        'CMBlblDescripcion
        '
        Me.CMBlblDescripcion.AutoSize = True
        Me.CMBlblDescripcion.Location = New System.Drawing.Point(28, 113)
        Me.CMBlblDescripcion.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.CMBlblDescripcion.Name = "CMBlblDescripcion"
        Me.CMBlblDescripcion.Size = New System.Drawing.Size(122, 20)
        Me.CMBlblDescripcion.TabIndex = 3
        Me.CMBlblDescripcion.Text = "Descripción :"
        '
        'bnEntregasGlobalesCancelar
        '
        Me.bnEntregasGlobalesCancelar.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.bnEntregasGlobalesCancelar.Name = "bnEntregasGlobalesCancelar"
        Me.bnEntregasGlobalesCancelar.Size = New System.Drawing.Size(96, 27)
        Me.bnEntregasGlobalesCancelar.Text = "&Cancelar"
        '
        'bnEntregasGlobalesGuargar
        '
        Me.bnEntregasGlobalesGuargar.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.bnEntregasGlobalesGuargar.Name = "bnEntregasGlobalesGuargar"
        Me.bnEntregasGlobalesGuargar.Size = New System.Drawing.Size(90, 27)
        Me.bnEntregasGlobalesGuargar.Text = "&Guardar"
        '
        'bnEntregasGlobales
        '
        Me.bnEntregasGlobales.AddNewItem = Nothing
        Me.bnEntregasGlobales.CountItem = Nothing
        Me.bnEntregasGlobales.DeleteItem = Nothing
        Me.bnEntregasGlobales.ImageScalingSize = New System.Drawing.Size(20, 20)
        Me.bnEntregasGlobales.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.bnEntregasGlobalesCancelar, Me.bnEntregasGlobalesGuargar})
        Me.bnEntregasGlobales.Location = New System.Drawing.Point(0, 0)
        Me.bnEntregasGlobales.MoveFirstItem = Nothing
        Me.bnEntregasGlobales.MoveLastItem = Nothing
        Me.bnEntregasGlobales.MoveNextItem = Nothing
        Me.bnEntregasGlobales.MovePreviousItem = Nothing
        Me.bnEntregasGlobales.Name = "bnEntregasGlobales"
        Me.bnEntregasGlobales.PositionItem = Nothing
        Me.bnEntregasGlobales.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.bnEntregasGlobales.Size = New System.Drawing.Size(1099, 30)
        Me.bnEntregasGlobales.TabIndex = 0
        Me.bnEntregasGlobales.TabStop = True
        '
        'FrmEntregasGlobales
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.AutoScroll = True
        Me.ClientSize = New System.Drawing.Size(1099, 687)
        Me.Controls.Add(Me.bnEntregasGlobales)
        Me.Controls.Add(Me.gbxIngresaGastos)
        Me.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Name = "FrmEntregasGlobales"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Realizar Entrega Global"
        Me.gbxIngresaGastos.ResumeLayout(False)
        Me.gbxIngresaGastos.PerformLayout()
        Me.gbxEntregaGlobal.ResumeLayout(False)
        Me.gbxEntregaGlobal.PerformLayout()
        Me.gbxMonedas.ResumeLayout(False)
        Me.gbxMonedas.PerformLayout()
        Me.gbxBilletes.ResumeLayout(False)
        Me.gbxBilletes.PerformLayout()
        CType(Me.bnEntregasGlobales, System.ComponentModel.ISupportInitialize).EndInit()
        Me.bnEntregasGlobales.ResumeLayout(False)
        Me.bnEntregasGlobales.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents gbxIngresaGastos As System.Windows.Forms.GroupBox
    Friend WithEvents txtImporte As System.Windows.Forms.TextBox
    Friend WithEvents txtDescripcion As System.Windows.Forms.TextBox
    Friend WithEvents txtUsuario As System.Windows.Forms.TextBox
    Friend WithEvents dtpFecha As System.Windows.Forms.DateTimePicker
    Friend WithEvents txtClaveEntrega As System.Windows.Forms.TextBox
    Friend WithEvents CMBlblUsuario As System.Windows.Forms.Label
    Friend WithEvents CMBlblClaveEntrega As System.Windows.Forms.Label
    Friend WithEvents CMBlblFecha As System.Windows.Forms.Label
    Friend WithEvents CMBlblDescripcion As System.Windows.Forms.Label
    Friend WithEvents txtEfectivoCobrado As System.Windows.Forms.TextBox
    Friend WithEvents lblCobradoEfectivo As System.Windows.Forms.Label
    Friend WithEvents txtM010 As System.Windows.Forms.TextBox
    Friend WithEvents txtM020 As System.Windows.Forms.TextBox
    Friend WithEvents txtM050 As System.Windows.Forms.TextBox
    Friend WithEvents txtB1000 As System.Windows.Forms.TextBox
    Friend WithEvents txtM1 As System.Windows.Forms.TextBox
    Friend WithEvents txtB500 As System.Windows.Forms.TextBox
    Friend WithEvents txtM2 As System.Windows.Forms.TextBox
    Friend WithEvents txtB200 As System.Windows.Forms.TextBox
    Friend WithEvents txtM5 As System.Windows.Forms.TextBox
    Friend WithEvents txtB100 As System.Windows.Forms.TextBox
    Friend WithEvents txtM10 As System.Windows.Forms.TextBox
    Friend WithEvents txtB50 As System.Windows.Forms.TextBox
    Friend WithEvents txtM20 As System.Windows.Forms.TextBox
    Friend WithEvents txtB20 As System.Windows.Forms.TextBox
    Friend WithEvents txtM50 As System.Windows.Forms.TextBox
    Friend WithEvents txtM100 As System.Windows.Forms.TextBox
    Friend WithEvents gbxEntregaGlobal As System.Windows.Forms.GroupBox
    Friend WithEvents gbxMonedas As System.Windows.Forms.GroupBox
    Friend WithEvents gbxBilletes As System.Windows.Forms.GroupBox
    Friend WithEvents lblM010 As System.Windows.Forms.Label
    Friend WithEvents lblM020 As System.Windows.Forms.Label
    Friend WithEvents lblB1000 As System.Windows.Forms.Label
    Friend WithEvents lblM050 As System.Windows.Forms.Label
    Friend WithEvents lblB500 As System.Windows.Forms.Label
    Friend WithEvents lblM1 As System.Windows.Forms.Label
    Friend WithEvents lblB200 As System.Windows.Forms.Label
    Friend WithEvents lblM2 As System.Windows.Forms.Label
    Friend WithEvents lblB100 As System.Windows.Forms.Label
    Friend WithEvents lblM5 As System.Windows.Forms.Label
    Friend WithEvents lblB50 As System.Windows.Forms.Label
    Friend WithEvents lblM10 As System.Windows.Forms.Label
    Friend WithEvents lblB20 As System.Windows.Forms.Label
    Friend WithEvents lblM20 As System.Windows.Forms.Label
    Friend WithEvents lblM100 As System.Windows.Forms.Label
    Friend WithEvents lblM50 As System.Windows.Forms.Label
    Friend WithEvents bnEntregasGlobalesCancelar As System.Windows.Forms.ToolStripButton
    Friend WithEvents bnEntregasGlobalesGuargar As System.Windows.Forms.ToolStripButton
    Friend WithEvents bnEntregasGlobales As System.Windows.Forms.BindingNavigator
End Class
