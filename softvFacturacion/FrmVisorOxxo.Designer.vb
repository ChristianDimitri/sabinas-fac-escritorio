<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmVisorOxxo
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim ESHOTELLabel1 As System.Windows.Forms.Label
        Dim SOLOINTERNETLabel1 As System.Windows.Forms.Label
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle4 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle5 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle6 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle7 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle8 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle9 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.MUESTRAVERDETALLE_OXXODataGridView = New System.Windows.Forms.DataGridView()
        Me.DataGridViewTextBoxColumn25 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn18 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn8 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn12 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn13 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn9 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn10 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Total_Puntos = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.MUESTRAVERDETALLE_OXXOBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.DataSetEdgar = New softvFacturacion.DataSetEdgar()
        Me.Button2 = New System.Windows.Forms.Button()
        Me.NewsoftvDataSet = New softvFacturacion.NewsoftvDataSet()
        Me.BUSCLIPORCONTRATO_FACBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.BUSCLIPORCONTRATO_FACTableAdapter = New softvFacturacion.NewsoftvDataSetTableAdapters.BUSCLIPORCONTRATO_FACTableAdapter()
        Me.TreeView1 = New System.Windows.Forms.TreeView()
        Me.DameSerDELCliFACBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.DameSerDELCliFACTableAdapter = New softvFacturacion.NewsoftvDataSetTableAdapters.DameSerDELCliFACTableAdapter()
        Me.Panel5 = New System.Windows.Forms.Panel()
        Me.LABEL19 = New System.Windows.Forms.TextBox()
        Me.MUESTRAVERDETALLE_OXXOTableAdapter = New softvFacturacion.DataSetEdgarTableAdapters.MUESTRAVERDETALLE_OXXOTableAdapter()
        Me.DAMETIPOSCLIENTESBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.DAMETIPOSCLIENTESTableAdapter = New softvFacturacion.DataSetEdgarTableAdapters.DAMETIPOSCLIENTESTableAdapter()
        Me.CMDPanel6 = New System.Windows.Forms.Panel()
        Me.ContratoTextBox = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label20 = New System.Windows.Forms.Label()
        Me.Clv_Session = New System.Windows.Forms.TextBox()
        Me.CLV_TIPOCLIENTELabel1 = New System.Windows.Forms.Label()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.DESCRIPCIONLabel1 = New System.Windows.Forms.Label()
        Me.CALLELabel1 = New System.Windows.Forms.Label()
        Me.Button8 = New System.Windows.Forms.Button()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.COLONIALabel1 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.ESHOTELCheckBox = New System.Windows.Forms.CheckBox()
        Me.NUMEROLabel1 = New System.Windows.Forms.Label()
        Me.CIUDADLabel1 = New System.Windows.Forms.Label()
        Me.SOLOINTERNETCheckBox = New System.Windows.Forms.CheckBox()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.NOMBRELabel1 = New System.Windows.Forms.Label()
        Me.CMBLabel3 = New System.Windows.Forms.Label()
        Me.CMBLabel4 = New System.Windows.Forms.Label()
        Me.CMBLabel5 = New System.Windows.Forms.Label()
        Me.CMBPanel1 = New System.Windows.Forms.Panel()
        Me.CMBImporte = New System.Windows.Forms.Label()
        Me.CMBPuntos = New System.Windows.Forms.Label()
        Me.CMBSubtotal = New System.Windows.Forms.Label()
        ESHOTELLabel1 = New System.Windows.Forms.Label()
        SOLOINTERNETLabel1 = New System.Windows.Forms.Label()
        CType(Me.MUESTRAVERDETALLE_OXXODataGridView, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.MUESTRAVERDETALLE_OXXOBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DataSetEdgar, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.NewsoftvDataSet, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.BUSCLIPORCONTRATO_FACBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DameSerDELCliFACBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel5.SuspendLayout()
        CType(Me.DAMETIPOSCLIENTESBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.CMDPanel6.SuspendLayout()
        Me.CMBPanel1.SuspendLayout()
        Me.SuspendLayout()
        '
        'ESHOTELLabel1
        '
        ESHOTELLabel1.AutoSize = True
        ESHOTELLabel1.BackColor = System.Drawing.Color.WhiteSmoke
        ESHOTELLabel1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        ESHOTELLabel1.ForeColor = System.Drawing.Color.LightSlateGray
        ESHOTELLabel1.Location = New System.Drawing.Point(480, 17)
        ESHOTELLabel1.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        ESHOTELLabel1.Name = "ESHOTELLabel1"
        ESHOTELLabel1.Size = New System.Drawing.Size(82, 20)
        ESHOTELLabel1.TabIndex = 18
        ESHOTELLabel1.Text = "Es Hotel"
        ESHOTELLabel1.Visible = False
        '
        'SOLOINTERNETLabel1
        '
        SOLOINTERNETLabel1.AutoSize = True
        SOLOINTERNETLabel1.BackColor = System.Drawing.Color.WhiteSmoke
        SOLOINTERNETLabel1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        SOLOINTERNETLabel1.ForeColor = System.Drawing.Color.LightSlateGray
        SOLOINTERNETLabel1.Location = New System.Drawing.Point(327, 17)
        SOLOINTERNETLabel1.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        SOLOINTERNETLabel1.Name = "SOLOINTERNETLabel1"
        SOLOINTERNETLabel1.Size = New System.Drawing.Size(116, 20)
        SOLOINTERNETLabel1.TabIndex = 16
        SOLOINTERNETLabel1.Text = "Solo Internet"
        '
        'MUESTRAVERDETALLE_OXXODataGridView
        '
        Me.MUESTRAVERDETALLE_OXXODataGridView.AllowUserToAddRows = False
        Me.MUESTRAVERDETALLE_OXXODataGridView.AllowUserToDeleteRows = False
        Me.MUESTRAVERDETALLE_OXXODataGridView.AutoGenerateColumns = False
        Me.MUESTRAVERDETALLE_OXXODataGridView.BackgroundColor = System.Drawing.Color.LightGray
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.MUESTRAVERDETALLE_OXXODataGridView.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle1
        Me.MUESTRAVERDETALLE_OXXODataGridView.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.DataGridViewTextBoxColumn25, Me.DataGridViewTextBoxColumn18, Me.DataGridViewTextBoxColumn8, Me.DataGridViewTextBoxColumn12, Me.DataGridViewTextBoxColumn13, Me.DataGridViewTextBoxColumn9, Me.DataGridViewTextBoxColumn10, Me.Total_Puntos})
        Me.MUESTRAVERDETALLE_OXXODataGridView.DataSource = Me.MUESTRAVERDETALLE_OXXOBindingSource
        Me.MUESTRAVERDETALLE_OXXODataGridView.Location = New System.Drawing.Point(16, 338)
        Me.MUESTRAVERDETALLE_OXXODataGridView.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.MUESTRAVERDETALLE_OXXODataGridView.Name = "MUESTRAVERDETALLE_OXXODataGridView"
        Me.MUESTRAVERDETALLE_OXXODataGridView.ReadOnly = True
        Me.MUESTRAVERDETALLE_OXXODataGridView.Size = New System.Drawing.Size(1323, 363)
        Me.MUESTRAVERDETALLE_OXXODataGridView.TabIndex = 2
        '
        'DataGridViewTextBoxColumn25
        '
        Me.DataGridViewTextBoxColumn25.DataPropertyName = "Descripcion"
        DataGridViewCellStyle2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.DataGridViewTextBoxColumn25.DefaultCellStyle = DataGridViewCellStyle2
        Me.DataGridViewTextBoxColumn25.HeaderText = "Concepto"
        Me.DataGridViewTextBoxColumn25.Name = "DataGridViewTextBoxColumn25"
        Me.DataGridViewTextBoxColumn25.ReadOnly = True
        Me.DataGridViewTextBoxColumn25.Width = 300
        '
        'DataGridViewTextBoxColumn18
        '
        Me.DataGridViewTextBoxColumn18.DataPropertyName = "tvAdic"
        DataGridViewCellStyle3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.DataGridViewTextBoxColumn18.DefaultCellStyle = DataGridViewCellStyle3
        Me.DataGridViewTextBoxColumn18.HeaderText = "Tvs. Adic."
        Me.DataGridViewTextBoxColumn18.Name = "DataGridViewTextBoxColumn18"
        Me.DataGridViewTextBoxColumn18.ReadOnly = True
        Me.DataGridViewTextBoxColumn18.Width = 50
        '
        'DataGridViewTextBoxColumn8
        '
        Me.DataGridViewTextBoxColumn8.DataPropertyName = "mesesApagar"
        DataGridViewCellStyle4.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.DataGridViewTextBoxColumn8.DefaultCellStyle = DataGridViewCellStyle4
        Me.DataGridViewTextBoxColumn8.HeaderText = "Meses"
        Me.DataGridViewTextBoxColumn8.Name = "DataGridViewTextBoxColumn8"
        Me.DataGridViewTextBoxColumn8.ReadOnly = True
        Me.DataGridViewTextBoxColumn8.Width = 60
        '
        'DataGridViewTextBoxColumn12
        '
        Me.DataGridViewTextBoxColumn12.DataPropertyName = "periodoPagadoIni"
        DataGridViewCellStyle5.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle5.Format = "MMM/yyyy"
        DataGridViewCellStyle5.NullValue = Nothing
        Me.DataGridViewTextBoxColumn12.DefaultCellStyle = DataGridViewCellStyle5
        Me.DataGridViewTextBoxColumn12.HeaderText = "Pagado Desde"
        Me.DataGridViewTextBoxColumn12.Name = "DataGridViewTextBoxColumn12"
        Me.DataGridViewTextBoxColumn12.ReadOnly = True
        '
        'DataGridViewTextBoxColumn13
        '
        Me.DataGridViewTextBoxColumn13.DataPropertyName = "periodoPagadoFin"
        DataGridViewCellStyle6.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle6.Format = "MMM/yyyy"
        DataGridViewCellStyle6.NullValue = Nothing
        Me.DataGridViewTextBoxColumn13.DefaultCellStyle = DataGridViewCellStyle6
        Me.DataGridViewTextBoxColumn13.HeaderText = "Pagado Hasta"
        Me.DataGridViewTextBoxColumn13.Name = "DataGridViewTextBoxColumn13"
        Me.DataGridViewTextBoxColumn13.ReadOnly = True
        '
        'DataGridViewTextBoxColumn9
        '
        Me.DataGridViewTextBoxColumn9.DataPropertyName = "importe"
        DataGridViewCellStyle7.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.DataGridViewTextBoxColumn9.DefaultCellStyle = DataGridViewCellStyle7
        Me.DataGridViewTextBoxColumn9.HeaderText = "Importe"
        Me.DataGridViewTextBoxColumn9.Name = "DataGridViewTextBoxColumn9"
        Me.DataGridViewTextBoxColumn9.ReadOnly = True
        '
        'DataGridViewTextBoxColumn10
        '
        Me.DataGridViewTextBoxColumn10.DataPropertyName = "importeAdicional"
        DataGridViewCellStyle8.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.DataGridViewTextBoxColumn10.DefaultCellStyle = DataGridViewCellStyle8
        Me.DataGridViewTextBoxColumn10.HeaderText = "Adicional"
        Me.DataGridViewTextBoxColumn10.Name = "DataGridViewTextBoxColumn10"
        Me.DataGridViewTextBoxColumn10.ReadOnly = True
        '
        'Total_Puntos
        '
        Me.Total_Puntos.DataPropertyName = "Total_Puntos"
        DataGridViewCellStyle9.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Total_Puntos.DefaultCellStyle = DataGridViewCellStyle9
        Me.Total_Puntos.HeaderText = "Total Puntos"
        Me.Total_Puntos.Name = "Total_Puntos"
        Me.Total_Puntos.ReadOnly = True
        Me.Total_Puntos.Width = 120
        '
        'MUESTRAVERDETALLE_OXXOBindingSource
        '
        Me.MUESTRAVERDETALLE_OXXOBindingSource.DataMember = "MUESTRAVERDETALLE_OXXO"
        Me.MUESTRAVERDETALLE_OXXOBindingSource.DataSource = Me.DataSetEdgar
        '
        'DataSetEdgar
        '
        Me.DataSetEdgar.DataSetName = "DataSetEdgar"
        Me.DataSetEdgar.EnforceConstraints = False
        Me.DataSetEdgar.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'Button2
        '
        Me.Button2.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button2.Location = New System.Drawing.Point(1183, 849)
        Me.Button2.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(156, 48)
        Me.Button2.TabIndex = 6
        Me.Button2.Text = "&CERRAR"
        Me.Button2.UseVisualStyleBackColor = True
        '
        'NewsoftvDataSet
        '
        Me.NewsoftvDataSet.DataSetName = "NewsoftvDataSet"
        Me.NewsoftvDataSet.EnforceConstraints = False
        Me.NewsoftvDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'BUSCLIPORCONTRATO_FACBindingSource
        '
        Me.BUSCLIPORCONTRATO_FACBindingSource.DataMember = "BUSCLIPORCONTRATO_FAC"
        Me.BUSCLIPORCONTRATO_FACBindingSource.DataSource = Me.NewsoftvDataSet
        '
        'BUSCLIPORCONTRATO_FACTableAdapter
        '
        Me.BUSCLIPORCONTRATO_FACTableAdapter.ClearBeforeFill = True
        '
        'TreeView1
        '
        Me.TreeView1.BackColor = System.Drawing.Color.WhiteSmoke
        Me.TreeView1.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.TreeView1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TreeView1.ForeColor = System.Drawing.Color.Black
        Me.TreeView1.Location = New System.Drawing.Point(840, 15)
        Me.TreeView1.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.TreeView1.Name = "TreeView1"
        Me.TreeView1.Size = New System.Drawing.Size(499, 316)
        Me.TreeView1.TabIndex = 528
        Me.TreeView1.TabStop = False
        '
        'DameSerDELCliFACBindingSource
        '
        Me.DameSerDELCliFACBindingSource.DataMember = "DameSerDELCliFAC"
        Me.DameSerDELCliFACBindingSource.DataSource = Me.NewsoftvDataSet
        '
        'DameSerDELCliFACTableAdapter
        '
        Me.DameSerDELCliFACTableAdapter.ClearBeforeFill = True
        '
        'Panel5
        '
        Me.Panel5.BackColor = System.Drawing.Color.WhiteSmoke
        Me.Panel5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel5.Controls.Add(Me.LABEL19)
        Me.Panel5.Location = New System.Drawing.Point(16, 750)
        Me.Panel5.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Panel5.Name = "Panel5"
        Me.Panel5.Size = New System.Drawing.Size(669, 147)
        Me.Panel5.TabIndex = 529
        Me.Panel5.Visible = False
        '
        'LABEL19
        '
        Me.LABEL19.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.LABEL19.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.LABEL19.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LABEL19.ForeColor = System.Drawing.Color.Red
        Me.LABEL19.Location = New System.Drawing.Point(15, 17)
        Me.LABEL19.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.LABEL19.Multiline = True
        Me.LABEL19.Name = "LABEL19"
        Me.LABEL19.ReadOnly = True
        Me.LABEL19.Size = New System.Drawing.Size(635, 116)
        Me.LABEL19.TabIndex = 500
        Me.LABEL19.TabStop = False
        '
        'MUESTRAVERDETALLE_OXXOTableAdapter
        '
        Me.MUESTRAVERDETALLE_OXXOTableAdapter.ClearBeforeFill = True
        '
        'DAMETIPOSCLIENTESBindingSource
        '
        Me.DAMETIPOSCLIENTESBindingSource.DataMember = "DAMETIPOSCLIENTES"
        Me.DAMETIPOSCLIENTESBindingSource.DataSource = Me.DataSetEdgar
        '
        'DAMETIPOSCLIENTESTableAdapter
        '
        Me.DAMETIPOSCLIENTESTableAdapter.ClearBeforeFill = True
        '
        'CMDPanel6
        '
        Me.CMDPanel6.BackColor = System.Drawing.Color.WhiteSmoke
        Me.CMDPanel6.Controls.Add(Me.ContratoTextBox)
        Me.CMDPanel6.Controls.Add(Me.Label1)
        Me.CMDPanel6.Controls.Add(Me.Label20)
        Me.CMDPanel6.Controls.Add(Me.Clv_Session)
        Me.CMDPanel6.Controls.Add(Me.CLV_TIPOCLIENTELabel1)
        Me.CMDPanel6.Controls.Add(Me.Label9)
        Me.CMDPanel6.Controls.Add(Me.DESCRIPCIONLabel1)
        Me.CMDPanel6.Controls.Add(Me.CALLELabel1)
        Me.CMDPanel6.Controls.Add(Me.Button8)
        Me.CMDPanel6.Controls.Add(Me.Label10)
        Me.CMDPanel6.Controls.Add(ESHOTELLabel1)
        Me.CMDPanel6.Controls.Add(Me.COLONIALabel1)
        Me.CMDPanel6.Controls.Add(Me.Label2)
        Me.CMDPanel6.Controls.Add(Me.Label11)
        Me.CMDPanel6.Controls.Add(Me.ESHOTELCheckBox)
        Me.CMDPanel6.Controls.Add(Me.NUMEROLabel1)
        Me.CMDPanel6.Controls.Add(SOLOINTERNETLabel1)
        Me.CMDPanel6.Controls.Add(Me.CIUDADLabel1)
        Me.CMDPanel6.Controls.Add(Me.SOLOINTERNETCheckBox)
        Me.CMDPanel6.Controls.Add(Me.Label8)
        Me.CMDPanel6.Controls.Add(Me.NOMBRELabel1)
        Me.CMDPanel6.Location = New System.Drawing.Point(16, 15)
        Me.CMDPanel6.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.CMDPanel6.Name = "CMDPanel6"
        Me.CMDPanel6.Size = New System.Drawing.Size(816, 316)
        Me.CMDPanel6.TabIndex = 530
        '
        'ContratoTextBox
        '
        Me.ContratoTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.ContratoTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.BUSCLIPORCONTRATO_FACBindingSource, "CONTRATO", True))
        Me.ContratoTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ContratoTextBox.Location = New System.Drawing.Point(131, 18)
        Me.ContratoTextBox.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.ContratoTextBox.Name = "ContratoTextBox"
        Me.ContratoTextBox.ReadOnly = True
        Me.ContratoTextBox.Size = New System.Drawing.Size(121, 29)
        Me.ContratoTextBox.TabIndex = 0
        Me.ContratoTextBox.Text = "0"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.BackColor = System.Drawing.Color.WhiteSmoke
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.ForeColor = System.Drawing.Color.LightSlateGray
        Me.Label1.Location = New System.Drawing.Point(35, 21)
        Me.Label1.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(84, 18)
        Me.Label1.TabIndex = 2
        Me.Label1.Text = "Contrato :"
        '
        'Label20
        '
        Me.Label20.AutoSize = True
        Me.Label20.BackColor = System.Drawing.Color.WhiteSmoke
        Me.Label20.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label20.ForeColor = System.Drawing.Color.LightSlateGray
        Me.Label20.Location = New System.Drawing.Point(43, 203)
        Me.Label20.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label20.Name = "Label20"
        Me.Label20.Size = New System.Drawing.Size(103, 18)
        Me.Label20.TabIndex = 185
        Me.Label20.Text = "Tipo Cobro :"
        '
        'Clv_Session
        '
        Me.Clv_Session.BackColor = System.Drawing.Color.DarkOrange
        Me.Clv_Session.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.Clv_Session.Location = New System.Drawing.Point(148, 23)
        Me.Clv_Session.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Clv_Session.Name = "Clv_Session"
        Me.Clv_Session.Size = New System.Drawing.Size(104, 15)
        Me.Clv_Session.TabIndex = 182
        Me.Clv_Session.TabStop = False
        Me.Clv_Session.Text = "0"
        '
        'CLV_TIPOCLIENTELabel1
        '
        Me.CLV_TIPOCLIENTELabel1.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DAMETIPOSCLIENTESBindingSource, "CLV_TIPOCLIENTE", True))
        Me.CLV_TIPOCLIENTELabel1.ForeColor = System.Drawing.Color.DarkOrange
        Me.CLV_TIPOCLIENTELabel1.Location = New System.Drawing.Point(61, 201)
        Me.CLV_TIPOCLIENTELabel1.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.CLV_TIPOCLIENTELabel1.Name = "CLV_TIPOCLIENTELabel1"
        Me.CLV_TIPOCLIENTELabel1.Size = New System.Drawing.Size(133, 28)
        Me.CLV_TIPOCLIENTELabel1.TabIndex = 184
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.BackColor = System.Drawing.Color.WhiteSmoke
        Me.Label9.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label9.ForeColor = System.Drawing.Color.LightSlateGray
        Me.Label9.Location = New System.Drawing.Point(29, 86)
        Me.Label9.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(90, 18)
        Me.Label9.TabIndex = 14
        Me.Label9.Text = "Dirección :"
        '
        'DESCRIPCIONLabel1
        '
        Me.DESCRIPCIONLabel1.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DAMETIPOSCLIENTESBindingSource, "DESCRIPCION", True))
        Me.DESCRIPCIONLabel1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.DESCRIPCIONLabel1.ForeColor = System.Drawing.Color.Black
        Me.DESCRIPCIONLabel1.Location = New System.Drawing.Point(39, 228)
        Me.DESCRIPCIONLabel1.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.DESCRIPCIONLabel1.Name = "DESCRIPCIONLabel1"
        Me.DESCRIPCIONLabel1.Size = New System.Drawing.Size(235, 28)
        Me.DESCRIPCIONLabel1.TabIndex = 183
        Me.DESCRIPCIONLabel1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'CALLELabel1
        '
        Me.CALLELabel1.AutoEllipsis = True
        Me.CALLELabel1.BackColor = System.Drawing.Color.WhiteSmoke
        Me.CALLELabel1.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.BUSCLIPORCONTRATO_FACBindingSource, "CALLE", True))
        Me.CALLELabel1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CALLELabel1.ForeColor = System.Drawing.Color.Black
        Me.CALLELabel1.Location = New System.Drawing.Point(131, 89)
        Me.CALLELabel1.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.CALLELabel1.Name = "CALLELabel1"
        Me.CALLELabel1.Size = New System.Drawing.Size(439, 28)
        Me.CALLELabel1.TabIndex = 5
        '
        'Button8
        '
        Me.Button8.BackColor = System.Drawing.Color.DarkRed
        Me.Button8.Enabled = False
        Me.Button8.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button8.ForeColor = System.Drawing.Color.White
        Me.Button8.Location = New System.Drawing.Point(509, 270)
        Me.Button8.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Button8.Name = "Button8"
        Me.Button8.Size = New System.Drawing.Size(281, 31)
        Me.Button8.TabIndex = 2
        Me.Button8.Text = "&VER HISTORIAL DE  PAGOS"
        Me.Button8.UseVisualStyleBackColor = False
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.BackColor = System.Drawing.Color.WhiteSmoke
        Me.Label10.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label10.ForeColor = System.Drawing.Color.LightSlateGray
        Me.Label10.Location = New System.Drawing.Point(95, 114)
        Me.Label10.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(33, 24)
        Me.Label10.TabIndex = 15
        Me.Label10.Text = "# :"
        '
        'COLONIALabel1
        '
        Me.COLONIALabel1.AutoEllipsis = True
        Me.COLONIALabel1.BackColor = System.Drawing.Color.WhiteSmoke
        Me.COLONIALabel1.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.BUSCLIPORCONTRATO_FACBindingSource, "COLONIA", True))
        Me.COLONIALabel1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.COLONIALabel1.ForeColor = System.Drawing.Color.Black
        Me.COLONIALabel1.Location = New System.Drawing.Point(131, 150)
        Me.COLONIALabel1.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.COLONIALabel1.Name = "COLONIALabel1"
        Me.COLONIALabel1.Size = New System.Drawing.Size(419, 28)
        Me.COLONIALabel1.TabIndex = 7
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.BackColor = System.Drawing.Color.WhiteSmoke
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.ForeColor = System.Drawing.Color.LightSlateGray
        Me.Label2.Location = New System.Drawing.Point(48, 178)
        Me.Label2.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(70, 18)
        Me.Label2.TabIndex = 173
        Me.Label2.Text = "Ciudad :"
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.BackColor = System.Drawing.Color.WhiteSmoke
        Me.Label11.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label11.ForeColor = System.Drawing.Color.LightSlateGray
        Me.Label11.Location = New System.Drawing.Point(43, 148)
        Me.Label11.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(76, 18)
        Me.Label11.TabIndex = 16
        Me.Label11.Text = "Colonia :"
        '
        'ESHOTELCheckBox
        '
        Me.ESHOTELCheckBox.DataBindings.Add(New System.Windows.Forms.Binding("CheckState", Me.BUSCLIPORCONTRATO_FACBindingSource, "ESHOTEL", True))
        Me.ESHOTELCheckBox.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.ESHOTELCheckBox.ForeColor = System.Drawing.Color.Black
        Me.ESHOTELCheckBox.Location = New System.Drawing.Point(463, 12)
        Me.ESHOTELCheckBox.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.ESHOTELCheckBox.Name = "ESHOTELCheckBox"
        Me.ESHOTELCheckBox.Size = New System.Drawing.Size(28, 30)
        Me.ESHOTELCheckBox.TabIndex = 19
        Me.ESHOTELCheckBox.TabStop = False
        Me.ESHOTELCheckBox.Visible = False
        '
        'NUMEROLabel1
        '
        Me.NUMEROLabel1.AutoEllipsis = True
        Me.NUMEROLabel1.BackColor = System.Drawing.Color.WhiteSmoke
        Me.NUMEROLabel1.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.BUSCLIPORCONTRATO_FACBindingSource, "NUMERO", True))
        Me.NUMEROLabel1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.NUMEROLabel1.ForeColor = System.Drawing.Color.Black
        Me.NUMEROLabel1.Location = New System.Drawing.Point(131, 119)
        Me.NUMEROLabel1.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.NUMEROLabel1.Name = "NUMEROLabel1"
        Me.NUMEROLabel1.Size = New System.Drawing.Size(229, 28)
        Me.NUMEROLabel1.TabIndex = 9
        '
        'CIUDADLabel1
        '
        Me.CIUDADLabel1.AutoEllipsis = True
        Me.CIUDADLabel1.BackColor = System.Drawing.Color.WhiteSmoke
        Me.CIUDADLabel1.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.BUSCLIPORCONTRATO_FACBindingSource, "CIUDAD", True))
        Me.CIUDADLabel1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CIUDADLabel1.ForeColor = System.Drawing.Color.Black
        Me.CIUDADLabel1.Location = New System.Drawing.Point(131, 181)
        Me.CIUDADLabel1.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.CIUDADLabel1.Name = "CIUDADLabel1"
        Me.CIUDADLabel1.Size = New System.Drawing.Size(309, 28)
        Me.CIUDADLabel1.TabIndex = 11
        '
        'SOLOINTERNETCheckBox
        '
        Me.SOLOINTERNETCheckBox.DataBindings.Add(New System.Windows.Forms.Binding("CheckState", Me.BUSCLIPORCONTRATO_FACBindingSource, "SOLOINTERNET", True))
        Me.SOLOINTERNETCheckBox.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.SOLOINTERNETCheckBox.ForeColor = System.Drawing.Color.Black
        Me.SOLOINTERNETCheckBox.Location = New System.Drawing.Point(312, 12)
        Me.SOLOINTERNETCheckBox.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.SOLOINTERNETCheckBox.Name = "SOLOINTERNETCheckBox"
        Me.SOLOINTERNETCheckBox.Size = New System.Drawing.Size(28, 30)
        Me.SOLOINTERNETCheckBox.TabIndex = 17
        Me.SOLOINTERNETCheckBox.TabStop = False
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.BackColor = System.Drawing.Color.WhiteSmoke
        Me.Label8.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label8.ForeColor = System.Drawing.Color.LightSlateGray
        Me.Label8.Location = New System.Drawing.Point(43, 53)
        Me.Label8.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(78, 18)
        Me.Label8.TabIndex = 13
        Me.Label8.Text = "Nombre :"
        '
        'NOMBRELabel1
        '
        Me.NOMBRELabel1.AutoEllipsis = True
        Me.NOMBRELabel1.BackColor = System.Drawing.Color.WhiteSmoke
        Me.NOMBRELabel1.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.BUSCLIPORCONTRATO_FACBindingSource, "NOMBRE", True))
        Me.NOMBRELabel1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.NOMBRELabel1.ForeColor = System.Drawing.Color.Black
        Me.NOMBRELabel1.Location = New System.Drawing.Point(131, 55)
        Me.NOMBRELabel1.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.NOMBRELabel1.Name = "NOMBRELabel1"
        Me.NOMBRELabel1.Size = New System.Drawing.Size(439, 31)
        Me.NOMBRELabel1.TabIndex = 3
        '
        'CMBLabel3
        '
        Me.CMBLabel3.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBLabel3.Location = New System.Drawing.Point(935, 705)
        Me.CMBLabel3.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.CMBLabel3.Name = "CMBLabel3"
        Me.CMBLabel3.Size = New System.Drawing.Size(188, 23)
        Me.CMBLabel3.TabIndex = 531
        Me.CMBLabel3.Text = "SubTotal :"
        Me.CMBLabel3.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'CMBLabel4
        '
        Me.CMBLabel4.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBLabel4.Location = New System.Drawing.Point(935, 750)
        Me.CMBLabel4.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.CMBLabel4.Name = "CMBLabel4"
        Me.CMBLabel4.Size = New System.Drawing.Size(188, 23)
        Me.CMBLabel4.TabIndex = 532
        Me.CMBLabel4.Text = "Total de Puntos :"
        Me.CMBLabel4.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'CMBLabel5
        '
        Me.CMBLabel5.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBLabel5.Location = New System.Drawing.Point(935, 794)
        Me.CMBLabel5.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.CMBLabel5.Name = "CMBLabel5"
        Me.CMBLabel5.Size = New System.Drawing.Size(188, 23)
        Me.CMBLabel5.TabIndex = 533
        Me.CMBLabel5.Text = "Importe Total  :"
        Me.CMBLabel5.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'CMBPanel1
        '
        Me.CMBPanel1.BackColor = System.Drawing.Color.White
        Me.CMBPanel1.Controls.Add(Me.CMBImporte)
        Me.CMBPanel1.Controls.Add(Me.CMBPuntos)
        Me.CMBPanel1.Controls.Add(Me.CMBSubtotal)
        Me.CMBPanel1.Location = New System.Drawing.Point(1131, 684)
        Me.CMBPanel1.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.CMBPanel1.Name = "CMBPanel1"
        Me.CMBPanel1.Size = New System.Drawing.Size(208, 146)
        Me.CMBPanel1.TabIndex = 534
        '
        'CMBImporte
        '
        Me.CMBImporte.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBImporte.ForeColor = System.Drawing.Color.Red
        Me.CMBImporte.Location = New System.Drawing.Point(9, 110)
        Me.CMBImporte.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.CMBImporte.Name = "CMBImporte"
        Me.CMBImporte.Size = New System.Drawing.Size(188, 23)
        Me.CMBImporte.TabIndex = 534
        Me.CMBImporte.Text = "0.0"
        Me.CMBImporte.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'CMBPuntos
        '
        Me.CMBPuntos.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBPuntos.ForeColor = System.Drawing.Color.Red
        Me.CMBPuntos.Location = New System.Drawing.Point(9, 65)
        Me.CMBPuntos.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.CMBPuntos.Name = "CMBPuntos"
        Me.CMBPuntos.Size = New System.Drawing.Size(188, 23)
        Me.CMBPuntos.TabIndex = 533
        Me.CMBPuntos.Text = "0.0"
        Me.CMBPuntos.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'CMBSubtotal
        '
        Me.CMBSubtotal.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBSubtotal.ForeColor = System.Drawing.Color.Red
        Me.CMBSubtotal.Location = New System.Drawing.Point(9, 21)
        Me.CMBSubtotal.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.CMBSubtotal.Name = "CMBSubtotal"
        Me.CMBSubtotal.Size = New System.Drawing.Size(188, 23)
        Me.CMBSubtotal.TabIndex = 532
        Me.CMBSubtotal.Text = "0.0"
        Me.CMBSubtotal.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'FrmVisorOxxo
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.AutoScroll = True
        Me.BackColor = System.Drawing.Color.WhiteSmoke
        Me.ClientSize = New System.Drawing.Size(1355, 912)
        Me.Controls.Add(Me.CMBLabel5)
        Me.Controls.Add(Me.CMBLabel4)
        Me.Controls.Add(Me.CMBLabel3)
        Me.Controls.Add(Me.CMDPanel6)
        Me.Controls.Add(Me.Panel5)
        Me.Controls.Add(Me.TreeView1)
        Me.Controls.Add(Me.Button2)
        Me.Controls.Add(Me.MUESTRAVERDETALLE_OXXODataGridView)
        Me.Controls.Add(Me.CMBPanel1)
        Me.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.MaximizeBox = False
        Me.Name = "FrmVisorOxxo"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Consultar Cobro"
        CType(Me.MUESTRAVERDETALLE_OXXODataGridView, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.MUESTRAVERDETALLE_OXXOBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DataSetEdgar, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.NewsoftvDataSet, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.BUSCLIPORCONTRATO_FACBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DameSerDELCliFACBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel5.ResumeLayout(False)
        Me.Panel5.PerformLayout()
        CType(Me.DAMETIPOSCLIENTESBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.CMDPanel6.ResumeLayout(False)
        Me.CMDPanel6.PerformLayout()
        Me.CMBPanel1.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents DataSetEdgar As softvFacturacion.DataSetEdgar
    Friend WithEvents MUESTRAVERDETALLE_OXXOBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents MUESTRAVERDETALLE_OXXOTableAdapter As softvFacturacion.DataSetEdgarTableAdapters.MUESTRAVERDETALLE_OXXOTableAdapter
    Friend WithEvents MUESTRAVERDETALLE_OXXODataGridView As System.Windows.Forms.DataGridView
    Friend WithEvents Button2 As System.Windows.Forms.Button
    Friend WithEvents NewsoftvDataSet As softvFacturacion.NewsoftvDataSet
    Friend WithEvents BUSCLIPORCONTRATO_FACBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents BUSCLIPORCONTRATO_FACTableAdapter As softvFacturacion.NewsoftvDataSetTableAdapters.BUSCLIPORCONTRATO_FACTableAdapter
    Friend WithEvents TreeView1 As System.Windows.Forms.TreeView
    Friend WithEvents DameSerDELCliFACBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents DameSerDELCliFACTableAdapter As softvFacturacion.NewsoftvDataSetTableAdapters.DameSerDELCliFACTableAdapter
    Friend WithEvents Panel5 As System.Windows.Forms.Panel
    Friend WithEvents LABEL19 As System.Windows.Forms.TextBox
    Friend WithEvents DAMETIPOSCLIENTESBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents DAMETIPOSCLIENTESTableAdapter As softvFacturacion.DataSetEdgarTableAdapters.DAMETIPOSCLIENTESTableAdapter
    Friend WithEvents CMDPanel6 As System.Windows.Forms.Panel
    Friend WithEvents ContratoTextBox As System.Windows.Forms.TextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label20 As System.Windows.Forms.Label
    Friend WithEvents Clv_Session As System.Windows.Forms.TextBox
    Friend WithEvents CLV_TIPOCLIENTELabel1 As System.Windows.Forms.Label
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents DESCRIPCIONLabel1 As System.Windows.Forms.Label
    Friend WithEvents CALLELabel1 As System.Windows.Forms.Label
    Friend WithEvents Button8 As System.Windows.Forms.Button
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents COLONIALabel1 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents ESHOTELCheckBox As System.Windows.Forms.CheckBox
    Friend WithEvents NUMEROLabel1 As System.Windows.Forms.Label
    Friend WithEvents CIUDADLabel1 As System.Windows.Forms.Label
    Friend WithEvents SOLOINTERNETCheckBox As System.Windows.Forms.CheckBox
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents NOMBRELabel1 As System.Windows.Forms.Label
    Friend WithEvents CMBLabel3 As System.Windows.Forms.Label
    Friend WithEvents CMBLabel4 As System.Windows.Forms.Label
    Friend WithEvents CMBLabel5 As System.Windows.Forms.Label
    Friend WithEvents CMBPanel1 As System.Windows.Forms.Panel
    Friend WithEvents CMBImporte As System.Windows.Forms.Label
    Friend WithEvents CMBPuntos As System.Windows.Forms.Label
    Friend WithEvents CMBSubtotal As System.Windows.Forms.Label
    Friend WithEvents DataGridViewTextBoxColumn25 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn18 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn8 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn12 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn13 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn9 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn10 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Total_Puntos As System.Windows.Forms.DataGridViewTextBoxColumn
End Class
