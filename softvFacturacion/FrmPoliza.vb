Imports System.Data.SqlClient

Public Class FrmPoliza



    Private Sub FrmPoliza_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        colorea(Me)
        gLOoPrEPpOLIZA = 0
        Me.Llena_Poliza()
        If LocopPoliza = "C" Then
            Me.Consulta_Genera_PolizaDataGridView.ReadOnly = True
            Me.ToolStripContainer1.Enabled = False

        End If
    End Sub

    Private Sub SUMA()
        Dim DEBE As Double = 0
        Dim HABER As Double = 0
        Dim I As Integer = 0
        For I = 0 To Me.Consulta_Genera_PolizaDataGridView.RowCount - 1
            DEBE = DEBE + Me.Consulta_Genera_PolizaDataGridView.Item(5, I).Value
            HABER = HABER + Me.Consulta_Genera_PolizaDataGridView.Item(6, I).Value
        Next
        Me.TextBox1.Text = Format(CDec(DEBE), "##,##0.00")
        Me.TextBox2.Text = Format(CDec(HABER), "##,##0.00")
    End Sub



    Private Sub Consulta_Genera_PolizaBindingNavigatorSaveItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Consulta_Genera_PolizaBindingNavigatorSaveItem.Click
        Dim conlidia As New SqlClient.SqlConnection(MiConexion)
        conlidia.Open()
        Me.Validate()
        Me.Consulta_Genera_PolizaBindingSource.EndEdit()
        Me.Consulta_Genera_PolizaTableAdapter.Connection = conlidia
        Me.Consulta_Genera_PolizaTableAdapter.Update(Me.DataSetEdgar3.Consulta_Genera_Poliza)
        conlidia.Close()
        modifica()
        SUMA()

    End Sub
    Private Sub modifica()
        Dim conlidia3 As New SqlClient.SqlConnection(MiConexion)
        conlidia3.Open()
        Dim comando As New SqlClient.SqlCommand
        With comando
            .CommandText = "Modificar_Descrip_Poliza"
            .CommandTimeout = 0
            .CommandType = CommandType.StoredProcedure
            .Connection = conlidia3
            Dim prm As New SqlParameter("@Clv_llave_Poliza", SqlDbType.BigInt)
            Dim prm1 As New SqlParameter("@descrip", SqlDbType.VarChar, 400)
            prm.Direction = ParameterDirection.Input
            prm1.Direction = ParameterDirection.Input
            prm.Value = CInt(Me.Clv_Llave_PolizaTextBox.Text)
            prm1.Value = Me.ConceptoTextBox.Text
            .Parameters.Add(prm)
            .Parameters.Add(prm1)
            Dim i As Integer = comando.ExecuteNonQuery
        End With
        conlidia3.Close()
    End Sub
    Private Sub Llena_Poliza()
        Try
            Dim Con As New SqlConnection(MiConexion)
            Con.Open()
            Me.Consulta_Genera_PolizaTableAdapter.Connection = Con
            Me.Consulta_Genera_PolizaTableAdapter.Fill(Me.DataSetEdgar3.Consulta_Genera_Poliza, LocGloClv_poliza)
            Me.Dame_Tabla_PolizaTableAdapter.Connection = Con
            Me.Dame_Tabla_PolizaTableAdapter.Fill(Me.DataSetEdgar3.Dame_Tabla_Poliza, LocGloClv_poliza)
            'MUESTRASucursalesPoliza(LocGloClv_poliza)
            MUESTRACiudadesPoliza(LocGloClv_poliza)
            If LocopPoliza <> "N" Then
                Dim ComandoLidia As New SqlClient.SqlCommand
                With ComandoLidia
                    .CommandText = "Dame_Clave_poliza"
                    .CommandTimeout = 0
                    .CommandType = CommandType.StoredProcedure
                    .Connection = Con
                    Dim Prm1 As New SqlParameter("@Clv_Llave_Poliza", SqlDbType.BigInt)
                    Prm1.Direction = ParameterDirection.Input
                    Prm1.Value = LocGloClv_poliza
                    .Parameters.Add(Prm1)

                    Dim Prm2 As New SqlParameter("@opc", SqlDbType.VarChar, 5)
                    Prm2.Direction = ParameterDirection.Input
                    Prm2.Value = LocopPoliza
                    .Parameters.Add(Prm2)

                    Dim Prm3 As New SqlParameter("@Clave", SqlDbType.BigInt)
                    Prm3.Direction = ParameterDirection.Output
                    Prm3.Value = 0
                    .Parameters.Add(Prm3)

                    Dim i As Integer = ComandoLidia.ExecuteNonQuery()
                    GloPoliza2 = Prm3.Value
                    Me.TextBox3.Text = GloPoliza2
                End With
            ElseIf LocopPoliza = "N" Then
                Me.TextBox3.Text = GloPoliza2
            End If

            Con.Close()
            SUMA()
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try

    End Sub

    Private Sub Button5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button5.Click
        Me.Close()
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        If IsNumeric(LocGloClv_poliza) = True Then
            gLOoPrEPpOLIZA = 0
            FrmImprimePoliza.Show()
        End If
    End Sub


    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        If IsNumeric(LocGloClv_poliza) = True Then
            gLOoPrEPpOLIZA = 1
            FrmImprimePoliza.Show()
            gLOoPrEPpOLIZA = 0
        End If
    End Sub

    Private Sub MUESTRASucursalesPoliza(ByVal Clv_llave_Poliza As Integer)
        Dim dTable As DataTable
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@Clv_llave_Poliza", SqlDbType.Int, Clv_llave_Poliza)
        dTable = BaseII.ConsultaDT("MUESTRASucursalesPoliza")
        If dTable.Rows.Count = 0 Then Exit Sub
        tbSucursales.Text = dTable.Rows(0)(0).ToString()
        'sucursales fsdf

    End Sub
    Private Sub MUESTRACiudadesPoliza(ByVal Clv_llave_Poliza As Integer)
        Dim dTable As DataTable
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@Clv_llave_Poliza", SqlDbType.Int, Clv_llave_Poliza)
        dTable = BaseII.ConsultaDT("MUESTRACiuPoliza")
        If dTable.Rows.Count = 0 Then Exit Sub
        tbSucursales.Text = dTable.Rows(0)(0).ToString()
        'sucursales fsdf
    End Sub
End Class