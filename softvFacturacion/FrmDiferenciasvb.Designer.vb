﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmDiferenciasvb
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.dgvDiferencias = New System.Windows.Forms.DataGridView()
        Me.idDiferencia = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.idCorte = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.fechaCorte = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.cajera = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.nomCajera = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.monto = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.status = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.autoriza = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.nomAutoriza = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.fechaAutorizado = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.fechaCancelado = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.fechaGenerado = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.btnSaldar = New System.Windows.Forms.Button()
        Me.btnCancelar = New System.Windows.Forms.Button()
        Me.btnSalir = New System.Windows.Forms.Button()
        Me.CMBlblBusqueda = New System.Windows.Forms.Label()
        Me.cmbCajera = New System.Windows.Forms.ComboBox()
        Me.rbSinSaldar = New System.Windows.Forms.RadioButton()
        Me.rbSaldadas = New System.Windows.Forms.RadioButton()
        Me.gbxStatus = New System.Windows.Forms.GroupBox()
        Me.gbxCajera = New System.Windows.Forms.GroupBox()
        Me.gbxAutoriza = New System.Windows.Forms.GroupBox()
        Me.cmbAutoriza = New System.Windows.Forms.ComboBox()
        CType(Me.dgvDiferencias, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gbxStatus.SuspendLayout()
        Me.gbxCajera.SuspendLayout()
        Me.gbxAutoriza.SuspendLayout()
        Me.SuspendLayout()
        '
        'dgvDiferencias
        '
        Me.dgvDiferencias.AllowUserToAddRows = False
        Me.dgvDiferencias.AllowUserToDeleteRows = False
        Me.dgvDiferencias.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvDiferencias.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.idDiferencia, Me.idCorte, Me.fechaCorte, Me.cajera, Me.nomCajera, Me.monto, Me.status, Me.autoriza, Me.nomAutoriza, Me.fechaAutorizado, Me.fechaCancelado, Me.fechaGenerado})
        Me.dgvDiferencias.Location = New System.Drawing.Point(243, 15)
        Me.dgvDiferencias.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.dgvDiferencias.Name = "dgvDiferencias"
        Me.dgvDiferencias.ReadOnly = True
        Me.dgvDiferencias.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvDiferencias.Size = New System.Drawing.Size(925, 657)
        Me.dgvDiferencias.TabIndex = 0
        '
        'idDiferencia
        '
        Me.idDiferencia.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.idDiferencia.DataPropertyName = "idDiferencia"
        Me.idDiferencia.HeaderText = "Id"
        Me.idDiferencia.Name = "idDiferencia"
        Me.idDiferencia.ReadOnly = True
        Me.idDiferencia.Visible = False
        '
        'idCorte
        '
        Me.idCorte.DataPropertyName = "idCorte"
        Me.idCorte.HeaderText = "Id Corte"
        Me.idCorte.Name = "idCorte"
        Me.idCorte.ReadOnly = True
        Me.idCorte.Visible = False
        '
        'fechaCorte
        '
        Me.fechaCorte.DataPropertyName = "fechaCorte"
        Me.fechaCorte.HeaderText = "Fecha Corte"
        Me.fechaCorte.Name = "fechaCorte"
        Me.fechaCorte.ReadOnly = True
        Me.fechaCorte.Width = 70
        '
        'cajera
        '
        Me.cajera.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.cajera.DataPropertyName = "cajera"
        Me.cajera.HeaderText = "Usuario"
        Me.cajera.Name = "cajera"
        Me.cajera.ReadOnly = True
        Me.cajera.Visible = False
        '
        'nomCajera
        '
        Me.nomCajera.DataPropertyName = "nomCajera"
        Me.nomCajera.HeaderText = "Cajera"
        Me.nomCajera.Name = "nomCajera"
        Me.nomCajera.ReadOnly = True
        Me.nomCajera.Width = 190
        '
        'monto
        '
        Me.monto.DataPropertyName = "monto"
        Me.monto.HeaderText = "Importe"
        Me.monto.Name = "monto"
        Me.monto.ReadOnly = True
        Me.monto.Width = 70
        '
        'status
        '
        Me.status.DataPropertyName = "status"
        Me.status.HeaderText = "Status"
        Me.status.Name = "status"
        Me.status.ReadOnly = True
        Me.status.Width = 60
        '
        'autoriza
        '
        Me.autoriza.DataPropertyName = "autoriza"
        Me.autoriza.HeaderText = "clvAutoriza"
        Me.autoriza.Name = "autoriza"
        Me.autoriza.ReadOnly = True
        Me.autoriza.Visible = False
        Me.autoriza.Width = 80
        '
        'nomAutoriza
        '
        Me.nomAutoriza.DataPropertyName = "nomAutoriza"
        Me.nomAutoriza.HeaderText = "Autoriza"
        Me.nomAutoriza.Name = "nomAutoriza"
        Me.nomAutoriza.ReadOnly = True
        Me.nomAutoriza.Width = 190
        '
        'fechaAutorizado
        '
        Me.fechaAutorizado.DataPropertyName = "fechaAutorizado"
        Me.fechaAutorizado.HeaderText = "Fecha Autorización"
        Me.fechaAutorizado.Name = "fechaAutorizado"
        Me.fechaAutorizado.ReadOnly = True
        Me.fechaAutorizado.Visible = False
        '
        'fechaCancelado
        '
        Me.fechaCancelado.DataPropertyName = "fechaCancelado"
        Me.fechaCancelado.HeaderText = "Fecha Cancelación"
        Me.fechaCancelado.Name = "fechaCancelado"
        Me.fechaCancelado.ReadOnly = True
        Me.fechaCancelado.Visible = False
        '
        'fechaGenerado
        '
        Me.fechaGenerado.DataPropertyName = "fechaGenerado"
        Me.fechaGenerado.HeaderText = "Fecha Generación"
        Me.fechaGenerado.Name = "fechaGenerado"
        Me.fechaGenerado.ReadOnly = True
        Me.fechaGenerado.Width = 70
        '
        'btnSaldar
        '
        Me.btnSaldar.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSaldar.Location = New System.Drawing.Point(1176, 15)
        Me.btnSaldar.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.btnSaldar.Name = "btnSaldar"
        Me.btnSaldar.Size = New System.Drawing.Size(148, 42)
        Me.btnSaldar.TabIndex = 1
        Me.btnSaldar.Text = "&Saldar"
        Me.btnSaldar.UseVisualStyleBackColor = True
        '
        'btnCancelar
        '
        Me.btnCancelar.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnCancelar.Location = New System.Drawing.Point(1176, 64)
        Me.btnCancelar.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.btnCancelar.Name = "btnCancelar"
        Me.btnCancelar.Size = New System.Drawing.Size(148, 42)
        Me.btnCancelar.TabIndex = 2
        Me.btnCancelar.Text = "&Cancelar"
        Me.btnCancelar.UseVisualStyleBackColor = True
        '
        'btnSalir
        '
        Me.btnSalir.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSalir.Location = New System.Drawing.Point(1176, 630)
        Me.btnSalir.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.btnSalir.Name = "btnSalir"
        Me.btnSalir.Size = New System.Drawing.Size(148, 42)
        Me.btnSalir.TabIndex = 3
        Me.btnSalir.Text = "&Salir"
        Me.btnSalir.UseVisualStyleBackColor = True
        '
        'CMBlblBusqueda
        '
        Me.CMBlblBusqueda.AutoSize = True
        Me.CMBlblBusqueda.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBlblBusqueda.Location = New System.Drawing.Point(16, 15)
        Me.CMBlblBusqueda.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.CMBlblBusqueda.Name = "CMBlblBusqueda"
        Me.CMBlblBusqueda.Size = New System.Drawing.Size(155, 29)
        Me.CMBlblBusqueda.TabIndex = 4
        Me.CMBlblBusqueda.Text = "Buscar Por :"
        '
        'cmbCajera
        '
        Me.cmbCajera.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmbCajera.FormattingEnabled = True
        Me.cmbCajera.Location = New System.Drawing.Point(3, 26)
        Me.cmbCajera.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.cmbCajera.Name = "cmbCajera"
        Me.cmbCajera.Size = New System.Drawing.Size(221, 25)
        Me.cmbCajera.TabIndex = 5
        '
        'rbSinSaldar
        '
        Me.rbSinSaldar.AutoSize = True
        Me.rbSinSaldar.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rbSinSaldar.Location = New System.Drawing.Point(21, 22)
        Me.rbSinSaldar.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.rbSinSaldar.Name = "rbSinSaldar"
        Me.rbSinSaldar.Size = New System.Drawing.Size(117, 24)
        Me.rbSinSaldar.TabIndex = 8
        Me.rbSinSaldar.TabStop = True
        Me.rbSinSaldar.Text = "Sin Saldar"
        Me.rbSinSaldar.UseVisualStyleBackColor = True
        '
        'rbSaldadas
        '
        Me.rbSaldadas.AutoSize = True
        Me.rbSaldadas.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rbSaldadas.Location = New System.Drawing.Point(21, 48)
        Me.rbSaldadas.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.rbSaldadas.Name = "rbSaldadas"
        Me.rbSaldadas.Size = New System.Drawing.Size(107, 24)
        Me.rbSaldadas.TabIndex = 9
        Me.rbSaldadas.TabStop = True
        Me.rbSaldadas.Text = "Saldadas"
        Me.rbSaldadas.UseVisualStyleBackColor = True
        '
        'gbxStatus
        '
        Me.gbxStatus.Controls.Add(Me.rbSinSaldar)
        Me.gbxStatus.Controls.Add(Me.rbSaldadas)
        Me.gbxStatus.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbxStatus.Location = New System.Drawing.Point(4, 64)
        Me.gbxStatus.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.gbxStatus.Name = "gbxStatus"
        Me.gbxStatus.Padding = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.gbxStatus.Size = New System.Drawing.Size(231, 75)
        Me.gbxStatus.TabIndex = 10
        Me.gbxStatus.TabStop = False
        Me.gbxStatus.Text = "Status"
        '
        'gbxCajera
        '
        Me.gbxCajera.Controls.Add(Me.cmbCajera)
        Me.gbxCajera.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbxCajera.Location = New System.Drawing.Point(4, 148)
        Me.gbxCajera.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.gbxCajera.Name = "gbxCajera"
        Me.gbxCajera.Padding = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.gbxCajera.Size = New System.Drawing.Size(231, 63)
        Me.gbxCajera.TabIndex = 11
        Me.gbxCajera.TabStop = False
        Me.gbxCajera.Text = "Cajera"
        '
        'gbxAutoriza
        '
        Me.gbxAutoriza.Controls.Add(Me.cmbAutoriza)
        Me.gbxAutoriza.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbxAutoriza.Location = New System.Drawing.Point(7, 218)
        Me.gbxAutoriza.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.gbxAutoriza.Name = "gbxAutoriza"
        Me.gbxAutoriza.Padding = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.gbxAutoriza.Size = New System.Drawing.Size(231, 63)
        Me.gbxAutoriza.TabIndex = 12
        Me.gbxAutoriza.TabStop = False
        Me.gbxAutoriza.Text = "Autoriza"
        '
        'cmbAutoriza
        '
        Me.cmbAutoriza.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmbAutoriza.FormattingEnabled = True
        Me.cmbAutoriza.Location = New System.Drawing.Point(3, 26)
        Me.cmbAutoriza.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.cmbAutoriza.Name = "cmbAutoriza"
        Me.cmbAutoriza.Size = New System.Drawing.Size(221, 25)
        Me.cmbAutoriza.TabIndex = 5
        '
        'FrmDiferenciasvb
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.AutoScroll = True
        Me.ClientSize = New System.Drawing.Size(1336, 678)
        Me.Controls.Add(Me.gbxAutoriza)
        Me.Controls.Add(Me.gbxCajera)
        Me.Controls.Add(Me.gbxStatus)
        Me.Controls.Add(Me.CMBlblBusqueda)
        Me.Controls.Add(Me.btnSalir)
        Me.Controls.Add(Me.btnCancelar)
        Me.Controls.Add(Me.btnSaldar)
        Me.Controls.Add(Me.dgvDiferencias)
        Me.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Name = "FrmDiferenciasvb"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Ajuste por Diferencias"
        CType(Me.dgvDiferencias, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gbxStatus.ResumeLayout(False)
        Me.gbxStatus.PerformLayout()
        Me.gbxCajera.ResumeLayout(False)
        Me.gbxAutoriza.ResumeLayout(False)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents dgvDiferencias As System.Windows.Forms.DataGridView
    Friend WithEvents btnSaldar As System.Windows.Forms.Button
    Friend WithEvents btnCancelar As System.Windows.Forms.Button
    Friend WithEvents btnSalir As System.Windows.Forms.Button
    Friend WithEvents CMBlblBusqueda As System.Windows.Forms.Label
    Friend WithEvents cmbCajera As System.Windows.Forms.ComboBox
    Friend WithEvents rbSinSaldar As System.Windows.Forms.RadioButton
    Friend WithEvents rbSaldadas As System.Windows.Forms.RadioButton
    Friend WithEvents gbxStatus As System.Windows.Forms.GroupBox
    Friend WithEvents gbxCajera As System.Windows.Forms.GroupBox
    Friend WithEvents gbxAutoriza As System.Windows.Forms.GroupBox
    Friend WithEvents cmbAutoriza As System.Windows.Forms.ComboBox
    Friend WithEvents idDiferencia As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents idCorte As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents fechaCorte As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents cajera As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents nomCajera As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents monto As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents status As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents autoriza As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents nomAutoriza As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents fechaAutorizado As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents fechaCancelado As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents fechaGenerado As System.Windows.Forms.DataGridViewTextBoxColumn
End Class
