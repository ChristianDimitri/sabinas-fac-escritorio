﻿Public Class FrmMotivoCancelacionFacturaAlmacen

    Private Sub muestraMotivos()
        BaseII.limpiaParametros()
        BaseII.ConsultaDS("MuestraMotivoCancelacion")
        DescripcionComboBox.DataSource = BaseII.ConsultaDT("MuestraMotivoCancelacion")
        DescripcionComboBox.DisplayMember = "MOTCAN"
        DescripcionComboBox.ValueMember = "Clv_MOTCAN"
    End Sub

    Private Sub FrmMotivoCancelacionFacturaAlmacen_Load(sender As Object, e As EventArgs) Handles Me.Load
        muestraMotivos()

    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@Clv_Factura", SqlDbType.BigInt, eCveFactura)
        BaseII.CreateMyParameter("@Clv_Motivo", SqlDbType.BigInt, DescripcionComboBox.SelectedValue)
        BaseII.CreateMyParameter("@clv_usuario", SqlDbType.VarChar, LocLoginUsuario, 5)
        BaseII.Inserta("GuardaMotivosAlmacen")


        Me.Close()
    End Sub
End Class