Public Class FrmSeleccionaTipo

    Private Sub FrmSeleccionaTipo_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        colorea(Me)

        Me.ComboBox1.Items.Add("Historial de Pagos")
        If IdSistema <> "LO" Then
            Me.ComboBox1.Items.Add("Historial de Ordenes de Servicio")
        End If
        'Me.ComboBox1.Items.Add("Historial de Pagos")
        Me.ComboBox1.Items.Add("Historial de Reportes")
        'Vamos a validar que el contrato tenga internet para el historial de los estados de cuenta
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@contrato", SqlDbType.BigInt, GloContrato)
        BaseII.CreateMyParameter("@tieneSaldo", ParameterDirection.Output, SqlDbType.Int)
        BaseII.ProcedimientoOutPut("ValidaHistorialContrato")
        If BaseII.dicoPar("@tieneSaldo") = 1 Then
            Me.ComboBox1.Items.Add("Historial de Estados de Cuenta")
        End If
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        Me.Close()
    End Sub

    Private Sub Button8_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button8.Click

        If IsNumeric(GloContrato) = True And GloContrato > 0 Then
            GloOpFacturas = 3
            eBotonGuardar = False
            If Me.ComboBox1.Text = "Historial de Pagos" Then
                'BrwFacturas_Cancelar.Show()
                FrmHistorialReimpresion.Show()
            ElseIf Me.ComboBox1.Text = "Historial de Ordenes de Servicio" Then
                VisorHistorialOrd.Show()
            ElseIf Me.ComboBox1.Text = "Historial de Reportes" Then
                VisorHistorialQuejas.Show()
            ElseIf Me.ComboBox1.Text = "Historial de Estados de Cuenta" Then
                FrmEstadoCuentaSaldo.Show()
            End If
        Else
            MsgBox("Seleccione un Cliente por favor", MsgBoxStyle.Information)
        End If
        Me.Close()


    End Sub
End Class