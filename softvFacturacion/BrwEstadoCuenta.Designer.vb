﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class BrwEstadoCuenta
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle5 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle4 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.dgPeriodo = New System.Windows.Forms.DataGridView()
        Me.Id = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Clv_Periodo = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.FechaGeneracion = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.PeriodoIni = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.PeriodoFin = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.FechaLimitePago = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.dgEstado = New System.Windows.Forms.DataGridView()
        Me.IdEstadoCuenta = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Contrato = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Nombre = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.TotalAPagar = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.bnVerTodos = New System.Windows.Forms.Button()
        Me.bnVer = New System.Windows.Forms.Button()
        Me.Button1 = New System.Windows.Forms.Button()
        CType(Me.dgPeriodo, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgEstado, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'dgPeriodo
        '
        Me.dgPeriodo.AllowUserToAddRows = False
        Me.dgPeriodo.AllowUserToDeleteRows = False
        Me.dgPeriodo.BackgroundColor = System.Drawing.Color.WhiteSmoke
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgPeriodo.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle1
        Me.dgPeriodo.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgPeriodo.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.Id, Me.Clv_Periodo, Me.FechaGeneracion, Me.PeriodoIni, Me.PeriodoFin, Me.FechaLimitePago})
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgPeriodo.DefaultCellStyle = DataGridViewCellStyle2
        Me.dgPeriodo.Location = New System.Drawing.Point(291, 43)
        Me.dgPeriodo.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.dgPeriodo.Name = "dgPeriodo"
        Me.dgPeriodo.ReadOnly = True
        Me.dgPeriodo.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgPeriodo.Size = New System.Drawing.Size(871, 300)
        Me.dgPeriodo.TabIndex = 0
        '
        'Id
        '
        Me.Id.DataPropertyName = "Id"
        Me.Id.HeaderText = "Id"
        Me.Id.Name = "Id"
        Me.Id.ReadOnly = True
        Me.Id.Visible = False
        '
        'Clv_Periodo
        '
        Me.Clv_Periodo.DataPropertyName = "Clv_Periodo"
        Me.Clv_Periodo.HeaderText = "Periodo"
        Me.Clv_Periodo.Name = "Clv_Periodo"
        Me.Clv_Periodo.ReadOnly = True
        '
        'FechaGeneracion
        '
        Me.FechaGeneracion.DataPropertyName = "FechaGeneracion"
        Me.FechaGeneracion.HeaderText = "Fecha de Generación"
        Me.FechaGeneracion.Name = "FechaGeneracion"
        Me.FechaGeneracion.ReadOnly = True
        Me.FechaGeneracion.Width = 200
        '
        'PeriodoIni
        '
        Me.PeriodoIni.DataPropertyName = "PeriodoIni"
        Me.PeriodoIni.HeaderText = "Periodo Inicial"
        Me.PeriodoIni.Name = "PeriodoIni"
        Me.PeriodoIni.ReadOnly = True
        Me.PeriodoIni.Width = 150
        '
        'PeriodoFin
        '
        Me.PeriodoFin.DataPropertyName = "PeriodoFin"
        Me.PeriodoFin.HeaderText = "Periodo Final"
        Me.PeriodoFin.Name = "PeriodoFin"
        Me.PeriodoFin.ReadOnly = True
        Me.PeriodoFin.Width = 150
        '
        'FechaLimitePago
        '
        Me.FechaLimitePago.DataPropertyName = "FechaLimitePago"
        Me.FechaLimitePago.HeaderText = "FechaLimitePago"
        Me.FechaLimitePago.Name = "FechaLimitePago"
        Me.FechaLimitePago.ReadOnly = True
        Me.FechaLimitePago.Visible = False
        '
        'dgEstado
        '
        Me.dgEstado.AllowUserToAddRows = False
        Me.dgEstado.AllowUserToDeleteRows = False
        Me.dgEstado.BackgroundColor = System.Drawing.Color.WhiteSmoke
        DataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgEstado.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle3
        Me.dgEstado.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgEstado.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.IdEstadoCuenta, Me.Contrato, Me.Nombre, Me.TotalAPagar})
        DataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle5.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle5.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle5.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle5.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle5.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgEstado.DefaultCellStyle = DataGridViewCellStyle5
        Me.dgEstado.Location = New System.Drawing.Point(28, 377)
        Me.dgEstado.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.dgEstado.Name = "dgEstado"
        Me.dgEstado.ReadOnly = True
        Me.dgEstado.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgEstado.Size = New System.Drawing.Size(1133, 414)
        Me.dgEstado.TabIndex = 1
        '
        'IdEstadoCuenta
        '
        Me.IdEstadoCuenta.DataPropertyName = "IdEstadoCuenta"
        Me.IdEstadoCuenta.HeaderText = "EstadoCuenta"
        Me.IdEstadoCuenta.Name = "IdEstadoCuenta"
        Me.IdEstadoCuenta.ReadOnly = True
        Me.IdEstadoCuenta.Width = 130
        '
        'Contrato
        '
        Me.Contrato.DataPropertyName = "Contrato"
        Me.Contrato.HeaderText = "Contrato"
        Me.Contrato.Name = "Contrato"
        Me.Contrato.ReadOnly = True
        '
        'Nombre
        '
        Me.Nombre.DataPropertyName = "Nombre"
        Me.Nombre.HeaderText = "Nombre"
        Me.Nombre.Name = "Nombre"
        Me.Nombre.ReadOnly = True
        Me.Nombre.Width = 400
        '
        'TotalAPagar
        '
        Me.TotalAPagar.DataPropertyName = "TotalAPagar"
        DataGridViewCellStyle4.Format = "C2"
        DataGridViewCellStyle4.NullValue = Nothing
        Me.TotalAPagar.DefaultCellStyle = DataGridViewCellStyle4
        Me.TotalAPagar.HeaderText = "Total a Pagar"
        Me.TotalAPagar.Name = "TotalAPagar"
        Me.TotalAPagar.ReadOnly = True
        Me.TotalAPagar.Width = 150
        '
        'bnVerTodos
        '
        Me.bnVerTodos.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.bnVerTodos.Location = New System.Drawing.Point(1169, 43)
        Me.bnVerTodos.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.bnVerTodos.Name = "bnVerTodos"
        Me.bnVerTodos.Size = New System.Drawing.Size(151, 28)
        Me.bnVerTodos.TabIndex = 2
        Me.bnVerTodos.Text = "Ver Todos"
        Me.bnVerTodos.UseVisualStyleBackColor = True
        '
        'bnVer
        '
        Me.bnVer.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.bnVer.Location = New System.Drawing.Point(1169, 377)
        Me.bnVer.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.bnVer.Name = "bnVer"
        Me.bnVer.Size = New System.Drawing.Size(151, 28)
        Me.bnVer.TabIndex = 4
        Me.bnVer.Text = "Ver"
        Me.bnVer.UseVisualStyleBackColor = True
        '
        'Button1
        '
        Me.Button1.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button1.Location = New System.Drawing.Point(1147, 839)
        Me.Button1.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(181, 44)
        Me.Button1.TabIndex = 6
        Me.Button1.Text = "&SALIR"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'BrwEstadoCuenta
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.AutoScroll = True
        Me.ClientSize = New System.Drawing.Size(1344, 898)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.bnVer)
        Me.Controls.Add(Me.bnVerTodos)
        Me.Controls.Add(Me.dgEstado)
        Me.Controls.Add(Me.dgPeriodo)
        Me.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Name = "BrwEstadoCuenta"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Estados de Cuenta"
        CType(Me.dgPeriodo, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgEstado, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents dgPeriodo As System.Windows.Forms.DataGridView
    Friend WithEvents dgEstado As System.Windows.Forms.DataGridView
    Friend WithEvents Id As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Clv_Periodo As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents FechaGeneracion As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents PeriodoIni As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents PeriodoFin As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents FechaLimitePago As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents bnVerTodos As System.Windows.Forms.Button
    Friend WithEvents bnVer As System.Windows.Forms.Button
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents IdEstadoCuenta As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Contrato As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Nombre As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents TotalAPagar As System.Windows.Forms.DataGridViewTextBoxColumn
End Class
