﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class BrwCancelacionArqueos
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.btnCancelar = New System.Windows.Forms.Button()
        Me.cmbBuscaAutoriza = New System.Windows.Forms.ComboBox()
        Me.CMBlblMuestraAutoriza = New System.Windows.Forms.Label()
        Me.cmbBuscaCajera = New System.Windows.Forms.ComboBox()
        Me.CMBlblBuscaCajera = New System.Windows.Forms.Label()
        Me.dgvCortesGlobales = New System.Windows.Forms.DataGridView()
        Me.idCorte = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.cajera = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.fechaCorte = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.fechaGeneracion = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.totalEfectivo = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.totalTarjeta = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.totalCheques = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.entregasParciales = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.totalCobrado = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.gastos = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.status = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.autoriza = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.pnlDatosGenerales = New System.Windows.Forms.Panel()
        Me.CMBlblGastos = New System.Windows.Forms.Label()
        Me.CMBlblMuestraGastos = New System.Windows.Forms.Label()
        Me.CMBlblEntregasPaciales = New System.Windows.Forms.Label()
        Me.CMBlblMuestraEntregasParciales = New System.Windows.Forms.Label()
        Me.CMBlblTotalCheques = New System.Windows.Forms.Label()
        Me.CMBlblMuestraTotalCheques = New System.Windows.Forms.Label()
        Me.CMBlblTotalTarjeta = New System.Windows.Forms.Label()
        Me.CMBlblMuestraTotalTarjeta = New System.Windows.Forms.Label()
        Me.CMBlblTotalEfectivo = New System.Windows.Forms.Label()
        Me.CMBlblMuestraTotalEfectivo = New System.Windows.Forms.Label()
        Me.CMBlblIdCorte = New System.Windows.Forms.Label()
        Me.CMBlblMuestraIdCorte = New System.Windows.Forms.Label()
        Me.btnSalir = New System.Windows.Forms.Button()
        Me.btnConsultar = New System.Windows.Forms.Button()
        Me.CMBlblBusqueda = New System.Windows.Forms.Label()
        Me.cmbBuscaStatus = New System.Windows.Forms.ComboBox()
        Me.CMBlblBuscaStatus = New System.Windows.Forms.Label()
        CType(Me.dgvCortesGlobales, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pnlDatosGenerales.SuspendLayout()
        Me.SuspendLayout()
        '
        'btnCancelar
        '
        Me.btnCancelar.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnCancelar.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnCancelar.Location = New System.Drawing.Point(1181, 66)
        Me.btnCancelar.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.btnCancelar.Name = "btnCancelar"
        Me.btnCancelar.Size = New System.Drawing.Size(163, 42)
        Me.btnCancelar.TabIndex = 39
        Me.btnCancelar.Text = "&Cancelar"
        Me.btnCancelar.UseVisualStyleBackColor = True
        '
        'cmbBuscaAutoriza
        '
        Me.cmbBuscaAutoriza.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmbBuscaAutoriza.FormattingEnabled = True
        Me.cmbBuscaAutoriza.Location = New System.Drawing.Point(15, 112)
        Me.cmbBuscaAutoriza.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.cmbBuscaAutoriza.Name = "cmbBuscaAutoriza"
        Me.cmbBuscaAutoriza.Size = New System.Drawing.Size(261, 28)
        Me.cmbBuscaAutoriza.TabIndex = 30
        '
        'CMBlblMuestraAutoriza
        '
        Me.CMBlblMuestraAutoriza.AutoSize = True
        Me.CMBlblMuestraAutoriza.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBlblMuestraAutoriza.Location = New System.Drawing.Point(12, 89)
        Me.CMBlblMuestraAutoriza.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.CMBlblMuestraAutoriza.Name = "CMBlblMuestraAutoriza"
        Me.CMBlblMuestraAutoriza.Size = New System.Drawing.Size(91, 20)
        Me.CMBlblMuestraAutoriza.TabIndex = 45
        Me.CMBlblMuestraAutoriza.Text = "Autoriza :"
        '
        'cmbBuscaCajera
        '
        Me.cmbBuscaCajera.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmbBuscaCajera.FormattingEnabled = True
        Me.cmbBuscaCajera.Location = New System.Drawing.Point(13, 188)
        Me.cmbBuscaCajera.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.cmbBuscaCajera.Name = "cmbBuscaCajera"
        Me.cmbBuscaCajera.Size = New System.Drawing.Size(263, 28)
        Me.cmbBuscaCajera.TabIndex = 34
        '
        'CMBlblBuscaCajera
        '
        Me.CMBlblBuscaCajera.AutoSize = True
        Me.CMBlblBuscaCajera.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBlblBuscaCajera.Location = New System.Drawing.Point(11, 165)
        Me.CMBlblBuscaCajera.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.CMBlblBuscaCajera.Name = "CMBlblBuscaCajera"
        Me.CMBlblBuscaCajera.Size = New System.Drawing.Size(76, 20)
        Me.CMBlblBuscaCajera.TabIndex = 44
        Me.CMBlblBuscaCajera.Text = "Cajera :"
        '
        'dgvCortesGlobales
        '
        Me.dgvCortesGlobales.AllowUserToAddRows = False
        Me.dgvCortesGlobales.AllowUserToDeleteRows = False
        Me.dgvCortesGlobales.BackgroundColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvCortesGlobales.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle1
        Me.dgvCortesGlobales.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvCortesGlobales.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.idCorte, Me.cajera, Me.fechaCorte, Me.fechaGeneracion, Me.totalEfectivo, Me.totalTarjeta, Me.totalCheques, Me.entregasParciales, Me.totalCobrado, Me.gastos, Me.status, Me.autoriza})
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgvCortesGlobales.DefaultCellStyle = DataGridViewCellStyle2
        Me.dgvCortesGlobales.Location = New System.Drawing.Point(285, 14)
        Me.dgvCortesGlobales.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.dgvCortesGlobales.Name = "dgvCortesGlobales"
        Me.dgvCortesGlobales.ReadOnly = True
        Me.dgvCortesGlobales.RowHeadersVisible = False
        Me.dgvCortesGlobales.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvCortesGlobales.Size = New System.Drawing.Size(888, 878)
        Me.dgvCortesGlobales.TabIndex = 35
        '
        'idCorte
        '
        Me.idCorte.DataPropertyName = "idCorte"
        Me.idCorte.HeaderText = "# Corte"
        Me.idCorte.Name = "idCorte"
        Me.idCorte.ReadOnly = True
        Me.idCorte.Width = 70
        '
        'cajera
        '
        Me.cajera.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.cajera.DataPropertyName = "cajera"
        Me.cajera.HeaderText = "Cajera"
        Me.cajera.Name = "cajera"
        Me.cajera.ReadOnly = True
        '
        'fechaCorte
        '
        Me.fechaCorte.DataPropertyName = "fechaCorte"
        Me.fechaCorte.HeaderText = "Fecha Corte"
        Me.fechaCorte.Name = "fechaCorte"
        Me.fechaCorte.ReadOnly = True
        '
        'fechaGeneracion
        '
        Me.fechaGeneracion.DataPropertyName = "fechaGeneracion"
        Me.fechaGeneracion.HeaderText = "Fecha Autorización"
        Me.fechaGeneracion.Name = "fechaGeneracion"
        Me.fechaGeneracion.ReadOnly = True
        '
        'totalEfectivo
        '
        Me.totalEfectivo.DataPropertyName = "totalEfectivo"
        Me.totalEfectivo.HeaderText = "Total Efectivo"
        Me.totalEfectivo.Name = "totalEfectivo"
        Me.totalEfectivo.ReadOnly = True
        Me.totalEfectivo.Visible = False
        '
        'totalTarjeta
        '
        Me.totalTarjeta.DataPropertyName = "totalTarjeta"
        Me.totalTarjeta.HeaderText = "Total Tarjeta"
        Me.totalTarjeta.Name = "totalTarjeta"
        Me.totalTarjeta.ReadOnly = True
        Me.totalTarjeta.Visible = False
        '
        'totalCheques
        '
        Me.totalCheques.DataPropertyName = "totalCheques"
        Me.totalCheques.HeaderText = "Total Cheques"
        Me.totalCheques.Name = "totalCheques"
        Me.totalCheques.ReadOnly = True
        Me.totalCheques.Visible = False
        '
        'entregasParciales
        '
        Me.entregasParciales.DataPropertyName = "entregasParciales"
        Me.entregasParciales.HeaderText = "Entregas Parciales"
        Me.entregasParciales.Name = "entregasParciales"
        Me.entregasParciales.ReadOnly = True
        Me.entregasParciales.Visible = False
        '
        'totalCobrado
        '
        Me.totalCobrado.DataPropertyName = "totalCobrado"
        Me.totalCobrado.HeaderText = "Total Cobrado"
        Me.totalCobrado.Name = "totalCobrado"
        Me.totalCobrado.ReadOnly = True
        '
        'gastos
        '
        Me.gastos.DataPropertyName = "gastos"
        Me.gastos.HeaderText = "Gastos"
        Me.gastos.Name = "gastos"
        Me.gastos.ReadOnly = True
        Me.gastos.Visible = False
        '
        'status
        '
        Me.status.DataPropertyName = "status"
        Me.status.HeaderText = "Status"
        Me.status.Name = "status"
        Me.status.ReadOnly = True
        Me.status.Width = 80
        '
        'autoriza
        '
        Me.autoriza.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.autoriza.DataPropertyName = "autoriza"
        Me.autoriza.HeaderText = "Autoriza"
        Me.autoriza.Name = "autoriza"
        Me.autoriza.ReadOnly = True
        '
        'pnlDatosGenerales
        '
        Me.pnlDatosGenerales.Controls.Add(Me.CMBlblGastos)
        Me.pnlDatosGenerales.Controls.Add(Me.CMBlblMuestraGastos)
        Me.pnlDatosGenerales.Controls.Add(Me.CMBlblEntregasPaciales)
        Me.pnlDatosGenerales.Controls.Add(Me.CMBlblMuestraEntregasParciales)
        Me.pnlDatosGenerales.Controls.Add(Me.CMBlblTotalCheques)
        Me.pnlDatosGenerales.Controls.Add(Me.CMBlblMuestraTotalCheques)
        Me.pnlDatosGenerales.Controls.Add(Me.CMBlblTotalTarjeta)
        Me.pnlDatosGenerales.Controls.Add(Me.CMBlblMuestraTotalTarjeta)
        Me.pnlDatosGenerales.Controls.Add(Me.CMBlblTotalEfectivo)
        Me.pnlDatosGenerales.Controls.Add(Me.CMBlblMuestraTotalEfectivo)
        Me.pnlDatosGenerales.Controls.Add(Me.CMBlblIdCorte)
        Me.pnlDatosGenerales.Controls.Add(Me.CMBlblMuestraIdCorte)
        Me.pnlDatosGenerales.Location = New System.Drawing.Point(17, 496)
        Me.pnlDatosGenerales.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.pnlDatosGenerales.Name = "pnlDatosGenerales"
        Me.pnlDatosGenerales.Size = New System.Drawing.Size(260, 395)
        Me.pnlDatosGenerales.TabIndex = 42
        '
        'CMBlblGastos
        '
        Me.CMBlblGastos.AutoSize = True
        Me.CMBlblGastos.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBlblGastos.Location = New System.Drawing.Point(11, 346)
        Me.CMBlblGastos.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.CMBlblGastos.Name = "CMBlblGastos"
        Me.CMBlblGastos.Size = New System.Drawing.Size(0, 20)
        Me.CMBlblGastos.TabIndex = 21
        '
        'CMBlblMuestraGastos
        '
        Me.CMBlblMuestraGastos.AutoSize = True
        Me.CMBlblMuestraGastos.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBlblMuestraGastos.Location = New System.Drawing.Point(5, 316)
        Me.CMBlblMuestraGastos.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.CMBlblMuestraGastos.Name = "CMBlblMuestraGastos"
        Me.CMBlblMuestraGastos.Size = New System.Drawing.Size(81, 20)
        Me.CMBlblMuestraGastos.TabIndex = 20
        Me.CMBlblMuestraGastos.Text = "Gastos :"
        '
        'CMBlblEntregasPaciales
        '
        Me.CMBlblEntregasPaciales.AutoSize = True
        Me.CMBlblEntregasPaciales.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBlblEntregasPaciales.Location = New System.Drawing.Point(11, 286)
        Me.CMBlblEntregasPaciales.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.CMBlblEntregasPaciales.Name = "CMBlblEntregasPaciales"
        Me.CMBlblEntregasPaciales.Size = New System.Drawing.Size(0, 20)
        Me.CMBlblEntregasPaciales.TabIndex = 19
        '
        'CMBlblMuestraEntregasParciales
        '
        Me.CMBlblMuestraEntregasParciales.AutoSize = True
        Me.CMBlblMuestraEntregasParciales.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBlblMuestraEntregasParciales.Location = New System.Drawing.Point(5, 256)
        Me.CMBlblMuestraEntregasParciales.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.CMBlblMuestraEntregasParciales.Name = "CMBlblMuestraEntregasParciales"
        Me.CMBlblMuestraEntregasParciales.Size = New System.Drawing.Size(181, 20)
        Me.CMBlblMuestraEntregasParciales.TabIndex = 18
        Me.CMBlblMuestraEntregasParciales.Text = "Entregas Parciales :"
        '
        'CMBlblTotalCheques
        '
        Me.CMBlblTotalCheques.AutoSize = True
        Me.CMBlblTotalCheques.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBlblTotalCheques.Location = New System.Drawing.Point(9, 223)
        Me.CMBlblTotalCheques.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.CMBlblTotalCheques.Name = "CMBlblTotalCheques"
        Me.CMBlblTotalCheques.Size = New System.Drawing.Size(0, 20)
        Me.CMBlblTotalCheques.TabIndex = 17
        '
        'CMBlblMuestraTotalCheques
        '
        Me.CMBlblMuestraTotalCheques.AutoSize = True
        Me.CMBlblMuestraTotalCheques.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBlblMuestraTotalCheques.Location = New System.Drawing.Point(4, 197)
        Me.CMBlblMuestraTotalCheques.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.CMBlblMuestraTotalCheques.Name = "CMBlblMuestraTotalCheques"
        Me.CMBlblMuestraTotalCheques.Size = New System.Drawing.Size(142, 20)
        Me.CMBlblMuestraTotalCheques.TabIndex = 16
        Me.CMBlblMuestraTotalCheques.Text = "Total Cheques :"
        '
        'CMBlblTotalTarjeta
        '
        Me.CMBlblTotalTarjeta.AutoSize = True
        Me.CMBlblTotalTarjeta.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBlblTotalTarjeta.Location = New System.Drawing.Point(8, 165)
        Me.CMBlblTotalTarjeta.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.CMBlblTotalTarjeta.Name = "CMBlblTotalTarjeta"
        Me.CMBlblTotalTarjeta.Size = New System.Drawing.Size(0, 20)
        Me.CMBlblTotalTarjeta.TabIndex = 15
        '
        'CMBlblMuestraTotalTarjeta
        '
        Me.CMBlblMuestraTotalTarjeta.AutoSize = True
        Me.CMBlblMuestraTotalTarjeta.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBlblMuestraTotalTarjeta.Location = New System.Drawing.Point(5, 138)
        Me.CMBlblMuestraTotalTarjeta.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.CMBlblMuestraTotalTarjeta.Name = "CMBlblMuestraTotalTarjeta"
        Me.CMBlblMuestraTotalTarjeta.Size = New System.Drawing.Size(128, 20)
        Me.CMBlblMuestraTotalTarjeta.TabIndex = 14
        Me.CMBlblMuestraTotalTarjeta.Text = "Total Tarjeta :"
        '
        'CMBlblTotalEfectivo
        '
        Me.CMBlblTotalEfectivo.AutoSize = True
        Me.CMBlblTotalEfectivo.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBlblTotalEfectivo.Location = New System.Drawing.Point(9, 108)
        Me.CMBlblTotalEfectivo.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.CMBlblTotalEfectivo.Name = "CMBlblTotalEfectivo"
        Me.CMBlblTotalEfectivo.Size = New System.Drawing.Size(0, 20)
        Me.CMBlblTotalEfectivo.TabIndex = 13
        '
        'CMBlblMuestraTotalEfectivo
        '
        Me.CMBlblMuestraTotalEfectivo.AutoSize = True
        Me.CMBlblMuestraTotalEfectivo.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBlblMuestraTotalEfectivo.Location = New System.Drawing.Point(4, 85)
        Me.CMBlblMuestraTotalEfectivo.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.CMBlblMuestraTotalEfectivo.Name = "CMBlblMuestraTotalEfectivo"
        Me.CMBlblMuestraTotalEfectivo.Size = New System.Drawing.Size(137, 20)
        Me.CMBlblMuestraTotalEfectivo.TabIndex = 12
        Me.CMBlblMuestraTotalEfectivo.Text = "Total Efectivo :"
        '
        'CMBlblIdCorte
        '
        Me.CMBlblIdCorte.AutoSize = True
        Me.CMBlblIdCorte.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBlblIdCorte.Location = New System.Drawing.Point(11, 52)
        Me.CMBlblIdCorte.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.CMBlblIdCorte.Name = "CMBlblIdCorte"
        Me.CMBlblIdCorte.Size = New System.Drawing.Size(0, 20)
        Me.CMBlblIdCorte.TabIndex = 10
        '
        'CMBlblMuestraIdCorte
        '
        Me.CMBlblMuestraIdCorte.AutoSize = True
        Me.CMBlblMuestraIdCorte.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBlblMuestraIdCorte.Location = New System.Drawing.Point(3, 17)
        Me.CMBlblMuestraIdCorte.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.CMBlblMuestraIdCorte.Name = "CMBlblMuestraIdCorte"
        Me.CMBlblMuestraIdCorte.Size = New System.Drawing.Size(83, 20)
        Me.CMBlblMuestraIdCorte.TabIndex = 9
        Me.CMBlblMuestraIdCorte.Text = "# Corte :"
        '
        'btnSalir
        '
        Me.btnSalir.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSalir.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSalir.Location = New System.Drawing.Point(1181, 849)
        Me.btnSalir.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.btnSalir.Name = "btnSalir"
        Me.btnSalir.Size = New System.Drawing.Size(160, 42)
        Me.btnSalir.TabIndex = 40
        Me.btnSalir.Text = "&Salir"
        Me.btnSalir.UseVisualStyleBackColor = True
        '
        'btnConsultar
        '
        Me.btnConsultar.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnConsultar.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnConsultar.Location = New System.Drawing.Point(1181, 14)
        Me.btnConsultar.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.btnConsultar.Name = "btnConsultar"
        Me.btnConsultar.Size = New System.Drawing.Size(163, 41)
        Me.btnConsultar.TabIndex = 37
        Me.btnConsultar.Text = "&Consultar"
        Me.btnConsultar.UseVisualStyleBackColor = True
        '
        'CMBlblBusqueda
        '
        Me.CMBlblBusqueda.AutoSize = True
        Me.CMBlblBusqueda.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBlblBusqueda.Location = New System.Drawing.Point(19, 25)
        Me.CMBlblBusqueda.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.CMBlblBusqueda.Name = "CMBlblBusqueda"
        Me.CMBlblBusqueda.Size = New System.Drawing.Size(155, 29)
        Me.CMBlblBusqueda.TabIndex = 41
        Me.CMBlblBusqueda.Text = "Buscar Por :"
        '
        'cmbBuscaStatus
        '
        Me.cmbBuscaStatus.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmbBuscaStatus.FormattingEnabled = True
        Me.cmbBuscaStatus.Location = New System.Drawing.Point(15, 263)
        Me.cmbBuscaStatus.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.cmbBuscaStatus.Name = "cmbBuscaStatus"
        Me.cmbBuscaStatus.Size = New System.Drawing.Size(263, 28)
        Me.cmbBuscaStatus.TabIndex = 46
        '
        'CMBlblBuscaStatus
        '
        Me.CMBlblBuscaStatus.AutoSize = True
        Me.CMBlblBuscaStatus.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBlblBuscaStatus.Location = New System.Drawing.Point(12, 240)
        Me.CMBlblBuscaStatus.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.CMBlblBuscaStatus.Name = "CMBlblBuscaStatus"
        Me.CMBlblBuscaStatus.Size = New System.Drawing.Size(75, 20)
        Me.CMBlblBuscaStatus.TabIndex = 47
        Me.CMBlblBuscaStatus.Text = "Status :"
        '
        'BrwCancelacionArqueos
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.AutoScroll = True
        Me.ClientSize = New System.Drawing.Size(1355, 903)
        Me.Controls.Add(Me.cmbBuscaStatus)
        Me.Controls.Add(Me.CMBlblBuscaStatus)
        Me.Controls.Add(Me.btnCancelar)
        Me.Controls.Add(Me.cmbBuscaAutoriza)
        Me.Controls.Add(Me.CMBlblMuestraAutoriza)
        Me.Controls.Add(Me.cmbBuscaCajera)
        Me.Controls.Add(Me.CMBlblBuscaCajera)
        Me.Controls.Add(Me.dgvCortesGlobales)
        Me.Controls.Add(Me.pnlDatosGenerales)
        Me.Controls.Add(Me.btnSalir)
        Me.Controls.Add(Me.btnConsultar)
        Me.Controls.Add(Me.CMBlblBusqueda)
        Me.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Name = "BrwCancelacionArqueos"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Cancelacion de Cortes Finales"
        CType(Me.dgvCortesGlobales, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pnlDatosGenerales.ResumeLayout(False)
        Me.pnlDatosGenerales.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents btnCancelar As System.Windows.Forms.Button
    Friend WithEvents cmbBuscaAutoriza As System.Windows.Forms.ComboBox
    Friend WithEvents CMBlblMuestraAutoriza As System.Windows.Forms.Label
    Friend WithEvents cmbBuscaCajera As System.Windows.Forms.ComboBox
    Friend WithEvents CMBlblBuscaCajera As System.Windows.Forms.Label
    Friend WithEvents dgvCortesGlobales As System.Windows.Forms.DataGridView
    Friend WithEvents pnlDatosGenerales As System.Windows.Forms.Panel
    Friend WithEvents CMBlblEntregasPaciales As System.Windows.Forms.Label
    Friend WithEvents CMBlblMuestraEntregasParciales As System.Windows.Forms.Label
    Friend WithEvents CMBlblTotalCheques As System.Windows.Forms.Label
    Friend WithEvents CMBlblMuestraTotalCheques As System.Windows.Forms.Label
    Friend WithEvents CMBlblTotalTarjeta As System.Windows.Forms.Label
    Friend WithEvents CMBlblMuestraTotalTarjeta As System.Windows.Forms.Label
    Friend WithEvents CMBlblTotalEfectivo As System.Windows.Forms.Label
    Friend WithEvents CMBlblMuestraTotalEfectivo As System.Windows.Forms.Label
    Friend WithEvents CMBlblIdCorte As System.Windows.Forms.Label
    Friend WithEvents CMBlblMuestraIdCorte As System.Windows.Forms.Label
    Friend WithEvents btnSalir As System.Windows.Forms.Button
    Friend WithEvents btnConsultar As System.Windows.Forms.Button
    Friend WithEvents CMBlblBusqueda As System.Windows.Forms.Label
    Friend WithEvents CMBlblGastos As System.Windows.Forms.Label
    Friend WithEvents CMBlblMuestraGastos As System.Windows.Forms.Label
    Friend WithEvents cmbBuscaStatus As System.Windows.Forms.ComboBox
    Friend WithEvents CMBlblBuscaStatus As System.Windows.Forms.Label
    Friend WithEvents idCorte As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents cajera As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents fechaCorte As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents fechaGeneracion As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents totalEfectivo As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents totalTarjeta As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents totalCheques As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents entregasParciales As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents totalCobrado As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents gastos As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents status As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents autoriza As System.Windows.Forms.DataGridViewTextBoxColumn
End Class
