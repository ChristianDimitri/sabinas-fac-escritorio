<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmMotivoCancelacionFactura
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim CMBDescripcionLabel As System.Windows.Forms.Label
        Dim Label1 As System.Windows.Forms.Label
        Dim Label2 As System.Windows.Forms.Label
        Me.DescripcionComboBox = New System.Windows.Forms.ComboBox()
        Me.MUESTRAMOTIVOSBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.DataSetEdgar = New softvFacturacion.DataSetEdgar()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.GuardaMotivosBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.GuardaMotivosTableAdapter = New softvFacturacion.DataSetEdgarTableAdapters.GuardaMotivosTableAdapter()
        Me.MUESTRAMOTIVOSTableAdapter = New softvFacturacion.DataSetEdgarTableAdapters.MUESTRAMOTIVOSTableAdapter()
        Me.Clv_MotivoLabel1 = New System.Windows.Forms.Label()
        Me.CMBLabel1 = New System.Windows.Forms.Label()
        Me.tbOtro = New System.Windows.Forms.TextBox()
        Me.tbRuta = New System.Windows.Forms.TextBox()
        Me.btnRuta = New System.Windows.Forms.Button()
        Me.btnLimpiar = New System.Windows.Forms.Button()
        Me.PictureBox1 = New System.Windows.Forms.PictureBox()
        CMBDescripcionLabel = New System.Windows.Forms.Label()
        Label1 = New System.Windows.Forms.Label()
        Label2 = New System.Windows.Forms.Label()
        CType(Me.MUESTRAMOTIVOSBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DataSetEdgar, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GuardaMotivosBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'CMBDescripcionLabel
        '
        CMBDescripcionLabel.AutoSize = True
        CMBDescripcionLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        CMBDescripcionLabel.Location = New System.Drawing.Point(9, 58)
        CMBDescripcionLabel.Name = "CMBDescripcionLabel"
        CMBDescripcionLabel.Size = New System.Drawing.Size(87, 15)
        CMBDescripcionLabel.TabIndex = 1
        CMBDescripcionLabel.Text = "Descripcion:"
        '
        'Label1
        '
        Label1.AutoSize = True
        Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label1.Location = New System.Drawing.Point(12, 166)
        Label1.Name = "Label1"
        Label1.Size = New System.Drawing.Size(73, 15)
        Label1.TabIndex = 8
        Label1.Text = "Evidencia:"
        Label1.Visible = False
        '
        'Label2
        '
        Label2.AutoSize = True
        Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label2.Location = New System.Drawing.Point(13, 188)
        Label2.Name = "Label2"
        Label2.Size = New System.Drawing.Size(36, 15)
        Label2.TabIndex = 10
        Label2.Text = "Ruta:"
        Label2.Visible = False
        '
        'DescripcionComboBox
        '
        Me.DescripcionComboBox.DataSource = Me.MUESTRAMOTIVOSBindingSource
        Me.DescripcionComboBox.DisplayMember = "Descripcion"
        Me.DescripcionComboBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.DescripcionComboBox.FormattingEnabled = True
        Me.DescripcionComboBox.Location = New System.Drawing.Point(12, 76)
        Me.DescripcionComboBox.Name = "DescripcionComboBox"
        Me.DescripcionComboBox.Size = New System.Drawing.Size(507, 23)
        Me.DescripcionComboBox.TabIndex = 0
        Me.DescripcionComboBox.ValueMember = "Clv_Motivo"
        '
        'MUESTRAMOTIVOSBindingSource
        '
        Me.MUESTRAMOTIVOSBindingSource.DataMember = "MUESTRAMOTIVOS"
        Me.MUESTRAMOTIVOSBindingSource.DataSource = Me.DataSetEdgar
        '
        'DataSetEdgar
        '
        Me.DataSetEdgar.DataSetName = "DataSetEdgar"
        Me.DataSetEdgar.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'Button1
        '
        Me.Button1.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button1.Location = New System.Drawing.Point(407, 219)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(112, 32)
        Me.Button1.TabIndex = 1
        Me.Button1.Text = "&ACEPTAR"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'GuardaMotivosBindingSource
        '
        Me.GuardaMotivosBindingSource.DataMember = "GuardaMotivos"
        Me.GuardaMotivosBindingSource.DataSource = Me.DataSetEdgar
        '
        'GuardaMotivosTableAdapter
        '
        Me.GuardaMotivosTableAdapter.ClearBeforeFill = True
        '
        'MUESTRAMOTIVOSTableAdapter
        '
        Me.MUESTRAMOTIVOSTableAdapter.ClearBeforeFill = True
        '
        'Clv_MotivoLabel1
        '
        Me.Clv_MotivoLabel1.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.MUESTRAMOTIVOSBindingSource, "Clv_Motivo", True))
        Me.Clv_MotivoLabel1.Location = New System.Drawing.Point(655, 332)
        Me.Clv_MotivoLabel1.Name = "Clv_MotivoLabel1"
        Me.Clv_MotivoLabel1.Size = New System.Drawing.Size(24, 16)
        Me.Clv_MotivoLabel1.TabIndex = 6
        '
        'CMBLabel1
        '
        Me.CMBLabel1.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBLabel1.Location = New System.Drawing.Point(94, 12)
        Me.CMBLabel1.Name = "CMBLabel1"
        Me.CMBLabel1.Size = New System.Drawing.Size(342, 31)
        Me.CMBLabel1.TabIndex = 4
        Me.CMBLabel1.Text = "¿Por qué Motivo se Cancelará el Ticket?"
        '
        'tbOtro
        '
        Me.tbOtro.Enabled = False
        Me.tbOtro.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold)
        Me.tbOtro.Location = New System.Drawing.Point(12, 105)
        Me.tbOtro.Multiline = True
        Me.tbOtro.Name = "tbOtro"
        Me.tbOtro.Size = New System.Drawing.Size(507, 53)
        Me.tbOtro.TabIndex = 7
        Me.tbOtro.Visible = False
        '
        'tbRuta
        '
        Me.tbRuta.Enabled = False
        Me.tbRuta.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold)
        Me.tbRuta.Location = New System.Drawing.Point(55, 185)
        Me.tbRuta.Name = "tbRuta"
        Me.tbRuta.Size = New System.Drawing.Size(430, 21)
        Me.tbRuta.TabIndex = 9
        Me.tbRuta.Visible = False
        '
        'btnRuta
        '
        Me.btnRuta.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnRuta.Location = New System.Drawing.Point(491, 185)
        Me.btnRuta.Name = "btnRuta"
        Me.btnRuta.Size = New System.Drawing.Size(28, 21)
        Me.btnRuta.TabIndex = 11
        Me.btnRuta.Text = "..."
        Me.btnRuta.UseVisualStyleBackColor = True
        Me.btnRuta.Visible = False
        '
        'btnLimpiar
        '
        Me.btnLimpiar.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnLimpiar.Location = New System.Drawing.Point(55, 212)
        Me.btnLimpiar.Name = "btnLimpiar"
        Me.btnLimpiar.Size = New System.Drawing.Size(63, 27)
        Me.btnLimpiar.TabIndex = 12
        Me.btnLimpiar.Text = "Limpiar"
        Me.btnLimpiar.UseVisualStyleBackColor = True
        Me.btnLimpiar.Visible = False
        '
        'PictureBox1
        '
        Me.PictureBox1.Location = New System.Drawing.Point(195, 212)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(100, 50)
        Me.PictureBox1.TabIndex = 13
        Me.PictureBox1.TabStop = False
        Me.PictureBox1.Visible = False
        '
        'FrmMotivoCancelacionFactura
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(540, 258)
        Me.Controls.Add(Me.PictureBox1)
        Me.Controls.Add(Me.btnLimpiar)
        Me.Controls.Add(Me.btnRuta)
        Me.Controls.Add(Label2)
        Me.Controls.Add(Me.tbRuta)
        Me.Controls.Add(Label1)
        Me.Controls.Add(Me.tbOtro)
        Me.Controls.Add(Me.CMBLabel1)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.DescripcionComboBox)
        Me.Controls.Add(Me.Clv_MotivoLabel1)
        Me.Controls.Add(CMBDescripcionLabel)
        Me.MaximizeBox = False
        Me.Name = "FrmMotivoCancelacionFactura"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Motivo de Cancelación"
        CType(Me.MUESTRAMOTIVOSBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DataSetEdgar, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GuardaMotivosBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents DescripcionComboBox As System.Windows.Forms.ComboBox
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents MUESTRAMOTIVOSBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents DataSetEdgar As softvFacturacion.DataSetEdgar
    Friend WithEvents GuardaMotivosBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents GuardaMotivosTableAdapter As softvFacturacion.DataSetEdgarTableAdapters.GuardaMotivosTableAdapter
    Friend WithEvents MUESTRAMOTIVOSTableAdapter As softvFacturacion.DataSetEdgarTableAdapters.MUESTRAMOTIVOSTableAdapter
    Friend WithEvents Clv_MotivoLabel1 As System.Windows.Forms.Label
    Friend WithEvents CMBLabel1 As System.Windows.Forms.Label
    Friend WithEvents tbOtro As System.Windows.Forms.TextBox
    Friend WithEvents tbRuta As System.Windows.Forms.TextBox
    Friend WithEvents btnRuta As System.Windows.Forms.Button
    Friend WithEvents btnLimpiar As System.Windows.Forms.Button
    Friend WithEvents PictureBox1 As System.Windows.Forms.PictureBox
End Class
