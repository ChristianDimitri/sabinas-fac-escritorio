﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmMotivoCancelacionFacturaAlmacen
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim CMBDescripcionLabel As System.Windows.Forms.Label
        Me.CMBLabel1 = New System.Windows.Forms.Label()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.DescripcionComboBox = New System.Windows.Forms.ComboBox()
        CMBDescripcionLabel = New System.Windows.Forms.Label()
        Me.SuspendLayout()
        '
        'CMBDescripcionLabel
        '
        CMBDescripcionLabel.AutoSize = True
        CMBDescripcionLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        CMBDescripcionLabel.Location = New System.Drawing.Point(15, 56)
        CMBDescripcionLabel.Name = "CMBDescripcionLabel"
        CMBDescripcionLabel.Size = New System.Drawing.Size(87, 15)
        CMBDescripcionLabel.TabIndex = 7
        CMBDescripcionLabel.Text = "Descripcion:"
        '
        'CMBLabel1
        '
        Me.CMBLabel1.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBLabel1.Location = New System.Drawing.Point(100, 10)
        Me.CMBLabel1.Name = "CMBLabel1"
        Me.CMBLabel1.Size = New System.Drawing.Size(342, 31)
        Me.CMBLabel1.TabIndex = 8
        Me.CMBLabel1.Text = "¿Por qué Motivo se Cancelará el Ticket?"
        '
        'Button1
        '
        Me.Button1.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button1.Location = New System.Drawing.Point(413, 217)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(112, 32)
        Me.Button1.TabIndex = 6
        Me.Button1.Text = "&ACEPTAR"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'DescripcionComboBox
        '
        Me.DescripcionComboBox.DisplayMember = "Descripcion"
        Me.DescripcionComboBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.DescripcionComboBox.FormattingEnabled = True
        Me.DescripcionComboBox.Location = New System.Drawing.Point(18, 74)
        Me.DescripcionComboBox.Name = "DescripcionComboBox"
        Me.DescripcionComboBox.Size = New System.Drawing.Size(507, 23)
        Me.DescripcionComboBox.TabIndex = 5
        Me.DescripcionComboBox.ValueMember = "Clv_Motivo"
        '
        'FrmMotivoCancelacionFacturaAlmacen
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(540, 258)
        Me.Controls.Add(Me.CMBLabel1)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.DescripcionComboBox)
        Me.Controls.Add(CMBDescripcionLabel)
        Me.Name = "FrmMotivoCancelacionFacturaAlmacen"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Motivo de Cancelación"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents CMBLabel1 As System.Windows.Forms.Label
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents DescripcionComboBox As System.Windows.Forms.ComboBox
End Class
