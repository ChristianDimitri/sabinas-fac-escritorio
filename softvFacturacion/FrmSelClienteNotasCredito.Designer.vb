﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmSelClienteNotasCredito
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim ServicioLabel As System.Windows.Forms.Label
        Dim ESHOTELLabel1 As System.Windows.Forms.Label
        Dim SOLOINTERNETLabel1 As System.Windows.Forms.Label
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle4 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle5 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle6 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle7 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle8 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle9 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle10 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle11 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.Button5 = New System.Windows.Forms.Button()
        Me.Button2 = New System.Windows.Forms.Button()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.SplitContainer1 = New System.Windows.Forms.SplitContainer()
        Me.cmbColonias = New System.Windows.Forms.ComboBox()
        Me.lblColonias = New System.Windows.Forms.Label()
        Me.AMaternoTextBox = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.APaternoTextBox = New System.Windows.Forms.TextBox()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.Button7 = New System.Windows.Forms.Button()
        Me.BNOMBRE = New System.Windows.Forms.TextBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.ContratoCompania = New System.Windows.Forms.Label()
        Me.TreeView1 = New System.Windows.Forms.TreeView()
        Me.ESHOTELCheckBox = New System.Windows.Forms.CheckBox()
        Me.SOLOINTERNETCheckBox = New System.Windows.Forms.CheckBox()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.CONTRATOLabel1 = New System.Windows.Forms.Label()
        Me.NOMBRELabel1 = New System.Windows.Forms.Label()
        Me.CALLELabel1 = New System.Windows.Forms.Label()
        Me.COLONIALabel1 = New System.Windows.Forms.Label()
        Me.NUMEROLabel1 = New System.Windows.Forms.Label()
        Me.CIUDADLabel1 = New System.Windows.Forms.Label()
        Me.Button8 = New System.Windows.Forms.Button()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.BCIUDAD = New System.Windows.Forms.TextBox()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.BNUMERO = New System.Windows.Forms.TextBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.BCALLE = New System.Windows.Forms.TextBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.bcONTRATO = New System.Windows.Forms.TextBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.CMBLabel1 = New System.Windows.Forms.Label()
        Me.DataGridView1 = New System.Windows.Forms.DataGridView()
        Me.CONTRATODataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.NOMBREDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Apellido_Paterno = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Apellido_Materno = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.CALLEDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.COLONIADataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.NUMERODataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.NUMEROINT = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.CIUDADDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.SOLOINTERNETDataGridViewCheckBoxColumn = New System.Windows.Forms.DataGridViewCheckBoxColumn()
        Me.ESHOTELDataGridViewCheckBoxColumn = New System.Windows.Forms.DataGridViewCheckBoxColumn()
        Me.BUSCLIPORCONTRATO2BindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.NewsoftvDataSet = New softvFacturacion.NewsoftvDataSet()
        Me.DameSerDELCliFACBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.DameSerDELCliFACTableAdapter = New softvFacturacion.NewsoftvDataSetTableAdapters.DameSerDELCliFACTableAdapter()
        Me.NewsoftvDataSet1 = New softvFacturacion.NewsoftvDataSet1()
        Me.MUESTRACALLESBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.MUESTRACALLESTableAdapter = New softvFacturacion.NewsoftvDataSet1TableAdapters.MUESTRACALLESTableAdapter()
        Me.BUSCLIPORCONTRATO2TableAdapter = New softvFacturacion.NewsoftvDataSetTableAdapters.BUSCLIPORCONTRATO2TableAdapter()
        Me.VerAcceso2TableAdapter1 = New softvFacturacion.ProcedimientosArnoldo3TableAdapters.VerAcceso2TableAdapter()
        Me.VerAcceso2TableAdapter2 = New softvFacturacion.ProcedimientosArnoldo3TableAdapters.VerAcceso2TableAdapter()
        ServicioLabel = New System.Windows.Forms.Label()
        ESHOTELLabel1 = New System.Windows.Forms.Label()
        SOLOINTERNETLabel1 = New System.Windows.Forms.Label()
        Me.Panel1.SuspendLayout()
        Me.SplitContainer1.Panel1.SuspendLayout()
        Me.SplitContainer1.Panel2.SuspendLayout()
        Me.SplitContainer1.SuspendLayout()
        Me.GroupBox1.SuspendLayout()
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.BUSCLIPORCONTRATO2BindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.NewsoftvDataSet, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DameSerDELCliFACBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.NewsoftvDataSet1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.MUESTRACALLESBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'ServicioLabel
        '
        ServicioLabel.AutoSize = True
        ServicioLabel.Location = New System.Drawing.Point(8, 191)
        ServicioLabel.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        ServicioLabel.Name = "ServicioLabel"
        ServicioLabel.Size = New System.Drawing.Size(182, 20)
        ServicioLabel.TabIndex = 18
        ServicioLabel.Text = "Servicio Asignados :"
        '
        'ESHOTELLabel1
        '
        ESHOTELLabel1.AutoSize = True
        ESHOTELLabel1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        ESHOTELLabel1.ForeColor = System.Drawing.Color.White
        ESHOTELLabel1.Location = New System.Drawing.Point(191, 170)
        ESHOTELLabel1.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        ESHOTELLabel1.Name = "ESHOTELLabel1"
        ESHOTELLabel1.Size = New System.Drawing.Size(80, 18)
        ESHOTELLabel1.TabIndex = 17
        ESHOTELLabel1.Text = "Es hotel :"
        '
        'SOLOINTERNETLabel1
        '
        SOLOINTERNETLabel1.AutoSize = True
        SOLOINTERNETLabel1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        SOLOINTERNETLabel1.ForeColor = System.Drawing.Color.White
        SOLOINTERNETLabel1.Location = New System.Drawing.Point(4, 170)
        SOLOINTERNETLabel1.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        SOLOINTERNETLabel1.Name = "SOLOINTERNETLabel1"
        SOLOINTERNETLabel1.Size = New System.Drawing.Size(114, 18)
        SOLOINTERNETLabel1.TabIndex = 16
        SOLOINTERNETLabel1.Text = "Solo Internet :"
        '
        'Button5
        '
        Me.Button5.BackColor = System.Drawing.Color.DarkOrange
        Me.Button5.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button5.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button5.ForeColor = System.Drawing.Color.Black
        Me.Button5.Location = New System.Drawing.Point(1157, 817)
        Me.Button5.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Button5.Name = "Button5"
        Me.Button5.Size = New System.Drawing.Size(181, 44)
        Me.Button5.TabIndex = 10
        Me.Button5.Text = "&CERRAR"
        Me.Button5.UseVisualStyleBackColor = False
        '
        'Button2
        '
        Me.Button2.BackColor = System.Drawing.Color.Orange
        Me.Button2.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button2.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button2.ForeColor = System.Drawing.Color.Black
        Me.Button2.Location = New System.Drawing.Point(1157, 15)
        Me.Button2.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(181, 44)
        Me.Button2.TabIndex = 9
        Me.Button2.Text = "&ACEPTAR"
        Me.Button2.UseVisualStyleBackColor = False
        '
        'Panel1
        '
        Me.Panel1.BackColor = System.Drawing.Color.WhiteSmoke
        Me.Panel1.Controls.Add(Me.SplitContainer1)
        Me.Panel1.Location = New System.Drawing.Point(16, 15)
        Me.Panel1.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(1119, 882)
        Me.Panel1.TabIndex = 11
        '
        'SplitContainer1
        '
        Me.SplitContainer1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SplitContainer1.Location = New System.Drawing.Point(0, 0)
        Me.SplitContainer1.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.SplitContainer1.Name = "SplitContainer1"
        '
        'SplitContainer1.Panel1
        '
        Me.SplitContainer1.Panel1.AutoScroll = True
        Me.SplitContainer1.Panel1.Controls.Add(Me.cmbColonias)
        Me.SplitContainer1.Panel1.Controls.Add(Me.lblColonias)
        Me.SplitContainer1.Panel1.Controls.Add(Me.AMaternoTextBox)
        Me.SplitContainer1.Panel1.Controls.Add(Me.Label1)
        Me.SplitContainer1.Panel1.Controls.Add(Me.APaternoTextBox)
        Me.SplitContainer1.Panel1.Controls.Add(Me.Label12)
        Me.SplitContainer1.Panel1.Controls.Add(Me.Button7)
        Me.SplitContainer1.Panel1.Controls.Add(Me.BNOMBRE)
        Me.SplitContainer1.Panel1.Controls.Add(Me.Label2)
        Me.SplitContainer1.Panel1.Controls.Add(Me.GroupBox1)
        Me.SplitContainer1.Panel1.Controls.Add(Me.Button8)
        Me.SplitContainer1.Panel1.Controls.Add(Me.Button1)
        Me.SplitContainer1.Panel1.Controls.Add(Me.BCIUDAD)
        Me.SplitContainer1.Panel1.Controls.Add(Me.Label6)
        Me.SplitContainer1.Panel1.Controls.Add(Me.BNUMERO)
        Me.SplitContainer1.Panel1.Controls.Add(Me.Label5)
        Me.SplitContainer1.Panel1.Controls.Add(Me.BCALLE)
        Me.SplitContainer1.Panel1.Controls.Add(Me.Label4)
        Me.SplitContainer1.Panel1.Controls.Add(Me.bcONTRATO)
        Me.SplitContainer1.Panel1.Controls.Add(Me.Label3)
        Me.SplitContainer1.Panel1.Controls.Add(Me.CMBLabel1)
        '
        'SplitContainer1.Panel2
        '
        Me.SplitContainer1.Panel2.Controls.Add(Me.DataGridView1)
        Me.SplitContainer1.Size = New System.Drawing.Size(1119, 882)
        Me.SplitContainer1.SplitterDistance = 372
        Me.SplitContainer1.SplitterWidth = 5
        Me.SplitContainer1.TabIndex = 0
        Me.SplitContainer1.TabStop = False
        '
        'cmbColonias
        '
        Me.cmbColonias.BackColor = System.Drawing.Color.LightGray
        Me.cmbColonias.DisplayMember = "nombre"
        Me.cmbColonias.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.cmbColonias.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmbColonias.FormattingEnabled = True
        Me.cmbColonias.Location = New System.Drawing.Point(13, 428)
        Me.cmbColonias.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.cmbColonias.Name = "cmbColonias"
        Me.cmbColonias.Size = New System.Drawing.Size(324, 28)
        Me.cmbColonias.TabIndex = 82
        Me.cmbColonias.ValueMember = "clvColonia"
        '
        'lblColonias
        '
        Me.lblColonias.AutoSize = True
        Me.lblColonias.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblColonias.Location = New System.Drawing.Point(17, 405)
        Me.lblColonias.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.lblColonias.Name = "lblColonias"
        Me.lblColonias.Size = New System.Drawing.Size(76, 18)
        Me.lblColonias.TabIndex = 83
        Me.lblColonias.Text = "Colonia :"
        '
        'AMaternoTextBox
        '
        Me.AMaternoTextBox.BackColor = System.Drawing.Color.LightGray
        Me.AMaternoTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.AMaternoTextBox.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.AMaternoTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.AMaternoTextBox.Location = New System.Drawing.Point(13, 230)
        Me.AMaternoTextBox.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.AMaternoTextBox.Name = "AMaternoTextBox"
        Me.AMaternoTextBox.Size = New System.Drawing.Size(333, 29)
        Me.AMaternoTextBox.TabIndex = 46
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(12, 206)
        Me.Label1.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(144, 18)
        Me.Label1.TabIndex = 48
        Me.Label1.Text = "Apellido Materno :"
        '
        'APaternoTextBox
        '
        Me.APaternoTextBox.BackColor = System.Drawing.Color.LightGray
        Me.APaternoTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.APaternoTextBox.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.APaternoTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.APaternoTextBox.Location = New System.Drawing.Point(13, 176)
        Me.APaternoTextBox.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.APaternoTextBox.Name = "APaternoTextBox"
        Me.APaternoTextBox.Size = New System.Drawing.Size(333, 29)
        Me.APaternoTextBox.TabIndex = 45
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label12.Location = New System.Drawing.Point(12, 150)
        Me.Label12.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(141, 18)
        Me.Label12.TabIndex = 47
        Me.Label12.Text = "Apellido Paterno :"
        '
        'Button7
        '
        Me.Button7.BackColor = System.Drawing.Color.DarkOrange
        Me.Button7.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button7.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button7.ForeColor = System.Drawing.Color.Black
        Me.Button7.Location = New System.Drawing.Point(228, 267)
        Me.Button7.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Button7.Name = "Button7"
        Me.Button7.Size = New System.Drawing.Size(117, 28)
        Me.Button7.TabIndex = 44
        Me.Button7.Text = "&Buscar"
        Me.Button7.UseVisualStyleBackColor = False
        '
        'BNOMBRE
        '
        Me.BNOMBRE.BackColor = System.Drawing.Color.LightGray
        Me.BNOMBRE.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.BNOMBRE.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.BNOMBRE.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BNOMBRE.Location = New System.Drawing.Point(12, 117)
        Me.BNOMBRE.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.BNOMBRE.Name = "BNOMBRE"
        Me.BNOMBRE.Size = New System.Drawing.Size(333, 28)
        Me.BNOMBRE.TabIndex = 43
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(12, 95)
        Me.Label2.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(78, 18)
        Me.Label2.TabIndex = 42
        Me.Label2.Text = "Nombre :"
        '
        'GroupBox1
        '
        Me.GroupBox1.BackColor = System.Drawing.Color.DarkOrange
        Me.GroupBox1.Controls.Add(Me.ContratoCompania)
        Me.GroupBox1.Controls.Add(Me.TreeView1)
        Me.GroupBox1.Controls.Add(ServicioLabel)
        Me.GroupBox1.Controls.Add(ESHOTELLabel1)
        Me.GroupBox1.Controls.Add(Me.ESHOTELCheckBox)
        Me.GroupBox1.Controls.Add(SOLOINTERNETLabel1)
        Me.GroupBox1.Controls.Add(Me.SOLOINTERNETCheckBox)
        Me.GroupBox1.Controls.Add(Me.Label11)
        Me.GroupBox1.Controls.Add(Me.Label10)
        Me.GroupBox1.Controls.Add(Me.Label9)
        Me.GroupBox1.Controls.Add(Me.Label8)
        Me.GroupBox1.Controls.Add(Me.Label7)
        Me.GroupBox1.Controls.Add(Me.CONTRATOLabel1)
        Me.GroupBox1.Controls.Add(Me.NOMBRELabel1)
        Me.GroupBox1.Controls.Add(Me.CALLELabel1)
        Me.GroupBox1.Controls.Add(Me.COLONIALabel1)
        Me.GroupBox1.Controls.Add(Me.NUMEROLabel1)
        Me.GroupBox1.Controls.Add(Me.CIUDADLabel1)
        Me.GroupBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox1.ForeColor = System.Drawing.Color.White
        Me.GroupBox1.Location = New System.Drawing.Point(4, 555)
        Me.GroupBox1.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Padding = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.GroupBox1.Size = New System.Drawing.Size(364, 487)
        Me.GroupBox1.TabIndex = 14
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Datos del Cliente"
        '
        'ContratoCompania
        '
        Me.ContratoCompania.BackColor = System.Drawing.Color.DarkOrange
        Me.ContratoCompania.Location = New System.Drawing.Point(100, 20)
        Me.ContratoCompania.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.ContratoCompania.Name = "ContratoCompania"
        Me.ContratoCompania.Size = New System.Drawing.Size(180, 28)
        Me.ContratoCompania.TabIndex = 21
        '
        'TreeView1
        '
        Me.TreeView1.BackColor = System.Drawing.Color.WhiteSmoke
        Me.TreeView1.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.TreeView1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TreeView1.ForeColor = System.Drawing.Color.Black
        Me.TreeView1.Location = New System.Drawing.Point(0, 214)
        Me.TreeView1.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.TreeView1.Name = "TreeView1"
        Me.TreeView1.Size = New System.Drawing.Size(364, 273)
        Me.TreeView1.TabIndex = 20
        Me.TreeView1.TabStop = False
        '
        'ESHOTELCheckBox
        '
        Me.ESHOTELCheckBox.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.ESHOTELCheckBox.Location = New System.Drawing.Point(288, 165)
        Me.ESHOTELCheckBox.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.ESHOTELCheckBox.Name = "ESHOTELCheckBox"
        Me.ESHOTELCheckBox.Size = New System.Drawing.Size(28, 30)
        Me.ESHOTELCheckBox.TabIndex = 18
        Me.ESHOTELCheckBox.TabStop = False
        '
        'SOLOINTERNETCheckBox
        '
        Me.SOLOINTERNETCheckBox.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.SOLOINTERNETCheckBox.Location = New System.Drawing.Point(141, 165)
        Me.SOLOINTERNETCheckBox.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.SOLOINTERNETCheckBox.Name = "SOLOINTERNETCheckBox"
        Me.SOLOINTERNETCheckBox.Size = New System.Drawing.Size(28, 30)
        Me.SOLOINTERNETCheckBox.TabIndex = 17
        Me.SOLOINTERNETCheckBox.TabStop = False
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label11.Location = New System.Drawing.Point(115, 142)
        Me.Label11.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(40, 17)
        Me.Label11.TabIndex = 16
        Me.Label11.Text = "Col. :"
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label10.Location = New System.Drawing.Point(16, 142)
        Me.Label10.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(24, 17)
        Me.Label10.TabIndex = 15
        Me.Label10.Text = "# :"
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label9.Location = New System.Drawing.Point(12, 92)
        Me.Label9.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(75, 17)
        Me.Label9.TabIndex = 14
        Me.Label9.Text = "Dirección :"
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label8.Location = New System.Drawing.Point(12, 43)
        Me.Label8.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(66, 17)
        Me.Label8.TabIndex = 13
        Me.Label8.Text = "Nombre :"
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7.Location = New System.Drawing.Point(8, 22)
        Me.Label7.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(82, 17)
        Me.Label7.TabIndex = 12
        Me.Label7.Text = "# Contrato :"
        '
        'CONTRATOLabel1
        '
        Me.CONTRATOLabel1.BackColor = System.Drawing.Color.DarkOrange
        Me.CONTRATOLabel1.Location = New System.Drawing.Point(100, 20)
        Me.CONTRATOLabel1.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.CONTRATOLabel1.Name = "CONTRATOLabel1"
        Me.CONTRATOLabel1.Size = New System.Drawing.Size(180, 28)
        Me.CONTRATOLabel1.TabIndex = 1
        '
        'NOMBRELabel1
        '
        Me.NOMBRELabel1.AutoEllipsis = True
        Me.NOMBRELabel1.BackColor = System.Drawing.Color.DarkOrange
        Me.NOMBRELabel1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.NOMBRELabel1.Location = New System.Drawing.Point(13, 64)
        Me.NOMBRELabel1.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.NOMBRELabel1.Name = "NOMBRELabel1"
        Me.NOMBRELabel1.Size = New System.Drawing.Size(303, 28)
        Me.NOMBRELabel1.TabIndex = 3
        '
        'CALLELabel1
        '
        Me.CALLELabel1.AutoEllipsis = True
        Me.CALLELabel1.BackColor = System.Drawing.Color.DarkOrange
        Me.CALLELabel1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CALLELabel1.Location = New System.Drawing.Point(16, 113)
        Me.CALLELabel1.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.CALLELabel1.Name = "CALLELabel1"
        Me.CALLELabel1.Size = New System.Drawing.Size(299, 28)
        Me.CALLELabel1.TabIndex = 5
        '
        'COLONIALabel1
        '
        Me.COLONIALabel1.AutoEllipsis = True
        Me.COLONIALabel1.BackColor = System.Drawing.Color.DarkOrange
        Me.COLONIALabel1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.COLONIALabel1.Location = New System.Drawing.Point(164, 142)
        Me.COLONIALabel1.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.COLONIALabel1.Name = "COLONIALabel1"
        Me.COLONIALabel1.Size = New System.Drawing.Size(151, 28)
        Me.COLONIALabel1.TabIndex = 7
        '
        'NUMEROLabel1
        '
        Me.NUMEROLabel1.AutoEllipsis = True
        Me.NUMEROLabel1.BackColor = System.Drawing.Color.DarkOrange
        Me.NUMEROLabel1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.NUMEROLabel1.Location = New System.Drawing.Point(44, 142)
        Me.NUMEROLabel1.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.NUMEROLabel1.Name = "NUMEROLabel1"
        Me.NUMEROLabel1.Size = New System.Drawing.Size(63, 28)
        Me.NUMEROLabel1.TabIndex = 9
        '
        'CIUDADLabel1
        '
        Me.CIUDADLabel1.AutoEllipsis = True
        Me.CIUDADLabel1.BackColor = System.Drawing.Color.DarkOrange
        Me.CIUDADLabel1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CIUDADLabel1.Location = New System.Drawing.Point(17, 191)
        Me.CIUDADLabel1.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.CIUDADLabel1.Name = "CIUDADLabel1"
        Me.CIUDADLabel1.Size = New System.Drawing.Size(299, 28)
        Me.CIUDADLabel1.TabIndex = 11
        '
        'Button8
        '
        Me.Button8.BackColor = System.Drawing.Color.DarkOrange
        Me.Button8.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button8.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button8.ForeColor = System.Drawing.Color.Black
        Me.Button8.Location = New System.Drawing.Point(229, 519)
        Me.Button8.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Button8.Name = "Button8"
        Me.Button8.Size = New System.Drawing.Size(117, 28)
        Me.Button8.TabIndex = 9
        Me.Button8.Text = "&Buscar"
        Me.Button8.UseVisualStyleBackColor = False
        '
        'Button1
        '
        Me.Button1.BackColor = System.Drawing.Color.DarkOrange
        Me.Button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button1.ForeColor = System.Drawing.Color.Black
        Me.Button1.Location = New System.Drawing.Point(189, 62)
        Me.Button1.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(117, 28)
        Me.Button1.TabIndex = 3
        Me.Button1.Text = "&Buscar"
        Me.Button1.UseVisualStyleBackColor = False
        '
        'BCIUDAD
        '
        Me.BCIUDAD.BackColor = System.Drawing.Color.LightGray
        Me.BCIUDAD.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.BCIUDAD.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.BCIUDAD.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BCIUDAD.Location = New System.Drawing.Point(16, 485)
        Me.BCIUDAD.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.BCIUDAD.Name = "BCIUDAD"
        Me.BCIUDAD.Size = New System.Drawing.Size(333, 28)
        Me.BCIUDAD.TabIndex = 8
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.Location = New System.Drawing.Point(17, 463)
        Me.Label6.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(70, 18)
        Me.Label6.TabIndex = 9
        Me.Label6.Text = "Ciudad :"
        '
        'BNUMERO
        '
        Me.BNUMERO.BackColor = System.Drawing.Color.LightGray
        Me.BNUMERO.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.BNUMERO.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.BNUMERO.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BNUMERO.Location = New System.Drawing.Point(16, 369)
        Me.BNUMERO.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.BNUMERO.Name = "BNUMERO"
        Me.BNUMERO.Size = New System.Drawing.Size(333, 28)
        Me.BNUMERO.TabIndex = 7
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.Location = New System.Drawing.Point(17, 347)
        Me.Label5.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(78, 18)
        Me.Label5.TabIndex = 7
        Me.Label5.Text = "Numero :"
        '
        'BCALLE
        '
        Me.BCALLE.BackColor = System.Drawing.Color.LightGray
        Me.BCALLE.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.BCALLE.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.BCALLE.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BCALLE.Location = New System.Drawing.Point(16, 314)
        Me.BCALLE.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.BCALLE.Name = "BCALLE"
        Me.BCALLE.Size = New System.Drawing.Size(333, 28)
        Me.BCALLE.TabIndex = 6
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.Location = New System.Drawing.Point(17, 292)
        Me.Label4.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(56, 18)
        Me.Label4.TabIndex = 5
        Me.Label4.Text = "Calle :"
        '
        'bcONTRATO
        '
        Me.bcONTRATO.BackColor = System.Drawing.Color.LightGray
        Me.bcONTRATO.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.bcONTRATO.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.bcONTRATO.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.bcONTRATO.Location = New System.Drawing.Point(16, 62)
        Me.bcONTRATO.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.bcONTRATO.Name = "bcONTRATO"
        Me.bcONTRATO.Size = New System.Drawing.Size(165, 28)
        Me.bcONTRATO.TabIndex = 2
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(12, 36)
        Me.Label3.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(84, 18)
        Me.Label3.TabIndex = 3
        Me.Label3.Text = "Contrato :"
        '
        'CMBLabel1
        '
        Me.CMBLabel1.AutoSize = True
        Me.CMBLabel1.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBLabel1.ForeColor = System.Drawing.Color.FromArgb(CType(CType(128, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.CMBLabel1.Location = New System.Drawing.Point(7, 6)
        Me.CMBLabel1.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.CMBLabel1.Name = "CMBLabel1"
        Me.CMBLabel1.Size = New System.Drawing.Size(245, 29)
        Me.CMBLabel1.TabIndex = 0
        Me.CMBLabel1.Text = "Buscar Cliente Por :"
        '
        'DataGridView1
        '
        Me.DataGridView1.AllowUserToAddRows = False
        Me.DataGridView1.AllowUserToDeleteRows = False
        Me.DataGridView1.AllowUserToOrderColumns = True
        Me.DataGridView1.AutoGenerateColumns = False
        Me.DataGridView1.BackgroundColor = System.Drawing.Color.WhiteSmoke
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle1.BackColor = System.Drawing.Color.Chocolate
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle1.ForeColor = System.Drawing.Color.White
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.DataGridView1.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle1
        Me.DataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DataGridView1.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.CONTRATODataGridViewTextBoxColumn, Me.NOMBREDataGridViewTextBoxColumn, Me.Apellido_Paterno, Me.Apellido_Materno, Me.CALLEDataGridViewTextBoxColumn, Me.COLONIADataGridViewTextBoxColumn, Me.NUMERODataGridViewTextBoxColumn, Me.NUMEROINT, Me.CIUDADDataGridViewTextBoxColumn, Me.SOLOINTERNETDataGridViewCheckBoxColumn, Me.ESHOTELDataGridViewCheckBoxColumn})
        Me.DataGridView1.DataSource = Me.BUSCLIPORCONTRATO2BindingSource
        Me.DataGridView1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.DataGridView1.Location = New System.Drawing.Point(0, 0)
        Me.DataGridView1.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.DataGridView1.MultiSelect = False
        Me.DataGridView1.Name = "DataGridView1"
        Me.DataGridView1.ReadOnly = True
        Me.DataGridView1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.DataGridView1.Size = New System.Drawing.Size(742, 882)
        Me.DataGridView1.TabIndex = 1
        Me.DataGridView1.TabStop = False
        '
        'CONTRATODataGridViewTextBoxColumn
        '
        Me.CONTRATODataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells
        Me.CONTRATODataGridViewTextBoxColumn.DataPropertyName = "CONTRATO"
        DataGridViewCellStyle2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CONTRATODataGridViewTextBoxColumn.DefaultCellStyle = DataGridViewCellStyle2
        Me.CONTRATODataGridViewTextBoxColumn.HeaderText = "Contrato"
        Me.CONTRATODataGridViewTextBoxColumn.Name = "CONTRATODataGridViewTextBoxColumn"
        Me.CONTRATODataGridViewTextBoxColumn.ReadOnly = True
        Me.CONTRATODataGridViewTextBoxColumn.Width = 117
        '
        'NOMBREDataGridViewTextBoxColumn
        '
        Me.NOMBREDataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells
        Me.NOMBREDataGridViewTextBoxColumn.DataPropertyName = "NOMBRE"
        DataGridViewCellStyle3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.NOMBREDataGridViewTextBoxColumn.DefaultCellStyle = DataGridViewCellStyle3
        Me.NOMBREDataGridViewTextBoxColumn.HeaderText = "Nombre"
        Me.NOMBREDataGridViewTextBoxColumn.Name = "NOMBREDataGridViewTextBoxColumn"
        Me.NOMBREDataGridViewTextBoxColumn.ReadOnly = True
        Me.NOMBREDataGridViewTextBoxColumn.Width = 114
        '
        'Apellido_Paterno
        '
        Me.Apellido_Paterno.DataPropertyName = "Apellido_Paterno"
        DataGridViewCellStyle4.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold)
        Me.Apellido_Paterno.DefaultCellStyle = DataGridViewCellStyle4
        Me.Apellido_Paterno.HeaderText = "Apellido Paterno"
        Me.Apellido_Paterno.Name = "Apellido_Paterno"
        Me.Apellido_Paterno.ReadOnly = True
        '
        'Apellido_Materno
        '
        Me.Apellido_Materno.DataPropertyName = "Apellido_Materno"
        DataGridViewCellStyle5.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold)
        Me.Apellido_Materno.DefaultCellStyle = DataGridViewCellStyle5
        Me.Apellido_Materno.HeaderText = "Apellido Materno"
        Me.Apellido_Materno.Name = "Apellido_Materno"
        Me.Apellido_Materno.ReadOnly = True
        '
        'CALLEDataGridViewTextBoxColumn
        '
        Me.CALLEDataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells
        Me.CALLEDataGridViewTextBoxColumn.DataPropertyName = "CALLE"
        DataGridViewCellStyle6.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CALLEDataGridViewTextBoxColumn.DefaultCellStyle = DataGridViewCellStyle6
        Me.CALLEDataGridViewTextBoxColumn.HeaderText = "Calle"
        Me.CALLEDataGridViewTextBoxColumn.Name = "CALLEDataGridViewTextBoxColumn"
        Me.CALLEDataGridViewTextBoxColumn.ReadOnly = True
        Me.CALLEDataGridViewTextBoxColumn.Width = 86
        '
        'COLONIADataGridViewTextBoxColumn
        '
        Me.COLONIADataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells
        Me.COLONIADataGridViewTextBoxColumn.DataPropertyName = "COLONIA"
        DataGridViewCellStyle7.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.COLONIADataGridViewTextBoxColumn.DefaultCellStyle = DataGridViewCellStyle7
        Me.COLONIADataGridViewTextBoxColumn.HeaderText = "Colonia"
        Me.COLONIADataGridViewTextBoxColumn.Name = "COLONIADataGridViewTextBoxColumn"
        Me.COLONIADataGridViewTextBoxColumn.ReadOnly = True
        Me.COLONIADataGridViewTextBoxColumn.Width = 110
        '
        'NUMERODataGridViewTextBoxColumn
        '
        Me.NUMERODataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells
        Me.NUMERODataGridViewTextBoxColumn.DataPropertyName = "NUMERO"
        DataGridViewCellStyle8.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.NUMERODataGridViewTextBoxColumn.DefaultCellStyle = DataGridViewCellStyle8
        Me.NUMERODataGridViewTextBoxColumn.HeaderText = "Numero Ext"
        Me.NUMERODataGridViewTextBoxColumn.Name = "NUMERODataGridViewTextBoxColumn"
        Me.NUMERODataGridViewTextBoxColumn.ReadOnly = True
        Me.NUMERODataGridViewTextBoxColumn.Width = 137
        '
        'NUMEROINT
        '
        Me.NUMEROINT.DataPropertyName = "NUMEROINT"
        Me.NUMEROINT.HeaderText = "Número Int"
        Me.NUMEROINT.Name = "NUMEROINT"
        Me.NUMEROINT.ReadOnly = True
        '
        'CIUDADDataGridViewTextBoxColumn
        '
        Me.CIUDADDataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells
        Me.CIUDADDataGridViewTextBoxColumn.DataPropertyName = "CIUDAD"
        DataGridViewCellStyle9.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CIUDADDataGridViewTextBoxColumn.DefaultCellStyle = DataGridViewCellStyle9
        Me.CIUDADDataGridViewTextBoxColumn.HeaderText = "Ciudad"
        Me.CIUDADDataGridViewTextBoxColumn.Name = "CIUDADDataGridViewTextBoxColumn"
        Me.CIUDADDataGridViewTextBoxColumn.ReadOnly = True
        Me.CIUDADDataGridViewTextBoxColumn.Width = 105
        '
        'SOLOINTERNETDataGridViewCheckBoxColumn
        '
        Me.SOLOINTERNETDataGridViewCheckBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells
        Me.SOLOINTERNETDataGridViewCheckBoxColumn.DataPropertyName = "SOLOINTERNET"
        DataGridViewCellStyle10.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle10.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle10.NullValue = False
        Me.SOLOINTERNETDataGridViewCheckBoxColumn.DefaultCellStyle = DataGridViewCellStyle10
        Me.SOLOINTERNETDataGridViewCheckBoxColumn.HeaderText = "Solo Internet"
        Me.SOLOINTERNETDataGridViewCheckBoxColumn.Name = "SOLOINTERNETDataGridViewCheckBoxColumn"
        Me.SOLOINTERNETDataGridViewCheckBoxColumn.ReadOnly = True
        Me.SOLOINTERNETDataGridViewCheckBoxColumn.Width = 121
        '
        'ESHOTELDataGridViewCheckBoxColumn
        '
        Me.ESHOTELDataGridViewCheckBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells
        Me.ESHOTELDataGridViewCheckBoxColumn.DataPropertyName = "ESHOTEL"
        DataGridViewCellStyle11.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle11.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle11.NullValue = False
        Me.ESHOTELDataGridViewCheckBoxColumn.DefaultCellStyle = DataGridViewCellStyle11
        Me.ESHOTELDataGridViewCheckBoxColumn.HeaderText = "Es Hotel"
        Me.ESHOTELDataGridViewCheckBoxColumn.Name = "ESHOTELDataGridViewCheckBoxColumn"
        Me.ESHOTELDataGridViewCheckBoxColumn.ReadOnly = True
        Me.ESHOTELDataGridViewCheckBoxColumn.Width = 86
        '
        'BUSCLIPORCONTRATO2BindingSource
        '
        Me.BUSCLIPORCONTRATO2BindingSource.DataMember = "BUSCLIPORCONTRATO2"
        Me.BUSCLIPORCONTRATO2BindingSource.DataSource = Me.NewsoftvDataSet
        '
        'NewsoftvDataSet
        '
        Me.NewsoftvDataSet.DataSetName = "NewsoftvDataSet"
        Me.NewsoftvDataSet.EnforceConstraints = False
        Me.NewsoftvDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'DameSerDELCliFACBindingSource
        '
        Me.DameSerDELCliFACBindingSource.DataMember = "DameSerDELCliFAC"
        Me.DameSerDELCliFACBindingSource.DataSource = Me.NewsoftvDataSet
        '
        'DameSerDELCliFACTableAdapter
        '
        Me.DameSerDELCliFACTableAdapter.ClearBeforeFill = True
        '
        'NewsoftvDataSet1
        '
        Me.NewsoftvDataSet1.DataSetName = "NewsoftvDataSet1"
        Me.NewsoftvDataSet1.EnforceConstraints = False
        Me.NewsoftvDataSet1.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'MUESTRACALLESBindingSource
        '
        Me.MUESTRACALLESBindingSource.DataMember = "MUESTRACALLES"
        Me.MUESTRACALLESBindingSource.DataSource = Me.NewsoftvDataSet1
        '
        'MUESTRACALLESTableAdapter
        '
        Me.MUESTRACALLESTableAdapter.ClearBeforeFill = True
        '
        'BUSCLIPORCONTRATO2TableAdapter
        '
        Me.BUSCLIPORCONTRATO2TableAdapter.ClearBeforeFill = True
        '
        'VerAcceso2TableAdapter1
        '
        Me.VerAcceso2TableAdapter1.ClearBeforeFill = True
        '
        'VerAcceso2TableAdapter2
        '
        Me.VerAcceso2TableAdapter2.ClearBeforeFill = True
        '
        'FrmSelClienteNotasCredito
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.AutoScroll = True
        Me.ClientSize = New System.Drawing.Size(1355, 912)
        Me.Controls.Add(Me.Button5)
        Me.Controls.Add(Me.Button2)
        Me.Controls.Add(Me.Panel1)
        Me.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Name = "FrmSelClienteNotasCredito"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Seleccione el Cliente"
        Me.Panel1.ResumeLayout(False)
        Me.SplitContainer1.Panel1.ResumeLayout(False)
        Me.SplitContainer1.Panel1.PerformLayout()
        Me.SplitContainer1.Panel2.ResumeLayout(False)
        Me.SplitContainer1.ResumeLayout(False)
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.BUSCLIPORCONTRATO2BindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.NewsoftvDataSet, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DameSerDELCliFACBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.NewsoftvDataSet1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.MUESTRACALLESBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents Button5 As System.Windows.Forms.Button
    Friend WithEvents Button2 As System.Windows.Forms.Button
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents SplitContainer1 As System.Windows.Forms.SplitContainer
    Friend WithEvents cmbColonias As System.Windows.Forms.ComboBox
    Friend WithEvents lblColonias As System.Windows.Forms.Label
    Friend WithEvents AMaternoTextBox As System.Windows.Forms.TextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents APaternoTextBox As System.Windows.Forms.TextBox
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents Button7 As System.Windows.Forms.Button
    Friend WithEvents BNOMBRE As System.Windows.Forms.TextBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents ContratoCompania As System.Windows.Forms.Label
    Friend WithEvents TreeView1 As System.Windows.Forms.TreeView
    Friend WithEvents ESHOTELCheckBox As System.Windows.Forms.CheckBox
    Friend WithEvents SOLOINTERNETCheckBox As System.Windows.Forms.CheckBox
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents CONTRATOLabel1 As System.Windows.Forms.Label
    Friend WithEvents NOMBRELabel1 As System.Windows.Forms.Label
    Friend WithEvents CALLELabel1 As System.Windows.Forms.Label
    Friend WithEvents COLONIALabel1 As System.Windows.Forms.Label
    Friend WithEvents NUMEROLabel1 As System.Windows.Forms.Label
    Friend WithEvents CIUDADLabel1 As System.Windows.Forms.Label
    Friend WithEvents Button8 As System.Windows.Forms.Button
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents BCIUDAD As System.Windows.Forms.TextBox
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents BNUMERO As System.Windows.Forms.TextBox
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents BCALLE As System.Windows.Forms.TextBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents bcONTRATO As System.Windows.Forms.TextBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents CMBLabel1 As System.Windows.Forms.Label
    Friend WithEvents NewsoftvDataSet As softvFacturacion.NewsoftvDataSet
    Friend WithEvents DameSerDELCliFACBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents DameSerDELCliFACTableAdapter As softvFacturacion.NewsoftvDataSetTableAdapters.DameSerDELCliFACTableAdapter
    Friend WithEvents NewsoftvDataSet1 As softvFacturacion.NewsoftvDataSet1
    Friend WithEvents MUESTRACALLESBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents MUESTRACALLESTableAdapter As softvFacturacion.NewsoftvDataSet1TableAdapters.MUESTRACALLESTableAdapter
    Friend WithEvents BUSCLIPORCONTRATO2BindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents BUSCLIPORCONTRATO2TableAdapter As softvFacturacion.NewsoftvDataSetTableAdapters.BUSCLIPORCONTRATO2TableAdapter
    Friend WithEvents VerAcceso2TableAdapter1 As softvFacturacion.ProcedimientosArnoldo3TableAdapters.VerAcceso2TableAdapter
    Friend WithEvents VerAcceso2TableAdapter2 As softvFacturacion.ProcedimientosArnoldo3TableAdapters.VerAcceso2TableAdapter
    Friend WithEvents DataGridView1 As System.Windows.Forms.DataGridView
    Friend WithEvents CONTRATODataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents NOMBREDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Apellido_Paterno As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Apellido_Materno As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents CALLEDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents COLONIADataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents NUMERODataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents NUMEROINT As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents CIUDADDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents SOLOINTERNETDataGridViewCheckBoxColumn As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents ESHOTELDataGridViewCheckBoxColumn As System.Windows.Forms.DataGridViewCheckBoxColumn
End Class
