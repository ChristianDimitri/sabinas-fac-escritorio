﻿Imports System.Data.SqlClient
Imports System.Text
Public Class FrmRepPagosDifFac

    Private Sub MUESTRAMESESOP()
        Dim conexion As New SqlConnection(MiConexion)
        Dim strSQL As New StringBuilder("EXEC MUESTRAMESESOP 0")

        Dim dataAdapter As New SqlDataAdapter(strSQL.ToString, conexion)
        Dim dataTable As New DataTable
        Dim bindingSource As New BindingSource

        dataAdapter.Fill(dataTable)
        bindingSource.DataSource = dataTable

        cbMes.DataSource = bindingSource
    End Sub

    Private Sub MUESTRAANIOSOP()
        Dim conexion As New SqlConnection(MiConexion)
        Dim strSQL As New StringBuilder("EXEC MUESTRAANIOSOP 0")

        Dim dataAdapter As New SqlDataAdapter(strSQL.ToString, conexion)
        Dim dataTable As New DataTable
        Dim bindingSource As New BindingSource

        dataAdapter.Fill(dataTable)
        bindingSource.DataSource = dataTable

        cbAnio.DataSource = bindingSource

    End Sub



    Private Sub FrmRepPagosDifFac_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        MUESTRAMESESOP()
        MUESTRAANIOSOP()
    End Sub

    Private Sub btnAceptar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAceptar.Click
        ePeriodo1 = 0
        ePeriodo2 = 0
        eMes = 0
        eAnio = 0
        ePago1 = 0
        ePago2 = 0
        If cbPeriodo1.Checked = True Then ePeriodo1 = 1
        If cbPeriodo2.Checked = True Then ePeriodo2 = 2
        If IsNumeric(cbMes.SelectedValue) = True Then eMes = cbMes.SelectedValue
        If IsNumeric(cbAnio.SelectedValue) = True Then eAnio = cbAnio.SelectedValue
        If cbPago1.Checked = True Then ePago1 = 1
        If cbPago2.Checked = True Then ePago2 = 2

        If ePeriodo1 = 0 And ePeriodo2 = 0 Then
            MsgBox("Selecciona al menos un Periodo.", MsgBoxStyle.Exclamation)
            Exit Sub
        End If
        If eMes = 0 And eAnio = 0 Then
            MsgBox("Establece una fecha de último pago.", MsgBoxStyle.Exclamation)
            Exit Sub
        End If
        If ePago1 = 0 And ePago2 = 0 Then
            MsgBox("Selecciona al menos un número de Pago.", MsgBoxStyle.Exclamation)
            Exit Sub
        End If

        eTitulo = "Reporte de Clientes con Pagos Diferidos del "
        If ePeriodo1 = 1 Then eTitulo = eTitulo & "Primer "
        If ePeriodo2 = 2 Then eTitulo = eTitulo & "Segundo "
        eTitulo = eTitulo & "periodo, "
        eTitulo = eTitulo & cbMes.Text & " de " & cbAnio.Text & " como fecha de último pago, con el número de "
        If ePago1 = 1 Then eTitulo = eTitulo & "Pago 1 "
        If ePago2 = 2 Then eTitulo = eTitulo & "Pago 2 "

        GloReporte = 12
        FrmImprimirRepGral.Show()
        Me.Close()

    End Sub

    Private Sub btnCancelar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancelar.Click
        Me.Close()
    End Sub
End Class