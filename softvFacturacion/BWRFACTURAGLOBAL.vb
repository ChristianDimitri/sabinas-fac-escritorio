Imports CrystalDecisions.Shared
Imports CrystalDecisions.CrystalReports.Engine
Imports System.Data.SqlClient
Imports System.Text

Public Class BWRFACTURAGLOBAL
    Dim Letra2 As String = Nothing
    Dim Importe As String = Nothing
    Private eCont As Integer = 1
    Private eRes As Integer = 0
    Private customersByCityReport As ReportDocument
    Dim SubTotal, Ieps, Iva, Total As Double

    Private Sub CancelaFacturaDigitalGlobal(ByVal LocClv_FacturaGlobal As Integer)
        FacturacionDigitalSoftv.ClassCFDI.MiConexion = MiConexion
        Dim identi As Integer = 0
        FacturacionDigitalSoftv.ClassCFDI.Locop = 1
        FacturacionDigitalSoftv.ClassCFDI.DameId_FacturaCDF(LocClv_FacturaGlobal, "G", MiConexion)
        Try
            If FacturacionDigitalSoftv.ClassCFDI.GloClv_FacturaCFD > 0 Then
                FacturacionDigitalSoftv.ClassCFDI.Cancelacion_FacturaCFD(FacturacionDigitalSoftv.ClassCFDI.GloClv_FacturaCFD, MiConexion)
            Else
                MsgBox("No se genero la factura digital cancele y vuelva a intentar por favor")
            End If
            'FormPruebaDigital.Show()
        Catch ex As Exception
        End Try
        FacturacionDigitalSoftv.ClassCFDI.locID_Compania_Mizart = ""
        FacturacionDigitalSoftv.ClassCFDI.locID_Sucursal_Mizart = ""
    End Sub

    Private Sub ImprimeFacturaDigitalGlobal(ByVal LocClv_FacturaGlobal As Integer)
        FacturacionDigitalSoftv.ClassCFDI.MiConexion = MiConexion
        Dim identi As Integer = 0
        FacturacionDigitalSoftv.ClassCFDI.Locop = 1
        FacturacionDigitalSoftv.ClassCFDI.DameId_FacturaCDF(LocClv_FacturaGlobal, "G", MiConexion)
        Try
            If FacturacionDigitalSoftv.ClassCFDI.GloClv_FacturaCFD > 0 Then

                Dim frm As New FacturacionDigitalSoftv.FrmImprimir
                frm.ShowDialog()
                Try
                    FacturacionDigitalSoftv.ClassCFDI.MiConexion = MiConexion
                    FacturacionDigitalSoftv.ClassCFDI.Locop = 1
                    FacturacionDigitalSoftv.ClassCFDI.DameId_FacturaCDF(LocClv_FacturaGlobal, "G", MiConexion)
                    FacturacionDigitalSoftv.ClassCFDI.ENVIAR_Factura(FacturacionDigitalSoftv.ClassCFDI.GloClv_FacturaCFD)
                Catch ex As Exception

                End Try

            Else
                MsgBox("No se genero la factura digital cancele y vuelva a intentar por favor")
            End If
            'FormPruebaDigital.Show()
        Catch ex As Exception
        End Try
        FacturacionDigitalSoftv.ClassCFDI.locID_Compania_Mizart = ""
        FacturacionDigitalSoftv.ClassCFDI.locID_Sucursal_Mizart = ""
    End Sub

    Private Sub Llena_companias()
        Try
            BaseII.limpiaParametros()
            'BaseII.CreateMyParameter("@Op", SqlDbType.Int, 3)
            'BaseII.CreateMyParameter("@ClvCompania", SqlDbType.Int, 0)
            GloIdDistribuidor = 0
            'ComboBoxCompanias.DataSource = BaseII.ConsultaDT("Muestra_Compania")
            BaseII.CreateMyParameter("@ClvUsuario", SqlDbType.Int, GloClvUsuario)
            ComboBoxCompanias.DataSource = BaseII.ConsultaDT("Muestra_Distribuidor_RelUsuario")
            ComboBoxCompanias.DisplayMember = "razon_social"
            ComboBoxCompanias.ValueMember = "id_compania"

            If ComboBoxCompanias.Items.Count > 0 Then
                'ComboBoxCompanias.SelectedValue = 0
                GloIdDistribuidor = ComboBoxCompanias.SelectedValue
            End If

            'ComboBoxCiudades.Text = ""
        Catch ex As Exception

        End Try
    End Sub
    Private Sub busqueda(ByVal op As Integer)
        Try
            Dim CON As New SqlConnection(MiConexion)
            CON.Open()
            BaseII.limpiaParametros()
            Select Case op '@Serie varchar(10), @factura varchar(50), @fecha DateTime, @sucursal  varchar(150),@Op int,@tipo varchar(1)
                Case 0
                    'Me.BuscaFacturasGlobalesTableAdapter.Connection = CON
                    'Me.BuscaFacturasGlobalesTableAdapter.Fill(Me.DataSetLydia.BuscaFacturasGlobales, Me.TextBox1.Text, Me.TextBox4.Text, "01/01/1900", "", op, bec_tipo)
                    BaseII.CreateMyParameter("@Serie", SqlDbType.VarChar, Me.TextBox1.Text)
                    BaseII.CreateMyParameter("@factura", SqlDbType.VarChar, Me.TextBox4.Text)
                    BaseII.CreateMyParameter("@fecha", SqlDbType.DateTime, "01/01/1900")
                    BaseII.CreateMyParameter("@sucursal", SqlDbType.VarChar, "")
                    BaseII.CreateMyParameter("@Op", SqlDbType.Int, op)
                    BaseII.CreateMyParameter("@tipo", SqlDbType.VarChar, bec_tipo)
                    BaseII.CreateMyParameter("@idDistribuidor", SqlDbType.Int, ComboBoxCompanias.SelectedValue)
                    BaseII.CreateMyParameter("@ClvUsuario", SqlDbType.Int, GloClvUsuario)
                Case 1
                    'Me.BuscaFacturasGlobalesTableAdapter.Connection = CON
                    'Me.BuscaFacturasGlobalesTableAdapter.Fill(Me.DataSetLydia.BuscaFacturasGlobales, "", "", Me.TextBox2.Text, "", op, bec_tipo)
                    BaseII.CreateMyParameter("@Serie", SqlDbType.VarChar, "")
                    BaseII.CreateMyParameter("@factura", SqlDbType.VarChar, "")
                    BaseII.CreateMyParameter("@fecha", SqlDbType.DateTime, Me.TextBox2.Text)
                    BaseII.CreateMyParameter("@sucursal", SqlDbType.VarChar, "")
                    BaseII.CreateMyParameter("@Op", SqlDbType.Int, op)
                    BaseII.CreateMyParameter("@tipo", SqlDbType.VarChar, bec_tipo)
                    BaseII.CreateMyParameter("@idDistribuidor", SqlDbType.Int, ComboBoxCompanias.SelectedValue)
                    BaseII.CreateMyParameter("@ClvUsuario", SqlDbType.Int, GloClvUsuario)
                Case 2
                    'Me.BuscaFacturasGlobalesTableAdapter.Connection = CON
                    'Me.BuscaFacturasGlobalesTableAdapter.Fill(Me.DataSetLydia.BuscaFacturasGlobales, "", "", "01/01/1900", Me.TextBox3.Text, op, bec_tipo)
                    BaseII.CreateMyParameter("@Serie", SqlDbType.VarChar, "")
                    BaseII.CreateMyParameter("@factura", SqlDbType.VarChar, "")
                    BaseII.CreateMyParameter("@fecha", SqlDbType.DateTime, "01/01/1900")
                    BaseII.CreateMyParameter("@sucursal", SqlDbType.VarChar, Me.TextBox3.Text)
                    BaseII.CreateMyParameter("@Op", SqlDbType.Int, op)
                    BaseII.CreateMyParameter("@tipo", SqlDbType.VarChar, bec_tipo)
                    BaseII.CreateMyParameter("@idDistribuidor", SqlDbType.Int, ComboBoxCompanias.SelectedValue)
                    BaseII.CreateMyParameter("@ClvUsuario", SqlDbType.Int, GloClvUsuario)
                Case 3
                    'Me.BuscaFacturasGlobalesTableAdapter.Connection = CON
                    'Me.BuscaFacturasGlobalesTableAdapter.Fill(Me.DataSetLydia.BuscaFacturasGlobales, "", "", "01/01/1900", "", op, bec_tipo)
                    BaseII.CreateMyParameter("@Serie", SqlDbType.VarChar, "")
                    BaseII.CreateMyParameter("@factura", SqlDbType.VarChar, "")
                    BaseII.CreateMyParameter("@fecha", SqlDbType.DateTime, "01/01/1900")
                    BaseII.CreateMyParameter("@sucursal", SqlDbType.VarChar, "")
                    BaseII.CreateMyParameter("@Op", SqlDbType.Int, op)
                    BaseII.CreateMyParameter("@tipo", SqlDbType.VarChar, bec_tipo)
                    BaseII.CreateMyParameter("@idDistribuidor", SqlDbType.Int, ComboBoxCompanias.SelectedValue)
                    BaseII.CreateMyParameter("@ClvUsuario", SqlDbType.Int, GloClvUsuario)
                Case 4
                    BaseII.CreateMyParameter("@Serie", SqlDbType.VarChar, "")
                    BaseII.CreateMyParameter("@factura", SqlDbType.VarChar, "")
                    BaseII.CreateMyParameter("@fecha", SqlDbType.DateTime, "01/01/1900")
                    BaseII.CreateMyParameter("@sucursal", SqlDbType.VarChar, "")
                    BaseII.CreateMyParameter("@Op", SqlDbType.Int, op)
                    BaseII.CreateMyParameter("@tipo", SqlDbType.VarChar, bec_tipo)
                    BaseII.CreateMyParameter("@idDistribuidor", SqlDbType.Int, ComboBoxCompanias.SelectedValue)
                    BaseII.CreateMyParameter("@ClvUsuario", SqlDbType.Int, GloClvUsuario)
            End Select
            Dim DT As DataTable = BaseII.ConsultaDT("BuscaFacturasGlobales")
            BuscaFacturasGlobalesDataGridView.DataSource = DT
            CON.Close()
            If BuscaFacturasGlobalesDataGridView.Rows.Count > 0 Then
                'CMBSerieTextBox.Text = BuscaFacturasGlobalesDataGridView.SelectedCells(0).Value
                'FacturaLabel1.Text = BuscaFacturasGlobalesDataGridView.SelectedCells(1).Value
                CMBIdFacturaTextBox.Text = BuscaFacturasGlobalesDataGridView.SelectedCells(0).Value
                CMBFechaTextBox.Text = BuscaFacturasGlobalesDataGridView.SelectedCells(1).Value
                CMBImporteTextBox.Text = BuscaFacturasGlobalesDataGridView.SelectedCells(2).Value
                CMBDistribuidorTextBox.Text = BuscaFacturasGlobalesDataGridView.SelectedCells(3).Value
                CMBCajeraTextBox.Text = BuscaFacturasGlobalesDataGridView.SelectedCells(4).Value
                CMBCanceladaTextBox.Text = BuscaFacturasGlobalesDataGridView.SelectedCells(5).Value
            End If
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try

    End Sub


    Private Sub BWRFACTURAGLOBAL_Activated(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Activated
        If bec_bandera = 1 Then
            bec_bandera = 0
            busqueda(4)
        End If
    End Sub
    Private Sub CantidadaLetra()
        Try
            Dim CON As New SqlConnection(MiConexion)
            CON.Open()
            Me.DameCantidadALetraTableAdapter.Connection = CON
            Me.DameCantidadALetraTableAdapter.Fill(Me.NewsoftvDataSet2.DameCantidadALetra, CDec(Importe), 0, Letra2)
            CON.Close()
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub BWRFACTURAGLOBAL_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        colorea(Me)
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        Llena_companias()
        'TODO: esta l�nea de c�digo carga datos en la tabla 'NewsoftvDataSet2.DameFechadelServidorHora' Puede moverla o quitarla seg�n sea necesario.
        Me.DameFechadelServidorHoraTableAdapter.Connection = CON
        Me.DameFechadelServidorHoraTableAdapter.Fill(Me.NewsoftvDataSet2.DameFechadelServidorHora)
        ''TODO: esta l�nea de c�digo carga datos en la tabla 'NewsoftvDataSet2.MUESTRATIPOFACTGLO' Puede moverla o quitarla seg�n sea necesario.
        Me.MUESTRATIPOFACTGLOTableAdapter.Connection = CON
        Me.MUESTRATIPOFACTGLOTableAdapter.Fill(Me.NewsoftvDataSet2.MUESTRATIPOFACTGLO)
        CON.Close()
        bec_tipo = Me.ComboBox2.SelectedValue
        busqueda(4)
        UspGuardaFormularios(Me.Name, Me.Text)
        UspGuardaBotonesFormularioSiste(Me, Me.Name)
        UspDesactivaBotones(Me, Me.Name)
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        busqueda(0)
    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        'Me.TextBox2.Text = String.Format("dd/mm/yyyy")
        busqueda(1)
    End Sub
    Private Sub TextBox1_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles TextBox1.KeyPress
        If Asc(e.KeyChar) = 13 And Me.TextBox4.Text = "" Then
            MsgBox("Selecciona Primero la Factura")
        ElseIf (Asc(e.KeyChar) = 13) Then
            busqueda(0)
        End If
    End Sub
    Private Sub TextBox4_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles TextBox4.KeyPress
        If (Asc(e.KeyChar) = 13) And Me.TextBox1.Text = "" Then
            MsgBox("Selecciona Primero la Serie")
        ElseIf (Asc(e.KeyChar) = 13) Then
            busqueda(0)
        End If
    End Sub

    Private Sub TextBox3_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles TextBox3.KeyPress
        If Asc(e.KeyChar) = 13 Then
            busqueda(2)
        End If
    End Sub
    Private Sub TextBox2_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles TextBox2.KeyPress
        If Asc(e.KeyChar) = 13 Then
            busqueda(1)
        End If
    End Sub

    Private Sub Button10_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button10.Click
        Me.Close()
    End Sub

    Private Sub Button6_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button6.Click
        'If ComboBoxCompanias.SelectedValue = 0 Then
        '    MsgBox("Selecciona una Compa��a")
        '    Exit Sub
        'End If
        GloIdDistribuidor = ComboBoxCompanias.SelectedValue
        Gloop = "N"
        LiTipo = 4
        bnd = 1
        CAPTURAFACTURAGLOBAL.Show()
    End Sub

    Private Sub Button9_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button9.Click
        CMBIdFacturaTextBox.Text = BuscaFacturasGlobalesDataGridView.SelectedCells(0).Value
        Me.CMBCanceladaTextBox.Text = BuscaFacturasGlobalesDataGridView.SelectedCells(7).Value
        If IsNumeric(CMBIdFacturaTextBox.Text) = False Then
            MsgBox("No se ha seleccionado una factura", MsgBoxStyle.Information)
            Exit Sub
        End If
        If Me.CMBCanceladaTextBox.Text = 1 Then
            MsgBox("La Factura ya ha sido Cancelada con Anterioridad")
        Else 'If Me.DateTimePicker1.Text = Me.CMBFechaTextBox.Text Then
            Dim resp As MsgBoxResult = MsgBoxResult.Cancel
            resp = MsgBox("� Esta Seguro de que Desea Cancelar la Factura Global con IdFactura: " + CMBIdFacturaTextBox.Text + " ?", MsgBoxStyle.YesNo)
            If resp = MsgBoxResult.Yes Then
                bec_bandera = 1
                Try
                    Dim CON As New SqlConnection(MiConexion)
                    CON.Open()
                    Me.CANCELAFACTGLOBALTableAdapter.Connection = CON
                    Me.CANCELAFACTGLOBALTableAdapter.Fill(Me.NewsoftvDataSet2.CANCELAFACTGLOBAL, Me.CMBIdFacturaTextBox.Text, " hola")
                    CON.Close()
                Catch ex As Exception

                End Try

                'FacturaFiscalCFD---------------------------------------------------------------------
                'Generaci�n de FF
                'facturaFiscalCFD = False
                'facturaFiscalCFD = ChecaSiEsFacturaFiscal("G", 0)
                'If facturaFiscalCFD = True Then
                '    CancelaFacturaCFD("G", Me.CMBIdFacturaTextBox.Text, CMBSerieTextBox.Text, FacturaLabel1.Text, 0, "")
                'End If
                '--------------------------------------------------------------------------------------
                If GloActivarCFD = 1 Then
                    Try

                        CancelaFacturaDigitalGlobal(Me.CMBIdFacturaTextBox.Text)
                    Catch ex As Exception

                    End Try

                End If

                MsgBox("La Factura ha sido Cancelada")
            End If
        End If
    End Sub

    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button3.Click
        busqueda(2)
    End Sub

    Private Sub ComboBox2_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ComboBox2.SelectedIndexChanged
        bec_tipo = Me.ComboBox2.SelectedValue
        If bec_tipo = Nothing Then
            Exit Sub
        End If
        busqueda(3)
    End Sub




    Private Sub Button4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button4.Click
        Try
            CMBIdFacturaTextBox.Text = BuscaFacturasGlobalesDataGridView.SelectedCells(0).Value
            Me.CMBCanceladaTextBox.Text = BuscaFacturasGlobalesDataGridView.SelectedCells(7).Value
            If IsNumeric(CMBIdFacturaTextBox.Text) = False Then
                MsgBox("No se ha seleccionado una factura", MsgBoxStyle.Information)
                Exit Sub
            End If
            Dim Serie As String = Nothing
            Dim fecha As String = Nothing
            Dim Cajera As String = Nothing
            Dim Factura As String = Nothing

            Serie = "" 'Me.CMBSerieTextBox.Text
            Factura = 0 'Me.FacturaLabel1.Text

            If LocImpresoraTickets = "" Then
                MsgBox("No Se Ha Asigando Una Impresora de Tickets A Esta Sucursal", MsgBoxStyle.Information)
            ElseIf LocImpresoraTickets <> "" Then
                'Me.ConfigureCrystalReportefacturaGlobal(Letra2, Importe, Serie, fecha, Cajera, Factura)
                If GloActivarCFD = 0 Then
                    fecha = Me.CMBFechaTextBox.Text
                    Cajera = Me.CMBCajeraTextBox.Text
                    Importe = Me.CMBImporteTextBox.Text
                    CantidadaLetra()
                    DameImporteFacturaGlobal(Today, Today, 0, " ", CMBIdFacturaTextBox.Text, 2)
                    ConfigureCrystalReportefacturaGlobal(Letra2, Importe, SubTotal, Iva, Ieps, Serie, fecha, Cajera, Factura)
                ElseIf GloActivarCFD = 1 Then
                    If IsNumeric(CMBIdFacturaTextBox.Text) = True Then
                        ImprimeFacturaDigitalGlobal(CLng(CMBIdFacturaTextBox.Text))
                    End If

                End If
            End If
        Catch ex As Exception

        End Try
    End Sub

    Private Sub Button5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button5.Click
        If Me.CMBIdFacturaTextBox.Text <> "" Then
            LocSerieglo = "" 'Me.CMBSerieTextBox.Text '.BuscaFacturasGlobalesDataGridView.SelectedCells(0).Value
            LocFacturaGlo = CMBIdFacturaTextBox.Text   'Me.FacturaLabel1.Text 'CInt(Me.BuscaFacturasGlobalesDataGridView.SelectedCells(1).Value)
            LocFechaGlo = Me.CMBFechaTextBox.Text 'Me.BuscaFacturasGlobalesDataGridView.SelectedCells(3).Value
            LocCajeroGlo = Me.CMBCajeraTextBox.Text 'Me.BuscaFacturasGlobalesDataGridView.SelectedCells(6).Value
            LocImporteGlo = Me.CMBImporteTextBox.Text 'Me.BuscaFacturasGlobalesDataGridView.SelectedCells(2).Value
            'Me.Dame_clv_sucursalTableAdapter.Fill(Me.Procedimientos_arnoldo.Dame_clv_sucursal, Me.BuscaFacturasGlobalesDataGridView.SelectedCells(0).Value, Locclv_sucursalglo)
            Locclv_sucursalglo = Me.Label6.Text
            LocFechaGloFinal = LocFechaGlo
            Gloop = "C"
            CAPTURAFACTURAGLOBAL.Show()
        Else
            MsgBox("No Se Ha Seleccionado Alguna Factura para Mostrar", MsgBoxStyle.Information)
        End If
    End Sub


    Private Sub DameImporteFacturaGlobal(ByVal Fecha As DateTime, ByVal FechaFin As DateTime, ByVal SelSucursal As Integer, ByVal Tipo As Char, ByVal IDFactura As Long, ByVal Op As Integer)
        Dim conexion As New SqlConnection(MiConexion)
        Dim strSQL As New StringBuilder
        strSQL.Append("EXEC DameImporteFacturaGlobal ")
        strSQL.Append("'" & CStr(Fecha) & "', ")
        strSQL.Append("'" & CStr(FechaFin) & "', ")
        strSQL.Append(CStr(SelSucursal) & ", ")
        strSQL.Append("'" & Tipo & "', ")
        strSQL.Append(CStr(IDFactura) & ", ")
        strSQL.Append(CStr(Op) & ", ")
        strSQL.Append(CStr(GloIdDistribuidor))
        Dim dataAdapter As New SqlDataAdapter(strSQL.ToString(), conexion)
        Dim dataTable As New DataTable

        Try

            dataAdapter.Fill(dataTable)
            Importe = Format(CDec(dataTable.Rows(0)(0).ToString()), "#####0.00")
            SubTotal = Format(CDec(dataTable.Rows(0)(1).ToString()), "#####0.00")
            Iva = Format(CDec(dataTable.Rows(0)(2).ToString()), "#####0.00")
            Ieps = Format(CDec(dataTable.Rows(0)(3).ToString()), "#####0.00")

        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        End Try
    End Sub

    Private Function Usp_DameDetallaFacturaGlobal(ByVal prmclvfactura As Long) As DataSet
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@IdFactura", SqlDbType.BigInt, prmclvfactura)
        Dim listaTablas As New List(Of String)
        listaTablas.Add("Usp_DameDetallaFacturaGlobal")
        Usp_DameDetallaFacturaGlobal = BaseII.ConsultaDS("Usp_DameDetallaFacturaGlobal", listaTablas)

    End Function
    Private Sub ConfigureCrystalReportefacturaGlobal(ByVal Letra2 As String, ByVal Importe As String, ByVal SubTotal As String, ByVal Iva As String, ByVal Ieps As String, ByVal Serie2 As String, ByVal Fecha2 As String, ByVal Cajera2 As String, ByVal Factura2 As String)
        Try
            Dim ds As New DataSet
            customersByCityReport = New ReportDocument
            Dim connectionInfo As New ConnectionInfo
            connectionInfo.ServerName = GloServerName
            connectionInfo.DatabaseName = GloDatabaseName
            connectionInfo.UserID = GloUserID
            connectionInfo.Password = GloPassword

            Dim cliente2 As String = "P�blico en General"
            Dim concepto2 As String = "Ingreso por Pago de Servicios"
            Dim reportPath As String = Nothing

            'Select Case Locclv_empresa
            '    Case "AG"
            reportPath = RutaReportes + "\ReporteFacturaGlobalGiga.rpt"
            '    Case "TO"
            '        reportPath = RutaReportes + "\ReporteFacturaGlobal.rpt"
            '    Case "SA"
            '        reportPath = RutaReportes + "\ReporteFacturaGlobalTvRey.rpt"
            '    Case "VA"
            '        reportPath = RutaReportes + "\ReporteFacturaGlobalTvRey.rpt"
            'End Select
            customersByCityReport.Load(reportPath)
            ds.Clear()
            ds.Tables.Clear()
            ds = Usp_DameDetallaFacturaGlobal(CInt(Me.CMBIdFacturaTextBox.Text))

            SetDBReport(ds, customersByCityReport)

            If Locclv_empresa = "SA" Or Locclv_empresa = "TO" Or Locclv_empresa = "AG" Or Locclv_empresa = "VA" Then

                customersByCityReport.DataDefinition.FormulaFields("Letra").Text = "'" & Letra2 & "'"
                customersByCityReport.DataDefinition.FormulaFields("Cliente").Text = "'" & cliente2 & "'"
                customersByCityReport.DataDefinition.FormulaFields("Concepto").Text = "'" & concepto2 & "'"
                customersByCityReport.DataDefinition.FormulaFields("Serie").Text = "'" & Serie2 & "'"
                customersByCityReport.DataDefinition.FormulaFields("Fecha").Text = "'" & Fecha2 & "'"
                customersByCityReport.DataDefinition.FormulaFields("Cajera").Text = "'" & Cajera2 & "'"
                customersByCityReport.DataDefinition.FormulaFields("ImporteServicio").Text = "'" & Format(CDec(SubTotal.ToString()), "##,##0.00") & "'"
                customersByCityReport.DataDefinition.FormulaFields("Factura").Text = "'" & Factura2 & "'"
                customersByCityReport.DataDefinition.FormulaFields("Direccion").Text = "'" & GloDireccionEmpresa & "'"
                customersByCityReport.DataDefinition.FormulaFields("RFC").Text = "'" & GloRfcEmpresa & "'"
                customersByCityReport.DataDefinition.FormulaFields("Ciudad").Text = "'" & GloCiudadEmpresa & "'"

                customersByCityReport.DataDefinition.FormulaFields("SubTotal").Text = "'" & Format(CDec(SubTotal), "##,##0.00") & "'"
                customersByCityReport.DataDefinition.FormulaFields("Iva").Text = "'" & Format(CDec(Iva.ToString()), "##,##0.00") & "'"
                customersByCityReport.DataDefinition.FormulaFields("Ieps").Text = "'" & Format(CDec(Ieps.ToString()), "##,##0.00") & "'"
                customersByCityReport.DataDefinition.FormulaFields("Total").Text = "'" & Format(CDec(Importe.ToString()), "##,##0.00") & "'"

            End If


            ''Select Case Locclv_empresa
            ''    Case "AG"
            ''customersByCityReport.PrintOptions.PrinterName = LocImpresoraTickets
            'impresorafiscal = "ticket
            'customersByCityReport.PrintOptions.PrinterName = impresorafiscal
            ''    Case "TO"
            ''        customersByCityReport.PrintOptions.PrinterName = impresorafiscal
            ''    Case "SA"
            ''        customersByCityReport.PrintOptions.PrinterName = impresorafiscal
            ''    Case "VA"
            ''        customersByCityReport.PrintOptions.PrinterName = impresorafiscal
            ''End Select

            'eCont = 1
            'eRes = 0
            'Do
            '    If (MsgBox("Se va a imprimir una Factura Global. �Est� lista la impresora?", MsgBoxStyle.YesNo)) = 6 Then
            '        customersByCityReport.PrintToPrinter(1, True, 1, 1)
            '        eRes = MsgBox("La Impresi�n de la Factura Global " + CStr(eCont) + "/4, �Fu� Correcta?", MsgBoxStyle.YesNo, "Atenci�n")
            '        '6=Yes;7=No
            '        If eRes = 6 Then eCont = eCont + 1
            '    Else
            '        Exit Sub
            '    End If
            'Loop While eCont <= 1
            'eCont = 1
            'eRes = 0
            'Do
            '    If (MsgBox("Se va a imprimir una copia de la Factura Global anterior. �Est� lista la impresora?", MsgBoxStyle.YesNo)) = 6 Then
            '        customersByCityReport.PrintToPrinter(1, True, 1, 1)
            '        eRes = MsgBox("La Impresi�n de la Factura Global " + CStr(eCont + 1) + "/4, �Fu� Correcta?", MsgBoxStyle.YesNo, "Atenci�n")
            '        '6=Yes;7=No
            '        If eRes = 6 Then eCont = eCont + 1
            '    Else
            '        Exit Sub
            '    End If
            'Loop While eCont <= 1
            'eCont = 1
            'eRes = 0
            'Do
            '    If (MsgBox("Se va a imprimir una copia de la Factura Global anterior. �Est� lista la impresora?", MsgBoxStyle.YesNo)) = 6 Then
            '        customersByCityReport.PrintToPrinter(1, True, 1, 1)
            '        eRes = MsgBox("La Impresi�n de la Factura Global " + CStr(eCont + 2) + "/4, �Fu� Correcta?", MsgBoxStyle.YesNo, "Atenci�n")
            '        '6=Yes;7=No
            '        If eRes = 6 Then eCont = eCont + 1
            '    Else
            '        Exit Sub
            '    End If
            'Loop While eCont <= 1
            'eCont = 1
            'eRes = 0
            'Do
            '    If (MsgBox("Se va a imprimir una copia de la Factura Global anterior. �Est� lista la impresora?", MsgBoxStyle.YesNo)) = 6 Then
            '        customersByCityReport.PrintToPrinter(1, True, 1, 1)
            '        eRes = MsgBox("La Impresi�n de la Factura " + CStr(eCont + 3) + "/4, �Fu� Correcta?", MsgBoxStyle.YesNo, "Atenci�n")
            '        '6=Yes;7=No
            '        If eRes = 6 Then eCont = eCont + 1
            '    Else
            '        Exit Sub
            '    End If
            'Loop While eCont <= 1
            ''customersByCityReport.PrintToPrinter(1, True, 1, 1)
            'customersByCityReport = Nothing
            FrmImprimir.CrystalReportViewer1.ShowGroupTreeButton = False
            FrmImprimir.CrystalReportViewer1.ToolPanelView = CrystalDecisions.Windows.Forms.ToolPanelViewType.None
            FrmImprimir.CrystalReportViewer1.ReportSource = customersByCityReport
            LiTipo = 0
            FrmImprimir.Show()

        Catch ex As Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try

    End Sub


    Private Sub ComboBoxCompanias_SelectedIndexChanged(sender As System.Object, e As System.EventArgs) Handles ComboBoxCompanias.SelectedIndexChanged
        Try
            GloIdDistribuidor = ComboBoxCompanias.SelectedValue
            Dim conexion As New SqlConnection(MiConexion)
            Dim comando As New SqlCommand()
            conexion.Open()
            comando.Connection = conexion
            comando.CommandText = "update Seleccion_compania set idcompania=" + GloIdDistribuidor.ToString()
            comando.ExecuteNonQuery()
            conexion.Close()
            busqueda(4)
        Catch ex As Exception

        End Try
    End Sub

    Private Sub BuscaFacturasGlobalesDataGridView_CellClick(sender As Object, e As System.Windows.Forms.DataGridViewCellEventArgs) Handles BuscaFacturasGlobalesDataGridView.CellClick
        Try
            'CMBSerieTextBox.Text = BuscaFacturasGlobalesDataGridView.SelectedCells(0).Value
            'FacturaLabel1.Text = BuscaFacturasGlobalesDataGridView.SelectedCells(1).Value
            CMBIdFacturaTextBox.Text = BuscaFacturasGlobalesDataGridView.SelectedCells(0).Value
            CMBFechaTextBox.Text = BuscaFacturasGlobalesDataGridView.SelectedCells(3).Value
            CMBImporteTextBox.Text = BuscaFacturasGlobalesDataGridView.SelectedCells(4).Value
            CMBDistribuidorTextBox.Text = BuscaFacturasGlobalesDataGridView.SelectedCells(5).Value
            CMBCajeraTextBox.Text = BuscaFacturasGlobalesDataGridView.SelectedCells(6).Value
            CMBCanceladaTextBox.Text = BuscaFacturasGlobalesDataGridView.SelectedCells(7).Value

            'Label8.Text = BuscaFacturasGlobalesDataGridView.SelectedCells(4).Value
        Catch ex As Exception

        End Try
    End Sub

    Private Sub BuscaFacturasGlobalesDataGridView_CellContentClick(sender As System.Object, e As System.Windows.Forms.DataGridViewCellEventArgs) Handles BuscaFacturasGlobalesDataGridView.CellContentClick

    End Sub

    Private Sub CMBSerieTextBox_TextChanged(sender As System.Object, e As System.EventArgs)

    End Sub

    Private Sub HazFacturaDigitalGlobal(ByVal LocClv_FacturaGlobal As Integer)
        FacturacionDigitalSoftv.ClassCFDI.MiConexion = MiConexion
        Dim identi As Integer = 0
        FacturacionDigitalSoftv.ClassCFDI.EsTimbrePrueba = eEsTimbrePrueba
        FacturacionDigitalSoftv.ClassCFDI.Locop = 1
        FacturacionDigitalSoftv.ClassCFDI.Dime_Aque_Compania_FacturarleGlobal(LocClv_FacturaGlobal, MiConexion)
        FacturacionDigitalSoftv.ClassCFDI.Graba_Factura_Digital_Global(LocClv_FacturaGlobal, CStr(FacturacionDigitalSoftv.ClassCFDI.locID_Compania_Mizart), CStr(FacturacionDigitalSoftv.ClassCFDI.locID_Sucursal_Mizart), MiConexion)
        Try
            If FacturacionDigitalSoftv.ClassCFDI.GloClv_FacturaCFD > 0 Then
                Dim frm As New FacturacionDigitalSoftv.FrmImprimir
                frm.ShowDialog()
                FacturacionDigitalSoftv.ClassCFDI.ENVIAR_Factura(FacturacionDigitalSoftv.ClassCFDI.GloClv_FacturaCFD)
            Else
                MsgBox("No se genero la factura digital cancele y vuelva a intentar por favor")
            End If
            'FormPruebaDigital.Show()
        Catch ex As Exception
        End Try
        FacturacionDigitalSoftv.ClassCFDI.locID_Compania_Mizart = ""
        FacturacionDigitalSoftv.ClassCFDI.locID_Sucursal_Mizart = ""
    End Sub

    Public Function ValidaReFActuraGlobal(oClvFactura As Long) As Integer
        ValidaReFActuraGlobal = 0
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@Clv_Factura", SqlDbType.BigInt, oClvFactura)
        BaseII.CreateMyParameter("@Res", ParameterDirection.Output, SqlDbType.Int)
        BaseII.CreateMyParameter("@Msg", ParameterDirection.Output, SqlDbType.VarChar, 250)
        BaseII.ProcedimientoOutPut("ValidaReFacturaGlobal")
        ValidaReFActuraGlobal = BaseII.dicoPar("@Res")
        If BaseII.dicoPar("@Res") > 0 Then
            MsgBox(BaseII.dicoPar("@Msg"))
        End If
    End Function
    Private Sub Button7_Click(sender As Object, e As EventArgs) Handles Button7.Click
        CMBIdFacturaTextBox.Text = BuscaFacturasGlobalesDataGridView.SelectedCells(0).Value
        Me.CMBCanceladaTextBox.Text = BuscaFacturasGlobalesDataGridView.SelectedCells(7).Value
        If IsNumeric(CMBIdFacturaTextBox.Text) = False Then
            MsgBox("No se ha seleccionado una factura", MsgBoxStyle.Information)
            Exit Sub
        End If
        Dim eRes As Integer = 0
        eRes = ValidaReFActuraGlobal(eCveFactura)
        If eRes = 0 Then

            Dim resp As MsgBoxResult = MsgBoxResult.Cancel
            resp = MsgBox("� Esta Seguro de que Deseas Refacturar la Factura Global con IdFactura: " + CMBIdFacturaTextBox.Text + " ?", MsgBoxStyle.YesNo)
            If resp = MsgBoxResult.Yes Then
                bec_bandera = 1
                'Try La Ticket no se Cancela solo la Digital
                '    Dim CON As New SqlConnection(MiConexion)
                '    CON.Open()
                '    Me.CANCELAFACTGLOBALTableAdapter.Connection = CON
                '    Me.CANCELAFACTGLOBALTableAdapter.Fill(Me.NewsoftvDataSet2.CANCELAFACTGLOBAL, Me.CMBIdFacturaTextBox.Text, "Refacturacion")
                '    CON.Close()
                'Catch ex As Exception

                'End Try           
                If GloActivarCFD = 1 Then
                    Try
                        CancelaFacturaDigitalGlobal(Me.CMBIdFacturaTextBox.Text)
                        HazFacturaDigitalGlobal(Me.CMBIdFacturaTextBox.Text)
                    Catch ex As Exception

                    End Try

                End If


            End If
        End If
    End Sub
End Class
