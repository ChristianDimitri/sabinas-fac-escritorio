Imports CrystalDecisions.Shared
Imports CrystalDecisions.CrystalReports.Engine


Public Class FrmImprimePoliza
    Private customersByCityReport As ReportDocument
    Private op As String = Nothing
    Private Titulo As String = Nothing

    'Private Sub ConfigureCrystalReports()
    '    customersByCityReport = New ReportDocument
    '    Dim connectionInfo As New ConnectionInfo
    '    '"Data Source=SERVER-2003\SQLEXPRESS;Initial Catalog=Newsoftv;Persist Security Info"& _ 
    '    '    "=True;User ID=DeSistema;Password=1975huli")
    '    connectionInfo.ServerName = GloServerName
    '    connectionInfo.DatabaseName = GloDatabaseName
    '    connectionInfo.UserID = GloUserID
    '    connectionInfo.Password = GloPassword
    '    Dim reportPath As String = Nothing
    '    Dim Titulo As String = Nothing
    '    Dim Sucursal As String = Nothing
    '    Dim Ciudades As String = Nothing
    '    Ciudades = " Ciudad(es): " + LocCiudades
    '    reportPath = RutaReportes + "\RepPoliza.rpt"

    '    'Sucursal = " Sucursal: " + GloNomSucursal
    '    customersByCityReport.Load(reportPath)

    '    'SetDBLogonForSubReport(connectionInfo, customersByCityReport)
    '    SetDBLogonForReport(connectionInfo, customersByCityReport)
    '    'SetDBLogonForReport(connectionInfo, customersByCityReport)
    '    'SetDBLogonForSubReport(connectionInfo, customersByCityReport)

    '    '@cLV_LLAVE_POLIZA
    '    customersByCityReport.SetParameterValue(0, LocGloClv_poliza)


    '    'customersByCityReport.DataDefinition.FormulaFields("Empresa").Text = "'" & LocNomEmpresa & "'"
    '    'customersByCityReport.DataDefinition.FormulaFields("Titulo").Text = "'" & Titulo & "'"
    '    'eFechaTitulo = "de la Fecha " & eFechaInicial & " a la Fecha " & eFechaFinal
    '    'customersByCityReport.DataDefinition.FormulaFields("Fecha").Text = "'" & eFechaTitulo & "'"
    '    'customersByCityReport.DataDefinition.FormulaFields("Subtitulo").Text = "'" & Sucursal & "'"
    '    'customersByCityReport.DataDefinition.FormulaFields("Ciudades").Text = "'" & Ciudades & "'"


    '    CrystalReportViewer1.ReportSource = customersByCityReport
    '    CrystalReportViewer1.Zoom(75)



    '    If GloOpFacturas = 3 Then
    '        CrystalReportViewer1.ShowExportButton = False
    '        CrystalReportViewer1.ShowPrintButton = False
    '        CrystalReportViewer1.ShowRefreshButton = False
    '    End If
    '    'SetDBLogonForReport2(connectionInfo)
    '    customersByCityReport = Nothing
    'End Sub

    'Private Sub SetDBLogonForReport(ByVal myConnectionInfo As ConnectionInfo, ByVal myReportDocument As ReportDocument)
    '    customersByCityReport.DataSourceConnections(0).SetConnection(GloServerName, GloDatabaseName, GloUserID, GloPassword)
    '    'customersByCityReport.SetDatabaseLogon(GloUserID, GloPassword, GloServerName, GloDatabaseName)

    '    Dim myTables As Tables = myReportDocument.Database.Tables
    '    Dim myTable As CrystalDecisions.CrystalReports.Engine.Table
    '    For Each myTable In myTables
    '        Dim myTableLogonInfo As TableLogOnInfo = myTable.LogOnInfo
    '        myTableLogonInfo.ConnectionInfo = myConnectionInfo
    '        myTable.ApplyLogOnInfo(myTableLogonInfo)
    '        myTable.Location = GloDatabaseName & ".dbo." & myTable.Location.Substring(myTable.Location.LastIndexOf(".") + 1)
    '    Next
    'End Sub

    'Private Sub SetDBLogonForSubReport(ByVal myConnectionInfo As ConnectionInfo, ByVal myReportDocument As ReportDocument)
    '    customersByCityReport.Subreports(0).SetDatabaseLogon(GloUserID, GloPassword, GloServerName, GloDatabaseName)
    '    'customersByCityReport.Subreports(0).DataSourceConnections(0).SetConnection(GloServerName, GloDatabaseName, GloUserID, GloPassword)

    '    Dim I As Integer = myReportDocument.Subreports.Count
    '    Dim X As Integer = 0
    '    For X = 0 To I - 1
    '        Dim myTables As Tables = myReportDocument.Subreports(X).Database.Tables
    '        Dim myTable As CrystalDecisions.CrystalReports.Engine.Table
    '        For Each myTable In myTables
    '            Dim myTableLogonInfo As TableLogOnInfo = myTable.LogOnInfo
    '            myTableLogonInfo.ConnectionInfo = myConnectionInfo
    '            myTable.ApplyLogOnInfo(myTableLogonInfo)
    '            myTable.Location = GloDatabaseName & ".dbo." & myTable.Location.Substring(myTable.Location.LastIndexOf(".") + 1)
    '        Next
    '    Next X
    'End Sub


    Private Sub FrmImprimePoliza_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim rDocument As New CrystalDecisions.CrystalReports.Engine.ReportDocument
        Dim dS As New DataSet
        dS = REPORTEPoliza(gLOoPrEPpOLIZA, LocGloClv_poliza, GloClvCompania)

        If gLOoPrEPpOLIZA = 1 Then
            rDocument.Load(RutaReportes + "\RepPolizaDetallado.rpt")
        Else
            rDocument.Load(RutaReportes + "\REPORTEPoliza.rpt")
        End If
        rDocument.SetDataSource(dS)
        CrystalReportViewer1.ReportSource = rDocument
        CrystalReportViewer1.Zoom(75)
        'ConfigureCrystalReports()
    End Sub
    Private Function REPORTEPoliza(ByVal Tipo As Integer, ByVal ClvLlavePoliza As Integer, ByVal ClvCompania As Integer) As DataSet
        Dim listaTablas As New List(Of String)
        listaTablas.Add("TABLE_DETALLE")
        listaTablas.Add("tblCompanias")
        listaTablas.Add("General")
        listaTablas.Add("SUCURSALES")

        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@TIPO", SqlDbType.Int, Tipo)
        BaseII.CreateMyParameter("@CLVLLAVEPOLIZA", SqlDbType.Int, ClvLlavePoliza)
        BaseII.CreateMyParameter("@CLVCOMPANIA", SqlDbType.Int, ClvCompania)
        REPORTEPOLIZA = BaseII.ConsultaDS("REPORTEPoliza", listaTablas)
    End Function
End Class