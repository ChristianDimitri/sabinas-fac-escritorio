﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class BrwGastos
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.VerAcceso2TableAdapter1 = New softvFacturacion.ProcedimientosArnoldo3TableAdapters.VerAcceso2TableAdapter()
        Me.CMBlblTipoGasto = New System.Windows.Forms.Label()
        Me.cmbBuscaTipoGasto = New System.Windows.Forms.ComboBox()
        Me.dgvGastos = New System.Windows.Forms.DataGridView()
        Me.ClaveGasto = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ActivoGasto = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.fecha = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DescripcionGasto = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.cajera = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.importe = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.estatus = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.idTipoGasto = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.pnlDatosGenerales = New System.Windows.Forms.Panel()
        Me.CMBlblStatus = New System.Windows.Forms.Label()
        Me.CMBlblMuestraStatus = New System.Windows.Forms.Label()
        Me.CMBlblImporte = New System.Windows.Forms.Label()
        Me.CMBlblMuestraImporte = New System.Windows.Forms.Label()
        Me.CMBlblCajera = New System.Windows.Forms.Label()
        Me.CMBlblMuestraCajera = New System.Windows.Forms.Label()
        Me.CMBlblFecha = New System.Windows.Forms.Label()
        Me.CMBlblMuestraFecha = New System.Windows.Forms.Label()
        Me.CMBlblIdTipoPago = New System.Windows.Forms.Label()
        Me.CMBlblMuestraIdTipoGasto = New System.Windows.Forms.Label()
        Me.btnSalir = New System.Windows.Forms.Button()
        Me.btnModificar = New System.Windows.Forms.Button()
        Me.btnConsultar = New System.Windows.Forms.Button()
        Me.btnAceptar = New System.Windows.Forms.Button()
        Me.CMBlblBusqueda = New System.Windows.Forms.Label()
        Me.CMBlblBuscaCajera = New System.Windows.Forms.Label()
        Me.cmbBuscaCajera = New System.Windows.Forms.ComboBox()
        Me.cmbBuscaEstatus = New System.Windows.Forms.ComboBox()
        Me.CMBlblEstatus = New System.Windows.Forms.Label()
        Me.btnBuscaDescripcion = New System.Windows.Forms.Button()
        Me.txtBuscaDescripcion = New System.Windows.Forms.TextBox()
        Me.CMBlblBuscaDescripcion = New System.Windows.Forms.Label()
        Me.btnCancelar = New System.Windows.Forms.Button()
        CType(Me.dgvGastos, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pnlDatosGenerales.SuspendLayout()
        Me.SuspendLayout()
        '
        'VerAcceso2TableAdapter1
        '
        Me.VerAcceso2TableAdapter1.ClearBeforeFill = True
        '
        'CMBlblTipoGasto
        '
        Me.CMBlblTipoGasto.AutoSize = True
        Me.CMBlblTipoGasto.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBlblTipoGasto.Location = New System.Drawing.Point(11, 161)
        Me.CMBlblTipoGasto.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.CMBlblTipoGasto.Name = "CMBlblTipoGasto"
        Me.CMBlblTipoGasto.Size = New System.Drawing.Size(139, 20)
        Me.CMBlblTipoGasto.TabIndex = 23
        Me.CMBlblTipoGasto.Text = "Tipo de Gasto :"
        '
        'cmbBuscaTipoGasto
        '
        Me.cmbBuscaTipoGasto.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmbBuscaTipoGasto.FormattingEnabled = True
        Me.cmbBuscaTipoGasto.Location = New System.Drawing.Point(13, 181)
        Me.cmbBuscaTipoGasto.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.cmbBuscaTipoGasto.Name = "cmbBuscaTipoGasto"
        Me.cmbBuscaTipoGasto.Size = New System.Drawing.Size(261, 28)
        Me.cmbBuscaTipoGasto.TabIndex = 1
        '
        'dgvGastos
        '
        Me.dgvGastos.AllowUserToAddRows = False
        Me.dgvGastos.AllowUserToDeleteRows = False
        Me.dgvGastos.BackgroundColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvGastos.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle1
        Me.dgvGastos.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvGastos.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.ClaveGasto, Me.ActivoGasto, Me.fecha, Me.DescripcionGasto, Me.cajera, Me.importe, Me.estatus, Me.idTipoGasto})
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgvGastos.DefaultCellStyle = DataGridViewCellStyle2
        Me.dgvGastos.Location = New System.Drawing.Point(284, 14)
        Me.dgvGastos.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.dgvGastos.Name = "dgvGastos"
        Me.dgvGastos.ReadOnly = True
        Me.dgvGastos.RowHeadersVisible = False
        Me.dgvGastos.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvGastos.Size = New System.Drawing.Size(888, 878)
        Me.dgvGastos.TabIndex = 5
        '
        'ClaveGasto
        '
        Me.ClaveGasto.DataPropertyName = "idGasto"
        Me.ClaveGasto.HeaderText = "Id Gasto"
        Me.ClaveGasto.Name = "ClaveGasto"
        Me.ClaveGasto.ReadOnly = True
        Me.ClaveGasto.Width = 50
        '
        'ActivoGasto
        '
        Me.ActivoGasto.DataPropertyName = "TipoGasto"
        Me.ActivoGasto.HeaderText = "Tipo Gasto"
        Me.ActivoGasto.Name = "ActivoGasto"
        Me.ActivoGasto.ReadOnly = True
        '
        'fecha
        '
        Me.fecha.DataPropertyName = "fecha"
        Me.fecha.HeaderText = "Fecha"
        Me.fecha.Name = "fecha"
        Me.fecha.ReadOnly = True
        Me.fecha.Width = 70
        '
        'DescripcionGasto
        '
        Me.DescripcionGasto.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.DescripcionGasto.DataPropertyName = "DESCRIPCION"
        Me.DescripcionGasto.HeaderText = "Descripción"
        Me.DescripcionGasto.Name = "DescripcionGasto"
        Me.DescripcionGasto.ReadOnly = True
        '
        'cajera
        '
        Me.cajera.DataPropertyName = "clvUsuario"
        Me.cajera.HeaderText = "Cajera"
        Me.cajera.Name = "cajera"
        Me.cajera.ReadOnly = True
        '
        'importe
        '
        Me.importe.DataPropertyName = "importe"
        Me.importe.HeaderText = "Importe"
        Me.importe.Name = "importe"
        Me.importe.ReadOnly = True
        '
        'estatus
        '
        Me.estatus.DataPropertyName = "status"
        Me.estatus.HeaderText = "Estatus"
        Me.estatus.Name = "estatus"
        Me.estatus.ReadOnly = True
        Me.estatus.Width = 50
        '
        'idTipoGasto
        '
        Me.idTipoGasto.DataPropertyName = "idTipoGasto"
        Me.idTipoGasto.HeaderText = "idTipoGasto"
        Me.idTipoGasto.Name = "idTipoGasto"
        Me.idTipoGasto.ReadOnly = True
        Me.idTipoGasto.Visible = False
        '
        'pnlDatosGenerales
        '
        Me.pnlDatosGenerales.Controls.Add(Me.CMBlblStatus)
        Me.pnlDatosGenerales.Controls.Add(Me.CMBlblMuestraStatus)
        Me.pnlDatosGenerales.Controls.Add(Me.CMBlblImporte)
        Me.pnlDatosGenerales.Controls.Add(Me.CMBlblMuestraImporte)
        Me.pnlDatosGenerales.Controls.Add(Me.CMBlblCajera)
        Me.pnlDatosGenerales.Controls.Add(Me.CMBlblMuestraCajera)
        Me.pnlDatosGenerales.Controls.Add(Me.CMBlblFecha)
        Me.pnlDatosGenerales.Controls.Add(Me.CMBlblMuestraFecha)
        Me.pnlDatosGenerales.Controls.Add(Me.CMBlblIdTipoPago)
        Me.pnlDatosGenerales.Controls.Add(Me.CMBlblMuestraIdTipoGasto)
        Me.pnlDatosGenerales.Location = New System.Drawing.Point(16, 597)
        Me.pnlDatosGenerales.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.pnlDatosGenerales.Name = "pnlDatosGenerales"
        Me.pnlDatosGenerales.Size = New System.Drawing.Size(260, 294)
        Me.pnlDatosGenerales.TabIndex = 20
        '
        'CMBlblStatus
        '
        Me.CMBlblStatus.AutoSize = True
        Me.CMBlblStatus.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBlblStatus.Location = New System.Drawing.Point(91, 262)
        Me.CMBlblStatus.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.CMBlblStatus.Name = "CMBlblStatus"
        Me.CMBlblStatus.Size = New System.Drawing.Size(0, 20)
        Me.CMBlblStatus.TabIndex = 19
        '
        'CMBlblMuestraStatus
        '
        Me.CMBlblMuestraStatus.AutoSize = True
        Me.CMBlblMuestraStatus.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBlblMuestraStatus.Location = New System.Drawing.Point(5, 262)
        Me.CMBlblMuestraStatus.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.CMBlblMuestraStatus.Name = "CMBlblMuestraStatus"
        Me.CMBlblMuestraStatus.Size = New System.Drawing.Size(75, 20)
        Me.CMBlblMuestraStatus.TabIndex = 18
        Me.CMBlblMuestraStatus.Text = "Status :"
        '
        'CMBlblImporte
        '
        Me.CMBlblImporte.AutoSize = True
        Me.CMBlblImporte.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBlblImporte.Location = New System.Drawing.Point(9, 223)
        Me.CMBlblImporte.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.CMBlblImporte.Name = "CMBlblImporte"
        Me.CMBlblImporte.Size = New System.Drawing.Size(0, 20)
        Me.CMBlblImporte.TabIndex = 17
        '
        'CMBlblMuestraImporte
        '
        Me.CMBlblMuestraImporte.AutoSize = True
        Me.CMBlblMuestraImporte.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBlblMuestraImporte.Location = New System.Drawing.Point(4, 197)
        Me.CMBlblMuestraImporte.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.CMBlblMuestraImporte.Name = "CMBlblMuestraImporte"
        Me.CMBlblMuestraImporte.Size = New System.Drawing.Size(84, 20)
        Me.CMBlblMuestraImporte.TabIndex = 16
        Me.CMBlblMuestraImporte.Text = "Importe :"
        '
        'CMBlblCajera
        '
        Me.CMBlblCajera.AutoSize = True
        Me.CMBlblCajera.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBlblCajera.Location = New System.Drawing.Point(8, 165)
        Me.CMBlblCajera.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.CMBlblCajera.Name = "CMBlblCajera"
        Me.CMBlblCajera.Size = New System.Drawing.Size(0, 20)
        Me.CMBlblCajera.TabIndex = 15
        '
        'CMBlblMuestraCajera
        '
        Me.CMBlblMuestraCajera.AutoSize = True
        Me.CMBlblMuestraCajera.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBlblMuestraCajera.Location = New System.Drawing.Point(5, 138)
        Me.CMBlblMuestraCajera.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.CMBlblMuestraCajera.Name = "CMBlblMuestraCajera"
        Me.CMBlblMuestraCajera.Size = New System.Drawing.Size(76, 20)
        Me.CMBlblMuestraCajera.TabIndex = 14
        Me.CMBlblMuestraCajera.Text = "Cajera :"
        '
        'CMBlblFecha
        '
        Me.CMBlblFecha.AutoSize = True
        Me.CMBlblFecha.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBlblFecha.Location = New System.Drawing.Point(9, 108)
        Me.CMBlblFecha.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.CMBlblFecha.Name = "CMBlblFecha"
        Me.CMBlblFecha.Size = New System.Drawing.Size(0, 20)
        Me.CMBlblFecha.TabIndex = 13
        '
        'CMBlblMuestraFecha
        '
        Me.CMBlblMuestraFecha.AutoSize = True
        Me.CMBlblMuestraFecha.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBlblMuestraFecha.Location = New System.Drawing.Point(4, 85)
        Me.CMBlblMuestraFecha.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.CMBlblMuestraFecha.Name = "CMBlblMuestraFecha"
        Me.CMBlblMuestraFecha.Size = New System.Drawing.Size(72, 20)
        Me.CMBlblMuestraFecha.TabIndex = 12
        Me.CMBlblMuestraFecha.Text = "Fecha :"
        '
        'CMBlblIdTipoPago
        '
        Me.CMBlblIdTipoPago.AutoSize = True
        Me.CMBlblIdTipoPago.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBlblIdTipoPago.Location = New System.Drawing.Point(9, 54)
        Me.CMBlblIdTipoPago.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.CMBlblIdTipoPago.Name = "CMBlblIdTipoPago"
        Me.CMBlblIdTipoPago.Size = New System.Drawing.Size(0, 20)
        Me.CMBlblIdTipoPago.TabIndex = 10
        '
        'CMBlblMuestraIdTipoGasto
        '
        Me.CMBlblMuestraIdTipoGasto.AutoSize = True
        Me.CMBlblMuestraIdTipoGasto.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBlblMuestraIdTipoGasto.Location = New System.Drawing.Point(3, 17)
        Me.CMBlblMuestraIdTipoGasto.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.CMBlblMuestraIdTipoGasto.Name = "CMBlblMuestraIdTipoGasto"
        Me.CMBlblMuestraIdTipoGasto.Size = New System.Drawing.Size(113, 20)
        Me.CMBlblMuestraIdTipoGasto.TabIndex = 9
        Me.CMBlblMuestraIdTipoGasto.Text = "Tipo Gasto :"
        '
        'btnSalir
        '
        Me.btnSalir.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSalir.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSalir.Location = New System.Drawing.Point(1180, 849)
        Me.btnSalir.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.btnSalir.Name = "btnSalir"
        Me.btnSalir.Size = New System.Drawing.Size(160, 42)
        Me.btnSalir.TabIndex = 10
        Me.btnSalir.Text = "&Salir"
        Me.btnSalir.UseVisualStyleBackColor = True
        '
        'btnModificar
        '
        Me.btnModificar.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnModificar.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnModificar.Location = New System.Drawing.Point(1180, 116)
        Me.btnModificar.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.btnModificar.Name = "btnModificar"
        Me.btnModificar.Size = New System.Drawing.Size(163, 42)
        Me.btnModificar.TabIndex = 8
        Me.btnModificar.Text = "&Modificar"
        Me.btnModificar.UseVisualStyleBackColor = True
        '
        'btnConsultar
        '
        Me.btnConsultar.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnConsultar.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnConsultar.Location = New System.Drawing.Point(1180, 68)
        Me.btnConsultar.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.btnConsultar.Name = "btnConsultar"
        Me.btnConsultar.Size = New System.Drawing.Size(163, 41)
        Me.btnConsultar.TabIndex = 7
        Me.btnConsultar.Text = "&Consultar"
        Me.btnConsultar.UseVisualStyleBackColor = True
        '
        'btnAceptar
        '
        Me.btnAceptar.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnAceptar.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnAceptar.Location = New System.Drawing.Point(1180, 17)
        Me.btnAceptar.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.btnAceptar.Name = "btnAceptar"
        Me.btnAceptar.Size = New System.Drawing.Size(163, 43)
        Me.btnAceptar.TabIndex = 6
        Me.btnAceptar.Text = "&Nuevo"
        Me.btnAceptar.UseVisualStyleBackColor = True
        '
        'CMBlblBusqueda
        '
        Me.CMBlblBusqueda.AutoSize = True
        Me.CMBlblBusqueda.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBlblBusqueda.Location = New System.Drawing.Point(17, 25)
        Me.CMBlblBusqueda.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.CMBlblBusqueda.Name = "CMBlblBusqueda"
        Me.CMBlblBusqueda.Size = New System.Drawing.Size(155, 29)
        Me.CMBlblBusqueda.TabIndex = 12
        Me.CMBlblBusqueda.Text = "Buscar Por :"
        '
        'CMBlblBuscaCajera
        '
        Me.CMBlblBuscaCajera.AutoSize = True
        Me.CMBlblBuscaCajera.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBlblBuscaCajera.Location = New System.Drawing.Point(12, 341)
        Me.CMBlblBuscaCajera.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.CMBlblBuscaCajera.Name = "CMBlblBuscaCajera"
        Me.CMBlblBuscaCajera.Size = New System.Drawing.Size(76, 20)
        Me.CMBlblBuscaCajera.TabIndex = 24
        Me.CMBlblBuscaCajera.Text = "Cajera :"
        '
        'cmbBuscaCajera
        '
        Me.cmbBuscaCajera.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmbBuscaCajera.FormattingEnabled = True
        Me.cmbBuscaCajera.Location = New System.Drawing.Point(15, 364)
        Me.cmbBuscaCajera.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.cmbBuscaCajera.Name = "cmbBuscaCajera"
        Me.cmbBuscaCajera.Size = New System.Drawing.Size(263, 28)
        Me.cmbBuscaCajera.TabIndex = 4
        '
        'cmbBuscaEstatus
        '
        Me.cmbBuscaEstatus.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmbBuscaEstatus.FormattingEnabled = True
        Me.cmbBuscaEstatus.Location = New System.Drawing.Point(13, 112)
        Me.cmbBuscaEstatus.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.cmbBuscaEstatus.Name = "cmbBuscaEstatus"
        Me.cmbBuscaEstatus.Size = New System.Drawing.Size(261, 28)
        Me.cmbBuscaEstatus.TabIndex = 0
        '
        'CMBlblEstatus
        '
        Me.CMBlblEstatus.AutoSize = True
        Me.CMBlblEstatus.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBlblEstatus.Location = New System.Drawing.Point(11, 89)
        Me.CMBlblEstatus.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.CMBlblEstatus.Name = "CMBlblEstatus"
        Me.CMBlblEstatus.Size = New System.Drawing.Size(85, 20)
        Me.CMBlblEstatus.TabIndex = 27
        Me.CMBlblEstatus.Text = "Estatus :"
        '
        'btnBuscaDescripcion
        '
        Me.btnBuscaDescripcion.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnBuscaDescripcion.Location = New System.Drawing.Point(165, 286)
        Me.btnBuscaDescripcion.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.btnBuscaDescripcion.Name = "btnBuscaDescripcion"
        Me.btnBuscaDescripcion.Size = New System.Drawing.Size(109, 31)
        Me.btnBuscaDescripcion.TabIndex = 3
        Me.btnBuscaDescripcion.Text = "&Buscar"
        Me.btnBuscaDescripcion.UseVisualStyleBackColor = True
        '
        'txtBuscaDescripcion
        '
        Me.txtBuscaDescripcion.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtBuscaDescripcion.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtBuscaDescripcion.Location = New System.Drawing.Point(15, 251)
        Me.txtBuscaDescripcion.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.txtBuscaDescripcion.Name = "txtBuscaDescripcion"
        Me.txtBuscaDescripcion.Size = New System.Drawing.Size(259, 26)
        Me.txtBuscaDescripcion.TabIndex = 2
        '
        'CMBlblBuscaDescripcion
        '
        Me.CMBlblBuscaDescripcion.AutoSize = True
        Me.CMBlblBuscaDescripcion.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBlblBuscaDescripcion.Location = New System.Drawing.Point(11, 231)
        Me.CMBlblBuscaDescripcion.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.CMBlblBuscaDescripcion.Name = "CMBlblBuscaDescripcion"
        Me.CMBlblBuscaDescripcion.Size = New System.Drawing.Size(122, 20)
        Me.CMBlblBuscaDescripcion.TabIndex = 29
        Me.CMBlblBuscaDescripcion.Text = "Descripción :"
        '
        'btnCancelar
        '
        Me.btnCancelar.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnCancelar.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnCancelar.Location = New System.Drawing.Point(1180, 165)
        Me.btnCancelar.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.btnCancelar.Name = "btnCancelar"
        Me.btnCancelar.Size = New System.Drawing.Size(163, 42)
        Me.btnCancelar.TabIndex = 9
        Me.btnCancelar.Text = "&Cancelar"
        Me.btnCancelar.UseVisualStyleBackColor = True
        '
        'BrwGastos
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.AutoScroll = True
        Me.ClientSize = New System.Drawing.Size(1355, 903)
        Me.Controls.Add(Me.btnCancelar)
        Me.Controls.Add(Me.btnBuscaDescripcion)
        Me.Controls.Add(Me.txtBuscaDescripcion)
        Me.Controls.Add(Me.CMBlblBuscaDescripcion)
        Me.Controls.Add(Me.cmbBuscaEstatus)
        Me.Controls.Add(Me.CMBlblEstatus)
        Me.Controls.Add(Me.cmbBuscaCajera)
        Me.Controls.Add(Me.CMBlblBuscaCajera)
        Me.Controls.Add(Me.CMBlblTipoGasto)
        Me.Controls.Add(Me.cmbBuscaTipoGasto)
        Me.Controls.Add(Me.dgvGastos)
        Me.Controls.Add(Me.pnlDatosGenerales)
        Me.Controls.Add(Me.btnSalir)
        Me.Controls.Add(Me.btnModificar)
        Me.Controls.Add(Me.btnConsultar)
        Me.Controls.Add(Me.btnAceptar)
        Me.Controls.Add(Me.CMBlblBusqueda)
        Me.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Name = "BrwGastos"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Gastos"
        CType(Me.dgvGastos, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pnlDatosGenerales.ResumeLayout(False)
        Me.pnlDatosGenerales.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents VerAcceso2TableAdapter1 As softvFacturacion.ProcedimientosArnoldo3TableAdapters.VerAcceso2TableAdapter
    Friend WithEvents CMBlblTipoGasto As System.Windows.Forms.Label
    Friend WithEvents cmbBuscaTipoGasto As System.Windows.Forms.ComboBox
    Friend WithEvents dgvGastos As System.Windows.Forms.DataGridView
    Friend WithEvents pnlDatosGenerales As System.Windows.Forms.Panel
    Friend WithEvents CMBlblMuestraFecha As System.Windows.Forms.Label
    Friend WithEvents CMBlblIdTipoPago As System.Windows.Forms.Label
    Friend WithEvents CMBlblMuestraIdTipoGasto As System.Windows.Forms.Label
    Friend WithEvents btnSalir As System.Windows.Forms.Button
    Friend WithEvents btnModificar As System.Windows.Forms.Button
    Friend WithEvents btnConsultar As System.Windows.Forms.Button
    Friend WithEvents btnAceptar As System.Windows.Forms.Button
    Friend WithEvents CMBlblBusqueda As System.Windows.Forms.Label
    Friend WithEvents CMBlblBuscaCajera As System.Windows.Forms.Label
    Friend WithEvents cmbBuscaCajera As System.Windows.Forms.ComboBox
    Friend WithEvents cmbBuscaEstatus As System.Windows.Forms.ComboBox
    Friend WithEvents CMBlblEstatus As System.Windows.Forms.Label
    Friend WithEvents btnBuscaDescripcion As System.Windows.Forms.Button
    Friend WithEvents txtBuscaDescripcion As System.Windows.Forms.TextBox
    Friend WithEvents CMBlblBuscaDescripcion As System.Windows.Forms.Label
    Friend WithEvents btnCancelar As System.Windows.Forms.Button
    Friend WithEvents CMBlblCajera As System.Windows.Forms.Label
    Friend WithEvents CMBlblMuestraCajera As System.Windows.Forms.Label
    Friend WithEvents CMBlblFecha As System.Windows.Forms.Label
    Friend WithEvents CMBlblStatus As System.Windows.Forms.Label
    Friend WithEvents CMBlblMuestraStatus As System.Windows.Forms.Label
    Friend WithEvents CMBlblImporte As System.Windows.Forms.Label
    Friend WithEvents CMBlblMuestraImporte As System.Windows.Forms.Label
    Friend WithEvents ClaveGasto As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ActivoGasto As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents fecha As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DescripcionGasto As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents cajera As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents importe As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents estatus As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents idTipoGasto As System.Windows.Forms.DataGridViewTextBoxColumn
End Class
