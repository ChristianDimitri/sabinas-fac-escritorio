<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmMenu
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim CMBNombreLabel As System.Windows.Forms.Label
        Dim CMBCiudadLabel As System.Windows.Forms.Label
        Dim CMBLabelciudad1 As System.Windows.Forms.Label
        Dim LabelUsuario As System.Windows.Forms.Label
        Me.ContextMenuStrip1 = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.FacturacionToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.CajasToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.VentasToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.FacturacionToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.CajasToolStripMenuItem2 = New System.Windows.Forms.ToolStripMenuItem()
        Me.CToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ListadosYAfectacionesToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.VentasToolStripMenuItem2 = New System.Windows.Forms.ToolStripMenuItem()
        Me.NotasDeCréditoToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.CajasWebToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.SalirToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.MenuStrip1 = New System.Windows.Forms.MenuStrip()
        Me.CancelaciónToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.EntregasParcialesToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.DesgloceDeMonedaToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ArqueoDeCajasToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.CancelaciònDeFacturasToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ReImpresionDeFacturasToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.CancelaciónYReimpresiónDeSucursalesEspecialesToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.FACTURAGLOBALToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.FacturaGlobalSucursalesEspecialesToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.RecepciónDelArchivoDeOxxoToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.PolizasToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.PolizasSucursalesEspecialesToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.CobroEquivocadoToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.PerfilesSISTEToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.PerfilesToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.RegistroDeGastosToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.EntregasGlobalesToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ControlDeEfectivoToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.AjustePorDiferenciasToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.FacturasDeAlmacénToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ConciliaciónBancariaToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.RefacturacionToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.RefacturaciónSucursalesEspecialesToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ConciliaciónCLABEToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ConciliaciónSucursalesEspecialesToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.EstadosDeCuentaInternetYComboToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ReportesToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.CortesDeFacturasToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.CortesEspecialesMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.FacturasGlobalesToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ResumenGeneralFacturaGlobalToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ListadoDeEntragasParcialesToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.RelaciónDeIngresosPorConceptosToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.RelacionSucursalesEspecialesMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ListadoDeBonificacionesToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ListadoDeFacturasCanceadasToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ListadoDeFacturasReimpresasToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ListadoDeNotasDeCréditoToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.RelaciónDeClientesToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.DesgloceDeMensualidadesToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.DesgloceDeContratacionesToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.FacturasConCargoAutomaticoToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ListadoDePagosEfectuadosPorElClienteToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ReporteGlobalToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ReporteIngresosTarjetasCréditoDébitoToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.IngresosDeClientesPorUnMontoToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.NúmeroDeBonificacionesToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.CobroErroneoToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ListadoDeCobrosParcialesDeMaterialporConvenioToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ControlDeEfectivoToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.ListadoDeGastosToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ListadoDeEntregasGlobalesToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.RelaciónDeIngresosPorConceptosporSucursalToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.EstadosDeCuentaToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.RelaciónDeIngresosPorConceptosDistribuidorPlazaToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.BonificacionesToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.DetalleDePagosPrimeraVersiónToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ReporteNuevoToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.DetalleDePagosSucursalesEspecialesToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ListadoDeNotasDeCréditoToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.ListadoDeNotasDeCréditoDeSucursalesEspecialesToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.CajasToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.VentasToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.CancelacionDeFacturasToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.PictureBox2 = New System.Windows.Forms.PictureBox()
        Me.MUESTRAIMAGENBindingSource1 = New System.Windows.Forms.BindingSource(Me.components)
        Me.NewsoftvDataSet2 = New softvFacturacion.NewsoftvDataSet2()
        Me.CMBLabel1 = New System.Windows.Forms.Label()
        Me.DameDatosGeneralesBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.NewsoftvDataSet = New softvFacturacion.NewsoftvDataSet()
        Me.CMBLabel2 = New System.Windows.Forms.Label()
        Me.DameDatosGeneralesTableAdapter = New softvFacturacion.NewsoftvDataSetTableAdapters.DameDatosGeneralesTableAdapter()
        Me.DameTipoUsusarioBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.DameTipoUsusarioTableAdapter = New softvFacturacion.NewsoftvDataSetTableAdapters.DameTipoUsusarioTableAdapter()
        Me.ALTASMENUSBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.ALTASMENUSTableAdapter = New softvFacturacion.NewsoftvDataSetTableAdapters.ALTASMENUSTableAdapter()
        Me.MUESTRAIMAGENBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.DameEspecifBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.DameEspecifTableAdapter = New softvFacturacion.NewsoftvDataSet2TableAdapters.DameEspecifTableAdapter()
        Me.MUESTRAIMAGENTableAdapter = New softvFacturacion.NewsoftvDataSet2TableAdapters.MUESTRAIMAGENTableAdapter()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.Procedimientos_arnoldo = New softvFacturacion.Procedimientos_arnoldo()
        Me.DameClv_Session_UsuariosBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.DameClv_Session_UsuariosTableAdapter = New softvFacturacion.Procedimientos_arnoldoTableAdapters.DameClv_Session_UsuariosTableAdapter()
        Me.CMBLabelciudad = New System.Windows.Forms.Label()
        Me.DataSetLydia = New softvFacturacion.DataSetLydia()
        Me.DamePermisosBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.DamePermisosTableAdapter = New softvFacturacion.DataSetLydiaTableAdapters.DamePermisosTableAdapter()
        Me.CMBLabelNombreSistema = New System.Windows.Forms.Label()
        Me.VerAcceso2TableAdapter1 = New softvFacturacion.ProcedimientosArnoldo3TableAdapters.VerAcceso2TableAdapter()
        Me.VerAcceso2TableAdapter2 = New softvFacturacion.ProcedimientosArnoldo3TableAdapters.VerAcceso2TableAdapter()
        Me.VerAcceso2TableAdapter3 = New softvFacturacion.ProcedimientosArnoldo3TableAdapters.VerAcceso2TableAdapter()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.VerAcceso2TableAdapter4 = New softvFacturacion.ProcedimientosArnoldo3TableAdapters.VerAcceso2TableAdapter()
        Me.VerAcceso2TableAdapter5 = New softvFacturacion.ProcedimientosArnoldo3TableAdapters.VerAcceso2TableAdapter()
        Me.VerAcceso2TableAdapter6 = New softvFacturacion.ProcedimientosArnoldo3TableAdapters.VerAcceso2TableAdapter()
        Me.VerAcceso2TableAdapter7 = New softvFacturacion.ProcedimientosArnoldo3TableAdapters.VerAcceso2TableAdapter()
        Me.VerAcceso2TableAdapter8 = New softvFacturacion.ProcedimientosArnoldo3TableAdapters.VerAcceso2TableAdapter()
        Me.VerAcceso2TableAdapter9 = New softvFacturacion.ProcedimientosArnoldo3TableAdapters.VerAcceso2TableAdapter()
        Me.VerAcceso2TableAdapter10 = New softvFacturacion.ProcedimientosArnoldo3TableAdapters.VerAcceso2TableAdapter()
        Me.VerAcceso2TableAdapter11 = New softvFacturacion.ProcedimientosArnoldo3TableAdapters.VerAcceso2TableAdapter()
        Me.LabelNombreUsuario = New System.Windows.Forms.Label()
        Me.FacturaGlobalSucursalesEspecialesToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.Timer1 = New System.Windows.Forms.Timer(Me.components)
        CMBNombreLabel = New System.Windows.Forms.Label()
        CMBCiudadLabel = New System.Windows.Forms.Label()
        CMBLabelciudad1 = New System.Windows.Forms.Label()
        LabelUsuario = New System.Windows.Forms.Label()
        Me.ContextMenuStrip1.SuspendLayout()
        Me.MenuStrip1.SuspendLayout()
        CType(Me.PictureBox2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.MUESTRAIMAGENBindingSource1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.NewsoftvDataSet2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DameDatosGeneralesBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.NewsoftvDataSet, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DameTipoUsusarioBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ALTASMENUSBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.MUESTRAIMAGENBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DameEspecifBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Procedimientos_arnoldo, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DameClv_Session_UsuariosBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DataSetLydia, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DamePermisosBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'CMBNombreLabel
        '
        CMBNombreLabel.AutoSize = True
        CMBNombreLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        CMBNombreLabel.ForeColor = System.Drawing.Color.Gray
        CMBNombreLabel.Location = New System.Drawing.Point(285, 393)
        CMBNombreLabel.Name = "CMBNombreLabel"
        CMBNombreLabel.Size = New System.Drawing.Size(90, 20)
        CMBNombreLabel.TabIndex = 4
        CMBNombreLabel.Text = "Empresa :"
        '
        'CMBCiudadLabel
        '
        CMBCiudadLabel.AutoSize = True
        CMBCiudadLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        CMBCiudadLabel.ForeColor = System.Drawing.Color.Gray
        CMBCiudadLabel.Location = New System.Drawing.Point(312, 432)
        CMBCiudadLabel.Name = "CMBCiudadLabel"
        CMBCiudadLabel.Size = New System.Drawing.Size(63, 20)
        CMBCiudadLabel.TabIndex = 6
        CMBCiudadLabel.Text = "Plaza :"
        '
        'CMBLabelciudad1
        '
        CMBLabelciudad1.AutoSize = True
        CMBLabelciudad1.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        CMBLabelciudad1.ForeColor = System.Drawing.Color.Gray
        CMBLabelciudad1.Location = New System.Drawing.Point(670, 500)
        CMBLabelciudad1.Name = "CMBLabelciudad1"
        CMBLabelciudad1.Size = New System.Drawing.Size(75, 20)
        CMBLabelciudad1.TabIndex = 10
        CMBLabelciudad1.Text = "Ciudad :"
        CMBLabelciudad1.Visible = False
        '
        'LabelUsuario
        '
        LabelUsuario.AutoSize = True
        LabelUsuario.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        LabelUsuario.ForeColor = System.Drawing.Color.Gray
        LabelUsuario.Location = New System.Drawing.Point(23, 614)
        LabelUsuario.Name = "LabelUsuario"
        LabelUsuario.Size = New System.Drawing.Size(81, 20)
        LabelUsuario.TabIndex = 17
        LabelUsuario.Text = "Usuario :"
        '
        'ContextMenuStrip1
        '
        Me.ContextMenuStrip1.ImageScalingSize = New System.Drawing.Size(20, 20)
        Me.ContextMenuStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.FacturacionToolStripMenuItem})
        Me.ContextMenuStrip1.Name = "ContextMenuStrip1"
        Me.ContextMenuStrip1.Size = New System.Drawing.Size(137, 26)
        '
        'FacturacionToolStripMenuItem
        '
        Me.FacturacionToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.CajasToolStripMenuItem1, Me.VentasToolStripMenuItem1})
        Me.FacturacionToolStripMenuItem.Name = "FacturacionToolStripMenuItem"
        Me.FacturacionToolStripMenuItem.Size = New System.Drawing.Size(136, 22)
        Me.FacturacionToolStripMenuItem.Text = "Facturacion"
        '
        'CajasToolStripMenuItem1
        '
        Me.CajasToolStripMenuItem1.Name = "CajasToolStripMenuItem1"
        Me.CajasToolStripMenuItem1.Size = New System.Drawing.Size(108, 22)
        Me.CajasToolStripMenuItem1.Text = "Cajas"
        '
        'VentasToolStripMenuItem1
        '
        Me.VentasToolStripMenuItem1.Name = "VentasToolStripMenuItem1"
        Me.VentasToolStripMenuItem1.Size = New System.Drawing.Size(108, 22)
        Me.VentasToolStripMenuItem1.Text = "Ventas"
        '
        'FacturacionToolStripMenuItem1
        '
        Me.FacturacionToolStripMenuItem1.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.CajasToolStripMenuItem2, Me.CToolStripMenuItem, Me.VentasToolStripMenuItem2, Me.NotasDeCréditoToolStripMenuItem, Me.CajasWebToolStripMenuItem})
        Me.FacturacionToolStripMenuItem1.ForeColor = System.Drawing.Color.Black
        Me.FacturacionToolStripMenuItem1.Name = "FacturacionToolStripMenuItem1"
        Me.FacturacionToolStripMenuItem1.Size = New System.Drawing.Size(108, 22)
        Me.FacturacionToolStripMenuItem1.Text = "&Facturación"
        '
        'CajasToolStripMenuItem2
        '
        Me.CajasToolStripMenuItem2.Name = "CajasToolStripMenuItem2"
        Me.CajasToolStripMenuItem2.Size = New System.Drawing.Size(224, 22)
        Me.CajasToolStripMenuItem2.Text = "Cajas"
        '
        'CToolStripMenuItem
        '
        Me.CToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ListadosYAfectacionesToolStripMenuItem})
        Me.CToolStripMenuItem.Name = "CToolStripMenuItem"
        Me.CToolStripMenuItem.Size = New System.Drawing.Size(224, 22)
        Me.CToolStripMenuItem.Text = "&Cargos Automaticos"
        '
        'ListadosYAfectacionesToolStripMenuItem
        '
        Me.ListadosYAfectacionesToolStripMenuItem.Name = "ListadosYAfectacionesToolStripMenuItem"
        Me.ListadosYAfectacionesToolStripMenuItem.Size = New System.Drawing.Size(252, 22)
        Me.ListadosYAfectacionesToolStripMenuItem.Text = "Listados y Afectaciones"
        '
        'VentasToolStripMenuItem2
        '
        Me.VentasToolStripMenuItem2.Name = "VentasToolStripMenuItem2"
        Me.VentasToolStripMenuItem2.Size = New System.Drawing.Size(224, 22)
        Me.VentasToolStripMenuItem2.Text = "Ventas"
        '
        'NotasDeCréditoToolStripMenuItem
        '
        Me.NotasDeCréditoToolStripMenuItem.Name = "NotasDeCréditoToolStripMenuItem"
        Me.NotasDeCréditoToolStripMenuItem.Size = New System.Drawing.Size(224, 22)
        Me.NotasDeCréditoToolStripMenuItem.Text = "Notas De Crédito"
        Me.NotasDeCréditoToolStripMenuItem.Visible = False
        '
        'CajasWebToolStripMenuItem
        '
        Me.CajasWebToolStripMenuItem.Name = "CajasWebToolStripMenuItem"
        Me.CajasWebToolStripMenuItem.Size = New System.Drawing.Size(224, 22)
        Me.CajasWebToolStripMenuItem.Text = "Facturación Web"
        '
        'SalirToolStripMenuItem
        '
        Me.SalirToolStripMenuItem.ForeColor = System.Drawing.SystemColors.HighlightText
        Me.SalirToolStripMenuItem.Name = "SalirToolStripMenuItem"
        Me.SalirToolStripMenuItem.Size = New System.Drawing.Size(56, 22)
        Me.SalirToolStripMenuItem.Text = "&Salir"
        '
        'MenuStrip1
        '
        Me.MenuStrip1.BackColor = System.Drawing.Color.Orange
        Me.MenuStrip1.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.MenuStrip1.ImageScalingSize = New System.Drawing.Size(20, 20)
        Me.MenuStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.FacturacionToolStripMenuItem1, Me.CancelaciónToolStripMenuItem, Me.ReportesToolStripMenuItem, Me.SalirToolStripMenuItem})
        Me.MenuStrip1.LayoutStyle = System.Windows.Forms.ToolStripLayoutStyle.Flow
        Me.MenuStrip1.Location = New System.Drawing.Point(0, 0)
        Me.MenuStrip1.Name = "MenuStrip1"
        Me.MenuStrip1.Size = New System.Drawing.Size(1016, 26)
        Me.MenuStrip1.TabIndex = 0
        Me.MenuStrip1.TabStop = True
        Me.MenuStrip1.Text = "MenuStrip1"
        '
        'CancelaciónToolStripMenuItem
        '
        Me.CancelaciónToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.EntregasParcialesToolStripMenuItem1, Me.DesgloceDeMonedaToolStripMenuItem, Me.ArqueoDeCajasToolStripMenuItem1, Me.CancelaciònDeFacturasToolStripMenuItem, Me.ReImpresionDeFacturasToolStripMenuItem, Me.CancelaciónYReimpresiónDeSucursalesEspecialesToolStripMenuItem, Me.FACTURAGLOBALToolStripMenuItem, Me.FacturaGlobalSucursalesEspecialesToolStripMenuItem1, Me.RecepciónDelArchivoDeOxxoToolStripMenuItem, Me.PolizasToolStripMenuItem, Me.PolizasSucursalesEspecialesToolStripMenuItem, Me.CobroEquivocadoToolStripMenuItem, Me.PerfilesSISTEToolStripMenuItem, Me.PerfilesToolStripMenuItem, Me.RegistroDeGastosToolStripMenuItem, Me.EntregasGlobalesToolStripMenuItem, Me.ControlDeEfectivoToolStripMenuItem, Me.AjustePorDiferenciasToolStripMenuItem, Me.FacturasDeAlmacénToolStripMenuItem, Me.ConciliaciónBancariaToolStripMenuItem, Me.RefacturacionToolStripMenuItem, Me.RefacturaciónSucursalesEspecialesToolStripMenuItem, Me.ConciliaciónCLABEToolStripMenuItem, Me.ConciliaciónSucursalesEspecialesToolStripMenuItem, Me.EstadosDeCuentaInternetYComboToolStripMenuItem})
        Me.CancelaciónToolStripMenuItem.Name = "CancelaciónToolStripMenuItem"
        Me.CancelaciónToolStripMenuItem.Size = New System.Drawing.Size(88, 22)
        Me.CancelaciónToolStripMenuItem.Text = "&Procesos"
        '
        'EntregasParcialesToolStripMenuItem1
        '
        Me.EntregasParcialesToolStripMenuItem1.Name = "EntregasParcialesToolStripMenuItem1"
        Me.EntregasParcialesToolStripMenuItem1.Size = New System.Drawing.Size(468, 22)
        Me.EntregasParcialesToolStripMenuItem1.Text = "Entregas Parciales"
        '
        'DesgloceDeMonedaToolStripMenuItem
        '
        Me.DesgloceDeMonedaToolStripMenuItem.Name = "DesgloceDeMonedaToolStripMenuItem"
        Me.DesgloceDeMonedaToolStripMenuItem.Size = New System.Drawing.Size(468, 22)
        Me.DesgloceDeMonedaToolStripMenuItem.Text = "Desgloce de Moneda"
        '
        'ArqueoDeCajasToolStripMenuItem1
        '
        Me.ArqueoDeCajasToolStripMenuItem1.Name = "ArqueoDeCajasToolStripMenuItem1"
        Me.ArqueoDeCajasToolStripMenuItem1.Size = New System.Drawing.Size(468, 22)
        Me.ArqueoDeCajasToolStripMenuItem1.Text = "Arqueo de Cajas"
        '
        'CancelaciònDeFacturasToolStripMenuItem
        '
        Me.CancelaciònDeFacturasToolStripMenuItem.Name = "CancelaciònDeFacturasToolStripMenuItem"
        Me.CancelaciònDeFacturasToolStripMenuItem.Size = New System.Drawing.Size(468, 22)
        Me.CancelaciònDeFacturasToolStripMenuItem.Text = "Cancelación de Tickets"
        '
        'ReImpresionDeFacturasToolStripMenuItem
        '
        Me.ReImpresionDeFacturasToolStripMenuItem.Name = "ReImpresionDeFacturasToolStripMenuItem"
        Me.ReImpresionDeFacturasToolStripMenuItem.Size = New System.Drawing.Size(468, 22)
        Me.ReImpresionDeFacturasToolStripMenuItem.Text = "ReImpresion de Tickets"
        '
        'CancelaciónYReimpresiónDeSucursalesEspecialesToolStripMenuItem
        '
        Me.CancelaciónYReimpresiónDeSucursalesEspecialesToolStripMenuItem.Name = "CancelaciónYReimpresiónDeSucursalesEspecialesToolStripMenuItem"
        Me.CancelaciónYReimpresiónDeSucursalesEspecialesToolStripMenuItem.Size = New System.Drawing.Size(468, 22)
        Me.CancelaciónYReimpresiónDeSucursalesEspecialesToolStripMenuItem.Text = "Cancelación y Reimpresión de Sucursales Especiales"
        '
        'FACTURAGLOBALToolStripMenuItem
        '
        Me.FACTURAGLOBALToolStripMenuItem.Name = "FACTURAGLOBALToolStripMenuItem"
        Me.FACTURAGLOBALToolStripMenuItem.Size = New System.Drawing.Size(468, 22)
        Me.FACTURAGLOBALToolStripMenuItem.Text = "Factura Global"
        '
        'FacturaGlobalSucursalesEspecialesToolStripMenuItem1
        '
        Me.FacturaGlobalSucursalesEspecialesToolStripMenuItem1.Name = "FacturaGlobalSucursalesEspecialesToolStripMenuItem1"
        Me.FacturaGlobalSucursalesEspecialesToolStripMenuItem1.Size = New System.Drawing.Size(468, 22)
        Me.FacturaGlobalSucursalesEspecialesToolStripMenuItem1.Text = "Factura Global Sucursales Especiales"
        '
        'RecepciónDelArchivoDeOxxoToolStripMenuItem
        '
        Me.RecepciónDelArchivoDeOxxoToolStripMenuItem.Name = "RecepciónDelArchivoDeOxxoToolStripMenuItem"
        Me.RecepciónDelArchivoDeOxxoToolStripMenuItem.Size = New System.Drawing.Size(468, 22)
        Me.RecepciónDelArchivoDeOxxoToolStripMenuItem.Text = "Recepción del Archivo de Oxxo"
        Me.RecepciónDelArchivoDeOxxoToolStripMenuItem.Visible = False
        '
        'PolizasToolStripMenuItem
        '
        Me.PolizasToolStripMenuItem.Name = "PolizasToolStripMenuItem"
        Me.PolizasToolStripMenuItem.Size = New System.Drawing.Size(468, 22)
        Me.PolizasToolStripMenuItem.Text = "Polizas"
        '
        'PolizasSucursalesEspecialesToolStripMenuItem
        '
        Me.PolizasSucursalesEspecialesToolStripMenuItem.Name = "PolizasSucursalesEspecialesToolStripMenuItem"
        Me.PolizasSucursalesEspecialesToolStripMenuItem.Size = New System.Drawing.Size(468, 22)
        Me.PolizasSucursalesEspecialesToolStripMenuItem.Text = "Polizas Sucursales Especiales"
        '
        'CobroEquivocadoToolStripMenuItem
        '
        Me.CobroEquivocadoToolStripMenuItem.Name = "CobroEquivocadoToolStripMenuItem"
        Me.CobroEquivocadoToolStripMenuItem.Size = New System.Drawing.Size(468, 22)
        Me.CobroEquivocadoToolStripMenuItem.Text = "Corrección de Pagos Equivocados"
        Me.CobroEquivocadoToolStripMenuItem.Visible = False
        '
        'PerfilesSISTEToolStripMenuItem
        '
        Me.PerfilesSISTEToolStripMenuItem.Name = "PerfilesSISTEToolStripMenuItem"
        Me.PerfilesSISTEToolStripMenuItem.Size = New System.Drawing.Size(468, 22)
        Me.PerfilesSISTEToolStripMenuItem.Text = "Perfiles (SISTE)"
        '
        'PerfilesToolStripMenuItem
        '
        Me.PerfilesToolStripMenuItem.Name = "PerfilesToolStripMenuItem"
        Me.PerfilesToolStripMenuItem.Size = New System.Drawing.Size(468, 22)
        Me.PerfilesToolStripMenuItem.Text = "Perfiles"
        '
        'RegistroDeGastosToolStripMenuItem
        '
        Me.RegistroDeGastosToolStripMenuItem.Name = "RegistroDeGastosToolStripMenuItem"
        Me.RegistroDeGastosToolStripMenuItem.Size = New System.Drawing.Size(468, 22)
        Me.RegistroDeGastosToolStripMenuItem.Text = "Registro de Gastos"
        Me.RegistroDeGastosToolStripMenuItem.Visible = False
        '
        'EntregasGlobalesToolStripMenuItem
        '
        Me.EntregasGlobalesToolStripMenuItem.Name = "EntregasGlobalesToolStripMenuItem"
        Me.EntregasGlobalesToolStripMenuItem.Size = New System.Drawing.Size(468, 22)
        Me.EntregasGlobalesToolStripMenuItem.Text = "Entregas Globales"
        Me.EntregasGlobalesToolStripMenuItem.Visible = False
        '
        'ControlDeEfectivoToolStripMenuItem
        '
        Me.ControlDeEfectivoToolStripMenuItem.Name = "ControlDeEfectivoToolStripMenuItem"
        Me.ControlDeEfectivoToolStripMenuItem.Size = New System.Drawing.Size(468, 22)
        Me.ControlDeEfectivoToolStripMenuItem.Text = "Control de Efectivo"
        Me.ControlDeEfectivoToolStripMenuItem.Visible = False
        '
        'AjustePorDiferenciasToolStripMenuItem
        '
        Me.AjustePorDiferenciasToolStripMenuItem.Name = "AjustePorDiferenciasToolStripMenuItem"
        Me.AjustePorDiferenciasToolStripMenuItem.Size = New System.Drawing.Size(468, 22)
        Me.AjustePorDiferenciasToolStripMenuItem.Text = "Ajuste por Diferencias"
        Me.AjustePorDiferenciasToolStripMenuItem.Visible = False
        '
        'FacturasDeAlmacénToolStripMenuItem
        '
        Me.FacturasDeAlmacénToolStripMenuItem.Name = "FacturasDeAlmacénToolStripMenuItem"
        Me.FacturasDeAlmacénToolStripMenuItem.Size = New System.Drawing.Size(468, 22)
        Me.FacturasDeAlmacénToolStripMenuItem.Text = "Facturas de Almacén"
        '
        'ConciliaciónBancariaToolStripMenuItem
        '
        Me.ConciliaciónBancariaToolStripMenuItem.Name = "ConciliaciónBancariaToolStripMenuItem"
        Me.ConciliaciónBancariaToolStripMenuItem.Size = New System.Drawing.Size(468, 22)
        Me.ConciliaciónBancariaToolStripMenuItem.Text = "Conciliación Bancaria"
        '
        'RefacturacionToolStripMenuItem
        '
        Me.RefacturacionToolStripMenuItem.Name = "RefacturacionToolStripMenuItem"
        Me.RefacturacionToolStripMenuItem.Size = New System.Drawing.Size(468, 22)
        Me.RefacturacionToolStripMenuItem.Text = "Refacturacion "
        '
        'RefacturaciónSucursalesEspecialesToolStripMenuItem
        '
        Me.RefacturaciónSucursalesEspecialesToolStripMenuItem.Name = "RefacturaciónSucursalesEspecialesToolStripMenuItem"
        Me.RefacturaciónSucursalesEspecialesToolStripMenuItem.Size = New System.Drawing.Size(468, 22)
        Me.RefacturaciónSucursalesEspecialesToolStripMenuItem.Text = "Refacturación Sucursales Especiales"
        '
        'ConciliaciónCLABEToolStripMenuItem
        '
        Me.ConciliaciónCLABEToolStripMenuItem.Name = "ConciliaciónCLABEToolStripMenuItem"
        Me.ConciliaciónCLABEToolStripMenuItem.Size = New System.Drawing.Size(468, 22)
        Me.ConciliaciónCLABEToolStripMenuItem.Text = "Conciliación CLABE"
        '
        'ConciliaciónSucursalesEspecialesToolStripMenuItem
        '
        Me.ConciliaciónSucursalesEspecialesToolStripMenuItem.Name = "ConciliaciónSucursalesEspecialesToolStripMenuItem"
        Me.ConciliaciónSucursalesEspecialesToolStripMenuItem.Size = New System.Drawing.Size(468, 22)
        Me.ConciliaciónSucursalesEspecialesToolStripMenuItem.Text = "Conciliación Sucursales Especiales"
        '
        'EstadosDeCuentaInternetYComboToolStripMenuItem
        '
        Me.EstadosDeCuentaInternetYComboToolStripMenuItem.Name = "EstadosDeCuentaInternetYComboToolStripMenuItem"
        Me.EstadosDeCuentaInternetYComboToolStripMenuItem.Size = New System.Drawing.Size(468, 22)
        Me.EstadosDeCuentaInternetYComboToolStripMenuItem.Text = "Estados de Cuenta Internet y Combo"
        '
        'ReportesToolStripMenuItem
        '
        Me.ReportesToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.CortesDeFacturasToolStripMenuItem, Me.CortesEspecialesMenuItem1, Me.FacturasGlobalesToolStripMenuItem, Me.ResumenGeneralFacturaGlobalToolStripMenuItem, Me.ListadoDeEntragasParcialesToolStripMenuItem, Me.RelaciónDeIngresosPorConceptosToolStripMenuItem, Me.RelacionSucursalesEspecialesMenuItem, Me.ListadoDeBonificacionesToolStripMenuItem, Me.ListadoDeFacturasCanceadasToolStripMenuItem, Me.ListadoDeFacturasReimpresasToolStripMenuItem, Me.ListadoDeNotasDeCréditoToolStripMenuItem, Me.RelaciónDeClientesToolStripMenuItem, Me.DesgloceDeMensualidadesToolStripMenuItem, Me.DesgloceDeContratacionesToolStripMenuItem, Me.FacturasConCargoAutomaticoToolStripMenuItem, Me.ListadoDePagosEfectuadosPorElClienteToolStripMenuItem, Me.ReporteGlobalToolStripMenuItem, Me.ReporteIngresosTarjetasCréditoDébitoToolStripMenuItem, Me.IngresosDeClientesPorUnMontoToolStripMenuItem, Me.NúmeroDeBonificacionesToolStripMenuItem, Me.CobroErroneoToolStripMenuItem, Me.ListadoDeCobrosParcialesDeMaterialporConvenioToolStripMenuItem, Me.ControlDeEfectivoToolStripMenuItem1, Me.ListadoDeGastosToolStripMenuItem, Me.ListadoDeEntregasGlobalesToolStripMenuItem, Me.RelaciónDeIngresosPorConceptosporSucursalToolStripMenuItem, Me.EstadosDeCuentaToolStripMenuItem1, Me.RelaciónDeIngresosPorConceptosDistribuidorPlazaToolStripMenuItem, Me.BonificacionesToolStripMenuItem, Me.DetalleDePagosPrimeraVersiónToolStripMenuItem, Me.ReporteNuevoToolStripMenuItem, Me.DetalleDePagosSucursalesEspecialesToolStripMenuItem, Me.ListadoDeNotasDeCréditoToolStripMenuItem1, Me.ListadoDeNotasDeCréditoDeSucursalesEspecialesToolStripMenuItem})
        Me.ReportesToolStripMenuItem.Name = "ReportesToolStripMenuItem"
        Me.ReportesToolStripMenuItem.Size = New System.Drawing.Size(88, 22)
        Me.ReportesToolStripMenuItem.Text = "&Reportes"
        '
        'CortesDeFacturasToolStripMenuItem
        '
        Me.CortesDeFacturasToolStripMenuItem.Name = "CortesDeFacturasToolStripMenuItem"
        Me.CortesDeFacturasToolStripMenuItem.Size = New System.Drawing.Size(488, 22)
        Me.CortesDeFacturasToolStripMenuItem.Text = "Cortes "
        '
        'CortesEspecialesMenuItem1
        '
        Me.CortesEspecialesMenuItem1.Name = "CortesEspecialesMenuItem1"
        Me.CortesEspecialesMenuItem1.Size = New System.Drawing.Size(488, 22)
        Me.CortesEspecialesMenuItem1.Text = "Cortes Sucursales Especiales"
        '
        'FacturasGlobalesToolStripMenuItem
        '
        Me.FacturasGlobalesToolStripMenuItem.Name = "FacturasGlobalesToolStripMenuItem"
        Me.FacturasGlobalesToolStripMenuItem.Size = New System.Drawing.Size(488, 22)
        Me.FacturasGlobalesToolStripMenuItem.Text = "Facturas Globales"
        '
        'ResumenGeneralFacturaGlobalToolStripMenuItem
        '
        Me.ResumenGeneralFacturaGlobalToolStripMenuItem.Name = "ResumenGeneralFacturaGlobalToolStripMenuItem"
        Me.ResumenGeneralFacturaGlobalToolStripMenuItem.Size = New System.Drawing.Size(488, 22)
        Me.ResumenGeneralFacturaGlobalToolStripMenuItem.Text = "Resumen General  Factura Global"
        '
        'ListadoDeEntragasParcialesToolStripMenuItem
        '
        Me.ListadoDeEntragasParcialesToolStripMenuItem.Name = "ListadoDeEntragasParcialesToolStripMenuItem"
        Me.ListadoDeEntragasParcialesToolStripMenuItem.Size = New System.Drawing.Size(488, 22)
        Me.ListadoDeEntragasParcialesToolStripMenuItem.Text = "Listado de Entregas Parciales"
        '
        'RelaciónDeIngresosPorConceptosToolStripMenuItem
        '
        Me.RelaciónDeIngresosPorConceptosToolStripMenuItem.Name = "RelaciónDeIngresosPorConceptosToolStripMenuItem"
        Me.RelaciónDeIngresosPorConceptosToolStripMenuItem.Size = New System.Drawing.Size(488, 22)
        Me.RelaciónDeIngresosPorConceptosToolStripMenuItem.Text = "Relación de Ingresos por Conceptos (por Ciudad)"
        '
        'RelacionSucursalesEspecialesMenuItem
        '
        Me.RelacionSucursalesEspecialesMenuItem.Name = "RelacionSucursalesEspecialesMenuItem"
        Me.RelacionSucursalesEspecialesMenuItem.Size = New System.Drawing.Size(488, 22)
        Me.RelacionSucursalesEspecialesMenuItem.Text = "Relación de Ingresos Sucursales Especiales"
        '
        'ListadoDeBonificacionesToolStripMenuItem
        '
        Me.ListadoDeBonificacionesToolStripMenuItem.Name = "ListadoDeBonificacionesToolStripMenuItem"
        Me.ListadoDeBonificacionesToolStripMenuItem.Size = New System.Drawing.Size(488, 22)
        Me.ListadoDeBonificacionesToolStripMenuItem.Text = "Listado de Bonificaciones"
        '
        'ListadoDeFacturasCanceadasToolStripMenuItem
        '
        Me.ListadoDeFacturasCanceadasToolStripMenuItem.Name = "ListadoDeFacturasCanceadasToolStripMenuItem"
        Me.ListadoDeFacturasCanceadasToolStripMenuItem.Size = New System.Drawing.Size(488, 22)
        Me.ListadoDeFacturasCanceadasToolStripMenuItem.Text = "Listado de Tickets Canceladas"
        '
        'ListadoDeFacturasReimpresasToolStripMenuItem
        '
        Me.ListadoDeFacturasReimpresasToolStripMenuItem.Name = "ListadoDeFacturasReimpresasToolStripMenuItem"
        Me.ListadoDeFacturasReimpresasToolStripMenuItem.Size = New System.Drawing.Size(488, 22)
        Me.ListadoDeFacturasReimpresasToolStripMenuItem.Text = "Listado de Tickets Reimpresas"
        '
        'ListadoDeNotasDeCréditoToolStripMenuItem
        '
        Me.ListadoDeNotasDeCréditoToolStripMenuItem.Name = "ListadoDeNotasDeCréditoToolStripMenuItem"
        Me.ListadoDeNotasDeCréditoToolStripMenuItem.Size = New System.Drawing.Size(488, 22)
        Me.ListadoDeNotasDeCréditoToolStripMenuItem.Text = "Listado de Notas de Crédito"
        Me.ListadoDeNotasDeCréditoToolStripMenuItem.Visible = False
        '
        'RelaciónDeClientesToolStripMenuItem
        '
        Me.RelaciónDeClientesToolStripMenuItem.Name = "RelaciónDeClientesToolStripMenuItem"
        Me.RelaciónDeClientesToolStripMenuItem.Size = New System.Drawing.Size(488, 22)
        Me.RelaciónDeClientesToolStripMenuItem.Text = "Relación de Pagos Adelantados"
        Me.RelaciónDeClientesToolStripMenuItem.Visible = False
        '
        'DesgloceDeMensualidadesToolStripMenuItem
        '
        Me.DesgloceDeMensualidadesToolStripMenuItem.Name = "DesgloceDeMensualidadesToolStripMenuItem"
        Me.DesgloceDeMensualidadesToolStripMenuItem.Size = New System.Drawing.Size(488, 22)
        Me.DesgloceDeMensualidadesToolStripMenuItem.Text = "Desgloce de Mensualidades"
        Me.DesgloceDeMensualidadesToolStripMenuItem.Visible = False
        '
        'DesgloceDeContratacionesToolStripMenuItem
        '
        Me.DesgloceDeContratacionesToolStripMenuItem.Name = "DesgloceDeContratacionesToolStripMenuItem"
        Me.DesgloceDeContratacionesToolStripMenuItem.Size = New System.Drawing.Size(488, 22)
        Me.DesgloceDeContratacionesToolStripMenuItem.Text = "Desgloce de Contrataciones"
        Me.DesgloceDeContratacionesToolStripMenuItem.Visible = False
        '
        'FacturasConCargoAutomaticoToolStripMenuItem
        '
        Me.FacturasConCargoAutomaticoToolStripMenuItem.Name = "FacturasConCargoAutomaticoToolStripMenuItem"
        Me.FacturasConCargoAutomaticoToolStripMenuItem.Size = New System.Drawing.Size(488, 22)
        Me.FacturasConCargoAutomaticoToolStripMenuItem.Text = "Facturas Con Cargo Automatico"
        '
        'ListadoDePagosEfectuadosPorElClienteToolStripMenuItem
        '
        Me.ListadoDePagosEfectuadosPorElClienteToolStripMenuItem.Name = "ListadoDePagosEfectuadosPorElClienteToolStripMenuItem"
        Me.ListadoDePagosEfectuadosPorElClienteToolStripMenuItem.Size = New System.Drawing.Size(488, 22)
        Me.ListadoDePagosEfectuadosPorElClienteToolStripMenuItem.Text = "Listado De Pagos Efectuados Por El Cliente"
        Me.ListadoDePagosEfectuadosPorElClienteToolStripMenuItem.Visible = False
        '
        'ReporteGlobalToolStripMenuItem
        '
        Me.ReporteGlobalToolStripMenuItem.Name = "ReporteGlobalToolStripMenuItem"
        Me.ReporteGlobalToolStripMenuItem.Size = New System.Drawing.Size(488, 22)
        Me.ReporteGlobalToolStripMenuItem.Text = "Reporte Global"
        Me.ReporteGlobalToolStripMenuItem.Visible = False
        '
        'ReporteIngresosTarjetasCréditoDébitoToolStripMenuItem
        '
        Me.ReporteIngresosTarjetasCréditoDébitoToolStripMenuItem.Name = "ReporteIngresosTarjetasCréditoDébitoToolStripMenuItem"
        Me.ReporteIngresosTarjetasCréditoDébitoToolStripMenuItem.Size = New System.Drawing.Size(488, 22)
        Me.ReporteIngresosTarjetasCréditoDébitoToolStripMenuItem.Text = "Reporte Ingresos Tarjetas Crédito/Débito"
        '
        'IngresosDeClientesPorUnMontoToolStripMenuItem
        '
        Me.IngresosDeClientesPorUnMontoToolStripMenuItem.Name = "IngresosDeClientesPorUnMontoToolStripMenuItem"
        Me.IngresosDeClientesPorUnMontoToolStripMenuItem.Size = New System.Drawing.Size(488, 22)
        Me.IngresosDeClientesPorUnMontoToolStripMenuItem.Text = "Ingresos de Clientes por un Monto"
        '
        'NúmeroDeBonificacionesToolStripMenuItem
        '
        Me.NúmeroDeBonificacionesToolStripMenuItem.Name = "NúmeroDeBonificacionesToolStripMenuItem"
        Me.NúmeroDeBonificacionesToolStripMenuItem.Size = New System.Drawing.Size(488, 22)
        Me.NúmeroDeBonificacionesToolStripMenuItem.Text = "Número de Bonificaciones"
        '
        'CobroErroneoToolStripMenuItem
        '
        Me.CobroErroneoToolStripMenuItem.Name = "CobroErroneoToolStripMenuItem"
        Me.CobroErroneoToolStripMenuItem.Size = New System.Drawing.Size(488, 22)
        Me.CobroErroneoToolStripMenuItem.Text = "Cobro Corrección de Pagos Equivocados"
        Me.CobroErroneoToolStripMenuItem.Visible = False
        '
        'ListadoDeCobrosParcialesDeMaterialporConvenioToolStripMenuItem
        '
        Me.ListadoDeCobrosParcialesDeMaterialporConvenioToolStripMenuItem.Name = "ListadoDeCobrosParcialesDeMaterialporConvenioToolStripMenuItem"
        Me.ListadoDeCobrosParcialesDeMaterialporConvenioToolStripMenuItem.Size = New System.Drawing.Size(488, 22)
        Me.ListadoDeCobrosParcialesDeMaterialporConvenioToolStripMenuItem.Text = "Listado de Cobros Parciales de Material (por Convenio)"
        '
        'ControlDeEfectivoToolStripMenuItem1
        '
        Me.ControlDeEfectivoToolStripMenuItem1.Name = "ControlDeEfectivoToolStripMenuItem1"
        Me.ControlDeEfectivoToolStripMenuItem1.Size = New System.Drawing.Size(488, 22)
        Me.ControlDeEfectivoToolStripMenuItem1.Text = "Control de Efectivo"
        Me.ControlDeEfectivoToolStripMenuItem1.Visible = False
        '
        'ListadoDeGastosToolStripMenuItem
        '
        Me.ListadoDeGastosToolStripMenuItem.Name = "ListadoDeGastosToolStripMenuItem"
        Me.ListadoDeGastosToolStripMenuItem.Size = New System.Drawing.Size(488, 22)
        Me.ListadoDeGastosToolStripMenuItem.Text = "Listado de Gastos"
        Me.ListadoDeGastosToolStripMenuItem.Visible = False
        '
        'ListadoDeEntregasGlobalesToolStripMenuItem
        '
        Me.ListadoDeEntregasGlobalesToolStripMenuItem.Name = "ListadoDeEntregasGlobalesToolStripMenuItem"
        Me.ListadoDeEntregasGlobalesToolStripMenuItem.Size = New System.Drawing.Size(488, 22)
        Me.ListadoDeEntregasGlobalesToolStripMenuItem.Text = "Listado de Entregas Globales"
        Me.ListadoDeEntregasGlobalesToolStripMenuItem.Visible = False
        '
        'RelaciónDeIngresosPorConceptosporSucursalToolStripMenuItem
        '
        Me.RelaciónDeIngresosPorConceptosporSucursalToolStripMenuItem.Name = "RelaciónDeIngresosPorConceptosporSucursalToolStripMenuItem"
        Me.RelaciónDeIngresosPorConceptosporSucursalToolStripMenuItem.Size = New System.Drawing.Size(488, 22)
        Me.RelaciónDeIngresosPorConceptosporSucursalToolStripMenuItem.Text = "Relación de Ingresos por Conceptos (por Sucursal)"
        '
        'EstadosDeCuentaToolStripMenuItem1
        '
        Me.EstadosDeCuentaToolStripMenuItem1.Name = "EstadosDeCuentaToolStripMenuItem1"
        Me.EstadosDeCuentaToolStripMenuItem1.Size = New System.Drawing.Size(488, 22)
        Me.EstadosDeCuentaToolStripMenuItem1.Text = "Estados de Cuenta"
        '
        'RelaciónDeIngresosPorConceptosDistribuidorPlazaToolStripMenuItem
        '
        Me.RelaciónDeIngresosPorConceptosDistribuidorPlazaToolStripMenuItem.Name = "RelaciónDeIngresosPorConceptosDistribuidorPlazaToolStripMenuItem"
        Me.RelaciónDeIngresosPorConceptosDistribuidorPlazaToolStripMenuItem.Size = New System.Drawing.Size(488, 22)
        Me.RelaciónDeIngresosPorConceptosDistribuidorPlazaToolStripMenuItem.Text = "Relación de Ingresos por Conceptos"
        '
        'BonificacionesToolStripMenuItem
        '
        Me.BonificacionesToolStripMenuItem.Name = "BonificacionesToolStripMenuItem"
        Me.BonificacionesToolStripMenuItem.Size = New System.Drawing.Size(488, 22)
        Me.BonificacionesToolStripMenuItem.Text = "Bonificaciones"
        '
        'DetalleDePagosPrimeraVersiónToolStripMenuItem
        '
        Me.DetalleDePagosPrimeraVersiónToolStripMenuItem.Name = "DetalleDePagosPrimeraVersiónToolStripMenuItem"
        Me.DetalleDePagosPrimeraVersiónToolStripMenuItem.Size = New System.Drawing.Size(488, 22)
        Me.DetalleDePagosPrimeraVersiónToolStripMenuItem.Text = "Detalle de Pagos (Primera versión)"
        '
        'ReporteNuevoToolStripMenuItem
        '
        Me.ReporteNuevoToolStripMenuItem.Name = "ReporteNuevoToolStripMenuItem"
        Me.ReporteNuevoToolStripMenuItem.Size = New System.Drawing.Size(488, 22)
        Me.ReporteNuevoToolStripMenuItem.Text = "Detalle de Pagos"
        '
        'DetalleDePagosSucursalesEspecialesToolStripMenuItem
        '
        Me.DetalleDePagosSucursalesEspecialesToolStripMenuItem.Name = "DetalleDePagosSucursalesEspecialesToolStripMenuItem"
        Me.DetalleDePagosSucursalesEspecialesToolStripMenuItem.Size = New System.Drawing.Size(488, 22)
        Me.DetalleDePagosSucursalesEspecialesToolStripMenuItem.Text = "Detalle de Pagos Sucursales Especiales"
        '
        'ListadoDeNotasDeCréditoToolStripMenuItem1
        '
        Me.ListadoDeNotasDeCréditoToolStripMenuItem1.Name = "ListadoDeNotasDeCréditoToolStripMenuItem1"
        Me.ListadoDeNotasDeCréditoToolStripMenuItem1.Size = New System.Drawing.Size(488, 22)
        Me.ListadoDeNotasDeCréditoToolStripMenuItem1.Text = "Listado de Notas de Crédito"
        '
        'ListadoDeNotasDeCréditoDeSucursalesEspecialesToolStripMenuItem
        '
        Me.ListadoDeNotasDeCréditoDeSucursalesEspecialesToolStripMenuItem.Name = "ListadoDeNotasDeCréditoDeSucursalesEspecialesToolStripMenuItem"
        Me.ListadoDeNotasDeCréditoDeSucursalesEspecialesToolStripMenuItem.Size = New System.Drawing.Size(488, 22)
        Me.ListadoDeNotasDeCréditoDeSucursalesEspecialesToolStripMenuItem.Text = "Listado de Notas de Crédito de Sucursales Especiales"
        '
        'CajasToolStripMenuItem
        '
        Me.CajasToolStripMenuItem.Name = "CajasToolStripMenuItem"
        Me.CajasToolStripMenuItem.Size = New System.Drawing.Size(272, 22)
        Me.CajasToolStripMenuItem.Text = "Cajas"
        '
        'VentasToolStripMenuItem
        '
        Me.VentasToolStripMenuItem.Name = "VentasToolStripMenuItem"
        Me.VentasToolStripMenuItem.Size = New System.Drawing.Size(272, 22)
        Me.VentasToolStripMenuItem.Text = "Ventas"
        '
        'CancelacionDeFacturasToolStripMenuItem
        '
        Me.CancelacionDeFacturasToolStripMenuItem.Name = "CancelacionDeFacturasToolStripMenuItem"
        Me.CancelacionDeFacturasToolStripMenuItem.Size = New System.Drawing.Size(272, 22)
        Me.CancelacionDeFacturasToolStripMenuItem.Text = "Cancelacion de Facturas"
        '
        'PictureBox2
        '
        Me.PictureBox2.DataBindings.Add(New System.Windows.Forms.Binding("Image", Me.MUESTRAIMAGENBindingSource1, "IMAGEN", True))
        Me.PictureBox2.Location = New System.Drawing.Point(27, 137)
        Me.PictureBox2.Name = "PictureBox2"
        Me.PictureBox2.Size = New System.Drawing.Size(964, 466)
        Me.PictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox2.TabIndex = 3
        Me.PictureBox2.TabStop = False
        '
        'MUESTRAIMAGENBindingSource1
        '
        Me.MUESTRAIMAGENBindingSource1.DataMember = "MUESTRAIMAGEN"
        Me.MUESTRAIMAGENBindingSource1.DataSource = Me.NewsoftvDataSet2
        '
        'NewsoftvDataSet2
        '
        Me.NewsoftvDataSet2.DataSetName = "NewsoftvDataSet2"
        Me.NewsoftvDataSet2.EnforceConstraints = False
        Me.NewsoftvDataSet2.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'CMBLabel1
        '
        Me.CMBLabel1.AutoSize = True
        Me.CMBLabel1.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DameDatosGeneralesBindingSource, "Nombre", True))
        Me.CMBLabel1.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBLabel1.ForeColor = System.Drawing.Color.Gray
        Me.CMBLabel1.Location = New System.Drawing.Point(392, 393)
        Me.CMBLabel1.Name = "CMBLabel1"
        Me.CMBLabel1.Size = New System.Drawing.Size(63, 20)
        Me.CMBLabel1.TabIndex = 7
        Me.CMBLabel1.Text = "Label1"
        '
        'DameDatosGeneralesBindingSource
        '
        Me.DameDatosGeneralesBindingSource.DataMember = "DameDatosGenerales"
        Me.DameDatosGeneralesBindingSource.DataSource = Me.NewsoftvDataSet
        '
        'NewsoftvDataSet
        '
        Me.NewsoftvDataSet.DataSetName = "NewsoftvDataSet"
        Me.NewsoftvDataSet.EnforceConstraints = False
        Me.NewsoftvDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'CMBLabel2
        '
        Me.CMBLabel2.AutoSize = True
        Me.CMBLabel2.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DameDatosGeneralesBindingSource, "Ciudad", True))
        Me.CMBLabel2.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBLabel2.ForeColor = System.Drawing.Color.Gray
        Me.CMBLabel2.Location = New System.Drawing.Point(392, 432)
        Me.CMBLabel2.Name = "CMBLabel2"
        Me.CMBLabel2.Size = New System.Drawing.Size(63, 20)
        Me.CMBLabel2.TabIndex = 8
        Me.CMBLabel2.Text = "Label2"
        '
        'DameDatosGeneralesTableAdapter
        '
        Me.DameDatosGeneralesTableAdapter.ClearBeforeFill = True
        '
        'DameTipoUsusarioBindingSource
        '
        Me.DameTipoUsusarioBindingSource.DataMember = "DameTipoUsusario"
        Me.DameTipoUsusarioBindingSource.DataSource = Me.NewsoftvDataSet
        '
        'DameTipoUsusarioTableAdapter
        '
        Me.DameTipoUsusarioTableAdapter.ClearBeforeFill = True
        '
        'ALTASMENUSBindingSource
        '
        Me.ALTASMENUSBindingSource.DataMember = "ALTASMENUS"
        Me.ALTASMENUSBindingSource.DataSource = Me.NewsoftvDataSet
        '
        'ALTASMENUSTableAdapter
        '
        Me.ALTASMENUSTableAdapter.ClearBeforeFill = True
        '
        'MUESTRAIMAGENBindingSource
        '
        Me.MUESTRAIMAGENBindingSource.DataMember = "MUESTRAIMAGEN"
        Me.MUESTRAIMAGENBindingSource.DataSource = Me.NewsoftvDataSet2
        '
        'DameEspecifBindingSource
        '
        Me.DameEspecifBindingSource.DataMember = "DameEspecif"
        Me.DameEspecifBindingSource.DataSource = Me.NewsoftvDataSet2
        '
        'DameEspecifTableAdapter
        '
        Me.DameEspecifTableAdapter.ClearBeforeFill = True
        '
        'MUESTRAIMAGENTableAdapter
        '
        Me.MUESTRAIMAGENTableAdapter.ClearBeforeFill = True
        '
        'Button1
        '
        Me.Button1.Location = New System.Drawing.Point(505, 361)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(54, 32)
        Me.Button1.TabIndex = 9
        Me.Button1.Text = "Prueba"
        Me.Button1.UseVisualStyleBackColor = True
        Me.Button1.Visible = False
        '
        'Procedimientos_arnoldo
        '
        Me.Procedimientos_arnoldo.DataSetName = "Procedimientos_arnoldo"
        Me.Procedimientos_arnoldo.EnforceConstraints = False
        Me.Procedimientos_arnoldo.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'DameClv_Session_UsuariosBindingSource
        '
        Me.DameClv_Session_UsuariosBindingSource.DataMember = "DameClv_Session_Usuarios"
        Me.DameClv_Session_UsuariosBindingSource.DataSource = Me.Procedimientos_arnoldo
        '
        'DameClv_Session_UsuariosTableAdapter
        '
        Me.DameClv_Session_UsuariosTableAdapter.ClearBeforeFill = True
        '
        'CMBLabelciudad
        '
        Me.CMBLabelciudad.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DameDatosGeneralesBindingSource, "Ciudad", True))
        Me.CMBLabelciudad.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBLabelciudad.ForeColor = System.Drawing.Color.Gray
        Me.CMBLabelciudad.Location = New System.Drawing.Point(31, 95)
        Me.CMBLabelciudad.Name = "CMBLabelciudad"
        Me.CMBLabelciudad.Size = New System.Drawing.Size(964, 29)
        Me.CMBLabelciudad.TabIndex = 11
        Me.CMBLabelciudad.Text = "Label2"
        Me.CMBLabelciudad.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.CMBLabelciudad.Visible = False
        '
        'DataSetLydia
        '
        Me.DataSetLydia.DataSetName = "DataSetLydia"
        Me.DataSetLydia.EnforceConstraints = False
        Me.DataSetLydia.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'DamePermisosBindingSource
        '
        Me.DamePermisosBindingSource.DataMember = "DamePermisos"
        Me.DamePermisosBindingSource.DataSource = Me.DataSetLydia
        '
        'DamePermisosTableAdapter
        '
        Me.DamePermisosTableAdapter.ClearBeforeFill = True
        '
        'CMBLabelNombreSistema
        '
        Me.CMBLabelNombreSistema.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBLabelNombreSistema.ForeColor = System.Drawing.Color.Gray
        Me.CMBLabelNombreSistema.Location = New System.Drawing.Point(12, 58)
        Me.CMBLabelNombreSistema.Name = "CMBLabelNombreSistema"
        Me.CMBLabelNombreSistema.Size = New System.Drawing.Size(992, 29)
        Me.CMBLabelNombreSistema.TabIndex = 12
        Me.CMBLabelNombreSistema.Text = "FACTURACIÓN"
        Me.CMBLabelNombreSistema.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'VerAcceso2TableAdapter1
        '
        Me.VerAcceso2TableAdapter1.ClearBeforeFill = True
        '
        'VerAcceso2TableAdapter2
        '
        Me.VerAcceso2TableAdapter2.ClearBeforeFill = True
        '
        'VerAcceso2TableAdapter3
        '
        Me.VerAcceso2TableAdapter3.ClearBeforeFill = True
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(24, 644)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(246, 16)
        Me.Label1.TabIndex = 16
        Me.Label1.Text = "Versión: 2.0.0.8, Fecha: 06/06/2019"
        '
        'VerAcceso2TableAdapter4
        '
        Me.VerAcceso2TableAdapter4.ClearBeforeFill = True
        '
        'VerAcceso2TableAdapter5
        '
        Me.VerAcceso2TableAdapter5.ClearBeforeFill = True
        '
        'VerAcceso2TableAdapter6
        '
        Me.VerAcceso2TableAdapter6.ClearBeforeFill = True
        '
        'VerAcceso2TableAdapter7
        '
        Me.VerAcceso2TableAdapter7.ClearBeforeFill = True
        '
        'VerAcceso2TableAdapter8
        '
        Me.VerAcceso2TableAdapter8.ClearBeforeFill = True
        '
        'VerAcceso2TableAdapter9
        '
        Me.VerAcceso2TableAdapter9.ClearBeforeFill = True
        '
        'VerAcceso2TableAdapter10
        '
        Me.VerAcceso2TableAdapter10.ClearBeforeFill = True
        '
        'VerAcceso2TableAdapter11
        '
        Me.VerAcceso2TableAdapter11.ClearBeforeFill = True
        '
        'LabelNombreUsuario
        '
        Me.LabelNombreUsuario.AutoSize = True
        Me.LabelNombreUsuario.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelNombreUsuario.ForeColor = System.Drawing.Color.Gray
        Me.LabelNombreUsuario.Location = New System.Drawing.Point(110, 614)
        Me.LabelNombreUsuario.Name = "LabelNombreUsuario"
        Me.LabelNombreUsuario.Size = New System.Drawing.Size(177, 20)
        Me.LabelNombreUsuario.TabIndex = 18
        Me.LabelNombreUsuario.Text = "LabelNombreUsuario"
        '
        'FacturaGlobalSucursalesEspecialesToolStripMenuItem
        '
        Me.FacturaGlobalSucursalesEspecialesToolStripMenuItem.Name = "FacturaGlobalSucursalesEspecialesToolStripMenuItem"
        Me.FacturaGlobalSucursalesEspecialesToolStripMenuItem.Size = New System.Drawing.Size(354, 22)
        Me.FacturaGlobalSucursalesEspecialesToolStripMenuItem.Text = "Factura Global Sucursales Especiales"
        '
        'Timer1
        '
        Me.Timer1.Enabled = True
        Me.Timer1.Interval = 5000
        '
        'FrmMenu
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.ClientSize = New System.Drawing.Size(1016, 673)
        Me.Controls.Add(Me.PictureBox2)
        Me.Controls.Add(Me.LabelNombreUsuario)
        Me.Controls.Add(LabelUsuario)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.CMBLabelNombreSistema)
        Me.Controls.Add(CMBCiudadLabel)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(CMBLabelciudad1)
        Me.Controls.Add(Me.CMBLabel2)
        Me.Controls.Add(CMBNombreLabel)
        Me.Controls.Add(Me.CMBLabel1)
        Me.Controls.Add(Me.CMBLabelciudad)
        Me.Controls.Add(Me.MenuStrip1)
        Me.MainMenuStrip = Me.MenuStrip1
        Me.Name = "FrmMenu"
        Me.RightToLeftLayout = True
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Menú Principal del Sistema de Facturación"
        Me.ContextMenuStrip1.ResumeLayout(False)
        Me.MenuStrip1.ResumeLayout(False)
        Me.MenuStrip1.PerformLayout()
        CType(Me.PictureBox2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.MUESTRAIMAGENBindingSource1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.NewsoftvDataSet2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DameDatosGeneralesBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.NewsoftvDataSet, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DameTipoUsusarioBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ALTASMENUSBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.MUESTRAIMAGENBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DameEspecifBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Procedimientos_arnoldo, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DameClv_Session_UsuariosBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DataSetLydia, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DamePermisosBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents ContextMenuStrip1 As System.Windows.Forms.ContextMenuStrip
    Friend WithEvents FacturacionToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents FacturacionToolStripMenuItem1 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents SalirToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents MenuStrip1 As System.Windows.Forms.MenuStrip
    Friend WithEvents CajasToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents VentasToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents CancelacionDeFacturasToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents CajasToolStripMenuItem1 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents VentasToolStripMenuItem1 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents CajasToolStripMenuItem2 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents VentasToolStripMenuItem2 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents CancelaciónToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents PictureBox2 As System.Windows.Forms.PictureBox
    Friend WithEvents NewsoftvDataSet As softvFacturacion.NewsoftvDataSet
    Friend WithEvents DameDatosGeneralesBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents DameDatosGeneralesTableAdapter As softvFacturacion.NewsoftvDataSetTableAdapters.DameDatosGeneralesTableAdapter
    Friend WithEvents CMBLabel1 As System.Windows.Forms.Label
    Friend WithEvents CMBLabel2 As System.Windows.Forms.Label
    Friend WithEvents ReportesToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents CortesDeFacturasToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents CToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ListadosYAfectacionesToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents EntregasParcialesToolStripMenuItem1 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents DesgloceDeMonedaToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ArqueoDeCajasToolStripMenuItem1 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents CancelaciònDeFacturasToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ReImpresionDeFacturasToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents FACTURAGLOBALToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents FacturasGlobalesToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ResumenGeneralFacturaGlobalToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ListadoDeEntragasParcialesToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents Procedimientos_arnoldo As softvFacturacion.Procedimientos_arnoldo
    Friend WithEvents DameClv_Session_UsuariosBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents DameClv_Session_UsuariosTableAdapter As softvFacturacion.Procedimientos_arnoldoTableAdapters.DameClv_Session_UsuariosTableAdapter
    Friend WithEvents DameTipoUsusarioBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents DameTipoUsusarioTableAdapter As softvFacturacion.NewsoftvDataSetTableAdapters.DameTipoUsusarioTableAdapter
    Friend WithEvents ALTASMENUSBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents ALTASMENUSTableAdapter As softvFacturacion.NewsoftvDataSetTableAdapters.ALTASMENUSTableAdapter
    Friend WithEvents NewsoftvDataSet2 As softvFacturacion.NewsoftvDataSet2
    Friend WithEvents DameEspecifBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents DameEspecifTableAdapter As softvFacturacion.NewsoftvDataSet2TableAdapters.DameEspecifTableAdapter
    Friend WithEvents MUESTRAIMAGENBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents MUESTRAIMAGENTableAdapter As softvFacturacion.NewsoftvDataSet2TableAdapters.MUESTRAIMAGENTableAdapter
    Friend WithEvents RelaciónDeIngresosPorConceptosToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents MUESTRAIMAGENBindingSource1 As System.Windows.Forms.BindingSource
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents ListadoDeBonificacionesToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ListadoDeFacturasCanceadasToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ListadoDeFacturasReimpresasToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents NotasDeCréditoToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ListadoDeNotasDeCréditoToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents RecepciónDelArchivoDeOxxoToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents RelaciónDeClientesToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents DataSetLydia As softvFacturacion.DataSetLydia
    Friend WithEvents DamePermisosBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents DamePermisosTableAdapter As softvFacturacion.DataSetLydiaTableAdapters.DamePermisosTableAdapter
    Friend WithEvents PolizasToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents DesgloceDeMensualidadesToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents DesgloceDeContratacionesToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents CMBLabelNombreSistema As System.Windows.Forms.Label
    Friend WithEvents FacturasConCargoAutomaticoToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ListadoDePagosEfectuadosPorElClienteToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ReporteGlobalToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ReporteIngresosTarjetasCréditoDébitoToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents IngresosDeClientesPorUnMontoToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents NúmeroDeBonificacionesToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents CobroEquivocadoToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents CobroErroneoToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents VerAcceso2TableAdapter1 As softvFacturacion.ProcedimientosArnoldo3TableAdapters.VerAcceso2TableAdapter
    Friend WithEvents VerAcceso2TableAdapter2 As softvFacturacion.ProcedimientosArnoldo3TableAdapters.VerAcceso2TableAdapter

    Friend WithEvents ListadoDeCobrosParcialesDeMaterialporConvenioToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents VerAcceso2TableAdapter3 As softvFacturacion.ProcedimientosArnoldo3TableAdapters.VerAcceso2TableAdapter
    Friend WithEvents PerfilesSISTEToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents PerfilesToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents VerAcceso2TableAdapter4 As softvFacturacion.ProcedimientosArnoldo3TableAdapters.VerAcceso2TableAdapter
    Friend WithEvents RegistroDeGastosToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents EntregasGlobalesToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ControlDeEfectivoToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents AjustePorDiferenciasToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ControlDeEfectivoToolStripMenuItem1 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ListadoDeGastosToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ListadoDeEntregasGlobalesToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents VerAcceso2TableAdapter5 As softvFacturacion.ProcedimientosArnoldo3TableAdapters.VerAcceso2TableAdapter
    Friend WithEvents VerAcceso2TableAdapter6 As softvFacturacion.ProcedimientosArnoldo3TableAdapters.VerAcceso2TableAdapter
    Friend WithEvents VerAcceso2TableAdapter7 As softvFacturacion.ProcedimientosArnoldo3TableAdapters.VerAcceso2TableAdapter
    Friend WithEvents VerAcceso2TableAdapter8 As softvFacturacion.ProcedimientosArnoldo3TableAdapters.VerAcceso2TableAdapter
    Friend WithEvents VerAcceso2TableAdapter9 As softvFacturacion.ProcedimientosArnoldo3TableAdapters.VerAcceso2TableAdapter
    Friend WithEvents VerAcceso2TableAdapter10 As softvFacturacion.ProcedimientosArnoldo3TableAdapters.VerAcceso2TableAdapter
    Friend WithEvents VerAcceso2TableAdapter11 As softvFacturacion.ProcedimientosArnoldo3TableAdapters.VerAcceso2TableAdapter
    Friend WithEvents RelaciónDeIngresosPorConceptosporSucursalToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents LabelNombreUsuario As System.Windows.Forms.Label
    Friend WithEvents EstadosDeCuentaToolStripMenuItem1 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents RelaciónDeIngresosPorConceptoDistribuidorPlazaToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents RelaciónDeIngresosPorConceptosDistribuidorPlazaToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents BonificacionesToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents FacturasDeAlmacénToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ReporteNuevoToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ConciliaciónBancariaToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents DetalleDePagosPrimeraVersiónToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ListadoDeNotasDeCréditoToolStripMenuItem1 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents RefacturacionToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents FacturaGlobalSucursalesEspecialesToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ConciliaciónCLABEToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents PolizasSucursalesEspecialesToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Public WithEvents CMBLabelciudad As System.Windows.Forms.Label
    Friend WithEvents FacturaGlobalSucursalesEspecialesToolStripMenuItem1 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents RelacionSucursalesEspecialesMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents CortesEspecialesMenuItem1 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents CancelaciónYReimpresiónDeSucursalesEspecialesToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents CajasWebToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents Timer1 As System.Windows.Forms.Timer
    Friend WithEvents ListadoDeNotasDeCréditoDeSucursalesEspecialesToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents DetalleDePagosSucursalesEspecialesToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ConciliaciónSucursalesEspecialesToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents RefacturaciónSucursalesEspecialesToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents EstadosDeCuentaInternetYComboToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
End Class
