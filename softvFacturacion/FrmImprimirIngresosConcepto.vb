Imports CrystalDecisions.Shared
Imports CrystalDecisions.CrystalReports.Engine
Imports System.Data.SqlClient
Imports System.Text

Public Class FrmImprimirIngresosConcepto

    Private customersByCityReport As ReportDocument

    Private op As String = Nothing
    Private Titulo As String = Nothing
    'Private Const PARAMETER_FIELD_NAME As String = "Op"    

    Private Sub ConfigureCrystalReports()
        customersByCityReport = New ReportDocument
        Dim connectionInfo As New ConnectionInfo
        '"Data Source=SERVER-2003\SQLEXPRESS;Initial Catalog=Newsoftv;Persist Security Info"& _ 
        '    "=True;User ID=DeSistema;Password=1975huli")
        'connectionInfo.ServerName = GloServerName
        'connectionInfo.DatabaseName = GloDatabaseName
        'connectionInfo.UserID = GloUserID
        'connectionInfo.Password = GloPassword
        Dim reportPath As String = Nothing
        Dim Titulo As String = Nothing
        Dim Sucursal As String = Nothing
        Dim Ciudades As String = Nothing
        Ciudades = " Ciudad(es): " + LocCiudades
        If GloBnd_Des_Cont = True Then
            reportPath = RutaReportes + "\ReporteDesgloce_Contrataciones.rpt"
            Titulo = "Desgloce de Contrataciones"
        ElseIf GloBnd_Des_Men = True Then
            reportPath = RutaReportes + "\ReporteDesgloce_Mensualidades.rpt"
            Titulo = "Desgloce de Mensualidades"
        ElseIf LocClientesPagosAdelantados = True Then
            LocClientesPagosAdelantados = False
            reportPath = RutaReportes + "\ClientesconPagosAdelantados.rpt"
            Titulo = "Relación de Clientes con Pagos Adelantados"
        ElseIf LocClientesPagosAdelantados = False Then
            If varfrmselcompania = "relingresonuevo" Then
                reportPath = RutaReportes + "\DesgloceporConceptosDistribuidor.rpt"
                Titulo = "Relación de Ingresos por Conceptos"
            Else
                reportPath = RutaReportes + "\DesgloceporConceptos.rpt"
                Titulo = "Relación de Ingresos por Conceptos"
            End If

        End If
        Sucursal = " Sucursal: " + GloNomSucursal


        If OpcionReporte = 1 Then

            'RElacion de ingresos anterior
            'Dim DS As New DataSet
            'DS.Clear()
            'BaseII.limpiaParametros()

            'BaseII.CreateMyParameter("@Fecha_Ini", SqlDbType.DateTime, eFechaInicial)
            'BaseII.CreateMyParameter("@Fecha_Fin", SqlDbType.DateTime, eFechaFinal)
            'BaseII.CreateMyParameter("@Tipo", SqlDbType.VarChar, "")
            'BaseII.CreateMyParameter("@sucursal", SqlDbType.Int, 0)
            'BaseII.CreateMyParameter("@Caja", SqlDbType.Int, 0)
            'BaseII.CreateMyParameter("@Cajera", SqlDbType.VarChar, "")
            'BaseII.CreateMyParameter("@Op", SqlDbType.Int, 0)
            'BaseII.CreateMyParameter("@Clv_Session", SqlDbType.BigInt, gloClv_Session)
            'BaseII.CreateMyParameter("@identificador2", SqlDbType.Int, identificador)
            'BaseII.CreateMyParameter("@CompaniasXml", SqlDbType.Xml, DocCompanias.InnerXml)
            ''BaseII.CreateMyParameter("@CiudadesXml", SqlDbType.Xml, DocCiudades.InnerXml)
            ''BaseII.CreateMyParameter("@LocalidadesXml", SqlDbType.Xml, DocLocalidades.InnerXml)
            'Dim listatablas As New List(Of String)
            'listatablas.Add("Desgloce_de_Prueba")
            'listatablas.Add("TipServ")

            'DS = BaseII.ConsultaDS("Desgloce_de_Prueba", listatablas)

            'customersByCityReport.Load(reportPath)
            'SetDBReport(DS, customersByCityReport)
            'customersByCityReport.DataDefinition.FormulaFields("Compania").Text = "'" & NombreCompania & "'"

            Dim dSet As New DataSet
            Dim tableNameList As New List(Of String)
            Dim rDocument As New ReportDocument

            tableNameList.Add("REPORTEDesglose")
            tableNameList.Add("Bancos")
            tableNameList.Add("IEPS")
            tableNameList.Add("IVA")
            tableNameList.Add("tblCompanias")
            tableNameList.Add("General")
            tableNameList.Add("Titulo")

            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@FECHAINI", SqlDbType.DateTime, eFechaInicial) 'OK
            BaseII.CreateMyParameter("@FECHAFIN", SqlDbType.DateTime, eFechaFinal) 'OK
            BaseII.CreateMyParameter("@TIPO", SqlDbType.VarChar, "C", 1) 'OK
            BaseII.CreateMyParameter("@CIUDAD", SqlDbType.Int, 0) 'OK
            BaseII.CreateMyParameter("@CAJA", SqlDbType.Int, 0) 'OK
            BaseII.CreateMyParameter("@CAJERA", SqlDbType.VarChar, GloUsuario, 11)
            BaseII.CreateMyParameter("@CLV_SESSION", SqlDbType.Int, identificador) 'OK
            BaseII.CreateMyParameter("@CLV_FACTURA", SqlDbType.Int, 0)
            BaseII.CreateMyParameter("@CLV_USUARIO", SqlDbType.VarChar, GloUsuario, 11)
            BaseII.CreateMyParameter("@CLVCOMPANIA", SqlDbType.Int, 0) 'OK
            BaseII.CreateMyParameter("@CLV_LLAVE_POLIZANEW", ParameterDirection.Output, SqlDbType.Int)
            BaseII.CreateMyParameter("@NUMERROR", ParameterDirection.Output, SqlDbType.Int)
            BaseII.CreateMyParameter("@MSJERROR", ParameterDirection.Output, SqlDbType.VarChar, 250)
            BaseII.CreateMyParameter("@CompaniasXml", SqlDbType.Xml, DocCompanias.InnerXml)
            'BaseII.CreateMyParameter("@EstadosXml", SqlDbType.Xml, DocEstados.InnerXml)
            BaseII.CreateMyParameter("@CiudadesXml", SqlDbType.Xml, DocCiudades.InnerXml)
            dSet = BaseII.ConsultaDS("REPORTEDesglosePorCiudad", tableNameList)

            'rDocument.Load(RutaReportes + "\REPORTEDesglosePorCiudad.rpt")
            'rDocument.SetDataSource(dSet)

            customersByCityReport.Load(RutaReportes + "\REPORTEDesglosePorCiudad.rpt")
            SetDBReport(dSet, customersByCityReport)
        ElseIf OpcionReporte = 2 Then
            Dim dSet As New DataSet
            Dim tableNameList As New List(Of String)
            Dim rDocument As New ReportDocument

            tableNameList.Add("REPORTEDesglose")
            tableNameList.Add("Bancos")
            tableNameList.Add("IEPS")
            tableNameList.Add("IVA")
            tableNameList.Add("tblCompanias")
            tableNameList.Add("General")
            tableNameList.Add("Titulo")

            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@FECHAINI", SqlDbType.DateTime, eFechaInicial) 'OK
            BaseII.CreateMyParameter("@FECHAFIN", SqlDbType.DateTime, eFechaFinal) 'OK
            BaseII.CreateMyParameter("@TIPO", SqlDbType.VarChar, "C", 1) 'OK
            BaseII.CreateMyParameter("@SUCURSAL", SqlDbType.Int, 0) 'OK
            BaseII.CreateMyParameter("@CAJA", SqlDbType.Int, 0) 'OK
            BaseII.CreateMyParameter("@CAJERA", SqlDbType.VarChar, GloUsuario, 11)
            BaseII.CreateMyParameter("@CLV_SESSION", SqlDbType.Int, identificador) 'OK
            BaseII.CreateMyParameter("@CLV_FACTURA", SqlDbType.Int, 0)
            BaseII.CreateMyParameter("@CLV_USUARIO", SqlDbType.VarChar, GloUsuario, 11)
            BaseII.CreateMyParameter("@CLVCOMPANIA", SqlDbType.Int, 0) 'OK
            BaseII.CreateMyParameter("@CLV_LLAVE_POLIZANEW", ParameterDirection.Output, SqlDbType.Int)
            BaseII.CreateMyParameter("@NUMERROR", ParameterDirection.Output, SqlDbType.Int)
            BaseII.CreateMyParameter("@MSJERROR", ParameterDirection.Output, SqlDbType.VarChar, 250)
            BaseII.CreateMyParameter("@CompaniasXml", SqlDbType.Xml, DocCompanias.InnerXml)
            BaseII.CreateMyParameter("@SucursalesXML", SqlDbType.Xml, DocSucursales.InnerXml)
            dSet = BaseII.ConsultaDS("REPORTEDesglose", tableNameList)

            customersByCityReport.Load(RutaReportes + "\REPORTEDesglose.rpt")
            SetDBReport(dSet, customersByCityReport)
        ElseIf OpcionReporte = 3 Then

            reportPath = RutaReportes + "\DesgloceporConceptos.rpt"
            Titulo = "Relación de Ingresos por Conceptos"
            'RElacion de ingresos anterior
            Dim DS As New DataSet
            DS.Clear()
            BaseII.limpiaParametros()

            BaseII.CreateMyParameter("@Fecha_Ini", SqlDbType.DateTime, eFechaInicial)
            BaseII.CreateMyParameter("@Fecha_Fin", SqlDbType.DateTime, eFechaFinal)
            BaseII.CreateMyParameter("@Tipo", SqlDbType.VarChar, "")
            BaseII.CreateMyParameter("@sucursal", SqlDbType.Int, 0)
            BaseII.CreateMyParameter("@Caja", SqlDbType.Int, 0)
            BaseII.CreateMyParameter("@Cajera", SqlDbType.VarChar, "")
            BaseII.CreateMyParameter("@Op", SqlDbType.Int, 0)
            BaseII.CreateMyParameter("@Clv_Session", SqlDbType.BigInt, gloClv_Session)
            BaseII.CreateMyParameter("@identificador2", SqlDbType.Int, identificador)
            BaseII.CreateMyParameter("@CompaniasXml", SqlDbType.Xml, DocCompanias.InnerXml)
            'BaseII.CreateMyParameter("@CiudadesXml", SqlDbType.Xml, DocCiudades.InnerXml)
            'BaseII.CreateMyParameter("@LocalidadesXml", SqlDbType.Xml, DocLocalidades.InnerXml)
            Dim listatablas As New List(Of String)
            listatablas.Add("Desgloce_de_Prueba")
            listatablas.Add("TipServ")

            DS = BaseII.ConsultaDS("Desgloce_de_Prueba", listatablas)

            customersByCityReport.Load(reportPath)
            SetDBReport(DS, customersByCityReport)
            customersByCityReport.DataDefinition.FormulaFields("Compania").Text = "'" & NombreCompania & "'"
        End If
        'CrystalReportViewer1.ReportSource = rDocument


        If Titulo = "Relación de Ingresos por Conceptos s" And varfrmselcompania <> "ConceptosSucursalesEspeciales" Then
            Dim companias As String = ""
            'Dim elementos = DocCompanias.SelectNodes("//COMPANIA")
            'For Each elem As Xml.XmlElement In elementos
            '    If elem.GetAttribute("Seleccion") = "1" Then
            '        If companias = "Plazas: " Then
            '            companias = companias + elem.GetAttribute("Nombre")
            '        Else
            '            companias = companias + ", " + elem.GetAttribute("Nombre")
            '        End If
            '    End If
            'Next
            Dim ciudadesreporte As String = "Distribuidores: "
            Dim elementos2 = DocPlazas.SelectNodes("//PLAZA")
            For Each elem As Xml.XmlElement In elementos2
                If elem.GetAttribute("Seleccion") = "1" Then
                    If ciudadesreporte = "Distribuidores: " Then
                        ciudadesreporte = ciudadesreporte + elem.GetAttribute("nombre")
                    Else
                        ciudadesreporte = ciudadesreporte + ", " + elem.GetAttribute("nombre")
                    End If
                End If
            Next
            customersByCityReport.DataDefinition.FormulaFields("Empresa").Text = "'" & ciudadesreporte & "'"
            customersByCityReport.DataDefinition.FormulaFields("Titulo").Text = "'" & Titulo & "'"
            eFechaTitulo = "de la Fecha " & eFechaInicial & " a la Fecha " & eFechaFinal
            customersByCityReport.DataDefinition.FormulaFields("Fecha").Text = "'" & eFechaTitulo & "'"
            customersByCityReport.DataDefinition.FormulaFields("Subtitulo").Text = "'" & companias & "'"
            customersByCityReport.DataDefinition.FormulaFields("Compania").Text = "'" & GloEmpresaPRincipal & "'"
        ElseIf Titulo = "Relación de Ingresos por Conceptos Sucursales Especiales" Then
            Dim companias As String = "Sucursales: "
            Dim elementos = DocSucursales.SelectNodes("//SUCURSAL")
            For Each elem As Xml.XmlElement In elementos
                If elem.GetAttribute("Seleccion") = "1" Then
                    If companias = "Sucursales: " Then
                        companias = companias + elem.GetAttribute("Nombre")
                    Else
                        companias = companias + ", " + elem.GetAttribute("Nombre")
                    End If
                End If
            Next
            customersByCityReport.DataDefinition.FormulaFields("Empresa").Text = "'" & companias & "'"
            customersByCityReport.DataDefinition.FormulaFields("Titulo").Text = "'" & Titulo & "'"
            eFechaTitulo = "de la Fecha " & eFechaInicial & " a la Fecha " & eFechaFinal
            customersByCityReport.DataDefinition.FormulaFields("Fecha").Text = "'" & eFechaTitulo & "'"
            customersByCityReport.DataDefinition.FormulaFields("Subtitulo").Text = "'" & "" & "'"
            customersByCityReport.DataDefinition.FormulaFields("Compania").Text = "'" & GloEmpresaPRincipal & "'"
        Else
            customersByCityReport.DataDefinition.FormulaFields("Empresa").Text = "'" & LocNomEmpresa & "'"
            customersByCityReport.DataDefinition.FormulaFields("Titulo").Text = "'" & Titulo & "'"
            eFechaTitulo = "de la Fecha " & eFechaInicial & " a la Fecha " & eFechaFinal
            customersByCityReport.DataDefinition.FormulaFields("Fecha").Text = "'" & eFechaTitulo & "'"
            customersByCityReport.DataDefinition.FormulaFields("Subtitulo").Text = "'" & Sucursal & "'"
            customersByCityReport.DataDefinition.FormulaFields("Ciudades").Text = "'" & Ciudades & "'"
        End If

        varfrmselcompania = ""

        CrystalReportViewer1.ShowGroupTreeButton = False
        CrystalReportViewer1.ToolPanelView = CrystalDecisions.Windows.Forms.ToolPanelViewType.None
        CrystalReportViewer1.ReportSource = customersByCityReport
        CrystalReportViewer1.Zoom(75)



        If GloOpFacturas = 3 Then
            CrystalReportViewer1.ShowExportButton = False
            CrystalReportViewer1.ShowPrintButton = False
            CrystalReportViewer1.ShowRefreshButton = False
        End If
        'SetDBLogonForReport2(connectionInfo)
        customersByCityReport = Nothing
        GloBnd_Des_Men = False
        GloBnd_Des_Cont = False

    End Sub

    Private Sub ReporteRelacionDeIngresosPorConceptos()
        customersByCityReport = New ReportDocument
        Dim conexion As New SqlConnection(MiConexion)
        Dim sBuilder As New StringBuilder("EXEC ReporteRelacionDeIngresosPorConceptos '" + eFechaInicial + "', '" + eFechaFinal + "', " + gloClv_Session.ToString)
        Dim dAdapter As New SqlDataAdapter(sBuilder.ToString(), conexion)
        Dim dSet As New DataSet
        Dim reportPath As String = Nothing
        Dim Titulo As String = Nothing
        Dim Sucursal As String = Nothing
        Dim Ciudades As String = Nothing

        Ciudades = " Ciudad(es): " + LocCiudades
        Titulo = "Relación de Ingresos por Conceptos"
        Sucursal = " Sucursal: " + GloNomSucursal

        Try

            dAdapter.Fill(dSet)
            dSet.Tables(0).TableName = "Desgloce_de_Prueba"
            dSet.Tables(1).TableName = "TipServ"

            reportPath = RutaReportes + "\RelacionDeIngresosPorConceptos.rpt"
            customersByCityReport.Load(reportPath)
            customersByCityReport.SetDataSource(dSet)

            customersByCityReport.DataDefinition.FormulaFields("Empresa").Text = "'" & LocNomEmpresa & "'"
            customersByCityReport.DataDefinition.FormulaFields("Titulo").Text = "'" & Titulo & "'"
            eFechaTitulo = "de la Fecha " & eFechaInicial & " a la Fecha " & eFechaFinal
            customersByCityReport.DataDefinition.FormulaFields("Fecha").Text = "'" & eFechaTitulo & "'"
            customersByCityReport.DataDefinition.FormulaFields("Subtitulo").Text = "'" & Sucursal & "'"
            customersByCityReport.DataDefinition.FormulaFields("Ciudades").Text = "'" & Ciudades & "'"

            CrystalReportViewer1.ShowGroupTreeButton = False
            CrystalReportViewer1.ToolPanelView = CrystalDecisions.Windows.Forms.ToolPanelViewType.None
            CrystalReportViewer1.ReportSource = customersByCityReport
            CrystalReportViewer1.Zoom(75)


            Me.CrystalReportViewer1.ShowPrintButton = True
            Me.CrystalReportViewer1.ShowExportButton = True
            Me.CrystalReportViewer1.ShowRefreshButton = False

            If GloOpFacturas = 3 Then
                CrystalReportViewer1.ShowExportButton = False
                CrystalReportViewer1.ShowPrintButton = False
                CrystalReportViewer1.ShowRefreshButton = False
            End If

            customersByCityReport = Nothing
            GloBnd_Des_Men = False
            GloBnd_Des_Cont = False

        Catch ex As Exception

        End Try

    End Sub

    Private Sub SetDBLogonForReport(ByVal myConnectionInfo As ConnectionInfo, ByVal myReportDocument As ReportDocument)
        customersByCityReport.DataSourceConnections(0).SetConnection(GloServerName, GloDatabaseName, GloUserID, GloPassword)
        'customersByCityReport.SetDatabaseLogon(GloUserID, GloPassword, GloServerName, GloDatabaseName)

        Dim myTables As Tables = myReportDocument.Database.Tables
        Dim myTable As CrystalDecisions.CrystalReports.Engine.Table
        For Each myTable In myTables
            Dim myTableLogonInfo As TableLogOnInfo = myTable.LogOnInfo
            myTableLogonInfo.ConnectionInfo = myConnectionInfo
            myTable.ApplyLogOnInfo(myTableLogonInfo)
            myTable.Location = GloDatabaseName & ".dbo." & myTable.Location.Substring(myTable.Location.LastIndexOf(".") + 1)
        Next
    End Sub

    Private Sub SetDBLogonForSubReport(ByVal myConnectionInfo As ConnectionInfo, ByVal myReportDocument As ReportDocument)
        customersByCityReport.Subreports(0).SetDatabaseLogon(GloUserID, GloPassword, GloServerName, GloDatabaseName)
        'customersByCityReport.Subreports(0).DataSourceConnections(0).SetConnection(GloServerName, GloDatabaseName, GloUserID, GloPassword)

        Dim I As Integer = myReportDocument.Subreports.Count
        Dim X As Integer = 0
        For X = 0 To I - 1
            Dim myTables As Tables = myReportDocument.Subreports(X).Database.Tables
            Dim myTable As CrystalDecisions.CrystalReports.Engine.Table
            For Each myTable In myTables
                Dim myTableLogonInfo As TableLogOnInfo = myTable.LogOnInfo
                myTableLogonInfo.ConnectionInfo = myConnectionInfo
                myTable.ApplyLogOnInfo(myTableLogonInfo)
                myTable.Location = GloDatabaseName & ".dbo." & myTable.Location.Substring(myTable.Location.LastIndexOf(".") + 1)
            Next
        Next X
    End Sub

    Private Sub FrmImprimirIngresosConcepto_Activated(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Activated

    End Sub

    Private Sub FrmImprimirIngresosConcepto_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        If IdSistema = "AG" Or IdSistema = "VA" Then
            If LocBndrelingporconceptosCiudad = True Then
                LocBndrelingporconceptosCiudad = False
                LocbndDesPagosCiudad = True
                FrmImprimirRepGral.Show()
            End If
            If LocBndrelingporconceptosSucursal = True Then
                LocBndrelingporconceptosSucursal = False
                LocbndDesPagosSucursal = True
                FrmImprimirRepGral.Show()
            End If
        End If
    End Sub

    Private Sub FrmImprimirIngresosConcepto_HandleDestroyed(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.HandleDestroyed

    End Sub

    Private Sub FrmImprimirIngresosConcepto_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If GloBnd_Des_Cont = True Then
            Me.Text = "Impresión Desgloce de Contrataciones"
        ElseIf GloBnd_Des_Men = True Then
            Me.Text = "Impresión Desgloce de Mensualidades"
        ElseIf LocClientesPagosAdelantados = True Then
            Me.Text = "Impresión Relación de Clientes con Pagos Adelantados"
        Else
            Me.Text = "Impresión Relación de Ingresos por Conceptos"
            'ReporteRelacionDeIngresosPorConceptos()
            'Exit Sub
        End If

        'If LocBndrelingporconceptosCiudad = True Then
        '    LocBndrelingporconceptosCiudad = False
        '    LocbndDesPagosCiudad = True
        '    FrmImprimirRepGral.Show()

        'else If LocBndrelingporconceptosSucursal = True Then
        '    LocBndrelingporconceptosSucursal = False
        '    LocbndDesPagosSucursal = True
        '    FrmImprimirRepGral.Show()
        'Else

        ConfigureCrystalReports()
        Me.CrystalReportViewer1.ShowPrintButton = True
        Me.CrystalReportViewer1.ShowExportButton = True
        Me.CrystalReportViewer1.ShowRefreshButton = False
        'End If
    End Sub

    Private Sub CrystalReportViewer1_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CrystalReportViewer1.Load

    End Sub
End Class