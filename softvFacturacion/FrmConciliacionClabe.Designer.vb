﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmConciliacionClabe
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle4 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.CMBNombreLabel = New System.Windows.Forms.Label()
        Me.Clv_sessionLabel = New System.Windows.Forms.Label()
        Me.ClienteLabel = New System.Windows.Forms.Label()
        Me.Clv_facturaLabel = New System.Windows.Forms.Label()
        Me.StatusLabel = New System.Windows.Forms.Label()
        Me.Button6 = New System.Windows.Forms.Button()
        Me.Button5 = New System.Windows.Forms.Button()
        Me.Button3 = New System.Windows.Forms.Button()
        Me.Button2 = New System.Windows.Forms.Button()
        Me.TextBox1 = New System.Windows.Forms.TextBox()
        Me.CMBLabel2 = New System.Windows.Forms.Label()
        Me.CMBLabel1 = New System.Windows.Forms.Label()
        Me.Busca_conciliacion_santaderDataGridView = New System.Windows.Forms.DataGridView()
        Me.Busca_conciliacion_santaderBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.ProcedimientosArnoldo3 = New softvFacturacion.ProcedimientosArnoldo3()
        Me.DateTimePicker1 = New System.Windows.Forms.DateTimePicker()
        Me.CMBLabel3 = New System.Windows.Forms.Label()
        Me.CMBPanel1 = New System.Windows.Forms.Panel()
        Me.btnBuscar = New System.Windows.Forms.Button()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.tbContrato = New System.Windows.Forms.TextBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.dgvArchivos = New System.Windows.Forms.DataGridView()
        Me.Clv_Archivo = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Nombre = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.FechaCarga = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Status = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.dtFechaArchivo = New System.Windows.Forms.DateTimePicker()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.CMBLabel5 = New System.Windows.Forms.Label()
        Me.Button7 = New System.Windows.Forms.Button()
        Me.ComboBox1 = New System.Windows.Forms.ComboBox()
        Me.MuestraStatusConciliacionBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.ProcedimientosArnoldo3BindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.CMBNombreLabel1 = New System.Windows.Forms.Label()
        Me.Busca_conciliacion_santaderTableAdapter = New softvFacturacion.ProcedimientosArnoldo3TableAdapters.Busca_conciliacion_santaderTableAdapter()
        Me.Muestra_Status_ConciliacionTableAdapter = New softvFacturacion.ProcedimientosArnoldo3TableAdapters.Muestra_Status_ConciliacionTableAdapter()
        Me.Button4 = New System.Windows.Forms.Button()
        Me.Clv_sessionTextBox = New System.Windows.Forms.TextBox()
        Me.ClienteTextBox = New System.Windows.Forms.TextBox()
        Me.OpenFileDialog1 = New System.Windows.Forms.OpenFileDialog()
        Me.Clv_facturaTextBox = New System.Windows.Forms.TextBox()
        Me.StatusTextBox = New System.Windows.Forms.TextBox()
        Me.CMBPanel3 = New System.Windows.Forms.Panel()
        Me.BindingNavigator1 = New System.Windows.Forms.BindingNavigator(Me.components)
        Me.BindingNavigatorSeparator2 = New System.Windows.Forms.ToolStripSeparator()
        Me.GuardarToolStripButton = New System.Windows.Forms.ToolStripButton()
        Me.tsbEliminar = New System.Windows.Forms.ToolStripButton()
        Me.Button8 = New System.Windows.Forms.Button()
        Me.Clv_Archivo2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Contrato = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.cliente = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Importe = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ImporteFacturacion = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.FechaPago = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Estado = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.StatusCliente = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Consecutivo = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Ciclo = New System.Windows.Forms.DataGridViewTextBoxColumn()
        CType(Me.Busca_conciliacion_santaderDataGridView, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Busca_conciliacion_santaderBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ProcedimientosArnoldo3, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.CMBPanel1.SuspendLayout()
        CType(Me.dgvArchivos, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.MuestraStatusConciliacionBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ProcedimientosArnoldo3BindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.CMBPanel3.SuspendLayout()
        CType(Me.BindingNavigator1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.BindingNavigator1.SuspendLayout()
        Me.SuspendLayout()
        '
        'CMBNombreLabel
        '
        Me.CMBNombreLabel.AutoSize = True
        Me.CMBNombreLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBNombreLabel.ForeColor = System.Drawing.Color.DarkRed
        Me.CMBNombreLabel.Location = New System.Drawing.Point(412, 354)
        Me.CMBNombreLabel.Name = "CMBNombreLabel"
        Me.CMBNombreLabel.Size = New System.Drawing.Size(169, 20)
        Me.CMBNombreLabel.TabIndex = 30
        Me.CMBNombreLabel.Text = "Nombre Del Cliente:"
        '
        'Clv_sessionLabel
        '
        Me.Clv_sessionLabel.AutoSize = True
        Me.Clv_sessionLabel.Location = New System.Drawing.Point(46, 538)
        Me.Clv_sessionLabel.Name = "Clv_sessionLabel"
        Me.Clv_sessionLabel.Size = New System.Drawing.Size(62, 13)
        Me.Clv_sessionLabel.TabIndex = 32
        Me.Clv_sessionLabel.Text = "clv session:"
        '
        'ClienteLabel
        '
        Me.ClienteLabel.AutoSize = True
        Me.ClienteLabel.Location = New System.Drawing.Point(309, 535)
        Me.ClienteLabel.Name = "ClienteLabel"
        Me.ClienteLabel.Size = New System.Drawing.Size(41, 13)
        Me.ClienteLabel.TabIndex = 33
        Me.ClienteLabel.Text = "cliente:"
        '
        'Clv_facturaLabel
        '
        Me.Clv_facturaLabel.AutoSize = True
        Me.Clv_facturaLabel.Location = New System.Drawing.Point(521, 532)
        Me.Clv_facturaLabel.Name = "Clv_facturaLabel"
        Me.Clv_facturaLabel.Size = New System.Drawing.Size(60, 13)
        Me.Clv_facturaLabel.TabIndex = 35
        Me.Clv_facturaLabel.Text = "clv factura:"
        '
        'StatusLabel
        '
        Me.StatusLabel.AutoSize = True
        Me.StatusLabel.Location = New System.Drawing.Point(732, 532)
        Me.StatusLabel.Name = "StatusLabel"
        Me.StatusLabel.Size = New System.Drawing.Size(40, 13)
        Me.StatusLabel.TabIndex = 36
        Me.StatusLabel.Text = "Status:"
        '
        'Button6
        '
        Me.Button6.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button6.Location = New System.Drawing.Point(806, 138)
        Me.Button6.Name = "Button6"
        Me.Button6.Size = New System.Drawing.Size(174, 33)
        Me.Button6.TabIndex = 19
        Me.Button6.Text = "Imprimir &Detalle"
        Me.Button6.UseVisualStyleBackColor = True
        Me.Button6.Visible = False
        '
        'Button5
        '
        Me.Button5.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button5.Location = New System.Drawing.Point(806, 186)
        Me.Button5.Name = "Button5"
        Me.Button5.Size = New System.Drawing.Size(174, 33)
        Me.Button5.TabIndex = 18
        Me.Button5.Text = "&Imprimir Ticket"
        Me.Button5.UseVisualStyleBackColor = True
        '
        'Button3
        '
        Me.Button3.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button3.Location = New System.Drawing.Point(809, 51)
        Me.Button3.Name = "Button3"
        Me.Button3.Size = New System.Drawing.Size(174, 33)
        Me.Button3.TabIndex = 16
        Me.Button3.Text = "&Afectar Clientes"
        Me.Button3.UseVisualStyleBackColor = True
        '
        'Button2
        '
        Me.Button2.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button2.Location = New System.Drawing.Point(809, 599)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(174, 30)
        Me.Button2.TabIndex = 20
        Me.Button2.Text = "&SALIR"
        Me.Button2.UseVisualStyleBackColor = True
        '
        'TextBox1
        '
        Me.TextBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBox1.Location = New System.Drawing.Point(637, 391)
        Me.TextBox1.Name = "TextBox1"
        Me.TextBox1.Size = New System.Drawing.Size(135, 24)
        Me.TextBox1.TabIndex = 23
        '
        'CMBLabel2
        '
        Me.CMBLabel2.AutoSize = True
        Me.CMBLabel2.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBLabel2.Location = New System.Drawing.Point(634, 426)
        Me.CMBLabel2.Name = "CMBLabel2"
        Me.CMBLabel2.Size = New System.Drawing.Size(59, 18)
        Me.CMBLabel2.TabIndex = 27
        Me.CMBLabel2.Text = "Fecha:"
        '
        'CMBLabel1
        '
        Me.CMBLabel1.AutoSize = True
        Me.CMBLabel1.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBLabel1.Location = New System.Drawing.Point(634, 370)
        Me.CMBLabel1.Name = "CMBLabel1"
        Me.CMBLabel1.Size = New System.Drawing.Size(79, 18)
        Me.CMBLabel1.TabIndex = 26
        Me.CMBLabel1.Text = "Contrato:"
        '
        'Busca_conciliacion_santaderDataGridView
        '
        Me.Busca_conciliacion_santaderDataGridView.AllowUserToAddRows = False
        Me.Busca_conciliacion_santaderDataGridView.AllowUserToDeleteRows = False
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.Busca_conciliacion_santaderDataGridView.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle1
        Me.Busca_conciliacion_santaderDataGridView.ColumnHeadersHeight = 40
        Me.Busca_conciliacion_santaderDataGridView.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.Clv_Archivo2, Me.Contrato, Me.cliente, Me.Importe, Me.ImporteFacturacion, Me.FechaPago, Me.Estado, Me.StatusCliente, Me.Consecutivo, Me.Ciclo})
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.Busca_conciliacion_santaderDataGridView.DefaultCellStyle = DataGridViewCellStyle2
        Me.Busca_conciliacion_santaderDataGridView.Location = New System.Drawing.Point(12, 225)
        Me.Busca_conciliacion_santaderDataGridView.Name = "Busca_conciliacion_santaderDataGridView"
        Me.Busca_conciliacion_santaderDataGridView.ReadOnly = True
        Me.Busca_conciliacion_santaderDataGridView.RowHeadersVisible = False
        Me.Busca_conciliacion_santaderDataGridView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.Busca_conciliacion_santaderDataGridView.Size = New System.Drawing.Size(971, 368)
        Me.Busca_conciliacion_santaderDataGridView.TabIndex = 27
        '
        'Busca_conciliacion_santaderBindingSource
        '
        Me.Busca_conciliacion_santaderBindingSource.DataMember = "Busca_conciliacion_santader"
        Me.Busca_conciliacion_santaderBindingSource.DataSource = Me.ProcedimientosArnoldo3
        '
        'ProcedimientosArnoldo3
        '
        Me.ProcedimientosArnoldo3.DataSetName = "ProcedimientosArnoldo3"
        Me.ProcedimientosArnoldo3.EnforceConstraints = False
        Me.ProcedimientosArnoldo3.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'DateTimePicker1
        '
        Me.DateTimePicker1.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.DateTimePicker1.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.DateTimePicker1.Location = New System.Drawing.Point(637, 447)
        Me.DateTimePicker1.Name = "DateTimePicker1"
        Me.DateTimePicker1.Size = New System.Drawing.Size(135, 24)
        Me.DateTimePicker1.TabIndex = 28
        '
        'CMBLabel3
        '
        Me.CMBLabel3.AutoSize = True
        Me.CMBLabel3.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBLabel3.Location = New System.Drawing.Point(4, 9)
        Me.CMBLabel3.Name = "CMBLabel3"
        Me.CMBLabel3.Size = New System.Drawing.Size(187, 18)
        Me.CMBLabel3.TabIndex = 29
        Me.CMBLabel3.Text = "Opciones de Búsqueda:"
        '
        'CMBPanel1
        '
        Me.CMBPanel1.Controls.Add(Me.btnBuscar)
        Me.CMBPanel1.Controls.Add(Me.Label3)
        Me.CMBPanel1.Controls.Add(Me.tbContrato)
        Me.CMBPanel1.Controls.Add(Me.Label2)
        Me.CMBPanel1.Controls.Add(Me.dgvArchivos)
        Me.CMBPanel1.Controls.Add(Me.dtFechaArchivo)
        Me.CMBPanel1.Controls.Add(Me.Label1)
        Me.CMBPanel1.Controls.Add(Me.CMBLabel3)
        Me.CMBPanel1.Controls.Add(Me.Button1)
        Me.CMBPanel1.Location = New System.Drawing.Point(12, 12)
        Me.CMBPanel1.Name = "CMBPanel1"
        Me.CMBPanel1.Size = New System.Drawing.Size(791, 204)
        Me.CMBPanel1.TabIndex = 30
        '
        'btnBuscar
        '
        Me.btnBuscar.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnBuscar.Location = New System.Drawing.Point(174, 156)
        Me.btnBuscar.Name = "btnBuscar"
        Me.btnBuscar.Size = New System.Drawing.Size(89, 26)
        Me.btnBuscar.TabIndex = 39
        Me.btnBuscar.Text = "Buscar"
        Me.btnBuscar.UseVisualStyleBackColor = True
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(306, 19)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(78, 18)
        Me.Label3.TabIndex = 42
        Me.Label3.Text = "Archivos:"
        '
        'tbContrato
        '
        Me.tbContrato.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbContrato.Location = New System.Drawing.Point(7, 115)
        Me.tbContrato.Name = "tbContrato"
        Me.tbContrato.Size = New System.Drawing.Size(135, 24)
        Me.tbContrato.TabIndex = 39
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(4, 94)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(259, 16)
        Me.Label2.TabIndex = 41
        Me.Label2.Text = "Archivos donde aparece el contrato:"
        '
        'dgvArchivos
        '
        Me.dgvArchivos.AllowUserToAddRows = False
        Me.dgvArchivos.AllowUserToDeleteRows = False
        DataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvArchivos.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle3
        Me.dgvArchivos.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvArchivos.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.Clv_Archivo, Me.Nombre, Me.FechaCarga, Me.Status})
        DataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle4.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgvArchivos.DefaultCellStyle = DataGridViewCellStyle4
        Me.dgvArchivos.Location = New System.Drawing.Point(304, 40)
        Me.dgvArchivos.Name = "dgvArchivos"
        Me.dgvArchivos.ReadOnly = True
        Me.dgvArchivos.RowHeadersVisible = False
        Me.dgvArchivos.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvArchivos.Size = New System.Drawing.Size(479, 156)
        Me.dgvArchivos.TabIndex = 40
        '
        'Clv_Archivo
        '
        Me.Clv_Archivo.DataPropertyName = "Clv_Archivo"
        Me.Clv_Archivo.HeaderText = "Clv_Archivo"
        Me.Clv_Archivo.Name = "Clv_Archivo"
        Me.Clv_Archivo.ReadOnly = True
        Me.Clv_Archivo.Visible = False
        '
        'Nombre
        '
        Me.Nombre.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.Nombre.DataPropertyName = "NombreArchivo"
        Me.Nombre.HeaderText = "Nombre"
        Me.Nombre.Name = "Nombre"
        Me.Nombre.ReadOnly = True
        '
        'FechaCarga
        '
        Me.FechaCarga.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.FechaCarga.DataPropertyName = "FechaCarga"
        Me.FechaCarga.HeaderText = "Fecha"
        Me.FechaCarga.Name = "FechaCarga"
        Me.FechaCarga.ReadOnly = True
        '
        'Status
        '
        Me.Status.DataPropertyName = "Estado"
        Me.Status.HeaderText = "Status"
        Me.Status.Name = "Status"
        Me.Status.ReadOnly = True
        '
        'dtFechaArchivo
        '
        Me.dtFechaArchivo.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtFechaArchivo.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtFechaArchivo.Location = New System.Drawing.Point(7, 58)
        Me.dtFechaArchivo.Name = "dtFechaArchivo"
        Me.dtFechaArchivo.Size = New System.Drawing.Size(135, 24)
        Me.dtFechaArchivo.TabIndex = 39
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(4, 37)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(198, 16)
        Me.Label1.TabIndex = 39
        Me.Label1.Text = "Fecha de carga de archivo:"
        '
        'Button1
        '
        Me.Button1.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button1.Location = New System.Drawing.Point(148, 113)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(34, 26)
        Me.Button1.TabIndex = 21
        Me.Button1.Text = "&..."
        Me.Button1.UseVisualStyleBackColor = True
        '
        'CMBLabel5
        '
        Me.CMBLabel5.AutoSize = True
        Me.CMBLabel5.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBLabel5.Location = New System.Drawing.Point(634, 310)
        Me.CMBLabel5.Name = "CMBLabel5"
        Me.CMBLabel5.Size = New System.Drawing.Size(172, 18)
        Me.CMBLabel5.TabIndex = 32
        Me.CMBLabel5.Text = "Seleccione un Status:"
        '
        'Button7
        '
        Me.Button7.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button7.Location = New System.Drawing.Point(787, 446)
        Me.Button7.Name = "Button7"
        Me.Button7.Size = New System.Drawing.Size(89, 26)
        Me.Button7.TabIndex = 30
        Me.Button7.Text = "&BUSCAR"
        Me.Button7.UseVisualStyleBackColor = True
        '
        'ComboBox1
        '
        Me.ComboBox1.DataSource = Me.MuestraStatusConciliacionBindingSource
        Me.ComboBox1.DisplayMember = "Descripcion"
        Me.ComboBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ComboBox1.FormattingEnabled = True
        Me.ComboBox1.Location = New System.Drawing.Point(637, 331)
        Me.ComboBox1.Name = "ComboBox1"
        Me.ComboBox1.Size = New System.Drawing.Size(269, 26)
        Me.ComboBox1.TabIndex = 31
        Me.ComboBox1.ValueMember = "clave"
        '
        'MuestraStatusConciliacionBindingSource
        '
        Me.MuestraStatusConciliacionBindingSource.DataMember = "Muestra_Status_Conciliacion"
        Me.MuestraStatusConciliacionBindingSource.DataSource = Me.ProcedimientosArnoldo3BindingSource
        '
        'ProcedimientosArnoldo3BindingSource
        '
        Me.ProcedimientosArnoldo3BindingSource.DataSource = Me.ProcedimientosArnoldo3
        Me.ProcedimientosArnoldo3BindingSource.Position = 0
        '
        'CMBNombreLabel1
        '
        Me.CMBNombreLabel1.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.Busca_conciliacion_santaderBindingSource, "Nombre", True))
        Me.CMBNombreLabel1.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBNombreLabel1.ForeColor = System.Drawing.Color.DarkRed
        Me.CMBNombreLabel1.Location = New System.Drawing.Point(334, 393)
        Me.CMBNombreLabel1.Name = "CMBNombreLabel1"
        Me.CMBNombreLabel1.Size = New System.Drawing.Size(472, 26)
        Me.CMBNombreLabel1.TabIndex = 31
        '
        'Busca_conciliacion_santaderTableAdapter
        '
        Me.Busca_conciliacion_santaderTableAdapter.ClearBeforeFill = True
        '
        'Muestra_Status_ConciliacionTableAdapter
        '
        Me.Muestra_Status_ConciliacionTableAdapter.ClearBeforeFill = True
        '
        'Button4
        '
        Me.Button4.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button4.Location = New System.Drawing.Point(809, 12)
        Me.Button4.Name = "Button4"
        Me.Button4.Size = New System.Drawing.Size(174, 33)
        Me.Button4.TabIndex = 32
        Me.Button4.Text = "&Cargar Archivo"
        Me.Button4.UseVisualStyleBackColor = True
        '
        'Clv_sessionTextBox
        '
        Me.Clv_sessionTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.Busca_conciliacion_santaderBindingSource, "clv_session", True))
        Me.Clv_sessionTextBox.Location = New System.Drawing.Point(114, 535)
        Me.Clv_sessionTextBox.Name = "Clv_sessionTextBox"
        Me.Clv_sessionTextBox.Size = New System.Drawing.Size(100, 20)
        Me.Clv_sessionTextBox.TabIndex = 33
        '
        'ClienteTextBox
        '
        Me.ClienteTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.Busca_conciliacion_santaderBindingSource, "cliente", True))
        Me.ClienteTextBox.Location = New System.Drawing.Point(356, 532)
        Me.ClienteTextBox.Name = "ClienteTextBox"
        Me.ClienteTextBox.Size = New System.Drawing.Size(100, 20)
        Me.ClienteTextBox.TabIndex = 34
        '
        'OpenFileDialog1
        '
        Me.OpenFileDialog1.FileName = "OpenFileDialog1"
        '
        'Clv_facturaTextBox
        '
        Me.Clv_facturaTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.Busca_conciliacion_santaderBindingSource, "clv_factura", True))
        Me.Clv_facturaTextBox.Location = New System.Drawing.Point(587, 529)
        Me.Clv_facturaTextBox.Name = "Clv_facturaTextBox"
        Me.Clv_facturaTextBox.Size = New System.Drawing.Size(100, 20)
        Me.Clv_facturaTextBox.TabIndex = 36
        '
        'StatusTextBox
        '
        Me.StatusTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.Busca_conciliacion_santaderBindingSource, "Status", True))
        Me.StatusTextBox.Location = New System.Drawing.Point(778, 529)
        Me.StatusTextBox.Name = "StatusTextBox"
        Me.StatusTextBox.Size = New System.Drawing.Size(100, 20)
        Me.StatusTextBox.TabIndex = 37
        '
        'CMBPanel3
        '
        Me.CMBPanel3.Controls.Add(Me.BindingNavigator1)
        Me.CMBPanel3.Location = New System.Drawing.Point(12, 374)
        Me.CMBPanel3.Name = "CMBPanel3"
        Me.CMBPanel3.Size = New System.Drawing.Size(952, 31)
        Me.CMBPanel3.TabIndex = 38
        Me.CMBPanel3.Visible = False
        '
        'BindingNavigator1
        '
        Me.BindingNavigator1.AddNewItem = Nothing
        Me.BindingNavigator1.CountItem = Nothing
        Me.BindingNavigator1.DeleteItem = Nothing
        Me.BindingNavigator1.ImageScalingSize = New System.Drawing.Size(20, 20)
        Me.BindingNavigator1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.BindingNavigatorSeparator2, Me.GuardarToolStripButton, Me.tsbEliminar})
        Me.BindingNavigator1.Location = New System.Drawing.Point(0, 0)
        Me.BindingNavigator1.MoveFirstItem = Nothing
        Me.BindingNavigator1.MoveLastItem = Nothing
        Me.BindingNavigator1.MoveNextItem = Nothing
        Me.BindingNavigator1.MovePreviousItem = Nothing
        Me.BindingNavigator1.Name = "BindingNavigator1"
        Me.BindingNavigator1.PositionItem = Nothing
        Me.BindingNavigator1.Size = New System.Drawing.Size(952, 25)
        Me.BindingNavigator1.TabIndex = 0
        Me.BindingNavigator1.Text = "BindingNavigator1"
        '
        'BindingNavigatorSeparator2
        '
        Me.BindingNavigatorSeparator2.Name = "BindingNavigatorSeparator2"
        Me.BindingNavigatorSeparator2.Size = New System.Drawing.Size(6, 25)
        '
        'GuardarToolStripButton
        '
        Me.GuardarToolStripButton.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right
        Me.GuardarToolStripButton.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GuardarToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.GuardarToolStripButton.Name = "GuardarToolStripButton"
        Me.GuardarToolStripButton.Size = New System.Drawing.Size(113, 22)
        Me.GuardarToolStripButton.Text = "&Guardar Detalle"
        '
        'tsbEliminar
        '
        Me.tsbEliminar.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right
        Me.tsbEliminar.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold)
        Me.tsbEliminar.Name = "tsbEliminar"
        Me.tsbEliminar.RightToLeftAutoMirrorImage = True
        Me.tsbEliminar.Size = New System.Drawing.Size(65, 22)
        Me.tsbEliminar.Text = "&Eliminar"
        '
        'Button8
        '
        Me.Button8.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button8.Location = New System.Drawing.Point(809, 89)
        Me.Button8.Name = "Button8"
        Me.Button8.Size = New System.Drawing.Size(174, 33)
        Me.Button8.TabIndex = 39
        Me.Button8.Text = "Cancelar Archivo"
        Me.Button8.UseVisualStyleBackColor = True
        '
        'Clv_Archivo2
        '
        Me.Clv_Archivo2.DataPropertyName = "Clv_Archivo"
        Me.Clv_Archivo2.HeaderText = "Clv_Archivo2"
        Me.Clv_Archivo2.Name = "Clv_Archivo2"
        Me.Clv_Archivo2.ReadOnly = True
        Me.Clv_Archivo2.Visible = False
        '
        'Contrato
        '
        Me.Contrato.DataPropertyName = "Contrato"
        Me.Contrato.HeaderText = "Contrato"
        Me.Contrato.Name = "Contrato"
        Me.Contrato.ReadOnly = True
        '
        'cliente
        '
        Me.cliente.DataPropertyName = "NOMBRE"
        Me.cliente.HeaderText = "Cliente"
        Me.cliente.Name = "cliente"
        Me.cliente.ReadOnly = True
        Me.cliente.Width = 320
        '
        'Importe
        '
        Me.Importe.DataPropertyName = "Importe"
        Me.Importe.HeaderText = "Importe Pagado"
        Me.Importe.Name = "Importe"
        Me.Importe.ReadOnly = True
        Me.Importe.Width = 80
        '
        'ImporteFacturacion
        '
        Me.ImporteFacturacion.DataPropertyName = "ImporteCobra"
        Me.ImporteFacturacion.HeaderText = "Importe Facturación"
        Me.ImporteFacturacion.Name = "ImporteFacturacion"
        Me.ImporteFacturacion.ReadOnly = True
        '
        'FechaPago
        '
        Me.FechaPago.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.FechaPago.DataPropertyName = "FechaPago"
        Me.FechaPago.HeaderText = "Fecha de Pago"
        Me.FechaPago.Name = "FechaPago"
        Me.FechaPago.ReadOnly = True
        '
        'Estado
        '
        Me.Estado.DataPropertyName = "Estado"
        Me.Estado.HeaderText = "Status"
        Me.Estado.Name = "Estado"
        Me.Estado.ReadOnly = True
        '
        'StatusCliente
        '
        Me.StatusCliente.DataPropertyName = "StatusCliente"
        Me.StatusCliente.HeaderText = "Status Cliente"
        Me.StatusCliente.Name = "StatusCliente"
        Me.StatusCliente.ReadOnly = True
        '
        'Consecutivo
        '
        Me.Consecutivo.DataPropertyName = "Consecutivo"
        Me.Consecutivo.HeaderText = "Consecutivo"
        Me.Consecutivo.Name = "Consecutivo"
        Me.Consecutivo.ReadOnly = True
        Me.Consecutivo.Visible = False
        '
        'Ciclo
        '
        Me.Ciclo.DataPropertyName = "Clv_Periodo"
        Me.Ciclo.HeaderText = "Ciclo"
        Me.Ciclo.Name = "Ciclo"
        Me.Ciclo.ReadOnly = True
        Me.Ciclo.Width = 60
        '
        'FrmConciliacionClabe
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.AutoScroll = True
        Me.ClientSize = New System.Drawing.Size(997, 642)
        Me.Controls.Add(Me.Busca_conciliacion_santaderDataGridView)
        Me.Controls.Add(Me.Button8)
        Me.Controls.Add(Me.Button2)
        Me.Controls.Add(Me.CMBLabel5)
        Me.Controls.Add(Me.CMBPanel3)
        Me.Controls.Add(Me.Button7)
        Me.Controls.Add(Me.ComboBox1)
        Me.Controls.Add(Me.TextBox1)
        Me.Controls.Add(Me.DateTimePicker1)
        Me.Controls.Add(Me.CMBLabel1)
        Me.Controls.Add(Me.CMBLabel2)
        Me.Controls.Add(Me.CMBNombreLabel1)
        Me.Controls.Add(Me.CMBNombreLabel)
        Me.Controls.Add(Me.Button4)
        Me.Controls.Add(Me.CMBPanel1)
        Me.Controls.Add(Me.Button6)
        Me.Controls.Add(Me.Button5)
        Me.Controls.Add(Me.Button3)
        Me.Controls.Add(Me.StatusLabel)
        Me.Controls.Add(Me.StatusTextBox)
        Me.Controls.Add(Me.Clv_facturaLabel)
        Me.Controls.Add(Me.Clv_facturaTextBox)
        Me.Controls.Add(Me.ClienteLabel)
        Me.Controls.Add(Me.ClienteTextBox)
        Me.Controls.Add(Me.Clv_sessionLabel)
        Me.Controls.Add(Me.Clv_sessionTextBox)
        Me.MaximizeBox = False
        Me.MaximumSize = New System.Drawing.Size(1013, 680)
        Me.MinimizeBox = False
        Me.MinimumSize = New System.Drawing.Size(1013, 597)
        Me.Name = "FrmConciliacionClabe"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Conciliación Bancaria de Cuentas CLABE"
        CType(Me.Busca_conciliacion_santaderDataGridView, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Busca_conciliacion_santaderBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ProcedimientosArnoldo3, System.ComponentModel.ISupportInitialize).EndInit()
        Me.CMBPanel1.ResumeLayout(False)
        Me.CMBPanel1.PerformLayout()
        CType(Me.dgvArchivos, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.MuestraStatusConciliacionBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ProcedimientosArnoldo3BindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.CMBPanel3.ResumeLayout(False)
        Me.CMBPanel3.PerformLayout()
        CType(Me.BindingNavigator1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.BindingNavigator1.ResumeLayout(False)
        Me.BindingNavigator1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Button6 As System.Windows.Forms.Button
    Friend WithEvents Button5 As System.Windows.Forms.Button
    Friend WithEvents Button3 As System.Windows.Forms.Button
    Friend WithEvents Button2 As System.Windows.Forms.Button
    Friend WithEvents TextBox1 As System.Windows.Forms.TextBox
    Friend WithEvents CMBLabel2 As System.Windows.Forms.Label
    Friend WithEvents CMBLabel1 As System.Windows.Forms.Label
    Friend WithEvents ProcedimientosArnoldo3 As softvFacturacion.ProcedimientosArnoldo3
    Friend WithEvents Busca_conciliacion_santaderBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Busca_conciliacion_santaderTableAdapter As softvFacturacion.ProcedimientosArnoldo3TableAdapters.Busca_conciliacion_santaderTableAdapter
    Friend WithEvents Busca_conciliacion_santaderDataGridView As System.Windows.Forms.DataGridView
    Friend WithEvents DateTimePicker1 As System.Windows.Forms.DateTimePicker
    Friend WithEvents CMBLabel3 As System.Windows.Forms.Label
    Friend WithEvents CMBPanel1 As System.Windows.Forms.Panel
    Friend WithEvents Button7 As System.Windows.Forms.Button
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents ComboBox1 As System.Windows.Forms.ComboBox
    Friend WithEvents CMBLabel5 As System.Windows.Forms.Label
    Friend WithEvents MuestraStatusConciliacionBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents ProcedimientosArnoldo3BindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Muestra_Status_ConciliacionTableAdapter As softvFacturacion.ProcedimientosArnoldo3TableAdapters.Muestra_Status_ConciliacionTableAdapter
    Friend WithEvents CMBNombreLabel1 As System.Windows.Forms.Label
    Friend WithEvents Button4 As System.Windows.Forms.Button
    Friend WithEvents Clv_sessionTextBox As System.Windows.Forms.TextBox
    Friend WithEvents ClienteTextBox As System.Windows.Forms.TextBox
    Friend WithEvents OpenFileDialog1 As System.Windows.Forms.OpenFileDialog
    Friend WithEvents Clv_facturaTextBox As System.Windows.Forms.TextBox
    Friend WithEvents StatusTextBox As System.Windows.Forms.TextBox
    Friend WithEvents CMBPanel3 As System.Windows.Forms.Panel
    Friend WithEvents BindingNavigator1 As System.Windows.Forms.BindingNavigator
    Friend WithEvents BindingNavigatorSeparator2 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents GuardarToolStripButton As System.Windows.Forms.ToolStripButton
    Friend WithEvents tsbEliminar As System.Windows.Forms.ToolStripButton
    Friend WithEvents dtFechaArchivo As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents tbContrato As System.Windows.Forms.TextBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents dgvArchivos As System.Windows.Forms.DataGridView
    Friend WithEvents btnBuscar As System.Windows.Forms.Button
    Friend WithEvents Clv_Archivo As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Nombre As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents FechaCarga As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Status As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Button8 As System.Windows.Forms.Button
    Friend WithEvents CMBNombreLabel As System.Windows.Forms.Label
    Friend WithEvents Clv_sessionLabel As System.Windows.Forms.Label
    Friend WithEvents ClienteLabel As System.Windows.Forms.Label
    Friend WithEvents Clv_facturaLabel As System.Windows.Forms.Label
    Friend WithEvents StatusLabel As System.Windows.Forms.Label
    Friend WithEvents Clv_Archivo2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Contrato As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents cliente As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Importe As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ImporteFacturacion As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents FechaPago As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Estado As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents StatusCliente As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Consecutivo As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Ciclo As System.Windows.Forms.DataGridViewTextBoxColumn
End Class
