﻿Public Class FrmOpRepEspeciales

    Private Sub FrmOpRep_Activated(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Activated
        If op = 0 Then
            Me.CMBLabel3.Visible = False
        ElseIf op = 1 Then
            If GloOpRepGral = "C" Or GloOpRepGral = "T" Then
                Me.ComboBox1.Visible = True
                Me.ComboBox2.Visible = False
                Me.ComboBox3.Visible = False
                Me.CheckBox1.Visible = True
                Me.CMBLabel3.Visible = True
                Me.CMBLabel3.Text = "Escoje Cajero(a):"
            ElseIf GloOpRepGral = "V" Then
                Me.ComboBox1.Visible = False
                Me.ComboBox2.Visible = False
                Me.ComboBox3.Visible = False
                'Me.ComboBox4.Visible = True
                Me.ComboVendedores.Visible = True
                Me.CheckBox1.Visible = True
                Me.CMBLabel3.Visible = True
                Me.CMBLabel3.Text = "Escoje el Vendedor:"
            End If
        ElseIf op = 4 Then
            If GloOpRepGral = "V" Then
                Me.ComboBox1.Visible = True
                Me.ComboBox2.Visible = False
                Me.ComboBox3.Visible = False
                Me.CheckBox1.Visible = True
                Me.CMBLabel3.Visible = True
                Me.CMBLabel3.Text = "Escoje Cajero(a):"
            Else
                Me.ComboBox1.Visible = False
                Me.ComboBox2.Visible = False
                Me.ComboBox3.Visible = False
                'Me.ComboBox4.Visible = True
                Me.ComboVendedores.Visible = True
                Me.CheckBox1.Visible = True
                Me.CMBLabel3.Visible = True
                Me.CMBLabel3.Text = "Escoje el Vendedor:"

            End If

        ElseIf op = 2 And (GloOpRepGral = "C" Or GloOpRepGral = "V") Then
            Me.ComboBox1.Visible = False
            Me.ComboBox3.Visible = False
            Me.ComboBox2.Visible = True
            Me.CheckBox1.Visible = True
            Me.CMBLabel3.Visible = True
            Me.CMBLabel3.Text = "Escoje tu Caja:"
        ElseIf op = 2 And (GloOpRepGral = "T") Then
            Me.ComboBox1.Visible = False
            Me.ComboBox2.Visible = False
            Me.ComboBox3.Visible = True
            Me.CheckBox1.Visible = True
            Me.CMBLabel3.Visible = True
            Me.CMBLabel3.Text = "Escoje el nombre de la Sucursal:"
        ElseIf op = 7 Then
            Me.ComboBox1.Visible = True
            Me.ComboBox2.Visible = False
            Me.ComboBox3.Visible = False
            Me.CheckBox1.Visible = True
            Me.CMBLabel3.Visible = True
            Me.CMBLabel3.Text = "Escoje Cajero(a):"
        ElseIf op = 8 Then
            Me.ComboBox1.Visible = False
            Me.ComboBox2.Visible = False
            Me.ComboBox3.Visible = False
            Me.CheckBox1.Visible = False
            Me.CMBLabel3.Visible = False
            Me.CMBLabel3.Text = "Escoje el nombre de la Sucursal:"
        Else
            Me.ComboBox1.Visible = False
            Me.ComboBox2.Visible = False
            Me.ComboBox3.Visible = True
            Me.CheckBox1.Visible = True
            Me.CMBLabel3.Visible = True
            Me.CMBLabel3.Text = "Escoje el nombre de la Sucursal:"
        End If

    End Sub


    Private Sub FrmOpRep_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        colorea(Me)

        Me.DateTimePicker2.MaxDate = Today
        Me.DateTimePicker2.Value = Today

    End Sub


    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click

        If Me.CheckBox1.Checked = True Then
            Resumen = True
            Resumen1 = 1
            Resumen3 = True
            Resumen2 = 1
            FlagCaja = True
        Else
            Resumen1 = 0
            Resumen3 = False
            Resumen2 = 0
            Resumen = False
            FlagCaja = False
        End If
        If GloOpRepGral = "C" Or GloOpRepGral = "T" Then
            NomCajera = Me.ComboBox1.SelectedValue
        ElseIf GloOpRepGral = "V" And op = 4 Then
            NomCajera = Me.ComboBox1.SelectedValue
        Else
            NomCajera = Me.ComboVendedores.SelectedValue
        End If

        SelCajaResumen = Me.ComboBox2.SelectedValue
        Fecha_ini = Me.DateTimePicker1.Text
        Fecha_Fin = Me.DateTimePicker2.Text
        NomCaja = "999" ' CStr(Me.ComboBox2.SelectedValue)
        NomSucursal = "999" 'CStr(Me.ComboBox3.SelectedValue)
        BanderaReporte = True
        If Me.ComboBox1.Visible = True Then
            ExtraT = Me.ComboBox1.Text
        ElseIf Me.ComboBox2.Visible = True Then
            ExtraT = Me.ComboBox2.Text
        ElseIf Me.ComboBox3.Visible = True Then
            ExtraT = Me.ComboBox3.Text
        ElseIf Me.ComboVendedores.Visible = True Then
            ExtraT = Me.ComboVendedores.Text
        End If
        Me.Close()
    End Sub
    Private Sub DateTimePicker1_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles DateTimePicker1.LostFocus
        Me.DateTimePicker2.MinDate = Me.DateTimePicker1.Value
    End Sub

    Private Sub DateTimePicker1_ValueChanged_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles DateTimePicker1.ValueChanged
        Me.DateTimePicker2.MinDate = Me.DateTimePicker1.Value
    End Sub

    Private Sub DateTimePicker2_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DateTimePicker2.TextChanged

        'If Fecha_ini > Fecha_Fin Then
        '    MsgBox("La Fecha inicial no puede ser menor que la final", MsgBoxStyle.Information, "Error Fecha")
        'End If
    End Sub

    Private Sub DateTimePicker2_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles DateTimePicker2.ValueChanged
        Me.DateTimePicker1.MaxDate = Me.DateTimePicker2.Value
    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        Me.Close()
    End Sub

    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button3.Click
        If Me.CheckBox1.Checked = True Then
            Resumen = True
            Resumen1 = 1
            Resumen3 = True
            Resumen2 = 1
            FlagCaja = True
        Else
            Resumen1 = 0
            Resumen3 = False
            Resumen2 = 0
            Resumen = False
            FlagCaja = False
        End If
        If GloOpRepGral = "C" Or GloOpRepGral = "T" Then
            NomCajera = Me.ComboBox1.SelectedValue
        ElseIf GloOpRepGral = "V" And op = 4 Then
            NomCajera = Me.ComboBox1.SelectedValue
        Else
            NomCajera = Me.ComboVendedores.SelectedValue
        End If

        SelCajaResumen = Me.ComboBox2.SelectedValue
        Fecha_ini = Me.DateTimePicker1.Text
        Fecha_Fin = Me.DateTimePicker2.Text
        NomCaja = CStr(Me.ComboBox2.SelectedValue)
        NomSucursal = CStr(Me.ComboBox3.SelectedValue)
        BanderaReporte = True
        If Me.ComboBox1.Visible = True Then
            ExtraT = Me.ComboBox1.Text
        ElseIf Me.ComboBox2.Visible = True Then
            ExtraT = Me.ComboBox2.Text
        ElseIf Me.ComboBox3.Visible = True Then
            ExtraT = Me.ComboBox3.Text
        ElseIf Me.ComboVendedores.Visible = True Then
            ExtraT = Me.ComboVendedores.Text
        End If
        Locbndcortedet = True
        Me.Close()
    End Sub
End Class