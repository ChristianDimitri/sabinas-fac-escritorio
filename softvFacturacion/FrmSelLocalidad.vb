﻿Imports System.Data.SqlClient
Public Class FrmSelLocalidad
    Public VieneDeIdentificador As Integer = 0
    Private Sub FrmSelLocalidad_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        colorea(Me)
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@opcion", SqlDbType.Int, 1)
        BaseII.CreateMyParameter("@identificador", SqlDbType.Int, identificador)
        BaseII.CreateMyParameter("@clv_localidad", SqlDbType.Int, 0)
        BaseII.CreateMyParameter("@clv_usuario", SqlDbType.Int, GloClvUsuario)
        BaseII.Inserta("ProcedureSeleccionLocalidad")
        llenalistboxs()
    End Sub
    Private Sub llenalistboxs()
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@opcion", SqlDbType.Int, 2)
        BaseII.CreateMyParameter("@identificador", SqlDbType.Int, identificador)
        BaseII.CreateMyParameter("@clv_localidad", SqlDbType.Int, 0)
        BaseII.CreateMyParameter("@clv_usuario", SqlDbType.Int, GloClvUsuario)
        loquehay.DataSource = BaseII.ConsultaDT("ProcedureSeleccionLocalidad")
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@opcion", SqlDbType.Int, 3)
        BaseII.CreateMyParameter("@identificador", SqlDbType.Int, identificador)
        BaseII.CreateMyParameter("@clv_localidad", SqlDbType.Int, 0)
        BaseII.CreateMyParameter("@clv_usuario", SqlDbType.Int, GloClvUsuario)
        seleccion.DataSource = BaseII.ConsultaDT("ProcedureSeleccionLocalidad")
    End Sub
    Private Sub agregar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles agregar.Click
        If loquehay.Items.Count = 0 Then
            Exit Sub
        End If
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@opcion", SqlDbType.Int, 4)
        BaseII.CreateMyParameter("@identificador", SqlDbType.Int, identificador)
        BaseII.CreateMyParameter("@clv_localidad", SqlDbType.Int, loquehay.SelectedValue)
        BaseII.CreateMyParameter("@clv_usuario", SqlDbType.Int, GloClvUsuario)
        BaseII.Inserta("ProcedureSeleccionLocalidad")
        llenalistboxs()
    End Sub

    Private Sub quitar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles quitar.Click
        If seleccion.Items.Count = 0 Then
            Exit Sub
        End If
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@opcion", SqlDbType.Int, 5)
        BaseII.CreateMyParameter("@identificador", SqlDbType.Int, identificador)
        BaseII.CreateMyParameter("@clv_localidad", SqlDbType.Int, seleccion.SelectedValue)
        BaseII.CreateMyParameter("@clv_usuario", SqlDbType.Int, GloClvUsuario)
        BaseII.Inserta("ProcedureSeleccionLocalidad")
        llenalistboxs()
    End Sub

    Private Sub agregartodo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles agregartodo.Click
        If loquehay.Items.Count = 0 Then
            Exit Sub
        End If
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@opcion", SqlDbType.Int, 6)
        BaseII.CreateMyParameter("@identificador", SqlDbType.Int, identificador)
        BaseII.CreateMyParameter("@clv_localidad", SqlDbType.Int, 0)
        BaseII.CreateMyParameter("@clv_usuario", SqlDbType.Int, GloClvUsuario)
        BaseII.Inserta("ProcedureSeleccionLocalidad")
        llenalistboxs()
    End Sub

    Private Sub quitartodo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles quitartodo.Click
        If seleccion.Items.Count = 0 Then
            Exit Sub
        End If
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@opcion", SqlDbType.Int, 7)
        BaseII.CreateMyParameter("@identificador", SqlDbType.Int, identificador)
        BaseII.CreateMyParameter("@clv_localidad", SqlDbType.Int, 0)
        BaseII.CreateMyParameter("@clv_usuario", SqlDbType.Int, GloClvUsuario)
        BaseII.Inserta("ProcedureSeleccionLocalidad")
        llenalistboxs()
    End Sub

    Private Sub Button5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button5.Click
        Me.Close()
    End Sub

    Private Sub Button6_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button6.Click
        If seleccion.Items.Count > 0 Then
            'FrmSelCompania.Show()
            'Me.Close()
            If varfrmselcompania = "cortefac" Then
                FrmOpRep.Show()
            ElseIf varfrmselcompania = "relingreso" Then
                If GloBnd_Des_Men = True Then
                    FrmFechaIngresosConcepto.Show()
                    Me.Close()
                ElseIf LocbndPolizaCiudad = True Then
                    FrmFechaIngresosConcepto.Show()
                    Me.Close()
                Else
                    GloBnd_Des_Men = False
                    GloBnd_Des_Cont = False
                    LocbndPolizaCiudad = False
                    FrmFechaIngresosConcepto.Show()

                End If
            ElseIf varfrmselcompania = "" And varfrmselsucursal = "relingreso" Then
                FrmSelSucursal.Show()
                Me.Close()
            End If

            Me.Close()
        End If
        Me.Close()
    End Sub
End Class