Imports System.Data.SqlClient

Public Class ResumenFacturaGlobal
    Private Sub Llena_companias()
        Try
            BaseII.limpiaParametros()
            'BaseII.CreateMyParameter("@Op", SqlDbType.Int, 3)
            BaseII.CreateMyParameter("@ClvUsuario", SqlDbType.Int, GloClvUsuario)
            ComboBoxCompanias.DataSource = BaseII.ConsultaDT("Muestra_Compania_RelUsuario")
            ComboBoxCompanias.DisplayMember = "razon_social"
            ComboBoxCompanias.ValueMember = "id_compania"

            If ComboBoxCompanias.Items.Count > 0 Then
                ComboBoxCompanias.SelectedIndex = 0

            End If
            GloIdCompania = 0
            'ComboBoxCiudades.Text = ""
        Catch ex As Exception

        End Try
    End Sub
    Private Sub ResumenFacturaGlobal_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        colorea(Me)
        'TODO: esta l�nea de c�digo carga datos en la tabla 'NewsoftvDataSet2.MUESTRATIPOFACTGLO' Puede moverla o quitarla seg�n sea necesario.
        Llena_companias()
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        Me.MUESTRATIPOFACTGLOTableAdapter.Connection = CON
        Me.MUESTRATIPOFACTGLOTableAdapter.Fill(Me.NewsoftvDataSet2.MUESTRATIPOFACTGLO)
        CON.Close()
        bec_tipo = Me.ComboBox2.SelectedValue
        DateTimePicker1.MaxDate = Today
    End Sub

    Private Sub DateTimePicker1_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles DateTimePicker1.ValueChanged
        bec_fecha = Me.DateTimePicker1.Text
    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        Me.Close()
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        'If ComboBoxCompanias.SelectedValue = 0 Then
        '    MsgBox("Selecciona una Compa��a")
        '    Exit Sub
        'End If
        If ComboBoxCompanias.Items.Count > 0 Then
            GloIdCompania = ComboBoxCompanias.SelectedValue
            GloReporte = 6
            My.Forms.FrmImprimirRepGral.Show()
            Me.Close()
        End If
    End Sub

    Private Sub ComboBox2_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ComboBox2.SelectedIndexChanged
        bec_tipo = Me.ComboBox2.SelectedValue
    End Sub

    Private Sub ComboBoxCompanias_SelectedIndexChanged(sender As System.Object, e As System.EventArgs) Handles ComboBoxCompanias.SelectedIndexChanged
        Try
            GloIdCompania = ComboBoxCompanias.SelectedValue
        Catch ex As Exception

        End Try
    End Sub
End Class