<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class BrwFacturas
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.DataGridViewFacturas = New System.Windows.Forms.DataGridView()
        Me.Clv_Factura = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Serie = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Folio = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Factura = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Importe = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Fecha = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Tipo = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Contrato = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Nombre = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.TextBoxBusSerie = New System.Windows.Forms.TextBox()
        Me.ButtonBusSerie = New System.Windows.Forms.Button()
        Me.CMBLabel1 = New System.Windows.Forms.Label()
        Me.CMBLabel2 = New System.Windows.Forms.Label()
        Me.ButtonBusFolio = New System.Windows.Forms.Button()
        Me.TextBoxBusFolio = New System.Windows.Forms.TextBox()
        Me.DateTimePicker1 = New System.Windows.Forms.DateTimePicker()
        Me.ButtonBusFecha = New System.Windows.Forms.Button()
        Me.CMBLabel3 = New System.Windows.Forms.Label()
        Me.ButtonAceptar = New System.Windows.Forms.Button()
        Me.ButtonSalir = New System.Windows.Forms.Button()
        Me.ComboBoxTipoFac = New System.Windows.Forms.ComboBox()
        Me.CMBLabel4 = New System.Windows.Forms.Label()
        Me.CMBLabel5 = New System.Windows.Forms.Label()
        Me.ButtonBusContrato = New System.Windows.Forms.Button()
        Me.TextBoxBusContrato = New System.Windows.Forms.TextBox()
        Me.CMBLabel6 = New System.Windows.Forms.Label()
        Me.ButtonBusNombre = New System.Windows.Forms.Button()
        Me.TextBoxBusNombre = New System.Windows.Forms.TextBox()
        CType(Me.DataGridViewFacturas, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'DataGridViewFacturas
        '
        Me.DataGridViewFacturas.AllowUserToAddRows = False
        Me.DataGridViewFacturas.AllowUserToDeleteRows = False
        Me.DataGridViewFacturas.BackgroundColor = System.Drawing.Color.WhiteSmoke
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.DataGridViewFacturas.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle1
        Me.DataGridViewFacturas.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DataGridViewFacturas.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.Clv_Factura, Me.Serie, Me.Folio, Me.Factura, Me.Importe, Me.Fecha, Me.Tipo, Me.Contrato, Me.Nombre})
        Me.DataGridViewFacturas.Location = New System.Drawing.Point(352, 15)
        Me.DataGridViewFacturas.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.DataGridViewFacturas.Name = "DataGridViewFacturas"
        Me.DataGridViewFacturas.ReadOnly = True
        Me.DataGridViewFacturas.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.DataGridViewFacturas.Size = New System.Drawing.Size(779, 874)
        Me.DataGridViewFacturas.TabIndex = 0
        '
        'Clv_Factura
        '
        Me.Clv_Factura.DataPropertyName = "Clv_Factura"
        Me.Clv_Factura.HeaderText = "Clv_Factura"
        Me.Clv_Factura.Name = "Clv_Factura"
        Me.Clv_Factura.ReadOnly = True
        Me.Clv_Factura.Visible = False
        '
        'Serie
        '
        Me.Serie.DataPropertyName = "Serie"
        Me.Serie.HeaderText = "Serie"
        Me.Serie.Name = "Serie"
        Me.Serie.ReadOnly = True
        Me.Serie.Width = 80
        '
        'Folio
        '
        Me.Folio.DataPropertyName = "Folio"
        Me.Folio.HeaderText = "Folio"
        Me.Folio.Name = "Folio"
        Me.Folio.ReadOnly = True
        Me.Folio.Width = 80
        '
        'Factura
        '
        Me.Factura.DataPropertyName = "Factura"
        Me.Factura.HeaderText = "Factura"
        Me.Factura.Name = "Factura"
        Me.Factura.ReadOnly = True
        Me.Factura.Visible = False
        '
        'Importe
        '
        Me.Importe.DataPropertyName = "Importe"
        Me.Importe.HeaderText = "Importe"
        Me.Importe.Name = "Importe"
        Me.Importe.ReadOnly = True
        '
        'Fecha
        '
        Me.Fecha.DataPropertyName = "Fecha"
        Me.Fecha.HeaderText = "Fecha"
        Me.Fecha.Name = "Fecha"
        Me.Fecha.ReadOnly = True
        '
        'Tipo
        '
        Me.Tipo.DataPropertyName = "Tipo"
        Me.Tipo.HeaderText = "Tipo"
        Me.Tipo.Name = "Tipo"
        Me.Tipo.ReadOnly = True
        Me.Tipo.Visible = False
        '
        'Contrato
        '
        Me.Contrato.DataPropertyName = "Contrato"
        Me.Contrato.HeaderText = "Contrato"
        Me.Contrato.Name = "Contrato"
        Me.Contrato.ReadOnly = True
        Me.Contrato.Width = 80
        '
        'Nombre
        '
        Me.Nombre.DataPropertyName = "Nombre"
        Me.Nombre.HeaderText = "Nombre"
        Me.Nombre.Name = "Nombre"
        Me.Nombre.ReadOnly = True
        Me.Nombre.Width = 200
        '
        'TextBoxBusSerie
        '
        Me.TextBoxBusSerie.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBoxBusSerie.Location = New System.Drawing.Point(37, 151)
        Me.TextBoxBusSerie.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.TextBoxBusSerie.Name = "TextBoxBusSerie"
        Me.TextBoxBusSerie.Size = New System.Drawing.Size(223, 24)
        Me.TextBoxBusSerie.TabIndex = 1
        '
        'ButtonBusSerie
        '
        Me.ButtonBusSerie.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ButtonBusSerie.Location = New System.Drawing.Point(37, 185)
        Me.ButtonBusSerie.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.ButtonBusSerie.Name = "ButtonBusSerie"
        Me.ButtonBusSerie.Size = New System.Drawing.Size(100, 28)
        Me.ButtonBusSerie.TabIndex = 2
        Me.ButtonBusSerie.Text = "Buscar"
        Me.ButtonBusSerie.UseVisualStyleBackColor = True
        '
        'CMBLabel1
        '
        Me.CMBLabel1.AutoSize = True
        Me.CMBLabel1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBLabel1.Location = New System.Drawing.Point(33, 129)
        Me.CMBLabel1.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.CMBLabel1.Name = "CMBLabel1"
        Me.CMBLabel1.Size = New System.Drawing.Size(47, 18)
        Me.CMBLabel1.TabIndex = 3
        Me.CMBLabel1.Text = "Serie"
        '
        'CMBLabel2
        '
        Me.CMBLabel2.AutoSize = True
        Me.CMBLabel2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBLabel2.Location = New System.Drawing.Point(36, 258)
        Me.CMBLabel2.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.CMBLabel2.Name = "CMBLabel2"
        Me.CMBLabel2.Size = New System.Drawing.Size(46, 18)
        Me.CMBLabel2.TabIndex = 6
        Me.CMBLabel2.Text = "Folio"
        '
        'ButtonBusFolio
        '
        Me.ButtonBusFolio.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ButtonBusFolio.Location = New System.Drawing.Point(37, 314)
        Me.ButtonBusFolio.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.ButtonBusFolio.Name = "ButtonBusFolio"
        Me.ButtonBusFolio.Size = New System.Drawing.Size(100, 28)
        Me.ButtonBusFolio.TabIndex = 5
        Me.ButtonBusFolio.Text = "Buscar"
        Me.ButtonBusFolio.UseVisualStyleBackColor = True
        '
        'TextBoxBusFolio
        '
        Me.TextBoxBusFolio.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBoxBusFolio.Location = New System.Drawing.Point(37, 281)
        Me.TextBoxBusFolio.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.TextBoxBusFolio.Name = "TextBoxBusFolio"
        Me.TextBoxBusFolio.Size = New System.Drawing.Size(223, 24)
        Me.TextBoxBusFolio.TabIndex = 4
        '
        'DateTimePicker1
        '
        Me.DateTimePicker1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.DateTimePicker1.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.DateTimePicker1.Location = New System.Drawing.Point(37, 401)
        Me.DateTimePicker1.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.DateTimePicker1.Name = "DateTimePicker1"
        Me.DateTimePicker1.Size = New System.Drawing.Size(223, 24)
        Me.DateTimePicker1.TabIndex = 7
        '
        'ButtonBusFecha
        '
        Me.ButtonBusFecha.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ButtonBusFecha.Location = New System.Drawing.Point(37, 434)
        Me.ButtonBusFecha.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.ButtonBusFecha.Name = "ButtonBusFecha"
        Me.ButtonBusFecha.Size = New System.Drawing.Size(100, 28)
        Me.ButtonBusFecha.TabIndex = 8
        Me.ButtonBusFecha.Text = "Buscar"
        Me.ButtonBusFecha.UseVisualStyleBackColor = True
        '
        'CMBLabel3
        '
        Me.CMBLabel3.AutoSize = True
        Me.CMBLabel3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBLabel3.Location = New System.Drawing.Point(33, 379)
        Me.CMBLabel3.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.CMBLabel3.Name = "CMBLabel3"
        Me.CMBLabel3.Size = New System.Drawing.Size(54, 18)
        Me.CMBLabel3.TabIndex = 9
        Me.CMBLabel3.Text = "Fecha"
        '
        'ButtonAceptar
        '
        Me.ButtonAceptar.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ButtonAceptar.Location = New System.Drawing.Point(1147, 15)
        Me.ButtonAceptar.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.ButtonAceptar.Name = "ButtonAceptar"
        Me.ButtonAceptar.Size = New System.Drawing.Size(181, 69)
        Me.ButtonAceptar.TabIndex = 10
        Me.ButtonAceptar.Text = "&COBRO ERRONEO"
        Me.ButtonAceptar.UseVisualStyleBackColor = True
        '
        'ButtonSalir
        '
        Me.ButtonSalir.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ButtonSalir.Location = New System.Drawing.Point(1147, 839)
        Me.ButtonSalir.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.ButtonSalir.Name = "ButtonSalir"
        Me.ButtonSalir.Size = New System.Drawing.Size(181, 44)
        Me.ButtonSalir.TabIndex = 11
        Me.ButtonSalir.Text = "&SALIR"
        Me.ButtonSalir.UseVisualStyleBackColor = True
        '
        'ComboBoxTipoFac
        '
        Me.ComboBoxTipoFac.DisplayMember = "Concepto"
        Me.ComboBoxTipoFac.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ComboBoxTipoFac.ForeColor = System.Drawing.Color.Red
        Me.ComboBoxTipoFac.FormattingEnabled = True
        Me.ComboBoxTipoFac.Location = New System.Drawing.Point(16, 49)
        Me.ComboBoxTipoFac.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.ComboBoxTipoFac.Name = "ComboBoxTipoFac"
        Me.ComboBoxTipoFac.Size = New System.Drawing.Size(279, 33)
        Me.ComboBoxTipoFac.TabIndex = 12
        Me.ComboBoxTipoFac.ValueMember = "Clave"
        '
        'CMBLabel4
        '
        Me.CMBLabel4.AutoSize = True
        Me.CMBLabel4.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBLabel4.Location = New System.Drawing.Point(13, 21)
        Me.CMBLabel4.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.CMBLabel4.Name = "CMBLabel4"
        Me.CMBLabel4.Size = New System.Drawing.Size(164, 25)
        Me.CMBLabel4.TabIndex = 13
        Me.CMBLabel4.Text = "Tipo de Factura"
        '
        'CMBLabel5
        '
        Me.CMBLabel5.AutoSize = True
        Me.CMBLabel5.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBLabel5.Location = New System.Drawing.Point(33, 517)
        Me.CMBLabel5.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.CMBLabel5.Name = "CMBLabel5"
        Me.CMBLabel5.Size = New System.Drawing.Size(74, 18)
        Me.CMBLabel5.TabIndex = 16
        Me.CMBLabel5.Text = "Contrato"
        '
        'ButtonBusContrato
        '
        Me.ButtonBusContrato.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ButtonBusContrato.Location = New System.Drawing.Point(37, 572)
        Me.ButtonBusContrato.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.ButtonBusContrato.Name = "ButtonBusContrato"
        Me.ButtonBusContrato.Size = New System.Drawing.Size(100, 28)
        Me.ButtonBusContrato.TabIndex = 15
        Me.ButtonBusContrato.Text = "Buscar"
        Me.ButtonBusContrato.UseVisualStyleBackColor = True
        '
        'TextBoxBusContrato
        '
        Me.TextBoxBusContrato.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBoxBusContrato.Location = New System.Drawing.Point(37, 539)
        Me.TextBoxBusContrato.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.TextBoxBusContrato.Name = "TextBoxBusContrato"
        Me.TextBoxBusContrato.Size = New System.Drawing.Size(223, 24)
        Me.TextBoxBusContrato.TabIndex = 14
        '
        'CMBLabel6
        '
        Me.CMBLabel6.AutoSize = True
        Me.CMBLabel6.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBLabel6.Location = New System.Drawing.Point(33, 644)
        Me.CMBLabel6.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.CMBLabel6.Name = "CMBLabel6"
        Me.CMBLabel6.Size = New System.Drawing.Size(68, 18)
        Me.CMBLabel6.TabIndex = 19
        Me.CMBLabel6.Text = "Nombre"
        '
        'ButtonBusNombre
        '
        Me.ButtonBusNombre.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ButtonBusNombre.Location = New System.Drawing.Point(37, 699)
        Me.ButtonBusNombre.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.ButtonBusNombre.Name = "ButtonBusNombre"
        Me.ButtonBusNombre.Size = New System.Drawing.Size(100, 28)
        Me.ButtonBusNombre.TabIndex = 18
        Me.ButtonBusNombre.Text = "Buscar"
        Me.ButtonBusNombre.UseVisualStyleBackColor = True
        '
        'TextBoxBusNombre
        '
        Me.TextBoxBusNombre.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBoxBusNombre.Location = New System.Drawing.Point(37, 666)
        Me.TextBoxBusNombre.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.TextBoxBusNombre.Name = "TextBoxBusNombre"
        Me.TextBoxBusNombre.Size = New System.Drawing.Size(257, 24)
        Me.TextBoxBusNombre.TabIndex = 17
        '
        'BrwFacturas
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.AutoScroll = True
        Me.BackColor = System.Drawing.Color.WhiteSmoke
        Me.ClientSize = New System.Drawing.Size(1344, 898)
        Me.Controls.Add(Me.CMBLabel6)
        Me.Controls.Add(Me.ButtonBusNombre)
        Me.Controls.Add(Me.TextBoxBusNombre)
        Me.Controls.Add(Me.CMBLabel5)
        Me.Controls.Add(Me.ButtonBusContrato)
        Me.Controls.Add(Me.TextBoxBusContrato)
        Me.Controls.Add(Me.CMBLabel4)
        Me.Controls.Add(Me.ComboBoxTipoFac)
        Me.Controls.Add(Me.ButtonSalir)
        Me.Controls.Add(Me.ButtonAceptar)
        Me.Controls.Add(Me.CMBLabel3)
        Me.Controls.Add(Me.ButtonBusFecha)
        Me.Controls.Add(Me.DateTimePicker1)
        Me.Controls.Add(Me.CMBLabel2)
        Me.Controls.Add(Me.ButtonBusFolio)
        Me.Controls.Add(Me.TextBoxBusFolio)
        Me.Controls.Add(Me.CMBLabel1)
        Me.Controls.Add(Me.ButtonBusSerie)
        Me.Controls.Add(Me.TextBoxBusSerie)
        Me.Controls.Add(Me.DataGridViewFacturas)
        Me.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Name = "BrwFacturas"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Historial de Pagos"
        CType(Me.DataGridViewFacturas, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents DataGridViewFacturas As System.Windows.Forms.DataGridView
    Friend WithEvents TextBoxBusSerie As System.Windows.Forms.TextBox
    Friend WithEvents ButtonBusSerie As System.Windows.Forms.Button
    Friend WithEvents CMBLabel1 As System.Windows.Forms.Label
    Friend WithEvents CMBLabel2 As System.Windows.Forms.Label
    Friend WithEvents ButtonBusFolio As System.Windows.Forms.Button
    Friend WithEvents TextBoxBusFolio As System.Windows.Forms.TextBox
    Friend WithEvents DateTimePicker1 As System.Windows.Forms.DateTimePicker
    Friend WithEvents ButtonBusFecha As System.Windows.Forms.Button
    Friend WithEvents CMBLabel3 As System.Windows.Forms.Label
    Friend WithEvents ButtonAceptar As System.Windows.Forms.Button
    Friend WithEvents ButtonSalir As System.Windows.Forms.Button
    Friend WithEvents Clv_Factura As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Serie As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Folio As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Factura As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Importe As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Fecha As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Tipo As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Contrato As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Nombre As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ComboBoxTipoFac As System.Windows.Forms.ComboBox
    Friend WithEvents CMBLabel4 As System.Windows.Forms.Label
    Friend WithEvents CMBLabel5 As System.Windows.Forms.Label
    Friend WithEvents ButtonBusContrato As System.Windows.Forms.Button
    Friend WithEvents TextBoxBusContrato As System.Windows.Forms.TextBox
    Friend WithEvents CMBLabel6 As System.Windows.Forms.Label
    Friend WithEvents ButtonBusNombre As System.Windows.Forms.Button
    Friend WithEvents TextBoxBusNombre As System.Windows.Forms.TextBox
End Class
