<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class BrwNotasdeCreditoLogitel
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.Button10 = New System.Windows.Forms.Button()
        Me.Button9 = New System.Windows.Forms.Button()
        Me.Button8 = New System.Windows.Forms.Button()
        Me.Button6 = New System.Windows.Forms.Button()
        Me.Button2 = New System.Windows.Forms.Button()
        Me.Panel5 = New System.Windows.Forms.Panel()
        Me.Button3 = New System.Windows.Forms.Button()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Button4 = New System.Windows.Forms.Button()
        Me.CONTRATOTextBox = New System.Windows.Forms.TextBox()
        Me.NOMBRETextBox = New System.Windows.Forms.TextBox()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.Button5 = New System.Windows.Forms.Button()
        Me.SplitContainer1 = New System.Windows.Forms.SplitContainer()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.LabelFactura = New System.Windows.Forms.Label()
        Me.LabelStatus = New System.Windows.Forms.Label()
        Me.LabelAplicada = New System.Windows.Forms.Label()
        Me.LabelFecha = New System.Windows.Forms.Label()
        Me.LabelContrato = New System.Windows.Forms.Label()
        Me.LabelNotaDeCredito = New System.Windows.Forms.Label()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.Button11 = New System.Windows.Forms.Button()
        Me.CMBLabel5 = New System.Windows.Forms.Label()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Button7 = New System.Windows.Forms.Button()
        Me.FECHATextBox = New System.Windows.Forms.TextBox()
        Me.TextBox1 = New System.Windows.Forms.TextBox()
        Me.SERIETextBox = New System.Windows.Forms.TextBox()
        Me.CMBLabel1 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.DataGridView1 = New System.Windows.Forms.DataGridView()
        Me.Clv_NotaDeCredito = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Contrato = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Fecha = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Aplicada = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Monto = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Status = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Factura = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DetalleNOTASDECREDITODataGridView = New System.Windows.Forms.DataGridView()
        Me.DataGridViewTextBoxColumn3 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn4 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DetalleNOTASDECREDITOBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.DataSetLydia = New softvFacturacion.DataSetLydia()
        Me.MUESTRASUCURSALES2BindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.BUSCANOTASDECREDITOBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.MUESTRASUCURSALES2TableAdapter = New softvFacturacion.DataSetLydiaTableAdapters.MUESTRASUCURSALES2TableAdapter()
        Me.BUSCANOTASDECREDITOTableAdapter = New softvFacturacion.DataSetLydiaTableAdapters.BUSCANOTASDECREDITOTableAdapter()
        Me.DetalleNOTASDECREDITOTableAdapter = New softvFacturacion.DataSetLydiaTableAdapters.DetalleNOTASDECREDITOTableAdapter()
        Me.Cancela_NotaCreditoBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Cancela_NotaCreditoTableAdapter = New softvFacturacion.DataSetLydiaTableAdapters.Cancela_NotaCreditoTableAdapter()
        Me.Panel5.SuspendLayout()
        Me.SplitContainer1.Panel1.SuspendLayout()
        Me.SplitContainer1.Panel2.SuspendLayout()
        Me.SplitContainer1.SuspendLayout()
        Me.Panel1.SuspendLayout()
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DetalleNOTASDECREDITODataGridView, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DetalleNOTASDECREDITOBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DataSetLydia, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.MUESTRASUCURSALES2BindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.BUSCANOTASDECREDITOBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Cancela_NotaCreditoBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Button10
        '
        Me.Button10.BackColor = System.Drawing.Color.DarkOrange
        Me.Button10.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button10.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button10.ForeColor = System.Drawing.Color.Black
        Me.Button10.Location = New System.Drawing.Point(1155, 82)
        Me.Button10.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Button10.Name = "Button10"
        Me.Button10.Size = New System.Drawing.Size(181, 44)
        Me.Button10.TabIndex = 46
        Me.Button10.Text = "&CONSULTA"
        Me.Button10.UseVisualStyleBackColor = False
        '
        'Button9
        '
        Me.Button9.BackColor = System.Drawing.Color.DarkOrange
        Me.Button9.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button9.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button9.ForeColor = System.Drawing.Color.Black
        Me.Button9.Location = New System.Drawing.Point(1155, 143)
        Me.Button9.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Button9.Name = "Button9"
        Me.Button9.Size = New System.Drawing.Size(181, 44)
        Me.Button9.TabIndex = 45
        Me.Button9.Text = "&MODIFICAR"
        Me.Button9.UseVisualStyleBackColor = False
        '
        'Button8
        '
        Me.Button8.BackColor = System.Drawing.Color.DarkOrange
        Me.Button8.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button8.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button8.ForeColor = System.Drawing.Color.Black
        Me.Button8.Location = New System.Drawing.Point(1155, 23)
        Me.Button8.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Button8.Name = "Button8"
        Me.Button8.Size = New System.Drawing.Size(181, 44)
        Me.Button8.TabIndex = 44
        Me.Button8.Text = "&NUEVO"
        Me.Button8.UseVisualStyleBackColor = False
        '
        'Button6
        '
        Me.Button6.BackColor = System.Drawing.Color.Orange
        Me.Button6.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button6.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button6.ForeColor = System.Drawing.Color.Black
        Me.Button6.Location = New System.Drawing.Point(1155, 209)
        Me.Button6.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Button6.Name = "Button6"
        Me.Button6.Size = New System.Drawing.Size(181, 102)
        Me.Button6.TabIndex = 38
        Me.Button6.Text = "&ReImprimir  Nota de Crédito"
        Me.Button6.UseVisualStyleBackColor = False
        '
        'Button2
        '
        Me.Button2.BackColor = System.Drawing.Color.Orange
        Me.Button2.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button2.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button2.ForeColor = System.Drawing.Color.Black
        Me.Button2.Location = New System.Drawing.Point(1155, 330)
        Me.Button2.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(181, 102)
        Me.Button2.TabIndex = 39
        Me.Button2.Text = "&Cancelar Nota de Crédito"
        Me.Button2.UseVisualStyleBackColor = False
        Me.Button2.Visible = False
        '
        'Panel5
        '
        Me.Panel5.Controls.Add(Me.Button3)
        Me.Panel5.Controls.Add(Me.Label6)
        Me.Panel5.Controls.Add(Me.Button4)
        Me.Panel5.Controls.Add(Me.CONTRATOTextBox)
        Me.Panel5.Controls.Add(Me.NOMBRETextBox)
        Me.Panel5.Controls.Add(Me.Label7)
        Me.Panel5.Location = New System.Drawing.Point(23, 300)
        Me.Panel5.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Panel5.Name = "Panel5"
        Me.Panel5.Size = New System.Drawing.Size(357, 208)
        Me.Panel5.TabIndex = 40
        '
        'Button3
        '
        Me.Button3.BackColor = System.Drawing.Color.DarkOrange
        Me.Button3.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button3.ForeColor = System.Drawing.Color.Black
        Me.Button3.Location = New System.Drawing.Point(19, 68)
        Me.Button3.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Button3.Name = "Button3"
        Me.Button3.Size = New System.Drawing.Size(117, 28)
        Me.Button3.TabIndex = 8
        Me.Button3.Text = "&Buscar"
        Me.Button3.UseVisualStyleBackColor = False
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.Location = New System.Drawing.Point(15, 12)
        Me.Label6.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(84, 18)
        Me.Label6.TabIndex = 21
        Me.Label6.Text = "Contrato :"
        '
        'Button4
        '
        Me.Button4.BackColor = System.Drawing.Color.DarkOrange
        Me.Button4.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button4.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button4.ForeColor = System.Drawing.Color.Black
        Me.Button4.Location = New System.Drawing.Point(19, 176)
        Me.Button4.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Button4.Name = "Button4"
        Me.Button4.Size = New System.Drawing.Size(117, 28)
        Me.Button4.TabIndex = 10
        Me.Button4.Text = "&Buscar"
        Me.Button4.UseVisualStyleBackColor = False
        Me.Button4.Visible = False
        '
        'CONTRATOTextBox
        '
        Me.CONTRATOTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.CONTRATOTextBox.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.CONTRATOTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CONTRATOTextBox.Location = New System.Drawing.Point(19, 34)
        Me.CONTRATOTextBox.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.CONTRATOTextBox.Name = "CONTRATOTextBox"
        Me.CONTRATOTextBox.Size = New System.Drawing.Size(117, 24)
        Me.CONTRATOTextBox.TabIndex = 7
        '
        'NOMBRETextBox
        '
        Me.NOMBRETextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.NOMBRETextBox.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.NOMBRETextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.NOMBRETextBox.Location = New System.Drawing.Point(19, 143)
        Me.NOMBRETextBox.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.NOMBRETextBox.Name = "NOMBRETextBox"
        Me.NOMBRETextBox.Size = New System.Drawing.Size(331, 24)
        Me.NOMBRETextBox.TabIndex = 9
        Me.NOMBRETextBox.Visible = False
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7.Location = New System.Drawing.Point(15, 121)
        Me.Label7.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(78, 18)
        Me.Label7.TabIndex = 24
        Me.Label7.Text = "Nombre :"
        Me.Label7.Visible = False
        '
        'Button5
        '
        Me.Button5.BackColor = System.Drawing.Color.DarkOrange
        Me.Button5.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button5.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button5.ForeColor = System.Drawing.Color.Black
        Me.Button5.Location = New System.Drawing.Point(1155, 852)
        Me.Button5.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Button5.Name = "Button5"
        Me.Button5.Size = New System.Drawing.Size(181, 44)
        Me.Button5.TabIndex = 41
        Me.Button5.Text = "&SALIR"
        Me.Button5.UseVisualStyleBackColor = False
        '
        'SplitContainer1
        '
        Me.SplitContainer1.Location = New System.Drawing.Point(19, 15)
        Me.SplitContainer1.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.SplitContainer1.Name = "SplitContainer1"
        '
        'SplitContainer1.Panel1
        '
        Me.SplitContainer1.Panel1.AutoScroll = True
        Me.SplitContainer1.Panel1.BackColor = System.Drawing.Color.WhiteSmoke
        Me.SplitContainer1.Panel1.Controls.Add(Me.Label1)
        Me.SplitContainer1.Panel1.Controls.Add(Me.Panel1)
        Me.SplitContainer1.Panel1.Controls.Add(Me.Button11)
        Me.SplitContainer1.Panel1.Controls.Add(Me.CMBLabel5)
        Me.SplitContainer1.Panel1.Controls.Add(Me.Button1)
        Me.SplitContainer1.Panel1.Controls.Add(Me.Label5)
        Me.SplitContainer1.Panel1.Controls.Add(Me.Button7)
        Me.SplitContainer1.Panel1.Controls.Add(Me.FECHATextBox)
        Me.SplitContainer1.Panel1.Controls.Add(Me.TextBox1)
        Me.SplitContainer1.Panel1.Controls.Add(Me.SERIETextBox)
        Me.SplitContainer1.Panel1.Controls.Add(Me.CMBLabel1)
        Me.SplitContainer1.Panel1.Controls.Add(Me.Label3)
        Me.SplitContainer1.Panel1.Controls.Add(Me.Label2)
        '
        'SplitContainer1.Panel2
        '
        Me.SplitContainer1.Panel2.Controls.Add(Me.DataGridView1)
        Me.SplitContainer1.Panel2.Controls.Add(Me.DetalleNOTASDECREDITODataGridView)
        Me.SplitContainer1.Size = New System.Drawing.Size(1115, 881)
        Me.SplitContainer1.SplitterDistance = 370
        Me.SplitContainer1.SplitterWidth = 5
        Me.SplitContainer1.TabIndex = 42
        Me.SplitContainer1.TabStop = False
        '
        'Label1
        '
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(191, 203)
        Me.Label1.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(169, 21)
        Me.Label1.TabIndex = 29
        Me.Label1.Text = "Ej. : dd/mm/aaaa"
        '
        'Panel1
        '
        Me.Panel1.BackColor = System.Drawing.Color.DarkOrange
        Me.Panel1.Controls.Add(Me.LabelFactura)
        Me.Panel1.Controls.Add(Me.LabelStatus)
        Me.Panel1.Controls.Add(Me.LabelAplicada)
        Me.Panel1.Controls.Add(Me.LabelFecha)
        Me.Panel1.Controls.Add(Me.LabelContrato)
        Me.Panel1.Controls.Add(Me.LabelNotaDeCredito)
        Me.Panel1.Controls.Add(Me.Label13)
        Me.Panel1.Controls.Add(Me.Label12)
        Me.Panel1.Controls.Add(Me.Label11)
        Me.Panel1.Controls.Add(Me.Label10)
        Me.Panel1.Controls.Add(Me.Label9)
        Me.Panel1.Controls.Add(Me.Label4)
        Me.Panel1.Controls.Add(Me.Label8)
        Me.Panel1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Panel1.Location = New System.Drawing.Point(4, 475)
        Me.Panel1.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(363, 384)
        Me.Panel1.TabIndex = 27
        '
        'LabelFactura
        '
        Me.LabelFactura.Location = New System.Drawing.Point(156, 263)
        Me.LabelFactura.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.LabelFactura.Name = "LabelFactura"
        Me.LabelFactura.Size = New System.Drawing.Size(173, 26)
        Me.LabelFactura.TabIndex = 12
        Me.LabelFactura.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'LabelStatus
        '
        Me.LabelStatus.Location = New System.Drawing.Point(156, 229)
        Me.LabelStatus.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.LabelStatus.Name = "LabelStatus"
        Me.LabelStatus.Size = New System.Drawing.Size(173, 26)
        Me.LabelStatus.TabIndex = 11
        Me.LabelStatus.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'LabelAplicada
        '
        Me.LabelAplicada.Location = New System.Drawing.Point(156, 188)
        Me.LabelAplicada.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.LabelAplicada.Name = "LabelAplicada"
        Me.LabelAplicada.Size = New System.Drawing.Size(173, 26)
        Me.LabelAplicada.TabIndex = 10
        Me.LabelAplicada.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'LabelFecha
        '
        Me.LabelFecha.Location = New System.Drawing.Point(156, 148)
        Me.LabelFecha.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.LabelFecha.Name = "LabelFecha"
        Me.LabelFecha.Size = New System.Drawing.Size(173, 26)
        Me.LabelFecha.TabIndex = 9
        Me.LabelFecha.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'LabelContrato
        '
        Me.LabelContrato.Location = New System.Drawing.Point(156, 112)
        Me.LabelContrato.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.LabelContrato.Name = "LabelContrato"
        Me.LabelContrato.Size = New System.Drawing.Size(173, 26)
        Me.LabelContrato.TabIndex = 8
        Me.LabelContrato.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'LabelNotaDeCredito
        '
        Me.LabelNotaDeCredito.Location = New System.Drawing.Point(156, 74)
        Me.LabelNotaDeCredito.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.LabelNotaDeCredito.Name = "LabelNotaDeCredito"
        Me.LabelNotaDeCredito.Size = New System.Drawing.Size(173, 26)
        Me.LabelNotaDeCredito.TabIndex = 7
        Me.LabelNotaDeCredito.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.Location = New System.Drawing.Point(72, 271)
        Me.Label13.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(70, 18)
        Me.Label13.TabIndex = 6
        Me.Label13.Text = "Factura:"
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Location = New System.Drawing.Point(83, 230)
        Me.Label12.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(61, 18)
        Me.Label12.TabIndex = 5
        Me.Label12.Text = "Status:"
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Location = New System.Drawing.Point(63, 188)
        Me.Label11.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(76, 18)
        Me.Label11.TabIndex = 4
        Me.Label11.Text = "Aplicada:"
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Location = New System.Drawing.Point(64, 113)
        Me.Label10.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(79, 18)
        Me.Label10.TabIndex = 3
        Me.Label10.Text = "Contrato:"
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Location = New System.Drawing.Point(84, 148)
        Me.Label9.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(59, 18)
        Me.Label9.TabIndex = 2
        Me.Label9.Text = "Fecha:"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(4, 74)
        Me.Label4.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(137, 18)
        Me.Label4.TabIndex = 1
        Me.Label4.Text = "Nota de Crédito: "
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label8.ForeColor = System.Drawing.Color.White
        Me.Label8.Location = New System.Drawing.Point(5, 5)
        Me.Label8.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(266, 24)
        Me.Label8.TabIndex = 0
        Me.Label8.Text = "Datos de la Nota de Crédito"
        '
        'Button11
        '
        Me.Button11.BackColor = System.Drawing.Color.DarkOrange
        Me.Button11.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button11.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button11.ForeColor = System.Drawing.Color.Black
        Me.Button11.Location = New System.Drawing.Point(27, 327)
        Me.Button11.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Button11.Name = "Button11"
        Me.Button11.Size = New System.Drawing.Size(88, 28)
        Me.Button11.TabIndex = 8
        Me.Button11.Text = "&Buscar"
        Me.Button11.UseVisualStyleBackColor = False
        '
        'CMBLabel5
        '
        Me.CMBLabel5.AutoSize = True
        Me.CMBLabel5.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBLabel5.ForeColor = System.Drawing.Color.FromArgb(CType(CType(128, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.CMBLabel5.Location = New System.Drawing.Point(17, 9)
        Me.CMBLabel5.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.CMBLabel5.Name = "CMBLabel5"
        Me.CMBLabel5.Size = New System.Drawing.Size(212, 29)
        Me.CMBLabel5.TabIndex = 0
        Me.CMBLabel5.Text = "Nota de Crédito  "
        '
        'Button1
        '
        Me.Button1.BackColor = System.Drawing.Color.DarkOrange
        Me.Button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button1.ForeColor = System.Drawing.Color.Black
        Me.Button1.Location = New System.Drawing.Point(27, 231)
        Me.Button1.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(88, 28)
        Me.Button1.TabIndex = 6
        Me.Button1.Text = "&Buscar"
        Me.Button1.UseVisualStyleBackColor = False
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.Location = New System.Drawing.Point(23, 272)
        Me.Label5.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(84, 18)
        Me.Label5.TabIndex = 21
        Me.Label5.Text = "Contrato :"
        '
        'Button7
        '
        Me.Button7.BackColor = System.Drawing.Color.DarkOrange
        Me.Button7.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button7.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button7.ForeColor = System.Drawing.Color.Black
        Me.Button7.Location = New System.Drawing.Point(27, 134)
        Me.Button7.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Button7.Name = "Button7"
        Me.Button7.Size = New System.Drawing.Size(88, 28)
        Me.Button7.TabIndex = 4
        Me.Button7.Text = "&Buscar"
        Me.Button7.UseVisualStyleBackColor = False
        '
        'FECHATextBox
        '
        Me.FECHATextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.FECHATextBox.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.FECHATextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FECHATextBox.Location = New System.Drawing.Point(27, 198)
        Me.FECHATextBox.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.FECHATextBox.Name = "FECHATextBox"
        Me.FECHATextBox.Size = New System.Drawing.Size(155, 24)
        Me.FECHATextBox.TabIndex = 5
        '
        'TextBox1
        '
        Me.TextBox1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.TextBox1.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.TextBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBox1.Location = New System.Drawing.Point(27, 294)
        Me.TextBox1.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.TextBox1.Name = "TextBox1"
        Me.TextBox1.Size = New System.Drawing.Size(187, 24)
        Me.TextBox1.TabIndex = 7
        '
        'SERIETextBox
        '
        Me.SERIETextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.SERIETextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.SERIETextBox.Location = New System.Drawing.Point(27, 101)
        Me.SERIETextBox.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.SERIETextBox.Name = "SERIETextBox"
        Me.SERIETextBox.Size = New System.Drawing.Size(187, 24)
        Me.SERIETextBox.TabIndex = 2
        '
        'CMBLabel1
        '
        Me.CMBLabel1.AutoSize = True
        Me.CMBLabel1.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBLabel1.ForeColor = System.Drawing.Color.FromArgb(CType(CType(128, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.CMBLabel1.Location = New System.Drawing.Point(16, 49)
        Me.CMBLabel1.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.CMBLabel1.Name = "CMBLabel1"
        Me.CMBLabel1.Size = New System.Drawing.Size(288, 25)
        Me.CMBLabel1.TabIndex = 0
        Me.CMBLabel1.Text = "Buscar Nota de Crédito Por :"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(23, 79)
        Me.Label3.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(137, 18)
        Me.Label3.TabIndex = 0
        Me.Label3.Text = "Nota de Crédito :"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(27, 176)
        Me.Label2.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(64, 18)
        Me.Label2.TabIndex = 6
        Me.Label2.Text = "Fecha :"
        '
        'DataGridView1
        '
        Me.DataGridView1.AllowUserToAddRows = False
        Me.DataGridView1.AllowUserToDeleteRows = False
        Me.DataGridView1.AllowUserToOrderColumns = True
        Me.DataGridView1.BackgroundColor = System.Drawing.Color.WhiteSmoke
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle1.BackColor = System.Drawing.Color.Chocolate
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle1.ForeColor = System.Drawing.Color.White
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.DataGridView1.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle1
        Me.DataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DataGridView1.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.Clv_NotaDeCredito, Me.Contrato, Me.Fecha, Me.Aplicada, Me.Monto, Me.Status, Me.Factura})
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.DataGridView1.DefaultCellStyle = DataGridViewCellStyle2
        Me.DataGridView1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.DataGridView1.Location = New System.Drawing.Point(0, 0)
        Me.DataGridView1.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.DataGridView1.Name = "DataGridView1"
        Me.DataGridView1.ReadOnly = True
        DataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.DataGridView1.RowHeadersDefaultCellStyle = DataGridViewCellStyle3
        Me.DataGridView1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.DataGridView1.Size = New System.Drawing.Size(740, 881)
        Me.DataGridView1.StandardTab = True
        Me.DataGridView1.TabIndex = 0
        Me.DataGridView1.TabStop = False
        '
        'Clv_NotaDeCredito
        '
        Me.Clv_NotaDeCredito.DataPropertyName = "Clv_NotaDeCredito"
        Me.Clv_NotaDeCredito.HeaderText = "NotaDeCredito"
        Me.Clv_NotaDeCredito.Name = "Clv_NotaDeCredito"
        Me.Clv_NotaDeCredito.ReadOnly = True
        Me.Clv_NotaDeCredito.Width = 120
        '
        'Contrato
        '
        Me.Contrato.DataPropertyName = "Contrato"
        Me.Contrato.HeaderText = "Contrato"
        Me.Contrato.Name = "Contrato"
        Me.Contrato.ReadOnly = True
        '
        'Fecha
        '
        Me.Fecha.DataPropertyName = "Fecha_DeGeneracion"
        Me.Fecha.HeaderText = "Fecha"
        Me.Fecha.Name = "Fecha"
        Me.Fecha.ReadOnly = True
        '
        'Aplicada
        '
        Me.Aplicada.DataPropertyName = "Aplicada"
        Me.Aplicada.HeaderText = "Aplicada"
        Me.Aplicada.Name = "Aplicada"
        Me.Aplicada.ReadOnly = True
        '
        'Monto
        '
        Me.Monto.DataPropertyName = "Monto"
        Me.Monto.HeaderText = "Monto"
        Me.Monto.Name = "Monto"
        Me.Monto.ReadOnly = True
        '
        'Status
        '
        Me.Status.DataPropertyName = "Status"
        Me.Status.HeaderText = "Status"
        Me.Status.Name = "Status"
        Me.Status.ReadOnly = True
        '
        'Factura
        '
        Me.Factura.DataPropertyName = "Factura"
        Me.Factura.HeaderText = "Factura"
        Me.Factura.Name = "Factura"
        Me.Factura.ReadOnly = True
        '
        'DetalleNOTASDECREDITODataGridView
        '
        Me.DetalleNOTASDECREDITODataGridView.AllowUserToAddRows = False
        Me.DetalleNOTASDECREDITODataGridView.AllowUserToDeleteRows = False
        Me.DetalleNOTASDECREDITODataGridView.AutoGenerateColumns = False
        Me.DetalleNOTASDECREDITODataGridView.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.DataGridViewTextBoxColumn3, Me.DataGridViewTextBoxColumn4, Me.DataGridViewTextBoxColumn2, Me.DataGridViewTextBoxColumn1})
        Me.DetalleNOTASDECREDITODataGridView.DataSource = Me.DetalleNOTASDECREDITOBindingSource
        Me.DetalleNOTASDECREDITODataGridView.Location = New System.Drawing.Point(-204, 788)
        Me.DetalleNOTASDECREDITODataGridView.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.DetalleNOTASDECREDITODataGridView.Name = "DetalleNOTASDECREDITODataGridView"
        Me.DetalleNOTASDECREDITODataGridView.ReadOnly = True
        Me.DetalleNOTASDECREDITODataGridView.Size = New System.Drawing.Size(103, 55)
        Me.DetalleNOTASDECREDITODataGridView.TabIndex = 47
        '
        'DataGridViewTextBoxColumn3
        '
        Me.DataGridViewTextBoxColumn3.DataPropertyName = "Column1"
        Me.DataGridViewTextBoxColumn3.HeaderText = "Factura"
        Me.DataGridViewTextBoxColumn3.Name = "DataGridViewTextBoxColumn3"
        Me.DataGridViewTextBoxColumn3.ReadOnly = True
        '
        'DataGridViewTextBoxColumn4
        '
        Me.DataGridViewTextBoxColumn4.DataPropertyName = "monto"
        Me.DataGridViewTextBoxColumn4.HeaderText = "Saldo"
        Me.DataGridViewTextBoxColumn4.Name = "DataGridViewTextBoxColumn4"
        Me.DataGridViewTextBoxColumn4.ReadOnly = True
        '
        'DataGridViewTextBoxColumn2
        '
        Me.DataGridViewTextBoxColumn2.DataPropertyName = "FECHA_degeneracion"
        Me.DataGridViewTextBoxColumn2.HeaderText = "Fecha de Generación"
        Me.DataGridViewTextBoxColumn2.Name = "DataGridViewTextBoxColumn2"
        Me.DataGridViewTextBoxColumn2.ReadOnly = True
        '
        'DataGridViewTextBoxColumn1
        '
        Me.DataGridViewTextBoxColumn1.DataPropertyName = "clv_Notadecredito"
        Me.DataGridViewTextBoxColumn1.HeaderText = "Folio de Nota de Crédito"
        Me.DataGridViewTextBoxColumn1.Name = "DataGridViewTextBoxColumn1"
        Me.DataGridViewTextBoxColumn1.ReadOnly = True
        '
        'DetalleNOTASDECREDITOBindingSource
        '
        Me.DetalleNOTASDECREDITOBindingSource.DataMember = "DetalleNOTASDECREDITO"
        Me.DetalleNOTASDECREDITOBindingSource.DataSource = Me.DataSetLydia
        '
        'DataSetLydia
        '
        Me.DataSetLydia.DataSetName = "DataSetLydia"
        Me.DataSetLydia.EnforceConstraints = False
        Me.DataSetLydia.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'MUESTRASUCURSALES2BindingSource
        '
        Me.MUESTRASUCURSALES2BindingSource.DataMember = "MUESTRASUCURSALES2"
        Me.MUESTRASUCURSALES2BindingSource.DataSource = Me.DataSetLydia
        '
        'BUSCANOTASDECREDITOBindingSource
        '
        Me.BUSCANOTASDECREDITOBindingSource.DataMember = "BUSCANOTASDECREDITO"
        Me.BUSCANOTASDECREDITOBindingSource.DataSource = Me.DataSetLydia
        '
        'MUESTRASUCURSALES2TableAdapter
        '
        Me.MUESTRASUCURSALES2TableAdapter.ClearBeforeFill = True
        '
        'BUSCANOTASDECREDITOTableAdapter
        '
        Me.BUSCANOTASDECREDITOTableAdapter.ClearBeforeFill = True
        '
        'DetalleNOTASDECREDITOTableAdapter
        '
        Me.DetalleNOTASDECREDITOTableAdapter.ClearBeforeFill = True
        '
        'Cancela_NotaCreditoBindingSource
        '
        Me.Cancela_NotaCreditoBindingSource.DataMember = "Cancela_NotaCredito"
        Me.Cancela_NotaCreditoBindingSource.DataSource = Me.DataSetLydia
        '
        'Cancela_NotaCreditoTableAdapter
        '
        Me.Cancela_NotaCreditoTableAdapter.ClearBeforeFill = True
        '
        'BrwNotasdeCreditoLogitel
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.AutoScroll = True
        Me.ClientSize = New System.Drawing.Size(1355, 912)
        Me.Controls.Add(Me.SplitContainer1)
        Me.Controls.Add(Me.Button10)
        Me.Controls.Add(Me.Button9)
        Me.Controls.Add(Me.Button8)
        Me.Controls.Add(Me.Button6)
        Me.Controls.Add(Me.Button2)
        Me.Controls.Add(Me.Panel5)
        Me.Controls.Add(Me.Button5)
        Me.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.MaximizeBox = False
        Me.Name = "BrwNotasdeCreditoLogitel"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Catálogo Notas de Crédito"
        Me.Panel5.ResumeLayout(False)
        Me.Panel5.PerformLayout()
        Me.SplitContainer1.Panel1.ResumeLayout(False)
        Me.SplitContainer1.Panel1.PerformLayout()
        Me.SplitContainer1.Panel2.ResumeLayout(False)
        Me.SplitContainer1.ResumeLayout(False)
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DetalleNOTASDECREDITODataGridView, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DetalleNOTASDECREDITOBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DataSetLydia, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.MUESTRASUCURSALES2BindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.BUSCANOTASDECREDITOBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Cancela_NotaCreditoBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents Button10 As System.Windows.Forms.Button
    Friend WithEvents Button9 As System.Windows.Forms.Button
    Friend WithEvents Button8 As System.Windows.Forms.Button
    Friend WithEvents Button6 As System.Windows.Forms.Button
    Friend WithEvents Button2 As System.Windows.Forms.Button
    Friend WithEvents Panel5 As System.Windows.Forms.Panel
    Friend WithEvents Button3 As System.Windows.Forms.Button
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Button4 As System.Windows.Forms.Button
    Friend WithEvents CONTRATOTextBox As System.Windows.Forms.TextBox
    Friend WithEvents NOMBRETextBox As System.Windows.Forms.TextBox
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents Button5 As System.Windows.Forms.Button
    Friend WithEvents SplitContainer1 As System.Windows.Forms.SplitContainer
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents CMBLabel5 As System.Windows.Forms.Label
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents Button7 As System.Windows.Forms.Button
    Friend WithEvents FECHATextBox As System.Windows.Forms.TextBox
    Friend WithEvents SERIETextBox As System.Windows.Forms.TextBox
    Friend WithEvents CMBLabel1 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents DataGridView1 As System.Windows.Forms.DataGridView
    Friend WithEvents Button11 As System.Windows.Forms.Button
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents TextBox1 As System.Windows.Forms.TextBox
    Friend WithEvents DataSetLydia As softvFacturacion.DataSetLydia
    Friend WithEvents FacturaDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents MUESTRASUCURSALES2BindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents MUESTRASUCURSALES2TableAdapter As softvFacturacion.DataSetLydiaTableAdapters.MUESTRASUCURSALES2TableAdapter
    Friend WithEvents BUSCANOTASDECREDITOBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents BUSCANOTASDECREDITOTableAdapter As softvFacturacion.DataSetLydiaTableAdapters.BUSCANOTASDECREDITOTableAdapter
    Friend WithEvents DetalleNOTASDECREDITOBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents DetalleNOTASDECREDITOTableAdapter As softvFacturacion.DataSetLydiaTableAdapters.DetalleNOTASDECREDITOTableAdapter
    Friend WithEvents DetalleNOTASDECREDITODataGridView As System.Windows.Forms.DataGridView
    Friend WithEvents DataGridViewTextBoxColumn3 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn4 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Cancela_NotaCreditoBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Cancela_NotaCreditoTableAdapter As softvFacturacion.DataSetLydiaTableAdapters.Cancela_NotaCreditoTableAdapter
    Friend WithEvents Clv_NotaDeCredito As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Contrato As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Fecha As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Aplicada As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Monto As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Status As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Factura As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents LabelFactura As System.Windows.Forms.Label
    Friend WithEvents LabelStatus As System.Windows.Forms.Label
    Friend WithEvents LabelAplicada As System.Windows.Forms.Label
    Friend WithEvents LabelFecha As System.Windows.Forms.Label
    Friend WithEvents LabelContrato As System.Windows.Forms.Label
    Friend WithEvents LabelNotaDeCredito As System.Windows.Forms.Label
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
End Class
