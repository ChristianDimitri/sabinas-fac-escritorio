﻿Imports System.Data.SqlClient
Public Class FrmSelCiudadJ

    Private Sub FrmSelCiudadJ_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If EntradasSelCiudad = 0 Then
            EntradasSelCiudad = 1
            Me.Close()
            FrmSelPlaza.Show()
            Exit Sub
        End If
        If EntradasSelCiudad = 1 Then
            EntradasSelCiudad = 0
        End If
        'Por si es uno
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@identificador", SqlDbType.BigInt, identificador)
        BaseII.CreateMyParameter("@numero", ParameterDirection.Output, SqlDbType.Int)
        BaseII.ProcedimientoOutPut("DameNumCiudadesUsuario")
        Dim numciudades As Integer = BaseII.dicoPar("@numero")
        If numciudades = 1 Then
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@identificador", SqlDbType.BigInt, identificador)
            BaseII.CreateMyParameter("@claveusuario", SqlDbType.Int, GloClvUsuario)
            BaseII.Inserta("InsertaSeleccionCiudadtbl")
            llevamealotro()
            Exit Sub

        End If
        colorea(Me)
        'BaseII.limpiaParametros()
        'BaseII.CreateMyParameter("@opcion", SqlDbType.Int, 1)
        'BaseII.CreateMyParameter("@clv_session", SqlDbType.Int, 0)
        'BaseII.CreateMyParameter("@clv_ciudad", SqlDbType.Int, 0)
        'BaseII.Inserta("ProcedureSeleccionCiudad")
        'Dim conexion As New SqlConnection(MiConexion)
        'conexion.Open()
        'Dim comando As New SqlCommand()
        'comando.Connection = conexion
        'comando.CommandText = " exec ProcedureSeleccionCiudad 1," + GloClvUsuario.ToString + ",0,0"
        'identificador = comando.ExecuteScalar()
        'conexion.Close()
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@opcion", SqlDbType.Int, 0)
        BaseII.CreateMyParameter("@clv_session", SqlDbType.Int, GloClvUsuario)
        BaseII.CreateMyParameter("@clv_ciudad", SqlDbType.Int, 0)
        BaseII.CreateMyParameter("@identificador", SqlDbType.Int, identificador)
        BaseII.Inserta("ProcedureSeleccionCiudad")
        llenalistboxs()
    End Sub
    Private Sub llenalistboxs()
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@opcion", SqlDbType.Int, 2)
        BaseII.CreateMyParameter("@clv_session", SqlDbType.Int, 0)
        BaseII.CreateMyParameter("@clv_ciudad", SqlDbType.Int, 0)
        BaseII.CreateMyParameter("@identificador", SqlDbType.Int, identificador)
        loquehay.DataSource = BaseII.ConsultaDT("ProcedureSeleccionCiudad")
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@opcion", SqlDbType.Int, 3)
        BaseII.CreateMyParameter("@clv_session", SqlDbType.Int, 0)
        BaseII.CreateMyParameter("@clv_ciudad", SqlDbType.Int, 0)
        BaseII.CreateMyParameter("@identificador", SqlDbType.Int, identificador)
        seleccion.DataSource = BaseII.ConsultaDT("ProcedureSeleccionCiudad")
    End Sub

    Private Sub agregar_Click(sender As System.Object, e As System.EventArgs) Handles agregar.Click
        If loquehay.Items.Count = 0 Then
            Exit Sub
        End If
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@opcion", SqlDbType.Int, 4)
        BaseII.CreateMyParameter("@clv_session", SqlDbType.Int, 0)
        BaseII.CreateMyParameter("@clv_ciudad", SqlDbType.Int, loquehay.SelectedValue)
        BaseII.CreateMyParameter("@identificador", SqlDbType.Int, identificador)
        BaseII.Inserta("ProcedureSeleccionCiudad")
        llenalistboxs()
    End Sub

    Private Sub quitar_Click(sender As System.Object, e As System.EventArgs) Handles quitar.Click
        If seleccion.Items.Count = 0 Then
            Exit Sub
        End If
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@opcion", SqlDbType.Int, 5)
        BaseII.CreateMyParameter("@clv_session", SqlDbType.Int, 0)
        BaseII.CreateMyParameter("@clv_ciudad", SqlDbType.Int, seleccion.SelectedValue)
        BaseII.CreateMyParameter("@identificador", SqlDbType.Int, identificador)
        BaseII.Inserta("ProcedureSeleccionCiudad")
        llenalistboxs()
    End Sub

    Private Sub agregartodo_Click(sender As System.Object, e As System.EventArgs) Handles agregartodo.Click
        If loquehay.Items.Count = 0 Then
            Exit Sub
        End If
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@opcion", SqlDbType.Int, 6)
        BaseII.CreateMyParameter("@clv_session", SqlDbType.Int, 0)
        BaseII.CreateMyParameter("@clv_ciudad", SqlDbType.Int, 0)
        BaseII.CreateMyParameter("@identificador", SqlDbType.Int, identificador)
        BaseII.Inserta("ProcedureSeleccionCiudad")
        llenalistboxs()
    End Sub

    Private Sub quitartodo_Click(sender As System.Object, e As System.EventArgs) Handles quitartodo.Click
        If seleccion.Items.Count = 0 Then
            Exit Sub
        End If
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@opcion", SqlDbType.Int, 7)
        BaseII.CreateMyParameter("@clv_session", SqlDbType.Int, 0)
        BaseII.CreateMyParameter("@clv_ciudad", SqlDbType.Int, 0)
        BaseII.CreateMyParameter("@identificador", SqlDbType.Int, identificador)
        BaseII.Inserta("ProcedureSeleccionCiudad")
        llenalistboxs()
    End Sub

    Private Sub Button5_Click(sender As System.Object, e As System.EventArgs) Handles Button5.Click
        Me.Close()
    End Sub

    Private Sub Button6_Click(sender As System.Object, e As System.EventArgs) Handles Button6.Click
        If seleccion.Items.Count > 0 Then
            'FrmSelCompania.Show()
            'Me.Close()
            'If varfrmselcompania = "cortefac" Then
            '    FrmOpRep.Show()
            'ElseIf varfrmselcompania = "relingreso" Then
            '    If GloBnd_Des_Men = True Then
            '        FrmFechaIngresosConcepto.Show()
            '        Me.Close()
            '    ElseIf LocbndPolizaCiudad = True Then
            '        FrmFechaIngresosConcepto.Show()
            '        Me.Close()
            '    Else
            '        GloBnd_Des_Men = False
            '        GloBnd_Des_Cont = False
            '        LocbndPolizaCiudad = False
            '        FrmFechaIngresosConcepto.Show()

            '    End If
            'ElseIf varfrmselcompania = "" And varfrmselsucursal = "relingreso" Then
            '    FrmSelSucursal.Show()
            '    Me.Close()
            'End If
            FrmSelLocalidad.Show()
            Me.Close()
        End If
    End Sub
    Private Sub llevamealotro()
        'FrmSelCompania.Show()
        'Me.Close()
        'If varfrmselcompania = "cortefac" Then
        '    FrmOpRep.Show()
        'ElseIf varfrmselcompania = "relingreso" Then
        '    If GloBnd_Des_Men = True Then
        '        FrmFechaIngresosConcepto.Show()
        '        Me.Close()
        '    ElseIf LocbndPolizaCiudad = True Then
        '        FrmFechaIngresosConcepto.Show()
        '        Me.Close()
        '    Else
        '        GloBnd_Des_Men = False
        '        GloBnd_Des_Cont = False
        '        LocbndPolizaCiudad = False
        '        FrmFechaIngresosConcepto.Show()

        '    End If
        'ElseIf varfrmselcompania = "" And varfrmselsucursal = "relingreso" Then
        '    FrmSelSucursal.Show()
        '    Me.Close()
        'End If
        FrmSelLocalidad.Show()
        Me.Close()
    End Sub
End Class