Imports System.Data.SqlClient
Imports System.Text
Public Class FrmCobroErroneo

    Dim BndContratoIzq As Boolean = False
    Dim BndContratoDer As Boolean = False
    Dim BndFactura As Boolean = False
    Dim Contrato As Long = 0
    Dim TipoNota As Integer = 0

    Private Sub Muestra_Tipo_Nota()
        Dim conexion As New SqlConnection(MiConexion)
        Dim strSQL As New StringBuilder("EXEC Muestra_Tipo_Nota 1")

        Dim dataAdapter As New SqlDataAdapter(strSQL.ToString(), conexion)
        Dim dataTable As New DataTable
        Dim bindingSource As New BindingSource

        Try
            dataAdapter.Fill(dataTable)
            bindingSource.DataSource = dataTable
            ComboBoxTipoNota.DataSource = bindingSource
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        End Try

    End Sub

    Function dameSerDELCobroEq(ByVal Contrato As Long, ByVal Clv_Factura As Long) As DataTable
        Dim conexion As New SqlConnection(MiConexion)
        Dim strSQL As New StringBuilder("EXEC dameSerDELCobroEq " & CStr(Contrato) & ", " & CStr(Clv_Factura))

        Dim dataAdapter As New SqlDataAdapter(strSQL.ToString(), conexion)
        Dim dataTable As New DataTable()

        dataAdapter.Fill(dataTable)

        Return dataTable

    End Function

    Function dameSerDELCli(ByVal Contrato As Long) As DataTable
        Dim conexion As New SqlConnection(MiConexion)
        Dim strSQL As New StringBuilder("EXEC dameSerDELCli " & CStr(Contrato))

        Dim dataAdapter As New SqlDataAdapter(strSQL.ToString(), conexion)
        Dim dataTable As New DataTable()

        dataAdapter.Fill(dataTable)

        Return dataTable

    End Function

    Function DameOrdenesCobroEq(ByVal Contrato As Long, ByVal Clv_Factura As Long) As DataTable
        Dim conexion As New SqlConnection(MiConexion)
        Dim strSQL As New StringBuilder("EXEC DameOrdenesCobroEq " & CStr(Contrato) & ", " & CStr(Clv_Factura))

        Dim dataAdapter As New SqlDataAdapter(strSQL.ToString(), conexion)
        Dim dataTable As New DataTable()

        dataAdapter.Fill(dataTable)

        Return dataTable
    End Function

    Private Sub LlenaTreeViewActual(ByVal Contrato As Long)
        Try

            Dim I As Integer = 0
            Dim X As Integer = 0
            Dim Y As Integer = 0
            Dim epasa As Boolean = True
            Dim pasa As Boolean = False
            Dim Net As Boolean = False
            Dim dig As Boolean = False
            Dim PasaJNet As Boolean = False
            Dim jNet As Integer = -1
            Dim jDig As Integer = -1
            Dim FilaRow As DataRow

            Me.TreeViewIzq.Nodes.Clear()
            For Each FilaRow In dameSerDELCli(Contrato).Rows


                X = 0

                If Mid(FilaRow("Servicio").ToString(), 1, 3) = "---" Then
                    Me.TreeViewIzq.Nodes.Add(FilaRow("Servicio").ToString()).ForeColor = System.Drawing.Color.FromArgb(ColorBwr)
                    pasa = True
                    Net = False
                    dig = False
                    jNet = -1
                    jDig = -1
                ElseIf Mid(FilaRow("Servicio").ToString(), 1, 15) = "Servicio Basico" Then
                    Me.TreeViewIzq.Nodes.Add(FilaRow("Servicio").ToString()).ForeColor = System.Drawing.Color.FromArgb(ColorBwr)
                    pasa = True
                    jNet = -1
                    jDig = -1
                ElseIf Mid(FilaRow("Servicio").ToString(), 1, 31) = "Servicios de Televisi�n Digital" Then
                    Me.TreeViewIzq.Nodes.Add(FilaRow("Servicio").ToString()).ForeColor = System.Drawing.Color.FromArgb(ColorBwr)
                    pasa = True
                    jNet = -1
                    jDig = -1
                ElseIf Mid(FilaRow("Servicio").ToString(), 1, 21) = "Servicios de Internet" Then
                    Me.TreeViewIzq.Nodes.Add(FilaRow("Servicio").ToString()).ForeColor = System.Drawing.Color.FromArgb(ColorBwr)
                    jNet = -1
                    jDig = -1

                    pasa = True
                ElseIf Mid(FilaRow("Servicio").ToString(), 1, 22) = "Servicios de Tel�fonia" Then
                    Me.TreeViewIzq.Nodes.Add(FilaRow("Servicio").ToString()).ForeColor = System.Drawing.Color.FromArgb(ColorBwr)
                    jNet = -1
                    jDig = -1
                    pasa = True
                Else
                    If Mid(FilaRow("Servicio").ToString(), 1, 14) = "Mac Cablemodem" Then
                        Me.TreeViewIzq.Nodes(I - 1).Nodes.Add(FilaRow("Servicio").ToString()).ForeColor = Color.Black
                        jNet = jNet + 1
                        pasa = False
                        Net = True
                    ElseIf Mid(FilaRow("Servicio").ToString(), 1, 15) = "Aparato Digital" Then
                        Me.TreeViewIzq.Nodes(I - 1).Nodes.Add(FilaRow("Servicio").ToString()).ForeColor = Color.Black
                        jDig = jDig + 1
                        pasa = False
                        dig = True
                    Else
                        If Net = True Then
                            Me.TreeViewIzq.Nodes(I - 1).Nodes(jNet).Nodes.Add(FilaRow("Servicio").ToString()).ForeColor = Color.Black
                        ElseIf dig = True Then
                            Me.TreeViewIzq.Nodes(I - 1).Nodes(jDig).Nodes.Add(FilaRow("Servicio").ToString()).ForeColor = Color.Black
                        Else
                            If epasa = True Then
                                Me.TreeViewIzq.Nodes(I - 1).Nodes.Add(FilaRow("Servicio").ToString()).ForeColor = Color.Black
                                pasa = False
                                epasa = False
                            Else
                                Me.TreeViewIzq.Nodes(I - 1).Nodes.Add(FilaRow("Servicio").ToString()).ForeColor = Color.Black
                                epasa = False
                                pasa = False
                            End If

                        End If
                    End If
                End If
                If pasa = True Then
                    I = I + 1
                    pasa = False
                End If

            Next

            For Y = 0 To (I - 1)
                Me.TreeViewIzq.Nodes(Y).ExpandAll()
            Next

        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub LlenaTreeViewCobroEq(ByVal Contrato As Long, ByVal Clv_Factura As Long)
        Try

            Dim I As Integer = 0
            Dim X As Integer = 0
            Dim Y As Integer = 0
            Dim epasa As Boolean = True
            Dim pasa As Boolean = False
            Dim Net As Boolean = False
            Dim dig As Boolean = False
            Dim PasaJNet As Boolean = False
            Dim jNet As Integer = -1
            Dim jDig As Integer = -1
            Dim FilaRow As DataRow

            Me.TreeViewDer.Nodes.Clear()
            For Each FilaRow In dameSerDELCobroEq(Contrato, Clv_Factura).Rows


                X = 0

                If Mid(FilaRow("Servicio").ToString(), 1, 3) = "---" Then
                    Me.TreeViewDer.Nodes.Add(FilaRow("Servicio").ToString()).ForeColor = System.Drawing.Color.FromArgb(ColorBwr)
                    pasa = True
                    Net = False
                    dig = False
                    jNet = -1
                    jDig = -1
                ElseIf Mid(FilaRow("Servicio").ToString(), 1, 15) = "Servicio Basico" Then
                    Me.TreeViewDer.Nodes.Add(FilaRow("Servicio").ToString()).ForeColor = System.Drawing.Color.FromArgb(ColorBwr)
                    pasa = True
                    jNet = -1
                    jDig = -1
                ElseIf Mid(FilaRow("Servicio").ToString(), 1, 31) = "Servicios de Televisi�n Digital" Then
                    Me.TreeViewDer.Nodes.Add(FilaRow("Servicio").ToString()).ForeColor = System.Drawing.Color.FromArgb(ColorBwr)
                    pasa = True
                    jNet = -1
                    jDig = -1
                ElseIf Mid(FilaRow("Servicio").ToString(), 1, 21) = "Servicios de Internet" Then
                    Me.TreeViewDer.Nodes.Add(FilaRow("Servicio").ToString()).ForeColor = System.Drawing.Color.FromArgb(ColorBwr)
                    jNet = -1
                    jDig = -1

                    pasa = True
                ElseIf Mid(FilaRow("Servicio").ToString(), 1, 22) = "Servicios de Tel�fonia" Then
                    Me.TreeViewDer.Nodes.Add(FilaRow("Servicio").ToString()).ForeColor = System.Drawing.Color.FromArgb(ColorBwr)
                    jNet = -1
                    jDig = -1
                    pasa = True
                Else
                    If Mid(FilaRow("Servicio").ToString(), 1, 14) = "Mac Cablemodem" Then
                        Me.TreeViewDer.Nodes(I - 1).Nodes.Add(FilaRow("Servicio").ToString()).ForeColor = Color.Black
                        jNet = jNet + 1
                        pasa = False
                        Net = True
                    ElseIf Mid(FilaRow("Servicio").ToString(), 1, 15) = "Aparato Digital" Then
                        Me.TreeViewDer.Nodes(I - 1).Nodes.Add(FilaRow("Servicio").ToString()).ForeColor = Color.Black
                        jDig = jDig + 1
                        pasa = False
                        dig = True
                    Else
                        If Net = True Then
                            Me.TreeViewDer.Nodes(I - 1).Nodes(jNet).Nodes.Add(FilaRow("Servicio").ToString()).ForeColor = Color.Black
                        ElseIf dig = True Then
                            Me.TreeViewDer.Nodes(I - 1).Nodes(jDig).Nodes.Add(FilaRow("Servicio").ToString()).ForeColor = Color.Black
                        Else
                            If epasa = True Then
                                Me.TreeViewDer.Nodes(I - 1).Nodes.Add(FilaRow("Servicio").ToString()).ForeColor = Color.Black
                                pasa = False
                                epasa = False
                            Else
                                Me.TreeViewDer.Nodes(I - 1).Nodes.Add(FilaRow("Servicio").ToString()).ForeColor = Color.Black
                                epasa = False
                                pasa = False
                            End If

                        End If
                    End If
                End If
                If pasa = True Then
                    I = I + 1
                    pasa = False
                End If

            Next

            For Y = 0 To (I - 1)
                Me.TreeViewDer.Nodes(Y).ExpandAll()
            Next

        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub LlenaTreeViewOrden(ByVal Contrato As Long, ByVal Clv_Factura As Long)
        TreeViewOrden.Nodes.Clear()

        For Each fila As DataRow In DameOrdenesCobroEq(eContrato, eClv_Factura).Rows
            TreeViewOrden.Nodes.Add(fila(0).ToString())
        Next
    End Sub

    Private Sub ProcesoCobroErroneo(ByVal ContratoErroneo As Long, ByVal Contrato As Long, ByVal Clv_Factura As Long, ByVal Clv_Usuario As String, ByVal Clv_Caja As Integer, ByVal TipoNota As Integer)
        Dim conexion As New SqlConnection(MiConexion)
        Dim comando As New SqlCommand("ProcesoCobroErroneo", conexion)
        comando.CommandType = CommandType.StoredProcedure
        comando.CommandTimeout = 0

        Dim parametro As New SqlParameter("@ContratoErroneo", SqlDbType.BigInt)
        parametro.Direction = ParameterDirection.Input
        parametro.Value = ContratoErroneo
        comando.Parameters.Add(parametro)

        Dim parametro2 As New SqlParameter("@Contrato", SqlDbType.BigInt)
        parametro2.Direction = ParameterDirection.Input
        parametro2.Value = Contrato
        comando.Parameters.Add(parametro2)

        Dim parametro3 As New SqlParameter("@Clv_Factura", SqlDbType.BigInt)
        parametro3.Direction = ParameterDirection.Input
        parametro3.Value = Clv_Factura
        comando.Parameters.Add(parametro3)

        Dim parametro4 As New SqlParameter("@Clv_Usuario", SqlDbType.VarChar, 5)
        parametro4.Direction = ParameterDirection.Input
        parametro4.Value = Clv_Usuario
        comando.Parameters.Add(parametro4)

        Dim parametro5 As New SqlParameter("@Clv_Caja", SqlDbType.Int)
        parametro5.Direction = ParameterDirection.Input
        parametro5.Value = Clv_Caja
        comando.Parameters.Add(parametro5)

        Dim parametro6 As New SqlParameter("@TipoNota", SqlDbType.Int)
        parametro6.Direction = ParameterDirection.Input
        parametro6.Value = TipoNota
        comando.Parameters.Add(parametro6)

        Dim parametro7 As New SqlParameter("@Clv_Nota", SqlDbType.BigInt)
        parametro7.Direction = ParameterDirection.Output
        comando.Parameters.Add(parametro7)

        Try
            conexion.Open()
            comando.ExecuteNonQuery()
            gloClvNota = CLng(parametro7.Value.ToString())

            eNota = True

        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        Finally
            conexion.Close()
            conexion.Dispose()
        End Try

    End Sub

    Private Sub ButtonBusContrato_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        BndContratoIzq = True
        FrmSelCliente.Show()
    End Sub

    Private Sub ButtonBusContrato2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        BndContratoDer = True
        FrmSelCliente.Show()
    End Sub

    Private Sub FrmCobroEq_Activated(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Activated
        If Glocontratosel > 0 Then
            TextBoxContrato.Text = Glocontratosel
            Glocontratosel = 0
        End If
    End Sub

    Private Sub FrmCobroEq_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Muestra_Tipo_Nota()
        LlenaTreeViewActual(eContrato)
        LlenaTreeViewCobroEq(eContrato, eClv_Factura)
        LlenaTreeViewOrden(eContrato, eClv_Factura)
        LabelContrato.Text = eContrato
        LabelFactura.Text = eFactura
        UspGuardaFormularios(Me.Name, Me.Text)
        UspGuardaBotonesFormularioSiste(Me, Me.Name)
        UspDesactivaBotones(Me, Me.Name)
    End Sub

    Private Sub ButtonBusContrato_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonBusContrato.Click
        Glocontratosel = 0
        FrmSelCliente.Show()
    End Sub

    Private Sub TSBProcesar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TSBProcesar.Click
        If TextBoxContrato.Text.Length = 0 Then
            MsgBox("Teclea un contrato.", MsgBoxStyle.Information)
            Exit Sub
        End If

        If ComboBoxTipoNota.Text.Length = 0 Then
            MsgBox("Selecciona un Tipo de Nota.", MsgBoxStyle.Information)
            Exit Sub
        End If

        Contrato = TextBoxContrato.Text
        TipoNota = ComboBoxTipoNota.SelectedValue

        BackgroundWorker1.RunWorkerAsync()
        PantallaProcesando.Show()
        Me.Close()

    End Sub

    Private Sub BackgroundWorker1_DoWork(ByVal sender As Object, ByVal e As System.ComponentModel.DoWorkEventArgs) Handles BackgroundWorker1.DoWork
        Try
            ProcesoCobroErroneo(eContrato, Contrato, eClv_Factura, GloUsuario, GloCaja, TipoNota)
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        End Try
    End Sub

    Private Sub BackgroundWorker1_RunWorkerCompleted(ByVal sender As Object, ByVal e As System.ComponentModel.RunWorkerCompletedEventArgs) Handles BackgroundWorker1.RunWorkerCompleted
        PantallaProcesando.Close()
    End Sub

    Private Sub ButtonSalir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonSalir.Click
        Me.Close()
    End Sub

End Class