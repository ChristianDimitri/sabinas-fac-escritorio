﻿Imports System.Data.SqlClient
Public Class FrmSelPlazaXML


    Private Sub FrmSelPlaza_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        CargarElementosPlazaXML()
        colorea(Me)
        llenalistboxs()
        If loquehay.Items.Count = 1 Then
            llevamealotro()
            Me.Close()
        End If
    End Sub

    Private Sub agregar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles agregar.Click
        If loquehay.Items.Count = 0 Then
            Exit Sub
        End If
        Dim elementos = DocPlazas.SelectNodes("//PLAZA")
        For Each elem As Xml.XmlElement In elementos
            If elem.GetAttribute("Clv_Plaza") = loquehay.SelectedValue Then
                elem.SetAttribute("Seleccion", 1)
            End If        
        Next
        llenalistboxs()
    End Sub

    Private Sub quitar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles quitar.Click
        If seleccion.Items.Count = 0 Then
            Exit Sub
        End If
        Dim elementos = DocPlazas.SelectNodes("//PLAZA")
        For Each elem As Xml.XmlElement In elementos
            If elem.GetAttribute("Clv_Plaza") = seleccion.SelectedValue Then
                elem.SetAttribute("Seleccion", 0)
            End If
        Next
        llenalistboxs()

    End Sub

    Private Sub agregartodo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles agregartodo.Click
        If loquehay.Items.Count = 0 Then
            Exit Sub
        End If
        Dim elementos = DocPlazas.SelectNodes("//PLAZA")
        For Each elem As Xml.XmlElement In elementos            
                elem.SetAttribute("Seleccion", 1)
        Next
        llenalistboxs()

    End Sub

    Private Sub quitartodo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles quitartodo.Click
        If seleccion.Items.Count = 0 Then
            Exit Sub
        End If
        Dim elementos = DocPlazas.SelectNodes("//PLAZA")
        For Each elem As Xml.XmlElement In elementos

            elem.SetAttribute("Seleccion", 0)

        Next
        llenalistboxs()

    End Sub

    Private Sub Button5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button5.Click
        EntradasSelCiudad = 0
        Me.Close()
    End Sub

    Private Sub Button6_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button6.Click
        If seleccion.Items.Count = 0 Then
            Exit Sub
        End If
        PasarTodoXML = CheckBox1.Checked
        If GloOpFiltrosXML = "listadoNotaCreditoHastaDistribuidor" Then
            FrmSelOpNotas.Show()
        End If
        If GloOpFiltrosXML = "Cortes" Then
            FrmSelCompaniaXML.Show()
        End If
        If GloOpFiltrosXML = "CortesPlaza" Then
            FrmOpRep.Show()
        End If
        If GloOpFiltrosXML = "RelacionConceptoCiudad" Then
            FrmSelCompaniaXML.Show()
        End If
        If GloOpFiltrosXML = "IngresoDistribuidor" Then
            FrmFechaIngresosConcepto.Show()
        End If
        If GloOpFiltrosXML = "Bonificaciones" Then
            FrmSelCompaniaXML.Show()
        End If
        If GloOpFiltrosXML = "CortePrimeraVersion" Then
            FrmSelCompaniaXML.Show()
        End If
        If GloOpFiltrosXML = "CorteNuevo" Then
            FrmSelCompaniaXML.Show()
        End If
        If GloOpFiltrosXML = "RelacionConceptoSucursal" Then
            FrmSelCompaniaXML.Show()
        End If
        If GloOpFiltrosXML = "IngresoNormal" Then
            FrmSelCompaniaXML.Show()
        End If
        Me.Close()
    End Sub
    Private Sub llevamealotro()
        Dim elementos = DocPlazas.SelectNodes("//PLAZA")
        For Each elem As Xml.XmlElement In elementos
            elem.SetAttribute("Seleccion", 1)
        Next
        If GloOpFiltrosXML = "listadoNotaCreditoHastaDistribuidor" Then
            FrmSelOpNotas.Show()
        End If
        If GloOpFiltrosXML = "Cortes" Then
            FrmSelCompaniaXML.Show()
        End If
        If GloOpFiltrosXML = "CortesPlaza" Then
            FrmOpRep.Show()
        End If
        If GloOpFiltrosXML = "RelacionConceptoCiudad" Then
            FrmSelCompaniaXML.Show()
        End If
        If GloOpFiltrosXML = "IngresoDistribuidor" Then
            FrmFechaIngresosConcepto.Show()
        End If
        If GloOpFiltrosXML = "Bonificaciones" Then
            FrmSelCompaniaXML.Show()
        End If
        If GloOpFiltrosXML = "CortePrimeraVersion" Then
            FrmSelCompaniaXML.Show()
        End If
        If GloOpFiltrosXML = "CorteNuevo" Then
            FrmSelCompaniaXML.Show()
        End If
        If GloOpFiltrosXML = "RelacionConceptoSucursal" Then
            FrmSelCompaniaXML.Show()
        End If
        If GloOpFiltrosXML = "IngresoNormal" Then
            FrmSelCompaniaXML.Show()
        End If
    End Sub

    Private Sub llenalistboxs()
       Dim dt As New DataTable
        dt.Columns.Add("Clv_Plaza", Type.GetType("System.String"))
        dt.Columns.Add("nombre", Type.GetType("System.String"))

        Dim dt2 As New DataTable
        dt2.Columns.Add("Clv_Plaza", Type.GetType("System.String"))
        dt2.Columns.Add("nombre", Type.GetType("System.String"))

        Dim elementos = DocPlazas.SelectNodes("//PLAZA")
        For Each elem As Xml.XmlElement In elementos
            If elem.GetAttribute("Seleccion") = 0 Then
                dt.Rows.Add(elem.GetAttribute("Clv_Plaza"), elem.GetAttribute("nombre"))
            End If
            If elem.GetAttribute("Seleccion") = 1 Then
                dt2.Rows.Add(elem.GetAttribute("Clv_Plaza"), elem.GetAttribute("nombre"))
            End If
        Next
        loquehay.DataSource = New BindingSource(dt, Nothing)
        seleccion.DataSource = New BindingSource(dt2, Nothing)
    End Sub

    Private Sub CheckBox1_CheckedChanged(sender As Object, e As EventArgs) Handles CheckBox1.CheckedChanged
        PasarTodoXML = CheckBox1.Checked
    End Sub
End Class