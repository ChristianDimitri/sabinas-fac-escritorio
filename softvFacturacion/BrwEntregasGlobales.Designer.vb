﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class BrwEntregasGlobales
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.btnCancelar = New System.Windows.Forms.Button()
        Me.btnBuscaDescripcion = New System.Windows.Forms.Button()
        Me.txtBuscaDescripcion = New System.Windows.Forms.TextBox()
        Me.CMBlblBuscaDescripcion = New System.Windows.Forms.Label()
        Me.cmbBuscaCajera = New System.Windows.Forms.ComboBox()
        Me.CMBlblBuscaCajera = New System.Windows.Forms.Label()
        Me.dgvEntregas = New System.Windows.Forms.DataGridView()
        Me.ClaveGasto = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.fecha = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DescripcionGasto = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.cajera = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.importe = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.estatus = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.pnlDatosGenerales = New System.Windows.Forms.Panel()
        Me.CMBlblStatus = New System.Windows.Forms.Label()
        Me.CMBlblMuestraStatus = New System.Windows.Forms.Label()
        Me.CMBlblImporte = New System.Windows.Forms.Label()
        Me.CMBlblMuestraImporte = New System.Windows.Forms.Label()
        Me.CMBlblCajera = New System.Windows.Forms.Label()
        Me.CMBlblMuestraCajera = New System.Windows.Forms.Label()
        Me.CMBlblFecha = New System.Windows.Forms.Label()
        Me.CMBlblMuestraFecha = New System.Windows.Forms.Label()
        Me.btnSalir = New System.Windows.Forms.Button()
        Me.btnModificar = New System.Windows.Forms.Button()
        Me.btnConsultar = New System.Windows.Forms.Button()
        Me.btnAceptar = New System.Windows.Forms.Button()
        Me.CMBlblBusqueda = New System.Windows.Forms.Label()
        Me.CMBlblEstatus = New System.Windows.Forms.Label()
        Me.cmbBuscaEstatus = New System.Windows.Forms.ComboBox()
        CType(Me.dgvEntregas, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pnlDatosGenerales.SuspendLayout()
        Me.SuspendLayout()
        '
        'btnCancelar
        '
        Me.btnCancelar.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnCancelar.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnCancelar.Location = New System.Drawing.Point(1181, 165)
        Me.btnCancelar.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.btnCancelar.Name = "btnCancelar"
        Me.btnCancelar.Size = New System.Drawing.Size(163, 42)
        Me.btnCancelar.TabIndex = 8
        Me.btnCancelar.Text = "&Cancelar"
        Me.btnCancelar.UseVisualStyleBackColor = True
        '
        'btnBuscaDescripcion
        '
        Me.btnBuscaDescripcion.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnBuscaDescripcion.Location = New System.Drawing.Point(165, 219)
        Me.btnBuscaDescripcion.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.btnBuscaDescripcion.Name = "btnBuscaDescripcion"
        Me.btnBuscaDescripcion.Size = New System.Drawing.Size(109, 31)
        Me.btnBuscaDescripcion.TabIndex = 2
        Me.btnBuscaDescripcion.Text = "&Buscar"
        Me.btnBuscaDescripcion.UseVisualStyleBackColor = True
        '
        'txtBuscaDescripcion
        '
        Me.txtBuscaDescripcion.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtBuscaDescripcion.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtBuscaDescripcion.Location = New System.Drawing.Point(15, 185)
        Me.txtBuscaDescripcion.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.txtBuscaDescripcion.Name = "txtBuscaDescripcion"
        Me.txtBuscaDescripcion.Size = New System.Drawing.Size(259, 26)
        Me.txtBuscaDescripcion.TabIndex = 1
        '
        'CMBlblBuscaDescripcion
        '
        Me.CMBlblBuscaDescripcion.AutoSize = True
        Me.CMBlblBuscaDescripcion.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBlblBuscaDescripcion.Location = New System.Drawing.Point(11, 165)
        Me.CMBlblBuscaDescripcion.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.CMBlblBuscaDescripcion.Name = "CMBlblBuscaDescripcion"
        Me.CMBlblBuscaDescripcion.Size = New System.Drawing.Size(122, 20)
        Me.CMBlblBuscaDescripcion.TabIndex = 46
        Me.CMBlblBuscaDescripcion.Text = "Descripción :"
        '
        'cmbBuscaCajera
        '
        Me.cmbBuscaCajera.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmbBuscaCajera.FormattingEnabled = True
        Me.cmbBuscaCajera.Location = New System.Drawing.Point(16, 298)
        Me.cmbBuscaCajera.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.cmbBuscaCajera.Name = "cmbBuscaCajera"
        Me.cmbBuscaCajera.Size = New System.Drawing.Size(263, 28)
        Me.cmbBuscaCajera.TabIndex = 3
        '
        'CMBlblBuscaCajera
        '
        Me.CMBlblBuscaCajera.AutoSize = True
        Me.CMBlblBuscaCajera.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBlblBuscaCajera.Location = New System.Drawing.Point(13, 274)
        Me.CMBlblBuscaCajera.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.CMBlblBuscaCajera.Name = "CMBlblBuscaCajera"
        Me.CMBlblBuscaCajera.Size = New System.Drawing.Size(76, 20)
        Me.CMBlblBuscaCajera.TabIndex = 44
        Me.CMBlblBuscaCajera.Text = "Cajera :"
        '
        'dgvEntregas
        '
        Me.dgvEntregas.AllowUserToAddRows = False
        Me.dgvEntregas.AllowUserToDeleteRows = False
        Me.dgvEntregas.BackgroundColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvEntregas.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle1
        Me.dgvEntregas.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvEntregas.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.ClaveGasto, Me.fecha, Me.DescripcionGasto, Me.cajera, Me.importe, Me.estatus})
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgvEntregas.DefaultCellStyle = DataGridViewCellStyle2
        Me.dgvEntregas.Location = New System.Drawing.Point(285, 14)
        Me.dgvEntregas.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.dgvEntregas.Name = "dgvEntregas"
        Me.dgvEntregas.ReadOnly = True
        DataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle3.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvEntregas.RowHeadersDefaultCellStyle = DataGridViewCellStyle3
        Me.dgvEntregas.RowHeadersVisible = False
        Me.dgvEntregas.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvEntregas.Size = New System.Drawing.Size(888, 878)
        Me.dgvEntregas.TabIndex = 4
        '
        'ClaveGasto
        '
        Me.ClaveGasto.DataPropertyName = "idEntrega"
        Me.ClaveGasto.HeaderText = "Id Entrega"
        Me.ClaveGasto.Name = "ClaveGasto"
        Me.ClaveGasto.ReadOnly = True
        Me.ClaveGasto.Width = 50
        '
        'fecha
        '
        Me.fecha.DataPropertyName = "fecha"
        Me.fecha.HeaderText = "Fecha"
        Me.fecha.Name = "fecha"
        Me.fecha.ReadOnly = True
        Me.fecha.Width = 70
        '
        'DescripcionGasto
        '
        Me.DescripcionGasto.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.DescripcionGasto.DataPropertyName = "descripcion"
        Me.DescripcionGasto.HeaderText = "Descripción"
        Me.DescripcionGasto.Name = "DescripcionGasto"
        Me.DescripcionGasto.ReadOnly = True
        '
        'cajera
        '
        Me.cajera.DataPropertyName = "clvUsuario"
        Me.cajera.HeaderText = "Cajera"
        Me.cajera.Name = "cajera"
        Me.cajera.ReadOnly = True
        '
        'importe
        '
        Me.importe.DataPropertyName = "importe"
        Me.importe.HeaderText = "Importe"
        Me.importe.Name = "importe"
        Me.importe.ReadOnly = True
        '
        'estatus
        '
        Me.estatus.DataPropertyName = "status"
        Me.estatus.HeaderText = "Estatus"
        Me.estatus.Name = "estatus"
        Me.estatus.ReadOnly = True
        Me.estatus.Width = 50
        '
        'pnlDatosGenerales
        '
        Me.pnlDatosGenerales.Controls.Add(Me.CMBlblStatus)
        Me.pnlDatosGenerales.Controls.Add(Me.CMBlblMuestraStatus)
        Me.pnlDatosGenerales.Controls.Add(Me.CMBlblImporte)
        Me.pnlDatosGenerales.Controls.Add(Me.CMBlblMuestraImporte)
        Me.pnlDatosGenerales.Controls.Add(Me.CMBlblCajera)
        Me.pnlDatosGenerales.Controls.Add(Me.CMBlblMuestraCajera)
        Me.pnlDatosGenerales.Controls.Add(Me.CMBlblFecha)
        Me.pnlDatosGenerales.Controls.Add(Me.CMBlblMuestraFecha)
        Me.pnlDatosGenerales.Location = New System.Drawing.Point(4, 661)
        Me.pnlDatosGenerales.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.pnlDatosGenerales.Name = "pnlDatosGenerales"
        Me.pnlDatosGenerales.Size = New System.Drawing.Size(276, 228)
        Me.pnlDatosGenerales.TabIndex = 41
        '
        'CMBlblStatus
        '
        Me.CMBlblStatus.AutoSize = True
        Me.CMBlblStatus.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBlblStatus.Location = New System.Drawing.Point(92, 193)
        Me.CMBlblStatus.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.CMBlblStatus.Name = "CMBlblStatus"
        Me.CMBlblStatus.Size = New System.Drawing.Size(0, 20)
        Me.CMBlblStatus.TabIndex = 19
        '
        'CMBlblMuestraStatus
        '
        Me.CMBlblMuestraStatus.AutoSize = True
        Me.CMBlblMuestraStatus.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBlblMuestraStatus.Location = New System.Drawing.Point(7, 193)
        Me.CMBlblMuestraStatus.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.CMBlblMuestraStatus.Name = "CMBlblMuestraStatus"
        Me.CMBlblMuestraStatus.Size = New System.Drawing.Size(75, 20)
        Me.CMBlblMuestraStatus.TabIndex = 18
        Me.CMBlblMuestraStatus.Text = "Status :"
        '
        'CMBlblImporte
        '
        Me.CMBlblImporte.AutoSize = True
        Me.CMBlblImporte.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBlblImporte.Location = New System.Drawing.Point(11, 154)
        Me.CMBlblImporte.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.CMBlblImporte.Name = "CMBlblImporte"
        Me.CMBlblImporte.Size = New System.Drawing.Size(0, 20)
        Me.CMBlblImporte.TabIndex = 17
        '
        'CMBlblMuestraImporte
        '
        Me.CMBlblMuestraImporte.AutoSize = True
        Me.CMBlblMuestraImporte.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBlblMuestraImporte.Location = New System.Drawing.Point(5, 128)
        Me.CMBlblMuestraImporte.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.CMBlblMuestraImporte.Name = "CMBlblMuestraImporte"
        Me.CMBlblMuestraImporte.Size = New System.Drawing.Size(84, 20)
        Me.CMBlblMuestraImporte.TabIndex = 16
        Me.CMBlblMuestraImporte.Text = "Importe :"
        '
        'CMBlblCajera
        '
        Me.CMBlblCajera.AutoSize = True
        Me.CMBlblCajera.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBlblCajera.Location = New System.Drawing.Point(9, 96)
        Me.CMBlblCajera.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.CMBlblCajera.Name = "CMBlblCajera"
        Me.CMBlblCajera.Size = New System.Drawing.Size(0, 20)
        Me.CMBlblCajera.TabIndex = 15
        '
        'CMBlblMuestraCajera
        '
        Me.CMBlblMuestraCajera.AutoSize = True
        Me.CMBlblMuestraCajera.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBlblMuestraCajera.Location = New System.Drawing.Point(7, 69)
        Me.CMBlblMuestraCajera.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.CMBlblMuestraCajera.Name = "CMBlblMuestraCajera"
        Me.CMBlblMuestraCajera.Size = New System.Drawing.Size(76, 20)
        Me.CMBlblMuestraCajera.TabIndex = 14
        Me.CMBlblMuestraCajera.Text = "Cajera :"
        '
        'CMBlblFecha
        '
        Me.CMBlblFecha.AutoSize = True
        Me.CMBlblFecha.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBlblFecha.Location = New System.Drawing.Point(11, 39)
        Me.CMBlblFecha.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.CMBlblFecha.Name = "CMBlblFecha"
        Me.CMBlblFecha.Size = New System.Drawing.Size(0, 20)
        Me.CMBlblFecha.TabIndex = 13
        '
        'CMBlblMuestraFecha
        '
        Me.CMBlblMuestraFecha.AutoSize = True
        Me.CMBlblMuestraFecha.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBlblMuestraFecha.Location = New System.Drawing.Point(5, 16)
        Me.CMBlblMuestraFecha.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.CMBlblMuestraFecha.Name = "CMBlblMuestraFecha"
        Me.CMBlblMuestraFecha.Size = New System.Drawing.Size(72, 20)
        Me.CMBlblMuestraFecha.TabIndex = 12
        Me.CMBlblMuestraFecha.Text = "Fecha :"
        '
        'btnSalir
        '
        Me.btnSalir.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSalir.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSalir.Location = New System.Drawing.Point(1181, 849)
        Me.btnSalir.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.btnSalir.Name = "btnSalir"
        Me.btnSalir.Size = New System.Drawing.Size(160, 42)
        Me.btnSalir.TabIndex = 9
        Me.btnSalir.Text = "&Salir"
        Me.btnSalir.UseVisualStyleBackColor = True
        '
        'btnModificar
        '
        Me.btnModificar.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnModificar.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnModificar.Location = New System.Drawing.Point(1181, 116)
        Me.btnModificar.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.btnModificar.Name = "btnModificar"
        Me.btnModificar.Size = New System.Drawing.Size(163, 42)
        Me.btnModificar.TabIndex = 7
        Me.btnModificar.Text = "&Modificar"
        Me.btnModificar.UseVisualStyleBackColor = True
        '
        'btnConsultar
        '
        Me.btnConsultar.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnConsultar.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnConsultar.Location = New System.Drawing.Point(1181, 68)
        Me.btnConsultar.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.btnConsultar.Name = "btnConsultar"
        Me.btnConsultar.Size = New System.Drawing.Size(163, 41)
        Me.btnConsultar.TabIndex = 6
        Me.btnConsultar.Text = "&Consultar"
        Me.btnConsultar.UseVisualStyleBackColor = True
        '
        'btnAceptar
        '
        Me.btnAceptar.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnAceptar.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnAceptar.Location = New System.Drawing.Point(1181, 17)
        Me.btnAceptar.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.btnAceptar.Name = "btnAceptar"
        Me.btnAceptar.Size = New System.Drawing.Size(163, 43)
        Me.btnAceptar.TabIndex = 5
        Me.btnAceptar.Text = "&Nuevo"
        Me.btnAceptar.UseVisualStyleBackColor = True
        '
        'CMBlblBusqueda
        '
        Me.CMBlblBusqueda.AutoSize = True
        Me.CMBlblBusqueda.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBlblBusqueda.Location = New System.Drawing.Point(19, 25)
        Me.CMBlblBusqueda.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.CMBlblBusqueda.Name = "CMBlblBusqueda"
        Me.CMBlblBusqueda.Size = New System.Drawing.Size(155, 29)
        Me.CMBlblBusqueda.TabIndex = 40
        Me.CMBlblBusqueda.Text = "Buscar Por :"
        '
        'CMBlblEstatus
        '
        Me.CMBlblEstatus.AutoSize = True
        Me.CMBlblEstatus.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBlblEstatus.Location = New System.Drawing.Point(12, 89)
        Me.CMBlblEstatus.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.CMBlblEstatus.Name = "CMBlblEstatus"
        Me.CMBlblEstatus.Size = New System.Drawing.Size(85, 20)
        Me.CMBlblEstatus.TabIndex = 45
        Me.CMBlblEstatus.Text = "Estatus :"
        '
        'cmbBuscaEstatus
        '
        Me.cmbBuscaEstatus.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmbBuscaEstatus.FormattingEnabled = True
        Me.cmbBuscaEstatus.Location = New System.Drawing.Point(15, 112)
        Me.cmbBuscaEstatus.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.cmbBuscaEstatus.Name = "cmbBuscaEstatus"
        Me.cmbBuscaEstatus.Size = New System.Drawing.Size(261, 28)
        Me.cmbBuscaEstatus.TabIndex = 0
        '
        'BrwEntregasGlobales
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.AutoScroll = True
        Me.ClientSize = New System.Drawing.Size(1355, 903)
        Me.Controls.Add(Me.btnCancelar)
        Me.Controls.Add(Me.btnBuscaDescripcion)
        Me.Controls.Add(Me.txtBuscaDescripcion)
        Me.Controls.Add(Me.CMBlblBuscaDescripcion)
        Me.Controls.Add(Me.cmbBuscaEstatus)
        Me.Controls.Add(Me.CMBlblEstatus)
        Me.Controls.Add(Me.cmbBuscaCajera)
        Me.Controls.Add(Me.CMBlblBuscaCajera)
        Me.Controls.Add(Me.dgvEntregas)
        Me.Controls.Add(Me.pnlDatosGenerales)
        Me.Controls.Add(Me.btnSalir)
        Me.Controls.Add(Me.btnModificar)
        Me.Controls.Add(Me.btnConsultar)
        Me.Controls.Add(Me.btnAceptar)
        Me.Controls.Add(Me.CMBlblBusqueda)
        Me.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Name = "BrwEntregasGlobales"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Entregas Globales"
        CType(Me.dgvEntregas, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pnlDatosGenerales.ResumeLayout(False)
        Me.pnlDatosGenerales.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents btnCancelar As System.Windows.Forms.Button
    Friend WithEvents btnBuscaDescripcion As System.Windows.Forms.Button
    Friend WithEvents txtBuscaDescripcion As System.Windows.Forms.TextBox
    Friend WithEvents CMBlblBuscaDescripcion As System.Windows.Forms.Label
    Friend WithEvents cmbBuscaCajera As System.Windows.Forms.ComboBox
    Friend WithEvents CMBlblBuscaCajera As System.Windows.Forms.Label
    Friend WithEvents dgvEntregas As System.Windows.Forms.DataGridView
    Friend WithEvents pnlDatosGenerales As System.Windows.Forms.Panel
    Friend WithEvents CMBlblStatus As System.Windows.Forms.Label
    Friend WithEvents CMBlblMuestraStatus As System.Windows.Forms.Label
    Friend WithEvents CMBlblImporte As System.Windows.Forms.Label
    Friend WithEvents CMBlblMuestraImporte As System.Windows.Forms.Label
    Friend WithEvents CMBlblCajera As System.Windows.Forms.Label
    Friend WithEvents CMBlblMuestraCajera As System.Windows.Forms.Label
    Friend WithEvents CMBlblFecha As System.Windows.Forms.Label
    Friend WithEvents CMBlblMuestraFecha As System.Windows.Forms.Label
    Friend WithEvents btnSalir As System.Windows.Forms.Button
    Friend WithEvents btnModificar As System.Windows.Forms.Button
    Friend WithEvents btnConsultar As System.Windows.Forms.Button
    Friend WithEvents btnAceptar As System.Windows.Forms.Button
    Friend WithEvents CMBlblBusqueda As System.Windows.Forms.Label
    Friend WithEvents ClaveGasto As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents fecha As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DescripcionGasto As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents cajera As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents importe As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents estatus As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents CMBlblEstatus As System.Windows.Forms.Label
    Friend WithEvents cmbBuscaEstatus As System.Windows.Forms.ComboBox
End Class
