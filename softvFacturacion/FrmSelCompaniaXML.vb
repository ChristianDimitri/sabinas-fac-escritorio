﻿Imports System.Data.SqlClient
Public Class FrmSelCompaniaXML

    Private Sub FrmSelCompaniaXML_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        colorea(Me)
        CargarElementosCompaniaXML()
        llenalistboxs()
        If loquehay.Items.Count = 1 Or PasarTodoXML Then
            llevamealotro()
            Me.Close()
        End If
    End Sub

    Private Sub llenalistboxs()
        Dim dt As New DataTable
        dt.Columns.Add("IdCompania", Type.GetType("System.String"))
        dt.Columns.Add("Nombre", Type.GetType("System.String"))

        Dim dt2 As New DataTable
        dt2.Columns.Add("IdCompania", Type.GetType("System.String"))
        dt2.Columns.Add("Nombre", Type.GetType("System.String"))

        Dim elementos = DocCompanias.SelectNodes("//COMPANIA")
        For Each elem As Xml.XmlElement In elementos
            If elem.GetAttribute("Seleccion") = 0 Then
                dt.Rows.Add(elem.GetAttribute("IdCompania"), elem.GetAttribute("Nombre"))
            End If
            If elem.GetAttribute("Seleccion") = 1 Then
                dt2.Rows.Add(elem.GetAttribute("IdCompania"), elem.GetAttribute("Nombre"))
            End If
        Next
        loquehay.DataSource = New BindingSource(dt, Nothing)
        seleccion.DataSource = New BindingSource(dt2, Nothing)
    End Sub

    Private Sub agregar_Click(sender As Object, e As EventArgs) Handles agregar.Click
        If loquehay.Items.Count = 0 Then
            Exit Sub
        End If
        Dim elementos = DocCompanias.SelectNodes("//COMPANIA")
        For Each elem As Xml.XmlElement In elementos
            If elem.GetAttribute("IdCompania") = loquehay.SelectedValue Then
                elem.SetAttribute("Seleccion", 1)
            End If
        Next
        llenalistboxs()
    End Sub

    Private Sub llevamealotro()
        Dim elementos = DocCompanias.SelectNodes("//COMPANIA")
        For Each elem As Xml.XmlElement In elementos
            elem.SetAttribute("Seleccion", 1)
        Next
        If GloOpFiltrosXML = "Cortes" Then
            FrmSelEstadoXML.Show()
            'FrmOpRep.Show()
        End If
        If GloOpFiltrosXML = "RelacionConceptoCiudad" Then
            FrmSelEstadoXML.Show()
            'FrmFechaIngresosConcepto.Show()
        End If
        If GloOpFiltrosXML = "Bonificaciones" Then
            FrmFiltroBonificacion.Show()
        End If
        If GloOpFiltrosXML = "CortePrimeraVersion" Then
            FrmSelSucursalXML.Show()
        End If
        If GloOpFiltrosXML = "CorteNuevo" Then
            FrmSelSucursalXML.Show()
        End If
        If GloOpFiltrosXML = "RelacionConceptoSucursal" Then
            FrmSelSucursalXML.Show()
        End If
        If GloOpFiltrosXML = "IngresoNormal" Then
            FrmFechaIngresosConcepto.Show()
        End If
    End Sub
    Private Sub agregartodo_Click(sender As Object, e As EventArgs) Handles agregartodo.Click
        If loquehay.Items.Count = 0 Then
            Exit Sub
        End If
        Dim elementos = DocCompanias.SelectNodes("//COMPANIA")
        For Each elem As Xml.XmlElement In elementos
            elem.SetAttribute("Seleccion", 1)
        Next
        llenalistboxs()
    End Sub

    Private Sub quitar_Click(sender As Object, e As EventArgs) Handles quitar.Click
        If seleccion.Items.Count = 0 Then
            Exit Sub
        End If
        Dim elementos = DocCompanias.SelectNodes("//COMPANIA")
        For Each elem As Xml.XmlElement In elementos
            If elem.GetAttribute("IdCompania") = seleccion.SelectedValue Then
                elem.SetAttribute("Seleccion", 0)
            End If
        Next
        llenalistboxs()
    End Sub

    Private Sub quitartodo_Click(sender As Object, e As EventArgs) Handles quitartodo.Click
        If seleccion.Items.Count = 0 Then
            Exit Sub
        End If
        Dim elementos = DocCompanias.SelectNodes("//COMPANIA")
        For Each elem As Xml.XmlElement In elementos
            elem.SetAttribute("Seleccion", 0)
        Next
        llenalistboxs()
    End Sub

    Private Sub Button6_Click(sender As Object, e As EventArgs) Handles Button6.Click
        If seleccion.Items.Count = 0 Then
            Exit Sub
        End If
        If GloOpFiltrosXML = "Cortes" Then
            FrmSelEstadoXML.Show()
            'FrmOpRep.Show()
        End If
        If GloOpFiltrosXML = "RelacionConceptoCiudad" Then
            FrmSelEstadoXML.Show()
            'FrmFechaIngresosConcepto.Show()
        End If
        If GloOpFiltrosXML = "Bonificaciones" Then
            FrmFiltroBonificacion.Show()
        End If
        If GloOpFiltrosXML = "CortePrimeraVersion" Then
            FrmSelSucursalXML.Show()
        End If
        If GloOpFiltrosXML = "CorteNuevo" Then
            FrmSelSucursalXML.Show()
        End If
        If GloOpFiltrosXML = "RelacionConceptoSucursal" Then
            FrmSelSucursalXML.Show()
        End If
        If GloOpFiltrosXML = "IngresoNormal" Then
            FrmFechaIngresosConcepto.Show()
        End If
        Me.Close()
    End Sub

    Private Sub Button5_Click(sender As Object, e As EventArgs) Handles Button5.Click
        Me.Close()
    End Sub
End Class