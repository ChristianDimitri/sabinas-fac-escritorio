Imports System.Data.SqlClient
Public Class FrmSelFechasReporteCargos

    Private Sub FrmSelFechasReporteCargos_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        colorea(Me)
        Dim con As New SqlClient.SqlConnection(MiConexion)
        Me.Text = "Selecci�n Fechas Reporte Facturas Con Cargo Automatico"
        Me.CMBLabel6.Text = "Seleccione Cajero(a)"
        con.Open()
        Me.MUESTRAUSUARIOS1TableAdapter.Connection = con
        Me.MUESTRAUSUARIOS1TableAdapter.Fill(Me.NewsoftvDataSet.MUESTRAUSUARIOS1, 0, GloClvUsuario)
        con.Close()
    End Sub

    Private Sub DateTimePicker1_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles DateTimePicker1.ValueChanged
        Me.DateTimePicker2.MinDate = Me.DateTimePicker1.Value
    End Sub


    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        bndcancelareportcargos = True
        Me.Close()
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        If Me.CheckBox1.CheckState = CheckState.Checked Then
            losresumencargos = True
        Else
            losresumencargos = False
        End If
        Fecha_IniCargo = Me.DateTimePicker1.Text
        Fecha_FinCargo = Me.DateTimePicker2.Text
        Cajero_Cargo = CStr(Me.ComboBox1.SelectedValue)

        bndreportcargos = True
        FrmImprimirRepGral.Show()
        Me.Close()
    End Sub

End Class