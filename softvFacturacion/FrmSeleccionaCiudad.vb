''Imports System
Imports System.IO
Imports System.Text
Imports Microsoft.VisualBasic

Public Class FrmSeleccionaCiudad


    Private Sub llena_combo()
        Dim a As Integer = 0
        Dim b As Integer = 0
        Dim c As Integer = 0
        Dim d As Integer = 0
        Dim retString As String = Nothing
        Dim ciudad As String = Nothing
        Dim IP As String = Nothing
        Dim BD As String = Nothing
        Dim Login As String = Nothing
        Dim Pass As String = Nothing

        'Dim path As String = "C:\SETUP_001.TXT"
        Dim path As String = "C:\Windows\SETUP_001.TXT"

        'eRutaCFD = "\\Tequis\Exes\NewSoftvFacturacion\FacturaNetCFD\"
        'rutaBanner = "\\Tequis\Exes\Softv\Banner"
        'Dim path As String = "\\192.168.1.10\EXES\SOFTV\SETUP_001.TXT"
        'Dim path As String = "\\Ixtlahuaca\EXES\SOFTV\SETUP_001.TXT"
        'Dim path As String = "\\192.168.50.41\Exes\Softv\SETUP_001.TXT"
        'Dim path As String = "\\192.168.10.109\EXES\SOFTV\SETUP_001.TXT"
        'Dim path As String = "\\Softv\EXES\SOFTV\SETUP_001.TXT"

        'Dim path As String = "\\172.16.1.120\EXES\SOFTV\SETUP_001.TXT" 'sabinas
        'Dim path As String = "\\172.16.1.67\EXES\SOFTV\SETUP_001.TXT" 'CuatroCienegas

        'Dim path As String = "\\172.16.126.41\EXES\SOFTV\SETUP_001.TXT" 'la buena
        ' Dim path As String = "\\172.16.126.44\EXES\SOFTV\SETUP_001.TXT" 'la buena pruebas
        'Dim path As String = "\\192.168.210.50\Exes\Softv\SETUP_001.TXT" 'vission

        'Dim path As String = "\\10.251.50.10\EXES\SOFTV\SETUP_001.TXT" 'nextdata
        'Dim path As String = "\\10.28.19.14\EXES\SOFTV\SETUP_001.TXT" 'Demo
        'eRutaCFD = "\\192.168.1.10\EXES\SOFTV\"
        'rutaBanner = "\\192.168.1.10\EXES\SOFTV\"

        Me.ComboBox1.Items.Clear()
        Me.ComboBox2.Items.Clear()
        Me.ComboBox3.Items.Clear()
        Me.ComboBox4.Items.Clear()
        Me.ComboBox5.Items.Clear()
        ' Open the file to read from.
        Dim sr As StreamReader = File.OpenText(path)
        Do While sr.Peek() >= 0
            retString = DesEncriptaME(sr.ReadLine())
            If Len(retString) > 0 Then
                a = InStr(1, Trim(retString), "|", vbTextCompare)
                If a > 0 Then
                    b = InStr(a + 1, Trim(retString), "|", vbTextCompare)
                    c = InStr(b + 1, Trim(retString), "|", vbTextCompare)
                    d = InStr(c + 1, Trim(retString), "|", vbTextCompare)
                    ciudad = Mid(Trim(retString), 1, a - 1)
                    IP = Mid(Trim(retString), a + 1, (b - a) - 1)
                    BD = Mid(Trim(retString), b + 1, (c - b) - 1)
                    Login = Mid(Trim(retString), c + 1, (d - c) - 1)
                    Pass = Mid(Trim(retString), d + 1, Len(Trim(Trim(retString))))
                    Me.ComboBox1.Items.Add(ciudad)
                    Me.ComboBox2.Items.Add(IP)
                    Me.ComboBox3.Items.Add(BD)
                    Me.ComboBox4.Items.Add(Login)
                    Me.ComboBox5.Items.Add(Pass)

                End If
            End If
        Loop
        sr.Close()
        If Me.ComboBox1.Items.Count = 1 Then
            Me.ComboBox1.SelectedIndex = 0
            Me.ComboBox2.SelectedIndex = 0
            Me.ComboBox3.SelectedIndex = 0
            Me.ComboBox4.SelectedIndex = 0
            Me.ComboBox5.SelectedIndex = 0
        End If
    End Sub


    Private Sub FrmSeleccionaCiudad_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        llena_combo()
    End Sub

    Private Sub ComboBox1_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles ComboBox1.KeyPress
        If Asc(e.KeyChar) = 13 Then
            Entrar()
        End If
    End Sub

    Private Sub ComboBox1_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles ComboBox1.LostFocus
        If Me.ComboBox1.SelectedIndex = -1 Then
            Me.ComboBox1.Text = ""
            Me.ComboBox2.Text = ""
            Me.ComboBox3.Text = ""
            Me.ComboBox4.Text = ""
            Me.ComboBox5.Text = ""
        End If
    End Sub

    Private Sub ComboBox1_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ComboBox1.SelectedIndexChanged
        Me.ComboBox2.SelectedIndex = Me.ComboBox1.SelectedIndex
        Me.ComboBox3.SelectedIndex = Me.ComboBox1.SelectedIndex
        Me.ComboBox4.SelectedIndex = Me.ComboBox1.SelectedIndex
        Me.ComboBox5.SelectedIndex = Me.ComboBox1.SelectedIndex
    End Sub

    Private Sub Entrar()
        If Me.ComboBox1.SelectedIndex <> -1 Then

            GloServerName = Me.ComboBox2.Text
            GloDatabaseName = Me.ComboBox3.Text
            GloUserID = Me.ComboBox4.Text
            GloPassword = Me.ComboBox5.Text



            'GloServerName = "Oswaldo-pc\MSSQLSERVER2014"
            'GloDatabaseName = "Nextdata"
            'GloUserID = "sa"
            'GloPassword = "06011975"


            'GloServerName = "38.65.141.182\MSSQLSERVER2014"
            'GloDatabaseName = "Newsoftv"
            'GloUserID = "sa"
            'GloPassword = "0601x-2L"

            'GloServerName = "Liz-pc\sql2014"
            'GloDatabaseName = "Newsoftv"
            'GloUserID = "sa"
            'GloPassword = "06011975"

            'GloServerName = "richard2-pc"
            'GloDatabaseName = "NewsoftvSabinas28Feb"
            'GloUserID = "sa"
            'GloPassword = "06011975"

            'GloServerName = "OSWALDO-PC\MSSQLSERVER2014"
            'GloDatabaseName = "Newsoftv"
            'GloUserID = "sa"
            'GloPassword = "06011975"

            GloServerName = "RICHARD2-PC"
            GloDatabaseName = "Newsoftv_SabinasFibra"
            GloUserID = "sa"
            GloPassword = "06011975"


            ' MsgBox(MiConexion, MsgBoxStyle.Information, "Conexi�n")
            MiConexion = "Data Source=" & GloServerName & ";Initial Catalog=" & GloDatabaseName & " ;User ID=" & GloUserID & ";Password=" & GloPassword & ";Connection Timeout=0" & ""
            GloIp = GloServerName
            LoginForm1.Show()
            Me.Close()
        End If
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        Entrar()
    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        End
        Me.Close()
    End Sub

    Private Sub Label2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Label2.Click

    End Sub

    Private Sub Label3_Click(sender As System.Object, e As System.EventArgs) Handles Label3.Click

    End Sub
End Class
