Imports System.Data.SqlClient

Public Class FormCAMDO
    Private opcionlocal As String = "N"

    Private Sub FormCAMDO_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        colorea(Me)
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        'TODO: esta l�nea de c�digo carga datos en la tabla 'NewSofTvDataSet.MUESTRACALLES' Puede moverla o quitarla seg�n sea necesario.
        Me.MUESTRACALLESTableAdapter.Connection = CON
        'Me.MUESTRACALLESTableAdapter.Fill(Me.NewsoftvDataSet1.MUESTRACALLES)
        'BaseII.limpiaParametros()
        'BaseII.CreateMyParameter("@idcompania", SqlDbType.Int, GloIdCompania)
        'Clv_CalleComboBox.DataSource = BaseII.ConsultaDT("MUESTRACALLESCOMPANIAS")
        CON.Close()
        'Me.Clv_ColoniaComboBox.Enabled = False
        'Me.Clv_CiudadComboBox.Enabled = False
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@contrato", SqlDbType.Int, CInt(GloContrato))
        BaseII.CreateMyParameter("@clv_ciudad", ParameterDirection.Output, SqlDbType.Int)
        Clv_CiudadComboBox.DataSource = BaseII.ConsultaDT("sp_llenaCiudadCamdo")
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@contrato", SqlDbType.Int, CInt(GloContrato))
        BaseII.CreateMyParameter("@clv_ciudad", ParameterDirection.Output, SqlDbType.Int)
        BaseII.ProcedimientoOutPut("sp_llenaCiudadCamdo")
        Clv_CiudadComboBox.SelectedValue = BaseII.dicoPar("@clv_ciudad")
        opcionlocal = "N"
        'Me.CONCAMDOFACBindingSource.AddNew()
        Me.CONTRATOTextBox.Text = GloContrato
        Me.Clv_OrdenTextBox.Text = gloClv_Session
        'Me.ContratoTextBox1.Text = GloContrato
        'Me.Clv_SessionTextBox.Text = gloClv_Session
        dimeSiCamdo()
        'Busca()
        UspGuardaFormularios(Me.Name, Me.Text)
        UspGuardaBotonesFormularioSiste(Me, Me.Name)
        UspDesactivaBotones(Me, Me.Name)
    End Sub

    Private Sub dimeSiCamdo()
        Try
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@existe", ParameterDirection.Output, SqlDbType.Int)
            BaseII.CreateMyParameter("@clv_sesion", SqlDbType.Int, gloClv_Session)
            BaseII.CreateMyParameter("@contrato", SqlDbType.Int, GloContrato)
            BaseII.ProcedimientoOutPut("CONCAMDOFAC")
            If BaseII.dicoPar("@existe") = 1 Then
                Dim conexion As New SqlConnection(MiConexion)
                conexion.Open()
                Dim comando As New SqlCommand()
                comando.Connection = conexion
                comando.CommandText = "exec CONCAMDOFAC " + gloClv_Session.ToString + "," + GloContrato.ToString + ",0"
                Dim reader As SqlDataReader = comando.ExecuteReader()
                reader.Read()
                NUMEROTextBox.Text = reader(4).ToString
                ENTRECALLESTextBox.Text = reader(5).ToString
                TELEFONOTextBox.Text = reader(7).ToString
                Clv_CiudadComboBox.SelectedValue = reader(9)
                cbLocalidad.SelectedValue = reader(10)
                Clv_ColoniaComboBox.SelectedValue = reader(6)
                Clv_CalleComboBox.SelectedValue = reader(3)
                reader.Close()
                conexion.Close()
            End If
        Catch ex As Exception
            MsgBox(ex.ToString)
        End Try
    End Sub

    Private Sub CONCAMDOFACBindingNavigatorSaveItem_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CONCAMDOFACBindingNavigatorSaveItem.Click

        If IsNumeric(Me.Clv_CalleComboBox.SelectedValue) = False Then
            MsgBox("Seleccione la Calle por favor", MsgBoxStyle.Information)
            Exit Sub
        End If
        If IsNumeric(Me.Clv_ColoniaComboBox.SelectedValue) = False Then
            MsgBox("Seleccione la Colonia por favor", MsgBoxStyle.Information)
            Exit Sub
        End If
        If IsNumeric(Me.Clv_CiudadComboBox.SelectedValue) = False Or Len(Me.Clv_CiudadComboBox.Text) = 0 Then
            MsgBox("Seleccione la Ciudad por favor", MsgBoxStyle.Information)
            Exit Sub
        End If
        If IsNumeric(Me.cbLocalidad.SelectedValue) = False Or Len(Me.cbLocalidad.Text) = 0 Then
            MsgBox("Seleccione la localidad por favor", MsgBoxStyle.Information)
            Exit Sub
        End If
        If Len(Trim(Me.NUMEROTextBox.Text)) = 0 Then
            MsgBox("Capture el n�mero por favor", MsgBoxStyle.Information)
            Exit Sub
        End If
        If Len(Trim(Me.ENTRECALLESTextBox.Text)) = 0 Then
            MsgBox("Capture las entre calles por favor", MsgBoxStyle.Information)
            Exit Sub
        End If
        If Len(Trim(Me.TELEFONOTextBox.Text)) = 0 Then
            MsgBox("Capture el n�mero telef�nico por favor ", MsgBoxStyle.Information)
            Exit Sub
        End If
        If Len(Trim(Me.TELEFONOTextBox.Text)) <> 10 Then
            MsgBox("El n�mero telef�nico debe tener una longitud de 10 n�meros")
            Exit Sub
        End If
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        Me.Validate()
        ''Me.CONCAMDOFACBindingSource.EndEdit()
        'Me.CONCAMDOFACTableAdapter.Connection = CON
        'Me.CONCAMDOFACTableAdapter.Update(gloClv_Session, GloContrato, CInt(Me.Clv_CalleComboBox.SelectedValue), Me.NUMEROTextBox.Text, Me.ENTRECALLESTextBox.Text, CInt(Me.Clv_ColoniaComboBox.SelectedValue), Me.TELEFONOTextBox.Text, 0, CInt(Me.Clv_CiudadComboBox.SelectedValue))
        ''Me.ConCAMDOTMPTableAdapter.Connection = CON
        ''Me.ConCAMDOTMPTableAdapter.Update(gloClv_Session, GloContrato, CInt(Me.Clv_CalleComboBox.SelectedValue), Me.NUMEROTextBox.Text, Me.ENTRECALLESTextBox.Text, CInt(Me.Clv_ColoniaComboBox.SelectedValue), Me.TELEFONOTextBox.Text, 0, CInt(Me.Clv_CiudadComboBox.SelectedValue))

        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@Clv_Sesion", SqlDbType.BigInt, gloClv_Session)
        BaseII.CreateMyParameter("@CONTRATO", SqlDbType.BigInt, GloContrato)
        BaseII.CreateMyParameter("@Clv_calle", SqlDbType.Int, Me.Clv_CalleComboBox.SelectedValue)
        BaseII.CreateMyParameter("@Numero", SqlDbType.VarChar, NUMEROTextBox.Text, 50)
        BaseII.CreateMyParameter("@NumeroInt", SqlDbType.VarChar, NumIntTbox.Text, 50)
        BaseII.CreateMyParameter("@EntreCalles", SqlDbType.VarChar, Me.ENTRECALLESTextBox.Text, 250)
        BaseII.CreateMyParameter("@Clv_Colonia", SqlDbType.Int, Clv_ColoniaComboBox.SelectedValue)
        BaseII.CreateMyParameter("@clv_localidad", SqlDbType.Int, cbLocalidad.SelectedValue)
        BaseII.CreateMyParameter("@Telefono", SqlDbType.VarChar, Me.TELEFONOTextBox.Text, 50)
        BaseII.CreateMyParameter("@ClvTecnica", SqlDbType.Int, 0)
        BaseII.CreateMyParameter("@Clv_Ciudad", SqlDbType.Int, Clv_CiudadComboBox.SelectedValue)
        BaseII.CreateMyParameter("@Clv_sector", SqlDbType.Int, 0)

        BaseII.Inserta("NUECAMDOFACnoInt")

        CON.Close()
        GloBndExt = True
        Me.Close()
    End Sub

    Private Sub BindingNavigatorDeleteItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BindingNavigatorDeleteItem.Click
        Dim CON As New SqlConnection(MiConexion)

        'If IsNumeric(Me.CLAVETextBox.Text) = False Then
        '    MsgBox("No ahi Datos para eliminar ", MsgBoxStyle.Information)
        '    Exit Sub
        'End If
        If IsNumeric(Me.Clv_OrdenTextBox.Text) = False Then
            MsgBox("No hay Datos para eliminar ", MsgBoxStyle.Information)
            Exit Sub
        End If
        If IsNumeric(Me.CONTRATOTextBox.Text) = False Then
            MsgBox("No hay Datos para eliminar ", MsgBoxStyle.Information)
            Exit Sub
        End If
        CON.Open()
        Me.CONCAMDOFACTableAdapter.Delete(gloClv_Session, GloContrato)
        Me.CONCAMDOFACBindingSource.CancelEdit()
        'Me.ConCAMDOTMPTableAdapter.Connection = CON
        'Me.ConCAMDOTMPTableAdapter.Delete(gloClv_Session, GloContrato)
        CON.Close()

        GloBndExt = False
        Me.Close()
    End Sub

    Private Sub Clv_CiudadComboBox_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Clv_CiudadComboBox.SelectedIndexChanged
        Try
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@contrato", SqlDbType.Int, GloContrato)
            BaseII.CreateMyParameter("@clv_localidad", ParameterDirection.Output, SqlDbType.Int)
            BaseII.CreateMyParameter("@clv_ciudad", SqlDbType.Int, Clv_CiudadComboBox.SelectedValue)
            cbLocalidad.DataSource = BaseII.ConsultaDT("sp_llenaLocalidadCamdo")
            BaseII.ProcedimientoOutPut("sp_llenaLocalidadCamdo")
            cbLocalidad.SelectedValue = BaseII.dicoPar("@clv_localidad")
        Catch ex As Exception

        End Try
    End Sub

    Private Sub Button5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button5.Click
        GloBndExt = False
        Me.Close()
    End Sub

    Private Sub Clv_ColoniaComboBox_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Clv_ColoniaComboBox.SelectedIndexChanged
        'Me.asignacolonia()
        'DameSector()
        Try
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@clv_colonia", SqlDbType.Int, Clv_ColoniaComboBox.SelectedValue)
            BaseII.CreateMyParameter("@contrato", SqlDbType.Int, GloContrato)
            Clv_CalleComboBox.DataSource = BaseII.ConsultaDT("sp_llenaCalleCamdo")
        Catch ex As Exception

        End Try
    End Sub
    Private Sub DameSector()
        Try
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@clv_colonia", SqlDbType.Int, Clv_ColoniaComboBox.SelectedValue)
            ComboBoxSector.DataSource = BaseII.ConsultaDT("MuestraSectoresNew")
        Catch ex As Exception

        End Try
    End Sub
    Private Sub Clv_CalleComboBox_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Clv_CalleComboBox.SelectedIndexChanged
        'asiganacalle()
    End Sub
    Private Sub Busca()
        Try
            Dim CON As New SqlConnection(MiConexion)
            CON.Open()
            Me.CONCAMDOFACTableAdapter.Connection = CON
            Me.CONCAMDOFACTableAdapter.Fill(Me.NewsoftvDataSet2.CONCAMDOFAC, New System.Nullable(Of Long)(CType(gloClv_Session, Long)), New System.Nullable(Of Long)(CType(GloContrato, Long)))
            'Me.ConCAMDOTMPTableAdapter.Connection = CON
            'Me.ConCAMDOTMPTableAdapter.Fill(Me.DataSetEric3.ConCAMDOTMP, gloClv_Session, GloContrato)
            CON.Close()
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try

    End Sub

    Private Sub asignacolonia()
        Try
            If IsNumeric(Me.Clv_ColoniaComboBox.SelectedValue) = True Then
                Dim CON As New SqlConnection(MiConexion)
                CON.Open()
                Me.MuestraCVECOLCIUTableAdapter.Connection = CON
                'Me.MuestraCVECOLCIUTableAdapter.Fill(Me.NewsoftvDataSet2.MuestraCVECOLCIU, Me.Clv_ColoniaComboBox.SelectedValue)
                BaseII.limpiaParametros()
                BaseII.CreateMyParameter("@Clv_Colonia", SqlDbType.Int, Clv_ColoniaComboBox.SelectedValue)
                'BaseII.CreateMyParameter("@idcompania", SqlDbType.Int, GloIdCompania)
                cbLocalidad.DataSource = BaseII.ConsultaDT("MuestraLocalidadColonia")
                CON.Close()
                If opcionlocal = "N" Then Me.Clv_CiudadComboBox.Text = ""
                Me.cbLocalidad.Enabled = True

            End If

        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)


        End Try
    End Sub

    Private Sub asiganacalle()
        Try
            If IsNumeric(Me.Clv_CalleComboBox.SelectedValue) = True Then
                Dim CON As New SqlConnection(MiConexion)
                CON.Open()
                Me.DAMECOLONIA_CALLETableAdapter.Connection = CON
                'Me.DAMECOLONIA_CALLETableAdapter.Fill(Me.NewsoftvDataSet2.DAMECOLONIA_CALLE, New System.Nullable(Of Integer)(CType(Me.Clv_CalleComboBox.SelectedValue, Integer)))
                BaseII.limpiaParametros()
                BaseII.CreateMyParameter("@CLV_CALLE", SqlDbType.Int, Clv_CalleComboBox.SelectedValue)
                BaseII.CreateMyParameter("@idcompania", SqlDbType.Int, GloIdCompania)
                Clv_ColoniaComboBox.DataSource = BaseII.ConsultaDT("DAMECOLONIA_CALLECOMPANIA")
                CON.Close()
                Me.Clv_ColoniaComboBox.Enabled = True
                If opcionlocal = "N" Then Me.Clv_ColoniaComboBox.Text = ""
            End If

        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub cbLocalidad_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cbLocalidad.SelectedIndexChanged
       Try
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@clv_localidad", SqlDbType.Int, cbLocalidad.SelectedValue)
            BaseII.CreateMyParameter("@contrato", SqlDbType.Int, GloContrato)
            Clv_ColoniaComboBox.DataSource = BaseII.ConsultaDT("sp_llenaColoniaCamdo")
        Catch ex As Exception

        End Try
    End Sub

    Private Sub Button38_Click(sender As Object, e As EventArgs) Handles Button38.Click
        FrmEntreCalles.ContratoEntreCalles = GloContrato
        FrmEntreCalles.clv_sesionEntreCalles = gloClv_Session
        FrmEntreCalles.clv_coloniaEntreCalles = Clv_ColoniaComboBox.SelectedValue
        FrmEntreCalles.CallePrincipal = Clv_CalleComboBox.SelectedValue
        FrmEntreCalles.Show()
    End Sub
End Class