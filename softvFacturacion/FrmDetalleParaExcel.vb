﻿Imports CrystalDecisions.Shared
Imports CrystalDecisions.CrystalReports.Engine
Imports System.Data.SqlClient
Imports NPOI.HSSF.UserModel
Imports NPOI.SS.UserModel
Imports NPOI.SS.Util
Imports NPOI.HSSF.Util
Imports NPOI.POIFS.FileSystem
Imports NPOI.HPSF
Imports NPOI.XSSF.UserModel
Imports NPOI.XSSF.Util
Imports System.Text
Imports System.IO
Imports System.Web.Security
Public Class FrmDetalleParaExcel
    Public companias As String = ""
    Public ciudades As String = ""
    Public contador As Integer = 0
    Dim DS As New DataSet
    Dim fileOut As FileStream
    Private Sub FrmDetalleParaExcel_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        If GloOpFiltrosXML = "CorteNuevo" Then
            ReporteDetallePagos()
        ElseIf GloOpFiltrosXML = "DetalleEspeciales" Then
            ReporteDetallePagosEspeciales()
        End If
        GloOpFiltrosXML = ""
    End Sub
    Private Sub ReporteDetallePagos()
        Try
            Me.Text = "Reporte de Detalle de Pagos"
            Dim customersByCityReport As ReportDocument = New ReportDocument
            Dim connectionInfo As New ConnectionInfo
            Dim reportPath As String = Nothing

            reportPath = RutaReportes + "\ReporteCorteNuevoDetallado.rpt"

            Dim listatablas As New List(Of String)
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@FecIni", SqlDbType.DateTime, eFechaInicial)
            BaseII.CreateMyParameter("@FecFin", SqlDbType.DateTime, eFechaFinal)
            BaseII.CreateMyParameter("@identificador", SqlDbType.Int, identificador)
            BaseII.CreateMyParameter("@CompaniasXml", SqlDbType.Xml, DocCompanias.InnerXml)
            BaseII.CreateMyParameter("@SucursalesXml", SqlDbType.Xml, DocSucursales.InnerXml)
            listatablas.Add("Detalle")
            DS = BaseII.ConsultaDS("ReporteMuchoDetallePagos", listatablas)

            customersByCityReport.Load(reportPath)
            customersByCityReport.SetDataSource(DS)

            Dim ciudades As String = ""
            'Dim elementos = DocCompanias.SelectNodes("//COMPANIA")
            'For Each elem As Xml.XmlElement In elementos
            '    If elem.GetAttribute("Seleccion") = "1" Then
            '        If ciudades = "Plazas: " Then
            '            ciudades = ciudades + elem.GetAttribute("Nombre")
            '        Else
            '            ciudades = ciudades + ", " + elem.GetAttribute("Nombre")
            '        End If
            '    End If
            'Next
            Dim companias As String = "Distribuidores: "
            Dim elementos2 = DocPlazas.SelectNodes("//PLAZA")
            For Each elem As Xml.XmlElement In elementos2
                If elem.GetAttribute("Seleccion") = "1" Then
                    If companias = "Distribuidores: " Then
                        companias = companias + elem.GetAttribute("nombre")
                    Else
                        companias = companias + ", " + elem.GetAttribute("nombre")
                    End If
                End If
            Next
            customersByCityReport.DataDefinition.FormulaFields("FechaIni").Text = "'" & eFechaInicial & "'"
            customersByCityReport.DataDefinition.FormulaFields("FechaFin").Text = "'" & eFechaFinal & "'"
            customersByCityReport.DataDefinition.FormulaFields("Companias").Text = "'" & ciudades & "'"
            customersByCityReport.DataDefinition.FormulaFields("Distribuidores").Text = "'" & companias & "'"
            CrystalReportViewer1.ShowGroupTreeButton = False
            CrystalReportViewer1.ToolPanelView = CrystalDecisions.Windows.Forms.ToolPanelViewType.None
            CrystalReportViewer1.ReportSource = customersByCityReport
            customersByCityReport = Nothing
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub ReporteDetallePagosEspeciales()
        Try
            Me.Text = "Reporte de Detalle de Pagos Sucursales Especiales"
            Dim customersByCityReport As ReportDocument = New ReportDocument
            Dim connectionInfo As New ConnectionInfo
            Dim reportPath As String = Nothing

            reportPath = RutaReportes + "\ReporteCorteNuevoDetallado.rpt"

            Dim listatablas As New List(Of String)
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@FecIni", SqlDbType.DateTime, eFechaInicial)
            BaseII.CreateMyParameter("@FecFin", SqlDbType.DateTime, eFechaFinal)
            BaseII.CreateMyParameter("@identificador", SqlDbType.Int, identificador)
            BaseII.CreateMyParameter("@SucursalesXml", SqlDbType.Xml, DocSucursales.InnerXml)
            listatablas.Add("Detalle")
            DS = BaseII.ConsultaDS("ReporteMuchoDetallePagosEspeciales", listatablas)

            customersByCityReport.Load(reportPath)
            customersByCityReport.SetDataSource(DS)

            Dim ciudades As String = ""

            Dim companias As String = "Sucursales: "
            Dim elementos2 = DocSucursales.SelectNodes("//SUCURSAL")
            For Each elem As Xml.XmlElement In elementos2
                If elem.GetAttribute("Seleccion") = "1" Then
                    If companias = "Sucursales: " Then
                        companias = companias + elem.GetAttribute("Nombre")
                    Else
                        companias = companias + ", " + elem.GetAttribute("Nombre")
                    End If
                End If
            Next
            customersByCityReport.DataDefinition.FormulaFields("FechaIni").Text = "'" & eFechaInicial & "'"
            customersByCityReport.DataDefinition.FormulaFields("FechaFin").Text = "'" & eFechaFinal & "'"
            customersByCityReport.DataDefinition.FormulaFields("Companias").Text = "'" & ciudades & "'"
            customersByCityReport.DataDefinition.FormulaFields("Distribuidores").Text = "'" & companias & "'"
            CrystalReportViewer1.ShowGroupTreeButton = False
            CrystalReportViewer1.ToolPanelView = CrystalDecisions.Windows.Forms.ToolPanelViewType.None
            CrystalReportViewer1.ReportSource = customersByCityReport
            customersByCityReport = Nothing
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        Try
            Dim rutaFile As String = ""
            Dim fecha As String = AgregaCeros(Today.Day.ToString) + AgregaCeros(Today.Month.ToString) + Today.Year.ToString
            'BaseII.limpiaParametros()
            'BaseII.CreateMyParameter("@FecIni", SqlDbType.DateTime, eFechaInicial)
            'BaseII.CreateMyParameter("@FecFin", SqlDbType.DateTime, eFechaFinal)
            'BaseII.CreateMyParameter("@identificador", SqlDbType.Int, identificador)
            'BaseII.CreateMyParameter("@CompaniasXml", SqlDbType.Xml, DocCompanias.InnerXml)
            'BaseII.CreateMyParameter("@SucursalesXml", SqlDbType.Xml, DocSucursales.InnerXml)
            'Dim dt = BaseII.ConsultaDT("ReporteMuchoDetallePagos")
            'Dim wb As New XSSFWorkbook()
            Dim wb As New XSSFWorkbook()
            Dim result As DialogResult = FolderBrowserDialog1.ShowDialog()
            If (result = DialogResult.OK) Then
                rutaFile = Me.FolderBrowserDialog1.SelectedPath.ToString
                If File.Exists(rutaFile + "\DetallePagos" + fecha + ".xlsx") Then
                    File.Delete(rutaFile + "\DetallePagos" + fecha + ".xlsx")
                End If
                Dim sheet1 As ISheet = wb.CreateSheet("Page 0")
                fileOut = New FileStream(rutaFile + "\DetallePagos" + fecha + "-0.xlsx", FileMode.Create, FileAccess.ReadWrite)
                Dim rowNew As IRow = sheet1.CreateRow(0)
                rowNew.CreateCell(0).SetCellValue("Clv_Factura")
                rowNew.CreateCell(1).SetCellValue("Distribuidor-Plaza")
                rowNew.CreateCell(2).SetCellValue("Ticket")
                rowNew.CreateCell(3).SetCellValue("Fecha")
                rowNew.CreateCell(4).SetCellValue("Contrato")
                rowNew.CreateCell(5).SetCellValue("Cliente")
                rowNew.CreateCell(6).SetCellValue("Periodo pagado")
                rowNew.CreateCell(7).SetCellValue("Periodo pagado (2)")
                rowNew.CreateCell(8).SetCellValue("Ciclo Facturación")
                rowNew.CreateCell(9).SetCellValue("Paquete al momento del pago")
                rowNew.CreateCell(10).SetCellValue("# STB al momento del pago")
                rowNew.CreateCell(11).SetCellValue("Tipo de Pago")
                rowNew.CreateCell(12).SetCellValue("Concepto de pago")
                rowNew.CreateCell(13).SetCellValue("Cantidad")
                rowNew.CreateCell(14).SetCellValue("Subtotal")
                rowNew.CreateCell(15).SetCellValue("IEPS")
                rowNew.CreateCell(16).SetCellValue("IVA")
                rowNew.CreateCell(17).SetCellValue("Total")
                rowNew.CreateCell(18).SetCellValue("Tipo de tarifa aplicada")
                rowNew.CreateCell(19).SetCellValue("Precio unitario")
                rowNew.CreateCell(20).SetCellValue("Contratación")
                rowNew.CreateCell(21).SetCellValue("Otros")
                rowNew.CreateCell(22).SetCellValue("Puntos")
                rowNew.CreateCell(23).SetCellValue("Descuento aplicado")
                rowNew.CreateCell(24).SetCellValue("Dias proporcionales")
                rowNew.CreateCell(25).SetCellValue("Importe dias proporcionales")
                rowNew.CreateCell(26).SetCellValue("Dias bonificados")
                rowNew.CreateCell(27).SetCellValue("Importe bonificado")
                rowNew.CreateCell(28).SetCellValue("Meses adelantados")
                rowNew.CreateCell(29).SetCellValue("Importe adelantados")
                rowNew.CreateCell(30).SetCellValue("Meses al corriente")
                rowNew.CreateCell(31).SetCellValue("Importe al corriente")
                rowNew.CreateCell(32).SetCellValue("Importe promocional")
                rowNew.CreateCell(33).SetCellValue("Dias proporcionales de cambio de paquete")
                rowNew.CreateCell(34).SetCellValue("Precio paquete nuevo (Cambio)")
                rowNew.CreateCell(35).SetCellValue("Precio renta nuevo (Cambio)")
                rowNew.CreateCell(36).SetCellValue("Precio paquete anterior (Cambio)")
                rowNew.CreateCell(37).SetCellValue("Precio renta anterior (Cambio)")
                rowNew.CreateCell(38).SetCellValue("Paquete Nuevo (Cambio)") 'Columna nueva
                rowNew.CreateCell(39).SetCellValue("Total desgloce")
                rowNew.CreateCell(40).SetCellValue("Diferencia")
                'wb.Write(fileOut)
                'fileOut.Close()

                Dim x As Integer = 1
                Dim paginas As Integer = 0
                For Each row As DataRow In DS.Tables(0).Rows
                    
                    Dim rowNew2 As IRow = sheet1.CreateRow(x)
                    For i As Integer = 0 To 40
                        Dim aux = row(i).GetType().ToString
                        If row(i).GetType().ToString = "System.Int32" Then
                            rowNew2.CreateCell(i).SetCellValue(CInt(row(i).ToString))
                        ElseIf row(i).GetType().ToString = "System.Int16" Then
                            rowNew2.CreateCell(i).SetCellValue(CInt(row(i).ToString))
                        ElseIf row(i).GetType().ToString = "System.Decimal" Then
                            rowNew2.CreateCell(i).SetCellValue(CDbl(row(i).ToString))
                        Else
                            rowNew2.CreateCell(i).SetCellValue(row(i).ToString)
                        End If
                    Next
                    x = x + 1
                    If x Mod 10000 = 0 Then
                        paginas = paginas + 1
                        'sheet1 = wb.CreateSheet("Page " + paginas.ToString)
                        wb.Write(fileOut)
                        fileOut.Close()
                        wb = New XSSFWorkbook()
                        sheet1 = wb.CreateSheet("Page 0")
                        fileOut = New FileStream(rutaFile + "\DetallePagos" + fecha + "-" + paginas.ToString + ".xlsx", FileMode.Create, FileAccess.ReadWrite)
                        x = 0
                        'ElseIf x = DS.Tables(0).Rows.Count Then
                        '    fileOut = New FileStream(rutaFile + "\DetallePagos" + fecha + ".xlsx", FileMode.Open, FileAccess.ReadWrite)
                        '    wb.Write(fileOut)
                        '    fileOut.Close()
                    End If
                Next
                wb.Write(fileOut)
                'wb.Write(fileOut)
                fileOut.Close()
                MsgBox("Archivo generado exitosamente.")
            End If
        Catch ex As Exception
            MsgBox(ex.ToString)
        End Try
    End Sub

    Private Function AgregaCeros(ByVal cadenas As String)
        If cadenas.Length = 1 Then
            cadenas = "0" + cadenas
        End If
        Return cadenas
    End Function
End Class