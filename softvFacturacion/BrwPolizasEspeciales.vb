﻿Public Class BrwPolizasEspeciales

    Private Sub BrwPolizasEspeciales_Activated(sender As Object, e As EventArgs) Handles Me.Activated
        If Locbndactualizapoiza = True Then
            Locbndactualizapoiza = False
            MUESTRAPoliza(0, 0, DateTime.Today, "", cbSucursal.SelectedValue)
        End If
    End Sub

    Private Sub BrwPolizasEspeciales_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        colorea(Me)
        llenaSucursales()
    End Sub

    Private Sub llenaSucursales()
        Try
            BaseII.limpiaParametros()
            cbSucursal.DataSource = BaseII.ConsultaDT("ObtieneSucursalesEspeciales")
        Catch ex As Exception

        End Try
    End Sub

    Private Sub Button7_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button7.Click
        If TextBox1.Text.Length = 0 Then
            MessageBox.Show("Captura una Clave Póliza.")
            Exit Sub
        End If
        If IsNumeric(TextBox1.Text) = False Then
            MessageBox.Show("Captura una Clave Póliza válida.")
            Exit Sub
        End If
        MUESTRAPoliza(1, TextBox1.Text, DateTime.Today, "", cbSucursal.SelectedValue)

    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        MUESTRAPoliza(2, 0, DateTimePicker1.Value, "", cbSucursal.SelectedValue)


    End Sub

    Private Sub Button5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button5.Click
        Me.Close()
    End Sub

    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button3.Click

        LocopPoliza = "N"

        gloClv_Session = 0
        LocbndPolizaCiudad = True
        GloClvCompania = cbSucursal.SelectedValue
        'FrmFiltroPoliza.Show()
        varfrmselcompania = "poliza"
        FrmFiltroPolizaEspecial.Show()
        'FrmSelCiudadJ.Show()
        ' FrmSeleccionaSucursalPoliza.Show()
        ' FrmFechaIngresosConcepto.Show()
        ' FrmSelSucursal.Show()
    End Sub

    Private Sub Button4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button4.Click
        If DataGridView1.Rows.Count = 0 Then
            MessageBox.Show("Selecciona una Póliza para ser Consultada.")
            Exit Sub
        End If
        LocopPoliza = "C"
        LocGloClv_poliza = DataGridView1.SelectedCells(0).Value
        FrmPolizaEspecial.Show()

    End Sub

    Private Sub Button6_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button6.Click
        If DataGridView1.Rows.Count = 0 Then
            MessageBox.Show("Selecciona una Póliza para ser Modificada.")
            Exit Sub
        End If
        LocopPoliza = "M"
        LocGloClv_poliza = DataGridView1.SelectedCells(0).Value
        FrmPolizaEspecial.Show()

    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        If DataGridView1.Rows.Count = 0 Then
            MessageBox.Show("Selecciona una Póliza a Eliminar.")
            Exit Sub
        End If
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@CLV_POLIZA", SqlDbType.Int, DataGridView1.SelectedCells(0).Value)
        BaseII.Inserta("ELIMINAPolizaEspecial")

        MUESTRAPoliza(0, 0, DateTime.Today, "", cbSucursal.SelectedValue)

    End Sub

    Private Sub Button8_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button8.Click
        LocGloClv_poliza = DataGridView1.SelectedCells(0).Value
        If IsNumeric(LocGloClv_poliza) = True Then
            gLOoPrEPpOLIZA = 0
            FrmImprimePoliza.Show()
            gLOoPrEPpOLIZA = 0
        End If
    End Sub

    Private Sub Button9_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button9.Click

        If IsNumeric(LocGloClv_poliza) = True Then
            gLOoPrEPpOLIZA = 1
            FrmImprimePoliza.Show()
            gLOoPrEPpOLIZA = 0
        End If
    End Sub

    Private Sub MUESTRAPoliza(ByVal Op As Integer, ByVal Clv_Llave_Poliza As Integer, ByVal Fecha As DateTime, ByVal Concepto As String, ByVal Sucursal As Integer)
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@Op", SqlDbType.Int, Op)
        BaseII.CreateMyParameter("@Clv_Llave_Poliza", SqlDbType.Int, Clv_Llave_Poliza)
        BaseII.CreateMyParameter("@Fecha", SqlDbType.DateTime, Fecha)
        BaseII.CreateMyParameter("@Concepto", SqlDbType.VarChar, Concepto, 250)
        BaseII.CreateMyParameter("@Sucursal", SqlDbType.Int, Sucursal)
        DataGridView1.DataSource = BaseII.ConsultaDT("MUESTRAPolizaEspecial")
    End Sub

    Private Sub cbCompania_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
        'If cbCompania.Items.Count = 0 Then
        '    Exit Sub
        'End If
        GloClvCompania = cbSucursal.SelectedValue
        MUESTRAPoliza(0, 0, DateTime.Today, "", cbSucursal.SelectedValue)
        'MUESTRASucursalesCompania(GloClvCompania)
    End Sub

    Private Sub DataGridView1_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles DataGridView1.CellContentClick
        Try
            Clv_calleLabel2.Text = DataGridView1.SelectedCells(0).Value
            CMBNombreTextBox.Text = DataGridView1.SelectedCells(1).Value
            CMBTextBox5.Text = DataGridView1.SelectedCells(2).Value
        Catch ex As Exception

        End Try
    End Sub

    Private Sub MUESTRASucursalesCompania(ByVal CLVCOMPANIA As Integer)
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@CLVCOMPANIA", SqlDbType.Int, CLVCOMPANIA)
        cbSucursal.DataSource = BaseII.ConsultaDT("MUESTRASucursalesPorCompania")
    End Sub

    Private Sub bnBuscarSucursal_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles bnBuscarSucursal.Click
        If cbSucursal.Text.Length = 0 Then Exit Sub
        MUESTRAPoliza(4, 0, DateTime.Today, "", cbSucursal.SelectedValue)
    End Sub

    Private Sub cbSucursal_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cbSucursal.SelectedIndexChanged
        MUESTRAPoliza(0, 0, DateTime.Today, "", cbSucursal.SelectedValue)
    End Sub

    Private Sub cbDistribuidor_SelectedIndexChanged(sender As Object, e As EventArgs)
        Try
            'BaseII.limpiaParametros()
            'BaseII.CreateMyParameter("@clv_plaza", SqlDbType.Int, cbDistribuidor.SelectedValue)
            'cbCompania.DataSource = BaseII.ConsultaDT("MuestraCompaniaPlaza")
            MUESTRAPoliza(0, 0, DateTime.Today, "", cbSucursal.SelectedValue)
            'MUESTRASucursalesCompania(cbDistribuidor.SelectedValue)
        Catch ex As Exception

        End Try
    End Sub

    Private Sub DataGridView1_SelectionChanged(sender As Object, e As EventArgs) Handles DataGridView1.SelectionChanged
        Try
            Clv_calleLabel2.Text = DataGridView1.SelectedCells(0).Value
            CMBNombreTextBox.Text = DataGridView1.SelectedCells(1).Value
            CMBTextBox5.Text = DataGridView1.SelectedCells(2).Value
        Catch ex As Exception

        End Try
    End Sub

    Private Sub CMBLabel1_Click(sender As Object, e As EventArgs) Handles CMBLabel1.Click

    End Sub
End Class