<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmRecepOxxo
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim ReciboLabel As System.Windows.Forms.Label
        Dim CLIENTELabel As System.Windows.Forms.Label
        Dim Clv_SessionLabel As System.Windows.Forms.Label
        Dim StatusLabel As System.Windows.Forms.Label
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle4 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.OpenFileDialog1 = New System.Windows.Forms.OpenFileDialog()
        Me.CONSULTA_Resultado_OxxoBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.DataSetEdgar = New softvFacturacion.DataSetEdgar()
        Me.Button2 = New System.Windows.Forms.Button()
        Me.Button3 = New System.Windows.Forms.Button()
        Me.ReciboTextBox = New System.Windows.Forms.TextBox()
        Me.CLIENTETextBox = New System.Windows.Forms.TextBox()
        Me.CONSULTA_Resultado_OxxoTableAdapter = New softvFacturacion.DataSetEdgarTableAdapters.CONSULTA_Resultado_OxxoTableAdapter()
        Me.CONSULTATablaClv_Session_OxxoBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.CONSULTATablaClv_Session_OxxoTableAdapter = New softvFacturacion.DataSetEdgarTableAdapters.CONSULTATablaClv_Session_OxxoTableAdapter()
        Me.Clv_SessionTextBox = New System.Windows.Forms.TextBox()
        Me.StatusTextBox = New System.Windows.Forms.TextBox()
        Me.Button5 = New System.Windows.Forms.Button()
        Me.Button6 = New System.Windows.Forms.Button()
        Me.dgvCargaArchivos = New System.Windows.Forms.DataGridView()
        Me.Clv_Usuario = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.nombre1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Clv_Session1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Fecha1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Status = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.dgvDetalleArchivoCargado = New System.Windows.Forms.DataGridView()
        Me.CLIENTE = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.nombre = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Clv_Session = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Consecutivo = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Tienda = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Fecha = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Hora = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Estado = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Temp = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Monto = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Clv_Cliente = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Factura = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.idFactura = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Recibo = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Detalle = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.MOVIMIENTO = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DIFIMPORTA = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.VerAcceso2TableAdapter1 = New softvFacturacion.ProcedimientosArnoldo3TableAdapters.VerAcceso2TableAdapter()
        Me.Button4 = New System.Windows.Forms.Button()
        Me.VerAcceso2TableAdapter2 = New softvFacturacion.ProcedimientosArnoldo3TableAdapters.VerAcceso2TableAdapter()
        Me.VerAcceso2TableAdapter3 = New softvFacturacion.ProcedimientosArnoldo3TableAdapters.VerAcceso2TableAdapter()
        Me.VerAcceso2TableAdapter4 = New softvFacturacion.ProcedimientosArnoldo3TableAdapters.VerAcceso2TableAdapter()
        Me.VerAcceso2TableAdapter5 = New softvFacturacion.ProcedimientosArnoldo3TableAdapters.VerAcceso2TableAdapter()
        ReciboLabel = New System.Windows.Forms.Label()
        CLIENTELabel = New System.Windows.Forms.Label()
        Clv_SessionLabel = New System.Windows.Forms.Label()
        StatusLabel = New System.Windows.Forms.Label()
        CType(Me.CONSULTA_Resultado_OxxoBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DataSetEdgar, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CONSULTATablaClv_Session_OxxoBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgvCargaArchivos, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgvDetalleArchivoCargado, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'ReciboLabel
        '
        ReciboLabel.AutoSize = True
        ReciboLabel.Location = New System.Drawing.Point(495, 417)
        ReciboLabel.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        ReciboLabel.Name = "ReciboLabel"
        ReciboLabel.Size = New System.Drawing.Size(56, 17)
        ReciboLabel.TabIndex = 6
        ReciboLabel.Text = "Recibo:"
        '
        'CLIENTELabel
        '
        CLIENTELabel.AutoSize = True
        CLIENTELabel.Location = New System.Drawing.Point(480, 449)
        CLIENTELabel.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        CLIENTELabel.Name = "CLIENTELabel"
        CLIENTELabel.Size = New System.Drawing.Size(69, 17)
        CLIENTELabel.TabIndex = 7
        CLIENTELabel.Text = "CLIENTE:"
        AddHandler CLIENTELabel.Click, AddressOf Me.CLIENTELabel_Click
        '
        'Clv_SessionLabel
        '
        Clv_SessionLabel.AutoSize = True
        Clv_SessionLabel.Location = New System.Drawing.Point(123, 225)
        Clv_SessionLabel.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Clv_SessionLabel.Name = "Clv_SessionLabel"
        Clv_SessionLabel.Size = New System.Drawing.Size(85, 17)
        Clv_SessionLabel.TabIndex = 9
        Clv_SessionLabel.Text = "Clv Session:"
        AddHandler Clv_SessionLabel.Click, AddressOf Me.Clv_SessionLabel_Click
        '
        'StatusLabel
        '
        StatusLabel.AutoSize = True
        StatusLabel.Location = New System.Drawing.Point(503, 182)
        StatusLabel.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        StatusLabel.Name = "StatusLabel"
        StatusLabel.Size = New System.Drawing.Size(52, 17)
        StatusLabel.TabIndex = 11
        StatusLabel.Text = "Status:"
        '
        'Button1
        '
        Me.Button1.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button1.Location = New System.Drawing.Point(1085, 14)
        Me.Button1.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(243, 41)
        Me.Button1.TabIndex = 0
        Me.Button1.Text = "CA&RGAR ARCHIVO"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'OpenFileDialog1
        '
        Me.OpenFileDialog1.FileName = "OpenFileDialog1"
        '
        'CONSULTA_Resultado_OxxoBindingSource
        '
        Me.CONSULTA_Resultado_OxxoBindingSource.DataMember = "CONSULTA_Resultado_Oxxo"
        Me.CONSULTA_Resultado_OxxoBindingSource.DataSource = Me.DataSetEdgar
        '
        'DataSetEdgar
        '
        Me.DataSetEdgar.DataSetName = "DataSetEdgar"
        Me.DataSetEdgar.EnforceConstraints = False
        Me.DataSetEdgar.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'Button2
        '
        Me.Button2.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button2.Location = New System.Drawing.Point(1085, 601)
        Me.Button2.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(243, 39)
        Me.Button2.TabIndex = 5
        Me.Button2.Text = "&SALIR"
        Me.Button2.UseVisualStyleBackColor = True
        '
        'Button3
        '
        Me.Button3.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button3.Location = New System.Drawing.Point(1085, 62)
        Me.Button3.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Button3.Name = "Button3"
        Me.Button3.Size = New System.Drawing.Size(243, 41)
        Me.Button3.TabIndex = 6
        Me.Button3.Text = "&AFECTAR CLIENTES"
        Me.Button3.UseVisualStyleBackColor = True
        '
        'ReciboTextBox
        '
        Me.ReciboTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CONSULTA_Resultado_OxxoBindingSource, "Recibo", True))
        Me.ReciboTextBox.Location = New System.Drawing.Point(561, 414)
        Me.ReciboTextBox.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.ReciboTextBox.Name = "ReciboTextBox"
        Me.ReciboTextBox.Size = New System.Drawing.Size(132, 22)
        Me.ReciboTextBox.TabIndex = 7
        '
        'CLIENTETextBox
        '
        Me.CLIENTETextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CONSULTA_Resultado_OxxoBindingSource, "CLIENTE", True))
        Me.CLIENTETextBox.Location = New System.Drawing.Point(561, 446)
        Me.CLIENTETextBox.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.CLIENTETextBox.Name = "CLIENTETextBox"
        Me.CLIENTETextBox.Size = New System.Drawing.Size(132, 22)
        Me.CLIENTETextBox.TabIndex = 8
        '
        'CONSULTA_Resultado_OxxoTableAdapter
        '
        Me.CONSULTA_Resultado_OxxoTableAdapter.ClearBeforeFill = True
        '
        'CONSULTATablaClv_Session_OxxoBindingSource
        '
        Me.CONSULTATablaClv_Session_OxxoBindingSource.DataMember = "CONSULTATablaClv_Session_Oxxo"
        Me.CONSULTATablaClv_Session_OxxoBindingSource.DataSource = Me.DataSetEdgar
        '
        'CONSULTATablaClv_Session_OxxoTableAdapter
        '
        Me.CONSULTATablaClv_Session_OxxoTableAdapter.ClearBeforeFill = True
        '
        'Clv_SessionTextBox
        '
        Me.Clv_SessionTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CONSULTATablaClv_Session_OxxoBindingSource, "Clv_Session", True))
        Me.Clv_SessionTextBox.Location = New System.Drawing.Point(217, 222)
        Me.Clv_SessionTextBox.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Clv_SessionTextBox.Name = "Clv_SessionTextBox"
        Me.Clv_SessionTextBox.Size = New System.Drawing.Size(132, 22)
        Me.Clv_SessionTextBox.TabIndex = 10
        '
        'StatusTextBox
        '
        Me.StatusTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CONSULTATablaClv_Session_OxxoBindingSource, "Status", True))
        Me.StatusTextBox.Location = New System.Drawing.Point(469, 134)
        Me.StatusTextBox.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.StatusTextBox.Name = "StatusTextBox"
        Me.StatusTextBox.Size = New System.Drawing.Size(132, 22)
        Me.StatusTextBox.TabIndex = 12
        '
        'Button5
        '
        Me.Button5.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button5.Location = New System.Drawing.Point(1085, 110)
        Me.Button5.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Button5.Name = "Button5"
        Me.Button5.Size = New System.Drawing.Size(243, 41)
        Me.Button5.TabIndex = 14
        Me.Button5.Text = "&IMPRIMIR FACTURAS"
        Me.Button5.UseVisualStyleBackColor = True
        '
        'Button6
        '
        Me.Button6.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button6.Location = New System.Drawing.Point(1085, 158)
        Me.Button6.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Button6.Name = "Button6"
        Me.Button6.Size = New System.Drawing.Size(243, 41)
        Me.Button6.TabIndex = 15
        Me.Button6.Text = "IMPRIMIR &DETALLE"
        Me.Button6.UseVisualStyleBackColor = True
        '
        'dgvCargaArchivos
        '
        Me.dgvCargaArchivos.AllowUserToAddRows = False
        Me.dgvCargaArchivos.AllowUserToDeleteRows = False
        Me.dgvCargaArchivos.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgvCargaArchivos.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle1
        Me.dgvCargaArchivos.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvCargaArchivos.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.Clv_Usuario, Me.nombre1, Me.Clv_Session1, Me.Fecha1, Me.Status})
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgvCargaArchivos.DefaultCellStyle = DataGridViewCellStyle2
        Me.dgvCargaArchivos.Location = New System.Drawing.Point(16, 15)
        Me.dgvCargaArchivos.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.dgvCargaArchivos.Name = "dgvCargaArchivos"
        Me.dgvCargaArchivos.ReadOnly = True
        Me.dgvCargaArchivos.RowTemplate.Height = 24
        Me.dgvCargaArchivos.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvCargaArchivos.Size = New System.Drawing.Size(1044, 274)
        Me.dgvCargaArchivos.TabIndex = 16
        '
        'Clv_Usuario
        '
        Me.Clv_Usuario.DataPropertyName = "Clv_Usuario"
        Me.Clv_Usuario.HeaderText = "Clave Usuario"
        Me.Clv_Usuario.Name = "Clv_Usuario"
        Me.Clv_Usuario.ReadOnly = True
        '
        'nombre1
        '
        Me.nombre1.DataPropertyName = "nombre"
        Me.nombre1.HeaderText = "Nombre Usuario"
        Me.nombre1.Name = "nombre1"
        Me.nombre1.ReadOnly = True
        '
        'Clv_Session1
        '
        Me.Clv_Session1.DataPropertyName = "Clv_Session"
        Me.Clv_Session1.HeaderText = "Clv_Session1"
        Me.Clv_Session1.Name = "Clv_Session1"
        Me.Clv_Session1.ReadOnly = True
        Me.Clv_Session1.Visible = False
        '
        'Fecha1
        '
        Me.Fecha1.DataPropertyName = "Fecha"
        Me.Fecha1.HeaderText = "Fecha"
        Me.Fecha1.Name = "Fecha1"
        Me.Fecha1.ReadOnly = True
        '
        'Status
        '
        Me.Status.DataPropertyName = "Status"
        Me.Status.HeaderText = "Status"
        Me.Status.Name = "Status"
        Me.Status.ReadOnly = True
        '
        'dgvDetalleArchivoCargado
        '
        Me.dgvDetalleArchivoCargado.AllowUserToAddRows = False
        Me.dgvDetalleArchivoCargado.AllowUserToDeleteRows = False
        Me.dgvDetalleArchivoCargado.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill
        DataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvDetalleArchivoCargado.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle3
        Me.dgvDetalleArchivoCargado.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvDetalleArchivoCargado.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.CLIENTE, Me.nombre, Me.Clv_Session, Me.Consecutivo, Me.Tienda, Me.Fecha, Me.Hora, Me.Estado, Me.Temp, Me.Monto, Me.Clv_Cliente, Me.Factura, Me.idFactura, Me.Recibo, Me.Detalle, Me.MOVIMIENTO, Me.DIFIMPORTA})
        Me.dgvDetalleArchivoCargado.Cursor = System.Windows.Forms.Cursors.Default
        DataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle4.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgvDetalleArchivoCargado.DefaultCellStyle = DataGridViewCellStyle4
        Me.dgvDetalleArchivoCargado.Location = New System.Drawing.Point(16, 295)
        Me.dgvDetalleArchivoCargado.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.dgvDetalleArchivoCargado.Name = "dgvDetalleArchivoCargado"
        Me.dgvDetalleArchivoCargado.ReadOnly = True
        Me.dgvDetalleArchivoCargado.RowTemplate.Height = 24
        Me.dgvDetalleArchivoCargado.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvDetalleArchivoCargado.Size = New System.Drawing.Size(1313, 300)
        Me.dgvDetalleArchivoCargado.TabIndex = 17
        '
        'CLIENTE
        '
        Me.CLIENTE.DataPropertyName = "CLIENTE"
        Me.CLIENTE.FillWeight = 70.0!
        Me.CLIENTE.HeaderText = "Contrato"
        Me.CLIENTE.Name = "CLIENTE"
        Me.CLIENTE.ReadOnly = True
        '
        'nombre
        '
        Me.nombre.DataPropertyName = "nombre"
        Me.nombre.HeaderText = "Nombre"
        Me.nombre.Name = "nombre"
        Me.nombre.ReadOnly = True
        '
        'Clv_Session
        '
        Me.Clv_Session.DataPropertyName = "Clv_Session"
        Me.Clv_Session.HeaderText = "Clv_Session"
        Me.Clv_Session.Name = "Clv_Session"
        Me.Clv_Session.ReadOnly = True
        Me.Clv_Session.Visible = False
        '
        'Consecutivo
        '
        Me.Consecutivo.DataPropertyName = "Consecutivo"
        Me.Consecutivo.HeaderText = "Consecutivo"
        Me.Consecutivo.Name = "Consecutivo"
        Me.Consecutivo.ReadOnly = True
        Me.Consecutivo.Visible = False
        '
        'Tienda
        '
        Me.Tienda.DataPropertyName = "Tienda"
        Me.Tienda.HeaderText = "Tienda"
        Me.Tienda.Name = "Tienda"
        Me.Tienda.ReadOnly = True
        '
        'Fecha
        '
        Me.Fecha.DataPropertyName = "Fecha"
        Me.Fecha.HeaderText = "Fecha"
        Me.Fecha.Name = "Fecha"
        Me.Fecha.ReadOnly = True
        '
        'Hora
        '
        Me.Hora.DataPropertyName = "Hora"
        Me.Hora.HeaderText = "Hora"
        Me.Hora.Name = "Hora"
        Me.Hora.ReadOnly = True
        Me.Hora.Visible = False
        '
        'Estado
        '
        Me.Estado.DataPropertyName = "Estado"
        Me.Estado.HeaderText = "Estado"
        Me.Estado.Name = "Estado"
        Me.Estado.ReadOnly = True
        '
        'Temp
        '
        Me.Temp.DataPropertyName = "Temp"
        Me.Temp.HeaderText = "Temp"
        Me.Temp.Name = "Temp"
        Me.Temp.ReadOnly = True
        Me.Temp.Visible = False
        '
        'Monto
        '
        Me.Monto.DataPropertyName = "Monto"
        Me.Monto.FillWeight = 70.0!
        Me.Monto.HeaderText = "Monto"
        Me.Monto.Name = "Monto"
        Me.Monto.ReadOnly = True
        '
        'Clv_Cliente
        '
        Me.Clv_Cliente.DataPropertyName = "Clv_Cliente"
        Me.Clv_Cliente.HeaderText = "Clv_Cliente"
        Me.Clv_Cliente.Name = "Clv_Cliente"
        Me.Clv_Cliente.ReadOnly = True
        Me.Clv_Cliente.Visible = False
        '
        'Factura
        '
        Me.Factura.DataPropertyName = "Factura"
        Me.Factura.FillWeight = 70.0!
        Me.Factura.HeaderText = "Factura"
        Me.Factura.Name = "Factura"
        Me.Factura.ReadOnly = True
        '
        'idFactura
        '
        Me.idFactura.DataPropertyName = "idFactura"
        Me.idFactura.HeaderText = "idFactura"
        Me.idFactura.Name = "idFactura"
        Me.idFactura.ReadOnly = True
        Me.idFactura.Visible = False
        '
        'Recibo
        '
        Me.Recibo.DataPropertyName = "Recibo"
        Me.Recibo.HeaderText = "Recibo"
        Me.Recibo.Name = "Recibo"
        Me.Recibo.ReadOnly = True
        Me.Recibo.Visible = False
        '
        'Detalle
        '
        Me.Detalle.DataPropertyName = "Detalle"
        Me.Detalle.HeaderText = "Detalle"
        Me.Detalle.Name = "Detalle"
        Me.Detalle.ReadOnly = True
        Me.Detalle.Visible = False
        '
        'MOVIMIENTO
        '
        Me.MOVIMIENTO.DataPropertyName = "MOVIMIENTO"
        Me.MOVIMIENTO.HeaderText = "Descripción de Saldos"
        Me.MOVIMIENTO.Name = "MOVIMIENTO"
        Me.MOVIMIENTO.ReadOnly = True
        '
        'DIFIMPORTA
        '
        Me.DIFIMPORTA.DataPropertyName = "DIFIMPORTA"
        Me.DIFIMPORTA.HeaderText = "Diferencia de Importe"
        Me.DIFIMPORTA.Name = "DIFIMPORTA"
        Me.DIFIMPORTA.ReadOnly = True
        '
        'VerAcceso2TableAdapter1
        '
        Me.VerAcceso2TableAdapter1.ClearBeforeFill = True
        '
        'Button4
        '
        Me.Button4.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button4.Location = New System.Drawing.Point(1085, 204)
        Me.Button4.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Button4.Name = "Button4"
        Me.Button4.Size = New System.Drawing.Size(243, 41)
        Me.Button4.TabIndex = 18
        Me.Button4.Text = "&CANCELAR ARCHIVO"
        Me.Button4.UseVisualStyleBackColor = True
        '
        'VerAcceso2TableAdapter2
        '
        Me.VerAcceso2TableAdapter2.ClearBeforeFill = True
        '
        'VerAcceso2TableAdapter3
        '
        Me.VerAcceso2TableAdapter3.ClearBeforeFill = True
        '
        'VerAcceso2TableAdapter4
        '
        Me.VerAcceso2TableAdapter4.ClearBeforeFill = True
        '
        'VerAcceso2TableAdapter5
        '
        Me.VerAcceso2TableAdapter5.ClearBeforeFill = True
        '
        'FrmRecepOxxo
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.AutoScroll = True
        Me.BackColor = System.Drawing.Color.WhiteSmoke
        Me.ClientSize = New System.Drawing.Size(1344, 655)
        Me.Controls.Add(Me.Button4)
        Me.Controls.Add(Me.dgvDetalleArchivoCargado)
        Me.Controls.Add(Me.dgvCargaArchivos)
        Me.Controls.Add(Me.Button6)
        Me.Controls.Add(Me.Button5)
        Me.Controls.Add(StatusLabel)
        Me.Controls.Add(Me.StatusTextBox)
        Me.Controls.Add(Me.Button3)
        Me.Controls.Add(Clv_SessionLabel)
        Me.Controls.Add(Me.Clv_SessionTextBox)
        Me.Controls.Add(CLIENTELabel)
        Me.Controls.Add(Me.CLIENTETextBox)
        Me.Controls.Add(ReciboLabel)
        Me.Controls.Add(Me.ReciboTextBox)
        Me.Controls.Add(Me.Button2)
        Me.Controls.Add(Me.Button1)
        Me.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.MaximizeBox = False
        Me.Name = "FrmRecepOxxo"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Recepción del Archivo de Oxxo"
        CType(Me.CONSULTA_Resultado_OxxoBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DataSetEdgar, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CONSULTATablaClv_Session_OxxoBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgvCargaArchivos, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgvDetalleArchivoCargado, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents OpenFileDialog1 As System.Windows.Forms.OpenFileDialog
    Friend WithEvents DataSetEdgar As softvFacturacion.DataSetEdgar
    Friend WithEvents CONSULTA_Resultado_OxxoBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents CONSULTA_Resultado_OxxoTableAdapter As softvFacturacion.DataSetEdgarTableAdapters.CONSULTA_Resultado_OxxoTableAdapter
    Friend WithEvents Button2 As System.Windows.Forms.Button
    Friend WithEvents Button3 As System.Windows.Forms.Button
    Friend WithEvents ReciboTextBox As System.Windows.Forms.TextBox
    Friend WithEvents CLIENTETextBox As System.Windows.Forms.TextBox
    Friend WithEvents CONSULTATablaClv_Session_OxxoBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents CONSULTATablaClv_Session_OxxoTableAdapter As softvFacturacion.DataSetEdgarTableAdapters.CONSULTATablaClv_Session_OxxoTableAdapter
    Friend WithEvents Clv_SessionTextBox As System.Windows.Forms.TextBox
    Friend WithEvents StatusTextBox As System.Windows.Forms.TextBox
    Friend WithEvents Button5 As System.Windows.Forms.Button
    Friend WithEvents Button6 As System.Windows.Forms.Button
    Friend WithEvents dgvCargaArchivos As System.Windows.Forms.DataGridView
    Friend WithEvents dgvDetalleArchivoCargado As System.Windows.Forms.DataGridView
    Friend WithEvents VerAcceso2TableAdapter1 As softvFacturacion.ProcedimientosArnoldo3TableAdapters.VerAcceso2TableAdapter
    Friend WithEvents Button4 As System.Windows.Forms.Button
    Friend WithEvents Clv_Usuario As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents nombre1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Clv_Session1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Fecha1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Status As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents VerAcceso2TableAdapter2 As softvFacturacion.ProcedimientosArnoldo3TableAdapters.VerAcceso2TableAdapter
    Friend WithEvents CLIENTE As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents nombre As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Clv_Session As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Consecutivo As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Tienda As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Fecha As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Hora As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Estado As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Temp As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Monto As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Clv_Cliente As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Factura As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents idFactura As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Recibo As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Detalle As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents MOVIMIENTO As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DIFIMPORTA As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents VerAcceso2TableAdapter3 As softvFacturacion.ProcedimientosArnoldo3TableAdapters.VerAcceso2TableAdapter
    Friend WithEvents VerAcceso2TableAdapter4 As softvFacturacion.ProcedimientosArnoldo3TableAdapters.VerAcceso2TableAdapter
    Friend WithEvents VerAcceso2TableAdapter5 As softvFacturacion.ProcedimientosArnoldo3TableAdapters.VerAcceso2TableAdapter
End Class
