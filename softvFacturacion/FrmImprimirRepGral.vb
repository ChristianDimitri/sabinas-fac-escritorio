
Imports CrystalDecisions.Shared
Imports CrystalDecisions.CrystalReports.Engine
Imports System.Data.SqlClient

Public Class FrmImprimirRepGral
    '1
    Public contratoCobroMaterial As Long
    Public fechaIniCobroMaterial, fechaFinCobroMaterial As Date
    Public opCobroMaterial As Integer
    Public entraReporte, pendientesCobroMaterial, saldadosCobroMaterial As Boolean

    Private customersByCityReport As ReportDocument
    Private op As String = Nothing

    Private Function uspChecaSiTieneArqueo(ByVal prmCajera As String, ByVal prmFechaArqueo As Date) As Boolean
        ControlEfectivoClass.limpiaParametros()

        ControlEfectivoClass.CreateMyParameter("@cajera", SqlDbType.VarChar, prmCajera, 5)
        ControlEfectivoClass.CreateMyParameter("@fechaArqueo", SqlDbType.DateTime, prmFechaArqueo)

        uspChecaSiTieneArqueo = ControlEfectivoClass.InsertaBol("uspChecaSiTieneArqueo")
    End Function

    Private Sub ConfigureCrystalReportsNotasCredito1()
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        Dim ba As Boolean
        Dim opc1, opc2 As String
        Dim busfac As New NewsoftvDataSet2TableAdapters.BusFacFiscalTableAdapter
        Dim bfac As New NewsoftvDataSet2.BusFacFiscalDataTable
        customersByCityReport = New ReportDocument
        Dim connectionInfo As New ConnectionInfo
        '"Data Source=SERVER-2003\SQLEXPRESS;Initial Catalog=Newsoftv;Persist Security Info"& _ 
        '    "=True;User ID=DeSistema;Password=1975huli")
        'connectionInfo.ServerName = GloServerName
        'connectionInfo.DatabaseName = GloDatabaseName
        'connectionInfo.UserID = GloUserID
        'connectionInfo.Password = GloPassword

        Dim reportPath As String = Nothing

        'If GloImprimeTickets = False Then
        ' reportPath = Application.StartupPath + "\Reportes\" + "ReporteCajas.rpt"
        'Else
        reportPath = RutaReportes + "\ReporteNotasdeCredito.rpt"

        'busfac.Connection = CON
        'busfac.Fill(bfac, Clv_Factura, identi)
        'If IdSistema = "SA" And facnormal = True And identi > 0 Then
        '    reportPath = RutaReportes + "\ReporteCajasTvRey.rpt"
        '    ba = True
        'ElseIf IdSistema = "TO" And facnormal = True And identi > 0 Then
        '    reportPath = RutaReportes + "\ReporteCajasCabSta.rpt"
        '    ba = True

        'Else
        '    reportPath = RutaReportes + "\ReporteCajasTickets_2.rpt"
        'End If

        'End If

        Dim DS As New DataSet
        DS.Clear()
        BaseII.limpiaParametros()

        BaseII.CreateMyParameter("@Clave", SqlDbType.BigInt, gloClvNota)
        BaseII.CreateMyParameter("@Clv_nota_Ini", SqlDbType.BigInt, 0)
        BaseII.CreateMyParameter("@Clv_nota_Fin", SqlDbType.BigInt, 0)
        BaseII.CreateMyParameter("@Fecha_Ini", SqlDbType.DateTime, "01/01/1900")
        BaseII.CreateMyParameter("@Fecha_Fin", SqlDbType.DateTime, "01/01/1900")
        BaseII.CreateMyParameter("@op", SqlDbType.Int, 0)

        Dim listatablas As New List(Of String)
        listatablas.Add("ReportesNotasDeCredito")
        listatablas.Add("DetFacturas_NotadeCredito")
        listatablas.Add("Notas_de_Credito")
        listatablas.Add("Rel_NotaCredito_ConceptosServ")

        DS = BaseII.ConsultaDS("ReportesNotasDeCredito", listatablas)

        customersByCityReport.Load(reportPath)
        SetDBReport(DS, customersByCityReport)

        'customersByCityReport.Load(reportPath)
        ''If GloImprimeTickets = False Then
        ''SetDBLogonForSubReport(connectionInfo, customersByCityReport)
        '' End If
        'SetDBLogonForReport(connectionInfo, customersByCityReport)
        '@Clv_Factura 
        customersByCityReport.SetParameterValue(0, gloClvNota)
        '@Clv_Factura_Ini
        customersByCityReport.SetParameterValue(1, "0")
        '@Clv_Factura_Fin
        customersByCityReport.SetParameterValue(2, "0")
        '@Fecha_Ini
        customersByCityReport.SetParameterValue(3, "01/01/1900")
        '@Fecha_Fin
        customersByCityReport.SetParameterValue(4, "01/01/1900")
        '@op
        customersByCityReport.SetParameterValue(5, "0")
        'If GloImprimeTickets = True Then
        If IdSistema = "VA" Then
            opc1 = "Devoluci�n en Efecitvo"
            opc2 = "Devoluci�n en Efectivo:"
        Else
            opc1 = "Nota de Cr�dito"
            opc2 = "Nota de Cr�dito :"
        End If
        If ba = False Then
            customersByCityReport.DataDefinition.FormulaFields("Titulo").Text = "'" & opc1 & "'"
            customersByCityReport.DataDefinition.FormulaFields("Clave").Text = "'" & opc2 & "'"
            customersByCityReport.DataDefinition.FormulaFields("Empresa").Text = "'" & GloEmpresa & "'"
            customersByCityReport.DataDefinition.FormulaFields("DireccionEmpresa").Text = "'" & GloDireccionEmpresa & "'"
            customersByCityReport.DataDefinition.FormulaFields("Colonia_CpEmpresa").Text = "'" & GloColonia_CpEmpresa & "'"
            customersByCityReport.DataDefinition.FormulaFields("CiudadEmpresa").Text = "'" & GloCiudadEmpresa & "'"
            customersByCityReport.DataDefinition.FormulaFields("RfcEmpresa").Text = "'" & GloRfcEmpresa & "'"
            customersByCityReport.DataDefinition.FormulaFields("TelefonoEmpresa").Text = "'" & GloTelefonoEmpresa & "'"
            If locoprepnotas = 0 Then
                customersByCityReport.DataDefinition.FormulaFields("Copia").Text = "'Copia'"
            ElseIf locoprepnotas = 1 Then
                customersByCityReport.DataDefinition.FormulaFields("Copia").Text = "'Original'"
            End If
        End If

        'If (IdSistema = "TO" Or IdSistema = "SA") Then 'And facnormal = True And identi > 0 
        '    customersByCityReport.PrintOptions.PrinterName = impresorafiscal
        'Else

        'customersByCityReport.PrintOptions.PrinterName = LocImpresoraTickets
        '' End If
        CrystalReportViewer1.ShowGroupTreeButton = False
        CrystalReportViewer1.ToolPanelView = CrystalDecisions.Windows.Forms.ToolPanelViewType.None
        CrystalReportViewer1.ReportSource = customersByCityReport
        CrystalReportViewer1.ShowPrintButton = True

        'customersByCityReport.PrintToPrinter(1, True, 1, 1)
        CON.Close()
        'If GloOpFacturas = 3 Then
        'CrystalReportViewer1.ShowExportButton = False
        'CrystalReportViewer1.ShowPrintButton = False
        'CrystalReportViewer1.ShowRefreshButton = False
        'End If
        'SetDBLogonForReport2(connectionInfo)
        customersByCityReport = Nothing
    End Sub

    Private Sub ConfigureCrystalReports(ByVal Clave As Long, ByVal Titulo As String, ByVal SubTitulo As String)
        'Try


        '    customersByCityReport = New ReportDocument
        '    Dim connectionInfo As New ConnectionInfo
        '    '"Data Source=SERVER-2003\SQLEXPRESS;Initial Catalog=Newsoftv;Persist Security Info"& _ 
        '    '    "=True;User ID=DeSistema;Password=1975huli")
        '    connectionInfo.ServerName = GloServerName
        '    connectionInfo.DatabaseName = GloDatabaseName
        '    connectionInfo.UserID = GloUserID
        '    connectionInfo.Password = GloPassword
        '    Dim mySelectFormula As String = Nothing
        '    Dim reportPath As String = Nothing
        '    reportPath = RutaReportes + "\ReporteListadoPreliminar.rpt"
        '    customersByCityReport.Load(reportPath)
        '    SetDBLogonForReport(connectionInfo, customersByCityReport)
        '    '@Clv_SessionBancos
        '    customersByCityReport.SetParameterValue(0, CStr(GloClv_SessionBancos))
        '    '@Op
        '    customersByCityReport.SetParameterValue(1, "0")

        '    mySelectFormula = "Listado de Clientes con Cargo Autom�tico"
        '    customersByCityReport.DataDefinition.FormulaFields("Titulo").Text = "'" & mySelectFormula & "'"
        '    mySelectFormula = " "
        '    customersByCityReport.DataDefinition.FormulaFields("SubTitulo").Text = "'" & mySelectFormula & "'"

        '    CrystalReportViewer1.ShowGroupTreeButton = False
        '    CrystalReportViewer1.ToolPanelView = CrystalDecisions.Windows.Forms.ToolPanelViewType.None
        '    CrystalReportViewer1.ReportSource = customersByCityReport
        '    'Me.CrystalReportViewer1.RefreshReport()
        '    SetDBLogonForReport2(connectionInfo)
        '    customersByCityReport = Nothing
        '    GloReporte = 0
        'Catch ex As Exception
        '    System.Windows.Forms.MessageBox.Show(ex.Message)
        'End Try
        Try
            customersByCityReport = New ReportDocument
            Dim connectionInfo As New ConnectionInfo
            Dim mySelectFormula As String = Nothing
            Dim reportPath As String = Nothing
            reportPath = RutaReportes + "\ReporteListadoPreliminar.rpt"

            Dim DS As New DataSet
            DS.Clear()
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@CLV_SESSIONBANCOS", SqlDbType.BigInt, GloClv_SessionBancos)
            BaseII.CreateMyParameter("@OP", SqlDbType.BigInt, 0)
            Dim listatablas As New List(Of String)
            listatablas.Add("ReporteListadoPreliminar")

            DS = BaseII.ConsultaDS("REPORTE_LISTADOPRELIMINAR", listatablas)

            customersByCityReport.Load(reportPath)
            SetDBReport(DS, customersByCityReport)
            CrystalReportViewer1.ReportSource = customersByCityReport
            SetDBLogonForReport2(connectionInfo)
            customersByCityReport = Nothing
            GloReporte = 0
            '    Dim mySelectFormula As String = Nothing
            '    Dim reportPath As String = Nothing
            '    reportPath = RutaReportes + "\ReporteListadoPreliminar.rpt"
            '    customersByCityReport.Load(reportPath)
            '    SetDBLogonForReport(connectionInfo, customersByCityReport)
            '    '@Clv_SessionBancos
            '    customersByCityReport.SetParameterValue(0, CStr(GloClv_SessionBancos))
            '    '@Op
            '    customersByCityReport.SetParameterValue(1, "0")

            mySelectFormula = "Listado de Clientes con Cargo Autom�tico"
            customersByCityReport.DataDefinition.FormulaFields("Titulo").Text = "'" & mySelectFormula & "'"
            mySelectFormula = " "
            customersByCityReport.DataDefinition.FormulaFields("SubTitulo").Text = "'" & mySelectFormula & "'"

            '    CrystalReportViewer1.ShowGroupTreeButton = False
            '    CrystalReportViewer1.ToolPanelView = CrystalDecisions.Windows.Forms.ToolPanelViewType.None
            '    CrystalReportViewer1.ReportSource = customersByCityReport
            '    'Me.CrystalReportViewer1.RefreshReport()
            '    SetDBLogonForReport2(connectionInfo)
            '    customersByCityReport = Nothing
            '    GloReporte = 0
        Catch ex As Exception

        End Try
    End Sub

    Private Sub ConfigureCrystalReportsOxxo(ByVal Clave As Long, ByVal Titulo As String, ByVal SubTitulo As String)
        Try


            customersByCityReport = New ReportDocument
            Dim connectionInfo As New ConnectionInfo
            '"Data Source=SERVER-2003\SQLEXPRESS;Initial Catalog=Newsoftv;Persist Security Info"& _ 
            '    "=True;User ID=DeSistema;Password=1975huli")
            'connectionInfo.ServerName = GloServerName
            'connectionInfo.DatabaseName = GloDatabaseName
            'connectionInfo.UserID = GloUserID
            'connectionInfo.Password = GloPassword
            Dim mySelectFormula As String = Nothing
            Dim reportPath As String = Nothing
            reportPath = RutaReportes + "\RepListadoOxxo_1.rpt"
            'customersByCityReport.Load(reportPath)
            'SetDBLogonForReport(connectionInfo, customersByCityReport)
            '@Clv_SessionBancos

            Dim DS As New DataSet
            DS.Clear()
            BaseII.limpiaParametros()

            BaseII.CreateMyParameter("@Clv_Session", SqlDbType.BigInt, GloClv_SessionBancos)


            Dim listatablas As New List(Of String)
            listatablas.Add("CONSULTA_Resultado_Oxxo")

            DS = BaseII.ConsultaDS("CONSULTA_Resultado_Oxxo", listatablas)

            customersByCityReport.Load(reportPath)
            SetDBReport(DS, customersByCityReport)

            customersByCityReport.SetParameterValue(0, CStr(GloClv_SessionBancos))

            mySelectFormula = "Listado de Clientes (Proceso de Oxxo)"
            customersByCityReport.DataDefinition.FormulaFields("Titulo").Text = "'" & GloEmpresa & "'"
            mySelectFormula = " "
            customersByCityReport.DataDefinition.FormulaFields("SubTitulo").Text = "'" & GloNomSucursal & "'"

            CrystalReportViewer1.ShowGroupTreeButton = False
            CrystalReportViewer1.ToolPanelView = CrystalDecisions.Windows.Forms.ToolPanelViewType.None
            CrystalReportViewer1.ReportSource = customersByCityReport
            'Me.CrystalReportViewer1.RefreshReport()
            SetDBLogonForReport2(connectionInfo)
            customersByCityReport = Nothing
            GloReporte = 0
        Catch ex As Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub ConfigureCrystalReportsParciales(ByVal Consecutivo As Long)
        customersByCityReport = New ReportDocument
        'Dim connectionInfo As New ConnectionInfo
        ''"Data Source=SERVER-2003\SQLEXPRESS;Initial Catalog=Newsoftv;Persist Security Info"& _ 
        ''    "=True;User ID=DeSistema;Password=1975huli")
        'connectionInfo.ServerName = GloServerName
        'connectionInfo.DatabaseName = GloDataBaseName
        'connectionInfo.UserID = GloUserID
        'connectionInfo.Password = GloPassword
        Dim mySelectFormula As String = Nothing

        Dim reportPath As String = Nothing
        reportPath = RutaReportes + "\Reporte_EntregaParcial.rpt"


        Dim DS As New DataSet
        DS.Clear()
        BaseII.limpiaParametros()

        BaseII.CreateMyParameter("@op", SqlDbType.Int, 0)
        BaseII.CreateMyParameter("@Consecutivo", SqlDbType.BigInt, Consecutivo)

        Dim listatablas As New List(Of String)
        listatablas.Add("Reporte_EntregasParciales")

        DS = BaseII.ConsultaDS("Reporte_EntregasParciales", listatablas)

        customersByCityReport.Load(reportPath)
        SetDBReport(DS, customersByCityReport)



        'MsgBox(reportPath)
        'customersByCityReport.Load(reportPath)
        'SetDBLogonForReport(connectionInfo, customersByCityReport)
        'SetDBLogonForSubReport(connectionInfo, customersByCityReport)
        '@Op
        customersByCityReport.SetParameterValue(0, 0)
        '@Consecutivo
        customersByCityReport.SetParameterValue(1, CStr(Consecutivo))
        '@Supervisor
        customersByCityReport.SetParameterValue(2, locnomsupervisor)

        'mySelectFormula = "Listado de Clientes con Cargo Autom�tico"
        'customersByCityReport.DataDefinition.FormulaFields("Titulo").Text = "'" & mySelectFormula & "'"
        'mySelectFormula = " "
        'customersByCityReport.DataDefinition.FormulaFields("SubTitulo").Text = "'" & mySelectFormula & "'"
        customersByCityReport.DataDefinition.FormulaFields("Empresa").Text = "'" & LocNomEmpresa & "'"
        customersByCityReport.DataDefinition.FormulaFields("Sucursal").Text = "'" & GloNomSucursal & "'"

        CrystalReportViewer1.ShowGroupTreeButton = False
        CrystalReportViewer1.ToolPanelView = CrystalDecisions.Windows.Forms.ToolPanelViewType.None
        CrystalReportViewer1.ReportSource = customersByCityReport
        'Me.CrystalReportViewer1.RefreshReport()
        'SetDBLogonForReport2(connectionInfo)
        customersByCityReport = Nothing
        GloReporte = 0
    End Sub

    Private Sub ConfigureCrystalDesglose2(ByVal Consecutivo As Long)
        customersByCityReport = New ReportDocument
        'Dim connectionInfo As New ConnectionInfo
        '"Data Source=SERVER-2003\SQLEXPRESS;Initial Catalog=Newsoftv;Persist Security Info"& _ 
        '    "=True;User ID=DeSistema;Password=1975huli")
        'connectionInfo.ServerName = GloServerName
        'connectionInfo.DatabaseName = GloDataBaseName
        'connectionInfo.UserID = GloUserID
        'connectionInfo.Password = GloPassword
        Dim mySelectFormula As String = Nothing

        Dim reportPath As String = Nothing
        reportPath = RutaReportes + "\Reporte_DesgloseMoneda.rpt"

        Dim DS As New DataSet
        DS.Clear()
        BaseII.limpiaParametros()

        BaseII.CreateMyParameter("@op", SqlDbType.Int, 0)
        BaseII.CreateMyParameter("@Consecutivo", SqlDbType.BigInt, Consecutivo)

        Dim listatablas As New List(Of String)
        listatablas.Add("Reporte_DesgloseMoneda")
        listatablas.Add("Rel_Desglose_Dolares")
        DS = BaseII.ConsultaDS("Reporte_DesgloseMoneda", listatablas)

        customersByCityReport.Load(reportPath)
        'customersByCityReport.SetDataSource = DS
        customersByCityReport.SetDataSource(DS)


        'SetDBReport(DS, customersByCityReport)

        'MsgBox(reportPath)
        'customersByCityReport.Load(reportPath)
        'SetDBLogonForReport(connectionInfo, customersByCityReport)
        'SetDBLogonForSubReport(connectionInfo, customersByCityReport)
        '@Op
        'customersByCityReport.SetParameterValue(0, 0)
        '@Consecutivo
        'customersByCityReport.SetParameterValue(1, CStr(Consecutivo))

        customersByCityReport.DataDefinition.FormulaFields("Empresa").Text = "'" & LocNomEmpresa & "'"

        CrystalReportViewer1.ShowGroupTreeButton = False
        CrystalReportViewer1.ToolPanelView = CrystalDecisions.Windows.Forms.ToolPanelViewType.None
        CrystalReportViewer1.ReportSource = customersByCityReport

        customersByCityReport = Nothing
        GloReporte = 0
    End Sub

    Private Sub GastosArqueo(ByVal FECHA As Date, ByVal CAJERA As String)
        Dim CON As New SqlConnection(MiConexion)

        LocTransferencia = 0
        LocTotTransferencia = 0

        LocTarjetaDebito = 0
        LocTotTarjetaDebito = 0

        Dim CMD As New SqlCommand("GastosArqueo", CON)
        CMD.CommandType = CommandType.StoredProcedure
        CMD.Parameters.AddWithValue("@Fecha", FECHA)
        CMD.Parameters.AddWithValue("@NomCajera", CAJERA)

        Dim READER As SqlDataReader

        Try
            CON.Open()
            READER = CMD.ExecuteReader()

            While READER.Read
                LocGastos = READER(0).ToString()
                LocSaldoAnterior = READER(1).ToString()
                LocTransferencia = READER(2).ToString()
                LocTotTransferencia = READER(3).ToString()
                LocTarjetaDebito = READER(4).ToString()
                LocTotTarjetaDebito = READER(5).ToString()
            End While
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Information)
        Finally
            CON.Close()
            CON.Dispose()
        End Try
    End Sub

    Private Sub ConfigureCrystalArqueo(ByVal Fecha As Date, ByVal Cajera As String)
        Try

            customersByCityReport = New ReportDocument
            Dim basededatos As String = Nothing
            Dim Total As Double = 0
            Dim Efectivo_Entergas As Double = 0
            Dim Tarjeta As Double = 0
            Dim Cheques As Double = 0


            '"Data Source=SERVER-2003\SQLEXPRESS;Initial Catalog=Newsoftv;Persist Security Info"& _ 
            '    "=True;User ID=DeSistema;Password=1975huli")
            'connectionInfo.ServerName = GloServerName
            'connectionInfo.DatabaseName = GloDatabaseName
            'connectionInfo.UserID = GloUserID
            'connectionInfo.Password = GloPassword
            Dim mySelectFormula As String = Nothing
            'Dim Parametro As String, Parametro1 As String = nothing
            Dim sumaefectivo As Long = 0
            Dim CON As New SqlConnection(MiConexion)
            CON.Open()
            Me.SumaArqueoTableAdapter.Connection = CON
            Me.SumaArqueoTableAdapter.Fill(Me.Procedimientos_arnoldo.SumaArqueo, Fecha_ini, GloCajera, LocDesglose, LocParciales, LocAuto, LocTarjeta, LocCheque, LocEfectivo, Efectivo_Entergas, Tarjeta, Cheques)
            Me.Dame_base_datosTableAdapter.Connection = CON
            Me.Dame_base_datosTableAdapter.Fill(Me.Procedimientos_arnoldo.Dame_base_datos, basededatos)
            CON.Close()

            GastosArqueo(Fecha_ini, GloCajera)

            Dim reportPath As String = Nothing
            reportPath = RutaReportes + "\Reporte_ArqueoPrincipal_3.rpt"

            'Select Case basededatos
            '    Case "SA"
            '        reportPath = RutaReportes + "\Reporte_ArqueoPrincipal_3.rpt"
            '    Case "JI"
            '        reportPath = RutaReportes + "\Reporte_ArqueoPrincipal_2_Ji.rpt"
            '    Case "SP"
            '        reportPath = RutaReportes + "\Reporte_ArqueoPrincipal_3_San.rpt"
            '    Case "PA"
            '        reportPath = RutaReportes + "\Reporte_ArqueoPrincipal_3_PA.rpt"
            '    Case "CU"
            '        reportPath = RutaReportes + "\Reporte_ArqueoPrincipal_3_CU.rpt"
            '    Case Else
            '        reportPath = RutaReportes + "\Reporte_ArqueoPrincipal_3.rpt"
            'End Select



            ReporteArqueoPrincipalXsd_3(Fecha_ini, GloCajera, reportPath)


            mySelectFormula = "Arqueo de Caja"
            customersByCityReport.DataDefinition.FormulaFields("Empresa").Text = "'" & GloEmpresaPRincipal & "'"
            customersByCityReport.DataDefinition.FormulaFields("Titulo").Text = "'" & mySelectFormula & "'"
            mySelectFormula = "Sucursal : " & GloNomSucursal
            customersByCityReport.DataDefinition.FormulaFields("Subtitulo").Text = "" ' "'Distribuidor: " & "'"
            customersByCityReport.DataDefinition.FormulaFields("TotalEfectivoParciales").Text = Efectivo_Entergas.ToString
            customersByCityReport.DataDefinition.FormulaFields("Efectivo").Text = LocEfectivo.ToString
            customersByCityReport.DataDefinition.FormulaFields("TotalTarjeta").Text = Tarjeta.ToString
            customersByCityReport.DataDefinition.FormulaFields("TarjetaCredito").Text = LocTarjeta.ToString
            customersByCityReport.DataDefinition.FormulaFields("TotalCheques").Text = Cheques.ToString
            customersByCityReport.DataDefinition.FormulaFields("Cheques").Text = LocCheque.ToString
            customersByCityReport.DataDefinition.FormulaFields("CargoAutomatico").Text = LocAuto.ToString
            customersByCityReport.DataDefinition.FormulaFields("FondoF").Text = "0"
            customersByCityReport.DataDefinition.FormulaFields("Gastos").Text = LocGastos.ToString
            customersByCityReport.DataDefinition.FormulaFields("SaldoAnterior").Text = LocSaldoAnterior.ToString
            customersByCityReport.DataDefinition.FormulaFields("Transferencia").Text = LocTransferencia.ToString
            customersByCityReport.DataDefinition.FormulaFields("TotalTransferencia").Text = LocTotTransferencia.ToString
            customersByCityReport.DataDefinition.FormulaFields("TarjetaDebito").Text = LocTarjetaDebito.ToString
            customersByCityReport.DataDefinition.FormulaFields("TotalTarjetaDebito").Text = LocTotTarjetaDebito.ToString


            Total = Efectivo_Entergas + Tarjeta + Cheques + LocAuto + LocTransferencia + LocTarjetaDebito
            'MsgBox(Total)
            customersByCityReport.DataDefinition.FormulaFields("Total").Text = Total.ToString
            customersByCityReport.DataDefinition.FormulaFields("FechaGenerado").Text = "'" & Fecha_ini & "'"
            customersByCityReport.DataDefinition.FormulaFields("NomCajera").Text = "'" & GLONOMCAJERAARQUEO & "'"

            CrystalReportViewer1.ShowGroupTreeButton = False
            CrystalReportViewer1.ToolPanelView = CrystalDecisions.Windows.Forms.ToolPanelViewType.None
            CrystalReportViewer1.ReportSource = customersByCityReport

            'customersByCityReport = Nothing
        Catch ex As Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub
    'Private Sub ConfigureCrystalArqueo(ByVal Fecha As Date, ByVal Cajera As String)
    '    Try

    '        customersByCityReport = New ReportDocument
    '        'Dim connectionInfo As New ConnectionInfo
    '        Dim basededatos As String = Nothing
    '        Dim Total As Double = 0
    '        Dim Efectivo_Entergas As Double = 0
    '        Dim Tarjeta As Double = 0
    '        Dim Cheques As Double = 0
    '        '"Data Source=SERVER-2003\SQLEXPRESS;Initial Catalog=Newsoftv;Persist Security Info"& _ 
    '        '    "=True;User ID=DeSistema;Password=1975huli")
    '        'connectionInfo.ServerName = GloServerName
    '        'connectionInfo.DatabaseName = GloDatabaseName
    '        'connectionInfo.UserID = GloUserID
    '        'connectionInfo.Password = GloPassword
    '        Dim mySelectFormula As String = Nothing
    '        'Dim Parametro As String, Parametro1 As String = nothing
    '        Dim sumaefectivo As Long = 0
    '        Dim CON As New SqlConnection(MiConexion)
    '        CON.Open()
    '        Me.SumaArqueoTableAdapter.Connection = CON
    '        Me.SumaArqueoTableAdapter.Fill(Me.Procedimientos_arnoldo.SumaArqueo, Fecha_ini, GloCajera, LocDesglose, LocParciales, LocAuto, LocTarjeta, LocCheque, LocEfectivo, Efectivo_Entergas, Tarjeta, Cheques)
    '        Me.Dame_base_datosTableAdapter.Connection = CON
    '        Me.Dame_base_datosTableAdapter.Fill(Me.Procedimientos_arnoldo.Dame_base_datos, basededatos)
    '        CON.Close()
    '        Dim reportPath As String = Nothing

    '        'reportPath = RutaReportes + "\Reporte_ArqueoPrincipal_3.rpt"

    '        'Dim report As New ReportDocument()
    '        'Dim connection As IConnectionInfo
    '        'Dim serverName1 As String = GloServerName
    '        'Dim userID As String = GloUserID
    '        'Dim password As String = GloPassword

    '        'customersByCityReport.Load(reportPath)

    '        '' Establecer conexi�n con base de datos al informe principal
    '        'For Each connection In customersByCityReport.DataSourceConnections
    '        '    Select Case connection.ServerName
    '        '        Case serverName1
    '        '            connection.SetLogon(userID, password)
    '        '    End Select
    '        'Next

    '        '' Establecer conexi�n al subinforme
    '        'Dim subreport As ReportDocument
    '        'For Each subreport In customersByCityReport.Subreports
    '        '    For Each connection In subreport.DataSourceConnections
    '        '        connection.SetLogon(userID, password)
    '        '    Next
    '        'Next



    '        ''SetDBLogonForReport(connectionInfo, customersByCityReport)
    '        'SetDBLogonForSubReport2(connectionInfo, customersByCityReport)


    '        ''@Fecha
    '        'customersByCityReport.SetParameterValue(0, Fecha_ini)
    '        ''@Cajera
    '        'customersByCityReport.SetParameterValue(1, GloCajera)






    '        '' SetDBLogonForSubReport(connectionInfo, customersByCityReport)




    '        reportPath = RutaReportes + "\Reporte_ArqueoPrincipal_3.rpt"

    '        ReporteArqueoPrincipalXsd_3(Fecha_ini, GloCajera, reportPath)


    '        mySelectFormula = "Arqueo de Caja"
    '        customersByCityReport.DataDefinition.FormulaFields("Empresa").Text = "'" & LocNomEmpresa & "'"
    '        customersByCityReport.DataDefinition.FormulaFields("Titulo").Text = "'" & mySelectFormula & "'"
    '        mySelectFormula = "Sucursal : " & GloNomSucursal
    '        customersByCityReport.DataDefinition.FormulaFields("Subtitulo").Text = "'" & mySelectFormula & "'"
    '        customersByCityReport.DataDefinition.FormulaFields("TotalEfectivoParciales").Text = Efectivo_Entergas.ToString
    '        customersByCityReport.DataDefinition.FormulaFields("Efectivo").Text = LocEfectivo.ToString
    '        customersByCityReport.DataDefinition.FormulaFields("TotalTarjeta").Text = Tarjeta.ToString
    '        customersByCityReport.DataDefinition.FormulaFields("TarjetaCredito").Text = LocTarjeta.ToString
    '        customersByCityReport.DataDefinition.FormulaFields("TotalCheques").Text = Cheques.ToString
    '        customersByCityReport.DataDefinition.FormulaFields("Cheques").Text = LocCheque.ToString
    '        customersByCityReport.DataDefinition.FormulaFields("CargoAutomatico").Text = LocAuto.ToString
    '        customersByCityReport.DataDefinition.FormulaFields("FondoF").Text = "0"

    '        Total = Efectivo_Entergas + Tarjeta + Cheques + LocAuto
    '        'MsgBox(Total)
    '        customersByCityReport.DataDefinition.FormulaFields("Total").Text = Total.ToString
    '        'Total = LocDesglose + LocParciales
    '        ' customersByCityReport.DataDefinition.FormulaFields("Total2").Text = Total.ToString
    '        customersByCityReport.DataDefinition.FormulaFields("FechaGenerado").Text = "'" & Fecha_ini & "'"
    '        customersByCityReport.DataDefinition.FormulaFields("NomCajera").Text = "'" & GLONOMCAJERAARQUEO & "'"
    '        CrystalReportViewer1.ReportSource = customersByCityReport
    '        GloReporte = 0
    '        'customersByCityReport = Nothing
    '    Catch ex As Exception
    '        System.Windows.Forms.MessageBox.Show(ex.Message)
    '    End Try
    'End Sub

    Private Sub SetDBLogonForReport(ByVal myConnectionInfo As ConnectionInfo, ByVal myReportDocument As ReportDocument)
        customersByCityReport.DataSourceConnections(0).SetConnection(GloServerName, GloDatabaseName, GloUserID, GloPassword)
        'customersByCityReport.SetDatabaseLogon(GloUserID, GloPassword, GloServerName, GloDatabaseName)

        Dim myTables As Tables = myReportDocument.Database.Tables
        Dim myTable As CrystalDecisions.CrystalReports.Engine.Table
        For Each myTable In myTables
            Dim myTableLogonInfo As TableLogOnInfo = myTable.LogOnInfo
            myTableLogonInfo.ConnectionInfo = myConnectionInfo
            myTable.ApplyLogOnInfo(myTableLogonInfo)
            myTable.Location = GloDatabaseName & ".dbo." & myTable.Location.Substring(myTable.Location.LastIndexOf(".") + 1)
        Next
    End Sub
    Private Sub SetDBLogonForSubReport2(ByVal myConnectionInfo As ConnectionInfo, ByVal myReportDocument As ReportDocument)
        customersByCityReport.Subreports(0).SetDatabaseLogon(GloUserID, GloPassword, GloServerName, GloDatabaseName)
        customersByCityReport.Subreports(1).SetDatabaseLogon(GloUserID, GloPassword, GloServerName, GloDatabaseName)
        customersByCityReport.Subreports(2).SetDatabaseLogon(GloUserID, GloPassword, GloServerName, GloDatabaseName)
        customersByCityReport.Subreports(3).SetDatabaseLogon(GloUserID, GloPassword, GloServerName, GloDatabaseName)
        'customersByCityReport.Subreports(0).DataSourceConnections(0).SetConnection(GloServerName, GloDatabaseName, GloUserID, GloPassword)

        Dim I As Integer = myReportDocument.Subreports.Count
        Dim X As Integer = 0
        For X = 0 To I - 1
            Dim myTables As Tables = myReportDocument.Subreports(X).Database.Tables
            Dim myTable As CrystalDecisions.CrystalReports.Engine.Table
            For Each myTable In myTables
                Dim myTableLogonInfo As TableLogOnInfo = myTable.LogOnInfo
                myTableLogonInfo.ConnectionInfo = myConnectionInfo
                myTable.ApplyLogOnInfo(myTableLogonInfo)
                myTable.Location = GloDatabaseName & ".dbo." & myTable.Location.Substring(myTable.Location.LastIndexOf(".") + 1)
            Next
        Next X
    End Sub


    Private Sub SetDBLogonForSubReport(ByVal myConnectionInfo As ConnectionInfo, ByVal myReportDocument As ReportDocument)
        customersByCityReport.Subreports(0).SetDatabaseLogon(GloUserID, GloPassword, GloServerName, GloDatabaseName)
        'customersByCityReport.Subreports(0).DataSourceConnections(0).SetConnection(GloServerName, GloDatabaseName, GloUserID, GloPassword)

        Dim I As Integer = myReportDocument.Subreports.Count
        Dim X As Integer = 0
        For X = 0 To I - 1
            Dim myTables As Tables = myReportDocument.Subreports(X).Database.Tables
            Dim myTable As CrystalDecisions.CrystalReports.Engine.Table
            For Each myTable In myTables
                Dim myTableLogonInfo As TableLogOnInfo = myTable.LogOnInfo
                myTableLogonInfo.ConnectionInfo = myConnectionInfo
                myTable.ApplyLogOnInfo(myTableLogonInfo)
                myTable.Location = GloDatabaseName & ".dbo." & myTable.Location.Substring(myTable.Location.LastIndexOf(".") + 1)
            Next
        Next X
    End Sub
    Private Sub SetDBLogonForReport2(ByVal myConnectionInfo As ConnectionInfo)
        Dim myTableLogOnInfos As TableLogOnInfos = Me.CrystalReportViewer1.LogOnInfo
        For Each myTableLogOnInfo As TableLogOnInfo In myTableLogOnInfos
            myTableLogOnInfo.ConnectionInfo = myConnectionInfo
        Next
    End Sub
    Private Sub ConfigureCrystalReportefacturaGlobal(ByVal Letra2 As String, ByVal importe2 As String, ByVal Serie2 As String, ByVal Fecha2 As String, ByVal Cajera2 As String, ByVal Factura2 As String)
        customersByCityReport = New ReportDocument
        Dim connectionInfo As New ConnectionInfo
        '"Data Source=SERVER-2003\SQLEXPRESS;Initial Catalog=Newsoftv;Persist Security Info"& _ 
        connectionInfo.ServerName = GloServerName
        connectionInfo.DatabaseName = GloDatabaseName
        connectionInfo.UserID = GloUserID
        connectionInfo.Password = GloPassword

        Dim cliente2 As String = "P�blico en General"
        Dim concepto2 As String = "Ingreso por Pago de Servicios"
        Dim txtsubtotal As String = Nothing
        Dim subtotal2 As Double
        Dim iva2 As Double
        Dim myString As String = iva2.ToString("00.00")
        Dim reportPath As String = Nothing
        reportPath = RutaReportes + "\ReporteFacturaGlobalticket.rpt"
        'MsgBox(reportPath)
        customersByCityReport.Load(reportPath)
        'SetDBLogonForReport(connectionInfo, customersByCityReport)
        'SetDBLogonForSubReport(connectionInfo, customersByCityReport)
        'customersByCityReport.DataDefinition.FormulaFields("Empresa").Text = "'" & LocNomEmpresa & "'"
        customersByCityReport.DataDefinition.FormulaFields("Empresa").Text = "'" & GloEmpresa & "'"
        customersByCityReport.DataDefinition.FormulaFields("DireccionEmpresa").Text = "'" & GloDireccionEmpresa & "'"
        customersByCityReport.DataDefinition.FormulaFields("Colonia_CpEmpresa").Text = "'" & GloColonia_CpEmpresa & "'"
        customersByCityReport.DataDefinition.FormulaFields("CiudadEmpresa").Text = "'" & GloCiudadEmpresa & "'"
        customersByCityReport.DataDefinition.FormulaFields("RfcEmpresa").Text = "'" & GloRfcEmpresa & "'"
        customersByCityReport.DataDefinition.FormulaFields("TelefonoEmpresa").Text = "'" & GloTelefonoEmpresa & "'"
        customersByCityReport.DataDefinition.FormulaFields("Letra").Text = "'" & Letra2 & "'"
        customersByCityReport.DataDefinition.FormulaFields("Cliente").Text = "'" & cliente2 & "'"
        customersByCityReport.DataDefinition.FormulaFields("Concepto").Text = "'" & concepto2 & "'"
        customersByCityReport.DataDefinition.FormulaFields("Serie").Text = "'" & Serie2 & "'"
        customersByCityReport.DataDefinition.FormulaFields("Fecha").Text = "'" & Fecha2 & "'"
        customersByCityReport.DataDefinition.FormulaFields("Cajera").Text = "'" & Cajera2 & "'"
        subtotal2 = CDec(importe2) / 1.15
        txtsubtotal = subtotal2
        txtsubtotal = subtotal2.ToString("##0.00")
        iva2 = CDec(importe2) / 1.15 * 0.15
        myString = iva2.ToString("##0.00")
        customersByCityReport.DataDefinition.FormulaFields("Subtotal").Text = "'" & txtsubtotal & "'"
        customersByCityReport.DataDefinition.FormulaFields("Iva").Text = "'" & myString & "'"
        customersByCityReport.DataDefinition.FormulaFields("ImporteServicio").Text = "'" & txtsubtotal & "'"
        customersByCityReport.DataDefinition.FormulaFields("Factura").Text = "'" & Factura2 & "'"
        customersByCityReport.DataDefinition.FormulaFields("Total").Text = "'" & importe2 & "'"

        'SetDBLogonForReport(connectionInfo, customersByCityReport)
        CrystalReportViewer1.ShowGroupTreeButton = False
        CrystalReportViewer1.ToolPanelView = CrystalDecisions.Windows.Forms.ToolPanelViewType.None
        CrystalReportViewer1.ReportSource = customersByCityReport
        customersByCityReport = Nothing
        GloReporte = 0

    End Sub

    Private Sub ConfigureCrystalReportefacturaGlobal2(ByVal Fecha As String, ByVal Tipo As String)
        customersByCityReport = New ReportDocument
        Dim connectionInfo As New ConnectionInfo
        '"Data Source=SERVER-2003\SQLEXPRESS;Initial Catalog=Newsoftv;Persist Security Info"& _ 
        '    "=True;User ID=DeSistema;Password=1975huli")
        'connectionInfo.ServerName = GloServerName
        'connectionInfo.DatabaseName = GloDatabaseName
        'connectionInfo.UserID = GloUserID
        'connectionInfo.Password = GloPassword

        Dim mySelectFormula As String = Nothing
        Dim Fecha1 As String = " "
        Dim Extra As String = " "
        Dim OpOrdenar As String = "0"

        Dim reportPath As String = Nothing
        reportPath = RutaReportes + "\ReporteFacturaGlobalW.rpt"

        Dim DS As New DataSet
        DS.Clear()
        BaseII.limpiaParametros()

        ' MsgBox(reportPath)
        'customersByCityReport.Load(reportPath)

        'SetDBLogonForReport(connectionInfo, customersByCityReport)
        'SetDBLogonForSubReport(connectionInfo, customersByCityReport)
        If Tipo = "V" Then

            mySelectFormula = "Comprobaci�n de Facturas Globales de Ventas por Sucursal"

            BaseII.CreateMyParameter("@fecha", SqlDbType.DateTime, Fecha)
            BaseII.CreateMyParameter("@Selsucursal", SqlDbType.Int, 0)
            BaseII.CreateMyParameter("@Op", SqlDbType.Int, 0)
            BaseII.CreateMyParameter("@idcompania", SqlDbType.Int, GloIdCompania)
            Dim listatablas As New List(Of String)
            listatablas.Add("ReporteFacturaGlobal")

            DS = BaseII.ConsultaDS("ReporteFacturaGlobal", listatablas)

            customersByCityReport.Load(reportPath)
            SetDBReport(DS, customersByCityReport)

            ' @fecha DateTime,
            customersByCityReport.SetParameterValue(0, Fecha)
            '  @Selsucursal  int
            customersByCityReport.SetParameterValue(1, CStr(0))
            '  @Op int
            customersByCityReport.SetParameterValue(2, CStr(0))

            'mySelectFormula = "Comprobaci�n de Facturas Globales de Ventas por Sucursal"


        Else
            If Tipo = "C" Then

                mySelectFormula = "Comprobaci�n de Facturas Globales de Cajas por Sucursal"

                BaseII.CreateMyParameter("@fecha", SqlDbType.DateTime, Fecha)
                BaseII.CreateMyParameter("@Selsucursal", SqlDbType.Int, GloSucursal)
                BaseII.CreateMyParameter("@Op", SqlDbType.Int, 2)
                BaseII.CreateMyParameter("@idcompania", SqlDbType.Int, GloIdCompania)
                Dim listatablas As New List(Of String)
                listatablas.Add("ReporteFacturaGlobal")

                DS = BaseII.ConsultaDS("ReporteFacturaGlobal", listatablas)

                customersByCityReport.Load(reportPath)
                SetDBReport(DS, customersByCityReport)

                '        'Fec_Ini
                customersByCityReport.SetParameterValue(0, Fecha)
                '        @Selsucursal  int
                customersByCityReport.SetParameterValue(1, GloSucursal)
                '        '@Op
                customersByCityReport.SetParameterValue(2, CStr(2))

                '        'Titulo del Reporte
                'mySelectFormula = "Comprobaci�n de Facturas Globales de Cajas por Sucursal"

            End If
        End If
        customersByCityReport.DataDefinition.FormulaFields("Empresa").Text = "'" & LocNomEmpresa & "'"
        customersByCityReport.DataDefinition.FormulaFields("Titulo").Text = "'" & mySelectFormula & "'"
        mySelectFormula = " Aguascalientes "
        customersByCityReport.DataDefinition.FormulaFields("Sucursal").Text = "'" & mySelectFormula & "'"
        mySelectFormula = " Fecha: " & bec_fecha
        customersByCityReport.DataDefinition.FormulaFields("SubTitulo").Text = "'" & mySelectFormula & "'"

        CrystalReportViewer1.ShowGroupTreeButton = False
        CrystalReportViewer1.ToolPanelView = CrystalDecisions.Windows.Forms.ToolPanelViewType.None
        CrystalReportViewer1.ReportSource = customersByCityReport
        customersByCityReport = Nothing
        GloReporte = 0

    End Sub
    Private Sub ConfigureCrystalReportefacturaGlobal3(ByVal Fecha As String)
        customersByCityReport = New ReportDocument
        Dim connectionInfo As New ConnectionInfo
        '"Data Source=SERVER-2003\SQLEXPRESS;Initial Catalog=Newsoftv;Persist Security Info"& _ 
        ''    "=True;User ID=DeSistema;Password=1975huli")
        'connectionInfo.ServerName = GloServerName
        'connectionInfo.DatabaseName = GloDatabaseName
        'connectionInfo.UserID = GloUserID
        'connectionInfo.Password = GloPassword

        Dim mySelectFormula As String = Nothing
        Dim Fecha1 As String = " "
        Dim Extra As String = " "
        Dim OpOrdenar As String = "0"

        Dim reportPath As String = Nothing
        reportPath = RutaReportes + "\ReporteFacturaGlobalW.rpt"
        ' MsgBox(reportPath)

        Dim DS As New DataSet
        DS.Clear()
        BaseII.limpiaParametros()

        mySelectFormula = " Cortes Generales de Cajas, Ventas y Facturas Globales "

        BaseII.CreateMyParameter("@fecha", SqlDbType.DateTime, Fecha)
        BaseII.CreateMyParameter("@Selsucursal", SqlDbType.Int, 0)
        BaseII.CreateMyParameter("@Op", SqlDbType.Int, 3)
        BaseII.CreateMyParameter("@idcompania", SqlDbType.Int, GloIdCompania)
        Dim listatablas As New List(Of String)
        listatablas.Add("ReporteFacturaGlobal")

        DS = BaseII.ConsultaDS("ReporteFacturaGlobal", listatablas)

        customersByCityReport.Load(reportPath)
        SetDBReport(DS, customersByCityReport)

        'customersByCityReport.Load(reportPath)

        'SetDBLogonForReport(connectionInfo, customersByCityReport)
        'SetDBLogonForSubReport(connectionInfo, customersByCityReport)
        '    'Fecha
        customersByCityReport.SetParameterValue(0, Fecha)
        '    '@Sucursal
        customersByCityReport.SetParameterValue(1, CStr(0))
        '    '@Op
        customersByCityReport.SetParameterValue(2, CStr(3))

        'mySelectFormula = " Cortes Generales de Cajas, Ventas y Facturas Globales "
        customersByCityReport.DataDefinition.FormulaFields("Empresa").Text = "'" & LocNomEmpresa & "'"
        customersByCityReport.DataDefinition.FormulaFields("Titulo").Text = "'" & mySelectFormula & "'"
        mySelectFormula = " Aguascalientes "
        customersByCityReport.DataDefinition.FormulaFields("Sucursal").Text = "'" & mySelectFormula & "'"
        mySelectFormula = " Fecha: " & bec_fecha
        customersByCityReport.DataDefinition.FormulaFields("SubTitulo").Text = "'" & mySelectFormula & "'"

        CrystalReportViewer1.ShowGroupTreeButton = False
        CrystalReportViewer1.ToolPanelView = CrystalDecisions.Windows.Forms.ToolPanelViewType.None
        CrystalReportViewer1.ReportSource = customersByCityReport
        customersByCityReport = Nothing

        GloReporte = 0

    End Sub

    Private Sub ConfigureCrystalReporteListEntregaParcial(ByVal clv_session As Integer, ByVal Fecha1 As String, ByVal Fecha As String)
        customersByCityReport = New ReportDocument
        ''Dim connectionInfo As New ConnectionInfo
        ''"Data Source=SERVER-2003\SQLEXPRESS;Initial Catalog=Newsoftv;Persist Security Info"& _ 
        ''    "=True;User ID=DeSistema;Password=1975huli")
        ''connectionInfo.ServerName = GloServerName
        ''connectionInfo.DatabaseName = GloDatabaseName
        ''connectionInfo.UserID = GloUserID
        ''connectionInfo.Password = GloPassword

        Dim mySelectFormula As String = Nothing
        Dim Fecha2 As String = " "
        Dim Extra As String = " "

        Dim reportPath As String = Nothing
        reportPath = RutaReportes + "\Listadp_Entregas_Parciales.rpt"

        Dim DS As New DataSet
        DS.Clear()
        BaseII.limpiaParametros()

        BaseII.CreateMyParameter("@clv_Session", SqlDbType.BigInt, clv_session)
        BaseII.CreateMyParameter("@Fecha_ini", SqlDbType.DateTime, Fecha1)
        BaseII.CreateMyParameter("@Fecha_fin", SqlDbType.DateTime, Fecha)

        Dim listatablas As New List(Of String)
        listatablas.Add("Reporte_Listado_Entregas_Parciales")

        DS = BaseII.ConsultaDS("Reporte_Listado_Entregas_Parciales", listatablas)

        customersByCityReport.Load(reportPath)
        SetDBReport(DS, customersByCityReport)

        ' MsgBox(reportPath)
        'customersByCityReport.Load(reportPath)
        'SetDBLogonForReport(connectionInfo, customersByCityReport)
        'SetDBLogonForSubReport(connectionInfo, customersByCityReport)
        '    'clave session
        customersByCityReport.SetParameterValue(0, clv_session)
        '    '@Fecha_ini
        customersByCityReport.SetParameterValue(1, Fecha1)
        '    '@Fecha_fin
        customersByCityReport.SetParameterValue(2, Fecha)
        ''Encabezados Reporte
        mySelectFormula = "Listado de Entregas Parciales por Cajera"
        Fecha2 = "Desde Fecha: " & Fecha1 & "  Hasta Fecha: " & Fecha
        If NomSucursal <> "999" And NomSucursal <> Nothing Then
            Extra = "Sucursal:" & NomSucursal
        End If
        customersByCityReport.DataDefinition.FormulaFields("Empresa").Text = "'" & GloEmpresaPRincipal & "'"
        customersByCityReport.DataDefinition.FormulaFields("Titulo").Text = "'" & mySelectFormula & "'"
        mySelectFormula = " Aguascalientes "
        customersByCityReport.DataDefinition.FormulaFields("Subtitulo").Text = "'" & Fecha2 & "'"
        mySelectFormula = " Fecha: " & bec_fecha
        customersByCityReport.DataDefinition.FormulaFields("Encabezado").Text = "'" & Extra & "'"

        CrystalReportViewer1.ShowGroupTreeButton = False
        CrystalReportViewer1.ToolPanelView = CrystalDecisions.Windows.Forms.ToolPanelViewType.None
        CrystalReportViewer1.ReportSource = customersByCityReport
        customersByCityReport = Nothing

        GloReporte = 0

    End Sub
    Private Sub ConfigureCrystalBonificaciones()
        Try
            customersByCityReport = New ReportDocument
            Dim connectionInfo As New ConnectionInfo
            '"Data Source=SERVER-2003\SQLEXPRESS;Initial Catalog=Newsoftv;Persist Security Info"& _ 
            '    "=True;User ID=DeSistema;Password=1975huli")
            'connectionInfo.ServerName = GloServerName
            'connectionInfo.DatabaseName = GloDatabaseName
            'connectionInfo.UserID = GloUserID
            'connectionInfo.Password = GloPassword

            Dim mySelectFormula As String = Nothing
            Dim Fecha2 As String = " "
            Dim Extra As String = " "

            Dim reportPath As String = Nothing
            If LocResumenBon = True Then
                reportPath = RutaReportes + "\ResumenBonificaciones.rpt"
                mySelectFormula = "Resumen de Tickets Bonificadas"
            Else
                reportPath = RutaReportes + "\ListadodeBonificaciones.rpt"
                mySelectFormula = "Listado de Tickets Bonificadas"
            End If

            Dim DS As New DataSet
            DS.Clear()
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@fecha1", SqlDbType.DateTime, LocFecha1)
            BaseII.CreateMyParameter("@fecha2", SqlDbType.DateTime, LocFecha2)
            BaseII.CreateMyParameter("@clv_txt", SqlDbType.VarChar, Locclv_usuario)
            BaseII.CreateMyParameter("@idcompania", SqlDbType.Int, GloIdCompania)
            BaseII.CreateMyParameter("@clv_ciudad", SqlDbType.Int, GloClvCiudad)
            Dim listatablas As New List(Of String)
            listatablas.Add("Listado_Bonificaciones")
            listatablas.Add("SUCURSALES")
            listatablas.Add("Usuarios")

            DS = BaseII.ConsultaDS("Listado_Bonificaciones", listatablas)

            customersByCityReport.Load(reportPath)
            SetDBReport(DS, customersByCityReport)

            ' MsgBox(reportPath)
            'customersByCityReport.Load(reportPath)
            'SetDBLogonForReport(connectionInfo, customersByCityReport)
            'SetDBLogonForSubReport(connectionInfo, customersByCityReport)
            '    '@Fecha_ini
            customersByCityReport.SetParameterValue(0, LocFecha1)
            '    '@Fecha_fin
            customersByCityReport.SetParameterValue(1, LocFecha2)
            ' @clv_txt varchar(5)
            customersByCityReport.SetParameterValue(2, Locclv_usuario)
            ''Encabezados Reporte

            Dim Nomsucursal As String = Nothing
            Nomsucursal = "Sucursal:" + GloNomSucursal

            Dim RangoFechas As String = Nothing
            RangoFechas = "De la Fecha: " + LocFecha1 + " A la Fecha: " + LocFecha2

            Dim Supervisor As String = Nothing
            Supervisor = "Supervisor: " + LocNombreusuario



            customersByCityReport.DataDefinition.FormulaFields("Empresa").Text = "'" & LocNomEmpresa & "'"
            customersByCityReport.DataDefinition.FormulaFields("Titulo").Text = "'" & mySelectFormula & "'"
            customersByCityReport.DataDefinition.FormulaFields("Fechas").Text = "'" & RangoFechas & "'"
            customersByCityReport.DataDefinition.FormulaFields("Subtitulo").Text = "'" & Nomsucursal & "'"
            customersByCityReport.DataDefinition.FormulaFields("Supervisor").Text = "'" & Supervisor & "'"

            If LocResumenBon = True Then
                customersByCityReport.PrintOptions.PaperOrientation = PaperOrientation.Portrait
            Else
                customersByCityReport.PrintOptions.PaperOrientation = PaperOrientation.Landscape
            End If

            CrystalReportViewer1.ShowGroupTreeButton = False
            CrystalReportViewer1.ToolPanelView = CrystalDecisions.Windows.Forms.ToolPanelViewType.None
            CrystalReportViewer1.ReportSource = customersByCityReport
            customersByCityReport = Nothing
            LocBndBon = False
        Catch ex As Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try

    End Sub

    Private Sub ConfigureCrystalBonificaciones2()
        Try
            customersByCityReport = New ReportDocument
            Dim connectionInfo As New ConnectionInfo

            Dim mySelectFormula As String = Nothing
            Dim Fecha2 As String = " "
            Dim Extra As String = " "

            Dim reportPath As String = Nothing

            reportPath = RutaReportes + "\Listado_Bonificaciones_2.rpt"
            mySelectFormula = "Listado de Tickets Bonificadas"


            Dim DS As New DataSet
            DS.Clear()
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@fecha1", SqlDbType.DateTime, LocFecha1)
            BaseII.CreateMyParameter("@fecha2", SqlDbType.DateTime, LocFecha2)
            BaseII.CreateMyParameter("@identificador", SqlDbType.BigInt, identificador)
            BaseII.CreateMyParameter("@aplicada", SqlDbType.Int, EstadoBonificacion)
            BaseII.CreateMyParameter("@CompaniasXml", SqlDbType.Xml, DocCompanias.InnerXml)
            Dim listatablas As New List(Of String)
            listatablas.Add("Listado_Bonificaciones")
            listatablas.Add("Titulos")

            DS = BaseII.ConsultaDS("Listado_Bonificaciones_2", listatablas)

            customersByCityReport.Load(reportPath)
            SetDBReport(DS, customersByCityReport)

            customersByCityReport.PrintOptions.PaperOrientation = PaperOrientation.Landscape


            CrystalReportViewer1.ShowGroupTreeButton = False
            CrystalReportViewer1.ToolPanelView = CrystalDecisions.Windows.Forms.ToolPanelViewType.None
            CrystalReportViewer1.ReportSource = customersByCityReport
            customersByCityReport = Nothing
            LocBndBon = False
        Catch ex As Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try

    End Sub
    Private Sub ConfigureCrystalFacturasCanceladas()
        Try
            customersByCityReport = New ReportDocument
            Dim connectionInfo As New ConnectionInfo
            '"Data Source=SERVER-2003\SQLEXPRESS;Initial Catalog=Newsoftv;Persist Security Info"& _ 
            '    "=True;User ID=DeSistema;Password=1975huli")
            'connectionInfo.ServerName = GloServerName
            'connectionInfo.DatabaseName = GloDatabaseName
            'connectionInfo.UserID = GloUserID
            'connectionInfo.Password = GloPassword

            Dim mySelectFormula As String = Nothing
            Dim Fecha2 As String = " "
            Dim Extra As String = " "

            Dim reportPath As String = Nothing
            Select Case LocBanderaRep1
                Case 0
                    mySelectFormula = "Listado de Tickets Canceladas"
                Case 1
                    mySelectFormula = "Listado de Tickets Reimpresas"
            End Select

            reportPath = RutaReportes + "\ListadoFacturasCanceladas.rpt"

            Dim DS As New DataSet
            DS.Clear()
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@fecha1", SqlDbType.DateTime, LocFecha1)
            BaseII.CreateMyParameter("@fecha2", SqlDbType.DateTime, LocFecha2)
            BaseII.CreateMyParameter("@clv_usuario", SqlDbType.VarChar, Locclv_usuario)
            BaseII.CreateMyParameter("@clv_reporte", SqlDbType.Int, LocBanderaRep1 + 1)
            BaseII.CreateMyParameter("@idcompania", SqlDbType.Int, GloIdCompania)
            BaseII.CreateMyParameter("@clv_ciudad", SqlDbType.Int, GloClvCiudad)
            Dim listatablas As New List(Of String)
            listatablas.Add("Listado_Facturas_Canceladas")
            listatablas.Add("SUCURSALES")

            DS = BaseII.ConsultaDS("Listado_Facturas_Canceladas", listatablas)

            customersByCityReport.Load(reportPath)
            SetDBReport(DS, customersByCityReport)

            ' MsgBox(reportPath)
            'customersByCityReport.Load(reportPath)
            'SetDBLogonForReport(connectionInfo, customersByCityReport)
            'SetDBLogonForSubReport(connectionInfo, customersByCityReport)
            '    '@Fecha_ini
            customersByCityReport.SetParameterValue(0, LocFecha1)
            '    '@Fecha_fin
            customersByCityReport.SetParameterValue(1, LocFecha2)
            ' @clv_txt varchar(5)
            customersByCityReport.SetParameterValue(2, Locclv_usuario)
            '@clv_reporte
            customersByCityReport.SetParameterValue(3, LocBanderaRep1 + 1)

            ''Encabezados Reporte

            Dim Nomsucursal As String = Nothing
            Nomsucursal = "Sucursal:" + GloNomSucursal

            Dim RangoFechas As String = Nothing
            RangoFechas = "De la Fecha: " + LocFecha1 + " A la Fecha: " + LocFecha2

            Dim Cajero As String = Nothing
            Cajero = "Usuario: " + LocNombreusuario


            customersByCityReport.DataDefinition.FormulaFields("Empresa").Text = "'" & GloEmpresaPRincipal & "'"
            customersByCityReport.DataDefinition.FormulaFields("Titulo").Text = "'" & mySelectFormula & "'"
            customersByCityReport.DataDefinition.FormulaFields("Fechas").Text = "'" & RangoFechas & "'"
            customersByCityReport.DataDefinition.FormulaFields("Subtitulo").Text = "'" & Nomsucursal & "'"
            customersByCityReport.DataDefinition.FormulaFields("Cajero").Text = "'" & Cajero & "'"

            customersByCityReport.PrintOptions.PaperOrientation = PaperOrientation.Portrait

            CrystalReportViewer1.ShowGroupTreeButton = False
            CrystalReportViewer1.ToolPanelView = CrystalDecisions.Windows.Forms.ToolPanelViewType.None
            CrystalReportViewer1.ReportSource = customersByCityReport
            customersByCityReport = Nothing
            LocBndrepfac1 = False
        Catch ex As Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try

    End Sub
    Private Sub ConfigureCrystalNotasCredito()
        Try
            customersByCityReport = New ReportDocument
            Dim connectionInfo As New ConnectionInfo
            '"Data Source=SERVER-2003\SQLEXPRESS;Initial Catalog=Newsoftv;Persist Security Info"& _ 
            '    "=True;User ID=DeSistema;Password=1975huli")
            'connectionInfo.ServerName = GloServerName
            'connectionInfo.DatabaseName = GloDatabaseName
            'connectionInfo.UserID = GloUserID
            'connectionInfo.Password = GloPassword

            Dim mySelectFormula As String = Nothing
            Dim Fecha2 As String = " "
            Dim Extra As String = " "

            Dim reportPath As String = Nothing

            Select Case Locbndrepnotas
                Case 0
                    reportPath = RutaReportes + "\ReporteNotasdeCreditoNuevo.rpt"
                Case 1
                    reportPath = RutaReportes + "\ReporteNotasdeCredito(sucursal).rpt"
            End Select

            Dim DS As New DataSet
            DS.Clear()
            BaseII.limpiaParametros()

            BaseII.CreateMyParameter("@xmlFiltro", SqlDbType.VarChar, DocPlazas.InnerXml)
            BaseII.CreateMyParameter("@fecha1", SqlDbType.DateTime, LocFecha1)
            BaseII.CreateMyParameter("@fecha2", SqlDbType.DateTime, LocFecha2)

            Dim listatablas As New List(Of String)
            listatablas.Add("Reporte_Notas_Credito")
            listatablas.Add("Titulo")

            DS = BaseII.ConsultaDS("Reporte_Notas_CreditoNuevo", listatablas)

            customersByCityReport.Load(reportPath)
            SetDBReport(DS, customersByCityReport)



            Dim Nomsucursal As String = Nothing
            Nomsucursal = "Sucursal:" + GloNomSucursal

            Dim RangoFechas As String = Nothing
            RangoFechas = "De la Fecha: " + LocFecha1 + " A la Fecha: " + LocFecha2

            mySelectFormula = "Listado de Notas de Cr�dito"




            customersByCityReport.DataDefinition.FormulaFields("Empresa").Text = "'" & GloEmpresaPRincipal & "'"
            customersByCityReport.DataDefinition.FormulaFields("Titulo").Text = "'" & mySelectFormula & "'"
            customersByCityReport.DataDefinition.FormulaFields("Fechas").Text = "'" & RangoFechas & "'"
            customersByCityReport.DataDefinition.FormulaFields("Sucursal").Text = "'" & Nomsucursal & "'"


            customersByCityReport.PrintOptions.PaperOrientation = PaperOrientation.Landscape

            CrystalReportViewer1.ShowGroupTreeButton = False
            CrystalReportViewer1.ToolPanelView = CrystalDecisions.Windows.Forms.ToolPanelViewType.None
            CrystalReportViewer1.ReportSource = customersByCityReport
            customersByCityReport = Nothing
            LocBndrepfac1 = False
        Catch ex As Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try

    End Sub

    Private Sub ConfigureCrystalNotasCreditoEspeciales()
        Try
            customersByCityReport = New ReportDocument
            Dim connectionInfo As New ConnectionInfo
            '"Data Source=SERVER-2003\SQLEXPRESS;Initial Catalog=Newsoftv;Persist Security Info"& _ 
            '    "=True;User ID=DeSistema;Password=1975huli")
            'connectionInfo.ServerName = GloServerName
            'connectionInfo.DatabaseName = GloDatabaseName
            'connectionInfo.UserID = GloUserID
            'connectionInfo.Password = GloPassword

            Dim mySelectFormula As String = Nothing
            Dim Fecha2 As String = " "
            Dim Extra As String = " "

            Dim reportPath As String = Nothing

            Select Case Locbndrepnotas
                Case 0
                    reportPath = RutaReportes + "\ReporteNotasdeCreditoNuevo.rpt"
                Case 1
                    reportPath = RutaReportes + "\ReporteNotasdeCredito(sucursal).rpt"
            End Select

            Dim DS As New DataSet
            DS.Clear()
            BaseII.limpiaParametros()

            BaseII.CreateMyParameter("@xmlFiltro", SqlDbType.VarChar, DocSucursales.InnerXml)
            BaseII.CreateMyParameter("@fecha1", SqlDbType.DateTime, LocFecha1)
            BaseII.CreateMyParameter("@fecha2", SqlDbType.DateTime, LocFecha2)

            Dim listatablas As New List(Of String)
            listatablas.Add("Reporte_Notas_Credito")
            listatablas.Add("Titulo")

            DS = BaseII.ConsultaDS("Reporte_Notas_CreditoNuevoEspeciales", listatablas)

            customersByCityReport.Load(reportPath)
            SetDBReport(DS, customersByCityReport)



            Dim Nomsucursal As String = Nothing
            Nomsucursal = "Sucursal:" + GloNomSucursal

            Dim RangoFechas As String = Nothing
            RangoFechas = "De la Fecha: " + LocFecha1 + " A la Fecha: " + LocFecha2

            mySelectFormula = "Listado de Notas de Cr�dito"




            customersByCityReport.DataDefinition.FormulaFields("Empresa").Text = "'" & GloEmpresaPRincipal & "'"
            customersByCityReport.DataDefinition.FormulaFields("Titulo").Text = "'" & mySelectFormula & "'"
            customersByCityReport.DataDefinition.FormulaFields("Fechas").Text = "'" & RangoFechas & "'"
            customersByCityReport.DataDefinition.FormulaFields("Sucursal").Text = "'" & Nomsucursal & "'"


            customersByCityReport.PrintOptions.PaperOrientation = PaperOrientation.Landscape

            CrystalReportViewer1.ShowGroupTreeButton = False
            CrystalReportViewer1.ToolPanelView = CrystalDecisions.Windows.Forms.ToolPanelViewType.None
            CrystalReportViewer1.ReportSource = customersByCityReport
            customersByCityReport = Nothing
            LocBndrepfac1 = False
        Catch ex As Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try

    End Sub

    Private Sub ConfigureCrystalDesglosePagosCiudad()
        Try
            Dim dSet As New DataSet
            Dim tableNameList As New List(Of String)
            Dim rDocument As New ReportDocument

            tableNameList.Add("REPORTEDesglose")
            tableNameList.Add("Bancos")
            tableNameList.Add("IEPS")
            tableNameList.Add("IVA")
            tableNameList.Add("tblCompanias")
            tableNameList.Add("General")
            tableNameList.Add("Titulo")

            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@FECHAINI", SqlDbType.DateTime, eFechaInicial) 'OK
            BaseII.CreateMyParameter("@FECHAFIN", SqlDbType.DateTime, eFechaFinal) 'OK
            BaseII.CreateMyParameter("@TIPO", SqlDbType.VarChar, "C", 1) 'OK
            BaseII.CreateMyParameter("@CIUDAD", SqlDbType.Int, 0) 'OK
            BaseII.CreateMyParameter("@CAJA", SqlDbType.Int, 0) 'OK
            BaseII.CreateMyParameter("@CAJERA", SqlDbType.VarChar, GloUsuario, 11)
            BaseII.CreateMyParameter("@CLV_SESSION", SqlDbType.Int, identificador) 'OK
            BaseII.CreateMyParameter("@CLV_FACTURA", SqlDbType.Int, 0)
            BaseII.CreateMyParameter("@CLV_USUARIO", SqlDbType.VarChar, GloUsuario, 11)
            BaseII.CreateMyParameter("@CLVCOMPANIA", SqlDbType.Int, 0) 'OK
            BaseII.CreateMyParameter("@CLV_LLAVE_POLIZANEW", ParameterDirection.Output, SqlDbType.Int)
            BaseII.CreateMyParameter("@NUMERROR", ParameterDirection.Output, SqlDbType.Int)
            BaseII.CreateMyParameter("@MSJERROR", ParameterDirection.Output, SqlDbType.VarChar, 250)
            dSet = BaseII.ConsultaDS("REPORTEDesglosePorCiudad", tableNameList)

            rDocument.Load(RutaReportes + "\REPORTEDesglosePorCiudad.rpt")
            rDocument.SetDataSource(dSet)

            CrystalReportViewer1.ShowGroupTreeButton = False
            CrystalReportViewer1.ToolPanelView = CrystalDecisions.Windows.Forms.ToolPanelViewType.None
            CrystalReportViewer1.ReportSource = rDocument
            CrystalReportViewer1.Zoom(100)
            CrystalReportViewer1.ShowPrintButton = True
            CrystalReportViewer1.ShowExportButton = True
            CrystalReportViewer1.ShowRefreshButton = False

            'customersByCityReport = New ReportDocument
            'Dim connectionInfo As New ConnectionInfo
            ''"Data Source=SERVER-2003\SQLEXPRESS;Initial Catalog=Newsoftv;Persist Security Info"& _ 
            ''    "=True;User ID=DeSistema;Password=1975huli")
            ''connectionInfo.ServerName = GloServerName
            ''connectionInfo.DatabaseName = GloDatabaseName
            ''connectionInfo.UserID = GloUserID
            ''connectionInfo.Password = GloPassword
            'Dim reportPath As String = Nothing
            'Dim Titulo As String = Nothing
            'Dim Sucursal As String = Nothing
            'Dim Ciudades As String = Nothing
            'Ciudades = " Ciudad(es): " + LocCiudades

            'reportPath = RutaReportes + "\ReportePagosRangoFechas.rpt"
            'Titulo = "Relaci�n de Ingresos por Conceptos"

            'Sucursal = " Sucursal: " + GloNomSucursal

            'Dim DS As New DataSet
            'DS.Clear()
            'BaseII.limpiaParametros()

            'BaseII.CreateMyParameter("@Fecha_Ini", SqlDbType.DateTime, eFechaInicial)
            'BaseII.CreateMyParameter("@Fecha_Fin", SqlDbType.DateTime, eFechaFinal)
            'BaseII.CreateMyParameter("@Tipo", SqlDbType.VarChar, "")
            'BaseII.CreateMyParameter("@sucursal", SqlDbType.Int, 0)
            'BaseII.CreateMyParameter("@Caja", SqlDbType.Int, 0)
            'BaseII.CreateMyParameter("@Cajera", SqlDbType.VarChar, "")
            'BaseII.CreateMyParameter("@Op", SqlDbType.Int, 0)
            'BaseII.CreateMyParameter("@Clv_Session", SqlDbType.BigInt, gloClv_Session)
            'BaseII.CreateMyParameter("@identificador", SqlDbType.Int, identificador)
            'Dim listatablas As New List(Of String)
            'listatablas.Add("Desglose_PagosRangoFechas")
            'listatablas.Add("SUCURSALES")

            'DS = BaseII.ConsultaDS("Desglose_PagosRangoFechas", listatablas)

            'customersByCityReport.Load(reportPath)
            'SetDBReport(DS, customersByCityReport)

            ''customersByCityReport.Load(reportPath)

            ' ''SetDBLogonForSubReport(connectionInfo, customersByCityReport)
            ''SetDBLogonForReport(connectionInfo, customersByCityReport)
            ''SetDBLogonForReport(connectionInfo, customersByCityReport)
            ''SetDBLogonForSubReport(connectionInfo, customersByCityReport)

            ''@FECHA_INI
            'customersByCityReport.SetParameterValue(0, eFechaInicial)
            ''@FECHA_FIN
            'customersByCityReport.SetParameterValue(1, eFechaFinal)
            ''@TIPO
            'customersByCityReport.SetParameterValue(2, "")
            ''@SUCURSAL
            'customersByCityReport.SetParameterValue(3, "0")
            ''@CAJA
            'customersByCityReport.SetParameterValue(4, "0")
            ''@CAJERA
            'customersByCityReport.SetParameterValue(5, "")
            ''@OP
            'customersByCityReport.SetParameterValue(6, "0")
            ''Clv_Session
            'customersByCityReport.SetParameterValue(7, gloClv_Session)




            'customersByCityReport.DataDefinition.FormulaFields("Empresa").Text = "'" & LocNomEmpresa & "'"
            'customersByCityReport.DataDefinition.FormulaFields("Titulo").Text = "'" & Titulo & "'"
            'eFechaTitulo = "de la Fecha " & eFechaInicial & " a la Fecha " & eFechaFinal
            'customersByCityReport.DataDefinition.FormulaFields("Fecha").Text = "'" & eFechaTitulo & "'"
            'customersByCityReport.DataDefinition.FormulaFields("Subtitulo").Text = "'" & Sucursal & "'"
            'customersByCityReport.DataDefinition.FormulaFields("Ciudades").Text = "'" & Ciudades & "'"
            'customersByCityReport.DataDefinition.FormulaFields("Compania").Text = "'" & NombreCompania & "'"

            'CrystalReportViewer1.ReportSource = customersByCityReport
            'CrystalReportViewer1.Zoom(75)

            'customersByCityReport = Nothing

        Catch ex As Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub
    Private Sub ConfigureCrystalDesglosePagosSucursal()
        Try

            Dim dSet As New DataSet
            Dim tableNameList As New List(Of String)
            Dim rDocument As New ReportDocument

            tableNameList.Add("REPORTEDesglose")
            tableNameList.Add("Bancos")
            tableNameList.Add("IEPS")
            tableNameList.Add("IVA")
            tableNameList.Add("tblCompanias")
            tableNameList.Add("General")
            tableNameList.Add("Titulo")

            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@FECHAINI", SqlDbType.DateTime, eFechaInicial) 'OK
            BaseII.CreateMyParameter("@FECHAFIN", SqlDbType.DateTime, eFechaFinal) 'OK
            BaseII.CreateMyParameter("@TIPO", SqlDbType.VarChar, "C", 1) 'OK
            BaseII.CreateMyParameter("@SUCURSAL", SqlDbType.Int, 0) 'OK
            BaseII.CreateMyParameter("@CAJA", SqlDbType.Int, 0) 'OK
            BaseII.CreateMyParameter("@CAJERA", SqlDbType.VarChar, GloUsuario, 11)
            BaseII.CreateMyParameter("@CLV_SESSION", SqlDbType.Int, identificador) 'OK
            BaseII.CreateMyParameter("@CLV_FACTURA", SqlDbType.Int, 0)
            BaseII.CreateMyParameter("@CLV_USUARIO", SqlDbType.VarChar, GloUsuario, 11)
            BaseII.CreateMyParameter("@CLVCOMPANIA", SqlDbType.Int, 0) 'OK
            BaseII.CreateMyParameter("@CLV_LLAVE_POLIZANEW", ParameterDirection.Output, SqlDbType.Int)
            BaseII.CreateMyParameter("@NUMERROR", ParameterDirection.Output, SqlDbType.Int)
            BaseII.CreateMyParameter("@MSJERROR", ParameterDirection.Output, SqlDbType.VarChar, 250)
            dSet = BaseII.ConsultaDS("REPORTEDesglose", tableNameList)

            rDocument.Load(RutaReportes + "\REPORTEDesglose.rpt")
            rDocument.SetDataSource(dSet)

            CrystalReportViewer1.ShowGroupTreeButton = False
            CrystalReportViewer1.ToolPanelView = CrystalDecisions.Windows.Forms.ToolPanelViewType.None
            CrystalReportViewer1.ReportSource = rDocument
            CrystalReportViewer1.Zoom(100)
            CrystalReportViewer1.ShowPrintButton = True
            CrystalReportViewer1.ShowExportButton = True
            CrystalReportViewer1.ShowRefreshButton = False


        Catch ex As Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try

    End Sub
    Private Sub ConfigureCrystalReportsDetalleConciliacion()
        Try

            customersByCityReport = New ReportDocument
            'Dim connectionInfo As New ConnectionInfo
            '"Data Source=SERVER-2003\SQLEXPRESS;Initial Catalog=Newsoftv;Persist Security Info"& _ 
            '    "=True;User ID=DeSistema;Password=1975huli")
            'connectionInfo.ServerName = GloServerName
            'connectionInfo.DatabaseName = GloDatabaseName
            'connectionInfo.UserID = GloUserID
            'connectionInfo.Password = GloPassword
            Dim reportPath As String = Nothing
            Dim Titulo As String = Nothing
            Dim Sucursal As String = Nothing
            Dim Ciudades As String = Nothing
            ' Ciudades = " Ciudad(es): " + LocCiudades

            reportPath = RutaReportes + "\Detalle_Prefacturas_Pagolinea.rpt"
            Titulo = "Detalle De Movimientos Por Fecha"

            Sucursal = " Sucursal: " + GloNomSucursal

            Dim DS As New DataSet
            DS.Clear()
            BaseII.limpiaParametros()

            BaseII.CreateMyParameter("@fecha1", SqlDbType.DateTime, locGlo_Fechaini)
            BaseII.CreateMyParameter("@fecha2", SqlDbType.DateTime, locGlo_Fechafin)
            Dim listatablas As New List(Of String)
            listatablas.Add("Imprime_Detalle_prefacturas_pagolinea")
            listatablas.Add("Companias")
            DS = BaseII.ConsultaDS("Imprime_Detalle_prefacturas_pagolinea", listatablas)

            customersByCityReport.Load(reportPath)
            SetDBReport(DS, customersByCityReport)

            'customersByCityReport.Load(reportPath)

            ''SetDBLogonForSubReport(connectionInfo, customersByCityReport)
            'SetDBLogonForReport(connectionInfo, customersByCityReport)
            'SetDBLogonForReport(connectionInfo, customersByCityReport)
            'SetDBLogonForSubReport(connectionInfo, customersByCityReport)

            '@FECHA_INI
            customersByCityReport.SetParameterValue(0, locGlo_Fechaini)
            '@FECHA_FIN
            customersByCityReport.SetParameterValue(1, locGlo_Fechafin)





            customersByCityReport.DataDefinition.FormulaFields("Empresa").Text = "'" & GloEmpresaPRincipal & "'"
            customersByCityReport.DataDefinition.FormulaFields("Titulo").Text = "'" & Titulo & "'"
            eFechaTitulo = "De la Fecha: " & locGlo_Fechaini & " a la Fecha: " & locGlo_Fechafin
            customersByCityReport.DataDefinition.FormulaFields("Fechas").Text = "'" & eFechaTitulo & "'"
            customersByCityReport.DataDefinition.FormulaFields("Sucursal").Text = ""
            'ustomersByCityReport.DataDefinition.FormulaFields("Ciudades").Text = "'" & Ciudades & "'"

            CrystalReportViewer1.ShowGroupTreeButton = False
            CrystalReportViewer1.ToolPanelView = CrystalDecisions.Windows.Forms.ToolPanelViewType.None
            CrystalReportViewer1.ReportSource = customersByCityReport
            CrystalReportViewer1.Zoom(75)

            customersByCityReport = Nothing

        Catch ex As Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try

    End Sub

    Private Sub ConfigureCrystalReportsDetalleConciliacionEspecial()
        Try

            customersByCityReport = New ReportDocument
            Dim reportPath As String = Nothing
            Dim Titulo As String = Nothing
            Dim Sucursal As String = Nothing
            Dim Ciudades As String = Nothing
            ' Ciudades = " Ciudad(es): " + LocCiudades

            reportPath = RutaReportes + "\Detalle_Prefacturas_Pagolinea.rpt"
            Titulo = "Detalle De Movimientos Por Fecha"

            Sucursal = " Sucursal: " + GloNomSucursal

            Dim DS As New DataSet
            DS.Clear()
            BaseII.limpiaParametros()

            BaseII.CreateMyParameter("@fecha1", SqlDbType.DateTime, locGlo_Fechaini)
            BaseII.CreateMyParameter("@fecha2", SqlDbType.DateTime, locGlo_Fechafin)
            Dim listatablas As New List(Of String)
            listatablas.Add("Imprime_Detalle_prefacturas_pagolinea")
            listatablas.Add("Companias")
            DS = BaseII.ConsultaDS("ImprimeDetalleEspecial", listatablas)

            customersByCityReport.Load(reportPath)
            SetDBReport(DS, customersByCityReport)

            'customersByCityReport.Load(reportPath)

            ''SetDBLogonForSubReport(connectionInfo, customersByCityReport)
            'SetDBLogonForReport(connectionInfo, customersByCityReport)
            'SetDBLogonForReport(connectionInfo, customersByCityReport)
            'SetDBLogonForSubReport(connectionInfo, customersByCityReport)

            '@FECHA_INI
            customersByCityReport.SetParameterValue(0, locGlo_Fechaini)
            '@FECHA_FIN
            customersByCityReport.SetParameterValue(1, locGlo_Fechafin)





            customersByCityReport.DataDefinition.FormulaFields("Empresa").Text = "'" & GloEmpresaPRincipal & "'"
            customersByCityReport.DataDefinition.FormulaFields("Titulo").Text = "'" & Titulo & "'"
            eFechaTitulo = "De la Fecha: " & locGlo_Fechaini & " a la Fecha: " & locGlo_Fechafin
            customersByCityReport.DataDefinition.FormulaFields("Fechas").Text = "'" & eFechaTitulo & "'"
            customersByCityReport.DataDefinition.FormulaFields("Sucursal").Text = ""
            'ustomersByCityReport.DataDefinition.FormulaFields("Ciudades").Text = "'" & Ciudades & "'"

            CrystalReportViewer1.ShowGroupTreeButton = False
            CrystalReportViewer1.ToolPanelView = CrystalDecisions.Windows.Forms.ToolPanelViewType.None
            CrystalReportViewer1.ReportSource = customersByCityReport
            CrystalReportViewer1.Zoom(75)

            customersByCityReport = Nothing

        Catch ex As Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try

    End Sub
    Private Sub ConfigureCrystalReportsCargosAutomaticos()
        Try

            customersByCityReport = New ReportDocument
            Dim connectionInfo As New ConnectionInfo
            '"Data Source=SERVER-2003\SQLEXPRESS;Initial Catalog=Newsoftv;Persist Security Info"& _ 
            '    "=True;User ID=DeSistema;Password=1975huli")
            'connectionInfo.ServerName = GloServerName
            'connectionInfo.DatabaseName = GloDatabaseName
            'connectionInfo.UserID = GloUserID
            'connectionInfo.Password = GloPassword
            Dim reportPath As String = Nothing
            Dim Titulo As String = Nothing
            Dim Sucursal As String = Nothing
            Dim Ciudades As String = Nothing
            ' Ciudades = " Ciudad(es): " + LocCiudades
            If losresumencargos = True Then
                reportPath = RutaReportes + "\ReporteFacturasCargoAutoResumen.rpt"
                Titulo = "Resumen De Tickets Con Cargo Automatico"
            Else
                reportPath = RutaReportes + "\ReporteFacturasCargoAutoDetallado.rpt"
                Titulo = "Listado De Tickets Con Cargo Automatico"
            End If


            Sucursal = " Sucursal: " + GloNomSucursal
            'customersByCityReport.Load(reportPath)



            'SetDBLogonForReport(connectionInfo, customersByCityReport)

            Dim DS As New DataSet
            DS.Clear()
            BaseII.limpiaParametros()

            BaseII.CreateMyParameter("@clv_Session", SqlDbType.BigInt, locclv_sessioncargosauto)
            BaseII.CreateMyParameter("@op", SqlDbType.Int, 0)
            BaseII.CreateMyParameter("@FechaIni", SqlDbType.DateTime, Fecha_IniCargo)
            BaseII.CreateMyParameter("@FechaFin", SqlDbType.DateTime, Fecha_FinCargo)
            BaseII.CreateMyParameter("@SelCajera", SqlDbType.VarChar, Cajero_Cargo)

            Dim listatablas As New List(Of String)
            listatablas.Add("Reporte_Facturas_Cargos_Automaticos")
            listatablas.Add("RELCLIBANCO")

            DS = BaseII.ConsultaDS("Reporte_Facturas_Cargos_Automaticos", listatablas)

            customersByCityReport.Load(reportPath)
            SetDBReport(DS, customersByCityReport)
            '(@clv_Session bigint,@op bigint,@FechaIni datetime,@FechaFin datetime,@SelCajera varchar(250)

            '@clv_session
            customersByCityReport.SetParameterValue(0, locclv_sessioncargosauto)
            '@op
            customersByCityReport.SetParameterValue(1, 0)
            '@fechaIni
            customersByCityReport.SetParameterValue(2, Fecha_IniCargo)
            '@fechaFin
            customersByCityReport.SetParameterValue(3, Fecha_FinCargo)
            '@SelCajera
            customersByCityReport.SetParameterValue(4, Cajero_Cargo)






            customersByCityReport.DataDefinition.FormulaFields("Empresa").Text = "'" & LocNomEmpresa & "'"
            customersByCityReport.DataDefinition.FormulaFields("Titulo").Text = "'" & Titulo & "'"
            eFechaTitulo = "De la Fecha: " & Fecha_IniCargo & " a la Fecha: " & Fecha_FinCargo
            customersByCityReport.DataDefinition.FormulaFields("Fechas").Text = "'" & eFechaTitulo & "'"
            customersByCityReport.DataDefinition.FormulaFields("Sucursal").Text = "'" & Sucursal & "'"
            'ustomersByCityReport.DataDefinition.FormulaFields("Ciudades").Text = "'" & Ciudades & "'"

            CrystalReportViewer1.ShowGroupTreeButton = False
            CrystalReportViewer1.ToolPanelView = CrystalDecisions.Windows.Forms.ToolPanelViewType.None
            CrystalReportViewer1.ReportSource = customersByCityReport
            CrystalReportViewer1.Zoom(75)

            customersByCityReport = Nothing
            bndcancelareportcargos = True
        Catch ex As Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try

    End Sub
    Private Sub ConfigureCrystalReportsPagosEfectuadosCliente()
        Try
            customersByCityReport = New ReportDocument
            Dim connectionInfo As New ConnectionInfo
            '"Data Source=SERVER-2003\SQLEXPRESS;Initial Catalog=Newsoftv;Persist Security Info"& _ 
            '    "=True;User ID=DeSistema;Password=1975huli")
            connectionInfo.ServerName = GloServerName
            connectionInfo.DatabaseName = GloDatabaseName
            connectionInfo.UserID = GloUserID
            connectionInfo.Password = GloPassword
            Dim reportPath As String = Nothing
            Dim Titulo As String = Nothing
            Dim Sucursal As String = Nothing
            Dim Ciudades As String = Nothing
            ' Ciudades = " Ciudad(es): " + LocCiudades
            reportPath = RutaReportes + "\Reporte_Clientes_facturas_detallado_Jiq.rpt"
            Titulo = "Listado De Pagos Efectuados Por El Cliente"
            Sucursal = " Sucursal: " + GloNomSucursal

            customersByCityReport.Load(reportPath)


            SetDBLogonForReport(connectionInfo, customersByCityReport)


            '(@clv_Session bigint,@op bigint,@FechaIni datetime,@FechaFin datetime,@SelCajera varchar(250)


            '@op
            customersByCityReport.SetParameterValue(0, 0)
            '@contratoini
            customersByCityReport.SetParameterValue(1, contratoini)
            '@contratofin
            customersByCityReport.SetParameterValue(2, contratofin)



            customersByCityReport.DataDefinition.FormulaFields("Empresa").Text = "'" & LocNomEmpresa & "'"
            customersByCityReport.DataDefinition.FormulaFields("Titulo").Text = "'" & Titulo & "'"
            customersByCityReport.DataDefinition.FormulaFields("Subtitulo").Text = "'" & Sucursal & "'"

            CrystalReportViewer1.ShowGroupTreeButton = False
            CrystalReportViewer1.ToolPanelView = CrystalDecisions.Windows.Forms.ToolPanelViewType.None
            CrystalReportViewer1.ReportSource = customersByCityReport
            CrystalReportViewer1.Zoom(75)
            customersByCityReport = Nothing
        Catch ex As Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try

    End Sub

    Private Sub ConfigureCrystalReportsCorteGlobal()
        Try
            customersByCityReport = New ReportDocument
            Dim connectionInfo As New ConnectionInfo
            Dim fechas As String = Nothing
            Dim reportPath As String = Nothing
            Dim titulo As String = Nothing

            connectionInfo.ServerName = GloServerName
            connectionInfo.DatabaseName = GloDatabaseName
            connectionInfo.UserID = GloUserID
            connectionInfo.Password = GloPassword


            reportPath = RutaReportes + "\ReportCortesGlobal.rpt"
            customersByCityReport.Load(reportPath)
            SetDBLogonForReport(connectionInfo, customersByCityReport)

            '@Clv_Session
            customersByCityReport.SetParameterValue(0, CStr(eClv_Session))
            '@FechaInicial
            customersByCityReport.SetParameterValue(1, CStr(eFechaInicial))
            '@FechaFinal
            customersByCityReport.SetParameterValue(2, CStr(eFechaFinal))

            titulo = "Reporte de Cortes Global"
            fechas = "Del " & CStr(eFechaInicial) & " al " & CStr(eFechaFinal)

            customersByCityReport.DataDefinition.FormulaFields("Empresa").Text = "'" & GloEmpresa & "'"
            customersByCityReport.DataDefinition.FormulaFields("Titulo").Text = "'" & titulo & "'"
            customersByCityReport.DataDefinition.FormulaFields("Fecha").Text = "'" & fechas & "'"
            customersByCityReport.DataDefinition.FormulaFields("Ciudad").Text = "'" & GloNomSucursal & "'"

            CrystalReportViewer1.ShowGroupTreeButton = False
            CrystalReportViewer1.ToolPanelView = CrystalDecisions.Windows.Forms.ToolPanelViewType.None
            CrystalReportViewer1.ReportSource = customersByCityReport
            SetDBLogonForReport2(connectionInfo)
            customersByCityReport = Nothing
        Catch ex As Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub rptCobroErroneo()
        Try
            customersByCityReport = New ReportDocument
            Dim connectionInfo As New ConnectionInfo
            Dim fechas As String = Nothing
            Dim reportPath As String = Nothing
            Dim titulo As String = Nothing

            'connectionInfo.ServerName = GloServerName
            'connectionInfo.DatabaseName = GloDatabaseName
            'connectionInfo.UserID = GloUserID
            'connectionInfo.Password = GloPassword


            reportPath = RutaReportes + "\rptCobroErroneo.rpt"
            'customersByCityReport.Load(reportPath)
            'SetDBLogonForReport(connectionInfo, customersByCityReport)
            Dim DS As New DataSet
            DS.Clear()
            BaseII.limpiaParametros()

            BaseII.CreateMyParameter("@OP", SqlDbType.Int, 0)
            BaseII.CreateMyParameter("@FECHAINI", SqlDbType.DateTime, eFechaIni)
            BaseII.CreateMyParameter("@FECHAFIN", SqlDbType.DateTime, eFechaFin)

            Dim listatablas As New List(Of String)
            listatablas.Add("ReporteCobroErroneo")

            DS = BaseII.ConsultaDS("ReporteCobroErroneo", listatablas)

            customersByCityReport.Load(reportPath)
            SetDBReport(DS, customersByCityReport)

            '@Op
            customersByCityReport.SetParameterValue(0, 0)
            '@FechaIni
            customersByCityReport.SetParameterValue(1, CStr(eFechaIni))
            '@FechaFin
            customersByCityReport.SetParameterValue(2, CStr(eFechaFin))


            fechas = "Del " & CStr(eFechaIni) & " al " & CStr(eFechaFin)

            customersByCityReport.DataDefinition.FormulaFields("Empresa").Text = "'" & GloEmpresa & "'"
            customersByCityReport.DataDefinition.FormulaFields("Fecha").Text = "'" & fechas & "'"
            customersByCityReport.DataDefinition.FormulaFields("Ciudad").Text = "'" & GloNomSucursal & "'"

            CrystalReportViewer1.ShowGroupTreeButton = False
            CrystalReportViewer1.ToolPanelView = CrystalDecisions.Windows.Forms.ToolPanelViewType.None
            CrystalReportViewer1.ReportSource = customersByCityReport
            SetDBLogonForReport2(connectionInfo)
            customersByCityReport = Nothing
        Catch ex As Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub ReportePagosDifFac()
        Try
            customersByCityReport = New ReportDocument
            Dim connectionInfo As New ConnectionInfo
            Dim fechas As String = Nothing
            Dim reportPath As String = Nothing
            Dim titulo As String = Nothing

            'connectionInfo.ServerName = GloServerName
            'connectionInfo.DatabaseName = GloDatabaseName
            'connectionInfo.UserID = GloUserID
            'connectionInfo.Password = GloPassword


            reportPath = RutaReportes + "\ReportPagosDifFac.rpt"
            'customersByCityReport.Load(reportPath)
            'SetDBLogonForReport(connectionInfo, customersByCityReport)
            Dim DS As New DataSet
            DS.Clear()
            BaseII.limpiaParametros()

            BaseII.CreateMyParameter("@PERIODO1", SqlDbType.Int, ePeriodo1)
            BaseII.CreateMyParameter("@PERIODO2", SqlDbType.Int, ePeriodo2)
            BaseII.CreateMyParameter("@ULTIMOMES", SqlDbType.Int, eMes)
            BaseII.CreateMyParameter("@ULTIMOANIO", SqlDbType.Int, eAnio)
            BaseII.CreateMyParameter("@PAGO1", SqlDbType.Int, ePago1)
            BaseII.CreateMyParameter("@PAGO2", SqlDbType.Int, ePago2)

            Dim listatablas As New List(Of String)
            listatablas.Add("ReportePagosDifFac")

            DS = BaseII.ConsultaDS("ReportePagosDifFac", listatablas)

            customersByCityReport.Load(reportPath)
            SetDBReport(DS, customersByCityReport)

            '@Periodo1
            customersByCityReport.SetParameterValue(0, ePeriodo1)
            '@Periodo2
            customersByCityReport.SetParameterValue(1, ePeriodo2)
            '@Mes
            customersByCityReport.SetParameterValue(2, eMes)
            '@Anio
            customersByCityReport.SetParameterValue(3, eAnio)
            '@Pago1
            customersByCityReport.SetParameterValue(4, ePago1)
            '@Pago2
            customersByCityReport.SetParameterValue(5, ePago2)




            customersByCityReport.DataDefinition.FormulaFields("Empresa").Text = "'" & GloEmpresa & "'"
            customersByCityReport.DataDefinition.FormulaFields("Titulo").Text = "'" & eTitulo & "'"
            customersByCityReport.DataDefinition.FormulaFields("Ciudad").Text = "'" & GloNomSucursal & "'"

            CrystalReportViewer1.ShowGroupTreeButton = False
            CrystalReportViewer1.ToolPanelView = CrystalDecisions.Windows.Forms.ToolPanelViewType.None
            CrystalReportViewer1.ReportSource = customersByCityReport
            SetDBLogonForReport2(connectionInfo)
            customersByCityReport = Nothing
        Catch ex As Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub


    Private Sub ReportePromocion()
        Try
            customersByCityReport = New ReportDocument
            Dim connectionInfo As New ConnectionInfo
            Dim fechas As String = Nothing
            Dim reportPath As String = Nothing
            Dim titulo As String = Nothing

            'connectionInfo.ServerName = GloServerName
            'connectionInfo.DatabaseName = GloDatabaseName
            'connectionInfo.UserID = GloUserID
            'connectionInfo.Password = GloPassword


            reportPath = RutaReportes + "\ReportPromocion.rpt"
            'customersByCityReport.Load(reportPath)
            'SetDBLogonForReport(connectionInfo, customersByCityReport)
            Dim DS As New DataSet
            DS.Clear()
            BaseII.limpiaParametros()

            BaseII.CreateMyParameter("@FECHASOLINI", SqlDbType.DateTime, eFechaIni)
            BaseII.CreateMyParameter("@FECHASOLFIN", SqlDbType.DateTime, eFechaFin)
            BaseII.CreateMyParameter("@C", SqlDbType.Bit, eC)
            BaseII.CreateMyParameter("@I", SqlDbType.Bit, eI)
            BaseII.CreateMyParameter("@D", SqlDbType.Bit, eD)
            BaseII.CreateMyParameter("@S", SqlDbType.Bit, eS)
            BaseII.CreateMyParameter("@B", SqlDbType.Bit, eB)
            BaseII.CreateMyParameter("@F", SqlDbType.Bit, eF)

            Dim listatablas As New List(Of String)
            listatablas.Add("ReportePromocion")

            DS = BaseII.ConsultaDS("ReportePromocion", listatablas)

            customersByCityReport.Load(reportPath)
            SetDBReport(DS, customersByCityReport)

            '@Periodo1
            customersByCityReport.SetParameterValue(0, eFechaIni)
            '@Periodo2
            customersByCityReport.SetParameterValue(1, eFechaFin)
            '@C
            customersByCityReport.SetParameterValue(2, eC)
            '@I
            customersByCityReport.SetParameterValue(3, eI)
            '@D
            customersByCityReport.SetParameterValue(4, eD)
            '@S
            customersByCityReport.SetParameterValue(5, eS)
            '@B
            customersByCityReport.SetParameterValue(6, eB)
            '@F
            customersByCityReport.SetParameterValue(7, eF)


            fechas = "Contratados del " & CStr(eFechaIni) & " al " & CStr(eFechaFin)

            customersByCityReport.DataDefinition.FormulaFields("Empresa").Text = "'" & GloEmpresa & "'"
            customersByCityReport.DataDefinition.FormulaFields("Titulo").Text = "'" & eTitulo & "'"
            customersByCityReport.DataDefinition.FormulaFields("Fechas").Text = "'" & fechas & "'"
            customersByCityReport.DataDefinition.FormulaFields("Ciudad").Text = "'" & GloNomSucursal & "'"

            CrystalReportViewer1.ShowGroupTreeButton = False
            CrystalReportViewer1.ToolPanelView = CrystalDecisions.Windows.Forms.ToolPanelViewType.None
            CrystalReportViewer1.ReportSource = customersByCityReport
            SetDBLogonForReport2(connectionInfo)
            customersByCityReport = Nothing
        Catch ex As Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub FrmImprimirRepGral_Activated(sender As Object, e As System.EventArgs) Handles Me.Activated

    End Sub


    Private Sub FrmImprimirRepGral_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed

    End Sub

    Private Sub FrmImprimirRepGral_FormClosing(sender As Object, e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        OPLISTADOCORTEFAC = 0
        TIPOCAJA = ""
        If GloReporte = 9 And (GloTipoUsuario = 40 Or GloTipoUsuario = 1) Then
            GloReporte = 0

            'If uspChecaSiTieneArqueo(GloCajera, Fecha_ini) = True Then 'VALIDAMOS SI LA CAJERA YA TIENE UN ARQUEO GENERADO DEL D�A PARA NO HACER LA PREGUNTA
            '    Exit Sub
            'End If

            'Dim res = MsgBox("�Deseas Guardar el Arqueo como el Corte Final del D�a?", MsgBoxStyle.YesNo) 'EN CASO DE NO TENER ARQUEO SE LE REALIZA LA PREGUNTA
            'If res = MsgBoxResult.Yes Then
            '    Dim frm As New FrmGuardaArqueos
            '    frm.CajeraArqueo = GloCajera
            '    frm.FechaArqueo = Fecha_ini
            '    frm.IdCorte = 0
            '    frm.Show()
            'Else
            '    Exit Sub
            'End If
        End If
    End Sub
    Private Sub FrmImprimirRepGral_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If entraReporte = True Then 'JUANJO REPORTE PARCIALIDADES
            entraReporte = False
            GloReporte = 0
            ConfigureCrystalCobrosParcialesMaterial(contratoCobroMaterial, fechaIniCobroMaterial, fechaFinCobroMaterial, opCobroMaterial, saldadosCobroMaterial, pendientesCobroMaterial)
        End If

        If GloReporte = 1 Then
            ConfigureCrystalReports(GloClv_SessionBancos, GloTitulo, GloSubTitulo)
        ElseIf GloReporte = 2 Then
            ConfigureCrystalReportsParciales(GloConsecutivo)
        ElseIf GloReporte = 3 Then
            Me.ConfigureCrystalDesglose2(GloConsecutivo)
        ElseIf GloReporte = 4 Then
            Me.ConfigureCrystalArqueo(Fecha_ini, GloCajera)
        ElseIf GloReporte = 5 Then
            ' ConfigureCrystalReortefacturaGlobal("", bec_importe, bec_serie, bec_fecha, GloUsuario, bec_factura)
            Me.ConfigureCrystalReportefacturaGlobal(bec_letra, bec_importe, bec_serie, bec_fecha, GloUsuario, bec_factura)
        ElseIf GloReporte = 6 Then
            ' ConfigureCrystalReortefacturaGlobal2("", bec_importe, bec_serie, bec_fecha, GloUsuario, bec_factura)
            Me.ConfigureCrystalReportefacturaGlobal2(bec_fecha, bec_tipo)
        ElseIf GloReporte = 7 Then
            ' ConfigureCrystalReortefacturaGlobal3("", bec_importe, bec_serie, bec_fecha, GloUsuario, bec_factura)
            Me.ConfigureCrystalReportefacturaGlobal3(bec_fecha)
        ElseIf GloReporte = 8 Then
            Me.ConfigureCrystalReporteListEntregaParcial(LocClv_session, Fecha_ini, Fecha_Fin)
        ElseIf GloReporte = 9 Then
            ConfigureCrystalReportsOxxo(GloClv_SessionBancos, GloTitulo, GloSubTitulo)
        ElseIf GloReporte = 10 Then
            GloReporte = 0
            ConfigureCrystalReportsCorteGlobal()
        ElseIf GloReporte = 11 Then
            GloReporte = 0
            rptCobroErroneo()
        ElseIf GloReporte = 12 Then
            GloReporte = 0
            ReportePagosDifFac()
        ElseIf GloReporte = 13 Then
            GloReporte = 0
            ReportePromocion()
        End If
        If LocBndBon = True Then
            LocBndBon = False
            ConfigureCrystalBonificaciones()
        End If
        If BonificacionNuevo = True Then
            ConfigureCrystalBonificaciones2()
            BonificacionNuevo = False
        End If
        If LocBndrepfac1 = True Then
            LocBndrepfac1 = False
            ConfigureCrystalFacturasCanceladas()
        End If
        If LocbndNotas = True Then
            LocbndNotas = False
            If GloOpFiltrosXML = "listadoNotaCreditoHastaDistribuidor" Then
                ConfigureCrystalNotasCredito()
            Else
                ConfigureCrystalNotasCreditoEspeciales()
            End If
        End If
        If LocbndDesPagosCiudad = True Then
            LocbndDesPagosCiudad = False
            ConfigureCrystalDesglosePagosCiudad()
            Me.CrystalReportViewer1.ShowPrintButton = True
        End If
        If LocbndDesPagosSucursal = True Then
            LocbndDesPagosSucursal = False
            ConfigureCrystalDesglosePagosSucursal()
            Me.CrystalReportViewer1.ShowPrintButton = True
        End If
        If LocBndNotasReporteTick = True Then
            LocBndNotasReporteTick = False
            'ConfigureCrystalReportsNotasCredito1()
            CrystalREPORTENotaDeCredito(gloClvNota)
        End If
        If bndreporteconciliacion = True Then
            If TipoConciliacion = 1 Then
                bndreporteconciliacion = False
                ConfigureCrystalReportsDetalleConciliacion()
            ElseIf TipoConciliacion = 2 Then
                bndreporteconciliacion = False
                ConfigureCrystalReportsDetalleConciliacionEspecial()
            End If
        End If
        If bndreportcargos = True Then
            bndreportcargos = False
            ConfigureCrystalReportsCargosAutomaticos()
        End If
        If BndRepImporteClietnes = True Then
            BndRepImporteClietnes = False
            Me.Text = "Listado De Pagos Efectuados Por El Cliente"
            ConfigureCrystalReportsPagosEfectuadosCliente()
        End If
    End Sub

    Public Sub ReporteMontos()
        Try
            customersByCityReport = New ReportDocument
            Dim connectionInfo As New ConnectionInfo
            Dim Fechas As String = Nothing
            Dim ReportPath As String = Nothing
            Dim Titulo As String = Nothing

            'connectionInfo.ServerName = GloServerName
            'connectionInfo.DatabaseName = GloDatabaseName
            'connectionInfo.UserID = GloUserID
            'connectionInfo.Password = GloPassword


            ReportPath = RutaReportes + "\ReportMontos.rpt"
            'customersByCityReport.Load(ReportPath)
            'SetDBLogonForReport(connectionInfo, customersByCityReport)

            Dim DS As New DataSet
            DS.Clear()
            BaseII.limpiaParametros()

            BaseII.CreateMyParameter("@CLV_TIPSER", SqlDbType.Int, eClvTipSer)
            BaseII.CreateMyParameter("@FECHAINI", SqlDbType.DateTime, eFechaIni)
            BaseII.CreateMyParameter("@FECHAFIN", SqlDbType.DateTime, eFechaFin)
            BaseII.CreateMyParameter("@OP", SqlDbType.Int, eOp)
            BaseII.CreateMyParameter("@MONTO", SqlDbType.Decimal, eMonto)

            Dim listatablas As New List(Of String)
            listatablas.Add("ReporteMontos")

            DS = BaseII.ConsultaDS("ReporteMontos", listatablas)

            customersByCityReport.Load(ReportPath)
            SetDBReport(DS, customersByCityReport)


            '@Clv_TipSer
            customersByCityReport.SetParameterValue(0, eClvTipSer)
            '@FechaIni
            customersByCityReport.SetParameterValue(1, eFechaIni)
            '@FechaFin
            customersByCityReport.SetParameterValue(2, eFechaFin)
            '@Op
            customersByCityReport.SetParameterValue(3, eOp)
            '@Monto
            customersByCityReport.SetParameterValue(4, eMonto)

            Titulo = "Ingresos de Clientes del Servicio de" & eConcepto & " por un Monto " & eOperador & " a $" & CStr(eMonto)
            Fechas = "En el Periodo del " & CStr(eFechaIni) & " al " & CStr(eFechaFin)


            customersByCityReport.DataDefinition.FormulaFields("Empresa").Text = "'" & GloEmpresa & "'"
            customersByCityReport.DataDefinition.FormulaFields("Titulo").Text = "'" & Titulo & "'"
            customersByCityReport.DataDefinition.FormulaFields("Fechas").Text = "'" & Fechas & "'"
            customersByCityReport.DataDefinition.FormulaFields("Ciudad").Text = "'" & GloNomSucursal & "'"

            CrystalReportViewer1.ShowGroupTreeButton = False
            CrystalReportViewer1.ToolPanelView = CrystalDecisions.Windows.Forms.ToolPanelViewType.None
            CrystalReportViewer1.ReportSource = customersByCityReport
            SetDBLogonForReport2(connectionInfo)
            customersByCityReport = Nothing
        Catch ex As Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try


    End Sub


    Public Sub ReporteBonificacion()
        Try
            customersByCityReport = New ReportDocument
            Dim connectionInfo As New ConnectionInfo
            Dim Fechas As String = Nothing
            Dim ReportPath As String = Nothing
            Dim Titulo As String = Nothing

            'connectionInfo.ServerName = GloServerName
            'connectionInfo.DatabaseName = GloDatabaseName
            'connectionInfo.UserID = GloUserID
            'connectionInfo.Password = GloPassword


            ReportPath = RutaReportes + "\ReportBonificacion.rpt"
            'customersByCityReport.Load(ReportPath)
            'SetDBLogonForReport(connectionInfo, customersByCityReport)

            Dim DS As New DataSet
            DS.Clear()
            BaseII.limpiaParametros()

            BaseII.CreateMyParameter("@FECHAINI", SqlDbType.DateTime, eFechaIni)
            BaseII.CreateMyParameter("@FECHAFIN", SqlDbType.DateTime, eFechaFin)
            BaseII.CreateMyParameter("@NUMERO", SqlDbType.Int, eNumero)

            Dim listatablas As New List(Of String)
            listatablas.Add("ReporteBonificacion")

            DS = BaseII.ConsultaDS("ReporteBonificacion", listatablas)

            customersByCityReport.Load(ReportPath)
            SetDBReport(DS, customersByCityReport)

            '@FechaIni
            customersByCityReport.SetParameterValue(0, eFechaIni)
            '@FechaFin
            customersByCityReport.SetParameterValue(1, eFechaFin)
            '@Numero
            customersByCityReport.SetParameterValue(2, eNumero)

            Titulo = "N�mero de Bonificaciones: " & CStr(eNumero)
            Fechas = "En el Periodo del " & CStr(eFechaIni) & " al " & CStr(eFechaFin)


            customersByCityReport.DataDefinition.FormulaFields("Empresa").Text = "'" & GloEmpresa & "'"
            customersByCityReport.DataDefinition.FormulaFields("Titulo").Text = "'" & Titulo & "'"
            customersByCityReport.DataDefinition.FormulaFields("Fechas").Text = "'" & Fechas & "'"
            customersByCityReport.DataDefinition.FormulaFields("Ciudad").Text = "'" & GloNomSucursal & "'"

            CrystalReportViewer1.ShowGroupTreeButton = False
            CrystalReportViewer1.ToolPanelView = CrystalDecisions.Windows.Forms.ToolPanelViewType.None
            CrystalReportViewer1.ReportSource = customersByCityReport
            SetDBLogonForReport2(connectionInfo)
            customersByCityReport = Nothing
        Catch ex As Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try


    End Sub


    Private Sub ReporteArqueoPrincipalXsd_3(ByVal FECHA As Date, ByVal CAJERA As String, ByVal RUTAREP As String)
        'Dim CON As New SqlConnection(MiConexion)

        'Dim CMD As New SqlCommand("ArqueoCajas_tmp", CON)
        'CMD.CommandType = CommandType.StoredProcedure
        'CMD.Parameters.AddWithValue("@Fecha", FECHA)
        'CMD.Parameters.AddWithValue("@NomCajera", CAJERA)
        'Dim DA As New SqlDataAdapter(CMD)

        'Dim DS As New DataSet()

        'DA.Fill(DS)

        'DS.Tables(0).TableName = "ArqueoCajas_tmp"
        'DS.Tables(1).TableName = "ARQUEOCAJAS"
        'DS.Tables(2).TableName = "ArqueoCajas_EntregasP"
        'DS.Tables(3).TableName = "ArqueoCajas_CreditCard"
        'DS.Tables(4).TableName = "ArqueoCajas_Cheques"

        'customersByCityReport.Load(RUTAREP)
        'SetDBReport(DS, customersByCityReport)
        Dim DS As New DataSet
        DS.Clear()
        BaseII.limpiaParametros()

        BaseII.CreateMyParameter("@Fecha", SqlDbType.DateTime, FECHA)
        BaseII.CreateMyParameter("@NomCajera", SqlDbType.VarChar, CAJERA)

        Dim listatablas As New List(Of String)
        listatablas.Add("ArqueoCajas_tmp")
        listatablas.Add("ARQUEOCAJAS")
        listatablas.Add("ArqueoCajas_EntregasP")
        listatablas.Add("ArqueoCajas_CreditCard")
        listatablas.Add("ArqueoCajas_Cheques")
        listatablas.Add("ArqueoCajas_Transferencia")
        listatablas.Add("ArqueoCajas_DebitCard")

        DS = BaseII.ConsultaDS("ArqueoCajas_tmp", listatablas)

        customersByCityReport.Load(RUTAREP)
        SetDBReport(DS, customersByCityReport)
    End Sub


    Private Function REPORTENotaDeCredito(ByVal Clv_NotaDeCredito As Integer) As DataSet
        Dim tableNameList As New List(Of String)
        tableNameList.Add("Cliente")
        tableNameList.Add("Nota")
        tableNameList.Add("DetNota")

        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@CLV_NOTADECREDITO", SqlDbType.Int, Clv_NotaDeCredito)
        Return BaseII.ConsultaDS("REPORTENotaDeCredito", tableNameList)
    End Function

    Private Sub CrystalREPORTENotaDeCredito(ByVal Clv_NotaDeCredito As Integer)
        'Dim rDocument As New CrystalDecisions.CrystalReports.Engine.ReportDocument
        'Dim dSet As New DataSet
        'dSet = REPORTENotaDeCredito(Clv_NotaDeCredito)
        'rDocument.Load(RutaReportes + "\REPORTENotaDeCredito.rpt")
        'rDocument.SetDataSource(dSet)
        'CrystalReportViewer1.ReportSource = rDocument
        'CrystalReportViewer1.ShowPrintButton = True


        'Nuevo.................................
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        Dim ba As Boolean
        Dim opc1, opc2 As String
        Dim busfac As New NewsoftvDataSet2TableAdapters.BusFacFiscalTableAdapter
        Dim bfac As New NewsoftvDataSet2.BusFacFiscalDataTable
        customersByCityReport = New ReportDocument
        Dim connectionInfo As New ConnectionInfo

        Dim reportPath As String = Nothing
        reportPath = RutaReportes + "\ReporteNotasdeCredito.rpt"
        Dim DS As New DataSet
        DS.Clear()
        BaseII.limpiaParametros()

        BaseII.CreateMyParameter("@Clave", SqlDbType.BigInt, Clv_NotaDeCredito)
        BaseII.CreateMyParameter("@Clv_nota_Ini", SqlDbType.BigInt, 0)
        BaseII.CreateMyParameter("@Clv_nota_Fin", SqlDbType.BigInt, 0)
        BaseII.CreateMyParameter("@Fecha_Ini", SqlDbType.DateTime, "01/01/1900")
        BaseII.CreateMyParameter("@Fecha_Fin", SqlDbType.DateTime, "01/01/1900")
        BaseII.CreateMyParameter("@op", SqlDbType.Int, 0)

        Dim listatablas As New List(Of String)
        listatablas.Add("ReportesNotasDeCredito;1")
        listatablas.Add("DetFacturas_NotadeCredito")
        listatablas.Add("Notas_de_Credito")
        listatablas.Add("Rel_NotaCredito_ConceptosServ")
        listatablas.Add("General")
        DS = BaseII.ConsultaDS("ReportesNotasDeCreditoNew", listatablas)

        customersByCityReport.Load(reportPath)
        SetDBReport(DS, customersByCityReport)
        If IdSistema = "VA" Then
            opc1 = "Devoluci�n en Efecitvo"
            opc2 = "Devoluci�n en Efectivo:"
        Else
            opc1 = "Nota de Cr�dito"
            opc2 = "Nota de Cr�dito :"
        End If
        If ba = False Then
            customersByCityReport.DataDefinition.FormulaFields("Titulo").Text = "'" & opc1 & "'"
            customersByCityReport.DataDefinition.FormulaFields("Clave").Text = "'" & opc2 & "'"
            customersByCityReport.DataDefinition.FormulaFields("Empresa").Text = "'" & GloEmpresa & "'"
            customersByCityReport.DataDefinition.FormulaFields("DireccionEmpresa").Text = "'" & GloDireccionEmpresa & "'"
            customersByCityReport.DataDefinition.FormulaFields("Colonia_CpEmpresa").Text = "'" & GloColonia_CpEmpresa & "'"
            customersByCityReport.DataDefinition.FormulaFields("CiudadEmpresa").Text = "'" & GloCiudadEmpresa & "'"
            customersByCityReport.DataDefinition.FormulaFields("RfcEmpresa").Text = "'" & GloRfcEmpresa & "'"
            customersByCityReport.DataDefinition.FormulaFields("TelefonoEmpresa").Text = "'" & GloTelefonoEmpresa & "'"
            If locoprepnotas = 0 Then
                customersByCityReport.DataDefinition.FormulaFields("Copia").Text = "'Copia'"
            ElseIf locoprepnotas = 1 Then
                customersByCityReport.DataDefinition.FormulaFields("Copia").Text = "'Original'"
            End If
        End If
        CrystalReportViewer1.ShowGroupTreeButton = False
        CrystalReportViewer1.ToolPanelView = CrystalDecisions.Windows.Forms.ToolPanelViewType.None
        CrystalReportViewer1.ReportSource = customersByCityReport
        CrystalReportViewer1.ShowPrintButton = True
        CON.Close()
        customersByCityReport = Nothing
    End Sub

    Private Sub ConfigureCrystalCobrosParcialesMaterial(ByVal prmContrato As Long, ByVal prmFechaIni As Date, ByVal prmFechaFin As Date, ByVal prmOp As Integer, _
                                                    ByVal prmSaldados As Boolean, ByVal prmPendientes As Boolean)
        'ESTO LO HIZO JUANJO PARA IMPRIMIR LOS PAGOS PARCIALES DEL COBRO DE MATERIAL
        Try

            customersByCityReport = New ReportDocument
            Dim mySelectFormula As String = Nothing
            Dim reportPath As String = Nothing

            reportPath = RutaReportes + "\rptReporteCobrosParcialesMaterial.rpt"

            customersByCityReport.Load(reportPath)
            customersByCityReport.SetDataSource(CobroParcialMaterial.spReporteCobrosParcialesMaterial(prmContrato, prmFechaIni, prmFechaFin, prmOp, prmSaldados, prmPendientes))

            mySelectFormula = "Listado de Convenios por Cobro de Material"
            customersByCityReport.DataDefinition.FormulaFields("Empresa").Text = "'" & GloEmpresaPRincipal & "'"
            customersByCityReport.DataDefinition.FormulaFields("Titulo").Text = "'" & mySelectFormula & "'"
            mySelectFormula = "Sucursal : " & GloNomSucursal
            customersByCityReport.DataDefinition.FormulaFields("Subtitulo").Text = "'" & mySelectFormula & "'"
            customersByCityReport.DataDefinition.FormulaFields("fecha1").Text = "'" & prmFechaIni.ToString & "'"
            customersByCityReport.DataDefinition.FormulaFields("fecha2").Text = "'" & prmFechaFin.ToString & "'"
            customersByCityReport.DataDefinition.FormulaFields("contrato").Text = "'" & prmContrato.ToString & "'"
            customersByCityReport.DataDefinition.FormulaFields("op").Text = "'" & prmOp.ToString & "'"
            If prmPendientes = True And prmSaldados = True Then
                mySelectFormula = "Saldados y Por Pagar"
            ElseIf prmPendientes = True And prmSaldados = False Then
                mySelectFormula = "Por Pagar"
            ElseIf prmPendientes = False And prmSaldados = True Then
                mySelectFormula = "Saldados"
            End If
            customersByCityReport.DataDefinition.FormulaFields("status").Text = "'" & mySelectFormula & "'"

            CrystalReportViewer1.ShowGroupTreeButton = False
            CrystalReportViewer1.ToolPanelView = CrystalDecisions.Windows.Forms.ToolPanelViewType.None
            CrystalReportViewer1.ReportSource = customersByCityReport
        Catch ex As Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub

End Class