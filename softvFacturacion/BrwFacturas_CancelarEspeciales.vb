﻿Public Class BrwFacturas_CancelarEspeciales
    Private opG As String = ""
    Private serieG As String = ""
    Private folioG As Integer = 0
    Private fechaG As String = "01/01/1900"
    Private contratoG As Integer = 0
    Private sucursalG As Integer = 0

    Private Sub FrmHistorialReimpresion_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        colorea(Me)
        Try
            BaseII.limpiaParametros()
            cbTipo.DataSource = BaseII.ConsultaDT("MUESTRATIPOFACTURA_Reimpresion")
            BaseII.limpiaParametros()
            cbSucursal.DataSource = BaseII.ConsultaDT("ObtieneSucursalesEspeciales_Reimpresion")
            UspGuardaFormularios(Me.Name, Me.Text)
            UspGuardaBotonesFormularioSiste(Me, Me.Name)
            UspDesactivaBotones(Me, Me.Name)
            lbNombre.ForeColor = lbSerie.ForeColor
            lbNombre.BackColor = lbSerie.BackColor
            If bndRefacturacionEspeciales = 0 Then
                btnRefacturacion.Visible = False
            Else
                btnRefacturacion.Visible = True
                Button8.Visible = False
                Button2.Visible = False
                Button3.Visible = False
            End If
        Catch ex As Exception

        End Try
    End Sub

    Private Sub cbTipo_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cbTipo.SelectedIndexChanged
        Try
            opG = "tipo"
            serieG = ""
            folioG = 0
            fechaG = "01/01/1900"
            contratoG = 0
            BuscaFactura("tipo", "", 0, "01/01/1900", 0, cbSucursal.SelectedValue)
        Catch ex As Exception

        End Try
    End Sub

    Private Sub BuscaFactura(op As String, serie As String, folio As Integer, fecha As String, contrato As Integer, clv_sucursal As Integer)
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@op", SqlDbType.VarChar, op)
        BaseII.CreateMyParameter("@serie", SqlDbType.VarChar, serie)
        BaseII.CreateMyParameter("@folio", SqlDbType.BigInt, folio)
        BaseII.CreateMyParameter("@fecha", SqlDbType.VarChar, fecha)
        BaseII.CreateMyParameter("@tipo", SqlDbType.VarChar, cbTipo.SelectedValue)
        BaseII.CreateMyParameter("@contrato", SqlDbType.BigInt, contrato)
        BaseII.CreateMyParameter("@clv_sucursal", SqlDbType.BigInt, clv_sucursal)
        BaseII.CreateMyParameter("@refacturacion", SqlDbType.BigInt, bndRefacturacionEspeciales)
        DataGridView1.DataSource = BaseII.ConsultaDT("BuscaFacturasEspeciales")
    End Sub

    Private Sub Button7_Click(sender As Object, e As EventArgs) Handles Button7.Click
        If tbFolio.Text.Trim.Length > 0 Or tbSerie.Text.Trim.Length > 0 Then
            If tbFolio.Text.Trim.Length > 0 And IsNumeric(tbFolio.Text) Then
                opG = "seriefolio"
                serieG = tbSerie.Text
                folioG = tbFolio.Text
                fechaG = "01/01/1900"
                contratoG = 0
                BuscaFactura("seriefolio", tbSerie.Text, tbFolio.Text, "01/01/1900", 0, cbSucursal.SelectedValue)
            Else
                opG = "seriefolio"
                serieG = tbSerie.Text
                folioG = 0
                fechaG = "01/01/1900"
                contratoG = 0
                BuscaFactura("seriefolio", tbSerie.Text, 0, "01/01/1900", 0, cbSucursal.SelectedValue)
            End If
        End If
    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        If tbFecha.Text.Trim.Length > 0 Then
            Try
                opG = "fecha"
                serieG = ""
                folioG = 0
                fechaG = tbFecha.Text
                contratoG = 0
                BuscaFactura("fecha", "", 0, tbFecha.Text, 0, cbSucursal.SelectedValue)
            Catch ex As Exception

            End Try
        End If
    End Sub

    Private Sub SplitContainer1_Panel2_Paint(sender As Object, e As PaintEventArgs) Handles SplitContainer1.Panel2.Paint

    End Sub

    Private Sub DataGridView1_SelectionChanged(sender As Object, e As EventArgs) Handles DataGridView1.SelectionChanged
        Try
            lbSerie.Text = DataGridView1.SelectedCells(2).Value.ToString
            lbFolio.Text = DataGridView1.SelectedCells(3).Value.ToString
            lbFecha.Text = DataGridView1.SelectedCells(4).Value.ToString
            lbContrato.Text = DataGridView1.SelectedCells(5).Value.ToString
            lbImporte.Text = DataGridView1.SelectedCells(6).Value.ToString
            lbNombre.Text = DataGridView1.SelectedCells(7).Value.ToString
            lbStatus.Text = DataGridView1.SelectedCells(1).Value.ToString
        Catch ex As Exception

        End Try
    End Sub

    Private Sub Button5_Click(sender As Object, e As EventArgs) Handles Button5.Click
        Me.Close()
    End Sub

    Private Sub Button8_Click(sender As Object, e As EventArgs) Handles Button8.Click
        If DataGridView1.Rows.Count = 0 Then
            Exit Sub
        End If
        If IsNumeric(DataGridView1.SelectedCells(0).Value) = True Then
            eReImprimirF = 1
            eCveFactura = DataGridView1.SelectedCells(0).Value
            Dim array As String() = DataGridView1.SelectedCells(5).Value.ToString.Split("-")
            GloIdCompania = array(1)
            GloContrato = array(0)
            Dim FRMMCF As New FrmMotivoCancelacionFactura
            FRMMCF.ShowDialog()
            If EClvMotCan > 0 Then
                BaseII.limpiaParametros()
                BaseII.CreateMyParameter("@contratoCompania", SqlDbType.Int, CInt(array(0)))
                BaseII.CreateMyParameter("@idcompania", SqlDbType.Int, CInt(array(1)))
                BaseII.CreateMyParameter("@contrato", ParameterDirection.Output, SqlDbType.Int)
                BaseII.ProcedimientoOutPut("sp_dameContrato")
                bitsist(LocLoginUsuario, BaseII.dicoPar("@contrato").ToString, "Facturación", Me.Text, "Se reimprimio la factura" + eCveFactura.ToString(), "Activa", "Cancelada", SubCiudad)
                'Validamos que no sea fiscal
                BaseII.limpiaParametros()
                BaseII.CreateMyParameter("@Clv_factura", SqlDbType.Int, DataGridView1.SelectedCells(0).Value)
                BaseII.CreateMyParameter("@fiscal", ParameterDirection.Output, SqlDbType.Int)
                BaseII.ProcedimientoOutPut("ValidaFacturaFiscal")
                If BaseII.dicoPar("@fiscal") = 0 Then
                    'Lo mostramos en pantalla
                    If DataGridView1.SelectedCells(8).Value.ToString = "N" Then
                        gloClvNota = DataGridView1.SelectedCells(0).Value
                        LocBndNotasReporteTick = True
                        locoprepnotas = 1
                        FrmImprimirRepGral.Show()
                    Else

                        GloClv_Factura = DataGridView1.SelectedCells(0).Value
                        If IdSistema = "LO" Then
                            LiTipo = 6
                        Else
                            LiTipo = 2
                        End If
                        Dim FrmImp2 As New FrmImprimir
                        FrmImp2.Show()
                    End If
                Else
                    If GloActivarCFD = 1 Then
                        Try
                            ImprimeFacturaDigital(eCveFactura)
                        Catch ex As Exception

                        End Try
                    End If
                End If
        End If
        Else
            MsgBox("Seleccione el Ticket ", MsgBoxStyle.Information)
        End If
    End Sub

    Private Sub cbSucursal_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cbSucursal.SelectedIndexChanged
        Try
            opG = "tipo"
            serieG = ""
            folioG = 0
            fechaG = "01/01/1900"
            contratoG = 0
            BuscaFactura("tipo", "", 0, "01/01/1900", 0, cbSucursal.SelectedValue)
        Catch ex As Exception

        End Try
    End Sub

    Private Sub btnContrato_Click(sender As Object, e As EventArgs) Handles btnContrato.Click
        Try
            Dim arr() = tbContrato.Text.Trim.Split("-")
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@contratoCompania", SqlDbType.Int, CInt(arr(0)))
            BaseII.CreateMyParameter("@idcompania", SqlDbType.Int, CInt(arr(1)))
            BaseII.CreateMyParameter("@contrato", ParameterDirection.Output, SqlDbType.Int)
            BaseII.ProcedimientoOutPut("sp_dameContrato")
            opG = "contrato"
            serieG = ""
            folioG = 0
            fechaG = "01/01/1900"
            contratoG = BaseII.dicoPar("@contrato")
            BuscaFactura("contrato", "", 0, "01/01/1900", BaseII.dicoPar("@contrato"), cbSucursal.SelectedValue)
        Catch ex As Exception

        End Try
    End Sub

    Private Sub tbContrato_KeyPress(sender As Object, e As KeyPressEventArgs) Handles tbContrato.KeyPress
        If Asc(e.KeyChar) = 13 Then
            Try
                Dim arr() = tbContrato.Text.Trim.Split("-")
                BaseII.limpiaParametros()
                BaseII.CreateMyParameter("@contratoCompania", SqlDbType.Int, CInt(arr(0)))
                BaseII.CreateMyParameter("@idcompania", SqlDbType.Int, CInt(arr(1)))
                BaseII.CreateMyParameter("@contrato", ParameterDirection.Output, SqlDbType.Int)
                BaseII.ProcedimientoOutPut("sp_dameContrato")
                opG = "contrato"
                serieG = ""
                folioG = 0
                fechaG = "01/01/1900"
                contratoG = BaseII.dicoPar("@contrato")
                BuscaFactura("contrato", "", 0, "01/01/1900", BaseII.dicoPar("@contrato"), cbSucursal.SelectedValue)
            Catch ex As Exception

            End Try
        End If
    End Sub

    Private Sub ImprimeFacturaDigital(ByVal oLocClv_Factura As Integer)
        FacturacionDigitalSoftv.ClassCFDI.MiConexion = MiConexion
        Dim identi As Integer = 0
        FacturacionDigitalSoftv.ClassCFDI.Locop = 1
        FacturacionDigitalSoftv.ClassCFDI.DameId_FacturaCDF(oLocClv_Factura, "N", MiConexion)
        Try
            If FacturacionDigitalSoftv.ClassCFDI.GloClv_FacturaCFD > 0 Then
                Dim frm As New FacturacionDigitalSoftv.FrmImprimir
                frm.ShowDialog()
                'Else
                '    MsgBox("No se genero la factura digital cancele y vuelva a intentar por favor")
            End If
            'FormPruebaDigital.Show()
        Catch ex As Exception
        End Try
        FacturacionDigitalSoftv.ClassCFDI.Locop = 0
        FacturacionDigitalSoftv.ClassCFDI.locID_Compania_Mizart = ""
        FacturacionDigitalSoftv.ClassCFDI.locID_Sucursal_Mizart = ""
    End Sub

    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        If DataGridView1.SelectedCells(8).Value.ToString = "Cancelada" Then
            Exit Sub
        End If
        Dim resp As MsgBoxResult
        Dim eRes As Integer = 0
        Dim eMsg As String = Nothing

        If IsNumeric(DataGridView1.SelectedCells(0).Value) = False Then
            MsgBox("Seleccione el Ticket ", MsgBoxStyle.Information)
            Exit Sub
        End If
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@Clv_Factura", SqlDbType.BigInt, DataGridView1.SelectedCells(0).Value)
        BaseII.CreateMyParameter("@Res", ParameterDirection.Output, SqlDbType.BigInt)
        BaseII.CreateMyParameter("@Msg", ParameterDirection.Output, SqlDbType.VarChar, 250)
        BaseII.ProcedimientoOutPut("ValidaCancelacionFactura")
        eRes = BaseII.dicoPar("@Res")
        eMsg = BaseII.dicoPar("@Msg")
        If eRes = 1 Then
            MsgBox(eMsg, MsgBoxStyle.Information)
            Exit Sub
        End If
        resp = MsgBox("¿Deseas cancelar el " & Me.cbTipo.Text & " : " & DataGridView1.SelectedCells(2).Value & "-" & DataGridView1.SelectedCells(3).Value & "?", MsgBoxStyle.OkCancel, "Cancelaciòn de Tickets")
        If resp = MsgBoxResult.Ok Then
            eCveFactura = DataGridView1.SelectedCells(0).Value
            'Variable que me permite saber si se cancela o se reimprime factura
            eReImprimirF = 0
            Dim FmMCF As New FrmMotivoCancelacionFactura
            FmMCF.ShowDialog()
            'Cancelamos la factura
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@Clv_factura", SqlDbType.BigInt, eCveFactura)
            BaseII.CreateMyParameter("@op", SqlDbType.Int, 0)
            BaseII.CreateMyParameter("@Msg", ParameterDirection.Output, SqlDbType.VarChar, 250)
            BaseII.CreateMyParameter("@BndError", ParameterDirection.Output, SqlDbType.Int)
            BaseII.ProcedimientoOutPut("CANCELACIONFACTURAS")
            'Guardamos en bitácora
            Dim arr() = DataGridView1.SelectedCells(5).Value.ToString.Split("-")
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@contratoCompania", SqlDbType.Int, CInt(arr(0)))
            BaseII.CreateMyParameter("@idcompania", SqlDbType.Int, CInt(arr(1)))
            BaseII.CreateMyParameter("@contrato", ParameterDirection.Output, SqlDbType.Int)
            BaseII.ProcedimientoOutPut("sp_dameContrato")
            bitsist(LocLoginUsuario, BaseII.dicoPar("@contrato"), "Facturación", Me.Text, "Se canceló la factura" + eCveFactura.ToString(), "Activa", "Cancelada", SubCiudad)

            If GloActivarCFD = 1 Then
                CancelaFacturaDigital(eCveFactura)
            End If
            BuscaFactura(opG, serieG, folioG, fechaG, contratoG, cbSucursal.SelectedValue)
        End If
    End Sub

    Private Sub Button3_Click(sender As Object, e As EventArgs) Handles Button3.Click
        If DataGridView1.SelectedCells(8).Value.ToString = "Cancelada" Then
            Exit Sub
        End If
        If IsNumeric(DataGridView1.SelectedCells(0).Value) = False Then
            MsgBox("Seleccione el Ticket ", MsgBoxStyle.Information)
            Exit Sub
        End If
        If GloActivarCFD = 1 Then
            EnviaCorreoFacturaDigital(DataGridView1.SelectedCells(0).Value)
        End If
    End Sub

    Private Sub EnviaCorreoFacturaDigital(ByVal oLocClv_Factura As Integer)
        FacturacionDigitalSoftv.ClassCFDI.MiConexion = MiConexion
        Dim identi As Integer = 0
        FacturacionDigitalSoftv.ClassCFDI.Locop = 1
        FacturacionDigitalSoftv.ClassCFDI.DameId_FacturaCDF(oLocClv_Factura, "N", MiConexion)
        Try
            If FacturacionDigitalSoftv.ClassCFDI.GloClv_FacturaCFD > 0 Then
                FacturacionDigitalSoftv.EnviaCorreo.EnviarCorreo(FacturacionDigitalSoftv.ClassCFDI.GloClv_FacturaCFD)
            Else
                MsgBox("No se envio el correo cancele y vuelva a intentar por favor")
            End If
            'FormPruebaDigital.Show()
        Catch ex As Exception
        End Try
        FacturacionDigitalSoftv.ClassCFDI.Locop = 0
        FacturacionDigitalSoftv.ClassCFDI.locID_Compania_Mizart = ""
        FacturacionDigitalSoftv.ClassCFDI.locID_Sucursal_Mizart = ""
    End Sub

    Private Sub btnRefacturacion_Click(sender As Object, e As EventArgs) Handles btnRefacturacion.Click
        If DataGridView1.SelectedCells(8).Value.ToString = "Cancelada" Then
            Exit Sub
        End If
        If IsNumeric(DataGridView1.SelectedCells(0).Value) = False Then
            MsgBox("Seleccione el Ticket ", MsgBoxStyle.Information)
            Exit Sub
        End If
        Dim resp As MsgBoxResult
        Dim eRes As Integer = 0
        Dim eMsg As String = Nothing

        eCveFactura = DataGridView1.SelectedCells(0).Value
        eRes = ValidaReFActura(DataGridView1.SelectedCells(0).Value)
        If eRes = 0 Then
            resp = MsgBox("Deseas Refacturar el ticket : " & Me.lbSerie.Text & "-" & Me.lbFolio.Text, MsgBoxStyle.OkCancel, "Refacturación de Tickets")
            If resp = MsgBoxResult.Ok Then

                'Primero si tiene una Factura Fiscal la Cancelamos
                If GloActivarCFD = 1 Then
                    CancelaFacturaDigital(eCveFactura)
                End If
                'Ahora Vamos hacer la Factura Digital Otra vez
                If GloActivarCFD = 1 Then
                    Try
                        HazFacturaDigital(eCveFactura)
                    Catch ex As Exception
                        MsgBox(ex.Message)
                    End Try

                End If
                Dim arr() = lbContrato.Text.Split("-")
                BaseII.limpiaParametros()
                BaseII.CreateMyParameter("@contratoCompania", SqlDbType.Int, CInt(arr(0)))
                BaseII.CreateMyParameter("@idcompania", SqlDbType.Int, CInt(arr(1)))
                BaseII.CreateMyParameter("@contrato", ParameterDirection.Output, SqlDbType.Int)
                BaseII.ProcedimientoOutPut("sp_dameContrato")

                bitsist(LocLoginUsuario, BaseII.dicoPar("@contrato"), "Facturación", Me.Text, "Se Refacturar la factura" + eCveFactura.ToString(), "Refacturar", "Refacturar", SubCiudad)

            End If
        End If
    End Sub

    Public Function ValidaReFActura(oClvFactura As Long) As Integer
        ValidaReFActura = 0
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@Clv_Factura", SqlDbType.BigInt, oClvFactura)
        BaseII.CreateMyParameter("@Res", ParameterDirection.Output, SqlDbType.Int)
        BaseII.CreateMyParameter("@Msg", ParameterDirection.Output, SqlDbType.VarChar, 250)
        BaseII.ProcedimientoOutPut("ValidaReFactura")
        ValidaReFActura = BaseII.dicoPar("@Res")
        If BaseII.dicoPar("@Res") > 0 Then
            MsgBox(BaseII.dicoPar("@Msg"))
        End If
    End Function

    Private Sub CancelaFacturaDigital(ByVal oLocClv_Factura As Integer)
        FacturacionDigitalSoftv.ClassCFDI.MiConexion = MiConexion
        Dim identi As Integer = 0
        FacturacionDigitalSoftv.ClassCFDI.Locop = 1
        FacturacionDigitalSoftv.ClassCFDI.DameId_FacturaCDF(oLocClv_Factura, "N", MiConexion)
        Try
            If FacturacionDigitalSoftv.ClassCFDI.GloClv_FacturaCFD > 0 Then
                FacturacionDigitalSoftv.ClassCFDI.Cancelacion_FacturaCFD(FacturacionDigitalSoftv.ClassCFDI.GloClv_FacturaCFD, MiConexion)

            End If
            'FormPruebaDigital.Show()
        Catch ex As Exception
        End Try
        FacturacionDigitalSoftv.ClassCFDI.Locop = 0
        FacturacionDigitalSoftv.ClassCFDI.locID_Compania_Mizart = ""
        FacturacionDigitalSoftv.ClassCFDI.locID_Sucursal_Mizart = ""
    End Sub

    Private Sub HazFacturaDigital(ByVal LocClv_Factura As Long)
        Try
            FacturacionDigitalSoftv.ClassCFDI.MiConexion = MiConexion
            Dim identi As Integer = 0
            FacturacionDigitalSoftv.ClassCFDI.EsTimbrePrueba = eEsTimbrePrueba
            identi = 0
            identi = FacturacionDigitalSoftv.ClassCFDI.BusFacFiscalOledb(LocClv_Factura, MiConexion)
            'fin Facturacion Digital
            FacturacionDigitalSoftv.ClassCFDI.locID_Compania_Mizart = ""
            FacturacionDigitalSoftv.ClassCFDI.locID_Sucursal_Mizart = ""

            If CInt(identi) > 0 Then
                FacturacionDigitalSoftv.ClassCFDI.Locop = 0
                FacturacionDigitalSoftv.ClassCFDI.Dime_Aque_Compania_Facturarle(LocClv_Factura, MiConexion)
                FacturacionDigitalSoftv.ClassCFDI.Graba_Factura_Digital(LocClv_Factura, identi, MiConexion)
                Try
                    If FacturacionDigitalSoftv.ClassCFDI.GloClv_FacturaCFD > 0 Then
                        Dim frm As New FacturacionDigitalSoftv.FrmImprimir
                        frm.ShowDialog()
                    Else
                        MsgBox("No se genero la factura digital cancele y vuelva a intentar por favor")
                    End If
                    'FormPruebaDigital.Show()
                Catch ex As Exception
                End Try
                FacturacionDigitalSoftv.ClassCFDI.locID_Compania_Mizart = ""
                FacturacionDigitalSoftv.ClassCFDI.locID_Sucursal_Mizart = ""

            End If
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try

    End Sub
End Class