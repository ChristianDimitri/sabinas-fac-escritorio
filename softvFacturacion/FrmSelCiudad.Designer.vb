<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmSelCiudad
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim Clv_CiudadLabel As System.Windows.Forms.Label
        Dim Clv_ciudadLabel1 As System.Windows.Forms.Label
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle4 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.DataGridView1 = New System.Windows.Forms.DataGridView()
        Me.NombreDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ClvSessionDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ClvCiudadDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.MuestraSeleccionaCiudadTmpCONSULTABindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.DataSetEdgar = New softvFacturacion.DataSetEdgar()
        Me.DataGridView2 = New System.Windows.Forms.DataGridView()
        Me.NOMBREDataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ClvsessionDataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ClvciudadDataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.MuestraSeleccionCiudadCONSULTABindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Button1 = New System.Windows.Forms.Button()
        Me.Button2 = New System.Windows.Forms.Button()
        Me.Button3 = New System.Windows.Forms.Button()
        Me.Button4 = New System.Windows.Forms.Button()
        Me.Button5 = New System.Windows.Forms.Button()
        Me.Button6 = New System.Windows.Forms.Button()
        Me.DameClv_Session_ServiciosBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.DameClv_Session_ServiciosTableAdapter = New softvFacturacion.DataSetEdgarTableAdapters.DameClv_Session_ServiciosTableAdapter()
        Me.MuestraSelecciona_CiudadTmpCONSULTATableAdapter = New softvFacturacion.DataSetEdgarTableAdapters.MuestraSelecciona_CiudadTmpCONSULTATableAdapter()
        Me.MuestraSeleccion_CiudadCONSULTATableAdapter = New softvFacturacion.DataSetEdgarTableAdapters.MuestraSeleccion_CiudadCONSULTATableAdapter()
        Me.PONTODOSSeleccion_Ciudad_Selecciona_CiudadTmpBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.PONTODOSSeleccion_Ciudad_Selecciona_CiudadTmpTableAdapter = New softvFacturacion.DataSetEdgarTableAdapters.PONTODOSSeleccion_Ciudad_Selecciona_CiudadTmpTableAdapter()
        Me.PONTODOSSelecciona_CiudadTmp_Seleccion_CiudadBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.PONTODOSSelecciona_CiudadTmp_Seleccion_CiudadTableAdapter = New softvFacturacion.DataSetEdgarTableAdapters.PONTODOSSelecciona_CiudadTmp_Seleccion_CiudadTableAdapter()
        Me.PONUNOSeleccion_Ciudad_Selecciona_CiudadTmpBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.PONUNOSeleccion_Ciudad_Selecciona_CiudadTmpTableAdapter = New softvFacturacion.DataSetEdgarTableAdapters.PONUNOSeleccion_Ciudad_Selecciona_CiudadTmpTableAdapter()
        Me.PONUNOSelecciona_CiudadTmp_Seleccion_CiudadBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.PONUNOSelecciona_CiudadTmp_Seleccion_CiudadTableAdapter = New softvFacturacion.DataSetEdgarTableAdapters.PONUNOSelecciona_CiudadTmp_Seleccion_CiudadTableAdapter()
        Me.Clv_CiudadTextBox = New System.Windows.Forms.TextBox()
        Me.Clv_ciudadTextBox1 = New System.Windows.Forms.TextBox()
        Me.MuestraSelecciona_CiudadTmpNUEVOBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.MuestraSelecciona_CiudadTmpNUEVOTableAdapter = New softvFacturacion.DataSetEdgarTableAdapters.MuestraSelecciona_CiudadTmpNUEVOTableAdapter()
        Me.Procedimientos_arnoldo = New softvFacturacion.Procedimientos_arnoldo()
        Me.Dame_ciudad_carteraBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Dame_ciudad_carteraTableAdapter = New softvFacturacion.Procedimientos_arnoldoTableAdapters.Dame_ciudad_carteraTableAdapter()
        Clv_CiudadLabel = New System.Windows.Forms.Label()
        Clv_ciudadLabel1 = New System.Windows.Forms.Label()
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.MuestraSeleccionaCiudadTmpCONSULTABindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DataSetEdgar, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DataGridView2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.MuestraSeleccionCiudadCONSULTABindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DameClv_Session_ServiciosBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PONTODOSSeleccion_Ciudad_Selecciona_CiudadTmpBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PONTODOSSelecciona_CiudadTmp_Seleccion_CiudadBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PONUNOSeleccion_Ciudad_Selecciona_CiudadTmpBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PONUNOSelecciona_CiudadTmp_Seleccion_CiudadBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.MuestraSelecciona_CiudadTmpNUEVOBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Procedimientos_arnoldo, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Dame_ciudad_carteraBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Clv_CiudadLabel
        '
        Clv_CiudadLabel.AutoSize = True
        Clv_CiudadLabel.Location = New System.Drawing.Point(29, 314)
        Clv_CiudadLabel.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Clv_CiudadLabel.Name = "Clv_CiudadLabel"
        Clv_CiudadLabel.Size = New System.Drawing.Size(79, 17)
        Clv_CiudadLabel.TabIndex = 8
        Clv_CiudadLabel.Text = "Clv Ciudad:"
        '
        'Clv_ciudadLabel1
        '
        Clv_ciudadLabel1.AutoSize = True
        Clv_ciudadLabel1.Location = New System.Drawing.Point(520, 303)
        Clv_ciudadLabel1.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Clv_ciudadLabel1.Name = "Clv_ciudadLabel1"
        Clv_ciudadLabel1.Size = New System.Drawing.Size(75, 17)
        Clv_ciudadLabel1.TabIndex = 10
        Clv_ciudadLabel1.Text = "clv ciudad:"
        '
        'DataGridView1
        '
        Me.DataGridView1.AllowUserToAddRows = False
        Me.DataGridView1.AllowUserToDeleteRows = False
        Me.DataGridView1.AutoGenerateColumns = False
        Me.DataGridView1.BackgroundColor = System.Drawing.Color.WhiteSmoke
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.DataGridView1.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle1
        Me.DataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DataGridView1.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.NombreDataGridViewTextBoxColumn, Me.ClvSessionDataGridViewTextBoxColumn, Me.ClvCiudadDataGridViewTextBoxColumn})
        Me.DataGridView1.DataSource = Me.MuestraSeleccionaCiudadTmpCONSULTABindingSource
        Me.DataGridView1.Location = New System.Drawing.Point(16, 18)
        Me.DataGridView1.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.DataGridView1.Name = "DataGridView1"
        Me.DataGridView1.ReadOnly = True
        Me.DataGridView1.Size = New System.Drawing.Size(325, 367)
        Me.DataGridView1.TabIndex = 0
        '
        'NombreDataGridViewTextBoxColumn
        '
        Me.NombreDataGridViewTextBoxColumn.DataPropertyName = "nombre"
        DataGridViewCellStyle2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.NombreDataGridViewTextBoxColumn.DefaultCellStyle = DataGridViewCellStyle2
        Me.NombreDataGridViewTextBoxColumn.HeaderText = "Ciudad"
        Me.NombreDataGridViewTextBoxColumn.Name = "NombreDataGridViewTextBoxColumn"
        Me.NombreDataGridViewTextBoxColumn.ReadOnly = True
        Me.NombreDataGridViewTextBoxColumn.Width = 200
        '
        'ClvSessionDataGridViewTextBoxColumn
        '
        Me.ClvSessionDataGridViewTextBoxColumn.DataPropertyName = "Clv_Session"
        Me.ClvSessionDataGridViewTextBoxColumn.HeaderText = "Clv_Session"
        Me.ClvSessionDataGridViewTextBoxColumn.Name = "ClvSessionDataGridViewTextBoxColumn"
        Me.ClvSessionDataGridViewTextBoxColumn.ReadOnly = True
        '
        'ClvCiudadDataGridViewTextBoxColumn
        '
        Me.ClvCiudadDataGridViewTextBoxColumn.DataPropertyName = "Clv_Ciudad"
        Me.ClvCiudadDataGridViewTextBoxColumn.HeaderText = "Clv_Ciudad"
        Me.ClvCiudadDataGridViewTextBoxColumn.Name = "ClvCiudadDataGridViewTextBoxColumn"
        Me.ClvCiudadDataGridViewTextBoxColumn.ReadOnly = True
        '
        'MuestraSeleccionaCiudadTmpCONSULTABindingSource
        '
        Me.MuestraSeleccionaCiudadTmpCONSULTABindingSource.DataMember = "MuestraSelecciona_CiudadTmpCONSULTA"
        Me.MuestraSeleccionaCiudadTmpCONSULTABindingSource.DataSource = Me.DataSetEdgar
        '
        'DataSetEdgar
        '
        Me.DataSetEdgar.DataSetName = "DataSetEdgar"
        Me.DataSetEdgar.EnforceConstraints = False
        Me.DataSetEdgar.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'DataGridView2
        '
        Me.DataGridView2.AllowUserToAddRows = False
        Me.DataGridView2.AllowUserToDeleteRows = False
        Me.DataGridView2.AutoGenerateColumns = False
        Me.DataGridView2.BackgroundColor = System.Drawing.Color.WhiteSmoke
        DataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.DataGridView2.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle3
        Me.DataGridView2.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DataGridView2.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.NOMBREDataGridViewTextBoxColumn1, Me.ClvsessionDataGridViewTextBoxColumn1, Me.ClvciudadDataGridViewTextBoxColumn1})
        Me.DataGridView2.DataSource = Me.MuestraSeleccionCiudadCONSULTABindingSource
        Me.DataGridView2.Location = New System.Drawing.Point(511, 15)
        Me.DataGridView2.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.DataGridView2.Name = "DataGridView2"
        Me.DataGridView2.ReadOnly = True
        Me.DataGridView2.Size = New System.Drawing.Size(325, 367)
        Me.DataGridView2.TabIndex = 1
        '
        'NOMBREDataGridViewTextBoxColumn1
        '
        Me.NOMBREDataGridViewTextBoxColumn1.DataPropertyName = "NOMBRE"
        DataGridViewCellStyle4.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.NOMBREDataGridViewTextBoxColumn1.DefaultCellStyle = DataGridViewCellStyle4
        Me.NOMBREDataGridViewTextBoxColumn1.HeaderText = "Ciudad"
        Me.NOMBREDataGridViewTextBoxColumn1.Name = "NOMBREDataGridViewTextBoxColumn1"
        Me.NOMBREDataGridViewTextBoxColumn1.ReadOnly = True
        Me.NOMBREDataGridViewTextBoxColumn1.Width = 200
        '
        'ClvsessionDataGridViewTextBoxColumn1
        '
        Me.ClvsessionDataGridViewTextBoxColumn1.DataPropertyName = "clv_session"
        Me.ClvsessionDataGridViewTextBoxColumn1.HeaderText = "clv_session"
        Me.ClvsessionDataGridViewTextBoxColumn1.Name = "ClvsessionDataGridViewTextBoxColumn1"
        Me.ClvsessionDataGridViewTextBoxColumn1.ReadOnly = True
        '
        'ClvciudadDataGridViewTextBoxColumn1
        '
        Me.ClvciudadDataGridViewTextBoxColumn1.DataPropertyName = "clv_ciudad"
        Me.ClvciudadDataGridViewTextBoxColumn1.HeaderText = "clv_ciudad"
        Me.ClvciudadDataGridViewTextBoxColumn1.Name = "ClvciudadDataGridViewTextBoxColumn1"
        Me.ClvciudadDataGridViewTextBoxColumn1.ReadOnly = True
        '
        'MuestraSeleccionCiudadCONSULTABindingSource
        '
        Me.MuestraSeleccionCiudadCONSULTABindingSource.DataMember = "MuestraSeleccion_CiudadCONSULTA"
        Me.MuestraSeleccionCiudadCONSULTABindingSource.DataSource = Me.DataSetEdgar
        '
        'Button1
        '
        Me.Button1.BackColor = System.Drawing.Color.DarkRed
        Me.Button1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button1.ForeColor = System.Drawing.Color.White
        Me.Button1.Location = New System.Drawing.Point(367, 20)
        Me.Button1.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(121, 36)
        Me.Button1.TabIndex = 2
        Me.Button1.Text = "&Agregar >"
        Me.Button1.UseVisualStyleBackColor = False
        '
        'Button2
        '
        Me.Button2.BackColor = System.Drawing.Color.DarkRed
        Me.Button2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button2.ForeColor = System.Drawing.Color.White
        Me.Button2.Location = New System.Drawing.Point(367, 63)
        Me.Button2.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(121, 53)
        Me.Button2.TabIndex = 3
        Me.Button2.Text = "Agregar  &Todo >>"
        Me.Button2.UseVisualStyleBackColor = False
        '
        'Button3
        '
        Me.Button3.BackColor = System.Drawing.Color.DarkRed
        Me.Button3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button3.ForeColor = System.Drawing.Color.White
        Me.Button3.Location = New System.Drawing.Point(367, 180)
        Me.Button3.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Button3.Name = "Button3"
        Me.Button3.Size = New System.Drawing.Size(121, 59)
        Me.Button3.TabIndex = 5
        Me.Button3.Text = "<<Quitar To&do"
        Me.Button3.UseVisualStyleBackColor = False
        '
        'Button4
        '
        Me.Button4.BackColor = System.Drawing.Color.DarkRed
        Me.Button4.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button4.ForeColor = System.Drawing.Color.White
        Me.Button4.Location = New System.Drawing.Point(367, 137)
        Me.Button4.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Button4.Name = "Button4"
        Me.Button4.Size = New System.Drawing.Size(121, 36)
        Me.Button4.TabIndex = 4
        Me.Button4.Text = "< &Quitar"
        Me.Button4.UseVisualStyleBackColor = False
        '
        'Button5
        '
        Me.Button5.BackColor = System.Drawing.Color.DarkOrange
        Me.Button5.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button5.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button5.ForeColor = System.Drawing.Color.Black
        Me.Button5.Location = New System.Drawing.Point(655, 410)
        Me.Button5.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Button5.Name = "Button5"
        Me.Button5.Size = New System.Drawing.Size(181, 44)
        Me.Button5.TabIndex = 6
        Me.Button5.Text = "&CERRAR"
        Me.Button5.UseVisualStyleBackColor = False
        '
        'Button6
        '
        Me.Button6.BackColor = System.Drawing.Color.DarkOrange
        Me.Button6.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button6.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button6.ForeColor = System.Drawing.Color.Black
        Me.Button6.Location = New System.Drawing.Point(465, 410)
        Me.Button6.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Button6.Name = "Button6"
        Me.Button6.Size = New System.Drawing.Size(181, 44)
        Me.Button6.TabIndex = 7
        Me.Button6.Text = "&ACEPTAR"
        Me.Button6.UseVisualStyleBackColor = False
        '
        'DameClv_Session_ServiciosBindingSource
        '
        Me.DameClv_Session_ServiciosBindingSource.DataMember = "DameClv_Session_Servicios"
        Me.DameClv_Session_ServiciosBindingSource.DataSource = Me.DataSetEdgar
        '
        'DameClv_Session_ServiciosTableAdapter
        '
        Me.DameClv_Session_ServiciosTableAdapter.ClearBeforeFill = True
        '
        'MuestraSelecciona_CiudadTmpCONSULTATableAdapter
        '
        Me.MuestraSelecciona_CiudadTmpCONSULTATableAdapter.ClearBeforeFill = True
        '
        'MuestraSeleccion_CiudadCONSULTATableAdapter
        '
        Me.MuestraSeleccion_CiudadCONSULTATableAdapter.ClearBeforeFill = True
        '
        'PONTODOSSeleccion_Ciudad_Selecciona_CiudadTmpBindingSource
        '
        Me.PONTODOSSeleccion_Ciudad_Selecciona_CiudadTmpBindingSource.DataMember = "PONTODOSSeleccion_Ciudad_Selecciona_CiudadTmp"
        Me.PONTODOSSeleccion_Ciudad_Selecciona_CiudadTmpBindingSource.DataSource = Me.DataSetEdgar
        '
        'PONTODOSSeleccion_Ciudad_Selecciona_CiudadTmpTableAdapter
        '
        Me.PONTODOSSeleccion_Ciudad_Selecciona_CiudadTmpTableAdapter.ClearBeforeFill = True
        '
        'PONTODOSSelecciona_CiudadTmp_Seleccion_CiudadBindingSource
        '
        Me.PONTODOSSelecciona_CiudadTmp_Seleccion_CiudadBindingSource.DataMember = "PONTODOSSelecciona_CiudadTmp_Seleccion_Ciudad"
        Me.PONTODOSSelecciona_CiudadTmp_Seleccion_CiudadBindingSource.DataSource = Me.DataSetEdgar
        '
        'PONTODOSSelecciona_CiudadTmp_Seleccion_CiudadTableAdapter
        '
        Me.PONTODOSSelecciona_CiudadTmp_Seleccion_CiudadTableAdapter.ClearBeforeFill = True
        '
        'PONUNOSeleccion_Ciudad_Selecciona_CiudadTmpBindingSource
        '
        Me.PONUNOSeleccion_Ciudad_Selecciona_CiudadTmpBindingSource.DataMember = "PONUNOSeleccion_Ciudad_Selecciona_CiudadTmp"
        Me.PONUNOSeleccion_Ciudad_Selecciona_CiudadTmpBindingSource.DataSource = Me.DataSetEdgar
        '
        'PONUNOSeleccion_Ciudad_Selecciona_CiudadTmpTableAdapter
        '
        Me.PONUNOSeleccion_Ciudad_Selecciona_CiudadTmpTableAdapter.ClearBeforeFill = True
        '
        'PONUNOSelecciona_CiudadTmp_Seleccion_CiudadBindingSource
        '
        Me.PONUNOSelecciona_CiudadTmp_Seleccion_CiudadBindingSource.DataMember = "PONUNOSelecciona_CiudadTmp_Seleccion_Ciudad"
        Me.PONUNOSelecciona_CiudadTmp_Seleccion_CiudadBindingSource.DataSource = Me.DataSetEdgar
        '
        'PONUNOSelecciona_CiudadTmp_Seleccion_CiudadTableAdapter
        '
        Me.PONUNOSelecciona_CiudadTmp_Seleccion_CiudadTableAdapter.ClearBeforeFill = True
        '
        'Clv_CiudadTextBox
        '
        Me.Clv_CiudadTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.MuestraSeleccionaCiudadTmpCONSULTABindingSource, "Clv_Ciudad", True))
        Me.Clv_CiudadTextBox.Location = New System.Drawing.Point(108, 268)
        Me.Clv_CiudadTextBox.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Clv_CiudadTextBox.Name = "Clv_CiudadTextBox"
        Me.Clv_CiudadTextBox.Size = New System.Drawing.Size(132, 22)
        Me.Clv_CiudadTextBox.TabIndex = 9
        '
        'Clv_ciudadTextBox1
        '
        Me.Clv_ciudadTextBox1.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.MuestraSeleccionCiudadCONSULTABindingSource, "clv_ciudad", True))
        Me.Clv_ciudadTextBox1.Location = New System.Drawing.Point(607, 294)
        Me.Clv_ciudadTextBox1.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Clv_ciudadTextBox1.Name = "Clv_ciudadTextBox1"
        Me.Clv_ciudadTextBox1.Size = New System.Drawing.Size(132, 22)
        Me.Clv_ciudadTextBox1.TabIndex = 11
        '
        'MuestraSelecciona_CiudadTmpNUEVOBindingSource
        '
        Me.MuestraSelecciona_CiudadTmpNUEVOBindingSource.DataMember = "MuestraSelecciona_CiudadTmpNUEVO"
        Me.MuestraSelecciona_CiudadTmpNUEVOBindingSource.DataSource = Me.DataSetEdgar
        '
        'MuestraSelecciona_CiudadTmpNUEVOTableAdapter
        '
        Me.MuestraSelecciona_CiudadTmpNUEVOTableAdapter.ClearBeforeFill = True
        '
        'Procedimientos_arnoldo
        '
        Me.Procedimientos_arnoldo.DataSetName = "Procedimientos_arnoldo"
        Me.Procedimientos_arnoldo.EnforceConstraints = False
        Me.Procedimientos_arnoldo.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'Dame_ciudad_carteraBindingSource
        '
        Me.Dame_ciudad_carteraBindingSource.DataMember = "Dame_ciudad_cartera"
        Me.Dame_ciudad_carteraBindingSource.DataSource = Me.Procedimientos_arnoldo
        '
        'Dame_ciudad_carteraTableAdapter
        '
        Me.Dame_ciudad_carteraTableAdapter.ClearBeforeFill = True
        '
        'FrmSelCiudad
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.AutoScroll = True
        Me.ClientSize = New System.Drawing.Size(863, 466)
        Me.Controls.Add(Me.DataGridView2)
        Me.Controls.Add(Me.DataGridView1)
        Me.Controls.Add(Clv_ciudadLabel1)
        Me.Controls.Add(Me.Clv_ciudadTextBox1)
        Me.Controls.Add(Clv_CiudadLabel)
        Me.Controls.Add(Me.Clv_CiudadTextBox)
        Me.Controls.Add(Me.Button6)
        Me.Controls.Add(Me.Button5)
        Me.Controls.Add(Me.Button3)
        Me.Controls.Add(Me.Button4)
        Me.Controls.Add(Me.Button2)
        Me.Controls.Add(Me.Button1)
        Me.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Name = "FrmSelCiudad"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Seleccione la Ciudad"
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.MuestraSeleccionaCiudadTmpCONSULTABindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DataSetEdgar, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DataGridView2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.MuestraSeleccionCiudadCONSULTABindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DameClv_Session_ServiciosBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PONTODOSSeleccion_Ciudad_Selecciona_CiudadTmpBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PONTODOSSelecciona_CiudadTmp_Seleccion_CiudadBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PONUNOSeleccion_Ciudad_Selecciona_CiudadTmpBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PONUNOSelecciona_CiudadTmp_Seleccion_CiudadBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.MuestraSelecciona_CiudadTmpNUEVOBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Procedimientos_arnoldo, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Dame_ciudad_carteraBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents DataGridView1 As System.Windows.Forms.DataGridView
    Friend WithEvents DataGridView2 As System.Windows.Forms.DataGridView
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents Button2 As System.Windows.Forms.Button
    Friend WithEvents Button3 As System.Windows.Forms.Button
    Friend WithEvents Button4 As System.Windows.Forms.Button
    Friend WithEvents Button5 As System.Windows.Forms.Button
    Friend WithEvents Button6 As System.Windows.Forms.Button
    Friend WithEvents DataSetEdgar As softvFacturacion.DataSetEdgar
    Friend WithEvents DameClv_Session_ServiciosBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents DameClv_Session_ServiciosTableAdapter As softvFacturacion.DataSetEdgarTableAdapters.DameClv_Session_ServiciosTableAdapter
    Friend WithEvents NombreDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ClvSessionDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ClvCiudadDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents MuestraSeleccionaCiudadTmpCONSULTABindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents MuestraSeleccionCiudadCONSULTABindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents MuestraSelecciona_CiudadTmpCONSULTATableAdapter As softvFacturacion.DataSetEdgarTableAdapters.MuestraSelecciona_CiudadTmpCONSULTATableAdapter
    Friend WithEvents MuestraSeleccion_CiudadCONSULTATableAdapter As softvFacturacion.DataSetEdgarTableAdapters.MuestraSeleccion_CiudadCONSULTATableAdapter
    Friend WithEvents NOMBREDataGridViewTextBoxColumn1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ClvsessionDataGridViewTextBoxColumn1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ClvciudadDataGridViewTextBoxColumn1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents PONTODOSSeleccion_Ciudad_Selecciona_CiudadTmpBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents PONTODOSSeleccion_Ciudad_Selecciona_CiudadTmpTableAdapter As softvFacturacion.DataSetEdgarTableAdapters.PONTODOSSeleccion_Ciudad_Selecciona_CiudadTmpTableAdapter
    Friend WithEvents PONTODOSSelecciona_CiudadTmp_Seleccion_CiudadBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents PONTODOSSelecciona_CiudadTmp_Seleccion_CiudadTableAdapter As softvFacturacion.DataSetEdgarTableAdapters.PONTODOSSelecciona_CiudadTmp_Seleccion_CiudadTableAdapter
    Friend WithEvents PONUNOSeleccion_Ciudad_Selecciona_CiudadTmpBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents PONUNOSeleccion_Ciudad_Selecciona_CiudadTmpTableAdapter As softvFacturacion.DataSetEdgarTableAdapters.PONUNOSeleccion_Ciudad_Selecciona_CiudadTmpTableAdapter
    Friend WithEvents PONUNOSelecciona_CiudadTmp_Seleccion_CiudadBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents PONUNOSelecciona_CiudadTmp_Seleccion_CiudadTableAdapter As softvFacturacion.DataSetEdgarTableAdapters.PONUNOSelecciona_CiudadTmp_Seleccion_CiudadTableAdapter
    Friend WithEvents Clv_CiudadTextBox As System.Windows.Forms.TextBox
    Friend WithEvents Clv_ciudadTextBox1 As System.Windows.Forms.TextBox
    Friend WithEvents MuestraSelecciona_CiudadTmpNUEVOBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents MuestraSelecciona_CiudadTmpNUEVOTableAdapter As softvFacturacion.DataSetEdgarTableAdapters.MuestraSelecciona_CiudadTmpNUEVOTableAdapter
    Friend WithEvents Procedimientos_arnoldo As softvFacturacion.Procedimientos_arnoldo
    Friend WithEvents Dame_ciudad_carteraBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Dame_ciudad_carteraTableAdapter As softvFacturacion.Procedimientos_arnoldoTableAdapters.Dame_ciudad_carteraTableAdapter
End Class
