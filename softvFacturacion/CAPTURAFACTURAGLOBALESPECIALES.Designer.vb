﻿
<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class CAPTURAFACTURAGLOBALESPECIALES
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.MueastrasucBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.NewsoftvDataSet2 = New softvFacturacion.NewsoftvDataSet2()
        Me.DameFechadelServidorHoraBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.MueastrasucTableAdapter = New softvFacturacion.NewsoftvDataSet2TableAdapters.mueastrasucTableAdapter()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.DameCantidadALetraBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.DameCantidadALetraTableAdapter = New softvFacturacion.NewsoftvDataSet2TableAdapters.DameCantidadALetraTableAdapter()
        Me.DameFechadelServidorHoraTableAdapter = New softvFacturacion.NewsoftvDataSet2TableAdapters.DameFechadelServidorHoraTableAdapter()
        Me.Procedimientos_arnoldo = New softvFacturacion.Procedimientos_arnoldo()
        Me.Llena_Factura_Global_nuevoBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Llena_Factura_Global_nuevoTableAdapter = New softvFacturacion.Procedimientos_arnoldoTableAdapters.Llena_Factura_Global_nuevoTableAdapter()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.Button9 = New System.Windows.Forms.Button()
        Me.ComboBox1 = New System.Windows.Forms.ComboBox()
        Me.TextBox4 = New System.Windows.Forms.TextBox()
        Me.DameImporteSucursalBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.DataSetLydia = New softvFacturacion.DataSetLydia()
        Me.TextBox5 = New System.Windows.Forms.TextBox()
        Me.CMBLabel7 = New System.Windows.Forms.Label()
        Me.CMBLabel1 = New System.Windows.Forms.Label()
        Me.CMBLabel2 = New System.Windows.Forms.Label()
        Me.CMBLabel3 = New System.Windows.Forms.Label()
        Me.CMBLabel4 = New System.Windows.Forms.Label()
        Me.CMBLabel5 = New System.Windows.Forms.Label()
        Me.DateTimePicker1 = New System.Windows.Forms.DateTimePicker()
        Me.NuevafacturaTextBox = New System.Windows.Forms.TextBox()
        Me.SerieTextBox = New System.Windows.Forms.TextBox()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.DateTimePicker2 = New System.Windows.Forms.DateTimePicker()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Con_Factura_GlobalBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Con_Factura_GlobalTableAdapter = New softvFacturacion.Procedimientos_arnoldoTableAdapters.Con_Factura_GlobalTableAdapter()
        Me.Selecciona_impresoraticketsBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Selecciona_impresoraticketsTableAdapter = New softvFacturacion.Procedimientos_arnoldoTableAdapters.Selecciona_impresoraticketsTableAdapter()
        Me.NUEVAFACTGLOBALBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.NUEVAFACTGLOBALTableAdapter = New softvFacturacion.DataSetLydiaTableAdapters.NUEVAFACTGLOBALTableAdapter()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.TextBoxImporte = New System.Windows.Forms.TextBox()
        Me.DameImporteSucursalTableAdapter = New softvFacturacion.DataSetLydiaTableAdapters.DameImporteSucursalTableAdapter()
        Me.TextBoxIEPS = New System.Windows.Forms.TextBox()
        Me.TextBoxIVA = New System.Windows.Forms.TextBox()
        Me.TextBoxSubTotal = New System.Windows.Forms.TextBox()
        Me.VerAcceso2TableAdapter1 = New softvFacturacion.ProcedimientosArnoldo3TableAdapters.VerAcceso2TableAdapter()
        Me.VerAcceso2TableAdapter2 = New softvFacturacion.ProcedimientosArnoldo3TableAdapters.VerAcceso2TableAdapter()
        Me.ComboBox2 = New System.Windows.Forms.ComboBox()
        CType(Me.MueastrasucBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.NewsoftvDataSet2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DameFechadelServidorHoraBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DameCantidadALetraBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Procedimientos_arnoldo, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Llena_Factura_Global_nuevoBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DameImporteSucursalBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DataSetLydia, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel1.SuspendLayout()
        CType(Me.Con_Factura_GlobalBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Selecciona_impresoraticketsBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.NUEVAFACTGLOBALBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox1.SuspendLayout()
        Me.SuspendLayout()
        '
        'MueastrasucBindingSource
        '
        Me.MueastrasucBindingSource.DataMember = "mueastrasuc"
        Me.MueastrasucBindingSource.DataSource = Me.NewsoftvDataSet2
        '
        'NewsoftvDataSet2
        '
        Me.NewsoftvDataSet2.DataSetName = "NewsoftvDataSet2"
        Me.NewsoftvDataSet2.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'DameFechadelServidorHoraBindingSource
        '
        Me.DameFechadelServidorHoraBindingSource.DataMember = "DameFechadelServidorHora"
        Me.DameFechadelServidorHoraBindingSource.DataSource = Me.NewsoftvDataSet2
        '
        'MueastrasucTableAdapter
        '
        Me.MueastrasucTableAdapter.ClearBeforeFill = True
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(776, 496)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(39, 13)
        Me.Label6.TabIndex = 60
        Me.Label6.Text = "Label6"
        '
        'DameCantidadALetraBindingSource
        '
        Me.DameCantidadALetraBindingSource.DataMember = "DameCantidadALetra"
        Me.DameCantidadALetraBindingSource.DataSource = Me.NewsoftvDataSet2
        '
        'DameCantidadALetraTableAdapter
        '
        Me.DameCantidadALetraTableAdapter.ClearBeforeFill = True
        '
        'DameFechadelServidorHoraTableAdapter
        '
        Me.DameFechadelServidorHoraTableAdapter.ClearBeforeFill = True
        '
        'Procedimientos_arnoldo
        '
        Me.Procedimientos_arnoldo.DataSetName = "Procedimientos_arnoldo"
        Me.Procedimientos_arnoldo.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'Llena_Factura_Global_nuevoBindingSource
        '
        Me.Llena_Factura_Global_nuevoBindingSource.DataMember = "Llena_Factura_Global_nuevo"
        Me.Llena_Factura_Global_nuevoBindingSource.DataSource = Me.Procedimientos_arnoldo
        '
        'Llena_Factura_Global_nuevoTableAdapter
        '
        Me.Llena_Factura_Global_nuevoTableAdapter.ClearBeforeFill = True
        '
        'Button1
        '
        Me.Button1.BackColor = System.Drawing.Color.Orange
        Me.Button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button1.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button1.Location = New System.Drawing.Point(404, 240)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(136, 36)
        Me.Button1.TabIndex = 66
        Me.Button1.Text = "&CANCELAR"
        Me.Button1.UseVisualStyleBackColor = False
        '
        'Button9
        '
        Me.Button9.BackColor = System.Drawing.Color.Orange
        Me.Button9.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button9.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button9.Location = New System.Drawing.Point(219, 240)
        Me.Button9.Name = "Button9"
        Me.Button9.Size = New System.Drawing.Size(136, 36)
        Me.Button9.TabIndex = 65
        Me.Button9.Text = "&ACEPTAR"
        Me.Button9.UseVisualStyleBackColor = False
        '
        'ComboBox1
        '
        Me.ComboBox1.DisplayMember = "CLV_SUCURSAL"
        Me.ComboBox1.Location = New System.Drawing.Point(121, 137)
        Me.ComboBox1.Name = "ComboBox1"
        Me.ComboBox1.Size = New System.Drawing.Size(50, 21)
        Me.ComboBox1.TabIndex = 57
        Me.ComboBox1.ValueMember = "CLV_SUCURSAL"
        Me.ComboBox1.Visible = False
        '
        'TextBox4
        '
        Me.TextBox4.BackColor = System.Drawing.Color.White
        Me.TextBox4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.TextBox4.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DameImporteSucursalBindingSource, "Column1", True))
        Me.TextBox4.Location = New System.Drawing.Point(100, 164)
        Me.TextBox4.Name = "TextBox4"
        Me.TextBox4.ReadOnly = True
        Me.TextBox4.Size = New System.Drawing.Size(13, 20)
        Me.TextBox4.TabIndex = 61
        Me.TextBox4.Visible = False
        '
        'DameImporteSucursalBindingSource
        '
        Me.DameImporteSucursalBindingSource.DataMember = "DameImporteSucursal"
        Me.DameImporteSucursalBindingSource.DataSource = Me.DataSetLydia
        '
        'DataSetLydia
        '
        Me.DataSetLydia.DataSetName = "DataSetLydia"
        Me.DataSetLydia.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'TextBox5
        '
        Me.TextBox5.BackColor = System.Drawing.Color.White
        Me.TextBox5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.TextBox5.Location = New System.Drawing.Point(109, 44)
        Me.TextBox5.Name = "TextBox5"
        Me.TextBox5.ReadOnly = True
        Me.TextBox5.Size = New System.Drawing.Size(130, 21)
        Me.TextBox5.TabIndex = 62
        '
        'CMBLabel7
        '
        Me.CMBLabel7.AutoSize = True
        Me.CMBLabel7.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBLabel7.Location = New System.Drawing.Point(48, 143)
        Me.CMBLabel7.Name = "CMBLabel7"
        Me.CMBLabel7.Size = New System.Drawing.Size(67, 15)
        Me.CMBLabel7.TabIndex = 65
        Me.CMBLabel7.Text = "Sucursal:"
        '
        'CMBLabel1
        '
        Me.CMBLabel1.AutoSize = True
        Me.CMBLabel1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBLabel1.Location = New System.Drawing.Point(97, 152)
        Me.CMBLabel1.Name = "CMBLabel1"
        Me.CMBLabel1.Size = New System.Drawing.Size(45, 15)
        Me.CMBLabel1.TabIndex = 66
        Me.CMBLabel1.Text = "Serie:"
        '
        'CMBLabel2
        '
        Me.CMBLabel2.AutoSize = True
        Me.CMBLabel2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBLabel2.Location = New System.Drawing.Point(75, 143)
        Me.CMBLabel2.Name = "CMBLabel2"
        Me.CMBLabel2.Size = New System.Drawing.Size(59, 15)
        Me.CMBLabel2.TabIndex = 67
        Me.CMBLabel2.Text = "Factura:"
        '
        'CMBLabel3
        '
        Me.CMBLabel3.AutoSize = True
        Me.CMBLabel3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBLabel3.Location = New System.Drawing.Point(25, 16)
        Me.CMBLabel3.Name = "CMBLabel3"
        Me.CMBLabel3.Size = New System.Drawing.Size(50, 15)
        Me.CMBLabel3.TabIndex = 68
        Me.CMBLabel3.Text = "Fecha:"
        '
        'CMBLabel4
        '
        Me.CMBLabel4.AutoSize = True
        Me.CMBLabel4.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBLabel4.Location = New System.Drawing.Point(270, 44)
        Me.CMBLabel4.Name = "CMBLabel4"
        Me.CMBLabel4.Size = New System.Drawing.Size(60, 15)
        Me.CMBLabel4.TabIndex = 69
        Me.CMBLabel4.Text = "Importe:"
        '
        'CMBLabel5
        '
        Me.CMBLabel5.AutoSize = True
        Me.CMBLabel5.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBLabel5.Location = New System.Drawing.Point(32, 46)
        Me.CMBLabel5.Name = "CMBLabel5"
        Me.CMBLabel5.Size = New System.Drawing.Size(71, 15)
        Me.CMBLabel5.TabIndex = 70
        Me.CMBLabel5.Text = "Cajero(a):"
        '
        'DateTimePicker1
        '
        Me.DateTimePicker1.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DameFechadelServidorHoraBindingSource, "Fecha", True))
        Me.DateTimePicker1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.DateTimePicker1.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.DateTimePicker1.Location = New System.Drawing.Point(91, 14)
        Me.DateTimePicker1.Name = "DateTimePicker1"
        Me.DateTimePicker1.Size = New System.Drawing.Size(130, 20)
        Me.DateTimePicker1.TabIndex = 60
        '
        'NuevafacturaTextBox
        '
        Me.NuevafacturaTextBox.BackColor = System.Drawing.Color.White
        Me.NuevafacturaTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.NuevafacturaTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.Llena_Factura_Global_nuevoBindingSource, "nuevafactura", True))
        Me.NuevafacturaTextBox.Location = New System.Drawing.Point(51, 137)
        Me.NuevafacturaTextBox.Name = "NuevafacturaTextBox"
        Me.NuevafacturaTextBox.ReadOnly = True
        Me.NuevafacturaTextBox.Size = New System.Drawing.Size(144, 20)
        Me.NuevafacturaTextBox.TabIndex = 59
        Me.NuevafacturaTextBox.Visible = False
        '
        'SerieTextBox
        '
        Me.SerieTextBox.BackColor = System.Drawing.Color.White
        Me.SerieTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.SerieTextBox.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.SerieTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.Llena_Factura_Global_nuevoBindingSource, "serie", True))
        Me.SerieTextBox.Location = New System.Drawing.Point(63, 153)
        Me.SerieTextBox.Name = "SerieTextBox"
        Me.SerieTextBox.ReadOnly = True
        Me.SerieTextBox.Size = New System.Drawing.Size(144, 20)
        Me.SerieTextBox.TabIndex = 58
        Me.SerieTextBox.Visible = False
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.DateTimePicker2)
        Me.Panel1.Controls.Add(Me.Label1)
        Me.Panel1.Controls.Add(Me.DateTimePicker1)
        Me.Panel1.Controls.Add(Me.CMBLabel3)
        Me.Panel1.Location = New System.Drawing.Point(12, 12)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(528, 49)
        Me.Panel1.TabIndex = 61
        '
        'DateTimePicker2
        '
        Me.DateTimePicker2.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DameFechadelServidorHoraBindingSource, "Fecha", True))
        Me.DateTimePicker2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.DateTimePicker2.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.DateTimePicker2.Location = New System.Drawing.Point(362, 14)
        Me.DateTimePicker2.Name = "DateTimePicker2"
        Me.DateTimePicker2.Size = New System.Drawing.Size(130, 20)
        Me.DateTimePicker2.TabIndex = 69
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(270, 16)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(86, 15)
        Me.Label1.TabIndex = 70
        Me.Label1.Text = "Fecha Final:"
        '
        'Con_Factura_GlobalBindingSource
        '
        Me.Con_Factura_GlobalBindingSource.DataMember = "Con_Factura_Global"
        Me.Con_Factura_GlobalBindingSource.DataSource = Me.Procedimientos_arnoldo
        '
        'Con_Factura_GlobalTableAdapter
        '
        Me.Con_Factura_GlobalTableAdapter.ClearBeforeFill = True
        '
        'Selecciona_impresoraticketsBindingSource
        '
        Me.Selecciona_impresoraticketsBindingSource.DataMember = "Selecciona_impresoratickets"
        Me.Selecciona_impresoraticketsBindingSource.DataSource = Me.Procedimientos_arnoldo
        '
        'Selecciona_impresoraticketsTableAdapter
        '
        Me.Selecciona_impresoraticketsTableAdapter.ClearBeforeFill = True
        '
        'NUEVAFACTGLOBALBindingSource
        '
        Me.NUEVAFACTGLOBALBindingSource.DataMember = "NUEVAFACTGLOBAL"
        Me.NUEVAFACTGLOBALBindingSource.DataSource = Me.DataSetLydia
        '
        'NUEVAFACTGLOBALTableAdapter
        '
        Me.NUEVAFACTGLOBALTableAdapter.ClearBeforeFill = True
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.CMBLabel5)
        Me.GroupBox1.Controls.Add(Me.TextBoxImporte)
        Me.GroupBox1.Controls.Add(Me.CMBLabel4)
        Me.GroupBox1.Controls.Add(Me.TextBox5)
        Me.GroupBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox1.Location = New System.Drawing.Point(12, 70)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(528, 153)
        Me.GroupBox1.TabIndex = 67
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Factura Global"
        '
        'TextBoxImporte
        '
        Me.TextBoxImporte.BackColor = System.Drawing.Color.White
        Me.TextBoxImporte.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.TextBoxImporte.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBoxImporte.Location = New System.Drawing.Point(336, 42)
        Me.TextBoxImporte.Name = "TextBoxImporte"
        Me.TextBoxImporte.ReadOnly = True
        Me.TextBoxImporte.Size = New System.Drawing.Size(172, 21)
        Me.TextBoxImporte.TabIndex = 68
        '
        'DameImporteSucursalTableAdapter
        '
        Me.DameImporteSucursalTableAdapter.ClearBeforeFill = True
        '
        'TextBoxIEPS
        '
        Me.TextBoxIEPS.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.TextBoxIEPS.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBoxIEPS.Location = New System.Drawing.Point(63, 164)
        Me.TextBoxIEPS.Name = "TextBoxIEPS"
        Me.TextBoxIEPS.Size = New System.Drawing.Size(10, 21)
        Me.TextBoxIEPS.TabIndex = 69
        Me.TextBoxIEPS.Visible = False
        '
        'TextBoxIVA
        '
        Me.TextBoxIVA.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.TextBoxIVA.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBoxIVA.Location = New System.Drawing.Point(51, 164)
        Me.TextBoxIVA.Name = "TextBoxIVA"
        Me.TextBoxIVA.Size = New System.Drawing.Size(10, 21)
        Me.TextBoxIVA.TabIndex = 70
        Me.TextBoxIVA.Visible = False
        '
        'TextBoxSubTotal
        '
        Me.TextBoxSubTotal.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.TextBoxSubTotal.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBoxSubTotal.Location = New System.Drawing.Point(35, 164)
        Me.TextBoxSubTotal.Name = "TextBoxSubTotal"
        Me.TextBoxSubTotal.Size = New System.Drawing.Size(10, 21)
        Me.TextBoxSubTotal.TabIndex = 71
        Me.TextBoxSubTotal.Visible = False
        '
        'VerAcceso2TableAdapter1
        '
        Me.VerAcceso2TableAdapter1.ClearBeforeFill = True
        '
        'VerAcceso2TableAdapter2
        '
        Me.VerAcceso2TableAdapter2.ClearBeforeFill = True
        '
        'ComboBox2
        '
        Me.ComboBox2.DataSource = Me.MueastrasucBindingSource
        Me.ComboBox2.DisplayMember = "NOMBRE"
        Me.ComboBox2.Location = New System.Drawing.Point(29, 152)
        Me.ComboBox2.Name = "ComboBox2"
        Me.ComboBox2.Size = New System.Drawing.Size(122, 21)
        Me.ComboBox2.TabIndex = 71
        Me.ComboBox2.ValueMember = "CLV_SUCURSAL"
        Me.ComboBox2.Visible = False
        '
        'CAPTURAFACTURAGLOBALESPECIALES
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.WhiteSmoke
        Me.ClientSize = New System.Drawing.Size(556, 293)
        Me.ControlBox = False
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.SerieTextBox)
        Me.Controls.Add(Me.ComboBox2)
        Me.Controls.Add(Me.ComboBox1)
        Me.Controls.Add(Me.TextBoxSubTotal)
        Me.Controls.Add(Me.TextBoxIVA)
        Me.Controls.Add(Me.CMBLabel7)
        Me.Controls.Add(Me.NuevafacturaTextBox)
        Me.Controls.Add(Me.TextBoxIEPS)
        Me.Controls.Add(Me.CMBLabel1)
        Me.Controls.Add(Me.CMBLabel2)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.Button9)
        Me.Controls.Add(Me.Panel1)
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Me.TextBox4)
        Me.Cursor = System.Windows.Forms.Cursors.Default
        Me.MaximizeBox = False
        Me.Name = "CAPTURAFACTURAGLOBALESPECIALES"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Nueva Factura Global De Cajas Por Sucursal"
        CType(Me.MueastrasucBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.NewsoftvDataSet2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DameFechadelServidorHoraBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DameCantidadALetraBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Procedimientos_arnoldo, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Llena_Factura_Global_nuevoBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DameImporteSucursalBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DataSetLydia, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        CType(Me.Con_Factura_GlobalBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Selecciona_impresoraticketsBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.NUEVAFACTGLOBALBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents NewsoftvDataSet2 As softvFacturacion.NewsoftvDataSet2
    Friend WithEvents MueastrasucBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents MueastrasucTableAdapter As softvFacturacion.NewsoftvDataSet2TableAdapters.mueastrasucTableAdapter
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents DameCantidadALetraBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents DameCantidadALetraTableAdapter As softvFacturacion.NewsoftvDataSet2TableAdapters.DameCantidadALetraTableAdapter
    Friend WithEvents DameFechadelServidorHoraBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents DameFechadelServidorHoraTableAdapter As softvFacturacion.NewsoftvDataSet2TableAdapters.DameFechadelServidorHoraTableAdapter
    Friend WithEvents Procedimientos_arnoldo As softvFacturacion.Procedimientos_arnoldo
    Friend WithEvents Llena_Factura_Global_nuevoBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Llena_Factura_Global_nuevoTableAdapter As softvFacturacion.Procedimientos_arnoldoTableAdapters.Llena_Factura_Global_nuevoTableAdapter
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents Button9 As System.Windows.Forms.Button
    Public WithEvents ComboBox1 As System.Windows.Forms.ComboBox
    Friend WithEvents TextBox4 As System.Windows.Forms.TextBox
    Friend WithEvents TextBox5 As System.Windows.Forms.TextBox
    Friend WithEvents CMBLabel7 As System.Windows.Forms.Label
    Friend WithEvents CMBLabel1 As System.Windows.Forms.Label
    Friend WithEvents CMBLabel2 As System.Windows.Forms.Label
    Friend WithEvents CMBLabel3 As System.Windows.Forms.Label
    Friend WithEvents CMBLabel4 As System.Windows.Forms.Label
    Friend WithEvents CMBLabel5 As System.Windows.Forms.Label
    Friend WithEvents DateTimePicker1 As System.Windows.Forms.DateTimePicker
    Friend WithEvents NuevafacturaTextBox As System.Windows.Forms.TextBox
    Friend WithEvents SerieTextBox As System.Windows.Forms.TextBox
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents Con_Factura_GlobalBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Con_Factura_GlobalTableAdapter As softvFacturacion.Procedimientos_arnoldoTableAdapters.Con_Factura_GlobalTableAdapter
    Friend WithEvents Selecciona_impresoraticketsBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Selecciona_impresoraticketsTableAdapter As softvFacturacion.Procedimientos_arnoldoTableAdapters.Selecciona_impresoraticketsTableAdapter
    Friend WithEvents DataSetLydia As softvFacturacion.DataSetLydia
    Friend WithEvents NUEVAFACTGLOBALBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents NUEVAFACTGLOBALTableAdapter As softvFacturacion.DataSetLydiaTableAdapters.NUEVAFACTGLOBALTableAdapter
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents DateTimePicker2 As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents DameImporteSucursalBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents DameImporteSucursalTableAdapter As softvFacturacion.DataSetLydiaTableAdapters.DameImporteSucursalTableAdapter
    Friend WithEvents TextBoxImporte As System.Windows.Forms.TextBox
    Friend WithEvents TextBoxIEPS As System.Windows.Forms.TextBox
    Friend WithEvents TextBoxIVA As System.Windows.Forms.TextBox
    Friend WithEvents TextBoxSubTotal As System.Windows.Forms.TextBox
    Friend WithEvents VerAcceso2TableAdapter1 As softvFacturacion.ProcedimientosArnoldo3TableAdapters.VerAcceso2TableAdapter
    Friend WithEvents VerAcceso2TableAdapter2 As softvFacturacion.ProcedimientosArnoldo3TableAdapters.VerAcceso2TableAdapter
    Public WithEvents ComboBox2 As System.Windows.Forms.ComboBox
    'Friend WithEvents NUEVAFACTGLOBALTableAdapter As softvFacturacion.NewsoftvDataSet2TableAdapters.NUEVAFACTGLOBALTableAdapter
End Class

'<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
'Partial Class CAPTURAFACTURAGLOBALESPECIALES
'    Inherits System.Windows.Forms.Form

'    'Form reemplaza a Dispose para limpiar la lista de componentes.
'    <System.Diagnostics.DebuggerNonUserCode()> _
'    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
'        Try
'            If disposing AndAlso components IsNot Nothing Then
'                components.Dispose()
'            End If
'        Finally
'            MyBase.Dispose(disposing)
'        End Try
'    End Sub

'    'Requerido por el Diseñador de Windows Forms
'    Private components As System.ComponentModel.IContainer

'    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
'    'Se puede modificar usando el Diseñador de Windows Forms.  
'    'No lo modifique con el editor de código.
'    <System.Diagnostics.DebuggerStepThrough()> _
'    Private Sub InitializeComponent()
'        components = New System.ComponentModel.Container
'        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
'        Me.Text = "CAPTURAFACTURAGLOBALESPECIALES"
'    End Sub
'End Class
