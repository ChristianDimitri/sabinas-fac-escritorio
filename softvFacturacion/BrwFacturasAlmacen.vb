﻿Public Class BrwFacturasAlmacen

    Private Sub BrwFacturasAlmacen_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        colorea(Me)
        UspGuardaFormularios(Me.Name, Me.Text)
        UspGuardaBotonesFormularioSiste(Me, Me.Name)
        UspDesactivaBotones(Me, Me.Name)
        llenaDistribuidor()
        llenaGrid(0, dtpFecha.Value, "", 0)
    End Sub

    Private Sub llenaDistribuidor()
        Try
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@Clv_plaza", SqlDbType.Int, 0)
            cbDistribuidor.DataSource = BaseII.ConsultaDT("Muestra_Plazas")
        Catch ex As Exception

        End Try
    End Sub

    Private Sub llenaGrid(ByVal clv_plaza As Integer, ByVal fecha As Date, ByVal usuario As String, ByVal opcion As Integer)
        Try
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@opcion", SqlDbType.Int, opcion)
            BaseII.CreateMyParameter("@clv_plaza", SqlDbType.Int, clv_plaza)
            BaseII.CreateMyParameter("@fecha", SqlDbType.DateTime, fecha)
            BaseII.CreateMyParameter("@usuario", SqlDbType.VarChar, usuario)
            DataGridView1.DataSource = BaseII.ConsultaDT("MuestraFacturasAlmacen")
        Catch ex As Exception

        End Try
    End Sub

    Private Sub CMBLabel5_Click(sender As Object, e As EventArgs) Handles CMBLabel5.Click

    End Sub

    Private Sub cbDistribuidor_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cbDistribuidor.SelectedIndexChanged
        llenaGrid(cbDistribuidor.SelectedValue, dtpFecha.Value, "", 1)
    End Sub

    Private Sub Button3_Click(sender As Object, e As EventArgs) Handles Button3.Click
        llenaGrid(cbDistribuidor.SelectedValue, dtpFecha.Value, "", 2)
    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        If tbusuario.Text.Trim = "" Then
            Exit Sub
        End If
        llenaGrid(cbDistribuidor.SelectedValue, dtpFecha.Value, tbusuario.Text, 3)
    End Sub

    Private Sub Button5_Click(sender As Object, e As EventArgs) Handles Button5.Click
        Me.Close()
    End Sub

    Public Function ValidaReFActuraAlmacen(oClvFactura As Long) As Integer
        ValidaReFActuraAlmacen = 0
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@Clv_Factura", SqlDbType.BigInt, oClvFactura)
        BaseII.CreateMyParameter("@Res", ParameterDirection.Output, SqlDbType.Int)
        BaseII.CreateMyParameter("@Msg", ParameterDirection.Output, SqlDbType.VarChar, 250)
        BaseII.ProcedimientoOutPut("ValidaReFacturaAlmacen")
        ValidaReFActuraAlmacen = BaseII.dicoPar("@Res")
        If BaseII.dicoPar("@Res") > 0 Then
            MsgBox(BaseII.dicoPar("@Msg"))
        End If
    End Function
    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        If DataGridView1.Rows.Count = 0 Then
            Exit Sub
        End If
        If DataGridView1.SelectedCells(7).Value.ToString = "Cancelada" Then
            MsgBox("La factura ha sido cancelada.")
            Exit Sub
        End If
        'Los índices de las columnas cambiaron, excepto cveFactura, el 6 es el 8
        'If CLng(DataGridView1.SelectedCells(6).Value) > 0 Then
        '    MsgBox("El proceso ya está facturado.")
        '    Exit Sub
        'End If
        Try
            eCveFactura = CLng(DataGridView1.SelectedCells(0).Value)
            Dim eRes As Integer = 0
            eRes = ValidaReFActuraAlmacen(CLng(eCveFactura))
            If eRes = 0 Then
                Dim resp As MsgBoxResult = MsgBoxResult.Cancel
                resp = MsgBox("¿ Esta Seguro de que Deseas Refacturar la Factura No.: " + CLng(eCveFactura) + " ?", MsgBoxStyle.YesNo)
                If resp = MsgBoxResult.Yes Then

                    'Cancelamos el Pago Digital
                    CancelarFacturaDigital(eCveFactura)
                    'Haz Factura Digital
                    HazFacturaDigitalAlmacen(eCveFactura)

                End If
            End If

        Catch ex As Exception
            MsgBox("No se Tiene Conexion con Mizar")
        End Try

    End Sub

    Private Sub Button4_Click(sender As Object, e As EventArgs) Handles Button4.Click 'Imprimir

        If DataGridView1.Rows.Count = 0 Then
            Exit Sub
        End If
        If DataGridView1.SelectedCells(7).Value.ToString = "Cancelada" Then
            MsgBox("La factura ha sido cancelada.")
            Exit Sub
        End If
        If CLng(DataGridView1.SelectedCells(8).Value.ToString) <= 0 Then
            MsgBox("El proceso no está facturado.")
            Exit Sub
        End If
        Try
            ImprimeFacturaDigital(CLng(DataGridView1.SelectedCells(0).Value))
        Catch ex As Exception
            MsgBox("No se Tiene Conexion con Mizar")
        End Try
    End Sub

    Private Sub ImprimeFacturaDigital(ByVal oLocClv_Factura As Integer)
        FacturacionDigitalSoftv.ClassCFDI.MiConexion = MiConexion
        Dim identi As Integer = 0
        FacturacionDigitalSoftv.ClassCFDI.Locop = 1
        FacturacionDigitalSoftv.ClassCFDI.DameId_FacturaCDF(oLocClv_Factura, "A", MiConexion)
        Try
            If FacturacionDigitalSoftv.ClassCFDI.GloClv_FacturaCFD > 0 Then
                Dim frm As New FacturacionDigitalSoftv.FrmImprimir
                frm.ShowDialog()
                bitsist(LocLoginUsuario, 0, "Facturación", "BrwFacturasAlmacen", "Se reimprimio la factura de almacen " + DataGridView1.SelectedCells(0).Value.ToString(), "", "1", SubCiudad)
            Else
                MsgBox("No se generó la factura digital cancele y vuelva a intentar por favor")
            End If
            'FormPruebaDigital.Show()
        Catch ex As Exception
            MsgBox(ex.ToString)
        End Try
        FacturacionDigitalSoftv.ClassCFDI.Locop = 0
        FacturacionDigitalSoftv.ClassCFDI.locID_Compania_Mizart = ""
        FacturacionDigitalSoftv.ClassCFDI.locID_Sucursal_Mizart = ""
    End Sub


    Private Sub CancelarFacturaDigital(ByVal oLocClv_Factura As Integer)
        FacturacionDigitalSoftv.ClassCFDI.MiConexion = MiConexion
        Dim identi As Integer = 0
        FacturacionDigitalSoftv.ClassCFDI.Locop = 1
        FacturacionDigitalSoftv.ClassCFDI.DameId_FacturaCDF(oLocClv_Factura, "A", MiConexion)
        Try
            If FacturacionDigitalSoftv.ClassCFDI.GloClv_FacturaCFD > 0 Then
                FacturacionDigitalSoftv.ClassCFDI.Cancelacion_FacturaCFD(FacturacionDigitalSoftv.ClassCFDI.GloClv_FacturaCFD, MiConexion)
                'Dim frm As New FacturacionDigitalSoftv.FrmImprimir
                'frm.ShowDialog()
                bitsist(LocLoginUsuario, 0, "Facturación", "BrwFacturasAlmacen", "Se canceló la factura de almacen " + eCveFactura.ToString(), "Activa", "Cancelada", SubCiudad)
            Else
                MsgBox("No se cancelo y vuelva a intentar por favor")
            End If
            'FormPruebaDigital.Show()
        Catch ex As Exception
            MsgBox(ex.ToString)
        End Try
        FacturacionDigitalSoftv.ClassCFDI.Locop = 0
        FacturacionDigitalSoftv.ClassCFDI.locID_Compania_Mizart = ""
        FacturacionDigitalSoftv.ClassCFDI.locID_Sucursal_Mizart = ""
    End Sub

    Private Sub HazFacturaDigitalAlmacen(ByVal LocClv_Factura As Long)
        Try
            FacturacionDigitalSoftv.ClassCFDI.MiConexion = MiConexion
            Dim identi As Integer = 0
            FacturacionDigitalSoftv.ClassCFDI.EsTimbrePrueba = eEsTimbrePrueba
            identi = 0
            identi = FacturacionDigitalSoftv.ClassCFDI.BusFacFiscalOledb_Almacen(LocClv_Factura, MiConexion)
            'fin Facturacion Digital
            FacturacionDigitalSoftv.ClassCFDI.locID_Compania_Mizart = ""
            FacturacionDigitalSoftv.ClassCFDI.locID_Sucursal_Mizart = ""

            If CInt(identi) > 0 Then
                FacturacionDigitalSoftv.ClassCFDI.Locop = 0
                FacturacionDigitalSoftv.ClassCFDI.Dime_Aque_Compania_Facturarle_Almacen(LocClv_Factura, MiConexion)
                FacturacionDigitalSoftv.ClassCFDI.Graba_Factura_Digital_Almacen(LocClv_Factura, identi, MiConexion)
                Try
                    If FacturacionDigitalSoftv.ClassCFDI.GloClv_FacturaCFD > 0 Then
                        Dim frm As New FacturacionDigitalSoftv.FrmImprimir
                        frm.ShowDialog()
                        bitsist(LocLoginUsuario, 0, "Facturación", "BrwFacturasAlmacen", "Se generó la factura de almacen " + DataGridView1.SelectedCells(0).Value.ToString(), "", "1", SubCiudad)
                    Else
                        MsgBox("No se genero la factura digital cancele y vuelva a intentar por favor")
                    End If
                    'FormPruebaDigital.Show()
                Catch ex As Exception
                End Try
                FacturacionDigitalSoftv.ClassCFDI.locID_Compania_Mizart = ""
                FacturacionDigitalSoftv.ClassCFDI.locID_Sucursal_Mizart = ""

            End If
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try

    End Sub

    Private Sub CancelaFacturasAlmacen(LocIdFactura As Long, LocClvMotCan As Integer, LocUsuario As Long)
        Try

            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@IDFACTURA", SqlDbType.BigInt, LocIdFactura)
            BaseII.CreateMyParameter("@CLV_MOTCAN", SqlDbType.BigInt, LocClvMotCan)
            BaseII.CreateMyParameter("@CLV_USUARIO", SqlDbType.BigInt, LocUsuario)
            BaseII.Inserta("CANCELACION_FACTURASALMACEN")
            MsgBox("¡ La cancelación se realizo correctamente !")
        Catch ex As Exception

        End Try
    End Sub

    Private Sub Button6_Click(sender As Object, e As EventArgs) Handles Button6.Click
        If DataGridView1.Rows.Count = 0 Then
            Exit Sub
        End If
        If DataGridView1.SelectedCells(7).Value.ToString = "Cancelada" Then
            MsgBox("La factura ha sido cancelada.")
            Exit Sub
        End If
        If CLng(DataGridView1.SelectedCells(8).Value.ToString) <= 0 Then
            MsgBox("El proceso no está facturado.")
            Exit Sub
        End If
        Try
            Dim resp As MsgBoxResult
            Dim eRes As Integer = 0
            Dim eMsg As String = Nothing
            resp = MsgBox("Deseas cancelar la factura " & DataGridView1.SelectedCells(1).Value.ToString + "-" + DataGridView1.SelectedCells(2).Value.ToString, MsgBoxStyle.OkCancel, "Cancelación de Facturas de Almacen")
            If resp = MsgBoxResult.Ok Then
                eCveFactura = CLng(DataGridView1.SelectedCells(0).Value)
                'Variable que me permite saber si se cancela o se reimprime factura
                eReImprimirF = 0
                Dim FmMCF As New FrmMotivoCancelacionFacturaAlmacen
                FmMCF.ShowDialog()
                CancelaFacturasAlmacen(eCveFactura, EClvMotCan, GloClvUsuario)
                CancelarFacturaDigital(CLng(eCveFactura))
            End If
        Catch ex As Exception
            MsgBox("No se Tiene Conexion con Mizar")
        End Try
    End Sub
End Class