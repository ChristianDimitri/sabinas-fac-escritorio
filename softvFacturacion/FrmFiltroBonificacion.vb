﻿Public Class FrmFiltroBonificacion

    Private Sub dtpInicio_ValueChanged(sender As Object, e As EventArgs) Handles dtpInicio.ValueChanged
        Me.dtpFinal.MinDate = Me.dtpInicio.Value
    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        If Not cbAplicada.Checked And Not cbNoAplicada.Checked Then
            MsgBox("Seleccione al menos un estado de bonificación para continuar.")
            Exit Sub
        End If
        If cbAplicada.Checked And Not cbNoAplicada.Checked Then
            EstadoBonificacion = 1
        End If
        If cbNoAplicada.Checked And Not cbAplicada.Checked Then
            EstadoBonificacion = 0
        End If
        If cbNoAplicada.Checked And cbAplicada.Checked Then
            EstadoBonificacion = 2
        End If
        LocFecha1 = dtpInicio.Value
        LocFecha2 = dtpFinal.Value
        FrmImprimirRepGral.Show()
        Me.Close()
    End Sub

    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        Me.Close()
    End Sub

    Private Sub FrmFiltroBonificacion_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        colorea(Me)
        GroupBox1.BackColor = Me.BackColor
        GroupBox2.BackColor = Me.BackColor
    End Sub
End Class