<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class BrwFacturas_Cancelar
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim FacturaLabel As System.Windows.Forms.Label
        Dim SerieLabel As System.Windows.Forms.Label
        Dim FECHALabel As System.Windows.Forms.Label
        Dim ClienteLabel As System.Windows.Forms.Label
        Dim NOMBRELabel As System.Windows.Forms.Label
        Dim ImporteLabel As System.Windows.Forms.Label
        Dim Label5 As System.Windows.Forms.Label
        Dim Label10 As System.Windows.Forms.Label
        Dim Label12 As System.Windows.Forms.Label
        Dim Label14 As System.Windows.Forms.Label
        Dim Label15 As System.Windows.Forms.Label
        Dim Label17 As System.Windows.Forms.Label
        Dim Label19 As System.Windows.Forms.Label
        Dim DataGridViewCellStyle9 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle10 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle11 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle12 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.SplitContainer1 = New System.Windows.Forms.SplitContainer()
        Me.Label23 = New System.Windows.Forms.Label()
        Me.ComboBoxCiudad = New System.Windows.Forms.ComboBox()
        Me.Label22 = New System.Windows.Forms.Label()
        Me.ComboBoxCompanias = New System.Windows.Forms.ComboBox()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.BUSCAFACTURASBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.NewsoftvDataSet1 = New softvFacturacion.NewsoftvDataSet1()
        Me.CMBNOMBRETextBox1 = New System.Windows.Forms.TextBox()
        Me.ImporteLabel1 = New System.Windows.Forms.Label()
        Me.ClienteLabel1 = New System.Windows.Forms.Label()
        Me.FECHALabel1 = New System.Windows.Forms.Label()
        Me.FacturaLabel1 = New System.Windows.Forms.Label()
        Me.SerieLabel1 = New System.Windows.Forms.Label()
        Me.Clv_FacturaLabel1 = New System.Windows.Forms.Label()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Panel5 = New System.Windows.Forms.Panel()
        Me.Button3 = New System.Windows.Forms.Button()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Button4 = New System.Windows.Forms.Button()
        Me.CONTRATOTextBox = New System.Windows.Forms.TextBox()
        Me.NOMBRETextBox = New System.Windows.Forms.TextBox()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.FOLIOTextBox = New System.Windows.Forms.TextBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.ComboBox4 = New System.Windows.Forms.ComboBox()
        Me.MUESTRATIPOFACTURABindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.DataSetLydia = New softvFacturacion.DataSetLydia()
        Me.CMBLabel5 = New System.Windows.Forms.Label()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.Button7 = New System.Windows.Forms.Button()
        Me.FECHATextBox = New System.Windows.Forms.TextBox()
        Me.SERIETextBox = New System.Windows.Forms.TextBox()
        Me.CMBLabel1 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.BUSCANOTASDECREDITODataGridView = New System.Windows.Forms.DataGridView()
        Me.ClvNotadecreditoDataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.StatusDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column1DataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.FECHAdegeneracionDataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ContratoDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.MontoDataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.UsuariocapturaDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.UsuarioautorizoDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.FechacaducidadDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ObservacionesDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.MotCanDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ClvFacturaAplicadaDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.SaldoDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.BUSCANOTASDECREDITOBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Panel4 = New System.Windows.Forms.Panel()
        Me.Button10 = New System.Windows.Forms.Button()
        Me.DataGridView1 = New System.Windows.Forms.DataGridView()
        Me.Status = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Panel3 = New System.Windows.Forms.Panel()
        Me.Button9 = New System.Windows.Forms.Button()
        Me.Button5 = New System.Windows.Forms.Button()
        Me.Button2 = New System.Windows.Forms.Button()
        Me.BUSCAFACTURASTableAdapter = New softvFacturacion.NewsoftvDataSet1TableAdapters.BUSCAFACTURASTableAdapter()
        Me.CANCELACIONFACTURASBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.CANCELACIONFACTURASTableAdapter = New softvFacturacion.NewsoftvDataSet1TableAdapters.CANCELACIONFACTURASTableAdapter()
        Me.CMBPanel3 = New System.Windows.Forms.Panel()
        Me.Button11 = New System.Windows.Forms.Button()
        Me.Button6 = New System.Windows.Forms.Button()
        Me.CMBPanel4 = New System.Windows.Forms.Panel()
        Me.Button8 = New System.Windows.Forms.Button()
        Me.DAMEFECHADELSERVIDORBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.DAMEFECHADELSERVIDORTableAdapter = New softvFacturacion.NewsoftvDataSet1TableAdapters.DAMEFECHADELSERVIDORTableAdapter()
        Me.FECHADateTimePicker = New System.Windows.Forms.DateTimePicker()
        Me.CMBPanel2 = New System.Windows.Forms.Panel()
        Me.EricDataSet = New softvFacturacion.EricDataSet()
        Me.CancelaCNRPPEBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.CancelaCNRPPETableAdapter = New softvFacturacion.EricDataSetTableAdapters.CancelaCNRPPETableAdapter()
        Me.DataSetEdgar = New softvFacturacion.DataSetEdgar()
        Me.CancelaCambioServClienteBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.CancelaCambioServClienteTableAdapter = New softvFacturacion.DataSetEdgarTableAdapters.CancelaCambioServClienteTableAdapter()
        Me.Procedimientos_arnoldo = New softvFacturacion.Procedimientos_arnoldo()
        Me.Selecciona_impresoraticketsBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Selecciona_impresoraticketsTableAdapter = New softvFacturacion.Procedimientos_arnoldoTableAdapters.Selecciona_impresoraticketsTableAdapter()
        Me.BUSCANOTASDECREDITOTableAdapter = New softvFacturacion.DataSetLydiaTableAdapters.BUSCANOTASDECREDITOTableAdapter()
        Me.MUESTRATIPOFACTURATableAdapter = New softvFacturacion.DataSetLydiaTableAdapters.MUESTRATIPOFACTURATableAdapter()
        Me.Panel2 = New System.Windows.Forms.Panel()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.DetalleNOTASDECREDITODataGridView = New System.Windows.Forms.DataGridView()
        Me.Column1DataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.MontoDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.FECHAdegeneracionDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ClvNotadecreditoDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DetalleNOTASDECREDITOBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.CMBTextBox1 = New System.Windows.Forms.TextBox()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.Label16 = New System.Windows.Forms.Label()
        Me.Label18 = New System.Windows.Forms.Label()
        Me.Label20 = New System.Windows.Forms.Label()
        Me.Label21 = New System.Windows.Forms.Label()
        Me.DetalleNOTASDECREDITOTableAdapter = New softvFacturacion.DataSetLydiaTableAdapters.DetalleNOTASDECREDITOTableAdapter()
        Me.EricDataSet2 = New softvFacturacion.EricDataSet2()
        Me.DameGeneralMsjTicketsBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.DameGeneralMsjTicketsTableAdapter = New softvFacturacion.EricDataSet2TableAdapters.DameGeneralMsjTicketsTableAdapter()
        Me.ValidaCancelacionFacturaBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.ValidaCancelacionFacturaTableAdapter = New softvFacturacion.EricDataSet2TableAdapters.ValidaCancelacionFacturaTableAdapter()
        Me.VerAcceso2TableAdapter1 = New softvFacturacion.ProcedimientosArnoldo3TableAdapters.VerAcceso2TableAdapter()
        Me.VerAcceso2TableAdapter2 = New softvFacturacion.ProcedimientosArnoldo3TableAdapters.VerAcceso2TableAdapter()
        Me.VerAcceso2TableAdapter3 = New softvFacturacion.ProcedimientosArnoldo3TableAdapters.VerAcceso2TableAdapter()
        Me.VerAcceso2TableAdapter4 = New softvFacturacion.ProcedimientosArnoldo3TableAdapters.VerAcceso2TableAdapter()
        Me.VerAcceso2TableAdapter5 = New softvFacturacion.ProcedimientosArnoldo3TableAdapters.VerAcceso2TableAdapter()
        Me.VerAcceso2TableAdapter6 = New softvFacturacion.ProcedimientosArnoldo3TableAdapters.VerAcceso2TableAdapter()
        Me.VerAcceso2TableAdapter7 = New softvFacturacion.ProcedimientosArnoldo3TableAdapters.VerAcceso2TableAdapter()
        Me.VerAcceso2TableAdapter8 = New softvFacturacion.ProcedimientosArnoldo3TableAdapters.VerAcceso2TableAdapter()
        Me.VerAcceso2TableAdapter9 = New softvFacturacion.ProcedimientosArnoldo3TableAdapters.VerAcceso2TableAdapter()
        Me.VerAcceso2TableAdapter10 = New softvFacturacion.ProcedimientosArnoldo3TableAdapters.VerAcceso2TableAdapter()
        Me.Button12 = New System.Windows.Forms.Button()
        Me.CMBPanelRefacturacion = New System.Windows.Forms.Panel()
        FacturaLabel = New System.Windows.Forms.Label()
        SerieLabel = New System.Windows.Forms.Label()
        FECHALabel = New System.Windows.Forms.Label()
        ClienteLabel = New System.Windows.Forms.Label()
        NOMBRELabel = New System.Windows.Forms.Label()
        ImporteLabel = New System.Windows.Forms.Label()
        Label5 = New System.Windows.Forms.Label()
        Label10 = New System.Windows.Forms.Label()
        Label12 = New System.Windows.Forms.Label()
        Label14 = New System.Windows.Forms.Label()
        Label15 = New System.Windows.Forms.Label()
        Label17 = New System.Windows.Forms.Label()
        Label19 = New System.Windows.Forms.Label()
        Me.SplitContainer1.Panel1.SuspendLayout()
        Me.SplitContainer1.Panel2.SuspendLayout()
        Me.SplitContainer1.SuspendLayout()
        Me.Panel1.SuspendLayout()
        CType(Me.BUSCAFACTURASBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.NewsoftvDataSet1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel5.SuspendLayout()
        CType(Me.MUESTRATIPOFACTURABindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DataSetLydia, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.BUSCANOTASDECREDITODataGridView, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.BUSCANOTASDECREDITOBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel4.SuspendLayout()
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel3.SuspendLayout()
        CType(Me.CANCELACIONFACTURASBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.CMBPanel3.SuspendLayout()
        Me.CMBPanel4.SuspendLayout()
        CType(Me.DAMEFECHADELSERVIDORBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.CMBPanel2.SuspendLayout()
        CType(Me.EricDataSet, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CancelaCNRPPEBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DataSetEdgar, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CancelaCambioServClienteBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Procedimientos_arnoldo, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Selecciona_impresoraticketsBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel2.SuspendLayout()
        CType(Me.DetalleNOTASDECREDITODataGridView, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DetalleNOTASDECREDITOBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.EricDataSet2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DameGeneralMsjTicketsBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ValidaCancelacionFacturaBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.CMBPanelRefacturacion.SuspendLayout()
        Me.SuspendLayout()
        '
        'FacturaLabel
        '
        FacturaLabel.AutoSize = True
        FacturaLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        FacturaLabel.ForeColor = System.Drawing.Color.White
        FacturaLabel.Location = New System.Drawing.Point(28, 59)
        FacturaLabel.Name = "FacturaLabel"
        FacturaLabel.Size = New System.Drawing.Size(47, 15)
        FacturaLabel.TabIndex = 30
        FacturaLabel.Text = "Folio :"
        '
        'SerieLabel
        '
        SerieLabel.AutoSize = True
        SerieLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        SerieLabel.ForeColor = System.Drawing.Color.White
        SerieLabel.Location = New System.Drawing.Point(28, 36)
        SerieLabel.Name = "SerieLabel"
        SerieLabel.Size = New System.Drawing.Size(49, 15)
        SerieLabel.TabIndex = 29
        SerieLabel.Text = "Serie :"
        '
        'FECHALabel
        '
        FECHALabel.AutoSize = True
        FECHALabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        FECHALabel.ForeColor = System.Drawing.Color.White
        FECHALabel.Location = New System.Drawing.Point(21, 82)
        FECHALabel.Name = "FECHALabel"
        FECHALabel.Size = New System.Drawing.Size(54, 15)
        FECHALabel.TabIndex = 32
        FECHALabel.Text = "Fecha :"
        '
        'ClienteLabel
        '
        ClienteLabel.AutoSize = True
        ClienteLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        ClienteLabel.ForeColor = System.Drawing.Color.White
        ClienteLabel.Location = New System.Drawing.Point(6, 105)
        ClienteLabel.Name = "ClienteLabel"
        ClienteLabel.Size = New System.Drawing.Size(69, 15)
        ClienteLabel.TabIndex = 33
        ClienteLabel.Text = "Contrato :"
        '
        'NOMBRELabel
        '
        NOMBRELabel.AutoSize = True
        NOMBRELabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        NOMBRELabel.ForeColor = System.Drawing.Color.White
        NOMBRELabel.Location = New System.Drawing.Point(8, 128)
        NOMBRELabel.Name = "NOMBRELabel"
        NOMBRELabel.Size = New System.Drawing.Size(66, 15)
        NOMBRELabel.TabIndex = 34
        NOMBRELabel.Text = "Nombre :"
        '
        'ImporteLabel
        '
        ImporteLabel.AutoSize = True
        ImporteLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        ImporteLabel.ForeColor = System.Drawing.Color.White
        ImporteLabel.Location = New System.Drawing.Point(13, 178)
        ImporteLabel.Name = "ImporteLabel"
        ImporteLabel.Size = New System.Drawing.Size(64, 15)
        ImporteLabel.TabIndex = 35
        ImporteLabel.Text = "Importe :"
        '
        'Label5
        '
        Label5.AutoSize = True
        Label5.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label5.ForeColor = System.Drawing.Color.White
        Label5.Location = New System.Drawing.Point(20, 199)
        Label5.Name = "Label5"
        Label5.Size = New System.Drawing.Size(55, 15)
        Label5.TabIndex = 101
        Label5.Text = "Status :"
        '
        'Label10
        '
        Label10.AutoSize = True
        Label10.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label10.ForeColor = System.Drawing.Color.White
        Label10.Location = New System.Drawing.Point(67, 123)
        Label10.Name = "Label10"
        Label10.Size = New System.Drawing.Size(52, 15)
        Label10.TabIndex = 101
        Label10.Text = "Saldo :"
        '
        'Label12
        '
        Label12.AutoSize = True
        Label12.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label12.ForeColor = System.Drawing.Color.White
        Label12.Location = New System.Drawing.Point(64, 100)
        Label12.Name = "Label12"
        Label12.Size = New System.Drawing.Size(55, 15)
        Label12.TabIndex = 35
        Label12.Text = "Monto :"
        '
        'Label14
        '
        Label14.AutoSize = True
        Label14.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label14.ForeColor = System.Drawing.Color.White
        Label14.Location = New System.Drawing.Point(56, 74)
        Label14.Name = "Label14"
        Label14.Size = New System.Drawing.Size(53, 15)
        Label14.TabIndex = 34
        Label14.Text = "Ticket :"
        '
        'Label15
        '
        Label15.AutoSize = True
        Label15.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label15.ForeColor = System.Drawing.Color.White
        Label15.Location = New System.Drawing.Point(50, 48)
        Label15.Name = "Label15"
        Label15.Size = New System.Drawing.Size(69, 15)
        Label15.TabIndex = 33
        Label15.Text = "Contrato :"
        '
        'Label17
        '
        Label17.AutoSize = True
        Label17.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label17.ForeColor = System.Drawing.Color.White
        Label17.Location = New System.Drawing.Point(65, 149)
        Label17.Name = "Label17"
        Label17.Size = New System.Drawing.Size(54, 15)
        Label17.TabIndex = 32
        Label17.Text = "Fecha :"
        '
        'Label19
        '
        Label19.AutoSize = True
        Label19.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label19.ForeColor = System.Drawing.Color.White
        Label19.Location = New System.Drawing.Point(4, 25)
        Label19.Name = "Label19"
        Label19.Size = New System.Drawing.Size(115, 15)
        Label19.TabIndex = 29
        Label19.Text = "Nota de Crédito :"
        '
        'SplitContainer1
        '
        Me.SplitContainer1.Location = New System.Drawing.Point(12, 12)
        Me.SplitContainer1.Name = "SplitContainer1"
        '
        'SplitContainer1.Panel1
        '
        Me.SplitContainer1.Panel1.AutoScroll = True
        Me.SplitContainer1.Panel1.BackColor = System.Drawing.Color.WhiteSmoke
        Me.SplitContainer1.Panel1.Controls.Add(Me.Label23)
        Me.SplitContainer1.Panel1.Controls.Add(Me.ComboBoxCiudad)
        Me.SplitContainer1.Panel1.Controls.Add(Me.Label22)
        Me.SplitContainer1.Panel1.Controls.Add(Me.ComboBoxCompanias)
        Me.SplitContainer1.Panel1.Controls.Add(Me.Panel1)
        Me.SplitContainer1.Panel1.Controls.Add(Me.Label1)
        Me.SplitContainer1.Panel1.Controls.Add(Me.Panel5)
        Me.SplitContainer1.Panel1.Controls.Add(Me.FOLIOTextBox)
        Me.SplitContainer1.Panel1.Controls.Add(Me.Label4)
        Me.SplitContainer1.Panel1.Controls.Add(Me.ComboBox4)
        Me.SplitContainer1.Panel1.Controls.Add(Me.CMBLabel5)
        Me.SplitContainer1.Panel1.Controls.Add(Me.Button1)
        Me.SplitContainer1.Panel1.Controls.Add(Me.Button7)
        Me.SplitContainer1.Panel1.Controls.Add(Me.FECHATextBox)
        Me.SplitContainer1.Panel1.Controls.Add(Me.SERIETextBox)
        Me.SplitContainer1.Panel1.Controls.Add(Me.CMBLabel1)
        Me.SplitContainer1.Panel1.Controls.Add(Me.Label3)
        Me.SplitContainer1.Panel1.Controls.Add(Me.Label2)
        '
        'SplitContainer1.Panel2
        '
        Me.SplitContainer1.Panel2.Controls.Add(Me.BUSCANOTASDECREDITODataGridView)
        Me.SplitContainer1.Panel2.Controls.Add(Me.Panel4)
        Me.SplitContainer1.Panel2.Controls.Add(Me.DataGridView1)
        Me.SplitContainer1.Panel2.Controls.Add(Me.Panel3)
        Me.SplitContainer1.Size = New System.Drawing.Size(836, 682)
        Me.SplitContainer1.SplitterDistance = 277
        Me.SplitContainer1.TabIndex = 23
        Me.SplitContainer1.TabStop = False
        '
        'Label23
        '
        Me.Label23.AutoSize = True
        Me.Label23.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold)
        Me.Label23.Location = New System.Drawing.Point(15, 3)
        Me.Label23.Name = "Label23"
        Me.Label23.Size = New System.Drawing.Size(65, 16)
        Me.Label23.TabIndex = 99
        Me.Label23.Text = "Ciudad :"
        Me.Label23.Visible = False
        '
        'ComboBoxCiudad
        '
        Me.ComboBoxCiudad.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ComboBoxCiudad.FormattingEnabled = True
        Me.ComboBoxCiudad.Location = New System.Drawing.Point(24, 22)
        Me.ComboBoxCiudad.Name = "ComboBoxCiudad"
        Me.ComboBoxCiudad.Size = New System.Drawing.Size(201, 23)
        Me.ComboBoxCiudad.TabIndex = 98
        Me.ComboBoxCiudad.Visible = False
        '
        'Label22
        '
        Me.Label22.AutoSize = True
        Me.Label22.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold)
        Me.Label22.ForeColor = System.Drawing.Color.FromArgb(CType(CType(128, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.Label22.Location = New System.Drawing.Point(12, 25)
        Me.Label22.Name = "Label22"
        Me.Label22.Size = New System.Drawing.Size(55, 16)
        Me.Label22.TabIndex = 61
        Me.Label22.Text = "Plaza :"
        '
        'ComboBoxCompanias
        '
        Me.ComboBoxCompanias.DisplayMember = "Clv_Vendedor"
        Me.ComboBoxCompanias.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold)
        Me.ComboBoxCompanias.ForeColor = System.Drawing.Color.Black
        Me.ComboBoxCompanias.FormattingEnabled = True
        Me.ComboBoxCompanias.Location = New System.Drawing.Point(14, 47)
        Me.ComboBoxCompanias.Name = "ComboBoxCompanias"
        Me.ComboBoxCompanias.Size = New System.Drawing.Size(239, 24)
        Me.ComboBoxCompanias.TabIndex = 60
        Me.ComboBoxCompanias.ValueMember = "Clv_Vendedor"
        '
        'Panel1
        '
        Me.Panel1.BackColor = System.Drawing.Color.DarkOrange
        Me.Panel1.Controls.Add(Label5)
        Me.Panel1.Controls.Add(Me.Label9)
        Me.Panel1.Controls.Add(Me.CMBNOMBRETextBox1)
        Me.Panel1.Controls.Add(ImporteLabel)
        Me.Panel1.Controls.Add(Me.ImporteLabel1)
        Me.Panel1.Controls.Add(NOMBRELabel)
        Me.Panel1.Controls.Add(ClienteLabel)
        Me.Panel1.Controls.Add(Me.ClienteLabel1)
        Me.Panel1.Controls.Add(FECHALabel)
        Me.Panel1.Controls.Add(Me.FECHALabel1)
        Me.Panel1.Controls.Add(FacturaLabel)
        Me.Panel1.Controls.Add(Me.FacturaLabel1)
        Me.Panel1.Controls.Add(SerieLabel)
        Me.Panel1.Controls.Add(Me.SerieLabel1)
        Me.Panel1.Controls.Add(Me.Clv_FacturaLabel1)
        Me.Panel1.Controls.Add(Me.Label8)
        Me.Panel1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Panel1.Location = New System.Drawing.Point(2, 477)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(251, 238)
        Me.Panel1.TabIndex = 27
        '
        'Label9
        '
        Me.Label9.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.BUSCAFACTURASBindingSource, "Status", True))
        Me.Label9.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label9.ForeColor = System.Drawing.Color.White
        Me.Label9.Location = New System.Drawing.Point(78, 199)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(169, 15)
        Me.Label9.TabIndex = 102
        '
        'BUSCAFACTURASBindingSource
        '
        Me.BUSCAFACTURASBindingSource.DataMember = "BUSCAFACTURAS"
        Me.BUSCAFACTURASBindingSource.DataSource = Me.NewsoftvDataSet1
        '
        'NewsoftvDataSet1
        '
        Me.NewsoftvDataSet1.DataSetName = "NewsoftvDataSet1"
        Me.NewsoftvDataSet1.EnforceConstraints = False
        Me.NewsoftvDataSet1.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'CMBNOMBRETextBox1
        '
        Me.CMBNOMBRETextBox1.BackColor = System.Drawing.Color.DarkOrange
        Me.CMBNOMBRETextBox1.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.CMBNOMBRETextBox1.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.BUSCAFACTURASBindingSource, "NOMBRE", True))
        Me.CMBNOMBRETextBox1.ForeColor = System.Drawing.Color.White
        Me.CMBNOMBRETextBox1.Location = New System.Drawing.Point(74, 131)
        Me.CMBNOMBRETextBox1.Multiline = True
        Me.CMBNOMBRETextBox1.Name = "CMBNOMBRETextBox1"
        Me.CMBNOMBRETextBox1.ReadOnly = True
        Me.CMBNOMBRETextBox1.Size = New System.Drawing.Size(177, 44)
        Me.CMBNOMBRETextBox1.TabIndex = 100
        Me.CMBNOMBRETextBox1.TabStop = False
        '
        'ImporteLabel1
        '
        Me.ImporteLabel1.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.BUSCAFACTURASBindingSource, "importe", True))
        Me.ImporteLabel1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ImporteLabel1.ForeColor = System.Drawing.Color.White
        Me.ImporteLabel1.Location = New System.Drawing.Point(81, 178)
        Me.ImporteLabel1.Name = "ImporteLabel1"
        Me.ImporteLabel1.Size = New System.Drawing.Size(167, 15)
        Me.ImporteLabel1.TabIndex = 36
        '
        'ClienteLabel1
        '
        Me.ClienteLabel1.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.BUSCAFACTURASBindingSource, "cliente", True))
        Me.ClienteLabel1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ClienteLabel1.ForeColor = System.Drawing.Color.White
        Me.ClienteLabel1.Location = New System.Drawing.Point(80, 105)
        Me.ClienteLabel1.Name = "ClienteLabel1"
        Me.ClienteLabel1.Size = New System.Drawing.Size(100, 23)
        Me.ClienteLabel1.TabIndex = 34
        '
        'FECHALabel1
        '
        Me.FECHALabel1.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.BUSCAFACTURASBindingSource, "FECHA", True))
        Me.FECHALabel1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FECHALabel1.ForeColor = System.Drawing.Color.White
        Me.FECHALabel1.Location = New System.Drawing.Point(83, 82)
        Me.FECHALabel1.Name = "FECHALabel1"
        Me.FECHALabel1.Size = New System.Drawing.Size(100, 23)
        Me.FECHALabel1.TabIndex = 33
        '
        'FacturaLabel1
        '
        Me.FacturaLabel1.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.BUSCAFACTURASBindingSource, "Factura", True))
        Me.FacturaLabel1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FacturaLabel1.ForeColor = System.Drawing.Color.White
        Me.FacturaLabel1.Location = New System.Drawing.Point(83, 59)
        Me.FacturaLabel1.Name = "FacturaLabel1"
        Me.FacturaLabel1.Size = New System.Drawing.Size(100, 23)
        Me.FacturaLabel1.TabIndex = 32
        '
        'SerieLabel1
        '
        Me.SerieLabel1.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.BUSCAFACTURASBindingSource, "Serie", True))
        Me.SerieLabel1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.SerieLabel1.ForeColor = System.Drawing.Color.White
        Me.SerieLabel1.Location = New System.Drawing.Point(83, 36)
        Me.SerieLabel1.Name = "SerieLabel1"
        Me.SerieLabel1.Size = New System.Drawing.Size(100, 23)
        Me.SerieLabel1.TabIndex = 31
        '
        'Clv_FacturaLabel1
        '
        Me.Clv_FacturaLabel1.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.BUSCAFACTURASBindingSource, "clv_Factura", True))
        Me.Clv_FacturaLabel1.ForeColor = System.Drawing.Color.DarkOrange
        Me.Clv_FacturaLabel1.Location = New System.Drawing.Point(188, 8)
        Me.Clv_FacturaLabel1.Name = "Clv_FacturaLabel1"
        Me.Clv_FacturaLabel1.Size = New System.Drawing.Size(58, 23)
        Me.Clv_FacturaLabel1.TabIndex = 0
        Me.Clv_FacturaLabel1.Visible = False
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label8.ForeColor = System.Drawing.Color.White
        Me.Label8.Location = New System.Drawing.Point(4, 4)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(131, 18)
        Me.Label8.TabIndex = 0
        Me.Label8.Text = "Datos del Ticket"
        '
        'Label1
        '
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(126, 271)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(127, 17)
        Me.Label1.TabIndex = 29
        Me.Label1.Text = "Ej. : dd/mm/aa"
        '
        'Panel5
        '
        Me.Panel5.Controls.Add(Me.Button3)
        Me.Panel5.Controls.Add(Me.Label6)
        Me.Panel5.Controls.Add(Me.Button4)
        Me.Panel5.Controls.Add(Me.CONTRATOTextBox)
        Me.Panel5.Controls.Add(Me.NOMBRETextBox)
        Me.Panel5.Controls.Add(Me.Label7)
        Me.Panel5.Location = New System.Drawing.Point(3, 325)
        Me.Panel5.Name = "Panel5"
        Me.Panel5.Size = New System.Drawing.Size(250, 146)
        Me.Panel5.TabIndex = 28
        '
        'Button3
        '
        Me.Button3.BackColor = System.Drawing.Color.DarkOrange
        Me.Button3.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button3.ForeColor = System.Drawing.Color.Black
        Me.Button3.Location = New System.Drawing.Point(14, 48)
        Me.Button3.Name = "Button3"
        Me.Button3.Size = New System.Drawing.Size(88, 23)
        Me.Button3.TabIndex = 8
        Me.Button3.Text = "&Buscar"
        Me.Button3.UseVisualStyleBackColor = False
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.Location = New System.Drawing.Point(11, 3)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(69, 15)
        Me.Label6.TabIndex = 21
        Me.Label6.Text = "Contrato :"
        '
        'Button4
        '
        Me.Button4.BackColor = System.Drawing.Color.DarkOrange
        Me.Button4.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button4.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button4.ForeColor = System.Drawing.Color.Black
        Me.Button4.Location = New System.Drawing.Point(14, 119)
        Me.Button4.Name = "Button4"
        Me.Button4.Size = New System.Drawing.Size(88, 23)
        Me.Button4.TabIndex = 10
        Me.Button4.Text = "&Buscar"
        Me.Button4.UseVisualStyleBackColor = False
        '
        'CONTRATOTextBox
        '
        Me.CONTRATOTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.CONTRATOTextBox.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.CONTRATOTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CONTRATOTextBox.Location = New System.Drawing.Point(14, 21)
        Me.CONTRATOTextBox.Name = "CONTRATOTextBox"
        Me.CONTRATOTextBox.Size = New System.Drawing.Size(88, 21)
        Me.CONTRATOTextBox.TabIndex = 7
        '
        'NOMBRETextBox
        '
        Me.NOMBRETextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.NOMBRETextBox.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.NOMBRETextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.NOMBRETextBox.Location = New System.Drawing.Point(14, 92)
        Me.NOMBRETextBox.Name = "NOMBRETextBox"
        Me.NOMBRETextBox.Size = New System.Drawing.Size(231, 21)
        Me.NOMBRETextBox.TabIndex = 9
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7.Location = New System.Drawing.Point(11, 74)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(66, 15)
        Me.Label7.TabIndex = 24
        Me.Label7.Text = "Nombre :"
        '
        'FOLIOTextBox
        '
        Me.FOLIOTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.FOLIOTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FOLIOTextBox.Location = New System.Drawing.Point(69, 193)
        Me.FOLIOTextBox.Name = "FOLIOTextBox"
        Me.FOLIOTextBox.Size = New System.Drawing.Size(88, 21)
        Me.FOLIOTextBox.TabIndex = 3
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.Location = New System.Drawing.Point(14, 195)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(47, 15)
        Me.Label4.TabIndex = 19
        Me.Label4.Text = "Folio :"
        '
        'ComboBox4
        '
        Me.ComboBox4.DataSource = Me.MUESTRATIPOFACTURABindingSource
        Me.ComboBox4.DisplayMember = "CONCEPTO"
        Me.ComboBox4.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ComboBox4.FormattingEnabled = True
        Me.ComboBox4.Location = New System.Drawing.Point(14, 109)
        Me.ComboBox4.Name = "ComboBox4"
        Me.ComboBox4.Size = New System.Drawing.Size(236, 24)
        Me.ComboBox4.TabIndex = 1
        Me.ComboBox4.ValueMember = "CLAVE"
        '
        'MUESTRATIPOFACTURABindingSource
        '
        Me.MUESTRATIPOFACTURABindingSource.DataMember = "MUESTRATIPOFACTURA"
        Me.MUESTRATIPOFACTURABindingSource.DataSource = Me.DataSetLydia
        '
        'DataSetLydia
        '
        Me.DataSetLydia.DataSetName = "DataSetLydia"
        Me.DataSetLydia.EnforceConstraints = False
        Me.DataSetLydia.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'CMBLabel5
        '
        Me.CMBLabel5.AutoSize = True
        Me.CMBLabel5.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold)
        Me.CMBLabel5.ForeColor = System.Drawing.Color.FromArgb(CType(CType(128, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.CMBLabel5.Location = New System.Drawing.Point(10, 89)
        Me.CMBLabel5.Name = "CMBLabel5"
        Me.CMBLabel5.Size = New System.Drawing.Size(121, 16)
        Me.CMBLabel5.TabIndex = 0
        Me.CMBLabel5.Text = "Tipo de Ticket  :"
        '
        'Button1
        '
        Me.Button1.BackColor = System.Drawing.Color.DarkOrange
        Me.Button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button1.ForeColor = System.Drawing.Color.Black
        Me.Button1.Location = New System.Drawing.Point(17, 298)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(88, 23)
        Me.Button1.TabIndex = 6
        Me.Button1.Text = "&Buscar"
        Me.Button1.UseVisualStyleBackColor = False
        '
        'Button7
        '
        Me.Button7.BackColor = System.Drawing.Color.DarkOrange
        Me.Button7.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button7.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button7.ForeColor = System.Drawing.Color.Black
        Me.Button7.Location = New System.Drawing.Point(17, 225)
        Me.Button7.Name = "Button7"
        Me.Button7.Size = New System.Drawing.Size(88, 23)
        Me.Button7.TabIndex = 4
        Me.Button7.Text = "&Buscar"
        Me.Button7.UseVisualStyleBackColor = False
        '
        'FECHATextBox
        '
        Me.FECHATextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.FECHATextBox.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.FECHATextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FECHATextBox.Location = New System.Drawing.Point(17, 271)
        Me.FECHATextBox.Name = "FECHATextBox"
        Me.FECHATextBox.Size = New System.Drawing.Size(88, 21)
        Me.FECHATextBox.TabIndex = 5
        '
        'SERIETextBox
        '
        Me.SERIETextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.SERIETextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.SERIETextBox.Location = New System.Drawing.Point(69, 166)
        Me.SERIETextBox.Name = "SERIETextBox"
        Me.SERIETextBox.Size = New System.Drawing.Size(88, 21)
        Me.SERIETextBox.TabIndex = 2
        '
        'CMBLabel1
        '
        Me.CMBLabel1.AutoSize = True
        Me.CMBLabel1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold)
        Me.CMBLabel1.ForeColor = System.Drawing.Color.FromArgb(CType(CType(128, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.CMBLabel1.Location = New System.Drawing.Point(13, 146)
        Me.CMBLabel1.Name = "CMBLabel1"
        Me.CMBLabel1.Size = New System.Drawing.Size(139, 16)
        Me.CMBLabel1.TabIndex = 0
        Me.CMBLabel1.Text = "Buscar Ticket Por :"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(14, 168)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(49, 15)
        Me.Label3.TabIndex = 0
        Me.Label3.Text = "Serie :"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(14, 253)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(54, 15)
        Me.Label2.TabIndex = 6
        Me.Label2.Text = "Fecha :"
        '
        'BUSCANOTASDECREDITODataGridView
        '
        Me.BUSCANOTASDECREDITODataGridView.AllowUserToAddRows = False
        Me.BUSCANOTASDECREDITODataGridView.AllowUserToDeleteRows = False
        Me.BUSCANOTASDECREDITODataGridView.AutoGenerateColumns = False
        DataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle9.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle9.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle9.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle9.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle9.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle9.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.BUSCANOTASDECREDITODataGridView.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle9
        Me.BUSCANOTASDECREDITODataGridView.ColumnHeadersHeight = 35
        Me.BUSCANOTASDECREDITODataGridView.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.ClvNotadecreditoDataGridViewTextBoxColumn1, Me.StatusDataGridViewTextBoxColumn, Me.Column1DataGridViewTextBoxColumn1, Me.FECHAdegeneracionDataGridViewTextBoxColumn1, Me.ContratoDataGridViewTextBoxColumn, Me.MontoDataGridViewTextBoxColumn1, Me.UsuariocapturaDataGridViewTextBoxColumn, Me.UsuarioautorizoDataGridViewTextBoxColumn, Me.FechacaducidadDataGridViewTextBoxColumn, Me.ObservacionesDataGridViewTextBoxColumn, Me.MotCanDataGridViewTextBoxColumn, Me.ClvFacturaAplicadaDataGridViewTextBoxColumn, Me.SaldoDataGridViewTextBoxColumn})
        Me.BUSCANOTASDECREDITODataGridView.DataSource = Me.BUSCANOTASDECREDITOBindingSource
        Me.BUSCANOTASDECREDITODataGridView.Location = New System.Drawing.Point(1, 22)
        Me.BUSCANOTASDECREDITODataGridView.Name = "BUSCANOTASDECREDITODataGridView"
        Me.BUSCANOTASDECREDITODataGridView.ReadOnly = True
        DataGridViewCellStyle10.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle10.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle10.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle10.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle10.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle10.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle10.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.BUSCANOTASDECREDITODataGridView.RowHeadersDefaultCellStyle = DataGridViewCellStyle10
        Me.BUSCANOTASDECREDITODataGridView.RowHeadersVisible = False
        Me.BUSCANOTASDECREDITODataGridView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.BUSCANOTASDECREDITODataGridView.Size = New System.Drawing.Size(554, 682)
        Me.BUSCANOTASDECREDITODataGridView.TabIndex = 29
        Me.BUSCANOTASDECREDITODataGridView.Visible = False
        '
        'ClvNotadecreditoDataGridViewTextBoxColumn1
        '
        Me.ClvNotadecreditoDataGridViewTextBoxColumn1.DataPropertyName = "clv_Notadecredito"
        Me.ClvNotadecreditoDataGridViewTextBoxColumn1.HeaderText = "clv_Notadecredito"
        Me.ClvNotadecreditoDataGridViewTextBoxColumn1.Name = "ClvNotadecreditoDataGridViewTextBoxColumn1"
        Me.ClvNotadecreditoDataGridViewTextBoxColumn1.ReadOnly = True
        Me.ClvNotadecreditoDataGridViewTextBoxColumn1.Visible = False
        '
        'StatusDataGridViewTextBoxColumn
        '
        Me.StatusDataGridViewTextBoxColumn.DataPropertyName = "Status"
        Me.StatusDataGridViewTextBoxColumn.HeaderText = "Status"
        Me.StatusDataGridViewTextBoxColumn.Name = "StatusDataGridViewTextBoxColumn"
        Me.StatusDataGridViewTextBoxColumn.ReadOnly = True
        '
        'Column1DataGridViewTextBoxColumn1
        '
        Me.Column1DataGridViewTextBoxColumn1.DataPropertyName = "Column1"
        Me.Column1DataGridViewTextBoxColumn1.HeaderText = "Nota de Crédito"
        Me.Column1DataGridViewTextBoxColumn1.Name = "Column1DataGridViewTextBoxColumn1"
        Me.Column1DataGridViewTextBoxColumn1.ReadOnly = True
        '
        'FECHAdegeneracionDataGridViewTextBoxColumn1
        '
        Me.FECHAdegeneracionDataGridViewTextBoxColumn1.DataPropertyName = "FECHA_degeneracion"
        Me.FECHAdegeneracionDataGridViewTextBoxColumn1.HeaderText = "Fecha"
        Me.FECHAdegeneracionDataGridViewTextBoxColumn1.Name = "FECHAdegeneracionDataGridViewTextBoxColumn1"
        Me.FECHAdegeneracionDataGridViewTextBoxColumn1.ReadOnly = True
        '
        'ContratoDataGridViewTextBoxColumn
        '
        Me.ContratoDataGridViewTextBoxColumn.DataPropertyName = "contrato"
        Me.ContratoDataGridViewTextBoxColumn.HeaderText = "Contrato"
        Me.ContratoDataGridViewTextBoxColumn.Name = "ContratoDataGridViewTextBoxColumn"
        Me.ContratoDataGridViewTextBoxColumn.ReadOnly = True
        '
        'MontoDataGridViewTextBoxColumn1
        '
        Me.MontoDataGridViewTextBoxColumn1.DataPropertyName = "monto"
        Me.MontoDataGridViewTextBoxColumn1.HeaderText = "Monto"
        Me.MontoDataGridViewTextBoxColumn1.Name = "MontoDataGridViewTextBoxColumn1"
        Me.MontoDataGridViewTextBoxColumn1.ReadOnly = True
        '
        'UsuariocapturaDataGridViewTextBoxColumn
        '
        Me.UsuariocapturaDataGridViewTextBoxColumn.DataPropertyName = "Usuario_captura"
        Me.UsuariocapturaDataGridViewTextBoxColumn.HeaderText = "Usuario que generó"
        Me.UsuariocapturaDataGridViewTextBoxColumn.Name = "UsuariocapturaDataGridViewTextBoxColumn"
        Me.UsuariocapturaDataGridViewTextBoxColumn.ReadOnly = True
        '
        'UsuarioautorizoDataGridViewTextBoxColumn
        '
        Me.UsuarioautorizoDataGridViewTextBoxColumn.DataPropertyName = "Usuario_autorizo"
        Me.UsuarioautorizoDataGridViewTextBoxColumn.HeaderText = "Usuario_autorizo"
        Me.UsuarioautorizoDataGridViewTextBoxColumn.Name = "UsuarioautorizoDataGridViewTextBoxColumn"
        Me.UsuarioautorizoDataGridViewTextBoxColumn.ReadOnly = True
        Me.UsuarioautorizoDataGridViewTextBoxColumn.Visible = False
        '
        'FechacaducidadDataGridViewTextBoxColumn
        '
        Me.FechacaducidadDataGridViewTextBoxColumn.DataPropertyName = "fecha_caducidad"
        Me.FechacaducidadDataGridViewTextBoxColumn.HeaderText = "Fecha de caducidad"
        Me.FechacaducidadDataGridViewTextBoxColumn.Name = "FechacaducidadDataGridViewTextBoxColumn"
        Me.FechacaducidadDataGridViewTextBoxColumn.ReadOnly = True
        '
        'ObservacionesDataGridViewTextBoxColumn
        '
        Me.ObservacionesDataGridViewTextBoxColumn.DataPropertyName = "observaciones"
        Me.ObservacionesDataGridViewTextBoxColumn.HeaderText = "observaciones"
        Me.ObservacionesDataGridViewTextBoxColumn.Name = "ObservacionesDataGridViewTextBoxColumn"
        Me.ObservacionesDataGridViewTextBoxColumn.ReadOnly = True
        Me.ObservacionesDataGridViewTextBoxColumn.Visible = False
        '
        'MotCanDataGridViewTextBoxColumn
        '
        Me.MotCanDataGridViewTextBoxColumn.DataPropertyName = "MotCan"
        Me.MotCanDataGridViewTextBoxColumn.HeaderText = "MotCan"
        Me.MotCanDataGridViewTextBoxColumn.Name = "MotCanDataGridViewTextBoxColumn"
        Me.MotCanDataGridViewTextBoxColumn.ReadOnly = True
        Me.MotCanDataGridViewTextBoxColumn.Visible = False
        '
        'ClvFacturaAplicadaDataGridViewTextBoxColumn
        '
        Me.ClvFacturaAplicadaDataGridViewTextBoxColumn.DataPropertyName = "Clv_Factura_Aplicada"
        Me.ClvFacturaAplicadaDataGridViewTextBoxColumn.HeaderText = "Clv_Factura_Aplicada"
        Me.ClvFacturaAplicadaDataGridViewTextBoxColumn.Name = "ClvFacturaAplicadaDataGridViewTextBoxColumn"
        Me.ClvFacturaAplicadaDataGridViewTextBoxColumn.ReadOnly = True
        Me.ClvFacturaAplicadaDataGridViewTextBoxColumn.Visible = False
        '
        'SaldoDataGridViewTextBoxColumn
        '
        Me.SaldoDataGridViewTextBoxColumn.DataPropertyName = "Saldo"
        Me.SaldoDataGridViewTextBoxColumn.HeaderText = "Saldo"
        Me.SaldoDataGridViewTextBoxColumn.Name = "SaldoDataGridViewTextBoxColumn"
        Me.SaldoDataGridViewTextBoxColumn.ReadOnly = True
        '
        'BUSCANOTASDECREDITOBindingSource
        '
        Me.BUSCANOTASDECREDITOBindingSource.DataMember = "BUSCANOTASDECREDITO"
        Me.BUSCANOTASDECREDITOBindingSource.DataSource = Me.DataSetLydia
        '
        'Panel4
        '
        Me.Panel4.Controls.Add(Me.Button10)
        Me.Panel4.Location = New System.Drawing.Point(319, 271)
        Me.Panel4.Name = "Panel4"
        Me.Panel4.Size = New System.Drawing.Size(157, 106)
        Me.Panel4.TabIndex = 30
        Me.Panel4.Visible = False
        '
        'Button10
        '
        Me.Button10.BackColor = System.Drawing.Color.Orange
        Me.Button10.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button10.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button10.ForeColor = System.Drawing.Color.Black
        Me.Button10.Location = New System.Drawing.Point(10, 11)
        Me.Button10.Name = "Button10"
        Me.Button10.Size = New System.Drawing.Size(136, 83)
        Me.Button10.TabIndex = 11
        Me.Button10.Text = "&Genera"
        Me.Button10.UseVisualStyleBackColor = False
        '
        'DataGridView1
        '
        Me.DataGridView1.AllowUserToAddRows = False
        Me.DataGridView1.AllowUserToDeleteRows = False
        Me.DataGridView1.AllowUserToOrderColumns = True
        Me.DataGridView1.BackgroundColor = System.Drawing.Color.WhiteSmoke
        DataGridViewCellStyle11.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle11.BackColor = System.Drawing.Color.Chocolate
        DataGridViewCellStyle11.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle11.ForeColor = System.Drawing.Color.White
        DataGridViewCellStyle11.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle11.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle11.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.DataGridView1.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle11
        Me.DataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DataGridView1.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.Status})
        Me.DataGridView1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.DataGridView1.Location = New System.Drawing.Point(0, 0)
        Me.DataGridView1.Name = "DataGridView1"
        Me.DataGridView1.ReadOnly = True
        Me.DataGridView1.RowHeadersVisible = False
        Me.DataGridView1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.DataGridView1.Size = New System.Drawing.Size(555, 682)
        Me.DataGridView1.StandardTab = True
        Me.DataGridView1.TabIndex = 0
        Me.DataGridView1.TabStop = False
        '
        'Status
        '
        Me.Status.DataPropertyName = "Status"
        DataGridViewCellStyle12.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Status.DefaultCellStyle = DataGridViewCellStyle12
        Me.Status.HeaderText = "Status"
        Me.Status.Name = "Status"
        Me.Status.ReadOnly = True
        '
        'Panel3
        '
        Me.Panel3.Controls.Add(Me.Button9)
        Me.Panel3.Location = New System.Drawing.Point(398, 411)
        Me.Panel3.Name = "Panel3"
        Me.Panel3.Size = New System.Drawing.Size(157, 106)
        Me.Panel3.TabIndex = 25
        Me.Panel3.Visible = False
        '
        'Button9
        '
        Me.Button9.BackColor = System.Drawing.Color.Orange
        Me.Button9.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button9.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button9.ForeColor = System.Drawing.Color.Black
        Me.Button9.Location = New System.Drawing.Point(10, 11)
        Me.Button9.Name = "Button9"
        Me.Button9.Size = New System.Drawing.Size(136, 83)
        Me.Button9.TabIndex = 11
        Me.Button9.Text = "Ver Evidencia"
        Me.Button9.UseVisualStyleBackColor = False
        '
        'Button5
        '
        Me.Button5.BackColor = System.Drawing.Color.DarkOrange
        Me.Button5.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button5.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button5.ForeColor = System.Drawing.Color.Black
        Me.Button5.Location = New System.Drawing.Point(856, 655)
        Me.Button5.Name = "Button5"
        Me.Button5.Size = New System.Drawing.Size(136, 36)
        Me.Button5.TabIndex = 12
        Me.Button5.Text = "&SALIR"
        Me.Button5.UseVisualStyleBackColor = False
        '
        'Button2
        '
        Me.Button2.BackColor = System.Drawing.Color.Orange
        Me.Button2.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button2.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button2.ForeColor = System.Drawing.Color.Black
        Me.Button2.Location = New System.Drawing.Point(1, 3)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(136, 83)
        Me.Button2.TabIndex = 11
        Me.Button2.Text = "&Cancelar Ticket"
        Me.Button2.UseVisualStyleBackColor = False
        '
        'BUSCAFACTURASTableAdapter
        '
        Me.BUSCAFACTURASTableAdapter.ClearBeforeFill = True
        '
        'CANCELACIONFACTURASBindingSource
        '
        Me.CANCELACIONFACTURASBindingSource.DataMember = "CANCELACIONFACTURAS"
        Me.CANCELACIONFACTURASBindingSource.DataSource = Me.NewsoftvDataSet1
        '
        'CANCELACIONFACTURASTableAdapter
        '
        Me.CANCELACIONFACTURASTableAdapter.ClearBeforeFill = True
        '
        'CMBPanel3
        '
        Me.CMBPanel3.Controls.Add(Me.Button11)
        Me.CMBPanel3.Controls.Add(Me.Button6)
        Me.CMBPanel3.Location = New System.Drawing.Point(854, 12)
        Me.CMBPanel3.Name = "CMBPanel3"
        Me.CMBPanel3.Size = New System.Drawing.Size(140, 175)
        Me.CMBPanel3.TabIndex = 25
        '
        'Button11
        '
        Me.Button11.BackColor = System.Drawing.Color.Orange
        Me.Button11.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button11.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button11.ForeColor = System.Drawing.Color.Black
        Me.Button11.Location = New System.Drawing.Point(2, 89)
        Me.Button11.Name = "Button11"
        Me.Button11.Size = New System.Drawing.Size(136, 83)
        Me.Button11.TabIndex = 12
        Me.Button11.Text = "&Enviar  Mail"
        Me.Button11.UseVisualStyleBackColor = False
        '
        'Button6
        '
        Me.Button6.BackColor = System.Drawing.Color.Orange
        Me.Button6.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button6.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button6.ForeColor = System.Drawing.Color.Black
        Me.Button6.Location = New System.Drawing.Point(2, 3)
        Me.Button6.Name = "Button6"
        Me.Button6.Size = New System.Drawing.Size(136, 83)
        Me.Button6.TabIndex = 11
        Me.Button6.Text = "&ReImprimir  Ticket"
        Me.Button6.UseVisualStyleBackColor = False
        '
        'CMBPanel4
        '
        Me.CMBPanel4.Controls.Add(Me.Button8)
        Me.CMBPanel4.Location = New System.Drawing.Point(855, 285)
        Me.CMBPanel4.Name = "CMBPanel4"
        Me.CMBPanel4.Size = New System.Drawing.Size(139, 91)
        Me.CMBPanel4.TabIndex = 26
        '
        'Button8
        '
        Me.Button8.BackColor = System.Drawing.Color.Orange
        Me.Button8.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button8.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button8.ForeColor = System.Drawing.Color.Black
        Me.Button8.Location = New System.Drawing.Point(3, 3)
        Me.Button8.Name = "Button8"
        Me.Button8.Size = New System.Drawing.Size(136, 83)
        Me.Button8.TabIndex = 11
        Me.Button8.Text = "&Ver  Ticket"
        Me.Button8.UseVisualStyleBackColor = False
        '
        'DAMEFECHADELSERVIDORBindingSource
        '
        Me.DAMEFECHADELSERVIDORBindingSource.DataMember = "DAMEFECHADELSERVIDOR"
        Me.DAMEFECHADELSERVIDORBindingSource.DataSource = Me.NewsoftvDataSet1
        '
        'DAMEFECHADELSERVIDORTableAdapter
        '
        Me.DAMEFECHADELSERVIDORTableAdapter.ClearBeforeFill = True
        '
        'FECHADateTimePicker
        '
        Me.FECHADateTimePicker.DataBindings.Add(New System.Windows.Forms.Binding("Value", Me.DAMEFECHADELSERVIDORBindingSource, "FECHA", True))
        Me.FECHADateTimePicker.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.FECHADateTimePicker.Location = New System.Drawing.Point(22, 655)
        Me.FECHADateTimePicker.Name = "FECHADateTimePicker"
        Me.FECHADateTimePicker.Size = New System.Drawing.Size(102, 20)
        Me.FECHADateTimePicker.TabIndex = 28
        Me.FECHADateTimePicker.TabStop = False
        '
        'CMBPanel2
        '
        Me.CMBPanel2.Controls.Add(Me.Button2)
        Me.CMBPanel2.Location = New System.Drawing.Point(855, 190)
        Me.CMBPanel2.Name = "CMBPanel2"
        Me.CMBPanel2.Size = New System.Drawing.Size(139, 91)
        Me.CMBPanel2.TabIndex = 24
        '
        'EricDataSet
        '
        Me.EricDataSet.DataSetName = "EricDataSet"
        Me.EricDataSet.EnforceConstraints = False
        Me.EricDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'CancelaCNRPPEBindingSource
        '
        Me.CancelaCNRPPEBindingSource.DataMember = "CancelaCNRPPE"
        Me.CancelaCNRPPEBindingSource.DataSource = Me.EricDataSet
        '
        'CancelaCNRPPETableAdapter
        '
        Me.CancelaCNRPPETableAdapter.ClearBeforeFill = True
        '
        'DataSetEdgar
        '
        Me.DataSetEdgar.DataSetName = "DataSetEdgar"
        Me.DataSetEdgar.EnforceConstraints = False
        Me.DataSetEdgar.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'CancelaCambioServClienteBindingSource
        '
        Me.CancelaCambioServClienteBindingSource.DataMember = "CancelaCambioServCliente"
        Me.CancelaCambioServClienteBindingSource.DataSource = Me.DataSetEdgar
        '
        'CancelaCambioServClienteTableAdapter
        '
        Me.CancelaCambioServClienteTableAdapter.ClearBeforeFill = True
        '
        'Procedimientos_arnoldo
        '
        Me.Procedimientos_arnoldo.DataSetName = "Procedimientos_arnoldo"
        Me.Procedimientos_arnoldo.EnforceConstraints = False
        Me.Procedimientos_arnoldo.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'Selecciona_impresoraticketsBindingSource
        '
        Me.Selecciona_impresoraticketsBindingSource.DataMember = "Selecciona_impresoratickets"
        Me.Selecciona_impresoraticketsBindingSource.DataSource = Me.Procedimientos_arnoldo
        '
        'Selecciona_impresoraticketsTableAdapter
        '
        Me.Selecciona_impresoraticketsTableAdapter.ClearBeforeFill = True
        '
        'BUSCANOTASDECREDITOTableAdapter
        '
        Me.BUSCANOTASDECREDITOTableAdapter.ClearBeforeFill = True
        '
        'MUESTRATIPOFACTURATableAdapter
        '
        Me.MUESTRATIPOFACTURATableAdapter.ClearBeforeFill = True
        '
        'Panel2
        '
        Me.Panel2.BackColor = System.Drawing.Color.DarkOrange
        Me.Panel2.Controls.Add(Label10)
        Me.Panel2.Controls.Add(Me.Label11)
        Me.Panel2.Controls.Add(Me.DetalleNOTASDECREDITODataGridView)
        Me.Panel2.Controls.Add(Me.CMBTextBox1)
        Me.Panel2.Controls.Add(Label12)
        Me.Panel2.Controls.Add(Me.Label13)
        Me.Panel2.Controls.Add(Label14)
        Me.Panel2.Controls.Add(Label15)
        Me.Panel2.Controls.Add(Me.Label16)
        Me.Panel2.Controls.Add(Label17)
        Me.Panel2.Controls.Add(Me.Label18)
        Me.Panel2.Controls.Add(Label19)
        Me.Panel2.Controls.Add(Me.Label20)
        Me.Panel2.Controls.Add(Me.Label21)
        Me.Panel2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Panel2.Location = New System.Drawing.Point(12, 380)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(272, 308)
        Me.Panel2.TabIndex = 29
        Me.Panel2.Visible = False
        '
        'Label11
        '
        Me.Label11.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.BUSCANOTASDECREDITOBindingSource, "Saldo", True))
        Me.Label11.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label11.ForeColor = System.Drawing.Color.White
        Me.Label11.Location = New System.Drawing.Point(137, 123)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(107, 23)
        Me.Label11.TabIndex = 102
        '
        'DetalleNOTASDECREDITODataGridView
        '
        Me.DetalleNOTASDECREDITODataGridView.AllowUserToAddRows = False
        Me.DetalleNOTASDECREDITODataGridView.AllowUserToDeleteRows = False
        Me.DetalleNOTASDECREDITODataGridView.AutoGenerateColumns = False
        Me.DetalleNOTASDECREDITODataGridView.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.Column1DataGridViewTextBoxColumn, Me.MontoDataGridViewTextBoxColumn, Me.FECHAdegeneracionDataGridViewTextBoxColumn, Me.ClvNotadecreditoDataGridViewTextBoxColumn})
        Me.DetalleNOTASDECREDITODataGridView.DataSource = Me.DetalleNOTASDECREDITOBindingSource
        Me.DetalleNOTASDECREDITODataGridView.Location = New System.Drawing.Point(0, 172)
        Me.DetalleNOTASDECREDITODataGridView.Name = "DetalleNOTASDECREDITODataGridView"
        Me.DetalleNOTASDECREDITODataGridView.ReadOnly = True
        Me.DetalleNOTASDECREDITODataGridView.Size = New System.Drawing.Size(273, 126)
        Me.DetalleNOTASDECREDITODataGridView.TabIndex = 47
        '
        'Column1DataGridViewTextBoxColumn
        '
        Me.Column1DataGridViewTextBoxColumn.DataPropertyName = "Column1"
        Me.Column1DataGridViewTextBoxColumn.HeaderText = "Ticket"
        Me.Column1DataGridViewTextBoxColumn.Name = "Column1DataGridViewTextBoxColumn"
        Me.Column1DataGridViewTextBoxColumn.ReadOnly = True
        '
        'MontoDataGridViewTextBoxColumn
        '
        Me.MontoDataGridViewTextBoxColumn.DataPropertyName = "monto"
        Me.MontoDataGridViewTextBoxColumn.HeaderText = "Monto"
        Me.MontoDataGridViewTextBoxColumn.Name = "MontoDataGridViewTextBoxColumn"
        Me.MontoDataGridViewTextBoxColumn.ReadOnly = True
        '
        'FECHAdegeneracionDataGridViewTextBoxColumn
        '
        Me.FECHAdegeneracionDataGridViewTextBoxColumn.DataPropertyName = "FECHA_degeneracion"
        Me.FECHAdegeneracionDataGridViewTextBoxColumn.HeaderText = "Fecha"
        Me.FECHAdegeneracionDataGridViewTextBoxColumn.Name = "FECHAdegeneracionDataGridViewTextBoxColumn"
        Me.FECHAdegeneracionDataGridViewTextBoxColumn.ReadOnly = True
        '
        'ClvNotadecreditoDataGridViewTextBoxColumn
        '
        Me.ClvNotadecreditoDataGridViewTextBoxColumn.DataPropertyName = "clv_Notadecredito"
        Me.ClvNotadecreditoDataGridViewTextBoxColumn.HeaderText = "Folio de la nota"
        Me.ClvNotadecreditoDataGridViewTextBoxColumn.Name = "ClvNotadecreditoDataGridViewTextBoxColumn"
        Me.ClvNotadecreditoDataGridViewTextBoxColumn.ReadOnly = True
        '
        'DetalleNOTASDECREDITOBindingSource
        '
        Me.DetalleNOTASDECREDITOBindingSource.DataMember = "DetalleNOTASDECREDITO"
        Me.DetalleNOTASDECREDITOBindingSource.DataSource = Me.DataSetLydia
        '
        'CMBTextBox1
        '
        Me.CMBTextBox1.BackColor = System.Drawing.Color.DarkOrange
        Me.CMBTextBox1.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.CMBTextBox1.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.BUSCANOTASDECREDITOBindingSource, "Column1", True))
        Me.CMBTextBox1.ForeColor = System.Drawing.Color.White
        Me.CMBTextBox1.Location = New System.Drawing.Point(140, 74)
        Me.CMBTextBox1.Multiline = True
        Me.CMBTextBox1.Name = "CMBTextBox1"
        Me.CMBTextBox1.ReadOnly = True
        Me.CMBTextBox1.Size = New System.Drawing.Size(107, 23)
        Me.CMBTextBox1.TabIndex = 100
        Me.CMBTextBox1.TabStop = False
        '
        'Label13
        '
        Me.Label13.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.BUSCANOTASDECREDITOBindingSource, "monto", True))
        Me.Label13.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label13.ForeColor = System.Drawing.Color.White
        Me.Label13.Location = New System.Drawing.Point(140, 100)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(107, 23)
        Me.Label13.TabIndex = 36
        '
        'Label16
        '
        Me.Label16.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.BUSCANOTASDECREDITOBindingSource, "contrato", True))
        Me.Label16.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label16.ForeColor = System.Drawing.Color.White
        Me.Label16.Location = New System.Drawing.Point(140, 48)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(107, 23)
        Me.Label16.TabIndex = 34
        '
        'Label18
        '
        Me.Label18.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.BUSCANOTASDECREDITOBindingSource, "FECHA_degeneracion", True))
        Me.Label18.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label18.ForeColor = System.Drawing.Color.White
        Me.Label18.Location = New System.Drawing.Point(140, 146)
        Me.Label18.Name = "Label18"
        Me.Label18.Size = New System.Drawing.Size(127, 23)
        Me.Label18.TabIndex = 33
        '
        'Label20
        '
        Me.Label20.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.BUSCANOTASDECREDITOBindingSource, "clv_Notadecredito", True))
        Me.Label20.ForeColor = System.Drawing.Color.DarkOrange
        Me.Label20.Location = New System.Drawing.Point(137, 23)
        Me.Label20.Name = "Label20"
        Me.Label20.Size = New System.Drawing.Size(107, 23)
        Me.Label20.TabIndex = 0
        '
        'Label21
        '
        Me.Label21.AutoSize = True
        Me.Label21.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label21.ForeColor = System.Drawing.Color.White
        Me.Label21.Location = New System.Drawing.Point(4, 3)
        Me.Label21.Name = "Label21"
        Me.Label21.Size = New System.Drawing.Size(218, 18)
        Me.Label21.TabIndex = 0
        Me.Label21.Text = "Datos de la Nota de Crédito"
        '
        'DetalleNOTASDECREDITOTableAdapter
        '
        Me.DetalleNOTASDECREDITOTableAdapter.ClearBeforeFill = True
        '
        'EricDataSet2
        '
        Me.EricDataSet2.DataSetName = "EricDataSet2"
        Me.EricDataSet2.EnforceConstraints = False
        Me.EricDataSet2.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'DameGeneralMsjTicketsBindingSource
        '
        Me.DameGeneralMsjTicketsBindingSource.DataMember = "DameGeneralMsjTickets"
        Me.DameGeneralMsjTicketsBindingSource.DataSource = Me.EricDataSet2
        '
        'DameGeneralMsjTicketsTableAdapter
        '
        Me.DameGeneralMsjTicketsTableAdapter.ClearBeforeFill = True
        '
        'ValidaCancelacionFacturaBindingSource
        '
        Me.ValidaCancelacionFacturaBindingSource.DataMember = "ValidaCancelacionFactura"
        Me.ValidaCancelacionFacturaBindingSource.DataSource = Me.EricDataSet2
        '
        'ValidaCancelacionFacturaTableAdapter
        '
        Me.ValidaCancelacionFacturaTableAdapter.ClearBeforeFill = True
        '
        'VerAcceso2TableAdapter1
        '
        Me.VerAcceso2TableAdapter1.ClearBeforeFill = True
        '
        'VerAcceso2TableAdapter2
        '
        Me.VerAcceso2TableAdapter2.ClearBeforeFill = True
        '
        'VerAcceso2TableAdapter3
        '
        Me.VerAcceso2TableAdapter3.ClearBeforeFill = True
        '
        'VerAcceso2TableAdapter4
        '
        Me.VerAcceso2TableAdapter4.ClearBeforeFill = True
        '
        'VerAcceso2TableAdapter5
        '
        Me.VerAcceso2TableAdapter5.ClearBeforeFill = True
        '
        'VerAcceso2TableAdapter6
        '
        Me.VerAcceso2TableAdapter6.ClearBeforeFill = True
        '
        'VerAcceso2TableAdapter7
        '
        Me.VerAcceso2TableAdapter7.ClearBeforeFill = True
        '
        'VerAcceso2TableAdapter8
        '
        Me.VerAcceso2TableAdapter8.ClearBeforeFill = True
        '
        'VerAcceso2TableAdapter9
        '
        Me.VerAcceso2TableAdapter9.ClearBeforeFill = True
        '
        'VerAcceso2TableAdapter10
        '
        Me.VerAcceso2TableAdapter10.ClearBeforeFill = True
        '
        'Button12
        '
        Me.Button12.BackColor = System.Drawing.Color.Orange
        Me.Button12.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button12.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button12.ForeColor = System.Drawing.Color.Black
        Me.Button12.Location = New System.Drawing.Point(2, 3)
        Me.Button12.Name = "Button12"
        Me.Button12.Size = New System.Drawing.Size(136, 83)
        Me.Button12.TabIndex = 11
        Me.Button12.Text = "Refac&turar Ticket"
        Me.Button12.UseVisualStyleBackColor = False
        '
        'CMBPanelRefacturacion
        '
        Me.CMBPanelRefacturacion.Controls.Add(Me.Button12)
        Me.CMBPanelRefacturacion.Location = New System.Drawing.Point(855, 380)
        Me.CMBPanelRefacturacion.Name = "CMBPanelRefacturacion"
        Me.CMBPanelRefacturacion.Size = New System.Drawing.Size(139, 91)
        Me.CMBPanelRefacturacion.TabIndex = 30
        Me.CMBPanelRefacturacion.Visible = False
        '
        'BrwFacturas_Cancelar
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.AutoScroll = True
        Me.ClientSize = New System.Drawing.Size(1016, 703)
        Me.Controls.Add(Me.CMBPanelRefacturacion)
        Me.Controls.Add(Me.CMBPanel2)
        Me.Controls.Add(Me.SplitContainer1)
        Me.Controls.Add(Me.Button5)
        Me.Controls.Add(Me.FECHADateTimePicker)
        Me.Controls.Add(Me.CMBPanel4)
        Me.Controls.Add(Me.CMBPanel3)
        Me.Controls.Add(Me.Panel2)
        Me.MaximizeBox = False
        Me.Name = "BrwFacturas_Cancelar"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Cancelación de Facturas"
        Me.SplitContainer1.Panel1.ResumeLayout(False)
        Me.SplitContainer1.Panel1.PerformLayout()
        Me.SplitContainer1.Panel2.ResumeLayout(False)
        Me.SplitContainer1.ResumeLayout(False)
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        CType(Me.BUSCAFACTURASBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.NewsoftvDataSet1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel5.ResumeLayout(False)
        Me.Panel5.PerformLayout()
        CType(Me.MUESTRATIPOFACTURABindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DataSetLydia, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.BUSCANOTASDECREDITODataGridView, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.BUSCANOTASDECREDITOBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel4.ResumeLayout(False)
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel3.ResumeLayout(False)
        CType(Me.CANCELACIONFACTURASBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.CMBPanel3.ResumeLayout(False)
        Me.CMBPanel4.ResumeLayout(False)
        CType(Me.DAMEFECHADELSERVIDORBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.CMBPanel2.ResumeLayout(False)
        CType(Me.EricDataSet, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CancelaCNRPPEBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DataSetEdgar, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CancelaCambioServClienteBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Procedimientos_arnoldo, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Selecciona_impresoraticketsBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel2.ResumeLayout(False)
        Me.Panel2.PerformLayout()
        CType(Me.DetalleNOTASDECREDITODataGridView, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DetalleNOTASDECREDITOBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.EricDataSet2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DameGeneralMsjTicketsBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ValidaCancelacionFacturaBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.CMBPanelRefacturacion.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents SplitContainer1 As System.Windows.Forms.SplitContainer
    Friend WithEvents ComboBox4 As System.Windows.Forms.ComboBox
    Friend WithEvents CMBLabel5 As System.Windows.Forms.Label
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents Button7 As System.Windows.Forms.Button
    Friend WithEvents FECHATextBox As System.Windows.Forms.TextBox
    Friend WithEvents SERIETextBox As System.Windows.Forms.TextBox
    Friend WithEvents CMBLabel1 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents DataGridView1 As System.Windows.Forms.DataGridView
    Friend WithEvents Button5 As System.Windows.Forms.Button
    Friend WithEvents Button2 As System.Windows.Forms.Button
    Friend WithEvents FOLIOTextBox As System.Windows.Forms.TextBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Button4 As System.Windows.Forms.Button
    Friend WithEvents NOMBRETextBox As System.Windows.Forms.TextBox
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents Button3 As System.Windows.Forms.Button
    Friend WithEvents CONTRATOTextBox As System.Windows.Forms.TextBox
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents NewsoftvDataSet1 As softvFacturacion.NewsoftvDataSet1
    Friend WithEvents BUSCAFACTURASBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents BUSCAFACTURASTableAdapter As softvFacturacion.NewsoftvDataSet1TableAdapters.BUSCAFACTURASTableAdapter
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents ClienteLabel1 As System.Windows.Forms.Label
    Friend WithEvents FECHALabel1 As System.Windows.Forms.Label
    Friend WithEvents FacturaLabel1 As System.Windows.Forms.Label
    Friend WithEvents SerieLabel1 As System.Windows.Forms.Label
    Friend WithEvents Clv_FacturaLabel1 As System.Windows.Forms.Label
    Friend WithEvents CMBNOMBRETextBox1 As System.Windows.Forms.TextBox
    Friend WithEvents ImporteLabel1 As System.Windows.Forms.Label
    Friend WithEvents CANCELACIONFACTURASBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents CANCELACIONFACTURASTableAdapter As softvFacturacion.NewsoftvDataSet1TableAdapters.CANCELACIONFACTURASTableAdapter
    Friend WithEvents CMBPanel3 As System.Windows.Forms.Panel
    Friend WithEvents Button6 As System.Windows.Forms.Button
    Friend WithEvents CMBPanel4 As System.Windows.Forms.Panel
    Friend WithEvents Button8 As System.Windows.Forms.Button
    Friend WithEvents Panel5 As System.Windows.Forms.Panel
    Friend WithEvents DAMEFECHADELSERVIDORBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents DAMEFECHADELSERVIDORTableAdapter As softvFacturacion.NewsoftvDataSet1TableAdapters.DAMEFECHADELSERVIDORTableAdapter
    Friend WithEvents FECHADateTimePicker As System.Windows.Forms.DateTimePicker
    Friend WithEvents CMBPanel2 As System.Windows.Forms.Panel
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents EricDataSet As softvFacturacion.EricDataSet
    Friend WithEvents CancelaCNRPPEBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents CancelaCNRPPETableAdapter As softvFacturacion.EricDataSetTableAdapters.CancelaCNRPPETableAdapter
    Friend WithEvents DataSetEdgar As softvFacturacion.DataSetEdgar
    Friend WithEvents CancelaCambioServClienteBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents CancelaCambioServClienteTableAdapter As softvFacturacion.DataSetEdgarTableAdapters.CancelaCambioServClienteTableAdapter
    Friend WithEvents Procedimientos_arnoldo As softvFacturacion.Procedimientos_arnoldo
    Friend WithEvents Selecciona_impresoraticketsBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Selecciona_impresoraticketsTableAdapter As softvFacturacion.Procedimientos_arnoldoTableAdapters.Selecciona_impresoraticketsTableAdapter
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents Status As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataSetLydia As softvFacturacion.DataSetLydia
    Friend WithEvents BUSCANOTASDECREDITOBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents BUSCANOTASDECREDITOTableAdapter As softvFacturacion.DataSetLydiaTableAdapters.BUSCANOTASDECREDITOTableAdapter
    Friend WithEvents BUSCANOTASDECREDITODataGridView As System.Windows.Forms.DataGridView
    Friend WithEvents MUESTRATIPOFACTURABindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents MUESTRATIPOFACTURATableAdapter As softvFacturacion.DataSetLydiaTableAdapters.MUESTRATIPOFACTURATableAdapter
    Friend WithEvents Panel2 As System.Windows.Forms.Panel
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents DetalleNOTASDECREDITODataGridView As System.Windows.Forms.DataGridView
    Friend WithEvents CMBTextBox1 As System.Windows.Forms.TextBox
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents Label16 As System.Windows.Forms.Label
    Friend WithEvents Label18 As System.Windows.Forms.Label
    Friend WithEvents Label20 As System.Windows.Forms.Label
    Friend WithEvents Label21 As System.Windows.Forms.Label
    Friend WithEvents DetalleNOTASDECREDITOBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents DetalleNOTASDECREDITOTableAdapter As softvFacturacion.DataSetLydiaTableAdapters.DetalleNOTASDECREDITOTableAdapter
    Friend WithEvents EricDataSet2 As softvFacturacion.EricDataSet2
    Friend WithEvents DameGeneralMsjTicketsBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents DameGeneralMsjTicketsTableAdapter As softvFacturacion.EricDataSet2TableAdapters.DameGeneralMsjTicketsTableAdapter
    Friend WithEvents ValidaCancelacionFacturaBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents ValidaCancelacionFacturaTableAdapter As softvFacturacion.EricDataSet2TableAdapters.ValidaCancelacionFacturaTableAdapter
    Friend WithEvents VerAcceso2TableAdapter1 As softvFacturacion.ProcedimientosArnoldo3TableAdapters.VerAcceso2TableAdapter
    Friend WithEvents Column1DataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents MontoDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents FECHAdegeneracionDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ClvNotadecreditoDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents VerAcceso2TableAdapter2 As softvFacturacion.ProcedimientosArnoldo3TableAdapters.VerAcceso2TableAdapter
    Friend WithEvents VerAcceso2TableAdapter3 As softvFacturacion.ProcedimientosArnoldo3TableAdapters.VerAcceso2TableAdapter
    Friend WithEvents VerAcceso2TableAdapter4 As softvFacturacion.ProcedimientosArnoldo3TableAdapters.VerAcceso2TableAdapter
    Friend WithEvents Label22 As System.Windows.Forms.Label
    Friend WithEvents ComboBoxCompanias As System.Windows.Forms.ComboBox
    Friend WithEvents VerAcceso2TableAdapter5 As softvFacturacion.ProcedimientosArnoldo3TableAdapters.VerAcceso2TableAdapter
    Friend WithEvents VerAcceso2TableAdapter6 As softvFacturacion.ProcedimientosArnoldo3TableAdapters.VerAcceso2TableAdapter
    Friend WithEvents VerAcceso2TableAdapter7 As softvFacturacion.ProcedimientosArnoldo3TableAdapters.VerAcceso2TableAdapter
    Friend WithEvents VerAcceso2TableAdapter8 As softvFacturacion.ProcedimientosArnoldo3TableAdapters.VerAcceso2TableAdapter
    Friend WithEvents Label23 As System.Windows.Forms.Label
    Friend WithEvents ComboBoxCiudad As System.Windows.Forms.ComboBox
    Friend WithEvents VerAcceso2TableAdapter9 As softvFacturacion.ProcedimientosArnoldo3TableAdapters.VerAcceso2TableAdapter
    Friend WithEvents VerAcceso2TableAdapter10 As softvFacturacion.ProcedimientosArnoldo3TableAdapters.VerAcceso2TableAdapter
    Friend WithEvents ClvNotadecreditoDataGridViewTextBoxColumn1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents StatusDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column1DataGridViewTextBoxColumn1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents FECHAdegeneracionDataGridViewTextBoxColumn1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ContratoDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents MontoDataGridViewTextBoxColumn1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents UsuariocapturaDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents UsuarioautorizoDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents FechacaducidadDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ObservacionesDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents MotCanDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ClvFacturaAplicadaDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents SaldoDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Panel3 As System.Windows.Forms.Panel
    Friend WithEvents Button9 As System.Windows.Forms.Button
    Friend WithEvents Panel4 As System.Windows.Forms.Panel
    Friend WithEvents Button10 As System.Windows.Forms.Button
    Friend WithEvents Button11 As System.Windows.Forms.Button
    Friend WithEvents Button12 As System.Windows.Forms.Button
    Friend WithEvents CMBPanelRefacturacion As System.Windows.Forms.Panel
End Class
