﻿Imports System.Linq
Public Class FrmCargaContratosConciliacion
    Public Class DetallePago
        Private _contrato As Integer
        Private _contratoCompuesto As String
        Private _importe As Decimal
        Private _metodoPago As String
        Private _sucursal As Integer
        Private _referencia As String
        Private _nombreSucursal As String
        Private _idMetodoPago As Integer
        Public Property ContratoLoc() As Integer
            Get
                Return _contrato
            End Get
            Set(ByVal value As Integer)
                _contrato = value
            End Set
        End Property
        Public Property Importe() As Decimal
            Get
                Return _importe
            End Get
            Set(ByVal value As Decimal)
                _importe = value
            End Set
        End Property
        Public Property Sucursal() As Integer
            Get
                Return _sucursal
            End Get
            Set(ByVal value As Integer)
                _sucursal = value
            End Set
        End Property
        Public Property MetodoPago() As String
            Get
                Return _metodoPago
            End Get
            Set(ByVal value As String)
                _metodoPago = value
            End Set
        End Property
        Public Property ContratoCompuesto() As String
            Get
                Return _contratoCompuesto
            End Get
            Set(ByVal value As String)
                _contratoCompuesto = value
            End Set
        End Property
        Public Property Referencia() As String
            Get
                Return _referencia
            End Get
            Set(ByVal value As String)
                _referencia = value
            End Set
        End Property
        Public Property NombreSucursal() As String
            Get
                Return _nombreSucursal
            End Get
            Set(ByVal value As String)
                _nombreSucursal = value
            End Set
        End Property

        Public Property IdMetodoPago() As Integer
            Get
                Return _idMetodoPago
            End Get
            Set(ByVal value As Integer)
                _idMetodoPago = value
            End Set
        End Property
    End Class

    Public Class MetodosPago
        Private _idMetodo As Integer
        Private _nombre As String
        Public Property IdMetodo() As Integer
            Get
                Return _idMetodo
            End Get
            Set(ByVal value As Integer)
                _idMetodo = value
            End Set
        End Property
        Public Property Nombre() As String
            Get
                Return _nombre
            End Get
            Set(ByVal value As String)
                _nombre = value
            End Set
        End Property
    End Class

    Public listaContratos As New List(Of DetallePago)
    Public listaMetodos As New List(Of MetodosPago)
    Dim bs As BindingSource
    Private Sub FrmCargaContratosConciliacion_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        colorea(Me)
        'Vamos a llenar los combos
        listaMetodos.Add(New MetodosPago() With {.IdMetodo = 1, .Nombre = "Efectivo"})
        listaMetodos.Add(New MetodosPago() With {.IdMetodo = 2, .Nombre = "Transferencia Electrónica"})
        listaMetodos.Add(New MetodosPago() With {.IdMetodo = 3, .Nombre = "Cheque"})
        cbMetodo.DataSource = listaMetodos
        BaseII.limpiaParametros()
        cbSucursal.DataSource = BaseII.ConsultaDT("ObtieneSucursalesEspecialesConciliacion")
        bs = New BindingSource()
        bs.DataSource = listaContratos
        dgvContratos.DataSource = bs
    End Sub

    Private Sub btnAgregar_Click(sender As Object, e As EventArgs) Handles btnAgregar.Click
        Try
            If tbContrato.Text.Length = 0 Or tbImporte.Text.Length = 0 Or tbImporte.Text = 0 Then
                MsgBox("Faltan campos por llenar para poder proseguir")
                Exit Sub
            End If
            Dim contratoCompuesto As Array = tbContrato.Text.Trim.Split("-")
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@contratoCompania", SqlDbType.Int, contratoCompuesto(0))
            BaseII.CreateMyParameter("@idcompania", SqlDbType.Int, contratoCompuesto(1))
            BaseII.CreateMyParameter("@contrato", ParameterDirection.Output, SqlDbType.Int)
            BaseII.ProcedimientoOutPut("sp_dameContrato")
            Dim dp As New DetallePago()
            dp.ContratoLoc = BaseII.dicoPar("@contrato")
            dp.ContratoCompuesto = tbContrato.Text
            dp.Importe = tbImporte.Text
            dp.Referencia = tbReferencia.Text
            dp.MetodoPago = cbMetodo.Text
            dp.IdMetodoPago = cbMetodo.SelectedValue
            dp.Sucursal = cbSucursal.SelectedValue
            dp.NombreSucursal = cbSucursal.Text
            listaContratos.Add(dp)
            'dgvContratos.DataSource = listaContratos
            bs.ResetBindings(False)
            tbContrato.Text = ""
            tbImporte.Text = ""
            tbReferencia.Text = ""
            tbContrato.Focus()
        Catch ex As Exception

        End Try
    End Sub

    Private Sub btnAceptar_Click(sender As Object, e As EventArgs) Handles btnAceptar.Click
        Me.Close()
    End Sub

    Private Sub btnSalir_Click(sender As Object, e As EventArgs) Handles btnSalir.Click
        listaContratos.Clear()
        Me.Close()
    End Sub

    Private Sub btnQuitar_Click(sender As Object, e As EventArgs) Handles btnQuitar.Click
        If listaContratos.Count > 0 Then
            listaContratos.RemoveAll(Function(DetallePago) DetallePago.ContratoLoc = dgvContratos.SelectedCells(0).Value)
            bs.ResetBindings(False)
        End If
    End Sub
End Class