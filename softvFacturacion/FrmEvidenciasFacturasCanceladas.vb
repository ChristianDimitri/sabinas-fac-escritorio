﻿Imports System.Data.SqlClient
Imports System.IO
Imports System.Collections.Generic
Imports System.ComponentModel
Imports System.Data
Imports System.Drawing
Imports System.Xml
Imports System.Net
Imports System.Linq
Imports System.Text
Imports System.Windows.Forms
Public Class FrmEvidenciasFacturasCanceladas

    Private Sub FrmEvidenciasFacturasCanceladas_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Try
            Dim conexion As New SqlConnection(MiConexion)
            conexion.Open()
            Dim comando As New SqlCommand()
            comando.Connection = conexion
            comando.CommandText = "select evidencia from EvidenciaCancelacionTicket where clv_factura=" + eCveFactura.ToString
            Dim reader As SqlDataReader = comando.ExecuteReader
            reader.Read()
            Dim by(reader.GetBytes(0, 0, Nothing, 0, 0) - 1) As Byte
            reader.GetBytes(0, 0, by, 0, by.Length)
            Dim ms As System.IO.MemoryStream = New System.IO.MemoryStream(by)
            PictureBox1.Image = Image.FromStream(ms)
            reader.Close()
            Dim comando2 As New SqlCommand()
            comando2.Connection = conexion
            comando2.CommandText = "exec DameDatosFacturaCancelada " + eCveFactura.ToString
            Dim reader2 As SqlDataReader = comando2.ExecuteReader()
            reader2.Read()
            tbContrato.Text = reader2(0).ToString
            tbClvFactura.Text = reader2(1).ToString
            tbSerie.Text = reader2(2).ToString
            tbFolio.Text = reader2(3).ToString
            reader2.Close()
            conexion.Close()
        Catch ex As Exception

        End Try
    End Sub
End Class