Imports System.Data.SqlClient
Public Class FrmSelFechas
    Private Sub Llena_companias()
        Try
            BaseII.limpiaParametros()
            'BaseII.CreateMyParameter("@Op", SqlDbType.Int, 3)
            BaseII.CreateMyParameter("@ClvUsuario", SqlDbType.Int, GloClvUsuario)
            ComboBoxCompanias.DataSource = BaseII.ConsultaDT("Muestra_Compania_RelUsuario")
            ComboBoxCompanias.DisplayMember = "razon_social"
            ComboBoxCompanias.ValueMember = "id_compania"

            If ComboBoxCompanias.Items.Count > 0 Then
                ComboBoxCompanias.SelectedIndex = 0

            End If
            GloIdCompania = 0
            'ComboBoxCiudades.Text = ""
        Catch ex As Exception

        End Try
    End Sub
    Private Sub Llena_Ciudad()
        Try
            BaseII.limpiaParametros()
            ComboBoxCiudad.DataSource = BaseII.ConsultaDT("Muestra_ciudad")
            ComboBoxCiudad.DisplayMember = "Nombre"
            ComboBoxCiudad.ValueMember = "clv_ciudad"

            If ComboBoxCiudad.Items.Count > 0 Then
                ComboBoxCiudad.SelectedIndex = 0
            End If
        Catch ex As Exception

        End Try
    End Sub
    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        Me.Close()
    End Sub


    Private Sub FrmSelFechas_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        colorea(Me)
        Llena_Ciudad()
        Llena_companias()
        GloIdCompania = ComboBoxCompanias.SelectedValue
        Dim CON As New SqlConnection(MiConexion)
        If LocBanderaRep1 = 0 Or LocBanderaRep1 = 1 Then
            Select Case LocBanderaRep1
                Case 0
                    Me.CMBLabel6.Text = "Usuario que Cancel�:"
                    Me.Text = "Selecci�n Fechas Listado Tickets Canceladas"
                Case 1
                    Me.CMBLabel6.Text = "Usuario que Reimprimi�:"
                    Me.Text = "Selecci�n Fechas Listado Tickets Reimpresas"
            End Select
            CON.Open() 'Miguel modificar, funciona para ambos
            Me.Muestra_Usuarios_Que_cancelaron_ImprimieronTableAdapter.Connection = CON
            Me.Muestra_Usuarios_Que_cancelaron_ImprimieronTableAdapter.Fill(Me.Procedimientos_arnoldo.Muestra_Usuarios_Que_cancelaron_Imprimieron, LocBanderaRep1, GloClvUsuario)
            CON.Close()
            Me.CheckBox1.Visible = False
            Me.CMBLabel5.Visible = False
        ElseIf LocBanderaRep1 = 2 Then
            'Me.ComboBox2.Visible = True
            LocResumenBon = False
            Me.ComboBox1.Visible = False
            Me.CMBLabel6.Text = "Usuario que registr� la bonificaci�n:"
            Me.Text = "Selecci�n Fechas Bonificaciones"
            CON.Open()
            Me.Muestra_Usuarios_Que_BonificaronTableAdapter.Connection = CON
            Me.Muestra_Usuarios_Que_BonificaronTableAdapter.Fill(Me.ProcedimientosArnoldo3.Muestra_Usuarios_Que_Bonificaron, LocBanderaRep1, ComboBoxCompanias.SelectedValue)
            CON.Close()
        End If
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        'If ComboBoxCompanias.SelectedValue = 0 Then
        '    MsgBox("Selecciona una Compa��a")
        '    Exit Sub
        'End If
        GloIdCompania = ComboBoxCompanias.SelectedValue
        GloClvCiudad = ComboBoxCiudad.SelectedValue
        LocFecha1 = Me.DateTimePicker1.Text
        LocFecha2 = Me.DateTimePicker2.Text
        Select Case LocBanderaRep1
            Case 0
                Locclv_usuario = Me.ComboBox1.SelectedValue
                LocNombreusuario = Me.ComboBox1.Text
                LocBndrepfac1 = True
            Case 1
                Locclv_usuario = Me.ComboBox1.SelectedValue
                LocNombreusuario = Me.ComboBox1.Text
                LocBndrepfac1 = True
            Case 2
                Locclv_usuario = Me.ComboBox2.SelectedValue
                LocNombreusuario = Me.ComboBox2.Text
                LocBndBon = True
        End Select
        'MsgBox(Locclv_usuario)
        FrmImprimirRepGral.Show()
        Me.Close()
    End Sub

    Private Sub CheckBox1_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CheckBox1.CheckedChanged
        If Me.CheckBox1.CheckState = CheckState.Checked Then
            LocResumenBon = True
        Else
            LocResumenBon = False
        End If
    End Sub

    Private Sub DateTimePicker1_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles DateTimePicker1.ValueChanged
        Me.DateTimePicker2.MinDate = Me.DateTimePicker1.Value
    End Sub

    Private Sub ComboBoxCompanias_SelectedIndexChanged(sender As System.Object, e As System.EventArgs) Handles ComboBoxCompanias.SelectedIndexChanged
        Try
            GloIdCompania = ComboBoxCompanias.SelectedValue
            NombreCompania = ComboBoxCompanias.Text
        Catch ex As Exception

        End Try
    End Sub

    Private Sub ComboBoxCiudad_SelectedIndexChanged(sender As System.Object, e As System.EventArgs) Handles ComboBoxCiudad.SelectedIndexChanged
        Try
            GloClvCiudad = ComboBoxCiudad.SelectedValue
            Llena_companias()
        Catch ex As Exception

        End Try
    End Sub

    Private Sub Label15_Click(sender As System.Object, e As System.EventArgs) Handles Label15.Click

    End Sub

    Private Sub DateTimePicker2_ValueChanged(sender As Object, e As EventArgs) Handles DateTimePicker2.ValueChanged

    End Sub
End Class