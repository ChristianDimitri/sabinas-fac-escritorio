Imports CrystalDecisions.Shared
Imports CrystalDecisions.CrystalReports.Engine
Imports System.Data.SqlClient
Imports System.Text

Public Class FrmMenu
    Dim j As Integer = 0
    Private eRes As Integer = 0
    Private eMsg As String = String.Empty
    Private prmCorteFinal As Integer = 0
    Dim prmDesglose As Integer
    Private customersByCityReport As ReportDocument
    Private eMsjTickets As String = Nothing
    Private eActTickets As Boolean = False
    Private identi As Integer = 0
    Private eCont As Integer = 1

    Private Sub SalirToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles SalirToolStripMenuItem.Click
        bitsist(GloUsuario, 0, GloSistema, "Salida Del Sistema", "", "Salida Del Sistema", GloNomSucursal, SubCiudad)
        End
    End Sub

    Private Sub CajasToolStripMenuItem2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CajasToolStripMenuItem2.Click
        Dim DT As New DataTable
        DT = UspDameClaveMenuFac(sender.name())
        If DT.Rows.Count > 0 Then
            GloClaveMenus = CInt(DT.Rows(0)(0).ToString)
        End If
        GloTipo = "C"
        If IdSistema = "LO" Then
            'MsgBox(1)
            FrmFACLogitel.Text = "Facturación"
            FrmFACLogitel.Show()
        Else
            'MsgBox(2)
            FrmFAC.Text = "Facturación"
            FrmFAC.Show()
        End If


    End Sub

    Private Sub FrmMenu_Activated(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Activated
        CMBLabelciudad.Text = GloCiudadEmpresa
        If bndcancelareportcargos = True Then
            bndcancelareportcargos = False
            Dim con4 As New SqlClient.SqlConnection(MiConexion)
            Dim cmd As New SqlClient.SqlCommand()

            cmd = New SqlClient.SqlCommand()
            con4.Open()
            With cmd
                .CommandText = "Borra_tablas_reporte_cargos"
                .CommandTimeout = 0
                .CommandType = CommandType.StoredProcedure
                .Connection = con4

                Dim prm As New SqlParameter("@clv_session", SqlDbType.BigInt)
                prm.Direction = ParameterDirection.Input
                prm.Value = locclv_sessioncargosauto

                .Parameters.Add(prm)

                Dim i As Integer = cmd.ExecuteNonQuery()
            End With
            con4.Close()
        End If

    End Sub

    Private Sub FrmMenu_Enter(sender As Object, e As EventArgs) Handles Me.Enter

    End Sub

    Private Sub FrmMenu_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        bitsist(GloUsuario, 0, GloSistema, "Salida Del Sistema", "", "Salida Del Sistema", GloNomSucursal, SubCiudad)
        End
    End Sub

    Private Sub FrmMenu_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'asas
        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''Inicio guardar menus para perfiles
        Dim NOMBREMENU As ToolStripMenuItem
        Dim NOMBRESUBMENU As ToolStripMenuItem

        Dim I, J, K As Integer

        If GloUsuario = "SISTE" Then
            For I = 0 To MenuStrip1.Items.Count - 1
                UspGuardaMenu(Me.MenuStrip1.Items(I).Name, Me.MenuStrip1.Items(I).Text, "", 1)
                NOMBREMENU = Me.MenuStrip1.Items(I)
                For J = 0 To NOMBREMENU.DropDownItems.Count - 1
                    UspGuardaMenu(NOMBREMENU.DropDownItems(J).Name, NOMBREMENU.DropDownItems(J).Text, NOMBREMENU.Name, 2)
                    NOMBRESUBMENU = NOMBREMENU.DropDownItems(J)
                    For K = 0 To NOMBRESUBMENU.DropDownItems.Count - 1
                        UspGuardaMenu(NOMBRESUBMENU.DropDownItems(K).Name, NOMBRESUBMENU.DropDownItems(K).Text, NOMBRESUBMENU.Name, 3)
                    Next
                Next
            Next
        End If
        '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''fin guardar menus para perfiles
        SP_Dame_General_sistema_II()
        SP_SERVICIOS_HABILITADOS()

        Me.PolizasToolStripMenuItem.Visible = True
        DesgloceDeMensualidadesToolStripMenuItem.Visible = False
        DesgloceDeContratacionesToolStripMenuItem.Visible = False
        Me.RelaciónDeClientesToolStripMenuItem.Visible = False
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        Me.DameEspecifTableAdapter.Connection = CON
        Me.DameEspecifTableAdapter.Fill(Me.NewsoftvDataSet2.DameEspecif, ColorBut, ColorLetraBut, ColorMenu, ColorMenuLetra, ColorBwr, ColorBwrLetra, ColorGrid, ColorForm, ColorLabel, ColorLetraLabel, ColorLetraForm)
        Me.MUESTRAIMAGENTableAdapter.Connection = CON
        Me.MUESTRAIMAGENTableAdapter.Fill(Me.NewsoftvDataSet2.MUESTRAIMAGEN)
        'TODO: esta línea de código carga datos en la tabla 'NewsoftvDataSet.DameDatosGenerales' Puede moverla o quitarla según sea necesario.
        Me.DameDatosGeneralesTableAdapter.Connection = CON
        Me.DameDatosGeneralesTableAdapter.Fill(Me.NewsoftvDataSet.DameDatosGenerales)

        'CMBLabelNombreSistema.Text = GloEmpresa
        LocNomEmpresa = Me.CMBLabel1.Text
        'LLENA LA TABLA DE MENUS AUTOMATICAMENTE
        Me.RecorrerEstructuraMenu(Me.MenuStrip1)
        If IdSistema = "TO" Then
            Me.CToolStripMenuItem.Visible = False
            Me.VentasToolStripMenuItem2.Visible = False
        ElseIf IdSistema = "SA" Then
            ' If GloDatabaseName = "Jiquilpan" Or GloDatabaseName = "jiquilpan" Then
            Me.ListadoDePagosEfectuadosPorElClienteToolStripMenuItem.Visible = True
            'End If
        ElseIf IdSistema = "LO" Then
            Me.NotasDeCréditoToolStripMenuItem.Visible = True
            Me.ListadoDeNotasDeCréditoToolStripMenuItem.Visible = True
            Me.NotasDeCréditoToolStripMenuItem.Text = "Notas De Crédito"
            Me.ListadoDeNotasDeCréditoToolStripMenuItem.Text = "Listado de Notas de Crédito"
            FacturacionToolStripMenuItem1.Text = "Pagos"
            CancelaciònDeFacturasToolStripMenuItem.Text = "Cancelación de Pagos"
            ReImpresionDeFacturasToolStripMenuItem.Text = "Reimpresion de Pagos"
            CortesDeFacturasToolStripMenuItem.Text = "Cortes de Pagos"
            ListadoDeFacturasCanceadasToolStripMenuItem.Text = "Listado de Pagos Cancelados"
            ListadoDeFacturasReimpresasToolStripMenuItem.Text = "Listado de Pagos Reimpresos"
        End If
        Me.DameTipoUsusarioTableAdapter.Connection = CON
        Me.DameTipoUsusarioTableAdapter.Fill(Me.NewsoftvDataSet.DameTipoUsusario, GloUsuario, GloTipoUsuario)
        CON.Close()
        Select Case SubCiudad
            Case "AG"
                Me.NotasDeCréditoToolStripMenuItem.Visible = True
                Me.ListadoDeNotasDeCréditoToolStripMenuItem.Visible = True
            Case "TV"
                Me.PolizasToolStripMenuItem.Visible = True
                DesgloceDeMensualidadesToolStripMenuItem.Visible = True
                DesgloceDeContratacionesToolStripMenuItem.Visible = True
                Me.RelaciónDeClientesToolStripMenuItem.Visible = True
            Case "VA"
                Me.PolizasToolStripMenuItem.Visible = True
                DesgloceDeMensualidadesToolStripMenuItem.Visible = True
                DesgloceDeContratacionesToolStripMenuItem.Visible = True
                Me.NotasDeCréditoToolStripMenuItem.Visible = True
                Me.NotasDeCréditoToolStripMenuItem.Text = "Devoluciones En Efectivo"
                Me.ListadoDeNotasDeCréditoToolStripMenuItem.Visible = True
                Me.ListadoDeNotasDeCréditoToolStripMenuItem.Text = "Listado de Devoluciones en Efectivo"
            Case "NV"
                Me.PolizasToolStripMenuItem.Visible = True
                DesgloceDeMensualidadesToolStripMenuItem.Visible = True
                DesgloceDeContratacionesToolStripMenuItem.Visible = True
                Me.NotasDeCréditoToolStripMenuItem.Visible = True
                Me.NotasDeCréditoToolStripMenuItem.Text = "Devoluciones En Efectivo"
                Me.ListadoDeNotasDeCréditoToolStripMenuItem.Visible = True
                Me.ListadoDeNotasDeCréditoToolStripMenuItem.Text = "Listado de Devoluciones en Efectivo"
        End Select

        colorea(Me)
        'Me.DamePermisosTableAdapter.Fill(Me.NewsoftvDataSet.DamePermisos, GloTipoUsuario, Me.Text, 2, glolec, gloescr, gloctr)
        bitsist(GloUsuario, 0, GloSistema, "Entrada Sistema", "", "Entrada Sistema", GloNomSucursal, SubCiudad)
        Me.RecorrerEstructuraMenu(Me.MenuStrip1)
        Me.CobroEquivocadoToolStripMenuItem.Visible = False
        Me.ReporteIngresosTarjetasCréditoDébitoToolStripMenuItem.Visible = False
        Me.IngresosDeClientesPorUnMontoToolStripMenuItem.Visible = False
        Me.NúmeroDeBonificacionesToolStripMenuItem.Visible = False
        Me.CobroErroneoToolStripMenuItem.Visible = False

        CToolStripMenuItem.Visible = False
        NotasDeCréditoToolStripMenuItem.Visible = False
        RecepciónDelArchivoDeOxxoToolStripMenuItem.Visible = False
        ListadoDeNotasDeCréditoToolStripMenuItem.Visible = False
        FacturasConCargoAutomaticoToolStripMenuItem.Visible = False

        'If GloTipoUsuario = 40 Then
        '    Me.RelaciónDeIngresosPorConceptosToolStripMenuItem.Visible = True
        'Else
        '    Me.RelaciónDeIngresosPorConceptosToolStripMenuItem.Visible = False
        'End If

        If GloTipoUsuario = 21 Then
            FacturasGlobalesToolStripMenuItem.Visible = False
            ResumenGeneralFacturaGlobalToolStripMenuItem.Visible = False
            ListadoDeEntragasParcialesToolStripMenuItem.Visible = False
            'RelaciónDeIngresosPorConceptosToolStripMenuItem.Visible = False
        End If

        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''inicio Para armar el menu segun el tipo de ususario
        If GloUsuario <> "SISTE" Then
            UspMostrarMenusCliente()
        End If
        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''final Para armar el menu segun el tipo de ususario
        SP_DamedatosUsuario(GloUsuario)
        If GloPermisoNotaCredito = 0 Then
            NotasDeCréditoToolStripMenuItem.Visible = False
        Else
            NotasDeCréditoToolStripMenuItem.Visible = True
        End If
        CMBLabelciudad.Text = GloCiudadEmpresa
    End Sub

    Private Sub CortesDeFacturasToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CortesDeFacturasToolStripMenuItem.Click
        Dim DT As New DataTable
        DT = UspDameClaveMenuFac(sender.name())
        If DT.Rows.Count > 0 Then
            GloClaveMenus = CInt(DT.Rows(0)(0).ToString)
        End If
        locband_pant = 4
        loctitulo = "Seguridad Corte de Facturas"
        FrmSupervisor.Show()
    End Sub

    Private Sub ListadosYAfectacionesToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ListadosYAfectacionesToolStripMenuItem.Click
        Dim DT As New DataTable
        DT = UspDameClaveMenuFac(sender.name())
        If DT.Rows.Count > 0 Then
            GloClaveMenus = CInt(DT.Rows(0)(0).ToString)
        End If
        FrmListadoPreliminar.Show()
    End Sub

    Private Sub VentasToolStripMenuItem2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles VentasToolStripMenuItem2.Click
        Dim DT As New DataTable
        DT = UspDameClaveMenuFac(sender.name())
        If DT.Rows.Count > 0 Then
            GloClaveMenus = CInt(DT.Rows(0)(0).ToString)
        End If
        GloTipo = "V"
        If IdSistema = "LO" Then
            FrmFACLogitel.Text = "Facturación Ventas"
            FrmFACLogitel.Show()
        Else
            FrmFAC.Text = "Facturación Ventas"
            FrmFAC.Show()
        End If


    End Sub

    Private Sub EntregasParcialesToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Dim DT As New DataTable
        DT = UspDameClaveMenuFac(sender.name())
        If DT.Rows.Count > 0 Then
            GloClaveMenus = CInt(DT.Rows(0)(0).ToString)
        End If
        loctitulo = "Seguridad Entregas Parciales"
        locband_pant = 1
        FrmSupervisor.Show()
    End Sub

    Private Sub DesgloseDeMonedaToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Dim DT As New DataTable
        DT = UspDameClaveMenuFac(sender.name())
        If DT.Rows.Count > 0 Then
            GloClaveMenus = CInt(DT.Rows(0)(0).ToString)
        End If
        BwrDESGLOSEMONEDA.Show()
    End Sub

    Private Sub ArqueoDeCajasToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Dim DT As New DataTable
        DT = UspDameClaveMenuFac(sender.name())
        If DT.Rows.Count > 0 Then
            GloClaveMenus = CInt(DT.Rows(0)(0).ToString)
        End If
        loctitulo = "Seguridad Arqueo de Cajas"
        locband_pant = 2
        My.Forms.FrmArqueo.Show()
        'FrmSupervisor.Show()
    End Sub

    Private Sub EntregasParcialesToolStripMenuItem1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles EntregasParcialesToolStripMenuItem1.Click
        Dim DT As New DataTable
        DT = UspDameClaveMenuFac(sender.name())
        If DT.Rows.Count > 0 Then
            GloClaveMenus = CInt(DT.Rows(0)(0).ToString)
        End If
        loctitulo = "Seguridad Entregas Parciales"
        locband_pant = 1
        FrmSupervisor.Show()
    End Sub

    Private Sub DesgloceDeMonedaToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles DesgloceDeMonedaToolStripMenuItem.Click
        Dim DT As New DataTable
        DT = UspDameClaveMenuFac(sender.name())
        If DT.Rows.Count > 0 Then
            GloClaveMenus = CInt(DT.Rows(0)(0).ToString)
        End If
        BwrDESGLOSEMONEDA.Show()
    End Sub



    Private Sub ArqueoDeCajasToolStripMenuItem1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ArqueoDeCajasToolStripMenuItem1.Click
        Dim DT As New DataTable
        DT = UspDameClaveMenuFac(sender.name())
        If DT.Rows.Count > 0 Then
            GloClaveMenus = CInt(DT.Rows(0)(0).ToString)
        End If
        loctitulo = "Seguridad Arqueo de Cajas"
        locband_pant = 2
        FrmSupervisor.Show()
    End Sub

    Private Sub CancelaciònDeFacturasToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CancelaciònDeFacturasToolStripMenuItem.Click
        Dim DT As New DataTable
        DT = UspDameClaveMenuFac(sender.name())
        If DT.Rows.Count > 0 Then
            GloClaveMenus = CInt(DT.Rows(0)(0).ToString)
        End If
        GloOpFacturas = 0 '--Cancelacion
        GloTipo = "C"
        loctitulo = "Seguridad Cancelación de Facturas"
        locband_pant = 5
        FrmSupervisor.Show()
    End Sub

    Private Sub ReImpresionDeFacturasToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ReImpresionDeFacturasToolStripMenuItem.Click
        Dim DT As New DataTable
        DT = UspDameClaveMenuFac(sender.name())
        If DT.Rows.Count > 0 Then
            GloClaveMenus = CInt(DT.Rows(0)(0).ToString)
        End If
        GloOpFacturas = 1 '--ReImpresion
        loctitulo = "Seguridad ReImpresion de Facturas"
        locband_pant = 6
        FrmSupervisor.Show()

    End Sub

    Private Sub FACTURAGLOBALToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles FACTURAGLOBALToolStripMenuItem.Click
        Dim DT As New DataTable
        DT = UspDameClaveMenuFac(sender.name())
        If DT.Rows.Count > 0 Then
            GloClaveMenus = CInt(DT.Rows(0)(0).ToString)
        End If
        BWRFACTURAGLOBAL.Show()

    End Sub

    Private Sub FacturasGlobalesToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles FacturasGlobalesToolStripMenuItem.Click
        Dim DT As New DataTable
        DT = UspDameClaveMenuFac(sender.name())
        If DT.Rows.Count > 0 Then
            GloClaveMenus = CInt(DT.Rows(0)(0).ToString)
        End If
        ResumenFacturaGlobal.Show()
    End Sub

    Private Sub ResumenGeneralFacturaGlobalToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ResumenGeneralFacturaGlobalToolStripMenuItem.Click
        Dim DT As New DataTable
        DT = UspDameClaveMenuFac(sender.name())
        If DT.Rows.Count > 0 Then
            GloClaveMenus = CInt(DT.Rows(0)(0).ToString)
        End If
        FrmResumenFacturaGlobal.Show()
    End Sub

    Private Sub ListadoDeEntragasParcialesToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ListadoDeEntragasParcialesToolStripMenuItem.Click
        Dim DT As New DataTable
        DT = UspDameClaveMenuFac(sender.name())
        If DT.Rows.Count > 0 Then
            GloClaveMenus = CInt(DT.Rows(0)(0).ToString)
        End If
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        Me.DameClv_Session_UsuariosTableAdapter.Connection = CON
        Me.DameClv_Session_UsuariosTableAdapter.Fill(Me.Procedimientos_arnoldo.DameClv_Session_Usuarios, LocClv_session)
        CON.Close()
        My.Forms.SelFecha.Show()
        'My.Forms.FrmListadoEntregasParciales.Show()
    End Sub
    Private Sub RecorrerEstructuraMenu(ByVal oMenu As MenuStrip)
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        Dim menu As ToolStripMenuItem
        For Each oOpcionMenu As ToolStripMenuItem In oMenu.Items
            'Me.DamePermisosTableAdapter.Connection = CON
            'Me.DamePermisosTableAdapter.Fill(Me.DataSetLydia.DamePermisos, GloTipoUsuario, oOpcionMenu.Name, oOpcionMenu.Text, 2, glolec, gloescr, gloctr)
            menu = New ToolStripMenuItem
            menu = oOpcionMenu
            If gloctr = 1 Then
                menu.Enabled = False
            End If
            menu = Nothing
            j = j + 1
            'Me.ALTASMENUSTableAdapter.Connection = CON
            'Me.ALTASMENUSTableAdapter.Fill(Me.NewsoftvDataSet.ALTASMENUS, oOpcionMenu.Text, 2, oOpcionMenu.Name, 10)
            If oOpcionMenu.DropDownItems.Count > 0 Then
                Me.RecorrerSubmenu(oOpcionMenu.DropDownItems, "----")
            End If
        Next
        CON.Close()
    End Sub

    Private Sub RecorrerSubmenu(ByVal oSubmenuItems As ToolStripItemCollection, ByVal sGuiones As String)
        Dim submenu As ToolStripItem
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        For Each oSubitem As ToolStripItem In oSubmenuItems
            If oSubitem.GetType Is GetType(ToolStripMenuItem) Then
                'Me.DamePermisosTableAdapter.Connection = CON
                'Me.DamePermisosTableAdapter.Fill(Me.DataSetLydia.DamePermisos, GloTipoUsuario, oSubitem.Name, oSubitem.Text, 2, glolec, gloescr, gloctr)
                submenu = New ToolStripMenuItem
                submenu = oSubitem
                If gloctr = 1 Then
                    submenu.Enabled = False
                End If
                'Me.ALTASMENUSTableAdapter.Connection = CON
                'Me.ALTASMENUSTableAdapter.Fill(Me.NewsoftvDataSet.ALTASMENUS, oSubitem.Text, 2, oSubitem.Name, j)
                Menu = Nothing
                If CType(oSubitem, ToolStripMenuItem).DropDownItems.Count > 0 Then
                    Me.RecorrerSubmenu(CType(oSubitem, ToolStripMenuItem).DropDownItems, sGuiones & "----")
                End If
            End If
        Next
        CON.Close()
    End Sub

    Private Sub RelaciónDeIngresosPorConceptosToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles RelaciónDeIngresosPorConceptosToolStripMenuItem.Click
        Dim DT As New DataTable
        DT = UspDameClaveMenuFac(sender.name())
        If DT.Rows.Count > 0 Then
            GloClaveMenus = CInt(DT.Rows(0)(0).ToString)
        End If
        locband_pant = 11
        loctitulo = "Relación Ingresos Por Concepto (por Ciudad)"
        GloOpFiltrosXML = "RelacionConceptoCiudad"
        OpcionReporte = 1
        FrmSelPlazaXML.Show()
        'FrmSupervisor.Show()
        'LocbndPolizaCiudad = False
        'LocBndrelingporconceptos = True
        'FrmSelCiudad.Show()
        'FrmFechaIngresosConcepto.Show()
    End Sub

    Private Sub CMBLabel1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CMBLabel1.Click

    End Sub

    Private Sub CobroDeContratosMaestrosToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Dim DT As New DataTable
        DT = UspDameClaveMenuFac(sender.name())
        If DT.Rows.Count > 0 Then
            GloClaveMenus = CInt(DT.Rows(0)(0).ToString)
        End If
        FrmFacContratoMaestro.Show()
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        'Dim DT As New DataTable
        'DT = UspDameClaveMenuFac(sender.name())
        'If DT.Rows.Count > 0 Then
        '    GloClaveMenus = CInt(DT.Rows(0)(0).ToString)
        'End If
        'FrmDetCobroDesc.Show()

    End Sub

    Private Sub ListadoDeBonificacionesToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ListadoDeBonificacionesToolStripMenuItem.Click
        Dim DT As New DataTable
        DT = UspDameClaveMenuFac(sender.name())
        If DT.Rows.Count > 0 Then
            GloClaveMenus = CInt(DT.Rows(0)(0).ToString)
        End If
        'LocBanderaRep1 = 2
        'FrmSelFechas.Show()
        locband_pant = 12
        loctitulo = "Listado de Bonificaciones"
        FrmSupervisor.Show()
    End Sub

    Private Sub ListadoDeFacturasCanceadasToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ListadoDeFacturasCanceadasToolStripMenuItem.Click
        Dim DT As New DataTable
        DT = UspDameClaveMenuFac(sender.name())
        If DT.Rows.Count > 0 Then
            GloClaveMenus = CInt(DT.Rows(0)(0).ToString)
        End If
        'LocBanderaRep1 = 0
        'FrmSelFechas.Show()
        locband_pant = 13
        loctitulo = "Listado de Facturas Canceladas"
        FrmSupervisor.Show()
    End Sub

    Private Sub ListadoDeFacturasReimpresasToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ListadoDeFacturasReimpresasToolStripMenuItem.Click
        Dim DT As New DataTable
        DT = UspDameClaveMenuFac(sender.name())
        If DT.Rows.Count > 0 Then
            GloClaveMenus = CInt(DT.Rows(0)(0).ToString)
        End If
        'LocBanderaRep1 = 1
        'FrmSelFechas.Show()
        locband_pant = 14
        loctitulo = "Listado de Facturas Reimpresas"
        FrmSupervisor.Show()
    End Sub

    Private Sub NotasDeCréditoToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles NotasDeCréditoToolStripMenuItem.Click
        Dim DT As New DataTable
        DT = UspDameClaveMenuFac(sender.name())
        If DT.Rows.Count > 0 Then
            GloClaveMenus = CInt(DT.Rows(0)(0).ToString)
        End If
        If IdSistema = "LO" Then
            BrwNotasdeCreditoLogitel.Show()
        Else
            BrwNotasdeCredito.Show()
        End If

    End Sub

    Private Sub ListadoDeNotasDeCréditoToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ListadoDeNotasDeCréditoToolStripMenuItem.Click
        Dim DT As New DataTable
        DT = UspDameClaveMenuFac(sender.name())
        If DT.Rows.Count > 0 Then
            GloClaveMenus = CInt(DT.Rows(0)(0).ToString)
        End If
        FrmSelOpNotas.Show()
    End Sub

    Private Sub RecepciónDelArchivoDeOxxoToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles RecepciónDelArchivoDeOxxoToolStripMenuItem.Click
        Dim DT As New DataTable
        DT = UspDameClaveMenuFac(sender.name())
        If DT.Rows.Count > 0 Then
            GloClaveMenus = CInt(DT.Rows(0)(0).ToString)
        End If
        FrmRecepOxxo.Show()
    End Sub

    Private Sub RelaciónDeClientesToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles RelaciónDeClientesToolStripMenuItem.Click
        Dim DT As New DataTable
        DT = UspDameClaveMenuFac(sender.name())
        If DT.Rows.Count > 0 Then
            GloClaveMenus = CInt(DT.Rows(0)(0).ToString)
        End If
        LocClientesPagosAdelantados = True
        FrmSelCiudad.Show()
    End Sub

    Private Sub PolizasToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PolizasToolStripMenuItem.Click
        Dim DT As New DataTable
        DT = UspDameClaveMenuFac(sender.name())
        If DT.Rows.Count > 0 Then
            GloClaveMenus = CInt(DT.Rows(0)(0).ToString)
        End If
        BrwPolizas.Show()
    End Sub

    Private Sub DesgloceDeMensualidadesToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles DesgloceDeMensualidadesToolStripMenuItem.Click
        Dim DT As New DataTable
        DT = UspDameClaveMenuFac(sender.name())
        If DT.Rows.Count > 0 Then
            GloClaveMenus = CInt(DT.Rows(0)(0).ToString)
        End If
        GloBnd_Des_Men = True
        'LocClientesPagosAdelantados = True
        FrmSelCiudad.Show()
    End Sub

    Private Sub DesgloceDeContratacionesToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles DesgloceDeContratacionesToolStripMenuItem.Click
        Dim DT As New DataTable
        DT = UspDameClaveMenuFac(sender.name())
        If DT.Rows.Count > 0 Then
            GloClaveMenus = CInt(DT.Rows(0)(0).ToString)
        End If
        GloBnd_Des_Men = True
        GloBnd_Des_Cont = True
        'LocClientesPagosAdelantados = True
        FrmSelCiudad.Show()
    End Sub

    Private Sub FrmMenu_LocationChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.LocationChanged

    End Sub

    Private Sub ConciliaciónBancariaDeSantanderSerfínToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Dim DT As New DataTable
        DT = UspDameClaveMenuFac(sender.name())
        If DT.Rows.Count > 0 Then
            GloClaveMenus = CInt(DT.Rows(0)(0).ToString)
        End If
        FrmConciliacionBancarioSantander.Show()
    End Sub

    Private Sub FacturasConCargoAutomaticoToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles FacturasConCargoAutomaticoToolStripMenuItem.Click
        Dim DT As New DataTable
        DT = UspDameClaveMenuFac(sender.name())
        If DT.Rows.Count > 0 Then
            GloClaveMenus = CInt(DT.Rows(0)(0).ToString)
        End If
        Dim con5 As New SqlClient.SqlConnection(MiConexion)
        Dim cmd As New SqlClient.SqlCommand()
        con5.Open()
        With cmd
            .CommandText = "Dame_clv_session_cargos_auto"
            .CommandTimeout = 0
            .Connection = con5
            .CommandType = CommandType.StoredProcedure

            Dim prm As New SqlParameter("@clv_session", SqlDbType.BigInt)
            prm.Direction = ParameterDirection.Output
            prm.Value = 0

            .Parameters.Add(prm)

            Dim i As Integer = cmd.ExecuteNonQuery()

            locclv_sessioncargosauto = prm.Value
        End With
        con5.Close()
        FrmSelTipCargoAuto.Show()

    End Sub

    Private Sub ChecaDesgloseMoneda(ByVal Clv_Usuario As String)
        Dim conexion As New SqlConnection(MiConexion)
        Dim comando As New SqlCommand("ChecaDesgloseMoneda", conexion)
        comando.CommandType = CommandType.StoredProcedure

        Dim parametro As New SqlParameter("Clv_Usuario", SqlDbType.VarChar, 10)
        parametro.Direction = ParameterDirection.Input
        parametro.Value = Clv_Usuario
        comando.Parameters.Add(parametro)

        Dim parametro1 As New SqlParameter("@Tipo", SqlDbType.VarChar, 50)
        parametro1.Direction = ParameterDirection.Output
        parametro1.Value = String.Empty
        comando.Parameters.Add(parametro1)

        Dim parametro2 As New SqlParameter("@Res", SqlDbType.Int)
        parametro2.Direction = ParameterDirection.Output
        parametro2.Value = 0
        comando.Parameters.Add(parametro2)

        Dim parametro3 As New SqlParameter("@Msg", SqlDbType.VarChar, 150)
        parametro3.Direction = ParameterDirection.Output
        parametro3.Value = String.Empty
        comando.Parameters.Add(parametro3)

        Try
            conexion.Open()
            comando.ExecuteNonQuery()
            eRes = CInt(parametro2.Value.ToString)
            eMsg = parametro3.Value.ToString
            conexion.Close()

            If eRes = 1 Then
                MsgBox(eMsg, MsgBoxStyle.Information)
                op = "N"
                My.Forms.FrmDESGLOSEMONEDA.Show()

            End If

        Catch ex As Exception
            conexion.Close()
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        End Try

    End Sub

    Private Sub FacturacionToolStripMenuItem1_DropDownOpened(ByVal sender As Object, ByVal e As System.EventArgs) Handles FacturacionToolStripMenuItem1.DropDownOpened
        If IdSistema = "LO" Then
            ChecaDesgloseMoneda(GloUsuario)
            'ESTA VARIABLE LA APAGO, PORQUE SE UTULIZA EN EL DESGLOSE DE MONEDA,
            'ME ESTABA CREANDO CONFLICTOS EN FRMFACLOGITEL
            GloBnd = False
        End If
    End Sub

    Private Sub ListadoDePagosEfectuadosPorElClienteToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ListadoDePagosEfectuadosPorElClienteToolStripMenuItem.Click
        Dim DT As New DataTable
        DT = UspDameClaveMenuFac(sender.name())
        If DT.Rows.Count > 0 Then
            GloClaveMenus = CInt(DT.Rows(0)(0).ToString)
        End If
        FrmRangoContratos.Show()
    End Sub

    Private Sub ReporteGlobalToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ReporteGlobalToolStripMenuItem.Click
        Dim DT As New DataTable
        DT = UspDameClaveMenuFac(sender.name())
        If DT.Rows.Count > 0 Then
            GloClaveMenus = CInt(DT.Rows(0)(0).ToString)
        End If
        FrmSelFechas2.Show()
    End Sub

    Private Sub ReporteIngresosTarjetasCréditoDébitoToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ReporteIngresosTarjetasCréditoDébitoToolStripMenuItem.Click
        Dim DT As New DataTable
        DT = UspDameClaveMenuFac(sender.name())
        If DT.Rows.Count > 0 Then
            GloClaveMenus = CInt(DT.Rows(0)(0).ToString)
        End If
        rBndTarjetas = True
        FrmSelFechaGral.Show()
    End Sub

    Private Sub MensualidadesAdelantadasToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Dim DT As New DataTable
        DT = UspDameClaveMenuFac(sender.name())
        If DT.Rows.Count > 0 Then
            GloClaveMenus = CInt(DT.Rows(0)(0).ToString)
        End If
        eBndMensAde = True
        LiTipo = 0
        FrmSelFechas2.Show()
    End Sub

    Private Sub IngresosDeClientesPorUnMontoToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles IngresosDeClientesPorUnMontoToolStripMenuItem.Click
        Dim DT As New DataTable
        DT = UspDameClaveMenuFac(sender.name())
        If DT.Rows.Count > 0 Then
            GloClaveMenus = CInt(DT.Rows(0)(0).ToString)
        End If
        eOpRep = 1
        FrmSelFechas3.Show()
    End Sub

    Private Sub NúmeroDeBonificacionesToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles NúmeroDeBonificacionesToolStripMenuItem.Click
        Dim DT As New DataTable
        DT = UspDameClaveMenuFac(sender.name())
        If DT.Rows.Count > 0 Then
            GloClaveMenus = CInt(DT.Rows(0)(0).ToString)
        End If
        eOpRep = 2
        FrmSelFechas3.Show()
    End Sub

    Private Sub CancelaciónDeFacturasRedEfectivaToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Dim DT As New DataTable
        DT = UspDameClaveMenuFac(sender.name())
        'Prueba
        If DT.Rows.Count > 0 Then
            GloClaveMenus = CInt(DT.Rows(0)(0).ToString)
        End If
        locband_pant = 10
        FrmSupervisor.Show()
    End Sub

    Private Sub CobroEquivocadoToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CobroEquivocadoToolStripMenuItem.Click
        Dim DT As New DataTable
        DT = UspDameClaveMenuFac(sender.name())
        If DT.Rows.Count > 0 Then
            GloClaveMenus = CInt(DT.Rows(0)(0).ToString)
        End If
        BrwFacturas.Show()
    End Sub

    Private Sub CobroErroneoToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CobroErroneoToolStripMenuItem.Click
        Dim DT As New DataTable
        DT = UspDameClaveMenuFac(sender.name())
        If DT.Rows.Count > 0 Then
            GloClaveMenus = CInt(DT.Rows(0)(0).ToString)
        End If
        FrmRepCobroErroneo.Show()
    End Sub

    Private Sub PagosDiferidosToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Dim DT As New DataTable
        DT = UspDameClaveMenuFac(sender.name())
        If DT.Rows.Count > 0 Then
            GloClaveMenus = CInt(DT.Rows(0)(0).ToString)
        End If
        FrmRepPagosDifFac.Show()
    End Sub

    Private Sub PromocionesToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Dim DT As New DataTable
        DT = UspDameClaveMenuFac(sender.name())
        If DT.Rows.Count > 0 Then
            GloClaveMenus = CInt(DT.Rows(0)(0).ToString)
        End If
        FrmRepPromocion.Show()
    End Sub

    Private Sub EstadosDeCuentaToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Dim DT As New DataTable
        DT = UspDameClaveMenuFac(sender.name())
        If DT.Rows.Count > 0 Then
            GloClaveMenus = CInt(DT.Rows(0)(0).ToString)
        End If
        BrwEstadoCuenta.Show()
    End Sub

    Private Sub GeneraciónDeFacturasFiscalesFacturaNetToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Dim DT As New DataTable
        DT = UspDameClaveMenuFac(sender.name())
        If DT.Rows.Count > 0 Then
            GloClaveMenus = CInt(DT.Rows(0)(0).ToString)
        End If
        FrmGeneracionFF.Show()
    End Sub

    Private Sub FacturacionToolStripMenuItem1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles FacturacionToolStripMenuItem1.Click

    End Sub

    Private Sub ListadoDeCobrosParcialesDeMaterialporConvenioToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ListadoDeCobrosParcialesDeMaterialporConvenioToolStripMenuItem.Click
        Dim DT As New DataTable
        DT = UspDameClaveMenuFac(sender.name())
        If DT.Rows.Count > 0 Then
            GloClaveMenus = CInt(DT.Rows(0)(0).ToString)
        End If
        'If GloTipoUsuario = 40 Or GloTipoUsuario = 1 Then
        '    FrmFechasCobroMaterial.Show()
        'Else
        '    MsgBox("No tiene Permisos para Ingresar en este Módulo")
        '    Exit Sub
        'End If
        locband_pant = 15
        loctitulo = "Cobros Parciales de Material"
        FrmSupervisor.Show()
    End Sub

    '''''''''''''''''PARA PERFILES LLENA TABLA MENU...............INICIO
    Private Sub UspMostrarMenusCliente()
        Try
            Dim NOMBREMENU As ToolStripMenuItem
            Dim NOMBRESUBMENU As ToolStripMenuItem

            Dim I, J, K As Integer
            For I = 0 To MenuStrip1.Items.Count - 1
                Me.MenuStrip1.Items(I).Visible = UspValidaMenu(Me.MenuStrip1.Items(I).Name)
                NOMBREMENU = Me.MenuStrip1.Items(I)
                For J = 0 To NOMBREMENU.DropDownItems.Count - 1
                    NOMBREMENU.DropDownItems(J).Visible = UspValidaMenu(NOMBREMENU.DropDownItems(J).Name)
                    NOMBRESUBMENU = NOMBREMENU.DropDownItems(J)
                    For K = 0 To NOMBRESUBMENU.DropDownItems.Count - 1
                        NOMBRESUBMENU.DropDownItems(K).Visible = UspValidaMenu(NOMBRESUBMENU.DropDownItems(K).Name)
                    Next
                Next
            Next

        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    '''''''''''''''''PARA PERFILES LLENA TABLA MENU...............FIN

    Private Sub PerfilesSISTEToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PerfilesSISTEToolStripMenuItem.Click
        FrmPerfilesSiste.Show()

    End Sub

    Private Sub PerfilesToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PerfilesToolStripMenuItem.Click
        FrmPerfilesUsuarios.Show()

    End Sub
    Private Sub uspChecaSiTieneCorteFinal(ByVal prmCajera As String) '''''VERIFICAMOS SI EL CAJERO YA TIENE EL CORTE FINAL GENERADO PARA RESTRINGIR ACCESOS
        '''''LIMPIAMOS LOS PARÁMETROS (INICIO)
        ControlEfectivoClass.limpiaParametros()
        '''''LIMPIAMOS LOS PARÁMETROS (FIN)

        '''''MANDAMOS LOS PARÁMETROS DEL PROCEDIMIENTO (INICIO) 
        ControlEfectivoClass.CreateMyParameter("@cajera", SqlDbType.VarChar, prmCajera, 5)
        ControlEfectivoClass.CreateMyParameter("@bndCorte", ParameterDirection.Output, SqlDbType.Int)
        '''''MANDAMOS LOS PARÁMETROS DEL PROCEDIMIENTO (FIN)

        '''''DECLARAMOS EL DICCIONARIO DONDE VAMOS A LLENAR LOS PARÁMETROS DE SALIDA (INICIO)
        Dim DicoParametros As New Dictionary(Of String, Object)
        '''''DECLARAMOS EL DICCIONARIO DONDE VAMOS A LLENAR LOS PARÁMETROS DE SALIDA (FIN)

        '''''LLENAMOS EL DICCIONARIO MANDANDO EJECUTAR EL PROCEDIMIENTO (INICIO)
        DicoParametros = ControlEfectivoClass.ProcedimientoOutPut("uspChecaSiTieneCorteFinal")
        '''''LLENAMOS EL DICCIONARIO MANDANDO EJECUTAR EL PROCEDIMIENTO (FIN)

        '''''LE ASIGNAMOS EL VALOR A LA VARIABLE QUE DEFINIRÁ SI TIENE O NO CORTE GENERADO (INICIO)
        prmCorteFinal = CInt(DicoParametros("@bndCorte").ToString)
        '''''LE ASIGNAMOS EL VALOR A LA VARIABLE QUE DEFINIRÁ SI TIENE O NO CORTE GENERADO (FIN)
    End Sub
    Private Sub uspChecaSiTieneDesglose(ByVal prmFecha As Date, ByVal prmCajera As String, ByVal prmOp As Integer) '''''VERIFICAMOS SI EL CAJERO YA TIENE DESGLOSE GENERADO
        '''''LIMPIAMOS LOS PARÁMETROS (INICIO)
        ControlEfectivoClass.limpiaParametros()
        '''''LIMPIAMOS LOS PARÁMETROS (FIN)

        '''''MANDAMOS LOS PARÁMETROS DEL PROCEDIMIENTO (INICIO) 
        ControlEfectivoClass.CreateMyParameter("@Fecha", SqlDbType.DateTime, prmFecha)
        ControlEfectivoClass.CreateMyParameter("@NomCajera", SqlDbType.VarChar, prmCajera, 5)
        ControlEfectivoClass.CreateMyParameter("@Op", SqlDbType.Int, prmOp)
        ControlEfectivoClass.CreateMyParameter("@bndDesglose", ParameterDirection.Output, SqlDbType.Int)
        '''''MANDAMOS LOS PARÁMETROS DEL PROCEDIMIENTO (FIN)

        '''''DECLARAMOS EL DICCIONARIO DONDE VAMOS A LLENAR LOS PARÁMETROS DE SALIDA (INICIO)
        Dim DicoParametros As New Dictionary(Of String, Object)
        '''''DECLARAMOS EL DICCIONARIO DONDE VAMOS A LLENAR LOS PARÁMETROS DE SALIDA (FIN)

        '''''LLENAMOS EL DICCIONARIO MANDANDO EJECUTAR EL PROCEDIMIENTO (INICIO)
        DicoParametros = ControlEfectivoClass.ProcedimientoOutPut("uspChecaSiTieneDesglose")
        '''''LLENAMOS EL DICCIONARIO MANDANDO EJECUTAR EL PROCEDIMIENTO (FIN)

        '''''LE ASIGNAMOS EL VALOR A LA VARIABLE QUE DEFINIRÁ SI TIENE O NO CORTE GENERADO (INICIO)
        prmDesglose = CInt(DicoParametros("@bndDesglose").ToString)
        '''''LE ASIGNAMOS EL VALOR A LA VARIABLE QUE DEFINIRÁ SI TIENE O NO CORTE GENERADO (FIN)
    End Sub

    Private Sub SP_DamedatosUsuario(ByVal oClv_Usuario As String) '''''VERIFICAMOS SI EL CAJERO YA TIENE DESGLOSE GENERADO
        '''''LIMPIAMOS LOS PARÁMETROS (INICIO)
        BaseII.limpiaParametros()
        '''''LIMPIAMOS LOS PARÁMETROS (FIN)

        '''''MANDAMOS LOS PARÁMETROS DEL PROCEDIMIENTO (INICIO) 
        BaseII.CreateMyParameter("@Clv_Usuario", SqlDbType.VarChar, oClv_Usuario, 50)
        BaseII.CreateMyParameter("@Nombre", ParameterDirection.Output, SqlDbType.VarChar, 250)
        '''''DECLARAMOS EL DICCIONARIO DONDE VAMOS A LLENAR LOS PARÁMETROS DE SALIDA (INICIO)
        Dim DicoParametros As New Dictionary(Of String, Object)
        '''''DECLARAMOS EL DICCIONARIO DONDE VAMOS A LLENAR LOS PARÁMETROS DE SALIDA (FIN)

        '''''LLENAMOS EL DICCIONARIO MANDANDO EJECUTAR EL PROCEDIMIENTO (INICIO)
        DicoParametros = BaseII.ProcedimientoOutPut("SP_DamedatosUsuario")
        '''''LLENAMOS EL DICCIONARIO MANDANDO EJECUTAR EL PROCEDIMIENTO (FIN)

        '''''LE ASIGNAMOS EL VALOR A LA VARIABLE QUE DEFINIRÁ SI TIENE O NO CORTE GENERADO (INICIO)
        LabelNombreUsuario.Text = DicoParametros("@Nombre").ToString
        '''''LE ASIGNAMOS EL VALOR A LA VARIABLE QUE DEFINIRÁ SI TIENE O NO CORTE GENERADO (FIN)
    End Sub

    Private Sub RegistroDeGastosToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles RegistroDeGastosToolStripMenuItem.Click
        '''''CHECAMOS QUE NO TENGA GENERADO SU CIRTE DEL DÍA; DE LO CONTRARIO NEGAMOS EL ACCESO (INICIO)
        uspChecaSiTieneCorteFinal(GloUsuario)
        If prmCorteFinal > 0 Then
            MsgBox("No puede accesar a este módulo por que ya tiene Generado el Corte Final del Día", MsgBoxStyle.Information)
            Exit Sub
        End If
        '''''CHECAMOS QUE NO TENGA GENERADO SU CIRTE DEL DÍA; DE LO CONTRARIO NEGAMOS EL ACCESO (FIN)

        '''''CHECAMOS QUE NO TENGA GENERADO DESGLOSE DEL DÍA; DE LO CONTRARIO NEGAMOS EL ACCESO (INICIO)
        uspChecaSiTieneDesglose("1900-01-01", GloUsuario, 2)
        If prmDesglose > 0 Then
            MsgBox("No puede accesar a este módulo por que ya tiene Generado un Desglose del Día; proceda a Eliminar el Desglose para poder continuar", MsgBoxStyle.Information)
            Exit Sub
        End If
        '''''CHECAMOS QUE NO TENGA GENERADO DESGLOSE DEL DÍA; DE LO CONTRARIO NEGAMOS EL ACCESO (FIN)

        'If GloTipoUsuario = 40 Or GloTipoUsuario = 2 Then
        BrwGastos.Show()
        'Else
        '    MsgBox("No tiene Permisos para Ingresar en este Módulo")
        '    Exit Sub
        'End If
    End Sub

    Private Sub EntregasGlobalesToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles EntregasGlobalesToolStripMenuItem.Click
        '''''CHECAMOS QUE NO TENGA GENERADO SU CIRTE DEL DÍA; DE LO CONTRARIO NEGAMOS EL ACCESO (INICIO)
        uspChecaSiTieneCorteFinal(GloUsuario)
        If prmCorteFinal > 0 Then
            MsgBox("No puede accesar a este módulo por que ya tiene Generado el Corte Final del Día", MsgBoxStyle.Information)
            Exit Sub
        End If
        '''''CHECAMOS QUE NO TENGA GENERADO SU CIRTE DEL DÍA; DE LO CONTRARIO NEGAMOS EL ACCESO (FIN)

        '''''CHECAMOS QUE NO TENGA GENERADO DESGLOSE DEL DÍA; DE LO CONTRARIO NEGAMOS EL ACCESO (INICIO)
        uspChecaSiTieneDesglose("1900-01-01", GloUsuario, 2)
        If prmDesglose > 0 Then
            MsgBox("No puede accesar a este módulo por que ya tiene Generado un Desglose del Día; proceda a Eliminar el Desglose para poder continuar", MsgBoxStyle.Information)
            Exit Sub
        End If
        '''''CHECAMOS QUE NO TENGA GENERADO DESGLOSE DEL DÍA; DE LO CONTRARIO NEGAMOS EL ACCESO (FIN)

        'If GloTipoUsuario = 40 Or GloTipoUsuario = 2 Then
        BrwEntregasGlobales.Show()
        'Else
        '    MsgBox("No tiene Permisos para Ingresar en este Módulo")
        '    Exit Sub
        'End If
    End Sub

    Private Sub ControlDeEfectivoToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ControlDeEfectivoToolStripMenuItem.Click
        'If GloTipoUsuario = 40 Or GloTipoUsuario = 1 Then
        BrwCancelacionArqueos.Show()
        'Else
        'MsgBox("No tiene Permisos para Ingresar en este Módulo")
        'Exit Sub
        'End If
    End Sub

    Private Sub AjustePorDiferenciasToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles AjustePorDiferenciasToolStripMenuItem.Click
        'If GloTipoUsuario = 40 Then
        FrmDiferenciasvb.Show()
        'Else
        '    MsgBox("No tiene Permisos para Ingresar en este Módulo")
        '    Exit Sub
        'End If
    End Sub

    Private Sub ControlDeEfectivoToolStripMenuItem1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ControlDeEfectivoToolStripMenuItem1.Click
        'If GloTipoUsuario = 40 Or GloTipoUsuario = 1 Then
        FrmFiltroEfectivo.Show()
        'Else
        '    MsgBox("No tiene Permisos para Ingresar en este Módulo")
        '    Exit Sub
        'End If
    End Sub

    Private Sub ListadoDeGastosToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ListadoDeGastosToolStripMenuItem.Click
        'If GloTipoUsuario = 40 Or GloTipoUsuario = 1 Then
        FrmFiltroGastos.Show()
        'Else
        '    MsgBox("No tiene Permisos para Ingresar en este Módulo")
        '    Exit Sub
        'End If
    End Sub

    Private Sub ListadoDeEntregasGlobalesToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ListadoDeEntregasGlobalesToolStripMenuItem.Click
        'If GloTipoUsuario = 40 Or GloTipoUsuario = 1 Then
        FrmFiltroEntregasGlobales.Show()
        'Else
        '    MsgBox("No tiene Permisos para Ingresar en este Módulo")
        '    Exit Sub
        'End If
    End Sub

    Private Sub RelaciónDeIngresosPorConceptosporSucursalToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles RelaciónDeIngresosPorConceptosporSucursalToolStripMenuItem.Click
        Dim DT As New DataTable
        DT = UspDameClaveMenuFac(sender.name())
        If DT.Rows.Count > 0 Then
            GloClaveMenus = CInt(DT.Rows(0)(0).ToString)
        End If
        locband_pant = 16
        OpcionReporte = 2
        loctitulo = "Relación Ingresos Por Concepto (por Sucursal)"
        GloOpFiltrosXML = "RelacionConceptoSucursal"
        FrmSelPlazaXML.Show()
    End Sub

    Private Sub EstadosDeCuentaToolStripMenuItem1_Click(sender As System.Object, e As System.EventArgs) Handles EstadosDeCuentaToolStripMenuItem1.Click
        FrmEstadosCuenta.Show()
    End Sub

    Private Sub RelaciónDeIngresosPorConceptosDistribuidorPlazaToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles RelaciónDeIngresosPorConceptosDistribuidorPlazaToolStripMenuItem.Click
        LocbndPolizaCiudad = False
        LocBndrelingporconceptosCiudad = True
        LocBndrelingporconceptosSucursal = False
        varfrmselcompania = "relingresonuevo"
        GloOpFiltrosXML = "IngresoNormal"
        OpcionReporte = 3
        FrmSelPlazaXML.Show()
        'FrmSelCiudadJ.Show() 'asdsad
    End Sub

    Private Sub BonificacionesToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles BonificacionesToolStripMenuItem.Click
        locband_pant = 12
        loctitulo = "Listado de Bonificaciones"
        BonificacionNuevo = True
        LocBanderaRep1 = 2
        varfrmselcompania = "BonificacionNuevo"
        EntradasSelCiudad = 0
        'FrmSelFechas.Show()
        'FrmSelCiudadJ.Show()
        GloOpFiltrosXML = "Bonificaciones"
        FrmSelPlazaXML.Show()
    End Sub

    Private Sub FacturasDeAlmacénToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles FacturasDeAlmacénToolStripMenuItem.Click
        BrwFacturasAlmacen.Show()
    End Sub

    Private Sub ReporteNuevoToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles ReporteNuevoToolStripMenuItem.Click
        LiTipo = 8
        varfrmselcompania = "CorteNuevo"
        GloOpFiltrosXML = "CorteNuevo"
        FrmSelPlazaXML.Show()
        EntradasSelCiudad = 0
        'FrmSelCiudadJ.Show()
    End Sub

    Private Sub ConciliaciónBancariaToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles ConciliaciónBancariaToolStripMenuItem.Click
        FrmConciliacionBancarioSantander.Show()
    End Sub

    Private Sub DetalleDePagosPrimeraVersiónToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles DetalleDePagosPrimeraVersiónToolStripMenuItem.Click
        LiTipo = 8
        varfrmselcompania = "CorteNuevoPrimeraVersion"
        GloOpFiltrosXML = "CortePrimeraVersion"
        FrmSelPlazaXML.Show()
        'EntradasSelCiudad = 0
        'FrmSelCiudadJ.Show()
    End Sub

    Private Sub ListadoDeNotasDeCréditoToolStripMenuItem1_Click(sender As Object, e As EventArgs) Handles ListadoDeNotasDeCréditoToolStripMenuItem1.Click
        Dim DT As New DataTable
        DT = UspDameClaveMenuFac(sender.name())
        If DT.Rows.Count > 0 Then
            GloClaveMenus = CInt(DT.Rows(0)(0).ToString)
        End If
        'FrmSelOpNotas.Show()
        GloOpFiltrosXML = "listadoNotaCreditoHastaDistribuidor"
        FrmSelPlazaXML.Show()
    End Sub

    Private Sub RefacturacionToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles RefacturacionToolStripMenuItem.Click
        Dim DT As New DataTable
        DT = UspDameClaveMenuFac(sender.name())
        If DT.Rows.Count > 0 Then
            GloClaveMenus = CInt(DT.Rows(0)(0).ToString)
        End If
        GloOpFacturas = 99 '--Refacturacion
        loctitulo = "Seguridad Refacturación"
        locband_pant = 6
        FrmSupervisor.Show()
    End Sub

    Private Sub FacturaGlobalSucursalesEspecialesToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles FacturaGlobalSucursalesEspecialesToolStripMenuItem.Click
        Dim DT As New DataTable
        DT = UspDameClaveMenuFac(sender.name())
        If DT.Rows.Count > 0 Then
            GloClaveMenus = CInt(DT.Rows(0)(0).ToString)
        End If
        BRWFACTURAGLOBALESPCIALES.Show()
        'BRWFACTURAGLOBALESPCIALES
    End Sub

    Private Sub ConciliaciónCLABEToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles ConciliaciónCLABEToolStripMenuItem.Click
        FrmConciliacionClabe.Show()
    End Sub

    Private Sub PolizasSucursalesEspecialesToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles PolizasSucursalesEspecialesToolStripMenuItem.Click
        BrwPolizasEspeciales.Show() 'Jano
    End Sub

    Private Sub FacturaGlobalSucursalesEspecialesToolStripMenuItem1_Click(sender As Object, e As EventArgs) Handles FacturaGlobalSucursalesEspecialesToolStripMenuItem1.Click
        Dim DT As New DataTable
        DT = UspDameClaveMenuFac(sender.name())
        If DT.Rows.Count > 0 Then
            GloClaveMenus = CInt(DT.Rows(0)(0).ToString)
        End If
        BRWFACTURAGLOBALESPCIALES.Show()
        'BRWFACTURAGLOBALESPCIALES
    End Sub

    Private Sub RelacionSucursalesEspecialesMenuItem_Click(sender As Object, e As EventArgs) Handles RelacionSucursalesEspecialesMenuItem.Click
        Dim DT As New DataTable
        DT = UspDameClaveMenuFac(sender.name())
        If DT.Rows.Count > 0 Then
            GloClaveMenus = CInt(DT.Rows(0)(0).ToString)
        End If
        locband_pant = 11
        loctitulo = "Relación Ingresos Por Concepto (por Ciudad)"
        varfrmselcompania = "ConceptosSucursalesEspeciales"
        GloOpFiltrosXML = "RelacionConceptoEspeciales"
        FrmSelSucursalEspecialXML.Show()
    End Sub

    Private Sub CortesEspecialesMenuItem1_Click(sender As Object, e As EventArgs) Handles CortesEspecialesMenuItem1.Click
        Dim DT As New DataTable
        DT = UspDameClaveMenuFac(sender.name())
        If DT.Rows.Count > 0 Then
            GloClaveMenus = CInt(DT.Rows(0)(0).ToString)
        End If
        FrmCortesEspeciales.Show()
    End Sub

    Private Sub CancelaciónYReimpresiónDeSucursalesEspecialesToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles CancelaciónYReimpresiónDeSucursalesEspecialesToolStripMenuItem.Click
        Dim DT As New DataTable
        DT = UspDameClaveMenuFac(sender.name())
        If DT.Rows.Count > 0 Then
            GloClaveMenus = CInt(DT.Rows(0)(0).ToString)
        End If
        bndRefacturacionEspeciales = 0
        BrwFacturas_CancelarEspeciales.Show()
    End Sub

    Private Sub CajasWebToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles CajasWebToolStripMenuItem.Click
        Dim MiToken As String = ""
        Token1 = ""
        MiToken = GenerarTokenYDatosUsuario(GloClvUsuario, GloUsuario, GloSucursal, GloHostName)
        Token1 = MiToken
        Dim MiUrl As String = "http://192.168.50.33:4000/SoftvApp/#!/auth/?token=" + MiToken.ToString
        System.Diagnostics.Process.Start(MiUrl.ToString)
        'Catch
        '    'Code to handle the error.
        'End Try
    End Sub

    Private Sub Timer1_Tick(sender As Object, e As EventArgs) Handles Timer1.Tick
        Try
            If SP_CHECARSIGRABOPAGO(Token1) = 1 Then
                Timer1.Enabled = False
                GenerarReportesProcesos(Token1)
                Timer1.Enabled = True
            End If
            If SP_CHECARSIGRABOPAGOOp() = 1 Then
                Timer1.Enabled = False
                GenerarOpcionesProcesos()
                Timer1.Enabled = True
            End If
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub


    Private Sub DetalleDePagosSucursalesEspecialesToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles DetalleDePagosSucursalesEspecialesToolStripMenuItem.Click
        Dim DT As New DataTable
        DT = UspDameClaveMenuFac(sender.name())
        If DT.Rows.Count > 0 Then
            GloClaveMenus = CInt(DT.Rows(0)(0).ToString)
        End If
        GloOpFiltrosXML = "DetalleEspeciales"
        FrmSelSucursalEspecialXML.Show()
    End Sub

    Private Sub ListadoDeNotasDeCréditoDeSucursalesEspecialesToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles ListadoDeNotasDeCréditoDeSucursalesEspecialesToolStripMenuItem.Click
        Dim DT As New DataTable
        DT = UspDameClaveMenuFac(sender.name())
        If DT.Rows.Count > 0 Then
            GloClaveMenus = CInt(DT.Rows(0)(0).ToString)
        End If
        GloOpFiltrosXML = "ListadoNotasEspeciales"
        FrmSelSucursalEspecialXML.Show()
    End Sub

    Private Sub ConciliaciónSucursalesEspecialesToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles ConciliaciónSucursalesEspecialesToolStripMenuItem.Click
        Dim DT As New DataTable
        DT = UspDameClaveMenuFac(sender.name())
        If DT.Rows.Count > 0 Then
            GloClaveMenus = CInt(DT.Rows(0)(0).ToString)
        End If
        FrmConciliacionDatalogic.Show()
    End Sub

    Private Sub RefacturaciónSucursalesEspecialesToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles RefacturaciónSucursalesEspecialesToolStripMenuItem.Click
        Dim DT As New DataTable
        DT = UspDameClaveMenuFac(sender.name())
        If DT.Rows.Count > 0 Then
            GloClaveMenus = CInt(DT.Rows(0)(0).ToString)
        End If
        bndRefacturacionEspeciales = 1
        BrwFacturas_CancelarEspeciales.Show()
    End Sub

    Private Sub EstadosDeCuentaInternetYComboToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles EstadosDeCuentaInternetYComboToolStripMenuItem.Click
        GloContrato = 0
        FrmEstadoCuentaSaldo.Show()
    End Sub

    Private Sub CancelaciónToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles CancelaciónToolStripMenuItem.Click

    End Sub
End Class
