﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmEntreCalles
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Label50 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.cbOeste = New System.Windows.Forms.ComboBox()
        Me.cbEste = New System.Windows.Forms.ComboBox()
        Me.cbSur = New System.Windows.Forms.ComboBox()
        Me.cbNorte = New System.Windows.Forms.ComboBox()
        Me.tbReferencia = New System.Windows.Forms.TextBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.RadioOeste = New System.Windows.Forms.RadioButton()
        Me.RadioEste = New System.Windows.Forms.RadioButton()
        Me.RadioSur = New System.Windows.Forms.RadioButton()
        Me.RadioNorte = New System.Windows.Forms.RadioButton()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.LbNorte = New System.Windows.Forms.Label()
        Me.LbSur = New System.Windows.Forms.Label()
        Me.LbEste = New System.Windows.Forms.Label()
        Me.LbOeste = New System.Windows.Forms.Label()
        Me.Button5 = New System.Windows.Forms.Button()
        Me.PictureBox1 = New System.Windows.Forms.PictureBox()
        Me.PictureBox2 = New System.Windows.Forms.PictureBox()
        Me.Button2 = New System.Windows.Forms.Button()
        Me.tbLatDecimal = New System.Windows.Forms.TextBox()
        Me.tbLongDecimal = New System.Windows.Forms.TextBox()
        Me.GroupBoxCoordenadas = New System.Windows.Forms.GroupBox()
        Me.Button3 = New System.Windows.Forms.Button()
        Me.GroupBox1.SuspendLayout()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBoxCoordenadas.SuspendLayout()
        Me.SuspendLayout()
        '
        'Label50
        '
        Me.Label50.AutoSize = True
        Me.Label50.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label50.ForeColor = System.Drawing.Color.LightSlateGray
        Me.Label50.Location = New System.Drawing.Point(10, 18)
        Me.Label50.Name = "Label50"
        Me.Label50.Size = New System.Drawing.Size(50, 15)
        Me.Label50.TabIndex = 2
        Me.Label50.Text = "Norte: "
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.ForeColor = System.Drawing.Color.LightSlateGray
        Me.Label1.Location = New System.Drawing.Point(10, 70)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(37, 15)
        Me.Label1.TabIndex = 36
        Me.Label1.Text = "Sur: "
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.ForeColor = System.Drawing.Color.LightSlateGray
        Me.Label2.Location = New System.Drawing.Point(10, 120)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(43, 15)
        Me.Label2.TabIndex = 38
        Me.Label2.Text = "Este: "
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.ForeColor = System.Drawing.Color.LightSlateGray
        Me.Label3.Location = New System.Drawing.Point(10, 171)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(52, 15)
        Me.Label3.TabIndex = 40
        Me.Label3.Text = "Oeste: "
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.ForeColor = System.Drawing.Color.LightSlateGray
        Me.Label4.Location = New System.Drawing.Point(264, 21)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(47, 15)
        Me.Label4.TabIndex = 42
        Me.Label4.Text = "Casa: "
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label9.ForeColor = System.Drawing.Color.LightSlateGray
        Me.Label9.Location = New System.Drawing.Point(15, 23)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(59, 15)
        Me.Label9.TabIndex = 2
        Me.Label9.Text = "Latitud: "
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label8.ForeColor = System.Drawing.Color.LightSlateGray
        Me.Label8.Location = New System.Drawing.Point(13, 58)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(71, 15)
        Me.Label8.TabIndex = 36
        Me.Label8.Text = "Longitud: "
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.cbOeste)
        Me.GroupBox1.Controls.Add(Me.cbEste)
        Me.GroupBox1.Controls.Add(Me.cbSur)
        Me.GroupBox1.Controls.Add(Me.cbNorte)
        Me.GroupBox1.Controls.Add(Me.tbReferencia)
        Me.GroupBox1.Controls.Add(Me.Label5)
        Me.GroupBox1.Controls.Add(Me.RadioOeste)
        Me.GroupBox1.Controls.Add(Me.RadioEste)
        Me.GroupBox1.Controls.Add(Me.RadioSur)
        Me.GroupBox1.Controls.Add(Me.Label4)
        Me.GroupBox1.Controls.Add(Me.RadioNorte)
        Me.GroupBox1.Controls.Add(Me.Label3)
        Me.GroupBox1.Controls.Add(Me.Label2)
        Me.GroupBox1.Controls.Add(Me.Label1)
        Me.GroupBox1.Controls.Add(Me.Label50)
        Me.GroupBox1.Controls.Add(Me.Button1)
        Me.GroupBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox1.Location = New System.Drawing.Point(9, 8)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(314, 336)
        Me.GroupBox1.TabIndex = 44
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Calles"
        '
        'cbOeste
        '
        Me.cbOeste.DisplayMember = "Nombre"
        Me.cbOeste.FormattingEnabled = True
        Me.cbOeste.Location = New System.Drawing.Point(15, 193)
        Me.cbOeste.Name = "cbOeste"
        Me.cbOeste.Size = New System.Drawing.Size(263, 26)
        Me.cbOeste.TabIndex = 51
        Me.cbOeste.ValueMember = "Clv_Calle"
        '
        'cbEste
        '
        Me.cbEste.DisplayMember = "Nombre"
        Me.cbEste.FormattingEnabled = True
        Me.cbEste.Location = New System.Drawing.Point(13, 143)
        Me.cbEste.Name = "cbEste"
        Me.cbEste.Size = New System.Drawing.Size(263, 26)
        Me.cbEste.TabIndex = 50
        Me.cbEste.ValueMember = "Clv_Calle"
        '
        'cbSur
        '
        Me.cbSur.DisplayMember = "Nombre"
        Me.cbSur.FormattingEnabled = True
        Me.cbSur.Location = New System.Drawing.Point(13, 94)
        Me.cbSur.Name = "cbSur"
        Me.cbSur.Size = New System.Drawing.Size(263, 26)
        Me.cbSur.TabIndex = 49
        Me.cbSur.ValueMember = "Clv_Calle"
        '
        'cbNorte
        '
        Me.cbNorte.DisplayMember = "Nombre"
        Me.cbNorte.FormattingEnabled = True
        Me.cbNorte.Location = New System.Drawing.Point(13, 38)
        Me.cbNorte.Name = "cbNorte"
        Me.cbNorte.Size = New System.Drawing.Size(263, 26)
        Me.cbNorte.TabIndex = 48
        Me.cbNorte.ValueMember = "Clv_Calle"
        '
        'tbReferencia
        '
        Me.tbReferencia.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.tbReferencia.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.tbReferencia.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbReferencia.Location = New System.Drawing.Point(13, 239)
        Me.tbReferencia.Multiline = True
        Me.tbReferencia.Name = "tbReferencia"
        Me.tbReferencia.Size = New System.Drawing.Size(252, 34)
        Me.tbReferencia.TabIndex = 46
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.ForeColor = System.Drawing.Color.LightSlateGray
        Me.Label5.Location = New System.Drawing.Point(10, 222)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(85, 15)
        Me.Label5.TabIndex = 47
        Me.Label5.Text = "Referencia: "
        '
        'RadioOeste
        '
        Me.RadioOeste.AutoSize = True
        Me.RadioOeste.Location = New System.Drawing.Point(282, 193)
        Me.RadioOeste.Name = "RadioOeste"
        Me.RadioOeste.Size = New System.Drawing.Size(14, 13)
        Me.RadioOeste.TabIndex = 45
        Me.RadioOeste.TabStop = True
        Me.RadioOeste.UseVisualStyleBackColor = True
        '
        'RadioEste
        '
        Me.RadioEste.AutoSize = True
        Me.RadioEste.Location = New System.Drawing.Point(282, 143)
        Me.RadioEste.Name = "RadioEste"
        Me.RadioEste.Size = New System.Drawing.Size(14, 13)
        Me.RadioEste.TabIndex = 44
        Me.RadioEste.TabStop = True
        Me.RadioEste.UseVisualStyleBackColor = True
        '
        'RadioSur
        '
        Me.RadioSur.AutoSize = True
        Me.RadioSur.Location = New System.Drawing.Point(282, 94)
        Me.RadioSur.Name = "RadioSur"
        Me.RadioSur.Size = New System.Drawing.Size(14, 13)
        Me.RadioSur.TabIndex = 43
        Me.RadioSur.TabStop = True
        Me.RadioSur.UseVisualStyleBackColor = True
        '
        'RadioNorte
        '
        Me.RadioNorte.AutoSize = True
        Me.RadioNorte.Location = New System.Drawing.Point(282, 41)
        Me.RadioNorte.Name = "RadioNorte"
        Me.RadioNorte.Size = New System.Drawing.Size(14, 13)
        Me.RadioNorte.TabIndex = 41
        Me.RadioNorte.TabStop = True
        Me.RadioNorte.UseVisualStyleBackColor = True
        '
        'Button1
        '
        Me.Button1.BackColor = System.Drawing.Color.DarkOrange
        Me.Button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button1.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button1.ForeColor = System.Drawing.Color.Black
        Me.Button1.Location = New System.Drawing.Point(85, 282)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(137, 40)
        Me.Button1.TabIndex = 34
        Me.Button1.Text = "Guardar"
        Me.Button1.UseVisualStyleBackColor = False
        '
        'LbNorte
        '
        Me.LbNorte.BackColor = System.Drawing.Color.White
        Me.LbNorte.Location = New System.Drawing.Point(527, 110)
        Me.LbNorte.Name = "LbNorte"
        Me.LbNorte.Size = New System.Drawing.Size(212, 39)
        Me.LbNorte.TabIndex = 45
        Me.LbNorte.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'LbSur
        '
        Me.LbSur.BackColor = System.Drawing.Color.White
        Me.LbSur.Location = New System.Drawing.Point(527, 267)
        Me.LbSur.Name = "LbSur"
        Me.LbSur.Size = New System.Drawing.Size(212, 39)
        Me.LbSur.TabIndex = 46
        Me.LbSur.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'LbEste
        '
        Me.LbEste.BackColor = System.Drawing.Color.White
        Me.LbEste.Location = New System.Drawing.Point(745, 46)
        Me.LbEste.Name = "LbEste"
        Me.LbEste.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.LbEste.Size = New System.Drawing.Size(67, 314)
        Me.LbEste.TabIndex = 47
        Me.LbEste.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'LbOeste
        '
        Me.LbOeste.BackColor = System.Drawing.Color.White
        Me.LbOeste.Location = New System.Drawing.Point(444, 48)
        Me.LbOeste.Name = "LbOeste"
        Me.LbOeste.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.LbOeste.Size = New System.Drawing.Size(77, 314)
        Me.LbOeste.TabIndex = 48
        Me.LbOeste.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Button5
        '
        Me.Button5.BackColor = System.Drawing.Color.DarkOrange
        Me.Button5.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button5.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button5.ForeColor = System.Drawing.Color.Black
        Me.Button5.Location = New System.Drawing.Point(778, 440)
        Me.Button5.Name = "Button5"
        Me.Button5.Size = New System.Drawing.Size(136, 40)
        Me.Button5.TabIndex = 49
        Me.Button5.Text = "&SALIR"
        Me.Button5.UseVisualStyleBackColor = False
        '
        'PictureBox1
        '
        Me.PictureBox1.Image = Global.softvFacturacion.My.Resources.Resources.Croquis
        Me.PictureBox1.Location = New System.Drawing.Point(329, 22)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(605, 380)
        Me.PictureBox1.TabIndex = 0
        Me.PictureBox1.TabStop = False
        '
        'PictureBox2
        '
        Me.PictureBox2.Image = Global.softvFacturacion.My.Resources.Resources.logoCasa
        Me.PictureBox2.Location = New System.Drawing.Point(619, 194)
        Me.PictureBox2.Name = "PictureBox2"
        Me.PictureBox2.Size = New System.Drawing.Size(25, 25)
        Me.PictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox2.TabIndex = 50
        Me.PictureBox2.TabStop = False
        Me.PictureBox2.Visible = False
        '
        'Button2
        '
        Me.Button2.BackColor = System.Drawing.Color.DarkOrange
        Me.Button2.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button2.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button2.ForeColor = System.Drawing.Color.Black
        Me.Button2.Location = New System.Drawing.Point(88, 87)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(137, 40)
        Me.Button2.TabIndex = 34
        Me.Button2.Text = "Guardar"
        Me.Button2.UseVisualStyleBackColor = False
        '
        'tbLatDecimal
        '
        Me.tbLatDecimal.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.tbLatDecimal.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.tbLatDecimal.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbLatDecimal.Location = New System.Drawing.Point(88, 21)
        Me.tbLatDecimal.Multiline = True
        Me.tbLatDecimal.Name = "tbLatDecimal"
        Me.tbLatDecimal.Size = New System.Drawing.Size(219, 26)
        Me.tbLatDecimal.TabIndex = 1
        '
        'tbLongDecimal
        '
        Me.tbLongDecimal.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.tbLongDecimal.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.tbLongDecimal.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbLongDecimal.Location = New System.Drawing.Point(88, 56)
        Me.tbLongDecimal.Multiline = True
        Me.tbLongDecimal.Name = "tbLongDecimal"
        Me.tbLongDecimal.Size = New System.Drawing.Size(219, 26)
        Me.tbLongDecimal.TabIndex = 35
        '
        'GroupBoxCoordenadas
        '
        Me.GroupBoxCoordenadas.Controls.Add(Me.tbLongDecimal)
        Me.GroupBoxCoordenadas.Controls.Add(Me.Label8)
        Me.GroupBoxCoordenadas.Controls.Add(Me.tbLatDecimal)
        Me.GroupBoxCoordenadas.Controls.Add(Me.Label9)
        Me.GroupBoxCoordenadas.Controls.Add(Me.Button2)
        Me.GroupBoxCoordenadas.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBoxCoordenadas.Location = New System.Drawing.Point(6, 350)
        Me.GroupBoxCoordenadas.Name = "GroupBoxCoordenadas"
        Me.GroupBoxCoordenadas.Size = New System.Drawing.Size(314, 134)
        Me.GroupBoxCoordenadas.TabIndex = 51
        Me.GroupBoxCoordenadas.TabStop = False
        Me.GroupBoxCoordenadas.Text = "Coordenadas"
        Me.GroupBoxCoordenadas.Visible = False
        '
        'Button3
        '
        Me.Button3.BackColor = System.Drawing.Color.DarkOrange
        Me.Button3.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button3.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button3.ForeColor = System.Drawing.Color.Black
        Me.Button3.Location = New System.Drawing.Point(352, 437)
        Me.Button3.Name = "Button3"
        Me.Button3.Size = New System.Drawing.Size(140, 40)
        Me.Button3.TabIndex = 52
        Me.Button3.Text = "Ver Mapa"
        Me.Button3.UseVisualStyleBackColor = False
        '
        'FrmEntreCalles
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(944, 509)
        Me.Controls.Add(Me.Button3)
        Me.Controls.Add(Me.GroupBoxCoordenadas)
        Me.Controls.Add(Me.PictureBox2)
        Me.Controls.Add(Me.Button5)
        Me.Controls.Add(Me.LbOeste)
        Me.Controls.Add(Me.LbEste)
        Me.Controls.Add(Me.LbSur)
        Me.Controls.Add(Me.LbNorte)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.PictureBox1)
        Me.Name = "FrmEntreCalles"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Entrecalles"
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBoxCoordenadas.ResumeLayout(False)
        Me.GroupBoxCoordenadas.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents PictureBox1 As System.Windows.Forms.PictureBox
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents LbNorte As System.Windows.Forms.Label
    Friend WithEvents LbSur As System.Windows.Forms.Label
    Friend WithEvents LbEste As System.Windows.Forms.Label
    Friend WithEvents LbOeste As System.Windows.Forms.Label
    Friend WithEvents Button5 As System.Windows.Forms.Button
    Friend WithEvents RadioOeste As System.Windows.Forms.RadioButton
    Friend WithEvents RadioEste As System.Windows.Forms.RadioButton
    Friend WithEvents RadioSur As System.Windows.Forms.RadioButton
    Friend WithEvents RadioNorte As System.Windows.Forms.RadioButton
    Friend WithEvents PictureBox2 As System.Windows.Forms.PictureBox
    Friend WithEvents Button2 As System.Windows.Forms.Button
    Friend WithEvents tbLatDecimal As System.Windows.Forms.TextBox
    Friend WithEvents tbLongDecimal As System.Windows.Forms.TextBox
    Friend WithEvents GroupBoxCoordenadas As System.Windows.Forms.GroupBox
    Friend WithEvents Button3 As System.Windows.Forms.Button
    Friend WithEvents Label50 As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents tbReferencia As System.Windows.Forms.TextBox
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents cbOeste As System.Windows.Forms.ComboBox
    Friend WithEvents cbEste As System.Windows.Forms.ComboBox
    Friend WithEvents cbSur As System.Windows.Forms.ComboBox
    Friend WithEvents cbNorte As System.Windows.Forms.ComboBox
End Class
