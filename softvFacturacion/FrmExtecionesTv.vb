Imports System.Data.SqlClient

Public Class FrmExtecionesTv
    Public LOCNUMTVADIC As Integer = 0

    Private Function SP_VALIDAEXTECIONESFAC(ByVal oContrato As Long) As Long
        SP_VALIDAEXTECIONESFAC = 0
        Dim conexion As New SqlConnection(MiConexion)
        Dim comando As New SqlCommand("SP_VALIDAEXTECIONESFAC", conexion)
        comando.CommandType = CommandType.StoredProcedure
        comando.CommandTimeout = 0

        Dim parametro As New SqlParameter("@Contrato", SqlDbType.BigInt)
        parametro.Direction = ParameterDirection.Input
        parametro.Value = oContrato
        comando.Parameters.Add(parametro)

        Dim parametro2 As New SqlParameter("@Limite", SqlDbType.Int)
        parametro2.Direction = ParameterDirection.Output
        comando.Parameters.Add(parametro2)



        Try
            conexion.Open()
            comando.ExecuteNonQuery()
            SP_VALIDAEXTECIONESFAC = CInt(parametro2.Value)
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        Finally
            conexion.Close()
            conexion.Dispose()
        End Try
    End Function

    Private Function SP_DIMECUANTASEXT_TIENES(ByVal oContrato As Long) As Long
        SP_DIMECUANTASEXT_TIENES = 0
        Dim conexion As New SqlConnection(MiConexion)
        Dim comando As New SqlCommand("SP_DIMECUANTASEXT_TIENES", conexion)
        comando.CommandType = CommandType.StoredProcedure
        comando.CommandTimeout = 0

        Dim parametro As New SqlParameter("@Contrato", SqlDbType.BigInt)
        parametro.Direction = ParameterDirection.Input
        parametro.Value = oContrato
        comando.Parameters.Add(parametro)

        Dim parametro2 As New SqlParameter("@TvAdic", SqlDbType.Int)
        parametro2.Direction = ParameterDirection.Output
        comando.Parameters.Add(parametro2)

        Try
            conexion.Open()
            comando.ExecuteNonQuery()
            SP_DIMECUANTASEXT_TIENES = CInt(parametro2.Value)
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        Finally
            conexion.Close()
            conexion.Dispose()
        End Try
    End Function

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        If Me.NumericUpDown1.Value > 0 Then
            GloBndExt = True
            GloExt = Me.NumericUpDown1.Value
            If LOCNUMTVADIC + GloExt > (GloTvConpago + GloTvSinPago) Then
                'Dim resp As MsgBoxResult
                'resp = MsgBox("Se ha rebasado el total de TVS sin costo. �Desea continuar?", MsgBoxStyle.YesNo, "Pregunta Importante")
                MsgBox("Se ha rebasado el total de TVS adicionales. Debe hacer un nuevo contrato.")
                ' If resp = MsgBoxResult.No Then
                GloExt = 0
                GloBndExt = False
                'End If
            End If
        Else
            GloExt = 0
            GloBndExt = False
        End If
        Me.Close()
    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        GloExt = 0
        GloBndExt = False
        Me.Close()
    End Sub

    Private Sub FrmExtecionesTv_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        colorea(Me)
        LOCNUMTVADIC = 0
        LOCNUMTVADIC = SP_DIMECUANTASEXT_TIENES(GloContrato)
        NumericUpDown1.Maximum = SP_VALIDAEXTECIONESFAC(GloContrato)
    End Sub
End Class