Imports System.Data.SqlClient
Imports System.Text

Public Class FrmArqueo
    Private Sub Llena_companias()

        Try
            BaseII.limpiaParametros()
            'BaseII.CreateMyParameter("@Op", SqlDbType.Int, 3)
            BaseII.CreateMyParameter("@ClvUsuario", SqlDbType.Int, GloClvUsuario)
            ComboBoxCompanias.DataSource = BaseII.ConsultaDT("Muestra_Compania_RelUsuario")
            ComboBoxCompanias.DisplayMember = "razon_social"
            ComboBoxCompanias.ValueMember = "id_compania"


            'ComboBoxCiudades.Text = ""
        Catch ex As Exception

        End Try
    End Sub
    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
    End Sub

    Private Sub FrmArqueo_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        colorea(Me)
        Llena_companias()
        GloIdCompania = ComboBoxCompanias.SelectedValue
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        'Me.MUESTRACAJERASTableAdapter.Connection = CON
        'Me.MUESTRACAJERASTableAdapter.Fill(Me.NewsoftvDataSet1.MUESTRACAJERAS, "0")
        MUESTRACAJERAS()
        CON.Close()
        Me.NombreComboBox.Text = ""
        Me.DateTimePicker1.Value = Today
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)

    End Sub

    Private Sub OK_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles OK.Click
        BanderaReporte = False
        GloCajera = Me.NombreComboBox.SelectedValue()
        GLONOMCAJERAARQUEO = Me.NombreComboBox.Text
        Fecha_ini = Me.DateTimePicker1.Text
        Dim x As Integer = 0
        For x = 0 To 1
            If x = 1 Then
                GloReporte = 9
            Else
                GloReporte = 4
            End If
            My.Forms.FrmImprimirRepGral.Show()
        Next
        Me.Close()
    End Sub

    Private Sub Cancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Cancel.Click
        Me.Close()


    End Sub

    Private Sub DateTimePicker1_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles DateTimePicker1.ValueChanged

        Me.DateTimePicker1.MaxDate = Today
    End Sub

    Private Sub MUESTRACAJERAS()
        Dim conexion As New SqlConnection(MiConexion)
        Dim strSQL As New StringBuilder
        strSQL.Append("EXEC MUESTRACAJERAS " + GloClvUsuario.ToString() + "," + GloIdCompania.ToString)
        'strSQL.Append(CStr(Me.ComboBox8.SelectedValue))

        Dim dataAdapter As New SqlDataAdapter(strSQL.ToString(), conexion)
        Dim dataTable As New DataTable
        Dim bindingSource As New BindingSource

        Try
            conexion.Open()
            dataAdapter.Fill(dataTable)
            bindingSource.DataSource = dataTable
            Me.NombreComboBox.DataSource = bindingSource
            'Me.CONTRATOLabel1.Text = CStr(Folio)
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        Finally
            conexion.Close()
            conexion.Dispose()
        End Try
    End Sub

    Private Sub ComboBoxCompanias_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ComboBoxCompanias.SelectedIndexChanged
        Try
            If GloIdCompania > 0 Then
                GloIdCompania = ComboBoxCompanias.SelectedValue
                MUESTRACAJERAS()
            End If
        Catch ex As Exception

        End Try
        
    End Sub
End Class