<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form4
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.DataSetLydia = New softvFacturacion.DataSetLydia()
        Me.Consulta_NotaCreditoBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Consulta_NotaCreditoTableAdapter = New softvFacturacion.DataSetLydiaTableAdapters.Consulta_NotaCreditoTableAdapter()
        CType(Me.DataSetLydia, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Consulta_NotaCreditoBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'DataSetLydia
        '
        Me.DataSetLydia.DataSetName = "DataSetLydia"
        Me.DataSetLydia.EnforceConstraints = False
        Me.DataSetLydia.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'Consulta_NotaCreditoBindingSource
        '
        Me.Consulta_NotaCreditoBindingSource.DataMember = "Consulta_NotaCredito"
        Me.Consulta_NotaCreditoBindingSource.DataSource = Me.DataSetLydia
        '
        'Consulta_NotaCreditoTableAdapter
        '
        Me.Consulta_NotaCreditoTableAdapter.ClearBeforeFill = True
        '
        'Form4
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.AutoScroll = True
        Me.ClientSize = New System.Drawing.Size(1220, 658)
        Me.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Name = "Form4"
        Me.Text = "Form4"
        CType(Me.DataSetLydia, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Consulta_NotaCreditoBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents DataSetLydia As softvFacturacion.DataSetLydia
    Friend WithEvents Consulta_NotaCreditoBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Consulta_NotaCreditoTableAdapter As softvFacturacion.DataSetLydiaTableAdapters.Consulta_NotaCreditoTableAdapter
End Class
