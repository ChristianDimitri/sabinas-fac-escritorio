﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmFechasCobroMaterial
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.rbPorContrato = New System.Windows.Forms.RadioButton()
        Me.rbPorFechas = New System.Windows.Forms.RadioButton()
        Me.lblContrato = New System.Windows.Forms.Label()
        Me.txtContrato = New System.Windows.Forms.TextBox()
        Me.lblFechaIni = New System.Windows.Forms.Label()
        Me.lblFechaFin = New System.Windows.Forms.Label()
        Me.dtpFechaIni = New System.Windows.Forms.DateTimePicker()
        Me.dtpFechaFin = New System.Windows.Forms.DateTimePicker()
        Me.btnGenerar = New System.Windows.Forms.Button()
        Me.btnCancelar = New System.Windows.Forms.Button()
        Me.gbRangos = New System.Windows.Forms.GroupBox()
        Me.pnlStatus = New System.Windows.Forms.Panel()
        Me.cbPendientes = New System.Windows.Forms.CheckBox()
        Me.cbSaldados = New System.Windows.Forms.CheckBox()
        Me.rbPorStatus = New System.Windows.Forms.RadioButton()
        Me.pnlFechas = New System.Windows.Forms.Panel()
        Me.pnlContrato = New System.Windows.Forms.Panel()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.ComboBoxCompanias = New System.Windows.Forms.ComboBox()
        Me.gbRangos.SuspendLayout()
        Me.pnlStatus.SuspendLayout()
        Me.pnlFechas.SuspendLayout()
        Me.pnlContrato.SuspendLayout()
        Me.GroupBox1.SuspendLayout()
        Me.SuspendLayout()
        '
        'rbPorContrato
        '
        Me.rbPorContrato.AutoSize = True
        Me.rbPorContrato.Checked = True
        Me.rbPorContrato.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rbPorContrato.Location = New System.Drawing.Point(14, 21)
        Me.rbPorContrato.Name = "rbPorContrato"
        Me.rbPorContrato.Size = New System.Drawing.Size(139, 24)
        Me.rbPorContrato.TabIndex = 0
        Me.rbPorContrato.TabStop = True
        Me.rbPorContrato.Text = "Por Contrato :"
        Me.rbPorContrato.UseVisualStyleBackColor = True
        '
        'rbPorFechas
        '
        Me.rbPorFechas.AutoSize = True
        Me.rbPorFechas.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rbPorFechas.Location = New System.Drawing.Point(14, 104)
        Me.rbPorFechas.Name = "rbPorFechas"
        Me.rbPorFechas.Size = New System.Drawing.Size(128, 24)
        Me.rbPorFechas.TabIndex = 3
        Me.rbPorFechas.Text = "Por Fechas :"
        Me.rbPorFechas.UseVisualStyleBackColor = True
        '
        'lblContrato
        '
        Me.lblContrato.AutoSize = True
        Me.lblContrato.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblContrato.Location = New System.Drawing.Point(21, 6)
        Me.lblContrato.Name = "lblContrato"
        Me.lblContrato.Size = New System.Drawing.Size(74, 16)
        Me.lblContrato.TabIndex = 1
        Me.lblContrato.Text = "Contrato :"
        '
        'txtContrato
        '
        Me.txtContrato.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtContrato.Location = New System.Drawing.Point(101, 3)
        Me.txtContrato.Name = "txtContrato"
        Me.txtContrato.Size = New System.Drawing.Size(138, 22)
        Me.txtContrato.TabIndex = 2
        '
        'lblFechaIni
        '
        Me.lblFechaIni.AutoSize = True
        Me.lblFechaIni.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblFechaIni.Location = New System.Drawing.Point(3, 6)
        Me.lblFechaIni.Name = "lblFechaIni"
        Me.lblFechaIni.Size = New System.Drawing.Size(104, 16)
        Me.lblFechaIni.TabIndex = 4
        Me.lblFechaIni.Text = "Fecha Inicial :"
        '
        'lblFechaFin
        '
        Me.lblFechaFin.AutoSize = True
        Me.lblFechaFin.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblFechaFin.Location = New System.Drawing.Point(10, 36)
        Me.lblFechaFin.Name = "lblFechaFin"
        Me.lblFechaFin.Size = New System.Drawing.Size(97, 16)
        Me.lblFechaFin.TabIndex = 6
        Me.lblFechaFin.Text = "Fecha Final :"
        '
        'dtpFechaIni
        '
        Me.dtpFechaIni.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpFechaIni.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpFechaIni.Location = New System.Drawing.Point(111, 3)
        Me.dtpFechaIni.Name = "dtpFechaIni"
        Me.dtpFechaIni.Size = New System.Drawing.Size(147, 22)
        Me.dtpFechaIni.TabIndex = 5
        '
        'dtpFechaFin
        '
        Me.dtpFechaFin.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpFechaFin.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpFechaFin.Location = New System.Drawing.Point(111, 35)
        Me.dtpFechaFin.Name = "dtpFechaFin"
        Me.dtpFechaFin.Size = New System.Drawing.Size(147, 22)
        Me.dtpFechaFin.TabIndex = 7
        '
        'btnGenerar
        '
        Me.btnGenerar.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnGenerar.Location = New System.Drawing.Point(23, 371)
        Me.btnGenerar.Name = "btnGenerar"
        Me.btnGenerar.Size = New System.Drawing.Size(112, 33)
        Me.btnGenerar.TabIndex = 8
        Me.btnGenerar.Text = "&Generar"
        Me.btnGenerar.UseVisualStyleBackColor = True
        '
        'btnCancelar
        '
        Me.btnCancelar.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnCancelar.Location = New System.Drawing.Point(163, 371)
        Me.btnCancelar.Name = "btnCancelar"
        Me.btnCancelar.Size = New System.Drawing.Size(110, 33)
        Me.btnCancelar.TabIndex = 9
        Me.btnCancelar.Text = "&Cancelar"
        Me.btnCancelar.UseVisualStyleBackColor = True
        '
        'gbRangos
        '
        Me.gbRangos.Controls.Add(Me.pnlStatus)
        Me.gbRangos.Controls.Add(Me.rbPorStatus)
        Me.gbRangos.Controls.Add(Me.pnlFechas)
        Me.gbRangos.Controls.Add(Me.pnlContrato)
        Me.gbRangos.Controls.Add(Me.rbPorContrato)
        Me.gbRangos.Controls.Add(Me.rbPorFechas)
        Me.gbRangos.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbRangos.Location = New System.Drawing.Point(9, 77)
        Me.gbRangos.Name = "gbRangos"
        Me.gbRangos.Size = New System.Drawing.Size(285, 288)
        Me.gbRangos.TabIndex = 10
        Me.gbRangos.TabStop = False
        Me.gbRangos.Text = "Rangos"
        '
        'pnlStatus
        '
        Me.pnlStatus.Controls.Add(Me.cbPendientes)
        Me.pnlStatus.Controls.Add(Me.cbSaldados)
        Me.pnlStatus.Location = New System.Drawing.Point(6, 239)
        Me.pnlStatus.Name = "pnlStatus"
        Me.pnlStatus.Size = New System.Drawing.Size(269, 43)
        Me.pnlStatus.TabIndex = 11
        '
        'cbPendientes
        '
        Me.cbPendientes.AutoSize = True
        Me.cbPendientes.Location = New System.Drawing.Point(148, 12)
        Me.cbPendientes.Name = "cbPendientes"
        Me.cbPendientes.Size = New System.Drawing.Size(105, 20)
        Me.cbPendientes.TabIndex = 12
        Me.cbPendientes.Text = "Pendientes"
        Me.cbPendientes.UseVisualStyleBackColor = True
        '
        'cbSaldados
        '
        Me.cbSaldados.AutoSize = True
        Me.cbSaldados.Location = New System.Drawing.Point(22, 12)
        Me.cbSaldados.Name = "cbSaldados"
        Me.cbSaldados.Size = New System.Drawing.Size(94, 20)
        Me.cbSaldados.TabIndex = 11
        Me.cbSaldados.Text = "Saldados"
        Me.cbSaldados.UseVisualStyleBackColor = True
        '
        'rbPorStatus
        '
        Me.rbPorStatus.AutoSize = True
        Me.rbPorStatus.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rbPorStatus.Location = New System.Drawing.Point(14, 209)
        Me.rbPorStatus.Name = "rbPorStatus"
        Me.rbPorStatus.Size = New System.Drawing.Size(122, 24)
        Me.rbPorStatus.TabIndex = 10
        Me.rbPorStatus.Text = "Por Status :"
        Me.rbPorStatus.UseVisualStyleBackColor = True
        '
        'pnlFechas
        '
        Me.pnlFechas.Controls.Add(Me.dtpFechaIni)
        Me.pnlFechas.Controls.Add(Me.lblFechaFin)
        Me.pnlFechas.Controls.Add(Me.lblFechaIni)
        Me.pnlFechas.Controls.Add(Me.dtpFechaFin)
        Me.pnlFechas.Location = New System.Drawing.Point(6, 134)
        Me.pnlFechas.Name = "pnlFechas"
        Me.pnlFechas.Size = New System.Drawing.Size(269, 64)
        Me.pnlFechas.TabIndex = 9
        '
        'pnlContrato
        '
        Me.pnlContrato.Controls.Add(Me.txtContrato)
        Me.pnlContrato.Controls.Add(Me.lblContrato)
        Me.pnlContrato.Location = New System.Drawing.Point(14, 51)
        Me.pnlContrato.Name = "pnlContrato"
        Me.pnlContrato.Size = New System.Drawing.Size(250, 36)
        Me.pnlContrato.TabIndex = 8
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.ComboBoxCompanias)
        Me.GroupBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold)
        Me.GroupBox1.Location = New System.Drawing.Point(9, 12)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(285, 59)
        Me.GroupBox1.TabIndex = 11
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Plaza"
        '
        'ComboBoxCompanias
        '
        Me.ComboBoxCompanias.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ComboBoxCompanias.FormattingEnabled = True
        Me.ComboBoxCompanias.Location = New System.Drawing.Point(38, 22)
        Me.ComboBoxCompanias.Name = "ComboBoxCompanias"
        Me.ComboBoxCompanias.Size = New System.Drawing.Size(201, 24)
        Me.ComboBoxCompanias.TabIndex = 521
        '
        'FrmFechasCobroMaterial
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(304, 414)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.gbRangos)
        Me.Controls.Add(Me.btnCancelar)
        Me.Controls.Add(Me.btnGenerar)
        Me.Name = "FrmFechasCobroMaterial"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Seleccion de Rangos"
        Me.gbRangos.ResumeLayout(False)
        Me.gbRangos.PerformLayout()
        Me.pnlStatus.ResumeLayout(False)
        Me.pnlStatus.PerformLayout()
        Me.pnlFechas.ResumeLayout(False)
        Me.pnlFechas.PerformLayout()
        Me.pnlContrato.ResumeLayout(False)
        Me.pnlContrato.PerformLayout()
        Me.GroupBox1.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents rbPorContrato As System.Windows.Forms.RadioButton
    Friend WithEvents rbPorFechas As System.Windows.Forms.RadioButton
    Friend WithEvents lblContrato As System.Windows.Forms.Label
    Friend WithEvents txtContrato As System.Windows.Forms.TextBox
    Friend WithEvents lblFechaIni As System.Windows.Forms.Label
    Friend WithEvents lblFechaFin As System.Windows.Forms.Label
    Friend WithEvents dtpFechaIni As System.Windows.Forms.DateTimePicker
    Friend WithEvents dtpFechaFin As System.Windows.Forms.DateTimePicker
    Friend WithEvents btnGenerar As System.Windows.Forms.Button
    Friend WithEvents btnCancelar As System.Windows.Forms.Button
    Friend WithEvents gbRangos As System.Windows.Forms.GroupBox
    Friend WithEvents pnlContrato As System.Windows.Forms.Panel
    Friend WithEvents pnlFechas As System.Windows.Forms.Panel
    Friend WithEvents pnlStatus As System.Windows.Forms.Panel
    Friend WithEvents cbPendientes As System.Windows.Forms.CheckBox
    Friend WithEvents cbSaldados As System.Windows.Forms.CheckBox
    Friend WithEvents rbPorStatus As System.Windows.Forms.RadioButton
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents ComboBoxCompanias As System.Windows.Forms.ComboBox
End Class
