﻿Imports System.Data.SqlClient
Public Class FrmSelCompania

    Private Sub FrmSelCompania_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        'Por si es uno
        Dim CONar As New SqlConnection(MiConexion)
        Dim cmd As New SqlCommand()
        Dim numero As Integer
        Try
            cmd = New SqlCommand()
            CONar.Open()
            '@Contrato bigint,@IdCompania int,@ContratoCompania bigint
            With cmd
                .CommandText = "DameNumCompaniasUsuario"
                .Connection = CONar
                .CommandTimeout = 0
                .CommandType = CommandType.StoredProcedure

                Dim prm As New SqlParameter("@ClaveUsuario", SqlDbType.Int)
                prm.Direction = ParameterDirection.Input
                prm.Value = GloClvUsuario
                .Parameters.Add(prm)

                Dim prm1 As New SqlParameter("@numero", SqlDbType.Int)
                prm1.Direction = ParameterDirection.Output
                prm1.Value = 0
                .Parameters.Add(prm1)

                Dim prm2 As New SqlParameter("@identificador", SqlDbType.BigInt)
                prm2.Direction = ParameterDirection.Input
                prm2.Value = identificador
                .Parameters.Add(prm2)
                Dim ia As Integer = .ExecuteNonQuery()

                numero = prm1.Value
            End With
            CONar.Close()
        Catch ex As Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
        If numero = 1 Then
            'Dim conexion As New SqlConnection(MiConexion)
            'conexion.Open()
            'Dim comando As New SqlCommand()
            'comando.Connection = conexion
            'comando.CommandText = " exec DameIdentificador"
            'identificador = comando.ExecuteScalar()
            'conexion.Close()
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@Identificador", SqlDbType.BigInt, identificador)
            BaseII.CreateMyParameter("@claveusuario", SqlDbType.Int, GloClvUsuario)
            BaseII.Inserta("InsertaSeleccionCompaniatbl")
            llevamealotro()
            Exit Sub
        End If
        'Por si es uni
        colorea(Me)
        'Dim conexion2 As New SqlConnection(MiConexion)
        'conexion2.Open()
        'Dim comando2 As New SqlCommand()
        'comando2.Connection = conexion2
        'comando2.CommandText = " exec DameIdentificador"
        'identificador = comando2.ExecuteScalar()
        'conexion2.Close()
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@opcion", SqlDbType.Int, -1)
        BaseII.CreateMyParameter("@clv_session", SqlDbType.Int, CInt(GloClvUsuario))
        BaseII.CreateMyParameter("@idcompania", SqlDbType.Int, 0)
        BaseII.CreateMyParameter("@identificador", SqlDbType.Int, identificador)
        BaseII.Inserta("ProcedureSeleccionCompania")
        llenalistboxs()
    End Sub
    Private Sub llenalistboxs()
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@opcion", SqlDbType.Int, 2)
        BaseII.CreateMyParameter("@clv_session", SqlDbType.Int, CInt(LocClv_session))
        BaseII.CreateMyParameter("@idcompania", SqlDbType.Int, 0)
        BaseII.CreateMyParameter("@identificador", SqlDbType.Int, identificador)
        loquehay.DataSource = BaseII.ConsultaDT("ProcedureSeleccionCompania")
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@opcion", SqlDbType.Int, 3)
        BaseII.CreateMyParameter("@clv_session", SqlDbType.Int, CInt(LocClv_session))
        BaseII.CreateMyParameter("@idcompania", SqlDbType.Int, 0)
        BaseII.CreateMyParameter("@identificador", SqlDbType.Int, identificador)
        seleccion.DataSource = BaseII.ConsultaDT("ProcedureSeleccionCompania")
    End Sub

    Private Sub agregar_Click(sender As System.Object, e As System.EventArgs) Handles agregar.Click
        If loquehay.Items.Count = 0 Then
            Exit Sub
        End If
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@opcion", SqlDbType.Int, 4)
        BaseII.CreateMyParameter("@clv_session", SqlDbType.Int, CInt(LocClv_session))
        BaseII.CreateMyParameter("@idcompania", SqlDbType.Int, loquehay.SelectedValue)
        BaseII.CreateMyParameter("@identificador", SqlDbType.Int, identificador)
        BaseII.Inserta("ProcedureSeleccionCompania")
        llenalistboxs()
    End Sub

    Private Sub quitar_Click(sender As System.Object, e As System.EventArgs) Handles quitar.Click
        If seleccion.Items.Count = 0 Then
            Exit Sub
        End If
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@opcion", SqlDbType.Int, 5)
        BaseII.CreateMyParameter("@clv_session", SqlDbType.Int, CInt(LocClv_session))
        BaseII.CreateMyParameter("@idcompania", SqlDbType.Int, seleccion.SelectedValue)
        BaseII.CreateMyParameter("@identificador", SqlDbType.Int, identificador)
        BaseII.Inserta("ProcedureSeleccionCompania")
        llenalistboxs()
    End Sub

    Private Sub agregartodo_Click(sender As System.Object, e As System.EventArgs) Handles agregartodo.Click
        If loquehay.Items.Count = 0 Then
            Exit Sub
        End If
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@opcion", SqlDbType.Int, 6)
        BaseII.CreateMyParameter("@clv_session", SqlDbType.Int, CInt(LocClv_session))
        BaseII.CreateMyParameter("@idcompania", SqlDbType.Int, 0)
        BaseII.CreateMyParameter("@identificador", SqlDbType.Int, identificador)
        BaseII.Inserta("ProcedureSeleccionCompania")
        llenalistboxs()
    End Sub

    Private Sub quitartodo_Click(sender As System.Object, e As System.EventArgs) Handles quitartodo.Click
        If seleccion.Items.Count = 0 Then
            Exit Sub
        End If
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@opcion", SqlDbType.Int, 7)
        BaseII.CreateMyParameter("@clv_session", SqlDbType.Int, CInt(LocClv_session))
        BaseII.CreateMyParameter("@idcompania", SqlDbType.Int, 0)
        BaseII.CreateMyParameter("@identificador", SqlDbType.Int, identificador)
        BaseII.Inserta("ProcedureSeleccionCompania")
        llenalistboxs()
    End Sub

    Private Sub Button5_Click(sender As System.Object, e As System.EventArgs) Handles Button5.Click
        EntradasSelCiudad = 0
        Me.Close()
    End Sub

    Private Sub Button6_Click(sender As System.Object, e As System.EventArgs) Handles Button6.Click
        If seleccion.Items.Count = 0 Then
            Exit Sub
        End If
        'If varfrmselcompania = "cortefac" Then
        '    FrmOpRep.Show()
        'ElseIf varfrmselcompania = "relingreso" Then
        '    If GloBnd_Des_Men = True Then
        '        FrmFechaIngresosConcepto.Show()
        '        Me.Close()
        '    ElseIf LocbndPolizaCiudad = True Then
        '        FrmFechaIngresosConcepto.Show()
        '        Me.Close()
        '    Else
        '        GloBnd_Des_Men = False
        '        GloBnd_Des_Cont = False
        '        LocbndPolizaCiudad = False
        '        FrmFechaIngresosConcepto.Show()

        '    End If
        'ElseIf varfrmselcompania = "" And varfrmselsucursal = "relingreso" Then
        '    FrmSelSucursal.Show()
        '    Me.Close()
        'End If
        If varfrmselcompania = "relingresonuevo" Then
            FrmFechaIngresosConcepto.Show()
            Me.Close()
            EntradasSelCiudad = 0
            Exit Sub
        End If
        If varfrmselcompania = "BonificacionNuevo" Then
            FrmFiltroBonificacion.Show()
            Me.Close()
            EntradasSelCiudad = 0
            Exit Sub
        End If
        If varfrmselcompania = "CorteNuevo" Then
            FrmSelSucursal.Show()
            Me.Close()
            EntradasSelCiudad = 0
            Exit Sub
        End If
        If varfrmselcompania = "CorteNuevoPrimeraVersion" Then
            FrmSelSucursal.Show()
            Me.Close()
            EntradasSelCiudad = 0
            Exit Sub
        End If
        'If varfrmselcompania = "poliza" Then
        '    FrmFiltroPoliza.Show()
        '    Me.Close()
        '    EntradasSelCiudad = 0
        '    Exit Sub
        'End If
        FrmSelEstado.Show()
        Me.Close()
    End Sub
    Private Sub llevamealotro()
        'If varfrmselcompania = "cortefac" Then
        '    FrmOpRep.Show()
        'ElseIf varfrmselcompania = "relingreso" Then
        '    If GloBnd_Des_Men = True Then
        '        FrmFechaIngresosConcepto.Show()
        '        Me.Close()
        '    ElseIf LocbndPolizaCiudad = True Then
        '        FrmFechaIngresosConcepto.Show()
        '        Me.Close()
        '    Else
        '        GloBnd_Des_Men = False
        '        GloBnd_Des_Cont = False
        '        LocbndPolizaCiudad = False
        '        FrmFechaIngresosConcepto.Show()

        '    End If
        'ElseIf varfrmselcompania = "" And varfrmselsucursal = "relingreso" Then
        '    FrmSelSucursal.Show()
        '    Me.Close()
        'End If
        If varfrmselcompania = "relingresonuevo" Then
            FrmFechaIngresosConcepto.Show()
            Me.Close()
            EntradasSelCiudad = 0
            Exit Sub
        End If
        If varfrmselcompania = "BonificacionNuevo" Then
            FrmFiltroBonificacion.Show()
            EntradasSelCiudad = 0
            Me.Close()
            Exit Sub
        End If
        If varfrmselcompania = "CorteNuevo" Then
            FrmSelSucursal.Show()
            Me.Close()
            EntradasSelCiudad = 0
            Exit Sub
        End If
        If varfrmselcompania = "CorteNuevoPrimeraVersion" Then
            FrmSelSucursal.Show()
            Me.Close()
            EntradasSelCiudad = 0
            Exit Sub
        End If
        'If varfrmselcompania = "poliza" Then
        '    FrmFiltroPoliza.Show()
        '    Me.Close()
        '    EntradasSelCiudad = 0
        '    Exit Sub
        'End If
        FrmSelEstado.Show()
        Me.Close()
    End Sub
End Class