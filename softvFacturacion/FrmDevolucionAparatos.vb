﻿
Imports System.Windows.Forms
Imports System.Data.SqlClient
Imports System.Text

Public Class FrmDevolucionAparatos

    Private Sub SP_GrabaDevolucionAparatos(oID As Long, oTipo As String, oRecibi As Integer, oClv_Session As Long, oClv_Detalle As Long, oSeleccion As Integer)
        '@Id Bigint,@Tipo varchar(50),@Recibi bit,@Clv_Session bigint,@Clv_Detalle bigint
        Try
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@Id", SqlDbType.BigInt, oID)
            BaseII.CreateMyParameter("@Tipo", SqlDbType.VarChar, oTipo, 50)
            BaseII.CreateMyParameter("@Recibi", SqlDbType.Bit, oRecibi)
            BaseII.CreateMyParameter("@Clv_Session", SqlDbType.BigInt, oClv_Session)
            BaseII.CreateMyParameter("@Clv_Detalle", SqlDbType.BigInt, 0)
            BaseII.CreateMyParameter("@Seleccion", SqlDbType.BigInt, oSeleccion)
            BaseII.Inserta("SP_GrabaDevolucionAparatos")
        Catch ex As Exception

        End Try
    End Sub

    Private Sub GrabaDetallegrid(oClv_Session As Long, oClv_Detalle As Long)
        Try
            'Dim oId As Long = 0
            'Dim oTipo As String = ""
            'Dim Fila As Integer = 0
            'For Fila = 0 To DGdVSeleccion.RowCount - 1
            '    oId = 0
            '    oTipo = ""
            '    oId = DGdVSeleccion.Item(1, Fila).Value
            '    oTipo = DGdVSeleccion.Item(4, Fila).Value
            '    If DGdVSeleccion.Item(3, Fila).Value = 1 Or DGdVSeleccion.Item(3, Fila).Value = True Then
            '        SP_GrabaDevolucionAparatos(oId, oTipo, 1, oClv_Session, oClv_Detalle)
            '    Else
            '        SP_GrabaDevolucionAparatos(oId, oTipo, 0, oClv_Session, oClv_Detalle)
            '    End If
            'Next
            Dim recibi As Integer = 0
            Dim seleccion As Integer = 0
            For Each row As DataGridViewRow In DGdVSeleccion.Rows
                If DGdVSeleccion.Rows(row.Index).Cells("Recibi").Value = True Then
                    recibi = 1
                Else
                    recibi = 0
                End If
                If DGdVSeleccion.Rows(row.Index).Cells("NoEntregado").Value = True Then
                    seleccion = 1
                Else
                    seleccion = 0
                End If
                SP_GrabaDevolucionAparatos(DGdVSeleccion.Rows(row.Index).Cells("Id").Value, DGdVSeleccion.Rows(row.Index).Cells("Tipo").Value, recibi, oClv_Session, 0, seleccion)
            Next
            MsgBox("Se grabó correctamente", MsgBoxStyle.Information, "Información")
        Catch ex As Exception

        End Try
    End Sub

    Private Sub Llena_Detalle(oClv_Session As Long, oClv_Detallle As Long, oContrato As Long)
        '@Clv_Session bigint,@Clv_Detalle bigint,@Contrato bigint,@Clv_Usuario VARCHAR(15)
        'Vamos a llenarlos todos, pero solo para seleccionar los que sí 
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@Clv_Session", SqlDbType.Int, oClv_Session)
        BaseII.CreateMyParameter("@Clv_Detalle", SqlDbType.Int, oClv_Detallle)
        BaseII.CreateMyParameter("@Contrato", SqlDbType.Int, oContrato)
        BaseII.CreateMyParameter("@CancelarTodo", SqlDbType.Bit, 1)
        DGdVSeleccion.DataSource = BaseII.ConsultaDT("SP_MuestraAparatosCobroAdeudo")
        'If DGdV.RowCount = 0 Then Exit Sub

    End Sub

    ''' <summary>
    ''' Llena los aparatos que se van seleccionar para aplicar cobro de adeudo
    ''' </summary>
    ''' <param name="oClv_Session"></param>
    ''' <param name="oClv_Detallle"></param>
    ''' <param name="oContrato"></param>
    ''' <remarks></remarks>
    Private Sub LLenaAparatosSeleccionar(oClv_Session As Long, oClv_Detallle As Long, oContrato As Long)
        '@Clv_Session bigint,@Clv_Detalle bigint,@Contrato bigint,@Clv_Usuario VARCHAR(15)
        'Vamos a llenarlos todos, pero solo para seleccionar los que sí 
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@Clv_Session", SqlDbType.Int, oClv_Session)
        BaseII.CreateMyParameter("@Clv_Detalle", SqlDbType.Int, oClv_Detallle)
        BaseII.CreateMyParameter("@Contrato", SqlDbType.Int, oContrato)
        BaseII.CreateMyParameter("@CancelarTodo", SqlDbType.Bit, 1)
        DGdVSeleccion.DataSource = BaseII.ConsultaDT("LlenaAparatosCobroAdeudo")
        'If DGdV.RowCount = 0 Then Exit Sub

    End Sub

    Function ValidaGrid()
        Dim valido As Boolean = False
        For Each row As DataGridViewRow In DGdVSeleccion.Rows
            DGdVSeleccion.Rows(row.Index).Cells("NoEntregado").Value = True
            If DGdVSeleccion.Rows(row.Index).Cells("NoEntregado").Value = True Then
                valido = True
            End If
        Next
        Return valido
    End Function

    Private Sub ValidaAntenaLnb()
        Dim aparatosd As Integer = 0
        Dim aparatosi As Integer = 0
        Dim recibidosd As Integer = 0
        Dim recibidosi As Integer = 0
        Try
            For Each row As DataGridViewRow In DGdVSeleccion.Rows
                If DGdVSeleccion.Rows(row.Index).Cells("Tipo").Value = "Aparato" Then
                    If DGdVSeleccion.Rows(row.Index).Cells("TipoServicio").Value = 2 Then
                        aparatosi = aparatosi + 1
                        If DGdVSeleccion.Rows(row.Index).Cells("NoEntregado").Value = True Then
                            recibidosi = recibidosi + 1
                        End If
                    End If
                    If DGdVSeleccion.Rows(row.Index).Cells("TipoServicio").Value = 3 Then
                        aparatosd = aparatosd + 1
                        If DGdVSeleccion.Rows(row.Index).Cells("NoEntregado").Value = True Then
                            recibidosd = recibidosd + 1
                        End If
                    End If
                End If
            Next
            If aparatosi <> recibidosi Then
                BaseII.limpiaParametros()
                BaseII.CreateMyParameter("@clv_session", SqlDbType.BigInt, FrmFAC.Clv_Session.Text)
                BaseII.CreateMyParameter("@Clv_TipSer", SqlDbType.Int, 2)
                BaseII.Inserta("QuitaAntenaDevolucion")
            End If
            If aparatosd <> recibidosd Then
                BaseII.limpiaParametros()
                BaseII.CreateMyParameter("@clv_session", SqlDbType.BigInt, FrmFAC.Clv_Session.Text)
                BaseII.CreateMyParameter("@Clv_TipSer", SqlDbType.Int, 3)
                BaseII.Inserta("QuitaAntenaDevolucion")
            End If
        Catch ex As Exception

        End Try
    End Sub

    Private Sub BnAceptar_Click(sender As System.Object, e As System.EventArgs) Handles BnAceptar.Click
        If Not ValidaGrid() Then
            MsgBox("Debe seleccionar al menos un aparato para aplicar el cobro de adeudo")
            Exit Sub
        End If
        'Vamos a checar si dejamos el lnb y la antena
        ValidaAntenaLnb()
        GrabaDetallegrid(FrmFAC.Clv_Session.Text, FrmFAC.CLV_DETALLETextBox.Text)
        CancelaDevolucion = False
        Me.Close()
    End Sub

   
    Private Sub FrmDevolucionAparatos_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        'colorea(Me)
        Panel1.BackColor = FrmFAC.BackColor
        'LLenaAparatosSeleccionar(FrmFAC.Clv_Session.Text, FrmFAC.CLV_DETALLETextBox.Text, GloContrato)
        Llena_Detalle(FrmFAC.Clv_Session.Text, FrmFAC.CLV_DETALLETextBox.Text, GloContrato)
    End Sub

    Private Sub CMBLabel12_Click(sender As Object, e As EventArgs) Handles CMBLabel12.Click

    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        CancelaDevolucion = True
        Me.Close()
    End Sub

    Private Sub DGdVSeleccion_CellClick(sender As Object, e As DataGridViewCellEventArgs) Handles DGdVSeleccion.CellClick
        'Try
        '    For Each row As DataGridViewRow In DGdVSeleccion.Rows
        '        If row.Cells(3).Value.ToString = "True" Then
        '            BaseII.limpiaParametros()
        '            BaseII.CreateMyParameter("@id", SqlDbType.Int, row.Cells(2).Value)
        '            MsgBox("Activado" + row.Cells(2).Value.ToString)
        '        End If
        '    Next

        'Catch ex As Exception

        'End Try
    End Sub

    Private Sub DGdVSeleccion_CellContentClick(sender As Object, e As DataGridViewCellEventArgs) Handles DGdVSeleccion.CellContentClick
        'Try
        '    For Each row As DataGridViewRow In DGdVSeleccion.Rows
        '        If row.Cells(3).Value.ToString = "True" Then
        '            BaseII.limpiaParametros()
        '            BaseII.CreateMyParameter("@id", SqlDbType.Int, row.Cells(2).Value)
        '            MsgBox("Activado" + row.Cells(2).Value.ToString)
        '        End If
        '    Next

        'Catch ex As Exception

        'End Try
    End Sub

    Private Sub DGdVSeleccion_DataSourceChanged(sender As Object, e As EventArgs) Handles DGdVSeleccion.DataSourceChanged
        'Try
        '    For Each row As DataGridViewRow In DGdVSeleccion.Rows
        '        If row.Cells(3).Value.ToString = "True" Then
        '            BaseII.limpiaParametros()
        '            BaseII.CreateMyParameter("@id", SqlDbType.Int, row.Cells(2).Value)
        '            MsgBox("Activado" + row.Cells(2).Value.ToString)
        '        End If
        '    Next

        'Catch ex As Exception

        'End Try
    End Sub

    Private Sub DGdVSeleccion_CellContentClick_1(sender As Object, e As DataGridViewCellEventArgs)
        'DGdVSeleccion.UpdateCellValue(3, DGdVSeleccion.SelectedCells(0).RowIndex)
    End Sub

    Private Sub DGdVSeleccion_CellValueChanged(sender As Object, e As DataGridViewCellEventArgs) Handles DGdVSeleccion.CellValueChanged
        'Try
        '    For Each row As DataGridViewRow In DGdVSeleccion.Rows
        '        If row.Cells(3).Value.ToString = "True" Then
        '            BaseII.limpiaParametros()
        '            BaseII.CreateMyParameter("@id", SqlDbType.Int, row.Cells(2).Value)
        '            MsgBox("Activado" + row.Cells(2).Value.ToString)
        '        End If
        '    Next

        'Catch ex As Exception

        'End Try
    End Sub

    Private Sub DGdVSeleccion_Click(sender As Object, e As EventArgs) Handles DGdVSeleccion.Click
        'DGdVSeleccion.UpdateCellValue(DGdVSeleccion.CurrentCellAddress.X, DGdVSeleccion.CurrentCellAddress.Y)
        'DGdVSeleccion.CommitEdit(DataGridViewDataErrorContexts.Commit)
        'DGdVSeleccion.EndEdit()
        'DGdVSeleccion.BeginEdit(False)
        'DGdVSeleccion.EndEdit()
        'DGdVSeleccion.RefreshEdit()
        'DGdVSeleccion.CurrentCell.Dispose()
        'DGdVSeleccion.CurrentCell.DetachEditingControl()
    End Sub
    Sub dataGridView1_CurrentCellDirtyStateChanged(ByVal sender As Object, ByVal e As EventArgs) Handles DGdVSeleccion.CurrentCellDirtyStateChanged

        If DGdVSeleccion.IsCurrentCellDirty Then
            DGdVSeleccion.CommitEdit(DataGridViewDataErrorContexts.Commit)
        End If
    End Sub

    ' If a check box cell is clicked, this event handler disables  
    ' or enables the button in the same row as the clicked cell.
    Public Sub dataGridView1_CellValueChanged(ByVal sender As Object, ByVal e As DataGridViewCellEventArgs) Handles DGdVSeleccion.CellValueChanged

        If DGdVSeleccion.Columns(e.ColumnIndex).Name = "NoEntregado" Then

            Dim checkCell As DataGridViewCheckBoxCell = CType(DGdVSeleccion.Rows(e.RowIndex).Cells("NoEntregado"), DataGridViewCheckBoxCell)
            If CType(checkCell.Value, [Boolean]) = True Then
                Try
                    Dim ContratoNet As Integer = DGdVSeleccion.Rows(e.RowIndex).Cells("ContratoNet").Value
                    For Each row As DataGridViewRow In DGdVSeleccion.Rows
                        If DGdVSeleccion.Rows(row.Index).Cells("ContratoNet").Value = ContratoNet Then
                            DGdVSeleccion.Rows(row.Index).Cells("NoEntregado").Value = True
                            DGdVSeleccion.Rows(row.Index).Cells("Recibi").ReadOnly = False
                        End If
                    Next
                    'DGdVSeleccion.Rows(e.RowIndex).Cells("Recibi").ReadOnly = False
                Catch ex As Exception

                End Try
            End If
            If CType(checkCell.Value, [Boolean]) = False Then
                Try
                    Dim ContratoNet As Integer = DGdVSeleccion.Rows(e.RowIndex).Cells("ContratoNet").Value
                    For Each row As DataGridViewRow In DGdVSeleccion.Rows
                        If DGdVSeleccion.Rows(row.Index).Cells("ContratoNet").Value = ContratoNet Then
                            DGdVSeleccion.Rows(row.Index).Cells("NoEntregado").Value = False
                            DGdVSeleccion.Rows(row.Index).Cells("Recibi").ReadOnly = True
                            DGdVSeleccion.Rows(row.Index).Cells("Recibi").Value = False
                        End If
                    Next
                    'DGdVSeleccion.Rows(e.RowIndex).Cells("Recibi").ReadOnly = False
                Catch ex As Exception

                End Try
            End If

            DGdVSeleccion.Invalidate()
        End If
    End Sub

   
End Class