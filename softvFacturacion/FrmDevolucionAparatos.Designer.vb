﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmDevolucionAparatos
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle4 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle5 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.CMBLabel12 = New System.Windows.Forms.Label()
        Me.CMBLabel1 = New System.Windows.Forms.Label()
        Me.BnAceptar = New System.Windows.Forms.Button()
        Me.DGdVSeleccion = New System.Windows.Forms.DataGridView()
        Me.Clv = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Id = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Detalle = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.NoEntregado = New System.Windows.Forms.DataGridViewCheckBoxColumn()
        Me.Recibi = New System.Windows.Forms.DataGridViewCheckBoxColumn()
        Me.Tipo = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ContratoNet = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.TipoServicio = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Panel1.SuspendLayout()
        CType(Me.DGdVSeleccion, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Panel1
        '
        Me.Panel1.BackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.Panel1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.Panel1.Controls.Add(Me.Label2)
        Me.Panel1.Controls.Add(Me.Label1)
        Me.Panel1.Controls.Add(Me.Button1)
        Me.Panel1.Controls.Add(Me.CMBLabel12)
        Me.Panel1.Controls.Add(Me.CMBLabel1)
        Me.Panel1.Controls.Add(Me.BnAceptar)
        Me.Panel1.Controls.Add(Me.DGdVSeleccion)
        Me.Panel1.Location = New System.Drawing.Point(0, 0)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(866, 594)
        Me.Panel1.TabIndex = 0
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.ForeColor = System.Drawing.Color.Red
        Me.Label2.Location = New System.Drawing.Point(41, 54)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(612, 13)
        Me.Label2.TabIndex = 19
        Me.Label2.Text = "Marque el aparato al que desea aplicar el cobro de adeudo y en seguida los aparat" & _
    "os y/o accesorios que trae consigo el cliente."
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(12, 32)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(145, 16)
        Me.Label1.TabIndex = 18
        Me.Label1.Text = "Aparatos de Cliente"
        '
        'Button1
        '
        Me.Button1.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button1.Location = New System.Drawing.Point(701, 549)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(135, 36)
        Me.Button1.TabIndex = 17
        Me.Button1.Text = "&CANCELAR"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'CMBLabel12
        '
        Me.CMBLabel12.BackColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.CMBLabel12.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBLabel12.ForeColor = System.Drawing.Color.White
        Me.CMBLabel12.Location = New System.Drawing.Point(0, 0)
        Me.CMBLabel12.Name = "CMBLabel12"
        Me.CMBLabel12.Size = New System.Drawing.Size(864, 25)
        Me.CMBLabel12.TabIndex = 16
        Me.CMBLabel12.Text = "Devolución de Aparatos  "
        Me.CMBLabel12.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'CMBLabel1
        '
        Me.CMBLabel1.AutoSize = True
        Me.CMBLabel1.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBLabel1.ForeColor = System.Drawing.Color.Red
        Me.CMBLabel1.Location = New System.Drawing.Point(12, 557)
        Me.CMBLabel1.Name = "CMBLabel1"
        Me.CMBLabel1.Size = New System.Drawing.Size(371, 18)
        Me.CMBLabel1.TabIndex = 15
        Me.CMBLabel1.Text = "Nota : Marque los aparatos que está recibiendo "
        Me.CMBLabel1.Visible = False
        '
        'BnAceptar
        '
        Me.BnAceptar.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BnAceptar.Location = New System.Drawing.Point(571, 549)
        Me.BnAceptar.Name = "BnAceptar"
        Me.BnAceptar.Size = New System.Drawing.Size(124, 36)
        Me.BnAceptar.TabIndex = 14
        Me.BnAceptar.Text = "&ACEPTAR"
        Me.BnAceptar.UseVisualStyleBackColor = True
        '
        'DGdVSeleccion
        '
        Me.DGdVSeleccion.AllowUserToAddRows = False
        Me.DGdVSeleccion.AllowUserToDeleteRows = False
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.DGdVSeleccion.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle1
        Me.DGdVSeleccion.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DGdVSeleccion.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.Clv, Me.Id, Me.Detalle, Me.NoEntregado, Me.Recibi, Me.Tipo, Me.ContratoNet, Me.TipoServicio})
        DataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle4.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.DGdVSeleccion.DefaultCellStyle = DataGridViewCellStyle4
        Me.DGdVSeleccion.Location = New System.Drawing.Point(15, 73)
        Me.DGdVSeleccion.Name = "DGdVSeleccion"
        DataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle5.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle5.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle5.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle5.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle5.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.DGdVSeleccion.RowHeadersDefaultCellStyle = DataGridViewCellStyle5
        Me.DGdVSeleccion.Size = New System.Drawing.Size(821, 464)
        Me.DGdVSeleccion.TabIndex = 0
        '
        'Clv
        '
        Me.Clv.DataPropertyName = "Clv"
        Me.Clv.HeaderText = "Clv"
        Me.Clv.Name = "Clv"
        Me.Clv.ReadOnly = True
        Me.Clv.Visible = False
        '
        'Id
        '
        Me.Id.DataPropertyName = "Id"
        Me.Id.HeaderText = "Id"
        Me.Id.Name = "Id"
        Me.Id.ReadOnly = True
        Me.Id.Visible = False
        '
        'Detalle
        '
        Me.Detalle.DataPropertyName = "Detalle"
        DataGridViewCellStyle2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Detalle.DefaultCellStyle = DataGridViewCellStyle2
        Me.Detalle.HeaderText = "Conceptos"
        Me.Detalle.Name = "Detalle"
        Me.Detalle.ReadOnly = True
        Me.Detalle.Width = 500
        '
        'NoEntregado
        '
        Me.NoEntregado.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.NoEntregado.HeaderText = "Aparatos a Cancelar"
        Me.NoEntregado.Name = "NoEntregado"
        Me.NoEntregado.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.NoEntregado.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic
        Me.NoEntregado.Visible = False
        '
        'Recibi
        '
        Me.Recibi.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.Recibi.DataPropertyName = "Recibi"
        Me.Recibi.HeaderText = "Aparatos a Recibir"
        Me.Recibi.Name = "Recibi"
        Me.Recibi.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.Recibi.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic
        '
        'Tipo
        '
        Me.Tipo.DataPropertyName = "Tipo"
        DataGridViewCellStyle3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Tipo.DefaultCellStyle = DataGridViewCellStyle3
        Me.Tipo.HeaderText = "Tipo"
        Me.Tipo.Name = "Tipo"
        Me.Tipo.ReadOnly = True
        Me.Tipo.Visible = False
        Me.Tipo.Width = 150
        '
        'ContratoNet
        '
        Me.ContratoNet.DataPropertyName = "ContratoNet"
        Me.ContratoNet.HeaderText = "ContratoNet"
        Me.ContratoNet.Name = "ContratoNet"
        Me.ContratoNet.Visible = False
        '
        'TipoServicio
        '
        Me.TipoServicio.DataPropertyName = "TipoServicio"
        Me.TipoServicio.HeaderText = "TipoServicio"
        Me.TipoServicio.Name = "TipoServicio"
        Me.TipoServicio.Visible = False
        '
        'FrmDevolucionAparatos
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.AutoScroll = True
        Me.BackColor = System.Drawing.Color.Gray
        Me.ClientSize = New System.Drawing.Size(867, 595)
        Me.Controls.Add(Me.Panel1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "FrmDevolucionAparatos"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Aparatos a Recibir "
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        CType(Me.DGdVSeleccion, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents DGdVSeleccion As System.Windows.Forms.DataGridView
    Friend WithEvents CMBLabel1 As System.Windows.Forms.Label
    Friend WithEvents BnAceptar As System.Windows.Forms.Button
    Friend WithEvents CMBLabel12 As System.Windows.Forms.Label
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Clv As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Id As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Detalle As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents NoEntregado As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents Recibi As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents Tipo As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ContratoNet As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents TipoServicio As System.Windows.Forms.DataGridViewTextBoxColumn
End Class
