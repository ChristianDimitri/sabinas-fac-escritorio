﻿Imports System.Data.SqlClient
Module ModuloFitrosXML

    Public Sub CargarElementosSucursalEspecialXML()
        Dim CONar As New SqlConnection(MiConexion)
        Dim cmd As New SqlCommand()
        Try
            cmd = New SqlCommand()
            CONar.Open()
            '@Contrato bigint,@IdCompania int,@ContratoCompania bigint
            With cmd
                .CommandText = "ProcedureSeleccionSucursalEspecialXml"
                .Connection = CONar
                .CommandTimeout = 0
                .CommandType = CommandType.StoredProcedure

                Dim prm1 As New SqlParameter("@SucursalesXml", SqlDbType.Xml)
                prm1.Direction = ParameterDirection.Output
                prm1.Value = 0
                .Parameters.Add(prm1)

                Dim ia As Integer = .ExecuteNonQuery()
                DocSucursales.LoadXml(prm1.Value)

            End With
            CONar.Close()
        Catch ex As Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub

    Public Sub CargarElementosSucursalXML()
        Dim CONar As New SqlConnection(MiConexion)
        Dim cmd As New SqlCommand()
        Try
            cmd = New SqlCommand()
            CONar.Open()
            '@Contrato bigint,@IdCompania int,@ContratoCompania bigint
            With cmd
                .CommandText = "ProcedureSeleccionSucursalXml"
                .Connection = CONar
                .CommandTimeout = 0
                .CommandType = CommandType.StoredProcedure

                Dim prm1 As New SqlParameter("@SucursalesXml", SqlDbType.Xml)
                prm1.Direction = ParameterDirection.Output
                prm1.Value = 0
                .Parameters.Add(prm1)

                Dim prm2 As New SqlParameter("@CompaniasXml", SqlDbType.Xml)
                prm2.Direction = ParameterDirection.Input
                prm2.Value = DocCompanias.InnerXml
                .Parameters.Add(prm2)

                Dim ia As Integer = .ExecuteNonQuery()
                DocSucursales.LoadXml(prm1.Value)

            End With
            CONar.Close()
        Catch ex As Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub

    Public Sub CargarElementosTecnicoXML()
        Dim CONar As New SqlConnection(MiConexion)
        Dim cmd As New SqlCommand()
        Try
            cmd = New SqlCommand()
            CONar.Open()
            '@Contrato bigint,@IdCompania int,@ContratoCompania bigint
            With cmd
                .CommandText = "ProcedureSeleccionTecnicosXml"
                .Connection = CONar
                .CommandTimeout = 0
                .CommandType = CommandType.StoredProcedure

                Dim prm As New SqlParameter("@clv_usuario", SqlDbType.Int)
                prm.Direction = ParameterDirection.Input
                prm.Value = GloClvUsuario
                .Parameters.Add(prm)

                Dim prm1 As New SqlParameter("@TecnicosXML", SqlDbType.Xml)
                prm1.Direction = ParameterDirection.Output
                prm1.Value = 0
                .Parameters.Add(prm1)

                Dim prm2 As New SqlParameter("@CompaniasXml", SqlDbType.Xml)
                prm2.Direction = ParameterDirection.Input
                prm2.Value = DocCompanias.InnerXml
                .Parameters.Add(prm2)

                Dim ia As Integer = .ExecuteNonQuery()
                DocTecnicos.LoadXml(prm1.Value)

            End With
            CONar.Close()
        Catch ex As Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub

    Public Sub CargarElementosPlazaXML()
        Dim CONar As New SqlConnection(MiConexion)
        Dim cmd As New SqlCommand()
        Try
            cmd = New SqlCommand()
            CONar.Open()
            '@Contrato bigint,@IdCompania int,@ContratoCompania bigint
            With cmd
                .CommandText = "ProcedureSeleccionPlazaXML"
                .Connection = CONar
                .CommandTimeout = 0
                .CommandType = CommandType.StoredProcedure

                Dim prm As New SqlParameter("@clv_usuario", SqlDbType.Int)
                prm.Direction = ParameterDirection.Input
                prm.Value = GloClvUsuario
                .Parameters.Add(prm)

                Dim prm1 As New SqlParameter("@PlazasXML", SqlDbType.Xml)
                prm1.Direction = ParameterDirection.Output
                prm1.Value = 0
                .Parameters.Add(prm1)

                Dim ia As Integer = .ExecuteNonQuery()
                DocPlazas.LoadXml(prm1.Value)

            End With
            CONar.Close()
        Catch ex As Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub
    'CargarElementosProblemaXML()
    Public Sub CargarElementosProblemaXML()
        Dim CONar As New SqlConnection(MiConexion)
        Dim cmd As New SqlCommand()
        Try
            cmd = New SqlCommand()
            CONar.Open()
            '@Contrato bigint,@IdCompania int,@ContratoCompania bigint
            With cmd
                .CommandText = "ProcedureSeleccionProblemaXML"
                .Connection = CONar
                .CommandTimeout = 0
                .CommandType = CommandType.StoredProcedure

                Dim prm1 As New SqlParameter("@ConceptosXML", SqlDbType.Xml)
                prm1.Direction = ParameterDirection.Output
                prm1.Value = 0
                .Parameters.Add(prm1)

                Dim ia As Integer = .ExecuteNonQuery()
                DocConceptos.LoadXml(prm1.Value)

            End With
            CONar.Close()
        Catch ex As Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub
    Public Sub CargarElementosTrabajoXML()
        Dim CONar As New SqlConnection(MiConexion)
        Dim cmd As New SqlCommand()
        Try
            cmd = New SqlCommand()
            CONar.Open()
            '@Contrato bigint,@IdCompania int,@ContratoCompania bigint
            With cmd
                .CommandText = "ProcedureSeleccionTrabajoXML"
                .Connection = CONar
                .CommandTimeout = 0
                .CommandType = CommandType.StoredProcedure

                Dim prm1 As New SqlParameter("@ConceptosXML", SqlDbType.Xml)
                prm1.Direction = ParameterDirection.Output
                prm1.Value = 0
                .Parameters.Add(prm1)

                Dim ia As Integer = .ExecuteNonQuery()
                DocConceptos.LoadXml(prm1.Value)

            End With
            CONar.Close()
        Catch ex As Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub

    Public Sub CargarElementosCompaniaXML()
        'DocCompanias.DocumentElement.RemoveAll()
        Dim CONar As New SqlConnection(MiConexion)
        Dim cmd As New SqlCommand()
        Try
            cmd = New SqlCommand()
            CONar.Open()
            With cmd
                .CommandText = "ProcedureSeleccionCompaniaXML"
                .Connection = CONar
                .CommandTimeout = 0
                .CommandType = CommandType.StoredProcedure

                Dim prm As New SqlParameter("@clv_usuario", SqlDbType.Int)
                prm.Direction = ParameterDirection.Input
                prm.Value = GloClvUsuario
                .Parameters.Add(prm)

                Dim prm1 As New SqlParameter("@xmlCompania", SqlDbType.Xml)
                prm1.Direction = ParameterDirection.Output
                prm1.Value = 0
                .Parameters.Add(prm1)

                Dim prm2 As New SqlParameter("@xmlPlaza", SqlDbType.Xml)
                prm2.Direction = ParameterDirection.Input
                prm2.Value = DocPlazas.InnerXml
                .Parameters.Add(prm2)


                Dim ia As Integer = .ExecuteNonQuery()
                DocCompanias.LoadXml(prm1.Value)

            End With

            CONar.Close()
        Catch ex As Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub

    Public Sub CargarElementosEstadosXML()
        'Por si es uno
        Dim CONar As New SqlConnection(MiConexion)
        Dim cmd As New SqlCommand()
        Try
            cmd = New SqlCommand()
            CONar.Open()
            '@Contrato bigint,@IdCompania int,@ContratoCompania bigint
            With cmd
                .CommandText = "ProcedureSeleccionEstadoXML"
                .Connection = CONar
                .CommandTimeout = 0
                .CommandType = CommandType.StoredProcedure

                Dim prm1 As New SqlParameter("@EstadosXML", SqlDbType.Xml)
                prm1.Direction = ParameterDirection.Output
                prm1.Value = 0
                .Parameters.Add(prm1)

                Dim prm2 As New SqlParameter("@CompaniasXML", SqlDbType.Xml)
                prm2.Direction = ParameterDirection.Input
                prm2.Value = DocCompanias.InnerXml
                .Parameters.Add(prm2)

                Dim ia As Integer = .ExecuteNonQuery()
                DocEstados.LoadXml(prm1.Value)

            End With
            CONar.Close()
        Catch ex As Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub

    Public Sub CargarElementosCiudadesXML()
        'Por si es uno
        Dim CONar As New SqlConnection(MiConexion)
        Dim cmd As New SqlCommand()
        Try
            cmd = New SqlCommand()
            CONar.Open()
            '@Contrato bigint,@IdCompania int,@ContratoCompania bigint
            With cmd
                .CommandText = "ProcedureSeleccionCiudadXML"
                .Connection = CONar
                .CommandTimeout = 0
                .CommandType = CommandType.StoredProcedure

                Dim prm1 As New SqlParameter("@CiudadesXML", SqlDbType.Xml)
                prm1.Direction = ParameterDirection.Output
                prm1.Value = 0
                .Parameters.Add(prm1)

                Dim prm2 As New SqlParameter("@CompaniasXML", SqlDbType.Xml)
                prm2.Direction = ParameterDirection.Input
                prm2.Value = DocCompanias.InnerXml
                .Parameters.Add(prm2)

                Dim prm3 As New SqlParameter("@EstadosXML", SqlDbType.Xml)
                prm3.Direction = ParameterDirection.Input
                prm3.Value = DocEstados.InnerXml
                .Parameters.Add(prm3)

                Dim ia As Integer = .ExecuteNonQuery()
                DocCiudades.LoadXml(prm1.Value)

            End With
            CONar.Close()
        Catch ex As Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub

    Public Sub CargarElementosLocalidadesXML()
        'Por si es uno
        Dim CONar As New SqlConnection(MiConexion)
        Dim cmd As New SqlCommand()
        Try
            cmd = New SqlCommand()
            CONar.Open()
            '@Contrato bigint,@IdCompania int,@ContratoCompania bigint
            With cmd
                .CommandText = "ProcedureSeleccionLocalidadXML"
                .Connection = CONar
                .CommandTimeout = 0
                .CommandType = CommandType.StoredProcedure

                Dim prm As New SqlParameter("@clv_usuario", SqlDbType.Int)
                prm.Direction = ParameterDirection.Input
                prm.Value = GloClvUsuario
                .Parameters.Add(prm)

                Dim prm1 As New SqlParameter("@LocalidadesXML", SqlDbType.Xml)
                prm1.Direction = ParameterDirection.Output
                prm1.Value = 0
                .Parameters.Add(prm1)

                Dim prm3 As New SqlParameter("@CiudadesXML", SqlDbType.Xml)
                prm3.Direction = ParameterDirection.Input
                prm3.Value = DocCiudades.InnerXml
                .Parameters.Add(prm3)

                Dim ia As Integer = .ExecuteNonQuery()
                DocLocalidades.LoadXml(prm1.Value)

            End With
            CONar.Close()
        Catch ex As Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub

    Public Sub CargarElementosColoniasXML()
        'Por si es uno
        Dim CONar As New SqlConnection(MiConexion)
        Dim cmd As New SqlCommand()
        Try
            cmd = New SqlCommand()
            CONar.Open()
            '@Contrato bigint,@IdCompania int,@ContratoCompania bigint
            With cmd
                .CommandText = "ProcedureSeleccionColoniaXML"
                .Connection = CONar
                .CommandTimeout = 0
                .CommandType = CommandType.StoredProcedure

                Dim prm1 As New SqlParameter("@ColoniasXML", SqlDbType.Xml)
                prm1.Direction = ParameterDirection.Output
                prm1.Value = 0
                .Parameters.Add(prm1)

                Dim prm3 As New SqlParameter("@LocalidadesXML", SqlDbType.Xml)
                prm3.Direction = ParameterDirection.Input
                prm3.Value = DocLocalidades.InnerXml
                .Parameters.Add(prm3)

                Dim ia As Integer = .ExecuteNonQuery()
                DocColonias.LoadXml(prm1.Value)

            End With
            CONar.Close()
        Catch ex As Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub

    Public Sub CargarElementosCallesXML()
        'Por si es uno
        Dim CONar As New SqlConnection(MiConexion)
        Dim cmd As New SqlCommand()
        Try
            cmd = New SqlCommand()
            CONar.Open()
            '@Contrato bigint,@IdCompania int,@ContratoCompania bigint
            With cmd
                .CommandText = "ProcedureSeleccionCalleXML"
                .Connection = CONar
                .CommandTimeout = 0
                .CommandType = CommandType.StoredProcedure

                Dim prm1 As New SqlParameter("@CallesXML", SqlDbType.Xml)
                prm1.Direction = ParameterDirection.Output
                prm1.Value = 0
                .Parameters.Add(prm1)

                Dim prm3 As New SqlParameter("@ColoniasXML", SqlDbType.Xml)
                prm3.Direction = ParameterDirection.Input
                prm3.Value = DocColonias.InnerXml
                .Parameters.Add(prm3)

                Dim ia As Integer = .ExecuteNonQuery()
                DocCalles.LoadXml(prm1.Value)

            End With
            CONar.Close()
        Catch ex As Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub


    Public Sub CargarElementosTipoClienteXML()
        'Por si es uno
        Dim CONar As New SqlConnection(MiConexion)
        Dim cmd As New SqlCommand()
        Try
            cmd = New SqlCommand()
            CONar.Open()
            '@Contrato bigint,@IdCompania int,@ContratoCompania bigint
            With cmd
                .CommandText = "ProcedureSeleccionTipoClienteXml"
                .Connection = CONar
                .CommandTimeout = 0
                .CommandType = CommandType.StoredProcedure

                Dim prm1 As New SqlParameter("@TipoClientesXML", SqlDbType.Xml)
                prm1.Direction = ParameterDirection.Output
                prm1.Value = 0
                .Parameters.Add(prm1)

                Dim ia As Integer = .ExecuteNonQuery()
                DocTipoClientes.LoadXml(prm1.Value)

            End With
            CONar.Close()
        Catch ex As Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub

    'Public Sub CargarElementosServiciosXML()
    '    'Por si es uno
    '    Dim CONar As New SqlConnection(MiConexion)
    '    Dim cmd As New SqlCommand()
    '    Try
    '        cmd = New SqlCommand()
    '        CONar.Open()
    '        '@Contrato bigint,@IdCompania int,@ContratoCompania bigint
    '        With cmd
    '            .CommandText = "ProcedureSeleccionServicioXml"
    '            .Connection = CONar
    '            .CommandTimeout = 0
    '            .CommandType = CommandType.StoredProcedure

    '            Dim prm1 As New SqlParameter("@ServiciosXML", SqlDbType.Xml)
    '            prm1.Direction = ParameterDirection.Output
    '            prm1.Value = 0
    '            .Parameters.Add(prm1)

    '            Dim prm3 As New SqlParameter("@Clv_TipSer", SqlDbType.Int)
    '            prm3.Direction = ParameterDirection.Input
    '            prm3.Value = GloClv_tipser2
    '            .Parameters.Add(prm3)

    '            Dim ia As Integer = .ExecuteNonQuery()
    '            DocServicios.LoadXml(prm1.Value)

    '        End With
    '        CONar.Close()
    '    Catch ex As Exception
    '        System.Windows.Forms.MessageBox.Show(ex.Message)
    '    End Try
    'End Sub

    Public Sub CargarElementosPeriodosXML()
        'Por si es uno
        Dim CONar As New SqlConnection(MiConexion)
        Dim cmd As New SqlCommand()
        Try
            cmd = New SqlCommand()
            CONar.Open()
            '@Contrato bigint,@IdCompania int,@ContratoCompania bigint
            With cmd
                .CommandText = "ProcedureSeleccionPeriodoXml"
                .Connection = CONar
                .CommandTimeout = 0
                .CommandType = CommandType.StoredProcedure

                Dim prm1 As New SqlParameter("@PeriodosXML", SqlDbType.Xml)
                prm1.Direction = ParameterDirection.Output
                prm1.Value = 0
                .Parameters.Add(prm1)

                Dim ia As Integer = .ExecuteNonQuery()
                DocPeriodos.LoadXml(prm1.Value)

            End With
            CONar.Close()
        Catch ex As Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub
End Module
