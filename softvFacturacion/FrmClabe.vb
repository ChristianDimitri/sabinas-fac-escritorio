﻿Public Class FrmClabe

    Private Sub FrmClabe_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        colorea(Me)
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@contrato", SqlDbType.BigInt, GloContrato)
        BaseII.CreateMyParameter("@yaTiene", ParameterDirection.Output, SqlDbType.Int)
        BaseII.CreateMyParameter("@clabe", ParameterDirection.Output, SqlDbType.VarChar, 50)
        BaseII.CreateMyParameter("@id", ParameterDirection.Output, SqlDbType.Int)
        BaseII.ProcedimientoOutPut("YaTieneClabe")
            tbClabe.Text = BaseII.dicoPar("@clabe").ToString
    End Sub

    Private Sub btnSalir_Click(sender As Object, e As EventArgs) Handles btnSalir.Click
        Me.Close()
    End Sub

    Private Sub btnImprimir_Click(sender As Object, e As EventArgs) Handles btnImprimir.Click
        LiTipo = 10
        FrmImprimir.Show()
        Me.Close()
    End Sub
End Class