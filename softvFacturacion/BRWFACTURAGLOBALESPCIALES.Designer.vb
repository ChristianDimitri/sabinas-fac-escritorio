﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class BRWFACTURAGLOBALESPCIALES
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.CajeraLabel = New System.Windows.Forms.Label()
        Me.ImporteLabel = New System.Windows.Forms.Label()
        Me.IdFacturaLabel = New System.Windows.Forms.Label()
        Me.CanceladaLabel = New System.Windows.Forms.Label()
        Me.FechaLabel = New System.Windows.Forms.Label()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.CMBIdFacturaTextBox = New System.Windows.Forms.TextBox()
        Me.BuscaFacturasGlobalesBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.DataSetLydia = New softvFacturacion.DataSetLydia()
        Me.NewsoftvDataSet2 = New softvFacturacion.NewsoftvDataSet2()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.TextBox3 = New System.Windows.Forms.TextBox()
        Me.Button2 = New System.Windows.Forms.Button()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.TextBox2 = New System.Windows.Forms.TextBox()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.TextBox1 = New System.Windows.Forms.TextBox()
        Me.CMBLabel1 = New System.Windows.Forms.Label()
        Me.Button10 = New System.Windows.Forms.Button()
        Me.Button9 = New System.Windows.Forms.Button()
        Me.Button6 = New System.Windows.Forms.Button()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.SplitContainer1 = New System.Windows.Forms.SplitContainer()
        Me.ComboBoxCompanias = New System.Windows.Forms.ComboBox()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.TextBox4 = New System.Windows.Forms.TextBox()
        Me.Panel2 = New System.Windows.Forms.Panel()
        Me.CMBDistribuidorTextBox = New System.Windows.Forms.TextBox()
        Me.CMBCanceladaTextBox = New System.Windows.Forms.TextBox()
        Me.CMBFechaTextBox = New System.Windows.Forms.TextBox()
        Me.CMBCajeraTextBox = New System.Windows.Forms.TextBox()
        Me.CMBImporteTextBox = New System.Windows.Forms.TextBox()
        Me.Button3 = New System.Windows.Forms.Button()
        Me.BuscaFacturasGlobalesDataGridView = New System.Windows.Forms.DataGridView()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.ComboBox2 = New System.Windows.Forms.ComboBox()
        Me.MUESTRATIPOFACTGLOBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Label6 = New System.Windows.Forms.Label()
        Me.DateTimePicker1 = New System.Windows.Forms.DateTimePicker()
        Me.DameFechadelServidorHoraBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.ModfacglobalBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.CANCELAFACTGLOBALBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.CANCELAFACTGLOBALTableAdapter = New softvFacturacion.NewsoftvDataSet2TableAdapters.CANCELAFACTGLOBALTableAdapter()
        Me.ModfacglobalTableAdapter = New softvFacturacion.NewsoftvDataSet2TableAdapters.ModfacglobalTableAdapter()
        Me.DameFechadelServidorHoraTableAdapter = New softvFacturacion.NewsoftvDataSet2TableAdapters.DameFechadelServidorHoraTableAdapter()
        Me.MUESTRATIPOFACTGLOTableAdapter = New softvFacturacion.NewsoftvDataSet2TableAdapters.MUESTRATIPOFACTGLOTableAdapter()
        Me.DameCantidadALetraBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.DameCantidadALetraTableAdapter = New softvFacturacion.NewsoftvDataSet2TableAdapters.DameCantidadALetraTableAdapter()
        Me.Button4 = New System.Windows.Forms.Button()
        Me.Button5 = New System.Windows.Forms.Button()
        Me.Procedimientos_arnoldo = New softvFacturacion.Procedimientos_arnoldo()
        Me.Dame_clv_sucursalBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Dame_clv_sucursalTableAdapter = New softvFacturacion.Procedimientos_arnoldoTableAdapters.Dame_clv_sucursalTableAdapter()
        Me.BuscaFacturasGlobalesTableAdapter = New softvFacturacion.DataSetLydiaTableAdapters.BuscaFacturasGlobalesTableAdapter()
        Me.VerAcceso2TableAdapter1 = New softvFacturacion.ProcedimientosArnoldo3TableAdapters.VerAcceso2TableAdapter()
        Me.VerAcceso2TableAdapter2 = New softvFacturacion.ProcedimientosArnoldo3TableAdapters.VerAcceso2TableAdapter()
        Me.VerAcceso2TableAdapter3 = New softvFacturacion.ProcedimientosArnoldo3TableAdapters.VerAcceso2TableAdapter()
        Me.Button7 = New System.Windows.Forms.Button()
        Me.IdFactura = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Serie = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Factura = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.fechai = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Monto = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.sucursal = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Cajera = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Cancelada = New System.Windows.Forms.DataGridViewCheckBoxColumn()
        Me.fechaf = New System.Windows.Forms.DataGridViewTextBoxColumn()
        CType(Me.BuscaFacturasGlobalesBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DataSetLydia, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.NewsoftvDataSet2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel1.SuspendLayout()
        Me.SplitContainer1.Panel1.SuspendLayout()
        Me.SplitContainer1.Panel2.SuspendLayout()
        Me.SplitContainer1.SuspendLayout()
        Me.Panel2.SuspendLayout()
        CType(Me.BuscaFacturasGlobalesDataGridView, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.MUESTRATIPOFACTGLOBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DameFechadelServidorHoraBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ModfacglobalBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CANCELAFACTGLOBALBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DameCantidadALetraBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Procedimientos_arnoldo, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Dame_clv_sucursalBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'CajeraLabel
        '
        Me.CajeraLabel.AutoSize = True
        Me.CajeraLabel.BackColor = System.Drawing.Color.DarkOrange
        Me.CajeraLabel.ForeColor = System.Drawing.Color.White
        Me.CajeraLabel.Location = New System.Drawing.Point(11, 101)
        Me.CajeraLabel.Name = "CajeraLabel"
        Me.CajeraLabel.Size = New System.Drawing.Size(62, 13)
        Me.CajeraLabel.TabIndex = 6
        Me.CajeraLabel.Text = "Cajero(a):"
        '
        'ImporteLabel
        '
        Me.ImporteLabel.AutoSize = True
        Me.ImporteLabel.BackColor = System.Drawing.Color.DarkOrange
        Me.ImporteLabel.ForeColor = System.Drawing.Color.White
        Me.ImporteLabel.Location = New System.Drawing.Point(20, 130)
        Me.ImporteLabel.Name = "ImporteLabel"
        Me.ImporteLabel.Size = New System.Drawing.Size(53, 13)
        Me.ImporteLabel.TabIndex = 8
        Me.ImporteLabel.Text = "Importe:"
        '
        'IdFacturaLabel
        '
        Me.IdFacturaLabel.AutoSize = True
        Me.IdFacturaLabel.BackColor = System.Drawing.Color.DarkOrange
        Me.IdFacturaLabel.ForeColor = System.Drawing.Color.White
        Me.IdFacturaLabel.Location = New System.Drawing.Point(8, 43)
        Me.IdFacturaLabel.Name = "IdFacturaLabel"
        Me.IdFacturaLabel.Size = New System.Drawing.Size(65, 13)
        Me.IdFacturaLabel.TabIndex = 12
        Me.IdFacturaLabel.Text = "IdFactura:"
        '
        'CanceladaLabel
        '
        Me.CanceladaLabel.AutoSize = True
        Me.CanceladaLabel.BackColor = System.Drawing.Color.DarkOrange
        Me.CanceladaLabel.ForeColor = System.Drawing.Color.White
        Me.CanceladaLabel.Location = New System.Drawing.Point(6, 156)
        Me.CanceladaLabel.Name = "CanceladaLabel"
        Me.CanceladaLabel.Size = New System.Drawing.Size(71, 13)
        Me.CanceladaLabel.TabIndex = 49
        Me.CanceladaLabel.Text = "Cancelada:"
        '
        'FechaLabel
        '
        Me.FechaLabel.AutoSize = True
        Me.FechaLabel.BackColor = System.Drawing.Color.DarkOrange
        Me.FechaLabel.ForeColor = System.Drawing.Color.White
        Me.FechaLabel.Location = New System.Drawing.Point(27, 72)
        Me.FechaLabel.Name = "FechaLabel"
        Me.FechaLabel.Size = New System.Drawing.Size(46, 13)
        Me.FechaLabel.TabIndex = 4
        Me.FechaLabel.Text = "Fecha:"
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.BackColor = System.Drawing.Color.DarkOrange
        Me.Label10.ForeColor = System.Drawing.Color.White
        Me.Label10.Location = New System.Drawing.Point(6, 187)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(75, 13)
        Me.Label10.TabIndex = 54
        Me.Label10.Text = "Distribuidor:"
        '
        'CMBIdFacturaTextBox
        '
        Me.CMBIdFacturaTextBox.BackColor = System.Drawing.Color.DarkOrange
        Me.CMBIdFacturaTextBox.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.CMBIdFacturaTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.BuscaFacturasGlobalesBindingSource, "IdFactura", True))
        Me.CMBIdFacturaTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBIdFacturaTextBox.ForeColor = System.Drawing.Color.White
        Me.CMBIdFacturaTextBox.Location = New System.Drawing.Point(107, 43)
        Me.CMBIdFacturaTextBox.Name = "CMBIdFacturaTextBox"
        Me.CMBIdFacturaTextBox.ReadOnly = True
        Me.CMBIdFacturaTextBox.Size = New System.Drawing.Size(109, 13)
        Me.CMBIdFacturaTextBox.TabIndex = 53
        Me.CMBIdFacturaTextBox.TabStop = False
        '
        'BuscaFacturasGlobalesBindingSource
        '
        Me.BuscaFacturasGlobalesBindingSource.DataMember = "BuscaFacturasGlobales"
        Me.BuscaFacturasGlobalesBindingSource.DataSource = Me.DataSetLydia
        '
        'DataSetLydia
        '
        Me.DataSetLydia.DataSetName = "DataSetLydia"
        Me.DataSetLydia.EnforceConstraints = False
        Me.DataSetLydia.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'NewsoftvDataSet2
        '
        Me.NewsoftvDataSet2.DataSetName = "NewsoftvDataSet2"
        Me.NewsoftvDataSet2.EnforceConstraints = False
        Me.NewsoftvDataSet2.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.BackColor = System.Drawing.Color.DarkOrange
        Me.Label7.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7.ForeColor = System.Drawing.Color.White
        Me.Label7.Location = New System.Drawing.Point(3, 10)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(198, 16)
        Me.Label7.TabIndex = 47
        Me.Label7.Text = "Datos de la Factura Global:"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.Location = New System.Drawing.Point(11, 342)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(63, 15)
        Me.Label4.TabIndex = 40
        Me.Label4.Text = "Sucursal"
        Me.Label4.Visible = False
        '
        'TextBox3
        '
        Me.TextBox3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.TextBox3.Location = New System.Drawing.Point(14, 360)
        Me.TextBox3.Name = "TextBox3"
        Me.TextBox3.Size = New System.Drawing.Size(159, 20)
        Me.TextBox3.TabIndex = 6
        Me.TextBox3.Visible = False
        '
        'Button2
        '
        Me.Button2.BackColor = System.Drawing.Color.Orange
        Me.Button2.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(177, Byte))
        Me.Button2.Location = New System.Drawing.Point(15, 300)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(88, 23)
        Me.Button2.TabIndex = 5
        Me.Button2.Text = "Buscar"
        Me.Button2.UseVisualStyleBackColor = False
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(12, 256)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(46, 15)
        Me.Label3.TabIndex = 37
        Me.Label3.Text = "Fecha"
        '
        'TextBox2
        '
        Me.TextBox2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.TextBox2.Location = New System.Drawing.Point(14, 274)
        Me.TextBox2.Name = "TextBox2"
        Me.TextBox2.Size = New System.Drawing.Size(107, 20)
        Me.TextBox2.TabIndex = 4
        '
        'Button1
        '
        Me.Button1.BackColor = System.Drawing.Color.Orange
        Me.Button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(177, Byte))
        Me.Button1.Location = New System.Drawing.Point(15, 216)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(88, 23)
        Me.Button1.TabIndex = 3
        Me.Button1.Text = "Buscar"
        Me.Button1.UseVisualStyleBackColor = False
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(12, 122)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(45, 15)
        Me.Label2.TabIndex = 34
        Me.Label2.Text = "Serie:"
        '
        'TextBox1
        '
        Me.TextBox1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.TextBox1.Location = New System.Drawing.Point(15, 140)
        Me.TextBox1.MaxLength = 5
        Me.TextBox1.Name = "TextBox1"
        Me.TextBox1.Size = New System.Drawing.Size(160, 20)
        Me.TextBox1.TabIndex = 1
        '
        'CMBLabel1
        '
        Me.CMBLabel1.AutoSize = True
        Me.CMBLabel1.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBLabel1.ForeColor = System.Drawing.Color.FromArgb(CType(CType(128, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.CMBLabel1.Location = New System.Drawing.Point(14, 78)
        Me.CMBLabel1.Name = "CMBLabel1"
        Me.CMBLabel1.Size = New System.Drawing.Size(205, 18)
        Me.CMBLabel1.TabIndex = 32
        Me.CMBLabel1.Text = "Buscar Factura Global de:"
        '
        'Button10
        '
        Me.Button10.BackColor = System.Drawing.Color.Orange
        Me.Button10.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.Button10.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button10.Location = New System.Drawing.Point(872, 657)
        Me.Button10.Name = "Button10"
        Me.Button10.Size = New System.Drawing.Size(124, 36)
        Me.Button10.TabIndex = 11
        Me.Button10.Text = "&SALIR"
        Me.Button10.UseVisualStyleBackColor = False
        '
        'Button9
        '
        Me.Button9.BackColor = System.Drawing.Color.Orange
        Me.Button9.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button9.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button9.Location = New System.Drawing.Point(872, 612)
        Me.Button9.Name = "Button9"
        Me.Button9.Size = New System.Drawing.Size(124, 36)
        Me.Button9.TabIndex = 10
        Me.Button9.Text = "&CANCELAR"
        Me.Button9.UseVisualStyleBackColor = False
        '
        'Button6
        '
        Me.Button6.BackColor = System.Drawing.Color.Orange
        Me.Button6.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.Button6.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button6.Location = New System.Drawing.Point(876, 30)
        Me.Button6.Name = "Button6"
        Me.Button6.Size = New System.Drawing.Size(133, 36)
        Me.Button6.TabIndex = 8
        Me.Button6.Text = "&NUEVO"
        Me.Button6.UseVisualStyleBackColor = False
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.SplitContainer1)
        Me.Panel1.Location = New System.Drawing.Point(25, 15)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(841, 714)
        Me.Panel1.TabIndex = 38
        '
        'SplitContainer1
        '
        Me.SplitContainer1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SplitContainer1.Location = New System.Drawing.Point(0, 0)
        Me.SplitContainer1.Name = "SplitContainer1"
        '
        'SplitContainer1.Panel1
        '
        Me.SplitContainer1.Panel1.Controls.Add(Me.ComboBoxCompanias)
        Me.SplitContainer1.Panel1.Controls.Add(Me.Label9)
        Me.SplitContainer1.Panel1.Controls.Add(Me.Label1)
        Me.SplitContainer1.Panel1.Controls.Add(Me.Label5)
        Me.SplitContainer1.Panel1.Controls.Add(Me.TextBox4)
        Me.SplitContainer1.Panel1.Controls.Add(Me.Panel2)
        Me.SplitContainer1.Panel1.Controls.Add(Me.Button3)
        Me.SplitContainer1.Panel1.Controls.Add(Me.Label4)
        Me.SplitContainer1.Panel1.Controls.Add(Me.TextBox3)
        Me.SplitContainer1.Panel1.Controls.Add(Me.Button2)
        Me.SplitContainer1.Panel1.Controls.Add(Me.Label3)
        Me.SplitContainer1.Panel1.Controls.Add(Me.TextBox2)
        Me.SplitContainer1.Panel1.Controls.Add(Me.Button1)
        Me.SplitContainer1.Panel1.Controls.Add(Me.Label2)
        Me.SplitContainer1.Panel1.Controls.Add(Me.TextBox1)
        Me.SplitContainer1.Panel1.Controls.Add(Me.CMBLabel1)
        '
        'SplitContainer1.Panel2
        '
        Me.SplitContainer1.Panel2.AutoScroll = True
        Me.SplitContainer1.Panel2.Controls.Add(Me.BuscaFacturasGlobalesDataGridView)
        Me.SplitContainer1.Panel2.Controls.Add(Me.Label8)
        Me.SplitContainer1.Panel2.Controls.Add(Me.ComboBox2)
        Me.SplitContainer1.Panel2.Controls.Add(Me.Label6)
        Me.SplitContainer1.Panel2.Controls.Add(Me.DateTimePicker1)
        Me.SplitContainer1.Size = New System.Drawing.Size(841, 714)
        Me.SplitContainer1.SplitterDistance = 277
        Me.SplitContainer1.TabIndex = 0
        Me.SplitContainer1.TabStop = False
        '
        'ComboBoxCompanias
        '
        Me.ComboBoxCompanias.DisplayMember = "Clv_Vendedor"
        Me.ComboBoxCompanias.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold)
        Me.ComboBoxCompanias.ForeColor = System.Drawing.Color.Black
        Me.ComboBoxCompanias.FormattingEnabled = True
        Me.ComboBoxCompanias.Location = New System.Drawing.Point(14, 41)
        Me.ComboBoxCompanias.Name = "ComboBoxCompanias"
        Me.ComboBoxCompanias.Size = New System.Drawing.Size(218, 21)
        Me.ComboBoxCompanias.TabIndex = 55
        Me.ComboBoxCompanias.ValueMember = "Clv_Vendedor"
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label9.ForeColor = System.Drawing.Color.FromArgb(CType(CType(128, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.Label9.Location = New System.Drawing.Point(14, 15)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(184, 18)
        Me.Label9.TabIndex = 54
        Me.Label9.Text = "Sucursales Especiales:"
        '
        'Label1
        '
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(127, 274)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(106, 17)
        Me.Label1.TabIndex = 53
        Me.Label1.Text = "Ej. : dd/mm/aa"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.Location = New System.Drawing.Point(11, 172)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(43, 15)
        Me.Label5.TabIndex = 50
        Me.Label5.Text = "Folio:"
        '
        'TextBox4
        '
        Me.TextBox4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.TextBox4.Location = New System.Drawing.Point(15, 190)
        Me.TextBox4.Name = "TextBox4"
        Me.TextBox4.Size = New System.Drawing.Size(160, 20)
        Me.TextBox4.TabIndex = 2
        '
        'Panel2
        '
        Me.Panel2.BackColor = System.Drawing.Color.DarkOrange
        Me.Panel2.Controls.Add(Me.CMBDistribuidorTextBox)
        Me.Panel2.Controls.Add(Me.Label10)
        Me.Panel2.Controls.Add(Me.CMBIdFacturaTextBox)
        Me.Panel2.Controls.Add(Me.CMBCanceladaTextBox)
        Me.Panel2.Controls.Add(Me.CanceladaLabel)
        Me.Panel2.Controls.Add(Me.Label7)
        Me.Panel2.Controls.Add(Me.FechaLabel)
        Me.Panel2.Controls.Add(Me.CMBFechaTextBox)
        Me.Panel2.Controls.Add(Me.CajeraLabel)
        Me.Panel2.Controls.Add(Me.CMBCajeraTextBox)
        Me.Panel2.Controls.Add(Me.ImporteLabel)
        Me.Panel2.Controls.Add(Me.CMBImporteTextBox)
        Me.Panel2.Controls.Add(Me.IdFacturaLabel)
        Me.Panel2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Panel2.Location = New System.Drawing.Point(3, 455)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(360, 253)
        Me.Panel2.TabIndex = 48
        '
        'CMBDistribuidorTextBox
        '
        Me.CMBDistribuidorTextBox.BackColor = System.Drawing.Color.DarkOrange
        Me.CMBDistribuidorTextBox.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.CMBDistribuidorTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.BuscaFacturasGlobalesBindingSource, "cancelada", True))
        Me.CMBDistribuidorTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBDistribuidorTextBox.ForeColor = System.Drawing.Color.White
        Me.CMBDistribuidorTextBox.Location = New System.Drawing.Point(107, 187)
        Me.CMBDistribuidorTextBox.Multiline = True
        Me.CMBDistribuidorTextBox.Name = "CMBDistribuidorTextBox"
        Me.CMBDistribuidorTextBox.ReadOnly = True
        Me.CMBDistribuidorTextBox.Size = New System.Drawing.Size(159, 52)
        Me.CMBDistribuidorTextBox.TabIndex = 55
        Me.CMBDistribuidorTextBox.TabStop = False
        '
        'CMBCanceladaTextBox
        '
        Me.CMBCanceladaTextBox.BackColor = System.Drawing.Color.DarkOrange
        Me.CMBCanceladaTextBox.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.CMBCanceladaTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.BuscaFacturasGlobalesBindingSource, "cancelada", True))
        Me.CMBCanceladaTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBCanceladaTextBox.ForeColor = System.Drawing.Color.White
        Me.CMBCanceladaTextBox.Location = New System.Drawing.Point(107, 156)
        Me.CMBCanceladaTextBox.Name = "CMBCanceladaTextBox"
        Me.CMBCanceladaTextBox.ReadOnly = True
        Me.CMBCanceladaTextBox.Size = New System.Drawing.Size(109, 13)
        Me.CMBCanceladaTextBox.TabIndex = 51
        Me.CMBCanceladaTextBox.TabStop = False
        '
        'CMBFechaTextBox
        '
        Me.CMBFechaTextBox.BackColor = System.Drawing.Color.DarkOrange
        Me.CMBFechaTextBox.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.CMBFechaTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.BuscaFacturasGlobalesBindingSource, "fecha", True))
        Me.CMBFechaTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBFechaTextBox.ForeColor = System.Drawing.Color.White
        Me.CMBFechaTextBox.Location = New System.Drawing.Point(107, 72)
        Me.CMBFechaTextBox.Name = "CMBFechaTextBox"
        Me.CMBFechaTextBox.ReadOnly = True
        Me.CMBFechaTextBox.Size = New System.Drawing.Size(109, 13)
        Me.CMBFechaTextBox.TabIndex = 5
        Me.CMBFechaTextBox.TabStop = False
        '
        'CMBCajeraTextBox
        '
        Me.CMBCajeraTextBox.BackColor = System.Drawing.Color.DarkOrange
        Me.CMBCajeraTextBox.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.CMBCajeraTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.BuscaFacturasGlobalesBindingSource, "cajera", True))
        Me.CMBCajeraTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBCajeraTextBox.ForeColor = System.Drawing.Color.White
        Me.CMBCajeraTextBox.Location = New System.Drawing.Point(107, 101)
        Me.CMBCajeraTextBox.Name = "CMBCajeraTextBox"
        Me.CMBCajeraTextBox.ReadOnly = True
        Me.CMBCajeraTextBox.Size = New System.Drawing.Size(109, 13)
        Me.CMBCajeraTextBox.TabIndex = 7
        Me.CMBCajeraTextBox.TabStop = False
        '
        'CMBImporteTextBox
        '
        Me.CMBImporteTextBox.BackColor = System.Drawing.Color.DarkOrange
        Me.CMBImporteTextBox.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.CMBImporteTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.BuscaFacturasGlobalesBindingSource, "importe", True))
        Me.CMBImporteTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBImporteTextBox.ForeColor = System.Drawing.Color.White
        Me.CMBImporteTextBox.Location = New System.Drawing.Point(107, 130)
        Me.CMBImporteTextBox.Name = "CMBImporteTextBox"
        Me.CMBImporteTextBox.ReadOnly = True
        Me.CMBImporteTextBox.Size = New System.Drawing.Size(109, 13)
        Me.CMBImporteTextBox.TabIndex = 9
        Me.CMBImporteTextBox.TabStop = False
        '
        'Button3
        '
        Me.Button3.BackColor = System.Drawing.Color.Orange
        Me.Button3.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(177, Byte))
        Me.Button3.Location = New System.Drawing.Point(15, 386)
        Me.Button3.Name = "Button3"
        Me.Button3.Size = New System.Drawing.Size(88, 23)
        Me.Button3.TabIndex = 7
        Me.Button3.Text = "Buscar"
        Me.Button3.UseVisualStyleBackColor = False
        Me.Button3.Visible = False
        '
        'BuscaFacturasGlobalesDataGridView
        '
        Me.BuscaFacturasGlobalesDataGridView.AllowUserToAddRows = False
        Me.BuscaFacturasGlobalesDataGridView.AllowUserToDeleteRows = False
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.BuscaFacturasGlobalesDataGridView.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle1
        Me.BuscaFacturasGlobalesDataGridView.ColumnHeadersHeight = 30
        Me.BuscaFacturasGlobalesDataGridView.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.IdFactura, Me.Serie, Me.Factura, Me.fechai, Me.Monto, Me.sucursal, Me.Cajera, Me.Cancelada, Me.fechaf})
        Me.BuscaFacturasGlobalesDataGridView.Location = New System.Drawing.Point(0, 0)
        Me.BuscaFacturasGlobalesDataGridView.MultiSelect = False
        Me.BuscaFacturasGlobalesDataGridView.Name = "BuscaFacturasGlobalesDataGridView"
        Me.BuscaFacturasGlobalesDataGridView.ReadOnly = True
        Me.BuscaFacturasGlobalesDataGridView.RowHeadersVisible = False
        Me.BuscaFacturasGlobalesDataGridView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.BuscaFacturasGlobalesDataGridView.Size = New System.Drawing.Size(606, 697)
        Me.BuscaFacturasGlobalesDataGridView.TabIndex = 0
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.BuscaFacturasGlobalesBindingSource, "Column1", True))
        Me.Label8.Location = New System.Drawing.Point(446, 492)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(39, 13)
        Me.Label8.TabIndex = 47
        Me.Label8.Text = "Label8"
        '
        'ComboBox2
        '
        Me.ComboBox2.DataSource = Me.MUESTRATIPOFACTGLOBindingSource
        Me.ComboBox2.DisplayMember = "Nombre"
        Me.ComboBox2.Enabled = False
        Me.ComboBox2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ComboBox2.FormattingEnabled = True
        Me.ComboBox2.Location = New System.Drawing.Point(388, 177)
        Me.ComboBox2.Name = "ComboBox2"
        Me.ComboBox2.Size = New System.Drawing.Size(218, 21)
        Me.ComboBox2.TabIndex = 0
        Me.ComboBox2.ValueMember = "Clave"
        Me.ComboBox2.Visible = False
        '
        'MUESTRATIPOFACTGLOBindingSource
        '
        Me.MUESTRATIPOFACTGLOBindingSource.DataMember = "MUESTRATIPOFACTGLO"
        Me.MUESTRATIPOFACTGLOBindingSource.DataSource = Me.NewsoftvDataSet2
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.BuscaFacturasGlobalesBindingSource, "Nombre", True))
        Me.Label6.Location = New System.Drawing.Point(401, 505)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(39, 13)
        Me.Label6.TabIndex = 46
        Me.Label6.Text = "Label6"
        '
        'DateTimePicker1
        '
        Me.DateTimePicker1.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DameFechadelServidorHoraBindingSource, "Fecha", True))
        Me.DateTimePicker1.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.DateTimePicker1.Location = New System.Drawing.Point(264, 190)
        Me.DateTimePicker1.Name = "DateTimePicker1"
        Me.DateTimePicker1.Size = New System.Drawing.Size(200, 20)
        Me.DateTimePicker1.TabIndex = 44
        Me.DateTimePicker1.TabStop = False
        '
        'DameFechadelServidorHoraBindingSource
        '
        Me.DameFechadelServidorHoraBindingSource.DataMember = "DameFechadelServidorHora"
        Me.DameFechadelServidorHoraBindingSource.DataSource = Me.NewsoftvDataSet2
        '
        'ModfacglobalBindingSource
        '
        Me.ModfacglobalBindingSource.DataMember = "Modfacglobal"
        Me.ModfacglobalBindingSource.DataSource = Me.NewsoftvDataSet2
        '
        'CANCELAFACTGLOBALBindingSource
        '
        Me.CANCELAFACTGLOBALBindingSource.DataMember = "CANCELAFACTGLOBAL"
        Me.CANCELAFACTGLOBALBindingSource.DataSource = Me.NewsoftvDataSet2
        '
        'CANCELAFACTGLOBALTableAdapter
        '
        Me.CANCELAFACTGLOBALTableAdapter.ClearBeforeFill = True
        '
        'ModfacglobalTableAdapter
        '
        Me.ModfacglobalTableAdapter.ClearBeforeFill = True
        '
        'DameFechadelServidorHoraTableAdapter
        '
        Me.DameFechadelServidorHoraTableAdapter.ClearBeforeFill = True
        '
        'MUESTRATIPOFACTGLOTableAdapter
        '
        Me.MUESTRATIPOFACTGLOTableAdapter.ClearBeforeFill = True
        '
        'DameCantidadALetraBindingSource
        '
        Me.DameCantidadALetraBindingSource.DataMember = "DameCantidadALetra"
        Me.DameCantidadALetraBindingSource.DataSource = Me.NewsoftvDataSet2
        '
        'DameCantidadALetraTableAdapter
        '
        Me.DameCantidadALetraTableAdapter.ClearBeforeFill = True
        '
        'Button4
        '
        Me.Button4.BackColor = System.Drawing.Color.Orange
        Me.Button4.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.Button4.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button4.Location = New System.Drawing.Point(872, 556)
        Me.Button4.Name = "Button4"
        Me.Button4.Size = New System.Drawing.Size(124, 50)
        Me.Button4.TabIndex = 9
        Me.Button4.Text = "&Reimprimir Factura"
        Me.Button4.UseVisualStyleBackColor = False
        '
        'Button5
        '
        Me.Button5.BackColor = System.Drawing.Color.Orange
        Me.Button5.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.Button5.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button5.Location = New System.Drawing.Point(875, 72)
        Me.Button5.Name = "Button5"
        Me.Button5.Size = New System.Drawing.Size(135, 36)
        Me.Button5.TabIndex = 45
        Me.Button5.Text = "&CONSULTA"
        Me.Button5.UseVisualStyleBackColor = False
        '
        'Procedimientos_arnoldo
        '
        Me.Procedimientos_arnoldo.DataSetName = "Procedimientos_arnoldo"
        Me.Procedimientos_arnoldo.EnforceConstraints = False
        Me.Procedimientos_arnoldo.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'Dame_clv_sucursalBindingSource
        '
        Me.Dame_clv_sucursalBindingSource.DataMember = "Dame_clv_sucursal"
        Me.Dame_clv_sucursalBindingSource.DataSource = Me.Procedimientos_arnoldo
        '
        'Dame_clv_sucursalTableAdapter
        '
        Me.Dame_clv_sucursalTableAdapter.ClearBeforeFill = True
        '
        'BuscaFacturasGlobalesTableAdapter
        '
        Me.BuscaFacturasGlobalesTableAdapter.ClearBeforeFill = True
        '
        'VerAcceso2TableAdapter1
        '
        Me.VerAcceso2TableAdapter1.ClearBeforeFill = True
        '
        'VerAcceso2TableAdapter2
        '
        Me.VerAcceso2TableAdapter2.ClearBeforeFill = True
        '
        'VerAcceso2TableAdapter3
        '
        Me.VerAcceso2TableAdapter3.ClearBeforeFill = True
        '
        'Button7
        '
        Me.Button7.BackColor = System.Drawing.Color.Orange
        Me.Button7.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.Button7.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button7.Location = New System.Drawing.Point(872, 500)
        Me.Button7.Name = "Button7"
        Me.Button7.Size = New System.Drawing.Size(124, 50)
        Me.Button7.TabIndex = 48
        Me.Button7.Text = "&Refacturar"
        Me.Button7.UseVisualStyleBackColor = False
        '
        'IdFactura
        '
        Me.IdFactura.DataPropertyName = "IdFactura"
        Me.IdFactura.HeaderText = "IdFactura"
        Me.IdFactura.Name = "IdFactura"
        Me.IdFactura.ReadOnly = True
        Me.IdFactura.Width = 5
        '
        'Serie
        '
        Me.Serie.DataPropertyName = "Serie"
        Me.Serie.HeaderText = "Serie"
        Me.Serie.Name = "Serie"
        Me.Serie.ReadOnly = True
        '
        'Factura
        '
        Me.Factura.DataPropertyName = "factura"
        Me.Factura.HeaderText = "Factura"
        Me.Factura.Name = "Factura"
        Me.Factura.ReadOnly = True
        '
        'fechai
        '
        Me.fechai.DataPropertyName = "fechai"
        Me.fechai.HeaderText = "Fecha"
        Me.fechai.Name = "fechai"
        Me.fechai.ReadOnly = True
        '
        'Monto
        '
        Me.Monto.DataPropertyName = "importe"
        Me.Monto.HeaderText = "Importe"
        Me.Monto.Name = "Monto"
        Me.Monto.ReadOnly = True
        '
        'sucursal
        '
        Me.sucursal.DataPropertyName = "Nombre"
        Me.sucursal.HeaderText = "Suc. Especial"
        Me.sucursal.Name = "sucursal"
        Me.sucursal.ReadOnly = True
        '
        'Cajera
        '
        Me.Cajera.DataPropertyName = "cajera"
        Me.Cajera.HeaderText = "Cajera (o)"
        Me.Cajera.Name = "Cajera"
        Me.Cajera.ReadOnly = True
        '
        'Cancelada
        '
        Me.Cancelada.DataPropertyName = "cancelada"
        Me.Cancelada.FalseValue = "0"
        Me.Cancelada.HeaderText = "Cancelada"
        Me.Cancelada.IndeterminateValue = "0"
        Me.Cancelada.Name = "Cancelada"
        Me.Cancelada.ReadOnly = True
        Me.Cancelada.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.Cancelada.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic
        Me.Cancelada.TrueValue = "1"
        '
        'fechaf
        '
        Me.fechaf.DataPropertyName = "fechaf"
        Me.fechaf.HeaderText = "fechaf"
        Me.fechaf.Name = "fechaf"
        Me.fechaf.ReadOnly = True
        Me.fechaf.Visible = False
        '
        'BRWFACTURAGLOBALESPCIALES
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.AutoScroll = True
        Me.BackColor = System.Drawing.Color.WhiteSmoke
        Me.ClientSize = New System.Drawing.Size(1016, 740)
        Me.Controls.Add(Me.Button7)
        Me.Controls.Add(Me.Button5)
        Me.Controls.Add(Me.Button4)
        Me.Controls.Add(Me.Button10)
        Me.Controls.Add(Me.Button9)
        Me.Controls.Add(Me.Button6)
        Me.Controls.Add(Me.Panel1)
        Me.MaximizeBox = False
        Me.Name = "BRWFACTURAGLOBALESPCIALES"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Factura Global de Sucursales Especiales"
        CType(Me.BuscaFacturasGlobalesBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DataSetLydia, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.NewsoftvDataSet2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel1.ResumeLayout(False)
        Me.SplitContainer1.Panel1.ResumeLayout(False)
        Me.SplitContainer1.Panel1.PerformLayout()
        Me.SplitContainer1.Panel2.ResumeLayout(False)
        Me.SplitContainer1.Panel2.PerformLayout()
        Me.SplitContainer1.ResumeLayout(False)
        Me.Panel2.ResumeLayout(False)
        Me.Panel2.PerformLayout()
        CType(Me.BuscaFacturasGlobalesDataGridView, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.MUESTRATIPOFACTGLOBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DameFechadelServidorHoraBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ModfacglobalBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CANCELAFACTGLOBALBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DameCantidadALetraBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Procedimientos_arnoldo, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Dame_clv_sucursalBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents TextBox3 As System.Windows.Forms.TextBox
    Friend WithEvents Button2 As System.Windows.Forms.Button
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents TextBox2 As System.Windows.Forms.TextBox
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents TextBox1 As System.Windows.Forms.TextBox
    Friend WithEvents CMBLabel1 As System.Windows.Forms.Label
    Friend WithEvents Button10 As System.Windows.Forms.Button
    Friend WithEvents Button9 As System.Windows.Forms.Button
    Friend WithEvents Button6 As System.Windows.Forms.Button
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents SplitContainer1 As System.Windows.Forms.SplitContainer
    Friend WithEvents Panel2 As System.Windows.Forms.Panel
    Friend WithEvents CMBFechaTextBox As System.Windows.Forms.TextBox
    Friend WithEvents CMBCajeraTextBox As System.Windows.Forms.TextBox
    Friend WithEvents CMBImporteTextBox As System.Windows.Forms.TextBox
    Friend WithEvents Button3 As System.Windows.Forms.Button
    Friend WithEvents NewsoftvDataSet2 As softvFacturacion.NewsoftvDataSet2
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents TextBox4 As System.Windows.Forms.TextBox
    Friend WithEvents CMBCanceladaTextBox As System.Windows.Forms.TextBox
    Friend WithEvents CANCELAFACTGLOBALBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents CANCELAFACTGLOBALTableAdapter As softvFacturacion.NewsoftvDataSet2TableAdapters.CANCELAFACTGLOBALTableAdapter
    Friend WithEvents ModfacglobalBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents ModfacglobalTableAdapter As softvFacturacion.NewsoftvDataSet2TableAdapters.ModfacglobalTableAdapter
    Friend WithEvents ComboBox2 As System.Windows.Forms.ComboBox
    Friend WithEvents DameFechadelServidorHoraBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents DameFechadelServidorHoraTableAdapter As softvFacturacion.NewsoftvDataSet2TableAdapters.DameFechadelServidorHoraTableAdapter
    Friend WithEvents DateTimePicker1 As System.Windows.Forms.DateTimePicker
    Friend WithEvents MUESTRATIPOFACTGLOBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents MUESTRATIPOFACTGLOTableAdapter As softvFacturacion.NewsoftvDataSet2TableAdapters.MUESTRATIPOFACTGLOTableAdapter
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents DameCantidadALetraBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents DameCantidadALetraTableAdapter As softvFacturacion.NewsoftvDataSet2TableAdapters.DameCantidadALetraTableAdapter
    Friend WithEvents Button4 As System.Windows.Forms.Button
    Friend WithEvents Button5 As System.Windows.Forms.Button
    Friend WithEvents Procedimientos_arnoldo As softvFacturacion.Procedimientos_arnoldo
    Friend WithEvents Dame_clv_sucursalBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Dame_clv_sucursalTableAdapter As softvFacturacion.Procedimientos_arnoldoTableAdapters.Dame_clv_sucursalTableAdapter
    Friend WithEvents DataSetLydia As softvFacturacion.DataSetLydia
    Friend WithEvents BuscaFacturasGlobalesDataGridView As System.Windows.Forms.DataGridView
    Friend WithEvents BuscaFacturasGlobalesBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents BuscaFacturasGlobalesTableAdapter As softvFacturacion.DataSetLydiaTableAdapters.BuscaFacturasGlobalesTableAdapter
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents CMBIdFacturaTextBox As System.Windows.Forms.TextBox
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents VerAcceso2TableAdapter1 As softvFacturacion.ProcedimientosArnoldo3TableAdapters.VerAcceso2TableAdapter
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents VerAcceso2TableAdapter2 As softvFacturacion.ProcedimientosArnoldo3TableAdapters.VerAcceso2TableAdapter
    Friend WithEvents ComboBoxCompanias As System.Windows.Forms.ComboBox
    Friend WithEvents VerAcceso2TableAdapter3 As softvFacturacion.ProcedimientosArnoldo3TableAdapters.VerAcceso2TableAdapter
    Friend WithEvents Column1DataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents FechaDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents CMBDistribuidorTextBox As System.Windows.Forms.TextBox
    Friend WithEvents fecha As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Button7 As System.Windows.Forms.Button
    Friend WithEvents CajeraLabel As System.Windows.Forms.Label
    Friend WithEvents ImporteLabel As System.Windows.Forms.Label
    Friend WithEvents IdFacturaLabel As System.Windows.Forms.Label
    Friend WithEvents CanceladaLabel As System.Windows.Forms.Label
    Friend WithEvents FechaLabel As System.Windows.Forms.Label
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents IdFactura As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Serie As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Factura As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents fechai As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Monto As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents sucursal As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Cajera As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Cancelada As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents fechaf As System.Windows.Forms.DataGridViewTextBoxColumn
End Class



'<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
'Partial Class BRWFACTURAGLOBALESPCIALES
'    Inherits System.Windows.Forms.Form

'    'Form reemplaza a Dispose para limpiar la lista de componentes.
'    <System.Diagnostics.DebuggerNonUserCode()> _
'    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
'        Try
'            If disposing AndAlso components IsNot Nothing Then
'                components.Dispose()
'            End If
'        Finally
'            MyBase.Dispose(disposing)
'        End Try
'    End Sub

'    'Requerido por el Diseñador de Windows Forms
'    Private components As System.ComponentModel.IContainer

'    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
'    'Se puede modificar usando el Diseñador de Windows Forms.  
'    'No lo modifique con el editor de código.
'    <System.Diagnostics.DebuggerStepThrough()> _
'    Private Sub InitializeComponent()
'        components = New System.ComponentModel.Container
'        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
'        Me.Text = "BRWFACTURAGLOBALESPCIALES"
'    End Sub
'End Class
