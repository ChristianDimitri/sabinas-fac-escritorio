﻿Imports System.Data.SqlClient
Imports System.Text
Imports CrystalDecisions.Shared
Imports CrystalDecisions.CrystalReports.Engine
Public Class FrmEstadosCuenta

    Private Sub FrmEstadosCuenta_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        colorea(Me)
        llenaDistribuidor()
        MuestraEstadoCuentaPeriodo()
    End Sub

    Private Sub llenaDistribuidor()
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@Clv_plaza", SqlDbType.Int, 0)
        BaseII.CreateMyParameter("@nombre", SqlDbType.VarChar, "")
        cbDistribuidor.DataSource = BaseII.ConsultaDT("Muestra_Plazas")
    End Sub

    Private Sub MuestraEstadoCuentaPeriodo()
        Dim conexion As New SqlConnection(MiConexion)
        Dim stringBuilder As New StringBuilder("EXEC MuestraEstadoCuentaPeriodo")
        Dim dataAdapter As New SqlDataAdapter(stringBuilder.ToString(), conexion)
        Dim dataTable As New DataTable()
        Dim bindingSource As New BindingSource()
        Try
            dataAdapter.Fill(dataTable)
            bindingSource.DataSource = dataTable
            dgPeriodo.DataSource = bindingSource
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        End Try
    End Sub

    Private Sub MuestraEstadoCuenta(ByVal Id As Integer)
        Dim conexion As New SqlConnection(MiConexion)
        Dim stringBuilder As New StringBuilder("EXEC MuestraEstadoCuenta " + Id.ToString() + "," + cbPlaza.SelectedValue.ToString)
        Dim dataAdapter As New SqlDataAdapter(stringBuilder.ToString(), conexion)
        Dim dataTable As New DataTable()
        Dim bindingSource As New BindingSource()
        Try
            dataAdapter.Fill(dataTable)
            bindingSource.DataSource = dataTable
            dgEstado.DataSource = bindingSource
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        End Try
    End Sub

    Private Sub ReporteEstadoCuenta(ByVal Id As Integer, ByVal Contrato As Integer, ByVal Op As Integer, ByVal imgPP As Byte(), ByVal imgTR As Byte())
        'Dim conexion As New SqlConnection(MiConexion)
        'Dim stringBuilder As New StringBuilder("EXEC ReporteEstadoCuentaNuevo " + Id.ToString() + ", " + Contrato.ToString() + ", " + Op.ToString() + "," + cbPlaza.SelectedValue.ToString)
        'Dim dataAdapter As New SqlDataAdapter(stringBuilder.ToString(), conexion)

        Dim dataSet As New DataSet()
        Dim reportDocument As New ReportDocument
        Dim dataTable As New DataTable
        Dim lp As List(Of String) = New List(Of String)
        lp.Add("EstadoCuenta")
        lp.Add("DetEstadoCuenta")
        'lp.Add("ReporteAreaTecnicaQuejas1")
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@ID", SqlDbType.Int, Id)
        BaseII.CreateMyParameter("@CONTRATO", SqlDbType.BigInt, Contrato)
        BaseII.CreateMyParameter("@OP", SqlDbType.Int, 1)
        BaseII.CreateMyParameter("@idcompania", SqlDbType.Int, GloIdCompania)
        BaseII.CreateMyParameter("@imgPP", SqlDbType.Image, imgPP)
        BaseII.CreateMyParameter("@imgTR", SqlDbType.Image, imgTR)
        dataSet = BaseII.ConsultaDS("ReporteEstadoCuentaNuevo", lp)
        Try

            dataTable = MuestraGeneral()

            dataSet.Tables(0).TableName = "EstadoCuenta"
            dataSet.Tables(1).TableName = "DetEstadoCuenta"
            dataSet.Tables.Add(dataTable)

            'For Each dr As DataRow In dataSet.Tables(0).Rows
            '    dr("CodigoDeBarrasOxxo") = GeneraCodeBar128((dr("oxxo").ToString()))
            'Next

            reportDocument.Load(RutaReportes + "\EstadoCuenta.rpt")
            reportDocument.SetDataSource(dataSet)

            LiTipo = 0
            FrmImprimir.CrystalReportViewer1.ReportSource = reportDocument
            FrmImprimir.Show()

        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        End Try
    End Sub

    Private Sub ReporteEstadoCuentaMuchos(ByVal Id As Integer, ByVal Contrato As Integer, ByVal Op As Integer)
        Dim conexion As New SqlConnection(MiConexion)
        Dim stringBuilder As New StringBuilder("EXEC ReporteEstadoCuentaNuevoMuchos " + Id.ToString() + ", " + Contrato.ToString() + ", " + Op.ToString() + "," + cbPlaza.SelectedValue.ToString + "," + LocClv_session.ToString)
        Dim dataAdapter As New SqlDataAdapter(stringBuilder.ToString(), conexion)
        Dim dataSet As New DataSet()
        Dim reportDocument As New ReportDocument
        Dim dataTable As New DataTable

        Try

            dataAdapter.Fill(dataSet)
            dataTable = MuestraGeneral()

            dataSet.Tables(0).TableName = "EstadoCuenta"
            dataSet.Tables(1).TableName = "DetEstadoCuenta"
            dataSet.Tables.Add(dataTable)

            'For Each dr As DataRow In dataSet.Tables(0).Rows
            '    dr("CodigoDeBarrasOxxo") = GeneraCodeBar128((dr("oxxo").ToString()))
            'Next

            reportDocument.Load(RutaReportes + "\EstadoCuenta.rpt")
            reportDocument.SetDataSource(dataSet)

            LiTipo = 0
            FrmImprimir.CrystalReportViewer1.ReportSource = reportDocument
            FrmImprimir.Show()

        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        End Try
    End Sub

    Function MuestraGeneral() As DataTable
        Dim conexion As New SqlConnection(MiConexion)
        Dim stringBuilder As New StringBuilder("EXEC MuestraGeneral")
        Dim dataAdapter As New SqlDataAdapter(stringBuilder.ToString(), conexion)
        Dim dataTable As New DataTable()

        Try

            dataAdapter.Fill(dataTable)

        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        End Try

        Return dataTable

    End Function

    Private Sub bnVerTodos_Click(sender As System.Object, e As System.EventArgs) Handles bnVerTodos.Click
        If dgPeriodo.Rows.Count = 0 Then
            MsgBox("Selecciona un periodo.")
            Exit Sub
        End If
        Dim lineaPP As String
        Dim lineaTR As String
        Dim imgPP As Byte()
        Dim imgTR As Byte()
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@Clv_Session", ParameterDirection.Output, SqlDbType.BigInt)
        BaseII.ProcedimientoOutPut("DameClv_Session")
        LocClv_session = BaseII.dicoPar("@Clv_Session")
        Dim dt As DataTable
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@id", SqlDbType.Int, dgPeriodo.SelectedCells(0).Value)
        dt = BaseII.ConsultaDT("DameLineasCapturaMuchos")
        Dim size As Integer
        size = dt.Rows.Count
        Dim i As Integer = 0
        While i < size
            lineaPP = dt.Rows(i)(0).ToString
            lineaTR = dt.Rows(i)(1).ToString
            imgPP = GeneraCodeBar128Nuevo(lineaPP)
            imgTR = GeneraCodeBar128Nuevo(lineaTR)
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("imgPP", SqlDbType.Image, imgPP)
            BaseII.CreateMyParameter("imgTR", SqlDbType.Image, imgTR)
            BaseII.CreateMyParameter("@idEstadoCuenta", SqlDbType.BigInt, dt.Rows(i)(2).ToString)
            BaseII.CreateMyParameter("@clv_session", SqlDbType.BigInt, LocClv_session)
            BaseII.Inserta("InsertaImgAux")
            i = i + 1
        End While
        ReporteEstadoCuentaMuchos(dgPeriodo.SelectedCells(0).Value, 0, 0)
    End Sub
    Public Function GeneraCodeBar128Nuevo(ByVal Data As String) As Byte()
        Dim b As New BarcodeLib.Barcode()
        Dim imgBarCode As Byte()
        Dim barcode As New PictureBox()
        Dim W As Integer = Convert.ToInt32(350)
        Dim H As Integer = Convert.ToInt32(100)
        'Dim Data As [String] = "31000016024201101070637005"
        Dim Align As BarcodeLib.AlignmentPositions = BarcodeLib.AlignmentPositions.CENTER
        Align = BarcodeLib.AlignmentPositions.CENTER
        Dim type As BarcodeLib.TYPE = BarcodeLib.TYPE.UNSPECIFIED
        type = BarcodeLib.TYPE.CODE128
        Try
            If type <> BarcodeLib.TYPE.UNSPECIFIED Then
                b.IncludeLabel = True
                b.Alignment = Align
                b.RotateFlipType = (RotateFlipType.RotateNoneFlipNone)
                b.LabelPosition = BarcodeLib.LabelPositions.BOTTOMCENTER
                barcode.Image = b.Encode(type, Data, Color.Black, Color.White, W, H)
            End If
            barcode.Width = barcode.Image.Width
            barcode.Height = barcode.Image.Height
            'File.WriteAllBytes(@"C:\XSD\1.jpg", imgbites);                
            Dim imgbites As Byte() = ImageToByte2(barcode.Image)
            imgBarCode = imgbites
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
        Return imgBarCode
    End Function

    Private Sub bnVer_Click(sender As System.Object, e As System.EventArgs) Handles bnVer.Click
        If dgPeriodo.Rows.Count = 0 Then
            MsgBox("Selecciona un estado de cuenta.")
            Exit Sub
        End If
        Dim imgPP As Byte()
        Dim imgTR As Byte()
        Dim lineaPP As String
        Dim lineaTR As String
        Dim conaux As Double
        Dim comando As New SqlCommand()
        Dim conexion As New SqlConnection(MiConexion)
        conexion.Open()
        comando.Connection = conexion
        Dim arr = dgEstado.SelectedCells(1).Value.ToString.Split("-")
        comando.CommandText = "select contrato from Rel_Contratos_Companias where ContratoCompania=" + arr(0) + " and IdCompania=" + arr(1)
        conaux = comando.ExecuteScalar()
        conexion.Close()
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@idEstadoCuenta", SqlDbType.Int, dgEstado.SelectedCells(0).Value)
        BaseII.CreateMyParameter("@contrato", SqlDbType.BigInt, conaux)
        BaseII.CreateMyParameter("@lineaPP", ParameterDirection.Output, SqlDbType.VarChar, 30)
        BaseII.CreateMyParameter("@lineaTR", ParameterDirection.Output, SqlDbType.VarChar, 30)
        BaseII.ProcedimientoOutPut("DameLineasCaptura2")
        lineaPP = BaseII.dicoPar("@lineaPP")
        lineaTR = BaseII.dicoPar("@lineaTR")
        imgPP = GeneraCodeBar128Nuevo(lineaPP)
        imgTR = GeneraCodeBar128Nuevo(lineaTR)
        ReporteEstadoCuenta(dgPeriodo.SelectedCells(0).Value, conaux, 1, imgPP, imgTR)
    End Sub

    Private Sub dgPeriodo_SelectionChanged(sender As System.Object, e As System.EventArgs) Handles dgPeriodo.SelectionChanged
        Try
            If dgPeriodo.Rows.Count = 0 Then Exit Sub
            MuestraEstadoCuenta(dgPeriodo.SelectedCells(0).Value)
        Catch ex As Exception

        End Try
    End Sub

    Private Sub Button1_Click(sender As System.Object, e As System.EventArgs) Handles Button1.Click
        Me.Close()
    End Sub

    Private Sub cbDistribuidor_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cbDistribuidor.SelectedIndexChanged
        Try
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@clv_plaza", SqlDbType.Int, cbDistribuidor.SelectedValue)
            cbPlaza.DataSource = BaseII.ConsultaDT("MuestraCompaniaPlaza")
        Catch ex As Exception

        End Try
    End Sub

    Private Sub cbPlaza_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cbPlaza.SelectedIndexChanged
        If dgPeriodo.Rows.Count = 0 Then
            Exit Sub
        End If
        MuestraEstadoCuenta(dgPeriodo.SelectedCells(0).Value)
    End Sub
End Class