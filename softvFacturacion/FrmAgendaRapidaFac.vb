﻿Imports System.Data.SqlClient
Public Class FrmAgendaRapidaFac

    Private Sub FrmAgendaRapidaFac_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        colorea(Me)
        'Vamos a validar que realmente tenga ordenes pendientes de las cuáles requeire que salga la agenda
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@Clv_Factura", SqlDbType.BigInt, GloClv_Factura)
        BaseII.CreateMyParameter("@valida", ParameterDirection.Output, SqlDbType.Int)
        BaseII.ProcedimientoOutPut("ValidaExistenciaOrdenPorFactura")
        If BaseII.dicoPar("@valida") = 0 Then
            Me.Close()
            Exit Sub
        End If
        'Fin validación
        Me.TextComentario.Text = ""
        Me.DateTimePicker1.Value = Today
        Me.ComboBox2.DataSource = spConsultaTurnos()
        Muestra_Tecnicos_Almacen()
        'Me.ComboBox1.Text = "6:00"
        'Me.ComboBox1.Enabled = False

        'Dim CON As New SqlConnection(MiConexion)
        ''Me.TextBox1.Text = GLONOM_TECNICO
        ''Me.Clv_TecnicoTextBox.Text = GloClv_tecnico
        'CON.Open()
        ''Me.Muestra_Tecnicos_AlmacenTableAdapter.Connection = CON
        ''Me.Muestra_Tecnicos_AlmacenTableAdapter.Fill(Me.DataSetLidia2.Muestra_Tecnicos_Almacen, 1)
        ''Me.MUESTRAHORAS_CITASTableAdapter.Connection = CON
        ''Me.MUESTRAHORAS_CITASTableAdapter.Fill(Me.NewSofTvDataSet.MUESTRAHORAS_CITAS, Me.DateTimePicker1.Text, GloClv_tecnico)
        'CON.Close()
    End Sub

    Private Sub Muestra_Tecnicos_Almacen()
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@op", SqlDbType.Int, GloContrato)
        ComboBox1.DataSource = BaseII.ConsultaDT("Muestra_Tecnicos_Almacen")
    End Sub
  

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        'If IsNumeric(Me.ComboBox1.SelectedValue) = False Or Me.ComboBox1.Text = "" Then
        '    MsgBox("Seleccione El Técnico Por Favor", MsgBoxStyle.Information)
        '    Exit Sub
        'End If
        If Me.ComboBox2.SelectedValue = 0 Or Me.ComboBox2.SelectedValue = Nothing Then
            MsgBox("Seleccione el Turno Por Favor", MsgBoxStyle.Information)
            Exit Sub
        End If
        Genera_Cita_OrdserFac(GloOrdenAgenda, TextComentario.Text, ComboBox2.Text, DateTimePicker1.Value)
        Me.Close()
    End Sub

    Public Sub Genera_Cita_OrdserFac(ByVal eClv_Queja As Integer, ByVal ComentarioAgenda As String, Optional ByVal prmTurno As String = "", Optional ByVal prmFecha As String = "")

        Dim CON80 As New SqlClient.SqlConnection(MiConexion)
        Dim CMD As New SqlClient.SqlCommand()

        Try
            'CMD = New SqlClient.SqlCommand()
            'CON80.Open()
            'With CMD
            '    .CommandText = "Genera_Cita_OrdserFac"
            '    .CommandType = CommandType.StoredProcedure
            '    .Connection = CON80
            '    .CommandTimeout = 0

            '    Dim prmClaveTecnico As New SqlParameter("@Clv_Tecnico", SqlDbType.BigInt)
            '    prmClaveTecnico.Value = ComboBox1.SelectedValue
            '    .Parameters.Add(prmClaveTecnico)

            '    Dim prmContrato As New SqlParameter("@Clv_Queja", SqlDbType.BigInt)
            '    prmContrato.Value = eClv_Queja
            '    .Parameters.Add(prmContrato)

            '    If prmTurno <> "" Then
            '        Dim prTurno As New SqlParameter("@Turno", SqlDbType.NVarChar, 50)
            '        prTurno.Value = prmTurno
            '        .Parameters.Add(prTurno)
            '    End If

            '    If IsDate(prmFecha) Then
            '        Dim prFecha As New SqlParameter("@Fecha", SqlDbType.DateTime)
            '        prFecha.Value = CType(prmFecha, Date)
            '        .Parameters.Add(prFecha)
            '    End If


            '    Dim prComentarioAgenda As New SqlParameter("@Comentario", SqlDbType.VarChar, 250)
            '    prComentarioAgenda.Value = ComentarioAgenda
            '    .Parameters.Add(prComentarioAgenda)

            '    Dim i As Integer = .ExecuteNonQuery()
            '    'MsgBox("Se ha guardado el Cliente como: SOLO INTERNET con éxito.", MsgBoxStyle.Information, "CLIENTE GUARDADO COMO: SOLO INTERNET")

            'End With
            'CON80.Close()
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@Clv_Tecnico", SqlDbType.BigInt, 0)
            BaseII.CreateMyParameter("@Clv_Queja", SqlDbType.BigInt, eClv_Queja)
            BaseII.CreateMyParameter("@Turno", SqlDbType.NVarChar, prmTurno)
            BaseII.CreateMyParameter("@Fecha", SqlDbType.DateTime, CType(prmFecha, Date))
            BaseII.CreateMyParameter("@Comentario", SqlDbType.VarChar, ComentarioAgenda)
            BaseII.Inserta("Genera_Cita_OrdserFac")
        Catch ex As Exception
            'If CON80.State = ConnectionState.Open Then CON80.Close()
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub

End Class