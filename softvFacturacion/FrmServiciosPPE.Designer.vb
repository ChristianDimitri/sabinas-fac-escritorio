<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmServiciosPPE
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim Clv_TxtLabel As System.Windows.Forms.Label
        Dim DescripcionLabel As System.Windows.Forms.Label
        Dim PrecioLabel As System.Windows.Forms.Label
        Dim HitsLabel As System.Windows.Forms.Label
        Dim ClasificacionLabel As System.Windows.Forms.Label
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle4 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.CMBLabel1 = New System.Windows.Forms.Label()
        Me.TextBox1 = New System.Windows.Forms.TextBox()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.DateTimePicker1 = New System.Windows.Forms.DateTimePicker()
        Me.Button6 = New System.Windows.Forms.Button()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Panel2 = New System.Windows.Forms.Panel()
        Me.ClasificacionLabel1 = New System.Windows.Forms.Label()
        Me.MuestraServiciosPPEBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.EricDataSet = New softvFacturacion.EricDataSet()
        Me.HitsLabel1 = New System.Windows.Forms.Label()
        Me.PrecioLabel1 = New System.Windows.Forms.Label()
        Me.DescripcionLabel1 = New System.Windows.Forms.Label()
        Me.Clv_TxtLabel1 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Button3 = New System.Windows.Forms.Button()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.TextBox3 = New System.Windows.Forms.TextBox()
        Me.Button2 = New System.Windows.Forms.Button()
        Me.TextBox2 = New System.Windows.Forms.TextBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.MuestraProgramacionPPEDataGridView = New System.Windows.Forms.DataGridView()
        Me.DataGridViewTextBoxColumn10 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn8 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn9 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.MuestraProgramacionPPEBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Clv_PrograTextBox = New System.Windows.Forms.TextBox()
        Me.Panel3 = New System.Windows.Forms.Panel()
        Me.ImagenPictureBox = New System.Windows.Forms.PictureBox()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Button4 = New System.Windows.Forms.Button()
        Me.Button5 = New System.Windows.Forms.Button()
        Me.Clv_TxtTextBox = New System.Windows.Forms.TextBox()
        Me.MuestraProgramacionPPETableAdapter = New softvFacturacion.EricDataSetTableAdapters.MuestraProgramacionPPETableAdapter()
        Me.MuestraServiciosPPETableAdapter = New softvFacturacion.EricDataSetTableAdapters.MuestraServiciosPPETableAdapter()
        Me.MuestraServiciosPPEDataGridView = New System.Windows.Forms.DataGridView()
        Me.DataGridViewTextBoxColumn2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn3 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn4 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn5 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn6 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn7 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewImageColumn1 = New System.Windows.Forms.DataGridViewImageColumn()
        Me.DAMEFECHADELSERVIDORBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.DAMEFECHADELSERVIDORTableAdapter = New softvFacturacion.EricDataSetTableAdapters.DAMEFECHADELSERVIDORTableAdapter()
        Me.FechaLabel1 = New System.Windows.Forms.Label()
        Clv_TxtLabel = New System.Windows.Forms.Label()
        DescripcionLabel = New System.Windows.Forms.Label()
        PrecioLabel = New System.Windows.Forms.Label()
        HitsLabel = New System.Windows.Forms.Label()
        ClasificacionLabel = New System.Windows.Forms.Label()
        Me.Panel1.SuspendLayout()
        Me.Panel2.SuspendLayout()
        CType(Me.MuestraServiciosPPEBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.EricDataSet, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.MuestraProgramacionPPEDataGridView, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.MuestraProgramacionPPEBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel3.SuspendLayout()
        CType(Me.ImagenPictureBox, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.MuestraServiciosPPEDataGridView, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DAMEFECHADELSERVIDORBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Clv_TxtLabel
        '
        Clv_TxtLabel.AutoSize = True
        Clv_TxtLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Clv_TxtLabel.ForeColor = System.Drawing.Color.White
        Clv_TxtLabel.Location = New System.Drawing.Point(21, 69)
        Clv_TxtLabel.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Clv_TxtLabel.Name = "Clv_TxtLabel"
        Clv_TxtLabel.Size = New System.Drawing.Size(68, 20)
        Clv_TxtLabel.TabIndex = 18
        Clv_TxtLabel.Text = "Clave :"
        '
        'DescripcionLabel
        '
        DescripcionLabel.AutoSize = True
        DescripcionLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DescripcionLabel.ForeColor = System.Drawing.Color.White
        DescripcionLabel.Location = New System.Drawing.Point(23, 118)
        DescripcionLabel.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        DescripcionLabel.Name = "DescripcionLabel"
        DescripcionLabel.Size = New System.Drawing.Size(122, 20)
        DescripcionLabel.TabIndex = 19
        DescripcionLabel.Text = "Descripción :"
        '
        'PrecioLabel
        '
        PrecioLabel.AutoSize = True
        PrecioLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        PrecioLabel.ForeColor = System.Drawing.Color.White
        PrecioLabel.Location = New System.Drawing.Point(21, 214)
        PrecioLabel.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        PrecioLabel.Name = "PrecioLabel"
        PrecioLabel.Size = New System.Drawing.Size(69, 20)
        PrecioLabel.TabIndex = 20
        PrecioLabel.Text = "Precio:"
        '
        'HitsLabel
        '
        HitsLabel.AutoSize = True
        HitsLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        HitsLabel.ForeColor = System.Drawing.Color.White
        HitsLabel.Location = New System.Drawing.Point(23, 267)
        HitsLabel.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        HitsLabel.Name = "HitsLabel"
        HitsLabel.Size = New System.Drawing.Size(50, 20)
        HitsLabel.TabIndex = 21
        HitsLabel.Text = "Hits:"
        '
        'ClasificacionLabel
        '
        ClasificacionLabel.AutoSize = True
        ClasificacionLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        ClasificacionLabel.ForeColor = System.Drawing.Color.White
        ClasificacionLabel.Location = New System.Drawing.Point(23, 316)
        ClasificacionLabel.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        ClasificacionLabel.Name = "ClasificacionLabel"
        ClasificacionLabel.Size = New System.Drawing.Size(130, 20)
        ClasificacionLabel.TabIndex = 22
        ClasificacionLabel.Text = "Clasificación :"
        '
        'CMBLabel1
        '
        Me.CMBLabel1.AutoSize = True
        Me.CMBLabel1.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBLabel1.ForeColor = System.Drawing.Color.FromArgb(CType(CType(128, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.CMBLabel1.Location = New System.Drawing.Point(12, 6)
        Me.CMBLabel1.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.CMBLabel1.Name = "CMBLabel1"
        Me.CMBLabel1.Size = New System.Drawing.Size(306, 29)
        Me.CMBLabel1.TabIndex = 11
        Me.CMBLabel1.Text = "Buscar Servicio PPE por:"
        '
        'TextBox1
        '
        Me.TextBox1.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.TextBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBox1.Location = New System.Drawing.Point(23, 66)
        Me.TextBox1.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.TextBox1.Name = "TextBox1"
        Me.TextBox1.Size = New System.Drawing.Size(185, 26)
        Me.TextBox1.TabIndex = 0
        '
        'Button1
        '
        Me.Button1.BackColor = System.Drawing.Color.DarkOrange
        Me.Button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button1.Location = New System.Drawing.Point(23, 101)
        Me.Button1.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(117, 28)
        Me.Button1.TabIndex = 1
        Me.Button1.Text = "Buscar"
        Me.Button1.UseVisualStyleBackColor = False
        '
        'Panel1
        '
        Me.Panel1.BackColor = System.Drawing.Color.WhiteSmoke
        Me.Panel1.Controls.Add(Me.DateTimePicker1)
        Me.Panel1.Controls.Add(Me.Button6)
        Me.Panel1.Controls.Add(Me.Label1)
        Me.Panel1.Controls.Add(Me.Panel2)
        Me.Panel1.Controls.Add(Me.Button3)
        Me.Panel1.Controls.Add(Me.Label4)
        Me.Panel1.Controls.Add(Me.TextBox3)
        Me.Panel1.Controls.Add(Me.Button2)
        Me.Panel1.Controls.Add(Me.TextBox2)
        Me.Panel1.Controls.Add(Me.Label3)
        Me.Panel1.Controls.Add(Me.Button1)
        Me.Panel1.Controls.Add(Me.TextBox1)
        Me.Panel1.Controls.Add(Me.CMBLabel1)
        Me.Panel1.Controls.Add(Me.Label2)
        Me.Panel1.Location = New System.Drawing.Point(4, 5)
        Me.Panel1.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(361, 884)
        Me.Panel1.TabIndex = 0
        Me.Panel1.TabStop = True
        '
        'DateTimePicker1
        '
        Me.DateTimePicker1.CalendarFont = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.DateTimePicker1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.DateTimePicker1.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.DateTimePicker1.Location = New System.Drawing.Point(23, 373)
        Me.DateTimePicker1.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.DateTimePicker1.Name = "DateTimePicker1"
        Me.DateTimePicker1.Size = New System.Drawing.Size(135, 26)
        Me.DateTimePicker1.TabIndex = 26
        Me.DateTimePicker1.Value = New Date(2008, 1, 18, 0, 0, 0, 0)
        '
        'Button6
        '
        Me.Button6.BackColor = System.Drawing.Color.DarkOrange
        Me.Button6.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button6.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button6.Location = New System.Drawing.Point(23, 407)
        Me.Button6.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Button6.Name = "Button6"
        Me.Button6.Size = New System.Drawing.Size(117, 28)
        Me.Button6.TabIndex = 7
        Me.Button6.Text = "Buscar"
        Me.Button6.UseVisualStyleBackColor = False
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(19, 350)
        Me.Label1.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(72, 20)
        Me.Label1.TabIndex = 24
        Me.Label1.Text = "Fecha :"
        '
        'Panel2
        '
        Me.Panel2.BackColor = System.Drawing.Color.DarkOrange
        Me.Panel2.Controls.Add(ClasificacionLabel)
        Me.Panel2.Controls.Add(Me.ClasificacionLabel1)
        Me.Panel2.Controls.Add(HitsLabel)
        Me.Panel2.Controls.Add(Me.HitsLabel1)
        Me.Panel2.Controls.Add(PrecioLabel)
        Me.Panel2.Controls.Add(Me.PrecioLabel1)
        Me.Panel2.Controls.Add(DescripcionLabel)
        Me.Panel2.Controls.Add(Me.DescripcionLabel1)
        Me.Panel2.Controls.Add(Clv_TxtLabel)
        Me.Panel2.Controls.Add(Me.Clv_TxtLabel1)
        Me.Panel2.Controls.Add(Me.Label5)
        Me.Panel2.Location = New System.Drawing.Point(4, 465)
        Me.Panel2.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(351, 415)
        Me.Panel2.TabIndex = 21
        '
        'ClasificacionLabel1
        '
        Me.ClasificacionLabel1.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.MuestraServiciosPPEBindingSource, "Clasificacion", True))
        Me.ClasificacionLabel1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ClasificacionLabel1.Location = New System.Drawing.Point(27, 336)
        Me.ClasificacionLabel1.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.ClasificacionLabel1.Name = "ClasificacionLabel1"
        Me.ClasificacionLabel1.Size = New System.Drawing.Size(227, 28)
        Me.ClasificacionLabel1.TabIndex = 23
        '
        'MuestraServiciosPPEBindingSource
        '
        Me.MuestraServiciosPPEBindingSource.DataMember = "MuestraServiciosPPE"
        Me.MuestraServiciosPPEBindingSource.DataSource = Me.EricDataSet
        '
        'EricDataSet
        '
        Me.EricDataSet.DataSetName = "EricDataSet"
        Me.EricDataSet.EnforceConstraints = False
        Me.EricDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'HitsLabel1
        '
        Me.HitsLabel1.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.MuestraServiciosPPEBindingSource, "Hits", True))
        Me.HitsLabel1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.HitsLabel1.Location = New System.Drawing.Point(27, 287)
        Me.HitsLabel1.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.HitsLabel1.Name = "HitsLabel1"
        Me.HitsLabel1.Size = New System.Drawing.Size(228, 28)
        Me.HitsLabel1.TabIndex = 22
        '
        'PrecioLabel1
        '
        Me.PrecioLabel1.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.MuestraServiciosPPEBindingSource, "Precio", True))
        Me.PrecioLabel1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.PrecioLabel1.Location = New System.Drawing.Point(25, 234)
        Me.PrecioLabel1.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.PrecioLabel1.Name = "PrecioLabel1"
        Me.PrecioLabel1.Size = New System.Drawing.Size(227, 28)
        Me.PrecioLabel1.TabIndex = 21
        '
        'DescripcionLabel1
        '
        Me.DescripcionLabel1.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.MuestraServiciosPPEBindingSource, "Descripcion", True))
        Me.DescripcionLabel1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.DescripcionLabel1.Location = New System.Drawing.Point(25, 138)
        Me.DescripcionLabel1.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.DescripcionLabel1.Name = "DescripcionLabel1"
        Me.DescripcionLabel1.Size = New System.Drawing.Size(316, 70)
        Me.DescripcionLabel1.TabIndex = 20
        '
        'Clv_TxtLabel1
        '
        Me.Clv_TxtLabel1.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.MuestraServiciosPPEBindingSource, "Clv_Txt", True))
        Me.Clv_TxtLabel1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Clv_TxtLabel1.Location = New System.Drawing.Point(25, 89)
        Me.Clv_TxtLabel1.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Clv_TxtLabel1.Name = "Clv_TxtLabel1"
        Me.Clv_TxtLabel1.Size = New System.Drawing.Size(243, 28)
        Me.Clv_TxtLabel1.TabIndex = 19
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.ForeColor = System.Drawing.Color.White
        Me.Label5.Location = New System.Drawing.Point(13, 15)
        Me.Label5.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(248, 25)
        Me.Label5.TabIndex = 18
        Me.Label5.Text = "Datos del Servicio PPE :"
        '
        'Button3
        '
        Me.Button3.BackColor = System.Drawing.Color.DarkOrange
        Me.Button3.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button3.Location = New System.Drawing.Point(23, 306)
        Me.Button3.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Button3.Name = "Button3"
        Me.Button3.Size = New System.Drawing.Size(117, 28)
        Me.Button3.TabIndex = 5
        Me.Button3.Text = "Buscar"
        Me.Button3.UseVisualStyleBackColor = False
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.Location = New System.Drawing.Point(19, 249)
        Me.Label4.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(130, 20)
        Me.Label4.TabIndex = 17
        Me.Label4.Text = "Clasificación :"
        '
        'TextBox3
        '
        Me.TextBox3.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.TextBox3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBox3.Location = New System.Drawing.Point(23, 272)
        Me.TextBox3.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.TextBox3.Name = "TextBox3"
        Me.TextBox3.Size = New System.Drawing.Size(185, 26)
        Me.TextBox3.TabIndex = 4
        '
        'Button2
        '
        Me.Button2.BackColor = System.Drawing.Color.DarkOrange
        Me.Button2.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button2.Location = New System.Drawing.Point(23, 203)
        Me.Button2.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(117, 28)
        Me.Button2.TabIndex = 3
        Me.Button2.Text = "Buscar"
        Me.Button2.UseVisualStyleBackColor = False
        '
        'TextBox2
        '
        Me.TextBox2.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.TextBox2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBox2.Location = New System.Drawing.Point(23, 169)
        Me.TextBox2.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.TextBox2.Name = "TextBox2"
        Me.TextBox2.Size = New System.Drawing.Size(324, 26)
        Me.TextBox2.TabIndex = 2
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(19, 145)
        Me.Label3.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(122, 20)
        Me.Label3.TabIndex = 16
        Me.Label3.Text = "Descripción :"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(19, 43)
        Me.Label2.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(68, 20)
        Me.Label2.TabIndex = 15
        Me.Label2.Text = "Clave :"
        '
        'MuestraProgramacionPPEDataGridView
        '
        Me.MuestraProgramacionPPEDataGridView.AllowUserToAddRows = False
        Me.MuestraProgramacionPPEDataGridView.AllowUserToDeleteRows = False
        Me.MuestraProgramacionPPEDataGridView.AutoGenerateColumns = False
        Me.MuestraProgramacionPPEDataGridView.BackgroundColor = System.Drawing.Color.WhiteSmoke
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.MuestraProgramacionPPEDataGridView.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle1
        Me.MuestraProgramacionPPEDataGridView.ColumnHeadersHeight = 25
        Me.MuestraProgramacionPPEDataGridView.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.DataGridViewTextBoxColumn10, Me.DataGridViewTextBoxColumn8, Me.DataGridViewTextBoxColumn9})
        Me.MuestraProgramacionPPEDataGridView.DataSource = Me.MuestraProgramacionPPEBindingSource
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.MuestraProgramacionPPEDataGridView.DefaultCellStyle = DataGridViewCellStyle2
        Me.MuestraProgramacionPPEDataGridView.Location = New System.Drawing.Point(4, 241)
        Me.MuestraProgramacionPPEDataGridView.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.MuestraProgramacionPPEDataGridView.Name = "MuestraProgramacionPPEDataGridView"
        Me.MuestraProgramacionPPEDataGridView.ReadOnly = True
        Me.MuestraProgramacionPPEDataGridView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.MuestraProgramacionPPEDataGridView.Size = New System.Drawing.Size(391, 484)
        Me.MuestraProgramacionPPEDataGridView.TabIndex = 22
        Me.MuestraProgramacionPPEDataGridView.TabStop = False
        '
        'DataGridViewTextBoxColumn10
        '
        Me.DataGridViewTextBoxColumn10.DataPropertyName = "Fecha"
        Me.DataGridViewTextBoxColumn10.HeaderText = "Programación"
        Me.DataGridViewTextBoxColumn10.Name = "DataGridViewTextBoxColumn10"
        Me.DataGridViewTextBoxColumn10.ReadOnly = True
        Me.DataGridViewTextBoxColumn10.Width = 220
        '
        'DataGridViewTextBoxColumn8
        '
        Me.DataGridViewTextBoxColumn8.DataPropertyName = "Clv_Progra"
        Me.DataGridViewTextBoxColumn8.HeaderText = "Clv_Progra"
        Me.DataGridViewTextBoxColumn8.Name = "DataGridViewTextBoxColumn8"
        Me.DataGridViewTextBoxColumn8.ReadOnly = True
        Me.DataGridViewTextBoxColumn8.Visible = False
        '
        'DataGridViewTextBoxColumn9
        '
        Me.DataGridViewTextBoxColumn9.DataPropertyName = "Clv_Txt"
        Me.DataGridViewTextBoxColumn9.HeaderText = "Clv_Txt"
        Me.DataGridViewTextBoxColumn9.Name = "DataGridViewTextBoxColumn9"
        Me.DataGridViewTextBoxColumn9.ReadOnly = True
        Me.DataGridViewTextBoxColumn9.Visible = False
        '
        'MuestraProgramacionPPEBindingSource
        '
        Me.MuestraProgramacionPPEBindingSource.DataMember = "MuestraProgramacionPPE"
        Me.MuestraProgramacionPPEBindingSource.DataSource = Me.EricDataSet
        '
        'Clv_PrograTextBox
        '
        Me.Clv_PrograTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.MuestraProgramacionPPEBindingSource, "Clv_Progra", True))
        Me.Clv_PrograTextBox.Location = New System.Drawing.Point(1140, 777)
        Me.Clv_PrograTextBox.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Clv_PrograTextBox.Name = "Clv_PrograTextBox"
        Me.Clv_PrograTextBox.ReadOnly = True
        Me.Clv_PrograTextBox.Size = New System.Drawing.Size(12, 22)
        Me.Clv_PrograTextBox.TabIndex = 23
        Me.Clv_PrograTextBox.TabStop = False
        '
        'Panel3
        '
        Me.Panel3.BackColor = System.Drawing.Color.DarkOrange
        Me.Panel3.Controls.Add(Me.ImagenPictureBox)
        Me.Panel3.Controls.Add(Me.Label6)
        Me.Panel3.Controls.Add(Me.MuestraProgramacionPPEDataGridView)
        Me.Panel3.Location = New System.Drawing.Point(953, 5)
        Me.Panel3.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Panel3.Name = "Panel3"
        Me.Panel3.Size = New System.Drawing.Size(399, 729)
        Me.Panel3.TabIndex = 22
        '
        'ImagenPictureBox
        '
        Me.ImagenPictureBox.DataBindings.Add(New System.Windows.Forms.Binding("Image", Me.MuestraServiciosPPEBindingSource, "Imagen", True))
        Me.ImagenPictureBox.Location = New System.Drawing.Point(133, 15)
        Me.ImagenPictureBox.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.ImagenPictureBox.Name = "ImagenPictureBox"
        Me.ImagenPictureBox.Size = New System.Drawing.Size(181, 219)
        Me.ImagenPictureBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.ImagenPictureBox.TabIndex = 23
        Me.ImagenPictureBox.TabStop = False
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.BackColor = System.Drawing.Color.DarkOrange
        Me.Label6.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.ForeColor = System.Drawing.Color.White
        Me.Label6.Location = New System.Drawing.Point(20, 15)
        Me.Label6.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(96, 25)
        Me.Label6.TabIndex = 18
        Me.Label6.Text = "Imagen :"
        Me.Label6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Button4
        '
        Me.Button4.BackColor = System.Drawing.Color.DarkOrange
        Me.Button4.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button4.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button4.Location = New System.Drawing.Point(1063, 766)
        Me.Button4.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Button4.Name = "Button4"
        Me.Button4.Size = New System.Drawing.Size(181, 44)
        Me.Button4.TabIndex = 8
        Me.Button4.Text = "&ACEPTAR"
        Me.Button4.UseVisualStyleBackColor = False
        '
        'Button5
        '
        Me.Button5.BackColor = System.Drawing.Color.DarkOrange
        Me.Button5.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button5.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button5.Location = New System.Drawing.Point(1063, 817)
        Me.Button5.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Button5.Name = "Button5"
        Me.Button5.Size = New System.Drawing.Size(181, 44)
        Me.Button5.TabIndex = 9
        Me.Button5.Text = "&SALIR"
        Me.Button5.UseVisualStyleBackColor = False
        '
        'Clv_TxtTextBox
        '
        Me.Clv_TxtTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.MuestraServiciosPPEBindingSource, "Clv_Txt", True))
        Me.Clv_TxtTextBox.Location = New System.Drawing.Point(1161, 779)
        Me.Clv_TxtTextBox.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Clv_TxtTextBox.Name = "Clv_TxtTextBox"
        Me.Clv_TxtTextBox.ReadOnly = True
        Me.Clv_TxtTextBox.Size = New System.Drawing.Size(12, 22)
        Me.Clv_TxtTextBox.TabIndex = 25
        Me.Clv_TxtTextBox.TabStop = False
        '
        'MuestraProgramacionPPETableAdapter
        '
        Me.MuestraProgramacionPPETableAdapter.ClearBeforeFill = True
        '
        'MuestraServiciosPPETableAdapter
        '
        Me.MuestraServiciosPPETableAdapter.ClearBeforeFill = True
        '
        'MuestraServiciosPPEDataGridView
        '
        Me.MuestraServiciosPPEDataGridView.AllowUserToAddRows = False
        Me.MuestraServiciosPPEDataGridView.AllowUserToDeleteRows = False
        Me.MuestraServiciosPPEDataGridView.AutoGenerateColumns = False
        Me.MuestraServiciosPPEDataGridView.BackgroundColor = System.Drawing.Color.WhiteSmoke
        DataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.MuestraServiciosPPEDataGridView.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle3
        Me.MuestraServiciosPPEDataGridView.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.DataGridViewTextBoxColumn2, Me.DataGridViewTextBoxColumn3, Me.DataGridViewTextBoxColumn1, Me.DataGridViewTextBoxColumn4, Me.DataGridViewTextBoxColumn5, Me.DataGridViewTextBoxColumn6, Me.DataGridViewTextBoxColumn7, Me.DataGridViewImageColumn1})
        Me.MuestraServiciosPPEDataGridView.DataSource = Me.MuestraServiciosPPEBindingSource
        DataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle4.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.MuestraServiciosPPEDataGridView.DefaultCellStyle = DataGridViewCellStyle4
        Me.MuestraServiciosPPEDataGridView.Location = New System.Drawing.Point(373, 5)
        Me.MuestraServiciosPPEDataGridView.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.MuestraServiciosPPEDataGridView.Name = "MuestraServiciosPPEDataGridView"
        Me.MuestraServiciosPPEDataGridView.ReadOnly = True
        Me.MuestraServiciosPPEDataGridView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.MuestraServiciosPPEDataGridView.Size = New System.Drawing.Size(572, 884)
        Me.MuestraServiciosPPEDataGridView.TabIndex = 24
        Me.MuestraServiciosPPEDataGridView.TabStop = False
        '
        'DataGridViewTextBoxColumn2
        '
        Me.DataGridViewTextBoxColumn2.DataPropertyName = "Clv_Txt"
        Me.DataGridViewTextBoxColumn2.HeaderText = "Clave"
        Me.DataGridViewTextBoxColumn2.Name = "DataGridViewTextBoxColumn2"
        Me.DataGridViewTextBoxColumn2.ReadOnly = True
        '
        'DataGridViewTextBoxColumn3
        '
        Me.DataGridViewTextBoxColumn3.DataPropertyName = "Descripcion"
        Me.DataGridViewTextBoxColumn3.HeaderText = "Descripción"
        Me.DataGridViewTextBoxColumn3.Name = "DataGridViewTextBoxColumn3"
        Me.DataGridViewTextBoxColumn3.ReadOnly = True
        Me.DataGridViewTextBoxColumn3.Width = 280
        '
        'DataGridViewTextBoxColumn1
        '
        Me.DataGridViewTextBoxColumn1.DataPropertyName = "Clv_Servicio"
        Me.DataGridViewTextBoxColumn1.HeaderText = "Clv_Servicio"
        Me.DataGridViewTextBoxColumn1.Name = "DataGridViewTextBoxColumn1"
        Me.DataGridViewTextBoxColumn1.ReadOnly = True
        Me.DataGridViewTextBoxColumn1.Visible = False
        '
        'DataGridViewTextBoxColumn4
        '
        Me.DataGridViewTextBoxColumn4.DataPropertyName = "Precio"
        Me.DataGridViewTextBoxColumn4.HeaderText = "Precio"
        Me.DataGridViewTextBoxColumn4.Name = "DataGridViewTextBoxColumn4"
        Me.DataGridViewTextBoxColumn4.ReadOnly = True
        Me.DataGridViewTextBoxColumn4.Visible = False
        '
        'DataGridViewTextBoxColumn5
        '
        Me.DataGridViewTextBoxColumn5.DataPropertyName = "Clv_PPE"
        Me.DataGridViewTextBoxColumn5.HeaderText = "Clv_PPE"
        Me.DataGridViewTextBoxColumn5.Name = "DataGridViewTextBoxColumn5"
        Me.DataGridViewTextBoxColumn5.ReadOnly = True
        Me.DataGridViewTextBoxColumn5.Visible = False
        '
        'DataGridViewTextBoxColumn6
        '
        Me.DataGridViewTextBoxColumn6.DataPropertyName = "Hits"
        Me.DataGridViewTextBoxColumn6.HeaderText = "Hits"
        Me.DataGridViewTextBoxColumn6.Name = "DataGridViewTextBoxColumn6"
        Me.DataGridViewTextBoxColumn6.ReadOnly = True
        Me.DataGridViewTextBoxColumn6.Visible = False
        '
        'DataGridViewTextBoxColumn7
        '
        Me.DataGridViewTextBoxColumn7.DataPropertyName = "Clasificacion"
        Me.DataGridViewTextBoxColumn7.HeaderText = "Clasificacion"
        Me.DataGridViewTextBoxColumn7.Name = "DataGridViewTextBoxColumn7"
        Me.DataGridViewTextBoxColumn7.ReadOnly = True
        Me.DataGridViewTextBoxColumn7.Visible = False
        '
        'DataGridViewImageColumn1
        '
        Me.DataGridViewImageColumn1.DataPropertyName = "Imagen"
        Me.DataGridViewImageColumn1.HeaderText = "Imagen"
        Me.DataGridViewImageColumn1.Name = "DataGridViewImageColumn1"
        Me.DataGridViewImageColumn1.ReadOnly = True
        Me.DataGridViewImageColumn1.Visible = False
        '
        'DAMEFECHADELSERVIDORBindingSource
        '
        Me.DAMEFECHADELSERVIDORBindingSource.DataMember = "DAMEFECHADELSERVIDOR"
        Me.DAMEFECHADELSERVIDORBindingSource.DataSource = Me.EricDataSet
        '
        'DAMEFECHADELSERVIDORTableAdapter
        '
        Me.DAMEFECHADELSERVIDORTableAdapter.ClearBeforeFill = True
        '
        'FechaLabel1
        '
        Me.FechaLabel1.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.MuestraProgramacionPPEBindingSource, "Fecha", True))
        Me.FechaLabel1.Location = New System.Drawing.Point(1119, 773)
        Me.FechaLabel1.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.FechaLabel1.Name = "FechaLabel1"
        Me.FechaLabel1.Size = New System.Drawing.Size(13, 28)
        Me.FechaLabel1.TabIndex = 26
        '
        'FrmServiciosPPE
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.AutoScroll = True
        Me.ClientSize = New System.Drawing.Size(1379, 913)
        Me.Controls.Add(Me.Button4)
        Me.Controls.Add(Me.FechaLabel1)
        Me.Controls.Add(Me.Clv_TxtTextBox)
        Me.Controls.Add(Me.MuestraServiciosPPEDataGridView)
        Me.Controls.Add(Me.Button5)
        Me.Controls.Add(Me.Clv_PrograTextBox)
        Me.Controls.Add(Me.Panel3)
        Me.Controls.Add(Me.Panel1)
        Me.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.MaximizeBox = False
        Me.Name = "FrmServiciosPPE"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Servicios de Pago Por Evento"
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        Me.Panel2.ResumeLayout(False)
        Me.Panel2.PerformLayout()
        CType(Me.MuestraServiciosPPEBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.EricDataSet, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.MuestraProgramacionPPEDataGridView, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.MuestraProgramacionPPEBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel3.ResumeLayout(False)
        Me.Panel3.PerformLayout()
        CType(Me.ImagenPictureBox, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.MuestraServiciosPPEDataGridView, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DAMEFECHADELSERVIDORBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents EricDataSet As softvFacturacion.EricDataSet
    Friend WithEvents CMBLabel1 As System.Windows.Forms.Label
    Friend WithEvents TextBox1 As System.Windows.Forms.TextBox
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Button3 As System.Windows.Forms.Button
    Friend WithEvents TextBox3 As System.Windows.Forms.TextBox
    Friend WithEvents Button2 As System.Windows.Forms.Button
    Friend WithEvents TextBox2 As System.Windows.Forms.TextBox
    Friend WithEvents Panel2 As System.Windows.Forms.Panel
    Friend WithEvents MuestraProgramacionPPEBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents MuestraProgramacionPPETableAdapter As softvFacturacion.EricDataSetTableAdapters.MuestraProgramacionPPETableAdapter
    Friend WithEvents MuestraProgramacionPPEDataGridView As System.Windows.Forms.DataGridView
    Friend WithEvents Clv_PrograTextBox As System.Windows.Forms.TextBox
    Friend WithEvents Panel3 As System.Windows.Forms.Panel
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Button4 As System.Windows.Forms.Button
    Friend WithEvents DataGridViewTextBoxColumn10 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn8 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn9 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Button5 As System.Windows.Forms.Button
    Friend WithEvents MuestraServiciosPPEBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents MuestraServiciosPPETableAdapter As softvFacturacion.EricDataSetTableAdapters.MuestraServiciosPPETableAdapter
    Friend WithEvents ClasificacionLabel1 As System.Windows.Forms.Label
    Friend WithEvents HitsLabel1 As System.Windows.Forms.Label
    Friend WithEvents PrecioLabel1 As System.Windows.Forms.Label
    Friend WithEvents DescripcionLabel1 As System.Windows.Forms.Label
    Friend WithEvents Clv_TxtLabel1 As System.Windows.Forms.Label
    Friend WithEvents ImagenPictureBox As System.Windows.Forms.PictureBox
    Friend WithEvents Button6 As System.Windows.Forms.Button
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Clv_TxtTextBox As System.Windows.Forms.TextBox
    Friend WithEvents DateTimePicker1 As System.Windows.Forms.DateTimePicker
    Friend WithEvents MuestraServiciosPPEDataGridView As System.Windows.Forms.DataGridView
    Friend WithEvents DataGridViewTextBoxColumn2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn3 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn4 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn5 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn6 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn7 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewImageColumn1 As System.Windows.Forms.DataGridViewImageColumn
    Friend WithEvents DAMEFECHADELSERVIDORBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents DAMEFECHADELSERVIDORTableAdapter As softvFacturacion.EricDataSetTableAdapters.DAMEFECHADELSERVIDORTableAdapter
    Friend WithEvents FechaLabel1 As System.Windows.Forms.Label
End Class
