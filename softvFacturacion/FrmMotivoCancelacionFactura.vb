Imports System.Data.SqlClient
Imports System.IO
Imports System.Collections.Generic
Imports System.ComponentModel
Imports System.Data
Imports System.Drawing
Imports System.Xml
Imports System.Net
Imports System.Linq
Imports System.Text
Imports System.Windows.Forms

Public Class FrmMotivoCancelacionFactura

    Private Sub FrmMotivoCancelacionFactura_FormClosing(sender As Object, e As FormClosingEventArgs) Handles Me.FormClosing

    End Sub

    Private Sub FrmMotivoCancelacionFactura_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        EClvMotCan = -1
        colorea(Me)
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        If eReImprimirF = 0 Then
            Me.Text = "Motivo de Cancelaci�n de el Ticket"
            Me.CMBLabel1.Text = "�Por qu� motivo cancelar�s el ticket?"
            Me.MUESTRAMOTIVOSTableAdapter.Connection = CON
            Me.MUESTRAMOTIVOSTableAdapter.Fill(Me.DataSetEdgar.MUESTRAMOTIVOS, eReImprimirF)
        Else
            Me.Text = "Motivo de ReImpresi�n de el Ticket"
            Me.CMBLabel1.Text = "�Por qu� motivo reimprimir�s el ticket?"
            Me.MUESTRAMOTIVOSTableAdapter.Connection = CON
            Me.MUESTRAMOTIVOSTableAdapter.Fill(Me.DataSetEdgar.MUESTRAMOTIVOS, eReImprimirF)
        End If
        CON.Close()
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        If eReImprimirF = 0 Then
            eEntra = True
            eCancelacionRE = True
        Else
            eEntraReImprimir = True
        End If
        'If eReImprimirF = 0 And DescripcionComboBox.SelectedValue = 25 And tbOtro.Text.Trim.Length = 0 Then
        '    MsgBox("Llena el campo de motivo para poder continuar.")
        '    Exit Sub
        'End If
        'If eReImprimirF = 0 And DescripcionComboBox.SelectedValue = 25 Then
        '    BaseII.limpiaParametros()
        '    BaseII.CreateMyParameter("@motivo", SqlDbType.VarChar, tbOtro.Text)
        '    BaseII.CreateMyParameter("@clv_factura", SqlDbType.BigInt, eCveFactura)
        '    BaseII.Inserta("GuardaMotivo")
        'End If
        Try

            Dim CON As New SqlConnection(MiConexion)
            CON.Open()
            Me.GuardaMotivosTableAdapter.Connection = CON
            Me.GuardaMotivosTableAdapter.Fill(Me.DataSetEdgar.GuardaMotivos, eCveFactura, Me.Clv_MotivoLabel1.Text, LocLoginUsuario)
            CON.Close()
            EClvMotCan = Me.Clv_MotivoLabel1.Text
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
        'If tbRuta.Text.Trim.Length > 0 Then
        '    Try
        '        Dim ms As System.IO.MemoryStream = New System.IO.MemoryStream()
        '        PictureBox1.Image.Save(ms, System.Drawing.Imaging.ImageFormat.Jpeg)
        '        BaseII.limpiaParametros()
        '        BaseII.CreateMyParameter("@clv_factura", SqlDbType.BigInt, eCveFactura)
        '        BaseII.CreateMyParameter("@archivo", SqlDbType.Image, ms.GetBuffer())
        '        BaseII.Inserta("GuardaEvidenciaCancelacion")
        '    Catch ex As Exception
        '        MsgBox(ex.ToString)
        '    End Try
        'End If
        Me.Close()
    End Sub

    'Private Sub FillToolStripButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
    '    Try
    '        Me.GuardaMotivosTableAdapter.Fill(Me.NewsoftvDataSet.GuardaMotivos, New System.Nullable(Of Long)(CType(Clv_FacturaToolStripTextBox.Text, Long)), New System.Nullable(Of Long)(CType(Clv_MotivoToolStripTextBox.Text, Long)))
    '    Catch ex As System.Exception
    '        System.Windows.Forms.MessageBox.Show(ex.Message)
    '    End Try

    'End Sub

    Private Sub DescripcionComboBox_SelectedIndexChanged(sender As Object, e As EventArgs) Handles DescripcionComboBox.SelectedIndexChanged
        'If DescripcionComboBox.SelectedValue = 25 And eReImprimirF = 0 Then
        '    tbOtro.Enabled = True
        'Else
        '    tbOtro.Enabled = False
        'End If
    End Sub

    Private Sub btnRuta_Click(sender As Object, e As EventArgs) Handles btnRuta.Click
        Dim ofd As New OpenFileDialog()
        ofd.Title = "Selecciona un archivo."
        ofd.Filter = "All files (*.*)|*.*|All files (*.*)|*.*"
        ofd.FilterIndex = 1
        ofd.RestoreDirectory = True
        If ofd.ShowDialog() = DialogResult.OK Then
            Dim fi As FileInfo = New FileInfo(ofd.FileName)
            If fi.Length > 1000000 Then
                MsgBox("Imagen muy grande, no soportada por el sistema.")
                Exit Sub
            End If
            tbRuta.Text = ofd.FileName
            PictureBox1.Image = Image.FromFile(ofd.FileName)
        End If
    End Sub

    Private Sub btnLimpiar_Click(sender As Object, e As EventArgs) Handles btnLimpiar.Click
        tbRuta.Text = ""
    End Sub
End Class